<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
    * 5. 스토어 Controller
    * @author 임정원 / 2020-02-14
    * @since  Version 1.0.0
*/

class Store extends CI_Controller {

	# 생성자
	function __construct() {
		parent::__construct();
		$this->load->model('User');
		$this->load->model('Payment');
		$this->load->model('PurchaseModel');
	}

	public function index() {
		$this->response->View('store/index');
	}

	public function paymentInfo() {
		if( !$this->session->userdata('UID') ) $this->util->alert('로그인 후 이용가능합니다.', '/login');
		//$result = $this->Payment->getPaymentInfo( ['nUserNo' => $this->session->userdata('UNO')] );
		$data = $this->PurchaseModel->getPayInfo();

		$data['emPayYN'] = ($data['result'] == "SUCCESS") ? "Y" : "N";

		if($data['emPayYN'] == "Y"){
			$data['vCardNo'] = substr($data['vCardNo'], 0, 4)."************";
			$data['vExpdt'] = "****";
			$data['vPhone'] = "***********";
		}
		else{
			$data['vCardNo'] = '';
			$data['vExpdt'] = '';
			$data['vPhone'] = '';
			$data['vName'] = '';
		}

/*
		if(isset($result)){
			redirect('/pay/subscription');
		}
*/

		$this->response->View('store/paymentInfo', $data);
	}

	public function point() { 
		if( !$this->session->userdata('UID') ) $this->util->alert('로그인 후 이용가능합니다.', '/login');
		$data['isPoint'] = false;
		$data['pointCnt'] = $this->User->getUserPointCnt($this->session->userdata('UNO'));
		$userData = $this->User->getUserPoint($this->session->userdata('UNO'));
		# 오늘 출석포인트 받았는지 체크 
		$today = date('d'); 
		$month = date('Y-m');
		if( $userData['vDate'] == $month && $userData['vDay'] == $today ){
			$data['isPoint'] = true;
		}
		$this->response->View('store/point', $data);
	}

/*
	function addPaymentInfo() {

		# form check
		$this->load->library('form_validation');
		$this->form_validation->set_rules('bank', '은행', FV_BANK);
		$this->form_validation->set_rules('cardNo1', '카드번호', FV_CARD);
		$this->form_validation->set_rules('cardNo2', '카드번호', FV_CARD);
		$this->form_validation->set_rules('cardNo3', '카드번호', FV_CARD);
		$this->form_validation->set_rules('cardNo4', '카드번호', FV_CARD);
		$this->form_validation->set_rules('cardDate', '만료날자', FV_CARD);
		$this->form_validation->set_rules('birth', '생년월일', FV_BIRTH);
		
		# form error
		if ( $this->form_validation->run() == FALSE ) {             
			$this->util->alert('폼체크 아웃', '');
		} 

		$insertData = [
			'nUserNo' => $this->session->userdata('UNO'),
			'vBank' => $this->input->post('bank'),
			'vCardNo_01' => $this->input->post('cardNo1'),
			'vCardNo_02' => $this->input->post('cardNo2'),
			'vCardNo_03' => $this->input->post('cardNo3'),
			'vCardNo_04' => $this->input->post('cardNo4'),
			'vDate' => $this->input->post('cardDate'),
			'vBirth' => $this->input->post('birth'),
		];
		
		$result = $this->Payment->addPaymentInfo($insertData);

		if($result){
			$this->util->alert('정상적으로 등록되었습니다.', '/pay/subscription');
		}else{
			$this->util->alert('DB error', '/store');
		}
	}
	*/

	public function addPaymentInfo(){
		$data = $this->PurchaseModel->putPayInfo();
		header('Content-Type: application/json;charset=utf-8');
		print json_encode($data, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
	}


    function addPoint()
    {
        $point = $this->input->post('point');

        if(!$point){
            $this->response->Json('error', '일시적인 오류', '', '');
        }

        # user point 업데이트 
        $result = $this->User->setUserPoint($this->session->userdata('UNO'), $point);
        if(!$result){
            $this->response->Json('error', 'DB error', '', '');exit;
        }

        # userPoint insert
        $insertData = [
            'nUserNo' => $this->session->userdata('UNO'),
            'nPoint' => $point,
            'vDescription' => '출석 포인트받기 +'.$point.'P 획득',
            'vUrl' => '/store/point',
            'vDate' => date('Y-m'),
            'vDay' => date('d'),
        ];

        $result = $this->User->addUserPoint($insertData);

        if($result){
            $this->response->Json('success', '정상적으로 등록', '', '');exit;
        }else{
            $this->response->Json('error', 'DB error', '', '');exit;
        }

    }
}