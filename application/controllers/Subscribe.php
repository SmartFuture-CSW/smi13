<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
    * 3. Vip 구독 Controller
    * @author 임정원 / 2020-02-14
    * @since  Version 1.0.0
*/

class Subscribe extends CI_Controller
{

    # 생성자
    function __construct() 
    {
        parent::__construct();
		$this->load->model('SiteinfoModel');
		$this->load->model('PurchaseModel');
    }

    public function index()
    {
		$data = $this->PurchaseModel->getPayInfo(1);


		if($data['result'] == 'SUCCESS'){
			$startDate = strtotime($data['vStartDate']);
			$endDate = strtotime($data['vEndDate']);
			$current = time();
			if($startDate <= $current && $endDate >= $current){
				redirect('subscribe/paycomplete','location');
				exit;
			}
		}

		$data['write'] = $this->SiteinfoModel->getPrivacy();
        $this->response->View('subscribe/index',$data);
    }


	public function paycomplete() {
		$data = $this->PurchaseModel->getPayInfo();
		if($data){
			$goodsInfo = @$this->PurchaseModel->getBuyGoodsInfo($data['nGoodsNo']);
			$data['vGoodsKind'] = $goodsInfo['vGoodsKind'];
			$startDate = @strtotime($data['vStartDate']);
			$endDate = @strtotime($data['vEndDate']);
			$current = time();
			if($startDate <= $current && $endDate >= $current){
				$data['vip'] = 'Y';
			}
		}
		$this->response->View('subscribe/paycomplete', $data);
	}

	public function pointcomplete() {
		$this->response->View('subscribe/pointcomplete');
	}




	public function vipapp(){

		$mAgent = array("iPhone","iPod","Android","Blackberry","Opera Mini","Windows ce","Nokia","sony","iPad");
		$chkMobile = false;
		$chkiPhone = false;
		for($i=0; $i<sizeof($mAgent); $i++){
			if(stripos( $_SERVER['HTTP_USER_AGENT'], $mAgent[$i] )){
				$chkMobile = true;
				if($mAgent[$i]=='iPhone' || $mAgent[$i]=='iPod') $chkiPhone = true;
				break;
			}
		}
		if($chkMobile && !$chkiPhone){
			Header("Location:intent://open?t=0#Intent;scheme=nodajimsgapp;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.nodajivip.talk;end");
			exit;
		}
		else if($chkMobile && $chkiPhone){
			Header("Location:itms-apps://itunes.apple.com/app/id1521211001");
			exit;
		}
		else{
			header("Location:https://www.nodajistock.co.kr/");
			exit;
		}

	}

}