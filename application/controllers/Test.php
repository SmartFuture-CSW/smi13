<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
*/
class Test extends CI_Controller {


	// 기존앱 테이블
	const TG_PAY_TABLE = 'g5_write_pay_result';
	const TG_REFUND_TABLE = 'g5_write_pay_refund';
	const TG_PAY_LOG = 'g5_pay_log';

	// 기존앱 회원 테이블
	const TG_MEMBER_TABLE = 'msg_member_list';
	
	// 신규앱 테이블
	const DT_PAY_TABLE = 'ndg_Payment';
	const DT_REFUND_TABLE = 'ndg_Refund';
	const DT_USER_RABLE = 'ndg_User';
	const DT_LOG_TABLE = 'ndg_PaymentLog';

	// 생성자
	public function __construct() {
		parent::__construct();

	//	$this->load->databases();
		$this->load->model('PurchaseModel');
		$this->load->model('Point_model');
		$this->load->model('User');
		$this->load->library('Secret');
		$this->load->library('Sms');


	}


	protected function mg_member(){

		

		// 현재 입력된 결제 회원 데이터
		$CI =& get_instance();
		$CI->new_db = $this->load->database('new_db', TRUE);

		$this->new_db->from(self::DT_USER_RABLE);
		$dt_user = $this->new_db->get()->result_array();

		$dt_user_data = array();
		foreach($dt_user as $dt_user_row){
			$dt_user_data[$dt_user_row['vPhone']] = $dt_user_row;
		}

		$sContentType = 'Content-Type: application/json';
		$sExec = sprintf("curl -XGET %s -H '%s'  ", 'https://stockboss.yesbit.co.kr/member/csworld',  $sContentType);
		$json_data = shell_exec($sExec);
		$data = json_decode($json_data, true);

		$addUserData = [];
		foreach($data as $row){
			$vPhone = $this->secret->secretEncode($row['phone']);

			if(!isset($dt_user_data[$vPhone]['vName'])){
				$key = array_search($vPhone, array_column($addUserData, 'vPhone'));

				if(!$key){
	
					$vName = $row['nick'];
					$nick = $row['nick'];
					$device = $row['device'];
					$ip = $row['reg_ip'];


					$loginkey = $row['loginkey'];
					$pushkey = $row['pushkey'];


					$startdate = $row['startdate'];
					$enddate = $row['enddate'];
					$regdate = $row['reg_dt'];


					$addUserData[] = [
						'vName' => $vName,
						'vPhone' => $vPhone,
						'vNick' => $nick,
						'vIp' => $ip,
						'vLoginKey' => $loginkey,
						'vDevice' => $device,
						'vPushKey' => $pushkey,
						'dtStartDate' => $startdate,
						'dtEndDate' => $enddate,
						'dtRegDate' => $regdate,
						'nLevel'=>1
					];
				}
			}


		}

		// 새로 갱신된 결제회원이 있다면 타겟 테이블에 적재
		if(count($addUserData) > 0){
			$this->new_db->insert_batch(self::DT_USER_RABLE, $addUserData);
		}
	}


	function mg_pay(){


		print date("Y-m-d H:i:s")."\r\n";

		print "Member CHECK \r\n";

		$this->mg_member();


		/**
		* @detail:	노다지 기존앱 정기결제
		*			타겟 데이터 테이블에서 데이터를 수집한다. 
		*/
		$CI =& get_instance();
		// 기존앱 데이터 베이스 연결
		$CI->original_db = $this->load->database('original_db', TRUE);
		$this->original_db->from(self::TG_PAY_TABLE);
		// 정기결제 테이블 데이터
		$tg_data = $this->original_db->get()->result_array();

		// 환불 신청 테이블 데이터
		$this->original_db->from(self::TG_REFUND_TABLE);
		$tg_refund_data = $this->original_db->get()->result_array();

		// 결제 실패 로그 테이블 데이터
		$this->original_db->from(self::TG_PAY_LOG);
		$tg_log_data =  $this->original_db->get()->result_array();
		

		/**
		* @detail:	노다지 신규앱 정기결제 테이블
		*			타겟데이터를 기존 테이블과 비교하여 입력한다
		*/
		// 현재 입력된 결제 정보 데이터
		$CI->new_db = $this->load->database('new_db', TRUE);
		$this->new_db->from(self::DT_PAY_TABLE);
		$this->new_db->where('mg_key is not null');
		$this->new_db->where('nType', 1);
		$dt_data = $this->new_db->get()->result_array();

		$dt_pay_data = array();
		foreach($dt_data as $dt_pay_row){
			$dt_pay_data[$dt_pay_row['mg_key']] = $dt_pay_row;
		}

		// 현재 입력된 결제 회원 데이터
		$this->new_db->from(self::DT_USER_RABLE);
		$dt_user = $this->new_db->get()->result_array();

		$dt_user_data = array();
		foreach($dt_user as $dt_user_row){
			$dt_user_data[$dt_user_row['vPhone']] = $dt_user_row;
		}


		// 현재 입력된 환불 정보 데이터
		$this->new_db->from(self::DT_REFUND_TABLE);
		$dt_refund = $this->new_db->get()->result_array();

		$dt_refund_data = array();
		foreach($dt_refund as $dt_refund_row){
			$dt_refund_data[$dt_refund_row['mg_key']] = $dt_refund_row;
		}


		// 현재 입력된 로그 정보 데이터
		$this->new_db->from(self::DT_LOG_TABLE);
		$dt_log = $this->new_db->get()->result_array();



		$dt_log_data = array();
		foreach($dt_log as $dt_log_row){
			$dt_log_data[$dt_log_row['mg_key']] = $dt_log_row;
		}




		// 추가 입력 결제 회원 데이터
		$addUserData = [];

		// 추가 결제 정보 데이터
		$addPayData = [];

		// 추가 구독해지 정보 데이터
		$addRefundData = [];

		// 추가 결제실패 log 데이터
		$addLogData = [];


		foreach($tg_data as $tg_row){


			$mg_key			= $tg_row['wr_id'];

			// 결제 고정 정보
			$nType			= 1;								// 결제타입 1:정기구독 2: 테마종목 3: 기간종목 4: 찌라시종목
			$vPaycode		= 'nodaji';							// 결제코드(nodaji)
			$nMonth			= 1;

			$nGoodsNo		= ($tg_row['wr_good'] == "9900") ? 5 : 4;		// 상품코드 goods.nSeqNo 결제금액이 9900원이면 상품코드5 아니면 4
			$nGoodsName		= '정기구독';

			// 결제 수단 정보
			$vStartDate		= $tg_row['wr_1'];					// 정기구독 시작일
			$vEndDate		= $tg_row['wr_2'];					// 정기구독 종료일

			$dtStartDate	= $vStartDate." 00:00:00";
			$dtEndDate		= $vEndDate." 23:59:59";

			$vCardName		= $tg_row['wr_3'];					// 카드사
			if(!empty($tg_row['wr_4'])){
				$vCardNo		= $this->secret->secretDecode($tg_row['wr_4']);					// 카드번호
				$vCardNo		= substr($vCardNo,0,4).$this->secret->secretEncode(substr($vCardNo, 4, strlen($vCardNo) -4));
			}else{
				$vCardNo	= $tg_row['wr_4'];
			}
			$vExpdt			= $tg_row['wr_8'];					// 카드유효기간
			$vUid			= $tg_row['wr_9'];					// Uid
			$vDevice		= $tg_row['wr_5'];					// 디바이스 정보
			$tno			= $tg_row['wr_homepage'];			// tno
			$nPrice			= $tg_row['wr_good'];				// 상품 가격
			$nAmount		= $tg_row['wr_good'];				// 상품 가격
			$vOrderIdx		= $tg_row['wr_content'];			// order_idx

			$dtRegDate		= $tg_row['wr_datetime'];

			// 회원정보	
			$vName			= $tg_row['wr_name'];				// 이름
			$vPhone			= $tg_row['wr_twitter_user'];		// 휴대폰번호
			$vIp			= $tg_row['wr_ip'];					// ip


			$vPhone = $this->secret->secretEncode($vPhone);


			if(!isset($dt_user_data[$vPhone]['vName'])){
				$key = array_search($vPhone, array_column($addUserData, 'vPhone'));

				if(!$key){
					$addUserData[] = [
						'vName' => $vName,
						'vNick' => $vName,
						'vPhone' => $vPhone,
						'vIp' => $vIp,
						'nLevel'=>1
					];
				}
			}


			if(!isset($dt_pay_data[$mg_key]['nSeqNo'])){
				$addPayData[] = [
					'mg_key' => $mg_key
					, 'nGoodsNo' => $nGoodsNo
					, 'vOrderIdx' => $vOrderIdx
					, 'vGoodName' => $nGoodsName
					, 'vName' => $vName
					, 'nType' => $nType
					, 'nPrice' => $nPrice
					, 'nAmount' => $nAmount
					, 'dtStartDate' => $dtStartDate
					, 'dtEndDate' => $dtEndDate
					, 'dtRegDate' => $dtRegDate
					, 'tno' => $tno
					, 'vIp' => $vIp
					, 'vPaycode' => $vPaycode
					, 'vPhone' => $vPhone
					, 'vStartDate' => $vStartDate
					, 'vEndDate' => $vEndDate
					, 'vCardName' => $vCardName
					, 'vCardNo' => $vCardNo
					, 'vDevice' => $vDevice
					, 'vExpdt' => $vExpdt
					, 'vUid' => $vUid
				];
			}
		}


		foreach($tg_refund_data as $tg_refund_row){

			$mg_key				= $tg_refund_row['wr_id'];
			$mg_parent_key		= $tg_refund_row['wr_1'];
			$vName				= $tg_refund_row['wr_name'];
			$vPhone				= $tg_refund_row['wr_twitter_user'];
			$nRefund			= $tg_refund_row['wr_2'];
			$nState				= $tg_refund_row['wr_good'];
			$dtRegDate			= $tg_refund_row['wr_datetime'];
			$vUid				= $tg_refund_row['wr_9'];


			$vPhone = $this->secret->secretEncode($vPhone);

			if(!isset($dt_refund_data[$mg_key]['nSeqNo'])){
				$addRefundData[] = [
					'mg_key' => $mg_key
					, 'mg_parent_key' => $mg_parent_key
					, 'vName' => $vName
					, 'vPhone' => $vPhone
					, 'nRefund' => $nRefund
					, 'nState' => $nState
					, 'dtRegDate' => $dtRegDate
					, 'vUid' => $vUid
				];
			}
		}


		foreach($tg_log_data as $tg_log_row){

			$res = $tg_log_row['pl_cont'];
			// 175|X|228880|신카드사용바람, (1588-4500)
			$arr = explode('|', $res);

			$mg_key = $arr[0];

			$nPayNo = 0;
			$nUserNo = 0;
			$vName = 0;
			$vPhone = 0;

			$nType = 1;
			$dtRegDate = $tg_log_row['pl_datetime'];
			$vPaycode = 'nodaji';
			$vReason = $arr[3];
			$vUid = $arr[2];
			$nState = $tg_log_row['pl_stat'];



			if(!isset($dt_log_data[$mg_key]['nSeqNo'])){
				$addLogData[] = [
					'nPayNo' => $nPayNo
					, 'nUserNo' => $nUserNo
					, 'vName' => $vName
					, 'nType' => $nType
					, 'dtRegDate' => $dtRegDate
					, 'vPaycode' => $vPaycode
					, 'vPhone' => $this->secret->secretEncode($vPhone)
					, 'vUid' => $vUid
					, 'vReason' => $vReason
					, 'nState' => $nState
					, 'mg_key' => $mg_key
				];
			}
		}



		// 새로 갱신된 결제회원이 있다면 타겟 테이블에 적재
		/*
		if(count($addUserData) > 0){
			$this->new_db->insert_batch(self::DT_USER_RABLE, $addUserData);
		}
		// 새로 갱신된 결제내역이 있다면 타겟 테이블에 적재
		if(count($addPayData) > 0){
			$this->new_db->insert_batch(self::DT_PAY_TABLE, $addPayData);
			// pay_result에 userno 갱신
			$this->new_db->query(' UPDATE '.self::DT_PAY_TABLE.' AS A SET nUserNo = (SELECT MAX(nSeqNo) FROM '.self::DT_USER_RABLE.' WHERE vPhone = A.vPhone GROUP BY vPhone)');
		}
		*/
/*
		if(count($addRefundData) > 0){
			$this->new_db->insert_batch(self::DT_REFUND_TABLE, $addRefundData);
			// refund에 pay_no 갱신
			$this->new_db->query(' UPDATE '.self::DT_REFUND_TABLE.' AS A SET nPayNo = ( SELECT nSeqNo FROM '.self::DT_PAY_TABLE.' WHERE mg_key = A.mg_parent_key)');
			// refund user_no 갱신
			$this->new_db->query(' UPDATE '.self::DT_REFUND_TABLE.' AS A SET nUserNo = ( SELECT nSeqNo FROM '.self::DT_USER_RABLE.' WHERE vPhone = A.vPhone)');
		}
*/

		// 새로 갱신된 결제 실패 내역이 있다면 타겟 테이블에 적재
		/*
		if(count($addLogData) > 0){
			$this->new_db->insert_batch(self::DT_LOG_TABLE, $addLogData);
			// 새로생신된 셜제 실패내역의 없는값 갱신
			$this->new_db->query(' UPDATE '.self::DT_LOG_TABLE.'  AS A LEFT JOIN '.self::DT_PAY_TABLE.' AS B ON A.mg_key = B.mg_key SET A.nPayNo =  B.nSeqNo , A.vName = B.vName , A.nUserNo = B.nUserNo , A.vPhone = B.vPhone');
		}

*/

		print "migration Data >>>\r\n";
		print " >> User Data : ". count($addUserData). "\r\n";
		/*
		print " >> Pay Data : ". count($addPayData). "\r\n";
		print " >> Refund Data : ". count($addRefundData). "\r\n";
		print " >> Log Data : ". count($addLogData). "\r\n";
		*/
		print "complete\r\n\r\n";
		

	}


	public function s1(){

//{"para":"api_key=%2A8e9fc600d15d8bd44c4326d46b66a5bb&mode=card&mb_id=smartfuture2&amount=15000&interesttype=1&expdt=2108&installment=00&goodname=1%EA%B0%9C%EC%9B%94+%EC%A0%95%EA%B8%B0%EA%B5%AC%EB%8F%85&ordername=%EC%8B%A0%ED%99%94%EA%B7%A0&phoneno=01090222850&cardno=9420114507724310","vPhone":"01090222850","response":{"orderstatus":"O","uid":"250769"}}


//{"para":"api_key=%2A8e9fc600d15d8bd44c4326d46b66a5bb&mode=card&mb_id=smartfuture2&amount=15000&interesttype=1&expdt=2108&installment=00&goodname=1%EA%B0%9C%EC%9B%94+%EC%A0%95%EA%B8%B0%EA%B5%AC%EB%8F%85&ordername=%EC%8B%A0%ED%99%94%EA%B7%A0&phoneno=01090222850&cardno=114507724310","vPhone":"01090222850","response":{"orderstatus":"O","uid":"250769"}}

//
//{"para":"api_key=%2A8e9fc600d15d8bd44c4326d46b66a5bb&mode=card&mb_id=smartfuture2&amount=15000&interesttype=1&expdt=2601&installment=00&goodname=1%EA%B0%9C%EC%9B%94+%EC%A0%95%EA%B8%B0%EA%B5%AC%EB%8F%85&ordername=%EC%B5%9C%EA%B2%BD%ED%98%B8&phoneno=01023958601&cardno=5272890142350168","vPhone":"01023958601","response":{"orderstatus":"O","uid":"250744"}}


		$nAmount = 15000;
		$expdt = 2108;
		$goodsNo = 5;




	}


	public function go(){
		exit;

		$vPhone = '01028079356';
		$period = 1;
		$vStartDate = date("2020-12-02");
		$vEndDate = date("2021-01-02",strtotime("+".$period." month", time()));

		$this->secret->call_ndg_vip($vPhone, $vStartDate, $vEndDate, 15);
	}


	public function csw2($de=null){
		$de = "5cTYQ5MzFRlsL5P6q6Lfuw==";
		$vPhone = $this->secret->secretDecode($de);
		echo $vPhone;
		exit;
	}
	public function csw($en=null){


//4518RKDGi/0kKjkZReqE9/2XZw==
//wjdOMsB0gEYnLQQNvUDCww==
		$en = '01050595977';
		$vPhone = $this->secret->secretEncode($en);
		echo $vPhone;
		exit;
	}


	public function point(){


		$param = [
			'nUserNo' => 3102
		];

		$data = $this->Point_model->getUserPointLog($param);

		echo '<pre>';
		print_r($data);
		exit;
	}



	public function setpoint(){
	// @param: nUserNo, nFreePoint, nPayPoint, nPointKind, nPointType, nType, nBuyNo, vDescription
		$param = [
			'nUserNo' => 3102
			, 'changePoint' => 700000
			, 'changeType' => 'dud'
			, 'nType' => 2
			, 'nBuyNo' => '999999' // 테스트
			, 'msg' => '테스트 차감처리'
			, 'nPointType' => null
			, 'nFreePoint' => null
			, 'nPayPoint' => null
		];
		$data = $this->Point_model->setChangePoint($param);
		echo '<pre>';
		print_r($data);
		exit;
	}

	public function setpointadd(){
	// @param: nUserNo, nFreePoint, nPayPoint, nPointKind, nPointType, nType, nBuyNo, vDescription
		$param = [
			'nUserNo' => 9999
			, 'changePoint' => 1500000
			, 'changeType' => 'add'
			, 'nType' => 2
			, 'nBuyNo' => '999999' // 테스트
			, 'msg' => '테스트 회원가입 적립처리'
			, 'nPointType' => 3
			, 'nFreePoint' => 50000
		];
		$data = $this->Point_model->setChangePoint($param);
		echo '<pre>';
		print_r($data);
		exit;
	}


	// 기존 포인트 유료 포인트 전환
	public function mg_point(){

		$data = $this->db->select('nSeqNo,nPoint')
			->from('ndg_User')->get()->result_array();

		$i = 0;
		$max = 500;
		$inputSet = [];

		foreach($data as $row){

			$nUserNo = $row['nSeqNo'];
			$nPayPoint = $row['nPoint'];
			$nFreePoint = 0;

			$inputSet[] = [
				'nUserNo' => $nUserNo
				, 'nPayPoint' => $nPayPoint
				, 'nFreePoint' => $nFreePoint
			];

			$logSet[] = [
				'nUserNo' => $nUserNo
				, 'nPointKind' => 1
				, 'nPointType' => 1
				, 'nType' => null
				, 'nBuyNo' => null
				, 'nPoint' => $nPayPoint
				, 'vDescription' => '정책 변경으로 보유 포인트 유료 포인트로 통합 전환'
			];

			$i++;


			if($i >= $max){
				$i = 0;
				$this->db->insert_batch(TABLE_USER_POINT, $inputSet);
				$this->db->insert_batch(TABLE_USER_POINT_LOG, $logSet);

				$inputSet = null;
				$logSet = null;
			}

		}


		$this->db->insert_batch(TABLE_USER_POINT, $inputSet);
		$this->db->insert_batch(TABLE_USER_POINT_LOG, $logSet);


	}


}