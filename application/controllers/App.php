<?php
defined('BASEPATH') or exit('No direct script access allowed');

class App extends CI_Controller {


	protected $securus_key = 'aqwemfkrmbkfsdfkljglq14932mfkrqw';

	// 생성자
	public function __construct() {
		parent::__construct();
		$this->load->database();


		$this->load->library('Secret');
		$this->load->model('App_model');
		$this->load->model('StockModel');
		$this->load->model('PurchaseModel');
	}

	public function index() {
		if( $this->session->userdata('UID') ) $this->util->alert('', '/main');
		$this->load->view('main/intro');            // 인트로 페이지
	}

	// 목표일이 지난 추천종목 상품 상태값 변경
	public function clear_stock(){
		$data = $this->StockModel->clearStock();
		echo $data;
	}


	// 디바이스 정보 php 세션 저장
	public function setDeviceInfo(){
		$sessionData = array(
			"deviceKey" => $_POST['deviceKey'],
			"platform" => $_POST['deviceType']
		);
		$this->session->set_userdata($sessionData);
	}


	// 정기결제
	public function cron_pay_proc(){

		// 크론 정보
		$cron = [ 
			'where' => array( 'nSeqNo' => 1 ),
			'set' => array('nCronStart' => 1 )
		];
		// Cron 실행 체크 모니터
		$this->App_model->cronMoniter($cron);
		// 오늘 결제 갱신할 회원 결제 목록
		$pRows = $this->PurchaseModel->getTodayPay();
		foreach($pRows as $row){
			$data = $this->PurchaseModel->autoPayment($row);
		}
		$cronResult = " REUSLT : : [".$data['result']."]"
			. ", error [".$data['error']."]"
			. ", msg [".$data['msg']."]"
			. ", uid [".$data['uid']."]"
			. ", cnt_success [".$data['cnt_success']."]"
			. ", cnt_fail [".$data['cnt_fail']."]";
		// 크론 정보
		$cron = [ 
			'where' => array( 'nSeqNo' => 1 ),
			'set' => array(
				'nCronEnd' => 1,
				'vCronResult' => $cronResult
			)
		];
		// Cron 실행 체크 모니터
		$this->App_model->cronMoniter($cron);
		print $cronResult;
	}


	// 정기결제 단일/결제실패내역 처리용 파라메터 Payment 테이블의 nSeqNo
	public function cron_one_pay($nPayNo){
		exit;

		// 크론 정보
		$cron = [ 
			'where' => array( 'nSeqNo' => 1 ),
			'set' => array('nCronStart' => 1 )
		];
		// Cron 실행 체크 모니터
		$this->App_model->cronMoniter($cron);
		// 오늘 결제 갱신할 회원 결제 목록
		//$pRows = $this->PurchaseModel->getTodayPay();
		$pRows = $this->PurchaseModel->getTodayPayETC($nPayNo);

		foreach($pRows as $row){
			$data = $this->PurchaseModel->autoPayment($row);
		}
		$cronResult = " REUSLT : : [".$data['result']."]"
			. ", error [".$data['error']."]"
			. ", msg [".$data['msg']."]"
			. ", uid [".$data['uid']."]"
			. ", cnt_success [".$data['cnt_success']."]"
			. ", cnt_fail [".$data['cnt_fail']."]";
		// 크론 정보
		$cron = [ 
			'where' => array( 'nSeqNo' => 1 ),
			'set' => array(
				'nCronEnd' => 1,
				'vCronResult' => $cronResult
			)
		];
		// Cron 실행 체크 모니터
		$this->App_model->cronMoniter($cron);
		print $cronResult;
	}


	// referer 정보 DB 입력
	public function postReferer(){
		if(!empty($_COOKIE['VNO'])){
			return true;
		}
		$aReferer = json_decode($this->input->post('referer'), true);
		$param = [
			"vReferer" => $aReferer['referrerUrl']
			, "nInstall" => 1
			, "fullReferer" => $this->input->post('referer')
			, 'vIp' => $_SERVER['REMOTE_ADDR']
			, 'vPhone' => $this->secret->secretEncode($this->input->post('vPhone'))
		];
		$data = $this->App_model->postReferer($param);
		$this->_visitSession($data);
		return true;
	}

	// referer 정보 DB 입력
	public function postRefererPc(){
		if(!empty($_COOKIE['VNO'])){
			return true;
		}

		//$aReferer = json_decode($this->input->post('referer'), true);
		$param = [
			"vReferer" => $this->input->post('adref')
			, "nInstall" => 0
			, "fullReferer" => $this->input->post('fullReferer')
			, 'vIp' => $_SERVER['REMOTE_ADDR']
		];
		$data = $this->App_model->postReferer($param);
		$this->_visitSession($data);
		return true;
	}



	// referer 관련 정보 세션 및 쿠키 입력
	public function _visitSession($data) {
		// 처리 데이터
		$sessionData = [
			'VNO'  => $data['nVisitNo']
		];
		// 쿠키 셋
		set_cookie('VNO', $data['nVisitNo'], 86400*30, '.nodajistock.co.kr', '/', '');
		// 세션 셋
		$this->session->set_userdata($sessionData);
		return true;
	}



}



