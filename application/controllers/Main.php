<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Main Controller
 *
 * @author 임정원 / 2020-02-14
 *
 * @since  Version 1.0.0
 */
class Main extends CI_Controller
{
    // 생성자
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User');
		$this->load->model('App_model');
		$this->load->model('Point_model');
        $this->load->model('SmsModel');
		$this->load->model('Board');
        $this->load->library('Secret');
        $this->load->library('Sms');
		$this->load->model('Site');
		$this->load->model('PurchaseModel');

		if(!stristr($_SERVER['HTTP_HOST'],"www.")) header("location: http://www.nodajistock.co.kr/".$REQUEST_URI);



		// login cookie 세션 동기화
		if(!empty(get_cookie('UNO')) && !$this->session->userdata('UNO')) {
			$sData = [
				'UNO'  => get_cookie('UNO'),
				'UID'  => get_cookie('UID'),
				'UNK'  => get_cookie('UNK'),
				'UNM'  => get_cookie('UNM')
			];		
			$this->session->set_userdata($sData);
		}
		else{
			if(isset($_GET['app'])) {
				if($_GET['app'] == 1) {
					redirect('/auth?first=Y');
				}
			}
		}
    }
	public function pushTest()
	{
		//테스트 기기 토큰
		//f4jlPkoExig:APA91bGj4-sFU7wnMUjyTkDwmRJVbdOXdWuiTso4zeriMFoJI2hGpCQttx2g9dG25pq1ODrPdisk8pj_KYtDPDtxCwBWST8w8d3FvogYP2RhU0u7-70xtVGItCq02eckiWzEVTh9GkL9
		$tokens = array();
	
		/*
		Db에서 검색하여 토큰 배열로 전달
		$table = '회원테이블';
		$where = array(); //조건
		$query = $this->db->get_where($table, $where);
		$result = $query->result();

		foreach($result as $val){
			$tokens[] = 'f4jlPkoExig:APA91bGj4-sFU7wnMUjyTkDwmRJVbdOXdWuiTso4zeriMFoJI2hGpCQttx2g9dG25pq1ODrPdisk8pj_KYtDPDtxCwBWST8w8d3FvogYP2RhU0u7-70xtVGItCq02eckiWzEVTh9GkL9';	
		}
		*/

		$tokens[] = 'czmCKRiFtz4:APA91bFL-060ljPXBxoUzCEjQ7_mzrrR8l8QAwXx9pcjNw8gSW0FY3Ktx3rZr-GhR3_duX-vRO5wbVohnHg8m2tpuZwDFsUydYZSxwiXFzh7oVnr1JfvFtz--pmUzNgLVO5GvREN_M--';

		$title = '테스트 제목';
		$content = '테스트 내용';
		$messageData = array("message" => $content);
		$fields = array(
			'registration_ids' => $tokens,
			'data' => $messageData,
			'notification' => array('title' => $title, 'body' => $content, 'click_action' => "FCM_PLUGIN_ACTIVITY", 'icon' => 'ic_action_push', 'sound' => 'default',"content_available" => true)
		);	
		$headers = array(
			'Authorization:key = AAAAXShP6hU:APA91bGnxgIc2E3qxmF4IsxemeB1I1qOD83zrcHfIbMGZbjn7j2vdhMTx-A1Xj1ZoQxgocqYRRINM1m9dcIwaTrTXGWbBhDuBVFvf6oHJ5YJCwR_bhtkSsBPN4suXuB-JpuNSlwxvMdJ',
			'Content-Type: application/json'
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);			
		curl_close($ch);			
		print_r($result);

	}


	public function index(){
		/**
		* @breif: 해당 컨트롤러 수정시 main/index 부분도 동일하게 수정해야 함
		* @author: csw
		*/
		$data['page'] = 'list';

		$data['today']     = $this->Board->getBoardToday();
		$data['category'] = $this->uri->segment(3) ? urldecode($this->uri->segment(3)) : 'date';

		switch($data['category']){
			case 'date' : $data['order'] = '최신순'; break;
			case 'hit' : $data['order'] = '조회수순'; break;
			case 'share' : $data['order'] = '공유순'; break;
			case 'reply' : $data['order'] = '댓글순'; break;
			default  : $data['order'] = '최신순'; break;
		}

		$data['recommend'] = $this->Board->getBoardMain(['vMainVal' => 2]);
		$data['invest'] = $this->Board->getBoardMain(['vMainVal' => 9]);
		$data['study'] = $this->Board->getBoardMain(['vMainVal' => 10]);
//		$data['community'] = $this->Board->getBoard_User(['b.vType' => 'community']);
		$data['community'] = $this->Board->getBoardCommunity(['vType' => 'community'], 5);
		$data['etc']       = $this->Board->getBoardAny();
		$data['banner']    = $this->Board->getBanner(['emKind' => 'band']);
		// 팝업 세팅 
		$popup = $this->_popupCheck('app_start');
		if($popup) $data['popup'] = $popup;
		$this->response->View('info/list_today', $data);
	}

    public function main()
    {
        $this->util->isLogin(false);
        redirect('/info/index');                   // 메인 페이지 뷰호출 -> 공개정보 대카테고리
    }

    public function login()
    {
        $this->util->isLogin(true);
        $this->response->View('main/login');        // 로그인 페이지
//exit;
    }

    public function logout()
    {
        session_destroy();

		delete_cookie('UID');
		delete_cookie('UNK');
		delete_cookie('UNM');
		delete_cookie('UNO');

		set_cookie('UID', '', time() - 3600 , '.nodajistock.co.kr', '/', '');
		set_cookie('UNK', '', time() - 3600 , '.nodajistock.co.kr', '/', '');
		set_cookie('UNM', '', time() - 3600 , '.nodajistock.co.kr', '/', '');
		set_cookie('UNO', '', time() - 3600 , '.nodajistock.co.kr', '/', '');

        $this->util->alert('', '/info/today');                    // 로그아웃 처리
    }

    public function join()
    {
        if( $this->session->userdata('UID') ) $this->util->alert('', '/main');
        $this->util->isLogin(true);
        $this->response->View('main/join');         // 회원가입 페이지
    }

    public function joinComplete()
    {
        $this->util->isLogin(false);
        $this->response->View('main/joinComplete'); // 회원가입 완료 페이지
    }

    public function joinNick()
    {
        $this->util->isLogin(false);
        $this->response->View('main/joinNick');    // 회원가입 닉 등록 페이지
    }

    public function loginOrigin()
    {
        $this->util->isLogin(true);
        $this->response->View('main/loginOrigin');  // 기존 회원 로그인 페이지
    }

    public function fdPasswd()
    {
        $this->util->isLogin(true);
        $this->response->View('main/findPassword'); // 비밀번호 변경 페이지
    }

	// 로그인 처리 프로세스 ajax 처리 -> retrurn json
	public function loginCheck()
	{
		// form check
		$this->load->library('form_validation');
		$this->form_validation->set_rules('phone', '아이디', FV_PHONE);
		//$this->form_validation->set_rules('passwd', '비밀번호', FV_PWD);

		// form error
		if ($this->form_validation->run() == false) {
			$this->response->Json('error', ERROR_06);
			return false;
		}

		$phone  = $this->input->post('phone');
//        $passwd = $this->input->post('passwd');

		// 이상준대리 요청 
		$isPhone = $this->User->getUser(['vPhone =' => $phone], 'vPhone');
		if(empty($isPhone)){
			$this->response->Json('error', '등록된 회원이 아닙니다.');
			return false;
		}

		// DB 조회
		$select = 'nSeqNo, vPhone, vName, vNick, vDevice, nLevel, vPushKey';
		$where  = ['vPhone =' => $phone];
		$result = $this->User->getUser($where, $select);

		if (! empty($result)) {
			// push device key refresh
			/*
			$set   = ['vPushKey' => $this->session->userdata('deviceKey')];
			$where = ['vPhone =' => $result['vPhone']];
			$data  = $this->User->setUser($set, $where);
			*/
//			$this->User->setUser();

			// session 처리
			$this->_userSession($result);

			// 접속 로그 처리
			$setData = [
				'nUserNo'    => $result['nSeqNo'],
				'vUserIp'    => $_SERVER['REMOTE_ADDR'],
				'vUserAgent' => $_SERVER['HTTP_USER_AGENT'],
			];
			$this->User->addUserLogin($setData);

			// return
			$this->response->Json('success', '', '/main');
			return false;
		} else {
			$this->response->Json('error', '비밀번호를 다시 확인해주세요.');
			return false;
		}
	}

	// 회원가입 전화번호 인증 버튼 Ajax 처리 -> retrun json
	public function phoneCheck()
	{
		$mode  = $this->input->post('mode');
		$phone = $this->input->post('phone');

		// 조회 / 휴대폰번호가 있는지 확인
		$select = 'nSeqNo, vPhone, vPwd';
		$where  = ['vPhone =' => $phone];
		$data   = $this->User->getUser($where, $select);

/*
		if ($mode == 'join') {
			// 회원 가입 폰체크
			if (isset($data)) {
				$this->response->Json('error', ERROR_09);
				return false;
			}
		} elseif ($mode == 'origin') {
			// 기존 회원 로그인
			if (! isset($data)) {
				$this->response->Json('error', ERROR_03);
				return false;
			}
		} elseif ($mode == 'find') {
			// 비밀번호 찾기
			if (!isset($data)) {
				$this->response->Json('error', ERROR_03);
				return false;
			}
		} elseif ($mode == 'login'){
			if (!isset($data)) {
				$this->response->Json('error', ERROR_03);
				return false;
			}
		} else {
			$this->response->Json('error', ERROR_03);
			return false;
		}
*/
		if ($this->session->userdata('auth_code')) {
			// 3분간 인증코드 유효
			$this->response->Json('error', ERROR_10);
			return false;
		}

		// 코드생성
		$auth_code = rand(1000, 9999);
		// 휴대폰 인증후 4자리 코드전송 -> sms 발송처리
		$this->sms->authCodeSend($phone, $auth_code);
		// 로그테이블 저장 


		$insertData = [
			'vType' => 'cert',
			'vSubject' => '회원가입인',
			'txContent' => "[노다지모바일인증] 인증코드는 [".$auth_code."] 입니다.",
			'vSendNum' => '18995445',
			'vReceiveNum' => $phone,
			'vIp' => $_SERVER['REMOTE_ADDR'],
		];


		if($mode == "login"){
			$insertData['vSubject'] = '회원로그인인증';
		}
		
		$this->SmsModel->setSmsLog($insertData);
		// 임시데이터 3분안에 삭제
		$this->session->set_tempdata('auth_code', $auth_code, 180);
		// return 알럿으로 일단 인증번호 표시
		$this->response->Json('success', ERROR_08);
	}

	// 회원가입, 비밀번호 변경페이지에서 sms 문자로발송된 코드 확인 처리 Ajax -> return json
	public function codeCheck()
	{
		// form check
		$this->load->library('form_validation');

		$this->form_validation->set_rules('code', '인증번호', FV_CODE);

		if ($this->form_validation->run() == false) {
			$this->response->Json('error', ERROR_06);
			return false;
		}

		$code      = $this->input->post('code');
		$auth_code = $this->session->userdata('auth_code');

		if ($code == $auth_code) {
			$this->response->Json('success', '인증되었습니다.');
			return false;
		} else {
			$this->response->Json('error', ERROR_07);
			return false;
		}
	}

    // 회원가입 insert 프로세스 Ajax -> return json
    public function doJoin()
    {
        $code      = $this->input->post('code');
        $auth_code = $this->session->userdata('auth_code');

        // form check
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', '이름', FV_UNAME);
        $this->form_validation->set_rules('phone', '아이디', FV_PHONE);
        //$this->form_validation->set_rules('passwd', '비밀번호', FV_PWD);
        $this->form_validation->set_rules('code', '인증번호', FV_CODE);

        if ($this->form_validation->run() == false) {
            $this->response->Json('error', ERROR_06);
            return false;
        }

        // 문자코드와 사용자 작성 코드 비교
        if ($code != $auth_code) {
            $this->response->Json('error', ERROR_07);
            return false;
        }

        $set = [
            'vPhone'  => $this->input->post('phone'),
            'vPwd'    => $this->secret->aes_encrypt($this->input->post('passwd')),
            'vName'   => $this->input->post('username'),
            'vIp'     => $_SERVER['REMOTE_ADDR'],
            'vDevice' => $this->input->post('device'),
			'vPushKey' => $this->session->userdata('deviceKey')
        ];

        $data = $this->User->addUser($set);

        if (isset($data)) {
            // session 처리
            $this->_userSession($data);
            // return
            $this->response->Json('success', '', '/joinComplete');
        } else {
            $this->util->alert(ERROR_06, '/join');
        }
    }

    // 비밀번호 변경 프로세스 ajax -> return json
    public function modifyPasswd()
    {
        $code      = $this->input->post('code');
        $auth_code = $this->session->userdata('auth_code');

        // 문자코드와 사용자 작성 코드 비교
        if ($code != $auth_code) {
            $this->response->Json('error', ERROR_07);
            return false;
        }

        // db 처리
        $set   = ['vPwd' => $this->secret->aes_encrypt($this->input->post('passwd'))];
        $where = ['vPhone =' => $this->input->post('phone')];
        $data  = $this->User->setUser($set, $where);

        if (isset($data)) {
            $this->response->Json('success', '비밀번호 변경 완료', '/login');
            return false;
        } else {
            $this->response->Json('error', ERROR_07);
            return false;
        }
    }

    // 기존 회원 비밀번호 변경 후 로그인 ajax -> return json
    public function orginModifyPasswd()
    {
        $code      = $this->input->post('code');
        $auth_code = $this->session->userdata('auth_code');

        // 문자코드와 사용자 작성 코드 비교
        if ($code != $auth_code) {
            $this->response->Json('error', ERROR_07);
            return false;
        }

        // db 처리
        $set   = ['vPwd' => $this->secret->aes_encrypt($this->input->post('passwd'))];
        $where = ['vPhone =' => $this->input->post('phone')];
        $data  = $this->User->setUser($set, $where);

        if (! isset($data)) {
            $this->response->Json('error', ERROR_07);
            return false;
        }

        // DB 조회 로그인 session 처리
        $select = 'nSeqNo, vPhone, vName, vNick, vDevice, nLevel';
        $where  = ['vPhone =' => $this->input->post('phone')];
        $result = $this->User->getUser($where, $select);

        if (! empty($result)) {
            // session 처리
            $this->_userSession($result);

            // 접속 로그 처리
            $setData = [
                'nUserNo'    => $result['nSeqNo'],
                'vUserIp'    => $_SERVER['REMOTE_ADDR'],
                'vUserAgent' => $_SERVER['HTTP_USER_AGENT'],
            ];
            $this->User->addUserLogin($setData);

            // return
            $this->response->Json('success', '', '/main');
            return false;
        } else {
            $this->response->Json('error', ERROR_03);
            return false;
        }
    }

    // 닉네임 업데이트 프로세스 Ajax
    public function modifyNick()
    {
        // form check
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nick', '닉네임', FV_UNAME);

        if ($this->form_validation->run() == false) {
            $this->response->Json('error', ERROR_06);
            return false;
        }

        // 중복된 닉 검사
        $where  = ['vNick =' => $this->input->post('nick')];
        $isNick = $this->User->getUser($where, 'vNick');

        if (isset($isNick)) {
            $this->response->Json('error', '이미 사용 중인 닉네임 입니다. 다시설정해 주세요.', '/main');
            return false;
        }

        // db 처리
        $set   = ['vNick' => $this->input->post('nick')];
        $where = ['nSeqNo =' => $_SESSION['UNO']];
        $data  = $this->User->setUser($set, $where);

        if (isset($data)) {
            $this->session->set_userdata('UNK', $this->input->post('nick'));
            $this->response->Json('success', '닉네임이 수정되었습니다.', '/main');
            return false;
        } else {
            $this->response->Json('error', ERROR_11);
            return false;
        }
    }

	// session 생성
	public function _userSession($data) {
		// session 처리
		$sessionData = [
			'UNO'  => $data['nSeqNo'],
			'UID'  => $data['vPhone'],
			'UNK'  => $data['vNick'],
			'UNM'  => $data['vName'],
		];
		set_cookie('UNO', $data['nSeqNo'], 86400*30, '.nodajistock.co.kr', '/', '');
		set_cookie('UID', $data['vPhone'], 86400*30, '.nodajistock.co.kr', '/', '');
		set_cookie('UNK', $data['vNick'], 86400*30, '.nodajistock.co.kr', '/', '');
		set_cookie('UNM', $data['vName'], 86400*30, '.nodajistock.co.kr', '/', '');
		$this->session->set_userdata($sessionData);
	}



	public function loginjoin(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('phone', '아이디', FV_PHONE);
		$this->form_validation->set_rules('username', '이름', FV_UNAME);
		$this->form_validation->set_rules('code', '인증번호', FV_CODE);

		// form error
		if ($this->form_validation->run() == false && $this->input->post('code')!='ymkt_2') {//210106 와이마케팅 제외
			$this->response->Json('error', ERROR_06);
			return false;
		}

		$code      = $this->input->post('code');
		$auth_code = $this->session->userdata('auth_code');
		$phone  = $this->input->post('phone');

		if($phone=="xKKfxZS9GHPdzNWtcOCUuQ==" || $phone=="01063506416"){//201223 송연미 처리
			$this->response->Json('error', ERROR_14);
			return false;
		}
		// 문자코드와 사용자 작성 코드 비교
		if ($code != $auth_code && $code!='ymkt_2') {//210106 와이마케팅 제외
			$this->response->Json('error', ERROR_07);
			return false;
		}

		// DB 조회
		$select = 'nSeqNo, vPhone, vName, vNick, vDevice, nLevel, vPushKey';
		$where  = ['vPhone =' => $this->secret->secretEncode($phone)];
		$result = $this->User->getUser($where, $select);

		
		if (! empty($result)) { // 회원 정보가 있다면 로그인 진행

			// push device key refresh
			$set   = ['vPushKey' => $this->session->userdata('deviceKey')];
			$where = ['nSeqNo =' => $result['nSeqNo']];
			$data  = $this->User->setUser($set, $where);
			//$this->User->setUser();

			// session 처리
			$this->_userSession($result);

			// 접속 로그 처리
			$setData = [
				'nUserNo'    => $result['nSeqNo'],
				'vUserIp'    => $_SERVER['REMOTE_ADDR'],
				'vUserAgent' => $_SERVER['HTTP_USER_AGENT'],
			];
			$this->User->addUserLogin($setData);


			// referer 회원가입 체크
			$param = [
				'nUserNo' => $result['nSeqNo']
				, 'vPhone' => $result['vPhone']
				, 'nJoin' => 1
			];
			$this->App_model->setReferer($param);

			$rtn = array(
				"result" => "success",
				"msg" => "login",
				"data" => $result
			);
			header("Content-Type: application/json;charset=utf-8");
			echo json_encode($rtn);
			exit;
		}
		else { // 회원 정보가 없다면 회원가입 진행

			$set = [
				'vPhone'  => $this->secret->secretEncode($this->input->post('phone')),
				'vPwd'    => $this->secret->secretEncode($this->input->post('passwd')),
				'vName'   => $this->input->post('username'),
				'vIp'     => $_SERVER['REMOTE_ADDR'],
				'vDevice' => $this->input->post('device'),
				'vPushKey' => $this->session->userdata('deviceKey'),
				'nPoint' => 0
			];

			$data = $this->User->addUser($set);

			if (isset($data)) {
				// session 처리
				$this->_userSession($data);

				if($this->session->userdata('rdscpa_1')=='3F96-0721-37V4-82P3-508MZ801HID4' && $this->session->userdata('rdscpa_3')!='' && ($this->session->userdata('rdscpa_2')=='CPA' || $this->session->userdata('rdscpa_2')=='BM')){//리더스cpa 201201
					$freferer='https://www.nodajistock.co.kr/auth?key_code='.$this->session->userdata('rdscpa_1').'&siteType='.$this->session->userdata('rdscpa_2').'&ridx='.$this->session->userdata('rdscpa_3').'&ref_url='.$this->session->userdata('rdscpa_4').'&ref_keyword='.$this->session->userdata('rdscpa_5');
					$param = [
						'vReferer' => 'readersCPA_'.$this->session->userdata('rdscpa_2')
						, 'nJoin' => 1
						, 'nUserNo' => $data['nSeqNo']
						, 'vPhone' => $data['vPhone']
						, 'fullReferer' => $freferer
						, 'vIP' => $_SERVER['REMOTE_ADDR']
					];

					if($this->session->userdata('rdscpa_2')=='CPA') $url="https://leaderscpa.com/merchant/nodaji/inc/consult_process.asp"; else if($this->session->userdata('rdscpa_2')=='BM') $url="http://bmcpa01.co.kr/merchant/nodaji/inc/consult_process.asp";
					$add="&key_code=".$this->session->userdata('rdscpa_1')."&ridx=".$this->session->userdata('rdscpa_3')."&name=".urlencode(iconv("UTF-8", "EUC-KR",$this->input->post('username')))."&hp=".$this->input->post('phone');
					
					$this->App_model->postReferer($param);
					$this->session->unset_userdata('rdscpa_1');
					$this->session->unset_userdata('rdscpa_2');
					$this->session->unset_userdata('rdscpa_3');
					$this->session->unset_userdata('rdscpa_4');
					$this->session->unset_userdata('rdscpa_5');
					$return_cpa=$this->sms->smsSend('receive_num','vType','title','msg',$url,$add,'cate');

				}else{
					// referer 회원가입 체크
					$param = [
						'nUserNo' => $data['nSeqNo']
						, 'vPhone' => $data['vPhone']
						, 'nJoin' => 1
					];
					$this->App_model->setReferer($param);
				}
				// 회원 포인트 적립 function putUserPoint($nUserNo, $nPoint, $nPointKind, $nType = null, $nBuyNo = null, $msg = null)
				$this->PurchaseModel->putUserPoint($data['nSeqNo'], 50000, 1, null, null, MSG_POINT_CHARGE);

				$param = [
					'nUserNo' => $data['nSeqNo']
					, 'changePoint' => 50000
					, 'changeType' => 'add'
					, 'nType' => null
					, 'nBuyNo' => null // 테스트
					, 'msg' => MSG_POINT_CHARGE
					, 'nPointType' => 2
					, 'nFreePoint' => 50000
				];
				
				$this->Point_model->setChangePoint($param);

				// return
				$rtn = array(
					"result" => "success",
					"msg" => "join",
					"data" => $data
				);
				header("Content-Type: application/json;charset=utf-8");
				echo json_encode($rtn);
			}
			else {
				$this->response->Json('error', ERROR_06);
			}
		}
		exit;
	}


	public function auth(){
		$this->util->isLogin(true);
		$popup = $this->_popupCheck('login');
		if($popup) $data['popup'] = $popup;
        $data['terms'] = $this->Site->getSiteInfo(['nSeqNo' => 1]);
		$data['adref'] = !empty($this->input->get('adref')) ? $this->input->get('adref') : null;
		$data['referer'] = base64_decode($this->input->get('ref'));
		if(strpos($data['referer'],"s://www.nodajistock.co.kr/") < 1 && strpos($data['referer'],"s://nodajistock.co.kr/") < 1 && substr($data['referer'],0,1)!='/') $data['referer']='/';
		if(isset($_GET['first'])) {
			if($_GET['first'] == 'Y'){
				$data['first']="Y";
			}
			else{
				$data['first']="N";
			}
		}
		else{
			$data['first']="N";
		}


		$data['key_code'] = !empty($this->input->post('key_code')) ? substr($this->input->post('key_code'),0,32) : null;//식별키 리더스cpa 추가
		$data['siteType'] = !empty($this->input->post('siteType')) ? substr($this->input->post('siteType'),0,3) : null;//캠페인 구분값 리더스cpa 추가
		$data['ridx'] = !empty($this->input->post('ridx')) ? $this->input->post('ridx') : null;//유입idx 리더스cpa 추가
		$data['ref_url'] = !empty($this->input->post('ref_url')) ? $this->input->post('ref_url') : null;//유입url 리더스cpa 추가
		$data['ref_keyword'] = !empty($this->input->post('ref_keyword')) ? $this->input->post('ref_keyword') : null;//유입키워드 리더스cpa 추가
		$this->session->set_userdata('rdscpa_1',$data['key_code']);//리더스cpa 추가
		$this->session->set_userdata('rdscpa_2',$data['siteType']);//리더스cpa 추가
		$this->session->set_userdata('rdscpa_3',$data['ridx']);//리더스cpa 추가
		$this->session->set_userdata('rdscpa_4',$data['ref_url']);//리더스cpa 추가
		$this->session->set_userdata('rdscpa_5',$data['ref_keyword']);//리더스cpa 추가

		$this->response->View('main/auth', $data);        // 로그인 페이지
	}


	public function test(){

		echo '<pre>';
		print_r($_COOKIE);

	}


	// @param[nType] : 팝업 위치 
	function _popupCheck($nType) {
		// 팝업가져오기 
		$popup = $this->Board->getBannerPopup(['emKind' => 'wide', 'vType' => $nType]);
		// 팝업 시간체크 
		$isPopup = $this->session->userdata($nType.'_POPUP_TIME');


		// 팝업정보가 있으며 시 5분 시간체크가 없을때 
		if($popup && !$isPopup){
			$this->session->set_tempdata($nType.'_POPUP_TIME', true, 300);
			return $popup;
		}
		else{
			return false;
		}
	}


}
