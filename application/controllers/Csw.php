<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Csw extends CI_Controller {


	// 생성자
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function index() {


		$aHeader = array(
			'user-agent: Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Mobile Safari/537.36'
			, '"authority: m.blog.naver.com" '
			,"authority: m.blog.naver.com" 
			,"sec-ch-ua-mobile: ?1" 
			,"user-agent: Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Mobile Safari/537.36" 
			,"sec-fetch-site: same-origin" 
			,"sec-fetch-mode: cors" 
			,"sec-fetch-dest: empty" 
			,"referer: https://m.blog.naver.com/yeogiya_pr" 
			,"accept-language: ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7" 
			,"cookie: NNB=C7W2K"
		);
		

		$u = "https://m.blog.naver.com/rego/BlogInfo.nhn?blogId=yeogiya_pr";




		$sUrl = sprintf("%s", $u);
		$sHtml = $this->my_get_curl($sUrl, $aHeader);

		$sHeader = get_headers($sUrl, TRUE); // header redirect check
	}


	public function my_get_curl($psFileName, $paHeader = NULL, $psData = NULL, $pnTimeout = 10, $psRequest = NULL) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $psFileName);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		if (!is_null($paHeader)) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, $paHeader);
		}
		if (!is_null($psData)) {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $psData);
		}
		if (!is_null($psRequest)) {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($psRequest));
		}
		//curl_setopt($ch, CURLOPT_ENCODING , 'gzip');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS, $pnTimeout * 1000);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041107 Firefox/1.0');
		$content = curl_exec($ch);
		curl_close($ch);

		return $content;
	}

}



