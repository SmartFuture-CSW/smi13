<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
    * 2. 종목 Controller
    * @author 임정원 / 2020-02-14
    * @since  Version 1.0.0
*/

class Stock extends CI_Controller
{

	# 생성자
	function __construct() 
	{
		parent::__construct();
		// 게시판 -> DB 처리
		$this->load->model('Board');
		$this->load->model('StockF');
		$this->load->model('Payment');
		$this->load->model('Share');
		$this->load->model('Scrap');

		// 로그인 상태체크

		// login cookie 세션 동기화
		if(!empty(get_cookie('UNO')) && !$this->session->userdata('UNO')) {
			$sData = [
				'UNO'  => get_cookie('UNO'),
				'UID'  => get_cookie('UID'),
				'UNK'  => get_cookie('UNK'),
				'UNM'  => get_cookie('UNM')
			];		
			$this->session->set_userdata($sData);
		}

		/*
		if (! $this->session->userdata('UNO')) {
			$this->util->alert('로그인 후 이용가능합니다.', '/login');
		}
		*/

	}

	public function index() {

		$data['page'] = 'list';

		$data['period'] = $this->Board->getBoardMainStock(['vMainVal' => 4]);
		$data['news'] = $this->Board->getBoardMainStock(['vMainVal' => 5]);
		$data['theme'] = $this->Board->getBoardMainStock(['vMainVal' => 6]);

		$data['arrStockResult'] = ['1'=>'진행', '2'=>'성공', '3'=>'실패', '4' => '기간종료'];
		$data['result'] = $this->Board->getStockResult();
		$data['popup'] = $this->Board->getBannerPopup(['emKind' => 'wide', 'vType' => 'stock']);
		//echo '<pre>'; print_r($data['theme']);exit;
		$this->response->View('stock/index', $data);
	}

	public function period() {
		$data['page'] = 'list';

		$data['period'] = $this->Board->getStock(['vType' => 'period']);
		$data['banner']= $this->Board->getBanner(['emKind' => 'band']);
		$data['popup'] = $this->Board->getBannerPopup(['emKind' => 'wide', 'vType' => 'theme']);
		$this->response->View('stock/period', $data);
	}


	public function result() {
		$data['page'] = 'list';

		$data['infinity_page'] = 1;

		// 투자성과 상태값
		$data['arrStockResult'] = ['1'=>'진행', '2'=>'성공', '3'=>'실패', '4' => '기간종료'];
		$data['result'] = $this->Board->getStockResult();
		$data['banner']= $this->Board->getBanner(['emKind' => 'band']);
		$this->response->View('stock/result', $data);
	}

	public function getInfinityResult() {

		$data['page'] = 'list';
		$keyword = $this->input->post('keyword') ? $this->input->post('keyword') : null;
		$like = null;

		if($keyword != null){
			$like = ['vSubject' => $keyword];
		}

		// 투자성과 상태값
		$data['list'] = $this->Board->getStockResult(5, $like);
		$data['status'] = "SUCCESS";
		$data['arrStockResult'] = ['1'=>'진행', '2'=>'성공', '3'=>'실패', '4' => '기간종료'];


		echo json_encode($data);

	}




	public function news(){
		$data['page'] = 'list';

		$data['news'] = $this->Board->getStock(['vType' => 'news']);
		$data['banner']= $this->Board->getBanner(['emKind' => 'band']);
		$data['popup'] = $this->Board->getBannerPopup(['emKind' => 'wide', 'vType' => 'theme']);
		$this->response->View('stock/news', $data);
	}


	public function theme() {
		
		$data['page'] = 'list';
		$data['theme'] = $this->Board->getStock(['vType' => 'theme']);
		$data['banner']= $this->Board->getBanner(['emKind' => 'band']);
		$data['popup'] = $this->Board->getBannerPopup(['emKind' => 'wide', 'vType' => 'theme']);
		$this->response->View('stock/theme', $data);
	}

	// 안쓰임
	public function day() {
		$data['day'] = $this->Board->getStock(['vType' => 'period']);
		$data['banner']= $this->Board->getBanner(['emKind' => 'band']);
		$data['popup'] = $this->Board->getBannerPopup(['emKind' => 'wide', 'vType' => 'period']);
		$this->response->View('stock/day', $data);
	}
	
	// 안쓰임
	public function rumor() {
		$data['rumor'] = $this->Board->getStock(['vType' => 'rumor']);
		$data['banner']= $this->Board->getBanner(['emKind' => 'band']);
		$data['popup'] = $this->Board->getBannerPopup(['emKind' => 'wide', 'vType' => 'rumor']);
		$this->response->View('stock/rumor', $data);
	}
	
	public function view($type, $stockNo)
	{
		if( !isset($type) ){
			$this->util->alert('잘못된 접근입니다.', '/stock');
		}

		// 결제한 유저인지 체크 
		$data['isAccess'] = $this->Payment->getStockAccess($this->session->userdata('UNO'), $stockNo);
		$data['type'] = $type;
		# 게시글 가져오기 
		$data['view'] = $this->Board->getStockView($stockNo);
		$data['isScrap'] = $this->Scrap->getIsScrap($stockNo, $type);
		# 배너 
		$data['banner']= $this->Board->getBanner(['emKind' => 'band']);

		// 조회수 CHECK
		if (! isset($_COOKIE["stockview_{$type}_{$stockNo}"])) {
			$result = $this->Board->setBoardHit($stockNo,'ndg_Stock');
			setcookie("stockview_{$type}_{$stockNo}", true, time() + 86400, '/stock/view'); // time() + 86400 = 쿠키 유효 시간 설정해줌(하루)
		}

		$this->load->view('include/header_view', $data);
		$this->load->view('stock/view', $data);
		$this->load->view('include/footer');


	}



	// search 페이지
	public function search() {
		
		$data['page'] = 'search';

		$vType = $this->uri->segment(3) ? urldecode($this->uri->segment(3)) : '';
		$data['pageType'] = $this->uri->segment(3) ? urldecode($this->uri->segment(3)) : 'all';
		$data['keyword'] = $this->input->get('keyword') ? $this->input->get('keyword') : urldecode($this->uri->segment(4));
		$data['banner']    = $this->Board->getBanner(['emKind' => 'band']);

			$data['arrStockResult'] = ['1'=>'진행', '2'=>'성공', '3'=>'실패', '4' => '기간종료'];
		if($data['pageType'] == "all"){
			$data['period'] = $this->Board->getStockSearchResult(['vType' => 'period'], ['vSubject' => $data['keyword']]);	// 추천종목 3
			$data['news'] = $this->Board->getStockSearchResult(['vType' => 'news'], ['vSubject' => $data['keyword']]);			// 투자정보 3
			$data['theme'] = $this->Board->getStockSearchResult(['vType' => 'theme'], ['vSubject' => $data['keyword']]);			// 주식공부 3
			$data['result'] = $this->Board->getStockResult(5, ['vSubject' => $data['keyword']]);			// 주식공부 3
		}
		else{
			if($vType == "result"){
				$data['infinity_page'] = 1;
				$data['result'] = $this->Board->getStockResult(5, ['vSubject' => $data['keyword']]);			// 주식공부 3
			}else{
				$data[$vType] = $this->Board->getStock(['vType' => $vType], ['vSubject' => $data['keyword']]);
			}
		}
		$this->response->View('stock/search', $data);

	}



	// 공유체크 
	public function stockShare() {
		$boardNo    = $this->input->post('boardNo');
		$userNo     = $this->session->userdata('UNO');
		$type       = $this->input->post('type');

		if(!$boardNo) {
			$this->response->Json('error', '필수 값이 없습니다.', '/info/community', '');
			exit;
		}

		if(!$userNo) {
			$this->response->Json('success', '비회원 공유', '/info/community', '');
			exit;
		}

		// share DB 조회 
		$result = $this->Share->getShare($userNo, $boardNo, $type);
		
		if(empty($result)) {
			$insertData = [
				'nUserNo' => $userNo,
				'nBoardNo' => $boardNo,
				'vShareObj' => $type,
			];
			$s_result = $this->Share->addShare($insertData);

			if($s_result) {
				$bs_result = $this->StockF->setStockShare($boardNo);
				if($bs_result){
					$this->response->Json('success', '정상적으로 등록되었습니다.', '', '');exit;
				}
				else{
					$this->response->Json('error', '공유수 업데이트 실패', '', '');exit;
				}
			}
			else{
				$this->response->Json('error', '공유 저장 실패', '', '');exit;
			}
		}
		else{
			// 카운팅안되게 
			$this->response->Json('success', '이미 공유하였습니다', '', '');exit;
		}
	}


	// 좋아요 처리
	public function likeCheck() {
		$stockNo = $this->input->post('idx');

		if (! $stockNo) {
			$this->response->Json('error', '일시적인 오류', '', '');
			exit;
		}

		if (! isset($_COOKIE["like_stock_{$stockNo}"])) {
			$result = $this->StockF->setStockLike($stockNo);
			setcookie("like_stock_{$stockNo}", true, time() + 86400 * 7, '/stock'); // time() + 86400 = 쿠키 유효 시간 설정해줌(하루)
			$this->response->Json('success', '추천 감사합니다.', '', '');
			exit;
		} else {
			$this->response->Json('error', '이미 좋아요한 상태입니다.', '', '');
			exit;
		}
	}


	// 스크랩 추가
	public function stockScrap(){
		$rtn = null;
		$nBoardNo	= $this->input->post('nBoardNo');
		$nUserNo		= $this->session->userdata('UNO');
		$vType		= $this->input->post('vType');


		if(!$nBoardNo) {
			$rtn = array(
				'status' => 'FAIL',
				'code' => '9100',
				'msg' => '필수 값이 없습니다'
			);
		}
		else if(!$nUserNo) {
			$rtn = array(
				'status' => 'FAIL',
				'code' => '9200',
				'msg' => '비회원은 스크랩이 불가능 합니다.'
			);
		}
		else{
			$rtn = $this->Scrap->postScrap('stock');
		}
		header("Content-Type: application/json;charset=utf-8");
		echo json_encode($rtn, true);
		exit;
	}

}