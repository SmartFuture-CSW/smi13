<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin Privacy Controller
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터 바인딩 처리 및 뷰페이지 호출
  *   # index # userList # history
  *   # getUserList # getUserCount
  *
*/

class Privacy extends CI_Controller
{

	function __construct()
	{
		# 생성자
		parent::__construct();
		$this->load->model('SiteinfoModel');
		if(!$this->session->userdata('ULV') && $this->session->userdata('ULV') != 10){
			redirect('admin/login','location');exit;
		}
	}

	public function index(){}

	# 회원목록
	public function Privacy()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');
		$data['write'] = $this->SiteinfoModel->getPrivacy();
		$data['mode']='1';

		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/privacy/write', $data);
		$this->load->view('admin/common/footer');
	}

	public function PrivacyInsert()
	{
		$mode=$this->input->post('mode')?$this->input->post('mode'):'';
		$add='';
		if($mode=='2') $add='setperm';
		$this->SiteinfoModel->setPrivacywrite();
		$this->util->alert('처리되었습니다.','/admin/privacy/'.$add);
	}

	public function setPerm()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');
		$data['write'] = $this->SiteinfoModel->getPrivacy();
		$data['mode']='2';

		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/privacy/write', $data);
		$this->load->view('admin/common/footer');
	}
}