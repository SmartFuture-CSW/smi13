<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin User Controller
  * @author 임정원 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터 바인딩 처리 및 뷰페이지 호출
  *   # index # userList # history
  *   # getUserList # getUserCount
  *
*/

class User extends CI_Controller
{

	function __construct()
	{
		# 생성자
		parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('Payment');
		$this->load->model('Point_model');
		$this->load->library('Secret');
		if($this->session->userdata('ULV') < 4){
			redirect('admin/login','location');exit;
		}
	}

	public function index(){}

	# 회원목록
	public function userList()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$limit = 10;

		$ex = $this->input->post('ex') ? $this->input->post('ex'):'';
		if($ex=='ex') $pageNo = $this->input->post('pageNoEx') ? $this->input->post('pageNoEx'):1;

		# Data bind
		$data['pageNo'] = $pageNo;
		$data['total'] = $this->UserModel->getUserCount();
		$data['list'] = $this->UserModel->getUserList($pageNo, $data['total'], $limit);
		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);
		$data['level_label'] = $this->config->item('level_label');
		for($i=0;$i<count($data['list']);$i++){
			if($data['list'][$i]['nLevel']>1) $data['list'][$i]['UserId']=$this->secret->secretDecode($data['list'][$i]['vPhone']); else $data['list'][$i]['UserId']="보안상 미기재";
		}

		# view 페이지
		if($ex=='ex'){
			
			$data['list'] = $this->UserModel->getUserList($pageNo, $data['total'], $data['total']);


			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=ndg_user_".date("YmdHis").".xls");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
			header("Pragma: public");
			echo '<table border=1><thead>
			<tr>
			<th style="background-color:silver">번호</th>
			<th style="background-color:silver">이름</th>
			<th style="background-color:silver">전화번호</th>
			<th style="background-color:silver">접속정보</th>
			<th style="background-color:silver">가입일</th>
			<th style="background-color:silver">그룹</th>
			<th style="background-color:silver">회원</th>
			<th style="background-color:silver">수신</th>
			</tr>
			</thead>
			<tbody>';
			foreach ($data['list'] as $value) {
				if($this->session->userdata('UNO')=='3938') $value['vPhone']=$this->secret->secretDecode($value['vPhone']);
				echo '<tr>
				<td>'.$data['no'].'</td>
				<td>'.$value['vName'].'</td>
				<td style=mso-number-format:\'\@\'>'.$value['vPhone'].'</td>
				<td>'.$value['vDevice'].'</td>
				<td>'.$value['dtRegDate'].'</td>
				<td>'.($value['mgroup']>0?'<span style="color:red;">구독':'<span>무료').'그룹</span></td>
				<td>'.($value['nLevel']=='1'?'일반':'전문가').'</td>
				<td>'.($value['vPushOs']=='1'?'<span style="color:red;">동의</span>':'거부').'</td>
				</tr>';
				$data['no']--;
			}
			if(count($data['list'])==0){
				echo '<tr><td colspan="9" align="center" valign="middle" height="120"><br><br>No Data.</td></tr>';
			}
			echo '</tbody></table>';

		}else{
			$this->load->view('admin/common/header',$default);
			$this->load->view('admin/user/list', $data);
			$this->load->view('admin/common/footer');
		}
	}

	# 접속로그
	public function userLogin()
	{
		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$limit = 10;

		# DB 처리
		$data['pageNo'] = $pageNo;
		$data['total'] = $this->UserModel->getUserLoginCount();
		$data['list'] = $this->UserModel->getUserLogin($pageNo, $data['total'], $limit);
		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);

		# view 페이지
		$this->load->view('admin/common/header');
		$this->load->view('admin/user/history', $data);
		$this->load->view('admin/common/footer');
	}

	public function join(){
		# 마스터 권한만 가능

		# view 페이지
		$this->load->view('admin/common/header');
		$this->load->view('admin/user/join');
		$this->load->view('admin/common/footer');
	}

	public function poptb(){
		$no = $this->input->post('no') ? $this->input->post('no'):'';
		$data['cont']='<table style="width:100%;" border=1><tr style="background-color:silver;"><td align="center">시작일</td><td align="center">종료일</td><td align="center">구매내역</td><td align="center">상태</td></tr>';
		if($no!=''){
			$cont=$this->UserModel->getUserBuyList($no);

			$arrstatus = ["0"=>"사용중", "1"=>"전체취소", "2"=>"부분취소", "3"=> "환불신청", "9"=>"종료"];

			$where = ['P.nUserNo =' => $no, 'nType =' => 1 ];
			$select = ["P.nSeqNo, P.vStartDate, P.vEndDate, P.nOrderStatus, CONCAT('[', G.vGoodsKindName,']', ' ', P.vGoodName) as vGoodName, R.nSeqNo AS refundNo"];
			$cont = $this->Payment->getPaymentRefund($where, $select);

			$n=0;
			foreach ($cont as &$value) {
				
				if($value['vEndDate'] < date("Y-m-d") && $value['nOrderStatus'] == 0 ) {
					$payStatus = '종료';
				}
				else{
					$payStatus = $arrstatus[$value['nOrderStatus']];
				}
				$data['cont'].='<tr><td align="center">'.$value['vStartDate'].'</td><td align="center">'.$value['vEndDate'].'</td><td align="center">'.$value['vGoodName'].'</td><td align="center">'.$payStatus.'</td></tr>';
				$n++;
			}
			if($n==0){
				$data['cont'].='<tr><td colspan="4" height="100" align="center">데이터가 없습니다.</td></tr>';
			}
		}
		$data['cont'].='</table>';

		$arrPointKind = ["1"=>"적립", "2"=>"사용"];
		$where = ['P.nUserNo =' => $no];
		$pLog = $this->Payment->getPointListDetail($where);

		$data['pLog']='<table style="width:100%;" border=1><tr style="background-color:silver;"><td align="center">구분</td><td align="center">내역</td><td align="center">포인트 현황</td><td align="center">날짜</td></tr>	';
		foreach ($pLog as $key => $value) {
			$data['pLog'] .= '<tr><td align="center">'.$arrPointKind[$value['nPointKind']].'</td><td align="center">'.$value['vDescription'].'</td><td align="center">'.number_format($value['nPoint']).'</td><td align="center">'.substr($value['dtRegDate'],0,10).'</td></tr>';
		}
		$data['pLog'].='</table>';
		
		echo json_encode($data);
		exit;
	}

	public function delmem(){
		$delno = $this->input->post('delno') ? $this->input->post('delno'):'';
		$delno=explode(",",$delno);
		$delno=array_filter($delno);
		
		//$delno=implode(",",$delno);
		$delmem=$this->UserModel->delUser($delno);
		$data['result']='success';
		echo json_encode($data);
	}

	public function changeLevel(){
		$no = $this->input->post('no') ? $this->input->post('no'):'';
		$levelval = $this->input->post('levelval') ? $this->input->post('levelval'):'';
		$delmem=$this->UserModel->updateUser($no,$levelval);
		$data['result']='success';
		echo json_encode($data);
	}


	public function postMember(){

// [vName] => 최상운↵    [vNick] => 상운월드↵    [vPhone] => 01031058689
		$vName = $this->input->post('vName');
		$vNick = $this->input->post('vNick');
		$vPhone = $this->input->post('vPhone');
		$vPwd = $this->input->post('vPwd');


		$set = [
			'vName' => $vName
			, 'vNick' => $vNick
			, 'vPhone' => $this->secret->secretEncode($vPhone)
			, 'vPwd' => $this->secret->aes_encrypt($vPwd)
			, 'nLevel' => 3
		];

		$rtn = $this->UserModel->addUser($set);

		if($rtn == true){
			$rtn = ['status' => "SUCCESS"];
		}
		else{
			$rtn = ['status' => "FAIL"];
		}

		echo json_encode($rtn);
	}

}