<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin Sms Controller
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터 바인딩 처리 및 뷰페이지 호출
  *   # index # userList # history
  *   # getUserList # getUserCount
  *
*/

class Adminsms extends CI_Controller
{

	function __construct()
	{
		# 생성자
		parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('SmsModel');
		$this->load->library('Util');
		$this->load->library('Sms');
		$this->load->library('Secret');
		if(!$this->session->userdata('ULV') && $this->session->userdata('ULV') != 10){
			redirect('admin/login','location');exit;
		}
	}

	public function index(){}

	# 회원목록
	public function smsList()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$vType = $this->input->post('vType') ? $this->input->post('vType'):'';
		$sdate = $this->input->post('sdate') ? $this->input->post('sdate'):date("Ym");
		$limit = 10;

		# Data bind
		$data['mcount']=$this->SmsModel->getUserList("count");
		$data['vType']=$vType;
		$data['pageNo'] = $pageNo;
		if($vType!=''){
			$vType = $vType=="0"?"":$vType;
			$smslist = $this->sms->smsSend('010','list','','a','http://www.sendmon.com/_REST/smsApi.asp','&month='.urlencode($sdate).'&room_idx='.urlencode($vType),'history');
			$smslist = preg_replace("/[\r\n]+/", " ", $smslist);
			$list_result = json_decode($smslist);
			$list = isset($list_result->result)?$list_result->result:null;
			$data["list"] = $list;

			if(count($list) &&  $list!="no result"){
				$data["total"] = array_reduce($list, function($i, $obj){return $i += $obj->total;});
				$data["success"] = array_reduce($list, function($i, $obj){return $i += $obj->success;});
				$data["wait"] = array_reduce($list, function($i, $obj){return $i += $obj->wait;});
				$data["fail"] = array_reduce($list, function($i, $obj){return $i += $obj->fail;});
			}			
		}
		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/adminsms/list', $data);
		$this->load->view('admin/common/footer');
	}

	public function smsWrite(){
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		$apikey = Sms::ApiKey;
		if($apikey!=""){
			$sms_api = file_get_contents("http://www.sendmon.com/_REST/smsApi.asp?apikey=".$apikey);
			$result = json_decode($sms_api);
			$data["cash"] = $result->cashinfo[0]->cash;
			$data["sms"] = $result->cashinfo[0]->sms;
			$data["lms"] = $result->cashinfo[0]->lms;
			$data["mms"] = $result->cashinfo[0]->mms;
		}
		$data['mcount']=$this->SmsModel->getUserList("count");
		$data['stockUser'] = $this->SmsModel->getStockUserList();
		$data['premiumSMS'] = $this->SmsModel->getPremiumSMSUserList();

		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/adminsms/write', $data);
		$this->load->view('admin/common/footer');
	}

	public function smsInsert()
	{
		$data['insert']=$this->SmsModel->sendSms();
		$data['code']='1';
		echo json_encode($data);
	}

	public function popMain()
	{
		$price=$this->input->post('price') ? $this->input->post('price'):'';
		if($price!=''){
			$price=$price*10000;
			$jdata=$this->sms->smsSend('1', '1', '1', '1', 'http://www.sendmon.com/_REST/settleApi.asp','&PayWay=ONLINE&SettleCash='.urlencode($price).'&SettlePrice='.urlencode($price));
			$data['code']='1';
		}else{
			$jdata=$this->sms->smsSend('1', '1', '1', '1', 'http://www.sendmon.com/_REST/settleListAsp.asp');
			$data['list']=json_decode($jdata,true);
		}
		echo json_encode($data);
	}

	public function popDetail()
	{
		$uniq_id=$this->input->post('uniq_id') ? $this->input->post('uniq_id'):'';
		$used_cd=$this->input->post('used_cd') ? $this->input->post('used_cd'):'';
		$data["detail"] = "";
		if($uniq_id!='' && $used_cd!=''){
			$history = $this->sms->smsSend('010','detail',$uniq_id,'a','http://www.sendmon.com/_REST/smsApi.asp','&room_idx='.urlencode($used_cd),'history');
			$history = preg_replace("/[\r\n]+/", " ", $history);
			$history_result = json_decode($history);

			$history_list = $history_result->result;
			$history_list2 = isset($history_result->result2)?$history_result->result2:null;
			$data["detail"] = $history_list2!=null?array_merge($history_list,$history_list2):$history_list;
			$data['code']='1';
		}else{
			$data['code']='2';
			$data['msg']="잘못된 접근입니다.";
		}
		echo json_encode($data);
	}

	public function Refresh(){
		$reg1=$this->input->post('reg1') ? $this->input->post('reg1'):'';
		$reg2=$this->input->post('reg2') ? $this->input->post('reg2'):'';
		$opt='';
		if($reg1!='') $opt.='s'.$reg1;
		if($reg2!='') $opt.='e'.$reg2;
		$data['mcount']=$this->SmsModel->getUserList("count",$opt);
		$data['all']=$data['mcount']['all'][0]['cnt'];
		$data['charge']=$data['mcount']['charge'][0]['cnt'];
		$data['free']=number_format($data['all']-$data['charge']);
		$data['all']=number_format($data['all']);
		$data['charge']=number_format($data['charge']);

		$data['premiumSMS'] = number_format(count($this->SmsModel->getPremiumSMSUserList()));
		$data['code']='1';
		echo json_encode($data);
	}
}