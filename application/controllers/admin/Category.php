<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin Board Controller
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터 바인딩 처리 및 뷰페이지 호출
  *   # index # userList # history
  *   # getUserList # getUserCount
  *
*/

class Category extends CI_Controller
{

	public $userInfo;


	function __construct()
	{
		# 생성자
		parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('BoardModel');
		$this->load->model('PushModel');
		$this->load->model('CategoryModel');
		$this->load->library('Util');

		if(!$this->session->userdata('ULV') && $this->session->userdata('ULV') != 10){
			redirect('admin/login','location');exit;
		}

		// user session info
		$userInfo['anick'] = $this->session->userdata('UNK');
		$userInfo['aname'] = $this->session->userdata('UNM');

		$this->userInfo = $userInfo;
	}

	// 게시판 카테고리 관리 리스트
	public function index(){
		$pageblock = 10;
		$boardList = $this->CategoryModel->getBoardList();
		for($i=0; $i<count($boardList); $i++){
			$vType = $boardList[$i]['vType'];
			$vName = $boardList[$i]['vName'];
			$data['board'][$vType] = $vName;
		}

		$data['pageNo'] = $this->input->post('pageNo') ? $this->input->post('pageNo') : 1;
		$data['total'] = $this->CategoryModel->getCategoryCount();
		$data['list'] = $this->CategoryModel->getCategoryList($data['pageNo'], $data['total'], $pageblock);

		$data['paging'] = $this->paging->admin_pagination($pageblock, $data['pageNo'], $data['total']);
		$data['no'] = $data['total'] - (($data['pageNo'] - 1) * $pageblock);
		$data['level_label'] = $this->config->item('level_label');

		# view 페이지
		$this->load->view('admin/common/header',$this->userInfo);
		$this->load->view('admin/category/list', $data);
		$this->load->view('admin/common/footer');

	}


	// 게시판 카테고리 입력 화면
	public function writeCategory(){
		$data['mode'] = 'post';
		$data['board'] = $this->CategoryModel->getBoardList();
		$data['writer'] = $this->userInfo['aname'];
		$this->load->view('admin/common/header',$this->userInfo);
		$this->load->view('admin/category/write', $data);
		$this->load->view('admin/common/footer');
	}

	// 게시판 카테고리 수정 화면
	public function editCategory(){
		$data['mode'] = 'put';
		$data['board'] = $this->CategoryModel->getBoardList();
		$data['category'] = $this->CategoryModel->getCategoryInfo();
		$data['writer'] = $data['category']['vName'] ? $data['category']['vName'] : $this->userInfo['aname'];
		$this->load->view('admin/common/header',$this->userInfo);
		$this->load->view('admin/category/write', $data);
		$this->load->view('admin/common/footer');
	}

	// 게시판 카테고리 입력
	public function postCategory(){
		$data = $this->CategoryModel->postCategory();
		if($data){
			redirect('admin/category','location');
			exit;
		}
		else{
			$this->util->alert('DB 오류!', '');
			redirect('admin/category','location');
			exit;
		}
	}

	// 게시판 카테고리 삭제
	public function removeCategory(){
		$data = $this->CategoryModel->removeCategory();
		if($data){
			redirect('admin/category','location');
			exit;
		}
		else{
			$this->util->alert('DB 오류!', '');
			redirect('admin/category','location');
			exit;
		}

	}
}