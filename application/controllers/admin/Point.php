<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin Point Controller
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터 바인딩 처리 및 뷰페이지 호출
  *   # index # userList # history
  *   # getUserList # getUserCount
  *
*/

class Point extends CI_Controller
{

	function __construct()
	{
		# 생성자
		parent::__construct();
		$this->load->model('PointModel');
		$this->load->model('Point_model');
		if(!$this->session->userdata('ULV') && $this->session->userdata('ULV') != 10){
			redirect('admin/login','location');exit;
		}
	}

	public function index(){}

	# 회원목록
	public function pointList()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$vType = $this->input->post('vType') ? $this->input->post('vType'):'';
		$limit = $this->input->post('limit') ? $this->input->post('limit'):10;
		$reg1 = $this->input->post('reg1') ? $this->input->post('reg1'):'';
		$reg2 = $this->input->post('reg2') ? $this->input->post('reg2'):'';
		$keyword = $this->input->post('keyword') ? $this->input->post('keyword'):'';

		# Data bind
		$data['pageNo'] = $pageNo;
		$data['vType'] = $vType;
		$data['limit'] = $limit;
		$data['reg1'] = $reg1;
		$data['reg2'] = $reg2;
		$data['keyword'] = $keyword;
		$data['total'] = $this->PointModel->getPointCount();
		$data['list'] = $this->PointModel->getPointList($pageNo, $data['total'], $limit);
		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);
		$data['level_label'] = $this->config->item('level_label');

		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/point/list', $data);
		$this->load->view('admin/common/footer');

	}

  # 접속로그
  public function savePoint()
  {
		$point=$this->input->post('point') ? $this->input->post('point'):'';
		$msg=$this->input->post('msg') ? $this->input->post('msg'):'';
		$no=$this->input->post('no') ? $this->input->post('no'):'';
		$data['point']=$point;
		$data['msg']=$msg;
		$data['no']=$no;
		if($point!='' && $no!=''){
			$this->PointModel->setPoint($no,$point,$msg);
			

			$param = [
				'nUserNo' => $no
				, 'changePoint' => abs($point)
				, 'changeType' => ($point < 0)? 'dud' : 'add'
				, 'nType' => null
				, 'nBuyNo' => null // 테스트
				, 'msg' => $msg
				, 'nPointType' => 2
				, 'nFreePoint' => $point
			];
			
			$this->Point_model->setChangePoint($param);


			$data['code']='1';
		}else{
			$data['code']='2';
		}
		echo json_encode($data);
  }

  public function join(){
    # 마스터 권한만 가능

    # view 페이지
    $this->load->view('admin/common/header');
    $this->load->view('admin/user/join');
    $this->load->view('admin/common/footer');
  }




}
