<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin Main Controller
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource
  *   # index # loginCheck # logOut
  *
*/

class Main extends CI_Controller
{

	function __construct()
	{
		# 생성자
		parent::__construct();
		$this->load->database();
		$this->load->library('Util');
        $this->load->library('Secret');
		$this->load->model('UserModel');
		$this->load->model('PaymentModel');
		$this->load->model('BoardModel');
		$this->load->model('StockModel');


	}

	# main / login
	public function index()
	{
		if(!$this->session->userdata('ULV') || $this->session->userdata('ULV') < 4){
			redirect('admin/login','location');
			exit;
		}
		# 세션 체크 true -> Main page / false -> Login page
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');
		$default['total'] = $this->UserModel->getUserCount();
		$default['paycount'] = $this->PaymentModel->getPaymentCount();
		$default['bbscount'] = $this->BoardModel->getBbsAllCount();
		$default['rplcount'] = $this->BoardModel->getReplyCount();
		$default['paylist'] = $this->PaymentModel->getPaymentList(1, 5, 5);
		$default['userlist'] = $this->UserModel->getUserList(1, 5, 5);
		$default['stocklist'] = $this->StockModel->getStockList(1, 5, 5);
		$_POST['vType']='reply';
		$default['replylist'] = $this->BoardModel->getBbsList(1, 5, 5);

		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/main');
		$this->load->view('admin/common/footer');
	}

	public function login()
	{
		$this->load->view('admin/login');
	}

	# 관리자 로그인체크 처리
	public function loginCheck()
	{
		# formData
		$vUserId = $this->input->post('userid');
		$vUserPwd = $this->input->post('passwd');

		if(!$vUserId) $this->util->alert(ERROR_01, '');
		if(!$vUserPwd) $this->util->alert(ERROR_02, '');

		$aUserId = [$vUserId, $this->secret->secretEncode($vUserId)];

		# DB 처리
		$this->db->from('ndg_User');
		$this->db->where_in('vPhone', $aUserId);
		$this->db->where('vPwd', $this->secret->aes_encrypt($vUserPwd));
		$this->db->where_in('nLevel', array(2,3,4,10));//2-전문가, 3-협력업체, 4-엑셀다운, 10-관리자
		$this->db->where('emDelFlag', 'N'); # 사용중회원 N / 탈퇴한 회원 Y
		# result
		$query=$this->db->get();

		if ($query->num_rows() > 0){

		  # session 처리
		  $row = $query->row();

		  $sessionData = array(
			  'UNO'  => $row->nSeqNo,
			  'UID'  => $row->vPhone,
			  'UNM'  => $row->vName,
			  'UNK'  => $row->vNick,
			  'ULV'	=> $row->nLevel
		  );

		  $this->session->set_userdata($sessionData);

		  # 접속기록 insert
		  $this->db->set('nUserNo', $row->nSeqNo);
		  $this->db->set('vUserIp', $_SERVER["REMOTE_ADDR"]);
		  $this->db->set('vUserAgent', $_SERVER["HTTP_USER_AGENT"]);
		  $this->db->set('dtRegDate', date("Y-m-d H:i:s"));
		  $this->db->insert('ndg_UserLogin');

		if($row->nLevel == 10 || $row->nLevel == 4){
			$this->util->alert('', '/admin');
		}else if($row->nLevel == 3){
			$this->util->alert('', '/admin/campain/list');
		}else if($row->nLevel == 2){
			$this->util->alert('', '/admin/adminsms');
		}
		  //$this->util->sweetAlert('로그인성공', '메인페이지로 이동', '/admin', 'success');
		}else{
			$this->util->alert(ERROR_03);
		  //$this->util->sweetAlert('Error', ERROR_03, '', 'error');
		}
	}

	# 로그아웃 처리
	public function logOut()
	{
		session_destroy();
		$this->util->alert('로그아웃 되었습니다.', '/admin');
	}
}