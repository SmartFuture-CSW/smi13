<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Campain extends CI_Controller {

	function __construct()
	{
		# 생성자
		parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('CampainModel');
		$this->load->library('Secret');
		if(!$this->session->userdata('ULV') && $this->session->userdata('ULV') < 3){
			redirect('admin/login','location');exit;
		}
	}

	public function index(){
	

	}

	// 캠페인 등록
	public function manage(){
		$default['anick'] = $this->session->userdata('UNK');
		$default['aname'] = $this->session->userdata('UNM');
		$data = [];
		if($this->session->userdata('ULV') >= 10){
			$data['adlist'] = $this->UserModel->getAdList();
		}
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/campain/manage', $data);
		$this->load->view('admin/common/footer');
	}

	// 캠페인 현황
	public function listCampain(){
		$default['anick'] = $this->session->userdata('UNK');
		$default['aname'] = $this->session->userdata('UNM');
		$data = [];
		$limit = (!empty($this->input->get('limit'))) ? $this->input->get('limit') : 10;

		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$total = $this->CampainModel->getCampainCount();
		$list = $this->CampainModel->getCampainList($pageNo, $total);
		$no = $total - (($pageNo - 1) * $limit);

		$paging =  $this->paging->admin_pagination($limit, $pageNo, $total);

		$data = [
			'pageNo' => $pageNo
			, 'total' => $total
			, 'list' => $list
			, 'no' => $no
			, 'paging' => $paging
		];
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/campain/list', $data);
		$this->load->view('admin/common/footer');
	}

	// 캠페인 통계
	public function stats(){
		$default['anick'] = $this->session->userdata('UNK');
		$default['aname'] = $this->session->userdata('UNM');
		$data = [];
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$limit = (!empty($this->input->post('limit'))) ? $this->input->post('limit') : 10;
		
		
		$stats = $this->CampainModel->getStatsSum();
		$campain = $this->CampainModel->getLiveCampain();
		$aCamapin = [];
		$aReferer = [];
		foreach($campain as $row){
			$aCamapin[$row['vReferer']] = $row['vCampain'];
			$aReferer[] = $row['vReferer'];
		}

		$total = $this->CampainModel->getVisitCount($aReferer);
		if($total != 0){
			$visit = $this->CampainModel->getVisitData($pageNo, $total, $aReferer);
		}else{
			$visit = [];
		}
		$no = $total - (($pageNo - 1) * $limit);

		$paging =  $this->paging->admin_pagination($limit, $pageNo, $total);

		$data = [
			'pageNo' => $pageNo
			, 'stats' => $stats
			, 'list' => $visit
			, 'no' => $no
			, 'paging' => $paging
			, 'aCamapin' => $aCamapin
			, 'campain' => $campain
		];


		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/campain/stats', $data);
		$this->load->view('admin/common/footer');

	}



	public function postCampain(){
		$aRtn = null;
		$vCampain	= $this->input->post("vCampain") ? $this->input->post("vCampain") : null;
		$vReferer	= $this->input->post("vReferer") ? $this->input->post("vReferer") : null;
		$vHp		= $this->input->post("vHp") ? $this->input->post("vHp") : null;
		$vAdDevice	= $this->input->post("vAdDevice") ? $this->input->post("vAdDevice") : null;
		$nUserNo	= $this->input->post("nUserNo") ? $this->input->post("nUserNo") : $this->session->userdata("UNO");
		if($vCampain == null || $vReferer == null || $vAdDevice == null){
			$aRtn['status'] = "FAIL";
			$aRtn['message'] = "누락값이 존재 합니다.";
		}
		else{
			$vAdUrl = ($vAdDevice != 1) ? "https://play.google.com/store/apps/details?id=com.stockboss.stock&referrer=" : ($this->input->post("vAdUrl"));
			$vAdUrl .= $vReferer;
			$param = [
				"vCampain" => $vCampain
				, "vReferer" => $vReferer
				, "vHp" => $this->secret->secretEncode($vHp)
				, "vAdDevice" => $vAdDevice
				, "nUserNo" => $nUserNo
				, "vAdUrl" => $vAdUrl
			];
			$data = $this->CampainModel->postCampain($param);
			$aRtn = $data;
		}
		echo json_encode($aRtn);
	}


	public function removeCampain(){
		$delNo = $this->input->post("delno");
		$delNo = explode(",", $delNo);
		$param = [
			"delNo" => $delNo
		];		
		$this->CampainModel->removeCampain($param);
		$aRtn = [
			'status' => 'SUCCESS'
		];
	
		echo json_encode($aRtn);
	}


}