<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin Board Controller
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터 바인딩 처리 및 뷰페이지 호출
  *   # index # userList # history
  *   # getUserList # getUserCount
  *
*/

class Board extends CI_Controller
{

	function __construct()
	{
		# 생성자
		parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('BoardModel');
		$this->load->model('PushModel');
		$this->load->model('CategoryModel');
		
		$this->load->library('Util');
		$this->load->library('Secret');
		if(!$this->session->userdata('ULV') && $this->session->userdata('ULV') != 10){
			redirect('admin/login','location');exit;
		}
	}

	public function index(){}

	# 회원목록
	public function bbsList()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';

		if($bo_table=='') $bo_table = $this->input->post('vType')?$this->input->post('vType'):'';
		if($bo_table=='') $bo_table='today';
		$_POST['vType']=$bo_table;
		$limit = 10;

		# Data bind
		$data['pageNo'] = $pageNo;
		$data['bo_table'] = $bo_table;
		$data['total'] = $this->BoardModel->getBbsCount();


		$bbs = $this->BoardModel->getBbsList($pageNo, $data['total'], $limit);

		foreach($bbs as &$value){
			$value['encurl'] = "https://www.nodajistock.co.kr/key/".$this->secret->encrypt256($value['nSeqNo']);
		}


		$data['list'] = $bbs;
		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);
		$data['level_label'] = $this->config->item('level_label');


		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/board/list', $data);
		$this->load->view('admin/common/footer');
	}

	public function bbsEdit()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$bd_no = $this->input->post('bd_no') ? $this->input->post('bd_no'):'';

		# Data bind
		$data['bdname']=array('today'=>'투데이','important'=>'추천종목','news'=>'뉴스','community'=>'커뮤니티', 'invest'=>'투자정보', 'study'=>'주식공부', 'notice'=>'공지사항', 'key'=>'핵심정보');
		$data['pageNo'] = $pageNo;
		$data['write'] = $this->BoardModel->getBbswrite($bd_no);

		$data['total'] = $this->BoardModel->getBbsCount();
		$data['ctzlist'] = $this->BoardModel->getBbsList(1, $data['total'], 100000);

		if(isset($data['write'])){
			$data['bo_table'] = $data['write'][0]['vType'];
		}else{
			$data['bo_table'] = $this->input->post('vType')?$this->input->post('vType'):'';
		}

		$data['category'] = $this->CategoryModel->getBoardCategoryList($this->input->post('vType'));
		if($data['bo_table']=='') $data['bo_table']='today';

		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/board/write', $data);
		$this->load->view('admin/common/footer');
	}

	public function bbsUpdate()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$bd_no = $this->input->post('bd_no') ? $this->input->post('bd_no'):'';
		$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
		if($bo_table=='') $bo_table='today';
		$limit = 10;

		# Data bind
		$data['bdname']=array('today'=>'투데이','important'=>'핵심정보','news'=>'뉴스','community'=>'커뮤니티');
		$data['pageNo'] = $pageNo;
		$data['bo_table']=$bo_table;	
		$this->BoardModel->setBbsupdate();
		$data['total'] = $this->BoardModel->getBbsCount();



		$data['list'] = $this->BoardModel->getBbsList($pageNo, $data['total'], $limit);





		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);
		$data['level_label'] = $this->config->item('level_label');
		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/board/list', $data);
		$this->load->view('admin/common/footer');
	}

	public function bbsDel()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');
//		$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
		if($bo_table=='') $bo_table='today';
		$data['bo_table'] = $bo_table;

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$limit = 10;

		# Data bind
		$data['pageNo'] = $pageNo;
		$this->BoardModel->delBbsData();

		redirect('admin/board/'.$bo_table,'location');exit;
		exit;	




		# Data bind
		$data['pageNo'] = $pageNo;
		$data['total'] = $this->BoardModel->getBbsCount();
		$data['list'] = $this->BoardModel->getBbsList($pageNo, $data['total'], $limit);
		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);
		$data['level_label'] = $this->config->item('level_label');

		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/board/list', $data);
		$this->load->view('admin/common/footer');
	}

	public function searchHash()
	{
		# 페이징설정
		$pageNo = 1;
		$limit = 3;

		# Data bind
		$data['total'] = $this->BoardModel->getBbsCount();
		$data['list'] = $this->BoardModel->getBbsList($pageNo, $data['total'], $limit);
		$data['str']='';
		foreach($data['list'] as $row){
			$data['str'].=",".$row['nSeqNo'];
		}
		$data['str']=substr($data['str'],1);
		echo json_encode($data);
	}

	public function bbsInsert()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');
		$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
		if($bo_table=='') $bo_table='today';
		$data['bo_table'] = $bo_table;

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$limit = 10;
		$data['insert'] = $this->BoardModel->setBbswrite();

		$emPush = ($this->input->post('emPush')?'1':'0');
		if($emPush == '1'){
			// sendPush($r1 = null, $vType = null, $vEtc2 = null, $txContent = null)
			if($data['insert']){
				$vEtc2 = '/boardView/' . $bo_table . "/" . $data['insert'];
			}else{
				$vEtc2 = null;
			}

			$text = strip_tags($this->input->post('txContent'));
			$text = str_replace("&nbsp;", " ", $text);

			$text = $this->util->cut_str($text, 60);
			$subject = $this->util->cut_str($this->input->post('vSubject'), 30);
			$data = $this->PushModel->sendPushAndroid(['all'], 'push', $vEtc2, $text, $subject);
		}

		redirect('admin/board/'.$bo_table,'location');exit;
		exit;	

		# Data bind
		$data['pageNo'] = $pageNo;
		$data['total'] = $this->BoardModel->getBbsCount();
		$data['list'] = $this->BoardModel->getBbsList($pageNo, $data['total'], $limit);
		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);
		$data['level_label'] = $this->config->item('level_label');

		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/board/list', $data);
		$this->load->view('admin/common/footer');
	}

	public function popMain()
	{
		$no = $this->input->post('mainval') ? $this->input->post('mainval'):'';

		$data['cont']='';
		if($no!=''){
			$cont=$this->BoardModel->getBbsMainList($no);
			for($i=1;$i<=5;$i++){
				$data['cont'][$i]['vType']='';
				$data['cont'][$i]['bdno']='';
				$data['cont'][$i]['vTypeList']='';
			}
			foreach ($cont as $value) {
				$data['cont'][$value['nSort']]['vType']=$value['vType'];
				$data['cont'][$value['nSort']]['bdno']=$value['nBoardNo'];
				$_POST['vType'] = $value['vType'];
				$data['cont'][$value['nSort']]['vTypeList']=$this->BoardModel->getBbsList(1,100,999999);
			}
		}
		echo json_encode($data);
		exit;
	}

	public function popMain2()
	{
		$no = $this->input->post('vType') ? $this->input->post('vType'):'';
		$data['cont']='';
		if($no!=''){
			$data['cont']=$this->BoardModel->getBbsList(1,100,999999);
		}
		echo json_encode($data);
		exit;
	}

	public function popUpdate()
	{
		$fval = $this->input->post('fval') ? $this->input->post('fval'):'';
		$cate = $this->input->post('category') ? $this->input->post('category'):'';
		$this->BoardModel->setBbsMainUpdate();
		$data['code']='1';
		echo json_encode($data);
		exit;
	}

	public function popAutoList()
	{
		$mainval = $this->input->post('mainval') ? $this->input->post('mainval'):'';
		$cont=$this->BoardModel->getBbsMainAutoList($mainval);
		for($i=1;$i<=5;$i++){
			$data['cont'][$i]['vType']='';
			$data['cont'][$i]['bdno']='';
			$data['cont'][$i]['vTypeList']='';
		}
		$n=1;
		foreach ($cont as $value) {
			$data['cont'][$n]['vType']=$value['vType'];
			$data['cont'][$n]['bdno']=$value['nSeqNo'];
			$_POST['vType'] = $value['vType'];
			$data['cont'][$n]['vTypeList']=$this->BoardModel->getBbsList(1,100,30);
			$n++;
		}
		echo json_encode($data);
		exit;
	}


}