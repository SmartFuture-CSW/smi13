<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin Stock Controller
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터 바인딩 처리 및 뷰페이지 호출
  *   # index # userList # history
  *   # getUserList # getUserCount
  *
*/

class Stock extends CI_Controller
{

	function __construct()
	{
		# 생성자
		parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('StockModel');
		$this->load->model('PushModel');
		$this->load->library('Util');
		if(!$this->session->userdata('ULV') && $this->session->userdata('ULV') != 10){
			redirect('admin/login','location');exit;
		}
	}

	public function index(){}

	# 회원목록
	public function stockList()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
		if($bo_table=='') $bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
		if($bo_table=='') $bo_table='all';
		$_POST['vType']=$bo_table;
		$limit = 10;

		# Data bind
		$data['nResult'] = ($this->input->post('nResult')) ? $this->input->post('nResult') : null;
		$data['pageNo'] = $pageNo;
		$data['bo_table'] = $bo_table;
		$data['total'] = $this->StockModel->getStockCount();
		$data['list'] = $this->StockModel->getStockList($pageNo, $data['total'], $limit);
		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);
		$data['level_label'] = $this->config->item('level_label');

		// 투자성과 상태값
		$data['arrStockResult'] = ['1'=>'진행', '2'=>'성공', '3'=>'실패', '4' => '기간종료'];



		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/stock/list', $data);
		$this->load->view('admin/common/footer');
	}

	public function stockEdit()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$bd_no = $this->input->post('bd_no') ? $this->input->post('bd_no'):'';

		# Data bind
		$data['bdname']=array('period'=>'종목 추천','news'=>'뉴스별 종목','theme'=>'테마별 종목');
		$data['pageNo'] = $pageNo;
		$data['write'] = $this->StockModel->getStockwrite($bd_no);
		$data['total'] = $this->StockModel->getStockCount();
		$data['ctzlist'] = $this->StockModel->getStockList(1, $data['total'], 100000);
		$data['bo_table'] = $this->input->post('vType')?$this->input->post('vType'):'';
		if($data['bo_table']=='') $data['bo_table']='theme';
		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/stock/write', $data);
		$this->load->view('admin/common/footer');
	}

	public function bbsUpdate()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$bd_no = $this->input->post('bd_no') ? $this->input->post('bd_no'):'';
		$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
		if($bo_table=='') $bo_table='theme';
		$limit = 10;

		# Data bind
		$data['bdname']=array('period'=>'종목 추천','news'=>'뉴스별 종목','theme'=>'테마별 종목');
		$data['pageNo'] = $pageNo;
		$data['bo_table']=$bo_table;	
		$this->StockModel->setBbsupdate();
		$data['total'] = $this->StockModel->getStockCount();
		$data['list'] = $this->StockModel->getStockList($pageNo, $data['total'], $limit);
		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);
		$data['level_label'] = $this->config->item('level_label');
		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/stock/list', $data);
		$this->load->view('admin/common/footer');
	}

	public function stockDel()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');
		$bo_table = $this->input->post('bo_table')?$this->input->post('bo_table'):'';	
		if($bo_table=='') $bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
		$this->StockModel->delStockData();
		$this->util->alert('삭제되었습니다.','/admin/stock/'.$bo_table);
	}

	public function stockInsert()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';
		if($bo_table=='') $bo_table='theme';
		$data['bo_table'] = $bo_table;

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$data['insert']=$this->StockModel->setStockwrite();

		$emPush = ($this->input->post('emPush')?'1':'0');
		if($emPush == '1'){
			// sendPush($r1 = null, $vType = null, $vEtc2 = null, $txContent = null)
			$data = $this->PushModel->sendPush('all', 'push', '', $this->input->post('txContent'), $this->input->post('vSubject'));
		}

		$this->util->alert('등록되었습니다.','/admin/stock/'.$bo_table);
	}

	public function popMain()
	{
		$no = $this->input->post('mainval') ? $this->input->post('mainval'):'';

		switch($no){
			case '4' : $ea = 6; break;
			case '5' : $ea = 2; break;
			case '6' : $ea = 4; break;
		}

//		$ea=3;
//		if($no==6) $ea=5;
		$data['cont']='';
		if($no!=''){
			$cont=$this->StockModel->getStockMainList($no);
			for($i=1;$i<=$ea;$i++){
				$data['cont'][$i]['vType']='';
				$data['cont'][$i]['bdno']='';
				$data['cont'][$i]['vTypeList']='';
			}
			foreach ($cont as $value) {
				$data['cont'][$value['nSort']]['vType']=$value['vType'];
				$data['cont'][$value['nSort']]['bdno']=$value['nBoardNo'];
				$_POST['vType'] = $value['vType'];
				$data['cont'][$value['nSort']]['vTypeList']=$this->StockModel->getStockList(1,100,30);
			}
		}
		echo json_encode($data);
		exit;
	}

	public function popMain2()
	{
		$no = $this->input->post('vType') ? $this->input->post('vType'):'';
		$data['cont']='';
		if($no!=''){
			$data['cont']=$this->StockModel->getStockList(1,100,30);
		}
		echo json_encode($data);
		exit;
	}

	public function popUpdate()
	{
		$fval = $this->input->post('fval') ? $this->input->post('fval'):'';
		$this->StockModel->setStockMainUpdate();
		$data['code']='1';
		echo json_encode($data);
		exit;
	}

	public function popAutoList()
	{
		$mainval = $this->input->post('mainval') ? $this->input->post('mainval'):'';
		$ea=3;
		if($mainval==6) $ea=5;
		$cont=$this->StockModel->getStockMainAutoList($mainval);
		for($i=1;$i<=$ea;$i++){
			$data['cont'][$i]['vType']='';
			$data['cont'][$i]['bdno']='';
			$data['cont'][$i]['vTypeList']='';
		}
		$n=1;
		foreach ($cont as $value) {
			$data['cont'][$n]['vType']=$value['vType'];
			$data['cont'][$n]['bdno']=$value['nSeqNo'];
			$_POST['vType'] = $value['vType'];
			$data['cont'][$n]['vTypeList']=$this->StockModel->getStockList(1,100,30);
			$n++;
		}
		echo json_encode($data);
		exit;
	}

	public function changeStat(){
		$no = $this->input->post('no') ? $this->input->post('no'):'';
		$stat = $this->input->post('stat') ? $this->input->post('stat'):'';
		$this->StockModel->changeStat($no,$stat);
		$data['result']='success';
		echo json_encode($data);
	}


	public function stoploss(){
		$aRtn = [
			'status' => ''
			, 'msg' => ''
		];
		$nSeqNo = $this->input->post('nSeqNo') ? $this->input->post('nSeqNo') : null;
		if($nSeqNo == null){
			$aRtn['status'] = "FAIL";
			$aRtn['msg'] = "잘못된 접근 입니다.";
		}
		else{
			$data = $this->StockModel->putStoploss($nSeqNo);

			if($data == true){
				$aRtn['status'] = "SUCCESS";
				$aRtn['msg'] = "";
			}else{
				$aRtn['status'] = "FAIL";
				$aRtn['msg'] = "처리 에러 관리자에 문의하세요.";
			}
		}

		echo json_encode($aRtn);
	}

	public function putStockResult(){

		$aRtn = [
			'status' => ''
			, 'msg' => ''
		];

		$nSeqNo = $this->input->post('nSeqNo') ? $this->input->post('nSeqNo') : null;
		$nResult = $this->input->post('nResult') ? $this->input->post('nResult') : null;
		if($nSeqNo == null || $nResult == null){
			$aRtn['status'] = "FAIL";
			$aRtn['msg'] = "잘못된 접근 입니다.";
		}
		else{
			$data = $this->StockModel->putStockResult($nSeqNo, $nResult);

			if($data == true){
				$aRtn['status'] = "SUCCESS";
				$aRtn['msg'] = "";
			}else{
				$aRtn['status'] = "FAIL";
				$aRtn['msg'] = "처리 에러 관리자에 문의하세요.";
			}
		}
	}


}