<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin Board Controller
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터 바인딩 처리 및 뷰페이지 호출
  *   # index # userList # history
  *   # getUserList # getUserCount
  *
*/

class Topboard extends CI_Controller
{

	public $userInfo;


	function __construct() {
		# 생성자
		parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('BoardModel');
		$this->load->model('CategoryModel');
		$this->load->library('Util');
		if(!$this->session->userdata('ULV') && $this->session->userdata('ULV') != 10){
			redirect('admin/login','location');exit;
		}
		// user session info
		$userInfo['anick'] = $this->session->userdata('UNK');
		$userInfo['aname'] = $this->session->userdata('UNM');
		$this->userInfo = $userInfo;
	}

	// 상단 게시물 관리 리스트
	public function index(){
		$vType = ($this->input->post('vType')) ? $this->input->post('vType') : 'today';
		$categoryNo = ($this->input->post('categoryNo')) ? $this->input->post('categoryNo') : 0;
		switch($vType){
			case 'study' :
				$mainNo = [8];
				$data['listArr'] = ['study'];
			break;
			case 'invest' :
				$mainNo = [7];
				$data['listArr'] = ['invest'];
			break;
			case 'important' :
				$mainNo = [3];
				$data['listArr'] = ['important'];
			break;
			case 'today' : 
				$mainNo = [1,2,9,10];
				$data['listArr'] = ['today', 'important', 'invest', 'study'];
			break;
			case 'stock' : 
				$mainNo = [4,5,6];
				$data['listArr'] = ['period', 'news', 'theme'];
			break;
		}
		$data['vType'] = $vType;
		$data['mainNo'] = $mainNo;
		$data['categoryNo'] = $categoryNo;

		$data['category'] = $this->CategoryModel->getBoardCategoryList($vType);

		$bbs = $this->BoardModel->getBoardName();
		for($i=0; $i<count($bbs); $i++){
			$data['boardName'][$bbs[$i]['vType']] = $bbs[$i]['vName'];
		}
		$cont = $this->BoardModel->getBbsListTop($vType, $data['listArr'], $data['categoryNo']);
		foreach($cont as $row){
			$cont[$row['vType']][] = $row;
		}
		$data['cont'] = $cont;
		$data['list'] = $this->BoardModel->getBbsTopBoardList($vType, $mainNo, $categoryNo);


		# view 페이지
		$this->load->view('admin/common/header',$this->userInfo);
		$this->load->view('admin/topboard/list', $data);
		$this->load->view('admin/common/footer');
	}


	// 게시판 카테고리 입력 화면
	public function writeCategory(){
		$data['mode'] = 'post';
		$data['board'] = $this->CategoryModel->getBoardList();
		$data['writer'] = $this->userInfo['aname'];
		$this->load->view('admin/common/header',$this->userInfo);
		$this->load->view('admin/category/write', $data);
		$this->load->view('admin/common/footer');
	}

	// 게시판 카테고리 수정 화면
	public function editCategory(){
		$data['mode'] = 'put';
		$data['board'] = $this->CategoryModel->getBoardList();
		$data['category'] = $this->CategoryModel->getCategoryInfo();
		$data['writer'] = $data['category']['vName'] ? $data['category']['vName'] : $this->userInfo['aname'];
		$this->load->view('admin/common/header',$this->userInfo);
		$this->load->view('admin/category/write', $data);
		$this->load->view('admin/common/footer');
	}

	// 게시판 카테고리 입력
	public function postCategory(){
		$data = $this->CategoryModel->postCategory();
		if($data){
			redirect('admin/category','location');
			exit;
		}
		else{
			$this->util->alert('DB 오류!', '');
			redirect('admin/category','location');
			exit;
		}
	}

	// 게시판 카테고리 삭제
	public function removeCategory(){
		$data = $this->CategoryModel->removeCategory();
		if($data){
			redirect('admin/category','location');
			exit;
		}
		else{
			$this->util->alert('DB 오류!', '');
			redirect('admin/category','location');
			exit;
		}

	}


	// 최신순 자동선택 리스트 출력
	public function getAutoTopList(){
		$vType = $this->input->post("vType");
		$categoryNo = ($this->input->post("categoryNo")) ? $this->input->post("categoryNo") : 0;

		switch($vType){
			case 'study' :
				$listArr = ['study'];
			break;
			case 'invest' :
				$listArr = ['invest'];
			break;
			case 'important' :
				$listArr = ['important'];
			break;
			case 'today' : 
				$listArr = [ 'important', 'invest', 'study'];
			break;
			case 'stock' : 
				$listArr = ['period', 'news', 'theme'];
			break;
		}

		$data = null;

		for($i=0; $i<count($listArr); $i++){
			$list = $this->BoardModel->getBoardAutolist($vType, $categoryNo, $listArr[$i]);
			$s = 1;
			foreach($list as $row){
				$data[$listArr[$i]."_".$s] = $row['nSeqNo'];
				$s++;
			}
		}

		header("Content-Type: application/json;charset=utf-8");
		echo json_encode($data, true);
		exit;

	}

	// 상단 게시물 관리 입력
	public function postTopBoard(){
		
		$data = $this->BoardModel->postTopBoard();
		if($data){
			redirect('admin/topboard','location');
			exit;
		}
		else{
			$this->util->alert('DB 오류!', '');
			redirect('admin/topboard','location');
			exit;
		}

	}
}