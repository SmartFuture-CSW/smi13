<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin Payment Controller
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터 바인딩 처리 및 뷰페이지 호출
  *   # index # paymentList # history
  *   # getPaymentList # getPaymentCount
  *
*/

class Payment extends CI_Controller
{

	function __construct()
	{
		# 생성자
		parent::__construct();
		$this->load->library('Secret');
		$this->load->model('UserModel');
		$this->load->model('PaymentModel');
		$this->load->model('PurchaseModel');
		if(!$this->session->userdata('ULV') && $this->session->userdata('ULV') != 10){
			redirect('admin/login','location');exit;
		}
	}

	public function index(){}


	# 매출관리
	public function paymentList($payType = null) {
		header("Progma:no-cache");
		header("Cache-Control:no-cache,must-revalidate");
		if($payType == "null"){
			$nType = null;
		}else{
			if($payType == "vip"){
				$nType = array(1);
				$data['title']='정기구독';
			}
			else if($payType == "stock"){
				$nType = array(2,3,4,5);
				$data['title']='종목구매';
			}
			else if($payType == "point"){
				$nType = array(6);
				$data['title']='포인트 구매';
			}
		}

		$data['payType'] = $payType;
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$limit = 20;

		$ex = $this->input->post('ex') ? $this->input->post('ex'):'';

		# Data bind
		$data['pageNo'] = $pageNo;
		$data['total'] = $this->PaymentModel->getPaymentCount($nType);
		$data['list'] = $this->PaymentModel->getPaymentList($pageNo, $data['total'], $limit, $nType);

		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);
		$data['level_label'] = $this->config->item('level_label');
		$data['searchDate'] = $this->PaymentModel->getEstiPayment($nType, 'searchDate');
		$data['currentMonth'] = $this->PaymentModel->getEstiPayment($nType, 'currentMonth');
		$data['nextMonth'] = $this->PaymentModel->getEstiPayment($nType, 'nextMonth');

		# view 페이지
		if($ex=='ex'){
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=".iconv("UTF-8","EUC-KR","노다지정기구독_".date("YmdHis")).".xls");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
			header("Pragma: public");

			echo '<table border=1><thead><tr><th style="background-color:silver">번호</th><th style="background-color:silver">이름</th><th style="background-color:silver">전화번호</th><th style="background-color:silver">상품명</th><th style="background-color:silver">상품가</th><th style="background-color:silver">실결제액</th><th style="background-color:silver">사용포인트</th><th style="background-color:silver">결제일</th><th style="background-color:silver">시작일</th><th style="background-color:silver">종료일</th><th style="background-color:silver">주문번호</th><th style="background-color:silver">결제수단</th><th style="background-color:silver">취소여부</th></tr></thead><tbody>';
			foreach ($data['list'] as $value) {
				echo '<tr><td>'.$data['no'].'</td><td>'.$value['vName'].'</td><td>암호화처리됨</td><td>'.$value['vGoodName'].'</td><td>'.number_format($value['nAmount']).'원</td><td>'.number_format($value['nPrice']).'원</td><td>'.number_format($value['nPoint']).'원</td><td>'.$value['dtRegDate'].'</td><td>'.$value['vStartDate'].'</td><td>'.$value['vEndDate'].'</td><td style=\'mso-number-format:"\@";\'>'.$value['vOrderIdx'].'</td><td>';
				if($value['nPrice']==0 && $value['nAmount']==$value['nPoint'] && $value['nPoint'] > 0){
					echo '전액 포인트결제';
				}else if($value['emMethod']=='C'){
					echo '신용카드';
				}else if($value['emMethod']=='M'){
					echo '무통장입금';
				}else if($value['emMethod']=='T'){
					echo '계좌이체';
				}
				echo '</td><td>'.($value['nOrderStatus']=='1'?'취소':($value['nOrderStatus']=='2'?'부분취소':' ')).'</td></tr>';
				$data['no']--;
			}
			if(count($data['list'])==0){
				echo '<tr><td colspan="11" align="center" valign="middle" height="120"><br><br>No Data.</td></tr>';
			}
			echo '</tbody></table>';

		}else{
			$this->load->view('admin/common/header',$default);

			if($data['payType'] == "point"){
				$this->load->view('admin/payment/point', $data);
			}else{
				$this->load->view('admin/payment/list', $data);
			}
			$this->load->view('admin/common/footer');
		}
	}

	public function changestat()
	{
		$no = $this->input->post('cno') ? $this->input->post('cno'):'';
		$agrno = $this->input->post('agrno') ? $this->input->post('agrno'):'';
		$chg=$this->PaymentModel->changeStat($no,$agrno);
		$data['result']='success';
		echo json_encode($data);
	}


	// 결제 실패 리스트
	public function paylog(){
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');
		$data['list'] = $this->PaymentModel->payFailList();
		$data['no'] = count($data['list']);
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/payment/log', $data);
		$this->load->view('admin/common/footer');
	}

	// 결제 실패 리스트
	public function refund(){
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$limit = 20;

		$data['pageNo'] = $pageNo;
		$data['total'] = $this->PaymentModel->getRefundCount();
		$data['list'] = $this->PaymentModel->payRefundList($pageNo, $data['total'], $limit);
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);
		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);

		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/payment/refund', $data);
		$this->load->view('admin/common/footer');

	}

	// 자동결제 처리상태 수정
	public function putLogState(){
		$where = $this->input->post('nSeqNo');
		$set = [
			'nState' => $this->input->post('nState')
		];
		$data = $this->PaymentModel->payFailState($where, $set);
		echo json_encode($data);
	}

	public function view($payNo){
		
		
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		$data['view'] = $this->PaymentModel->getDetailPayment($payNo);


		if($data['view']['nPrice']==0 && $data['view']['nAmount'] == $data['view']['nPrice'] && $data['view']['nPoint'] > 0){
			$data['view']['emMethod'] = 'P';
		}

		if($data['view']['nOrderStatus'] == "1"){
			$data['view']['bgcolor'] = "#ffeaea";
		}else if($data['view']['nOrderStatus'] == "2"){
			$data['view']['bgcolor'] = "yellow";
		}else{
			$data['view']['bgcolor'] = "";
		}


		$data['method'] = array('C'=>'신용카드', 'M'=>'무통장입금', 'T'=>'계좌이체', 'P'=>'포인트결제');
		$data['orderStatus'] = array('0'=>'결제완료', '1'=>'전체취소', '2'=>'부분취소', "3"=>"취소신청");


		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/payment/view', $data);
		$this->load->view('admin/common/footer');

	}


	public function postRefundInfo(){
		$rtn = array(
			"result" => "FAIL",
			"msg" => "",
			"data" => ""
		);
		$data = $this->PurchaseModel->postRefundInfo();
		$rtn['data'] = $data;
		header('Content-Type: application/json;charset=utf-8');
		print json_encode($rtn, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
	}
}