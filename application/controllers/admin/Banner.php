<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin Banner Controller
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터 바인딩 처리 및 뷰페이지 호출
  *   # index # userList # history
  *   # getUserList # getUserCount
  *
*/

class Banner extends CI_Controller
{

	function __construct()
	{
		# 생성자
		parent::__construct();
		$this->load->model('BannerModel');
		if(!$this->session->userdata('ULV') && $this->session->userdata('ULV') != 10){
			redirect('admin/login','location');exit;
		}
	}

	public function index(){}

	# 회원목록
	public function bannerList()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$limit = 10;

		# Data bind
		$data['vType_arr']=array('app_start'=>'app시작시','app_end'=>'app종료시','today'=>'TODAY','important'=>'핵심정보','news'=>'뉴스','community'=>'커뮤니티','recommand'=>'종목추천','subscript'=>'구독','store'=>'스토어', 'login'=>'인증/로그인시');
		$data['pageNo'] = $pageNo;
		$data['total'] = $this->BannerModel->getBannerCount();
		$data['list'] = $this->BannerModel->getBannerList($pageNo, $data['total'], $limit);
		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);
		$data['level_label'] = $this->config->item('level_label');

		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/banner/list', $data);
		$this->load->view('admin/common/footer');
	}

	public function bannerEdit()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$bd_no = $this->input->post('bd_no') ? $this->input->post('bd_no'):'';
		$data['pageNo'] = $pageNo;
		$data['write'] = $this->BannerModel->getBannerwrite($bd_no);

		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/banner/write', $data);
		$this->load->view('admin/common/footer');
	}

	public function bannerInsert()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');
		$this->BannerModel->setBannerwrite();
		$this->util->alert('등록되었습니다.','/admin/banner/');
	}

	public function bannerDel()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');
		$this->BannerModel->delBannerData();
		$this->bannerList();
	}

	public function updateField(){
		$no = $this->input->post('no') ? $this->input->post('no'):'';
		$field = $this->input->post('field') ? $this->input->post('field'):'';
		$fval = $this->input->post('fval') ? $this->input->post('fval'):'';
		$this->BannerModel->updateField($no,$field,$fval);
		$data['result']='success';
		echo json_encode($data);
	}
}