<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin Push Controller
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터 바인딩 처리 및 뷰페이지 호출
  *   # index # userList # history
  *
*/

class Push extends CI_Controller
{

	function __construct()
	{
		# 생성자
		parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('PushModel');
		$this->load->library('Util');
		if(!$this->session->userdata('ULV') && $this->session->userdata('ULV') != 10){
			redirect('admin/login','location');exit;
		}
	}

	public function index(){}

	# 회원목록
	public function pushList()
	{
		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');

		# 페이징설정
		$pageNo = $this->input->post('pageNo') ? $this->input->post('pageNo'):1;
		$limit = $this->input->post('limit') ? $this->input->post('limit'):'10';
		$reg1 = $this->input->post('reg1') ? $this->input->post('reg1'):'';
		$reg2 = $this->input->post('reg2') ? $this->input->post('reg2'):'';

		# Data bind
		$data['total'] = $this->PushModel->getPushCount();
		$data['list'] = $this->PushModel->getPushList($pageNo, $data['total'], $limit);
		$data['paging'] = $this->paging->admin_pagination($limit, $pageNo, $data['total']);
		$data['limit']=$limit;
		$data['reg1']=$reg1;
		$data['reg2']=$reg2;
		$data['pageNo'] = $pageNo;
		$data['no'] = $data['total'] - (($pageNo - 1) * $limit);
		# view 페이지
		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/push/list', $data);
		$this->load->view('admin/common/footer');
	}

	public function pushWrite()
	{

		$default['anick']=$this->session->userdata('UNK');
		$default['aname']=$this->session->userdata('UNM');


		

//		$data['mcount']=$this->PushModel->getUserList("count");

		$data['mcount'] = $this->PushModel->getPushReceiveList();
	
		


		$this->load->view('admin/common/header',$default);
		$this->load->view('admin/push/write', $data);
		$this->load->view('admin/common/footer');
	}

	public function pushInsert() {

		$data['insert']=$this->PushModel->sendPushAndroid();
		$data['code']='1';
		echo json_encode($data);

	}

	public function popMain()
	{
		$price=$this->input->post('price') ? $this->input->post('price'):'';
		if($price!=''){
			$price=$price*10000;
			$jdata=$this->sms->smsSend('1', '1', '1', '1', 'http://www.sendmon.com/_REST/settleApi.asp','&PayWay=ONLINE&SettleCash='.urlencode($price).'&SettlePrice='.urlencode($price));
			$data['code']='1';
		}else{
			$jdata=$this->sms->smsSend('1', '1', '1', '1', 'http://www.sendmon.com/_REST/settleListAsp.asp');
			$data['list']=json_decode($jdata,true);
		}
		echo json_encode($data);
	}

	public function popDetail()
	{
		$uniq_id=$this->input->post('uniq_id') ? $this->input->post('uniq_id'):'';
		$used_cd=$this->input->post('used_cd') ? $this->input->post('used_cd'):'';
		$data["detail"] = "";
		if($uniq_id!='' && $used_cd!=''){
			$history = $this->sms->smsSend('010','detail',$uniq_id,'a','http://www.sendmon.com/_REST/smsApi.asp','&room_idx='.urlencode($used_cd),'history');
			$history = preg_replace("/[\r\n]+/", " ", $history);
			$history_result = json_decode($history);

			$history_list = $history_result->result;
			$history_list2 = isset($history_result->result2)?$history_result->result2:null;
			$data["detail"] = $history_list2!=null?array_merge($history_list,$history_list2):$history_list;
			$data['code']='1';
		}else{
			$data['code']='2';
			$data['msg']="잘못된 접근입니다.";
		}
		echo json_encode($data);
	}
}