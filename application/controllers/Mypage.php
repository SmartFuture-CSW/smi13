<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mypage extends CI_Controller {
	// 생성자
	public function __construct() {
		parent::__construct();
		$this->load->model('User');
		$this->load->model('Payment');
		$this->load->model('Site');
		$this->load->model('Board');
		$this->load->library('Secret');
		$this->load->model('PurchaseModel');
		$this->load->model('Point_model');
		// 로그인 상태체크

		// login cookie 세션 동기화
		if(!empty(get_cookie('UNO')) && !$this->session->userdata('UNO')) {
			$sData = [
				'UNO'  => get_cookie('UNO'),
				'UID'  => get_cookie('UID'),
				'UNK'  => get_cookie('UNK'),
				'UNM'  => get_cookie('UNM')
			];		
			$this->session->set_userdata($sData);
		}

		if (!$this->session->userdata('UNO') && $this->uri->segment(2)!='terms_01' && $this->uri->segment(2)!='terms_02' && $this->uri->segment(2)!='terms_03') {
			$this->util->alert('로그인 후 이용가능합니다.', '/auth');
		}
	}


	// 공지사항
	public function notice() {
		$data['notice'] = $this->Board->getBoard(['vType' => 'notice']);
		$data['banner']    = $this->Board->getBanner(['emKind' => 'band']);
		$this->response->View('mypage/notice', $data);
	}


	// 게시판 뷰페이지
	public function view($boardNo) {

		$type = 'notice';

		if (! $boardNo) {
			$this->util->alert('일시적인 오류입니다.', '/info');
		}

		$data['type'] = $type;

		// 조회수 CHECK
		if (! isset($_COOKIE["view_{$type}_{$boardNo}"])) {
			$result = $this->Board->setBoardHit($boardNo);
			setcookie("view_{$type}_{$boardNo}", true, time() + 86400, '/boardView'); // time() + 86400 = 쿠키 유효 시간 설정해줌(하루)
		}

		$data['view'] = $this->Board->getBoardView($boardNo);

		$data['banner']    = $this->Board->getBanner(['emKind' => 'band']);
		$this->response->View('mypage/view', $data);
	}



	public function index() {
		$data['user'] = $this->User->getUser(['nSeqNo' => $this->session->userdata('UNO')]);
		// vip, 일반회원 인지 체크 
		$where = [
			'nUserNo =' => $this->session->userdata('UNO'),
			'nType =' => 1,
			'nOrderStatus =' => '0',
			'vEndDate >=' => date('Y-m-d'),
		];
		$data['isVip'] = $this->Payment->getPayment($where, 'P.nSeqNo');
		$data['banner'] = $this->Board->getBanner(['emKind' => 'band']);

		$data['point'] = $this->Point_model->getUserPoint(['nUserNo'=> $this->session->userdata('UNO')]);

		$this->response->View('mypage/index', $data);
	}

    public function profile()
    {
        $data['user'] = $this->User->getUser(['nSeqNo' => $this->session->userdata('UNO')]);
        $data['banner'] = $this->Board->getBanner(['emKind' => 'band']);
        $this->response->View('mypage/profil', $data);
    }

	public function subscription() {
		// DB 조회
		$where = ['P.nUserNo =' => $this->session->userdata('UNO'), 'nType =' => 1 ];
		$select = ["P.nSeqNo, P.vStartDate, P.vEndDate, P.nOrderStatus, CONCAT('[', G.vGoodsKindName,']', ' ', P.vGoodName) as vGoodName, R.nSeqNo AS refundNo"];
		$data['list'] = $this->Payment->getPaymentRefund($where, $select);
		$data['banner'] = $this->Board->getBanner(['emKind' => 'band']);
		//print_r($data);exit;
		$this->response->View('mypage/subscription', $data);
	}


	public function subscription_test() {
		// DB 조회
		$where = ['P.nUserNo =' => $this->session->userdata('UNO'), 'nType =' => 1 ];
		$select = ["P.nSeqNo, P.vStartDate, P.vEndDate, P.nOrderStatus, CONCAT('[', G.vGoodsKindName,']', ' ', P.vGoodName) as vGoodName, R.nSeqNo AS refundNo"];
		$data['list'] = $this->Payment->getPaymentRefund($where, $select);
		$data['banner'] = $this->Board->getBanner(['emKind' => 'band']);
		//print_r($data);exit;

		$this->response->View('mypage/subscription_test', $data);
	}


    public function purchase()
    {
        // DB 조회
        $data['list'] = $this->Payment->getPaymentMyPage(['P.nUserNo =' => $this->session->userdata('UNO'), 'P.nType !=' => 1 ]);
        $data['banner'] = $this->Board->getBanner(['emKind' => 'band']);
        $this->response->View('mypage/purchase', $data);
    }

    public function point2()
    {
        // DB 조회
        $data['list'] = $this->Payment->getPointList(['P.nUserNo =' => $this->session->userdata('UNO')]);
        $data['banner'] = $this->Board->getBanner(['emKind' => 'band']);
        $this->response->View('mypage/point', $data);
    }

    public function paymentInfo()
    {
        $result = $this->Payment->getPaymentInfo(['nUserNo' => $this->session->userdata('UNO')]);

        if (empty($result)) {
            redirect('/store/paymentInfo'); // 결제 등록폼 이동 
        }
        $data['pay'] = $result;
        $this->response->View('mypage/paymentInfo', $data);
    }
/*
    public function notice()
    {
        $this->response->View('mypage/notice');
    }
*/
    public function passwordChange()
    {
        $data['banner'] = $this->Board->getBanner(['emKind' => 'band']);
        $this->response->View('mypage/passwordChange', $data);
    }

    public function terms_01()
    {
        $data['terms'] = $this->Site->getSiteInfo(['nSeqNo' => 1]);
        $this->response->View('mypage/terms_01', $data);
    }

    public function terms_02()
    {
        $data['terms'] = $this->Site->getSiteInfo(['nSeqNo' => 1]);
        $this->response->View('mypage/terms_02', $data);
    }

    public function terms_03()
    {
		/*
		echo '<pre>';
		print_r($_SESSION);
		exit;
		*/

        $data['terms'] = $this->Site->getSiteInfo(['nSeqNo' => 1]);
        $this->response->View('mypage/terms_03', $data);
    }

    public function modifyPasswd()
    {
        // form check
        $this->load->library('form_validation');
        $this->form_validation->set_rules('passwd', '현재패스워드', FV_PWD);
        $this->form_validation->set_rules('passwd_new', '신규패스워드', FV_PWD);
        $this->form_validation->set_rules('passwd_new_confirm', '신규패스워드확인', FV_PWD);

        // form error
        if ($this->form_validation->run() == false) {
            $this->response->Json('error', '패스워드를 확인해주세요.', '', '');
            return false;
        }

        $passwd             = $this->input->post('passwd');
        $passwd_new         = $this->input->post('passwd_new');
        $passwd_new_confirm = $this->input->post('passwd_new_confirm');
        $userNo             = $this->session->userdata('UNO');

        if ($passwd == $passwd_new) {
            $this->response->Json('error', '기존 패스워드와 같습니다.', '', '');
            return false;
        }
        if ($passwd_new != $passwd_new_confirm) {
            $this->response->Json('error', '비밀번호 확인과 일치하지 않습니다.', '', '');
            return false;
        }

        // db 처리
        $set   = ['vPwd' => $this->secret->aes_encrypt($passwd_new)];
        $where = ['nSeqNo =' => $userNo];
        $data  = $this->User->setUser($set, $where);

        if (isset($data)) {
            $this->response->Json('success', '비밀번호 변경이 완료되었습니다.', '/mypage', '');
            return false;
        } else {
            $this->response->Json('error', ERROR_07, '', '');
            return false;
        }
    }

    public function modifyNick()
    {
        // form check
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nick', '아이디', FV_NICK);

        // form error
        if ($this->form_validation->run() == false) {
            $this->response->Json('error', '10글자이내 영문/한글만 가능합니다.', '', '');
            return false;
        }

        $nick   = $this->input->post('nick');
        $userNo = $this->session->userdata('UNO');

        // 중복된 닉 검사
        $where  = ['vNick =' => $nick];
        $isNick = $this->User->getUser($where, 'vNick');

        if (isset($isNick)) {
            $this->response->Json('error', '이미 사용 중인 닉네임 입니다. 다시설정해 주세요.', '', '');
            return false;
        } else {
            // db 처리
            $set   = ['vNick' => $nick];
            $where = ['nSeqNo =' => $userNo];
            $data  = $this->User->setUser($set, $where);

            if (isset($data)) {
                $this->session->set_userdata('UNK', $nick);
                $this->response->Json('success', '닉네임이 수정되었습니다.', '/mypage/profile', '');
                return false;
            } else {
                $this->response->Json('error', ERROR_11, '', '');
                return false;
            }
        }
    }

    public function modifyPaymentInfo()
    {
        // form check
        $this->load->library('form_validation');
        $this->form_validation->set_rules('bank', '은행', FV_BANK);
        $this->form_validation->set_rules('cardNo1', '카드번호', FV_CARD);
        $this->form_validation->set_rules('cardNo2', '카드번호', FV_CARD);
        $this->form_validation->set_rules('cardNo3', '카드번호', FV_CARD);
        $this->form_validation->set_rules('cardNo4', '카드번호', FV_CARD);
        $this->form_validation->set_rules('cardDate', '만료날자', FV_CARD);
        $this->form_validation->set_rules('birth', '생년월일', FV_BIRTH);

        // form error
        if ($this->form_validation->run() == false) {
            $this->util->alert('폼체크 아웃', '');
        }

        $setData = [
            'nUserNo'    => $this->session->userdata('UNO'),
            'vBank'      => $this->input->post('bank'),
            'vCardNo_01' => $this->input->post('cardNo1'),
            'vCardNo_02' => $this->input->post('cardNo2'),
            'vCardNo_03' => $this->input->post('cardNo3'),
            'vCardNo_04' => $this->input->post('cardNo4'),
            'vDate'      => $this->input->post('cardDate'),
            'vBirth'     => $this->input->post('birth'),
        ];

        $result = $this->Payment->setPaymentInfo($setData, ['nUserNo' => $this->session->userdata('UNO')]);

        if ($result) {
            $this->util->alert('정상적으로 수정되었습니다.', '/mypage');
        } else {
            $this->util->alert('DB error', '');
        }
    }

    public function imgFileUpload()
    {
        if (isset($_FILES)) {
            $name = $_FILES['userImg']['name'];
            $type = $_FILES['userImg']['type'];
            $size = $_FILES['userImg']['size'];
            $tmp  = $_FILES['userImg']['tmp_name'];
            $code = $_FILES['userImg']['error'];

            // 업로드 경로
            $upDir  = $_SERVER['DOCUMENT_ROOT'] . '/data/user/profile/';
            $userNo = $this->session->userdata('UNO');

            // 파일명
            $fileName = 'uno_' . $userNo . '_' . date('Ymd') . '_' . $name;

            // 사이즈 체크
            if ($size > 5000000) {
                $this->response->Json('success', '사이즈범위초과', '', $_FILES);
                exit;
            }

            // 업로드성공
            if (! move_uploaded_file($tmp, $upDir . basename($fileName))) {
                $this->response->Json('success', '파일업로드실패', '', $_FILES);
                exit;
            } else {
                // 유저테이블에 파일명 업데이트
                $set   = ['vImage' => $fileName];
                $where = ['nSeqNo =' => $userNo];
                $data  = $this->User->setUser($set, $where);
                // tmp 파일 삭제
                @unlink($_FILES['userImg']['tmp_name']);

                $this->response->Json('success', '프로필이 저장되었습니다.', '', $_FILES);
                exit;
            }
        } else {
            $this->response->Json('error', '파일전송에러');
        }
    }


	/**
	* @breif: 마이페이지 구독 내역 리스트
	* @author: csw
	*/
	function getPaymentList() {
		$where = ['nUserNo =' => $this->session->userdata('UNO'), 'nType =' => 1 ];
		$select = ["P.nSeqNo, P.vStartDate, P.vEndDate, P.nOrderStatus, CONCAT('[', G.vGoodsKindName,']', ' ', P.vGoodName) as vGoodName"];
		$result = $this->Payment->getPayment($where, $select);
		if($result) $this->response->Json('success', 'list ok', '', $result);
		else $this->response->Json('error', 'DB 에러', '', '');
		
	}




	function modifyPush() {

		$push   = $this->input->post('push');
		$userNo = $this->session->userdata('UNO');
		if(empty($push)){ 
			$this->response->Json('error', '값이 없습니다.');
		}
		if(empty($userNo)){ 
			$this->response->Json('error', '로그인 후 이용가능합니다.');
		}
		// 
		$pushVal = ( $push == 'true' ) ? '1':'0';
		// db 처리
		$set   = ['vPushOs' => $pushVal];
		$where = ['nSeqNo =' => $userNo];
		$data  = $this->User->setUser($set, $where);
		if($data){
			$this->response->Json('success', '', '', $pushVal);
		}else{
			$this->response->Json('error', '처리중 에러', '', '');
		}
		
	}

	// 마이페이지 > 스크랩 콘텐츠 확인 리스트
	public function scrap(){
		$data['scrap'] = $this->User->getUserScrap();
		$this->response->View('mypage/scrap', $data);
	}



	// 마이페잊 통합 포인트 조회
	public function point() {
		// DB 조회
		$data["arrPointKind"] = ["1"=>"적립", "2"=>"사용"];
		$pointKind = $this->input->post("pointKind");
		if($pointKind != null){
			$where = ['P.nUserNo =' => $this->session->userdata('UNO'), "P.nPointKind" => $pointKind];
		}
		else{
			$where = ['P.nUserNo =' => $this->session->userdata('UNO')];
		}
		$data['pointKind'] = $pointKind;
		$data['list'] = $this->Payment->getPointListDetail($where);
		$data['banner'] = $this->Board->getBanner(['emKind' => 'band']);
		$this->response->View('mypage/point2', $data);
	}

	
	/**
	* @brief: 취소 요청시 환불 금액 영수증 마이페이지
	* @author: csw
	*/
	public function getRefundReceipt(){
		$nPayNo = $this->input->post("nPayNo");
		$nUserNo = $this->session->userdata('UNO');
		$data = $this->PurchaseModel->getRefundReceipt($nPayNo, $nUserNo);
		header("Content-Type: application/json;charset=utf-8");
		echo json_encode($data, true);
	}

	/**
	* @brief: 취소신청 처리
	* @detail: 사용자의 결제 정보만큼 포인트 차감, 이용일수 초기화
	* @author: csw
	*/
	function postRefundData() {
		$idx    = $this->input->post('idx');
		$userNo = $this->session->userdata('UNO');

		if(empty($idx)){
			$this->response->Json('error', '값이 없습니다.');
		}
		else{
			$where = ['nPayNo' => $idx, 'nUserNo' => $userNo];
			$result  = $this->PurchaseModel->postRefundData($where); 
			if($result){
				$this->response->Json('success', '해지 완료', '', '');
			}
			else{
				$this->response->Json('error', 'DB처리 에러', '', '');
			}
		}
	}

}
