<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Landing extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('Secret');
		$this->load->model('App_model');
		$this->load->model('StockModel');
		$this->load->model('PurchaseModel');
		$this->load->model('Site');
	}

	public function index() {
	}
	public function Ymkt(){
		$ld_no=$this->uri->segment(3);
		$data['adref']=$this->uri->segment(4);
		$data['terms'] = $this->Site->getSiteInfo(['nSeqNo' => 1]);
		if($ld_no==''){
			return;exit;
		}
		$_GET['adref']=$data['adref']!=''?$data['adref']:'';
		$this->load->view('include/header');
		$this->load->view('landing/'.$ld_no,$data);
		$this->load->view('include/footer');
	}
}