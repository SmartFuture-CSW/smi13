<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 1. 공개정보 Controller
 *
 * @author 임정원 / 2020-02-14
 *
 * @since  Version 1.0.0
 */
class Info extends CI_Controller
{
	// 생성자
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Secret');
		// 게시판 -> DB 처리
		$this->load->model('Board');
		$this->load->model('Share');
		$this->load->model('User');
		$this->load->model('Scrap');
		$this->load->helper('cookie');
//		$this->load->helper('usi');


		// login cookie 세션 동기화
		if(!empty(get_cookie('UNO')) && !$this->session->userdata('UNO')) {
			$sData = [
				'UNO'  => get_cookie('UNO'),
				'UID'  => get_cookie('UID'),
				'UNK'  => get_cookie('UNK'),
				'UNM'  => get_cookie('UNM')
			];		
			$this->session->set_userdata($sData);
		}

		// $this->setDeviceKey();

	}

	public function visitlog(){
		$set   = [
			'vPushKey' => $this->session->userdata('deviceKey')
			, 'nUserNo' => $this->session->userdata('UNO')
		];
		$data  = $this->User->setDeviceKey($set);
	}

	public function index()
	{
		redirect('/info/today');
	}


	// 공개정보 메인 페이지
	public function today() {

		/**
		* @breif: 해당 컨트롤러 수정시 main/index 부분도 동일하게 수정해야 함
		* @author: csw
		*/
		$data['page'] = 'list';
		$data['today']     = $this->Board->getBoardTodayNew();
		$data['category'] = $this->uri->segment(3) ? urldecode($this->uri->segment(3)) : 'date';

		switch($data['category']){
			case 'date' : $data['order'] = '최신순'; break;
			case 'hit' : $data['order'] = '조회수순'; break;
			case 'share' : $data['order'] = '공유순'; break;
			case 'reply' : $data['order'] = '댓글순'; break;
			case 'rec' : $data['order'] = '추천순'; break;
			default  : $data['order'] = '최신순'; break;
		}


		$data['recommend'] = $this->Board->getBoardMain(['vMainVal' => 2]);
		$data['invest'] = $this->Board->getBoardMain(['vMainVal' => 9]);
		$data['study'] = $this->Board->getBoardMain(['vMainVal' => 10]);
//		$data['community'] = $this->Board->getBoard_User(['b.vType' => 'community']);
		$data['community'] = $this->Board->getBoardCommunity(['vType' => 'community'], 5);
		$data['etc']       = $this->Board->getBoardAny();
		$data['banner']    = $this->Board->getBanner(['emKind' => 'band']);
		// 팝업 세팅 
		$popup = $this->_popupCheck('app_start');
		if($popup) $data['popup'] = $popup;
		$this->response->View('info/list_today', $data);
	}




	// response json
	public function todaylist(){


		$full = $this->input->post('full');

		if($full == 'on'){
			$limit = '0';
			$offset = '15';
		}
		else if($full == 'half'){
			$limit = '5';
			$offset = '10';
		}
		else{
			$limit = '0';
			$offset = '5';
		}

		$data     = $this->Board->getBoardToday($limit,$offset);

		header("Content-Type: application/json;charset=utf-8");
		echo json_encode($data);
		exit;
	}

	// 핵심정보 페이지 @ 명칭변경 : 추천종목
	public function important($nCategoryNo = 17) {

		$data['page'] = 'list';
		$data['important'] = $this->Board->getBoardMain(['vMainVal' => 3, 'b.nCategoryNo' => $nCategoryNo]);
		$data['nCategoryNo'] = $nCategoryNo;

		if($nCategoryNo == 0){
			$where = ['A.vType' => 'important'];
		}else{
			$where = ['A.vType' => 'important', 'A.nCategoryNo' => $nCategoryNo];
		}


		//$data['etc']       = $this->Board->getBoardAny();
		// 인피니티 스크롤로 출력될 컨텐츠. (메인노출 컨텐츠는 포함되지 않음) 처음 호출시 1page 내용 불러옴
		$data['infinity_page'] = 1;
		$data['mainVal'] = 7;
		$data['infinity']  = $this->Board->getBoardInfinity($where, $data['infinity_page'], $data['mainVal']);
		$data['banner']    = $this->Board->getBanner(['emKind' => 'band']);
		$data['category'] = $this->Board->getCategory('important');

		
		// 팝업 세팅 
		$popup = $this->_popupCheck('important');
		if($popup) $data['popup'] = $popup;

		$this->response->View('info/list_important', $data);
	}

	/**
	* @breif : 공개정보 > 카테고리 하단 무한스크롤 게시판 데이터 출력
	* @author : csw
	*/
	public function get_infinity_data()
	{
		$data = null;
		$type = $this->input->post('type');
		$infinity_page = $this->input->post('page');

		$mainVal = $this->input->post('mainVal');
		$category = $this->input->post('category');

		if($category == 0){
			$where = ['A.vType' => $type];
		}else{
			$where = ['A.vType' => $type, 'A.nCategoryNo' => $category];
		}

		$list = $this->Board->getBoardInfinity($where, $infinity_page, $mainVal);

		if($list){
			$data = array(
				"status" => "SUCCESS", "list" => $list
			);
		} else {
			$data = array(
				"status" => "FAIL"
			);
		}
		header("Content-Type: application/json;charset=utf-8");
		echo json_encode($data, true);
		exit;
	}

	// 투자정보 페이지
	public function invest($nCategoryNo = 0) {

		$data['page'] = 'list';
		$data['invest'] = $this->Board->getBoardMain(['vMainVal' => 7, 'b.nCategoryNo' => $nCategoryNo]);
		$data['nCategoryNo'] = $nCategoryNo;


		if($nCategoryNo == 0){
			$where = ['A.vType' => 'invest'];
		}else{
			$where = ['A.vType' => 'invest', 'A.nCategoryNo' => $nCategoryNo];
		}

		//$data['etc']       = $this->Board->getBoardAny();
		// 인피니티 스크롤로 출력될 컨텐츠. (메인노출 컨텐츠는 포함되지 않음) 처음 호출시 1page 내용 불러옴
		$data['infinity_page'] = 1;
		$data['mainVal'] = 7;
		$data['infinity']  = $this->Board->getBoardInfinity($where, $data['infinity_page'], $data['mainVal']);
		$data['banner']    = $this->Board->getBanner(['emKind' => 'band']);
		$data['category'] = $this->Board->getCategory('invest');

		// 팝업 세팅 
		$popup = $this->_popupCheck('invest');
		if($popup) $data['popup'] = $popup;
		$this->response->View('info/list_invest', $data);
	}


	// 주식공부 페이지
	public function study($nCategoryNo = 0) {
		$data['page'] = 'list';
		$data['study'] = $this->Board->getBoardMain(['vMainVal' => 8, 'b.nCategoryNo' => $nCategoryNo]);
		$data['nCategoryNo'] = $nCategoryNo;


		if($nCategoryNo == 0){
			$where = ['A.vType' => 'study'];
		}else{
			$where = ['A.vType' => 'study', 'A.nCategoryNo' => $nCategoryNo];
		}

		//$data['etc']       = $this->Board->getBoardAny();
		// 인피니티 스크롤로 출력될 컨텐츠. (메인노출 컨텐츠는 포함되지 않음) 처음 호출시 1page 내용 불러옴
		$data['infinity_page'] = 1;
		$data['mainVal'] = 8;
		$data['infinity']  = $this->Board->getBoardInfinity($where, $data['infinity_page'], $data['mainVal']);
		$data['banner']    = $this->Board->getBanner(['emKind' => 'band']);
		$data['category'] = $this->Board->getCategory('study');
		
		// 팝업 세팅 
		$popup = $this->_popupCheck('study');
		if($popup) $data['popup'] = $popup;
		$this->response->View('info/list_study', $data);
	}


    // 뉴스 페이지
    public function news()
    {
        $data['news']   = $this->Board->getBoard(['vType' => 'news']);

		$data['banner']    = $this->Board->getBanner(['emKind' => 'band']);
        
        // 팝업 세팅 
        $popup = $this->_popupCheck('news');
        if($popup) $data['popup'] = $popup;

        $this->response->View('info/list_news', $data);
    }




	// 커뮤니티 페이지
	public function community($nCategoryNo = 0) {
		$data['page'] = 'list';
		$data['category'] = $this->Board->getCategory('community');


		if($nCategoryNo == 0){
			$where = ['vType' => 'community'];
		}
		else{
			$where = ['vType' => 'community', 'nCategoryNo' => $nCategoryNo];
		}

		$data['community'] = $this->Board->getBoardCommunity($where);
		$data['best'] = $this->Board->getCommunityBest();
		$data['nCategoryNo'] = $nCategoryNo;


		// 팝업 세팅 
		$popup = $this->_popupCheck('community');
		if($popup) $data['popup'] = $popup;

		$this->response->View('info/list_community', $data);
	}



	// 커뮤니티 페이지
	public function community_test($nCategoryNo = 0) {
		$data['page'] = 'list';
		$data['category'] = $this->Board->getCategory('community');



		if($nCategoryNo == 0){
			$where = ['vType' => 'community'];
		}
		else{
			$where = ['vType' => 'community', 'nCategoryNo' => $nCategoryNo];
		}

		$data['community'] = $this->Board->getBoardCommunity($where);
		$data['best'] = $this->Board->getCommunityBest();
		$data['nCategoryNo'] = $nCategoryNo;


		// 팝업 세팅 
		$popup = $this->_popupCheck('community');
		if($popup) $data['popup'] = $popup;

		$this->response->View('info/list_community_test', $data);
	}




	// 커뮤니티 글쓰기 페이지
	public function communityReg($boardNo = '') {
		if (! $this->session->userdata('UNO')) {
			$this->util->alert('로그인 후 이용가능합니다.', '/auth');
		}
		$category = $this->Board->getCategory('community');
		$i = 0;
		foreach($category as $row){
			if($row['nSeqNo'] == 0 || $row['nSeqNo'] == 12){
				continue;
			}
			$data['category'][$i] = $row;
			$i++;
		}
		if (empty($boardNo)) {
			// 작성
			$this->response->View('info/reg_community', $data);
		}
		else {
			// 수정
			$data['view'] = $this->Board->getBoard(['nSeqNo' => $boardNo, 'vType' => 'community']);

			if (! $data['view']) {
				$this->util->alert('비정상적인 접근입니다.', '/community');
			}
			//print_r($data);exit;
			$this->response->View('info/reg_community', $data);
		}
	}

    // 공지사항
    public function notice()
    {
        $data['notice'] = $this->Board->getBoard(['vType' => 'notice']);

		$data['banner']    = $this->Board->getBanner(['emKind' => 'band']);

        $this->response->View('info/list_notice', $data);
    }

    // 게시판 뷰페이지
	public function boardView($type, $boardNo) {

		if (! $boardNo || ! $type) {
			$this->util->alert('일시적인 오류입니다.', '/info');
		}




		$data['type'] = $type;

		// 조회수 CHECK
		if (! isset($_COOKIE["view_{$type}_{$boardNo}"])) {
			$result = $this->Board->setBoardHit($boardNo);
			setcookie("view_{$type}_{$boardNo}", true, time() + 86400, '/boardView'); // time() + 86400 = 쿠키 유효 시간 설정해줌(하루)
		}

		// 투데이
		$data['etc1'] = $this->Board->getInfoAny(['vType' => 'important']);
		$data['etc2'] = $this->Board->getInfoAny(['vType' => 'invest']);
		$data['etc3'] = $this->Board->getInfoAny(['vType' => 'study']);

		// 게시글 가져오기
		if ($type == 'community') {
			$data['view'] = $this->Board->getBoardView_User($boardNo);
			

			if($this->session->userdata('UNO') != 1){
				if( $data['view']['nUserNo'] != $this->session->userdata('UNO') && $data['view']['nCategoryNo'] != "12") { 
					$this->util->alert('작성자 본인만 열람 가능합니다.', '/info/community');
					exit;
				}
			}

		} else {
			$data['view'] = $this->Board->getBoardView($boardNo);
		}




		if($data['view']['vType'] == "key"){
			exit;
		}


		// 댓글 수 가져오기
		$data['countReply'] = $this->Board->getReplyCount($boardNo);

		// 좋아요 체크
		$data['isScrap'] = $this->Scrap->getIsScrap($boardNo, $type);







		// view 페이지 호출
		switch ($type) {
			case 'community':
				$this->load->view('include/header_view', $data);
				$this->load->view('info/view_community', $data);
				$this->load->view('include/footer');
			break;
			default:
				// 추천 컨텐츠 
				if($data['view']['vRecom1'] != 0) $data['recom1'] = $this->Board->getBoardView($data['view']['vRecom1']);
				if($data['view']['vRecom2'] != 0) $data['recom2'] = $this->Board->getBoardView($data['view']['vRecom2']);
				if($data['view']['vRecom3'] != 0) $data['recom3'] = $this->Board->getBoardView($data['view']['vRecom3']);
				if($data['view']['vRecom4'] != 0) $data['recom4'] = $this->Board->getBoardView($data['view']['vRecom4']);

				$data['banner']    = $this->Board->getBanner(['emKind' => 'band']);
				$data['popup']  = $this->Board->getBannerPopup(['emKind' => 'wide', 'vType' => 'view']);

				$this->load->view('include/header_view', $data);
				$this->load->view('info/view', $data);
				$this->load->view('include/footer');
			break;
		}
	}


	// 게시판 뷰페이지
	public function key($boardNo) {

		if (! $boardNo) {
			$this->util->alert('일시적인 오류입니다.', '/info');
		}
		$boardNo = $this->secret->decrypt256($boardNo);

		$type = 'key';
		$data['type'] = $type;

		// 조회수 CHECK
		if (! isset($_COOKIE["view_{$type}_{$boardNo}"])) {
			$result = $this->Board->setBoardHit($boardNo);
			setcookie("view_{$type}_{$boardNo}", true, time() + 86400, '/boardView'); // time() + 86400 = 쿠키 유효 시간 설정해줌(하루)
		}

		// 투데이
		$data['etc1'] = $this->Board->getInfoAny(['vType' => 'important']);
		$data['etc2'] = $this->Board->getInfoAny(['vType' => 'invest']);
		$data['etc3'] = $this->Board->getInfoAny(['vType' => 'study']);

		// 게시글 가져오기
		$data['view'] = $this->Board->getBoardView($boardNo);


		$this->load->view('include/header_view', $data);
		$this->load->view('info/view_key', $data);
		$this->load->view('include/footer');
	}


	// 댓글상세 페이지
	public function reply($boardNo) {
		// 게시글 가져오기
		$data['view'] = $this->Board->getBoardView($boardNo);
		// 댓글 수 가져오기
		$data['countReply'] = $this->Board->getReplyCount($boardNo);

		$this->response->View('info/reply', $data);
	}

	// search 페이지
	public function search() {
		header("Progma:no-cache");
		header("Cache-Control:no-cache,must-revalidate");
		$data['page'] = 'search';
		$data['pageType'] = $this->uri->segment(3) ? urldecode($this->uri->segment(3)) : 'all';

		$data['keyword'] = $this->input->get('keyword') ? $this->input->get('keyword') : urldecode($this->uri->segment(4));
		$data['banner']    = $this->Board->getBanner(['emKind' => 'band']);


		// 전체 검색결과 페이지
		if($data['pageType'] == "all"){
			$data['info'] = $this->Board->getBoardSearchResult(['vType' => array('important', 'invest', 'study')], ['vSubject' => $data['keyword']]);	// 공개정보 3
			$data['stock'] = $this->Board->getStockInfoSearchResult(['vType' => array('period', 'news', 'theme')], ['vSubject' => $data['keyword']]);	// 추천종목 3
			$data['community'] = $this->Board->getBoardCommunity(['vType' => 'community'], 3, ['vSubject' => $data['keyword']]);

			$data['best'] = $this->Board->getCommunityBest();
		}
		else{
			// 각 게시판별 검색결과
			$data['infinity_page'] = 1;
			$data['mainVal'] = 3;
			if($data['pageType'] == "info"){
				$data['infinity']  = $this->Board->getBoardInfinitySearch(['vType' => array('important', 'invest', 'study')], $data['infinity_page'], $data['mainVal'], $data['keyword']);
			}else if($data['pageType'] == "stock"){
				//$data['infinity']  = $this->Board->getStockInfinitySearch(['vType' => array('period', 'news', 'theme')], $data['infinity_page'], $data['mainVal'], $data['keyword']);
				$data['stock']  = $this->Board->getStockInfinitySearch(['vType' => array('period', 'news', 'theme')], $data['infinity_page'], $data['mainVal'], $data['keyword']);
			}else if($data['pageType'] == "community"){
				//$data['infinity']  = $this->Board->getCommunityInfinitySearch(['vType' => 'community'], $data['infinity_page'], $data['mainVal'], $data['keyword']);
				$data['community']  = $this->Board->getCommunityInfinitySearch(['vType' => 'community'], $data['infinity_page'], $data['mainVal'], $data['keyword']);
				$data['best'] = $this->Board->getCommunityBest();
			}
		}
		$this->response->View('info/search', $data);
	}


	public function getBoardInfinitySearch(){

		$data = null;
		$type = $this->input->post('type');

		$infinity_page = $this->input->post('page');

		$category = $this->input->post('category');
		$keyword = $this->input->post('keyword');
		$mainval = null;

		if($type == "info"){
			$where = ['vType' => array('important', 'invest', 'study')];
		}else{
			$where = ['vType' => $type];
		}

		$list = $this->Board->getBoardInfinitySearch($where, $infinity_page, $mainval, $keyword);

		if($list){
			$data = array(
				"status" => "SUCCESS", "list" => $list
			);
		} else {
			$data = array(
				"status" => "FAIL"
			);
		}
		header("Content-Type: application/json;charset=utf-8");
		echo json_encode($data, true);
		exit;

	}

	


	// search 페이지
	public function searchAjax()
	{
		$keyword = $this->input->post('keyword');
		$start_record = $this->input->post('start_record');
		
		// db 처리
		$list = $this->Board->getBoardSearch($keyword, $start_record);
		
		if($list){
			$this->response->Json('success', '성공', '', $list);exit;
		}else{
			$this->response->Json('error', '더 이상 글이 존재하지않습니다.', '', '');exit;
		}
	}

	// 좋아요 처리
	public function likeCheck() {

		$boardNo = $this->input->post('idx');

		if (! $boardNo) {
			$this->response->Json('error', '일시적인 오류', '', '');
			exit;
		}

		if (! isset($_COOKIE["like_board_{$boardNo}"])) {
			$result = $this->Board->setBoardLike($boardNo);
			setcookie("like_board_{$boardNo}", true, time() + 86400 * 7, '/boardView'); // time() + 86400 = 쿠키 유효 시간 설정해줌(하루)
			$this->response->Json('success', '추천 감사합니다.', '', '');
			exit;
		} else {
			$this->response->Json('error', '이미 좋아요한 상태입니다.', '', '');
			exit;
		}
	}

    // 좋아요 댓글 처리
    public function replyLikeCheck()
    {
        $idx = $this->input->post('idx');

        if (! isset($_COOKIE["replyLike_{$idx}"])) {
            $result = $this->Board->setReplyLike($idx);
            setcookie("replyLike_{$idx}", true, time() + 86400 * 7, '/'); // time() + 86400 = 쿠키 유효 시간 설정해줌(하루)
            $this->response->Json('success', '추천 감사합니다.', '', '');
            exit;
        } else {
            $this->response->Json('error', '이미 좋아요한 상태입니다.', '', '');
            exit;
        }
    }

    // 게시판 댓글 등록
    public function addReply()
    {
        $idx   = $this->input->post('idx');
        $reply = $this->input->post('reply');

        if (! $this->session->userdata('UNO')) {
            $this->response->Json('error_lg', '로그인 후 이용가능합니다.', '/auth', '');
            exit;
        }

        if (! $idx) {
            $this->response->Json('error', '글번호가 없습니다.', '', '');
            exit;
        }

        if (! $reply) {
            $this->response->Json('error', '댓글이 없습니다.', '', '');
            exit;
        }

        // DB insert
        $insertData = [
            'nBoardNo'  => $idx,
            'nUserNo'   => $this->session->userdata('UNO'),
            'vNick'     => $this->session->userdata('UNK'),
            'txComment' => $reply,
        ];

        $result = $this->Board->addReply($insertData);

        if ($result) {
            $this->response->Json('success', '정상적으로 등록되었습니다.', '/info/community', '');
            exit;
        } else {
            $this->response->Json('error', '처리중 오류', '', $result);
            exit;
        }
    }

	// 댓글 리스트
	public function getReplyList(){
		$idx          = $this->input->post('idx');
		$start_record = $this->input->post('start_record');
		$orderBy      = $this->input->post('orderby') ? $this->input->post('orderby') : '';
		$level        = $this->input->post('level') ? $this->input->post('level') : '1';


		if (! $idx) {
			$this->response->Json('error', '글번호가 없습니다.');
			exit;
		}

		// DB select
		if (! empty($orderBy)) {
			$result = $this->Board->getReplyOrderBy($idx, $start_record, $orderBy, $level);
		} else {
			$result = $this->Board->getReply($idx, $start_record);
		}

		if ($result) {
			$this->response->Json('success', '정상적으로 등록되었습니다.', '', $result);
		} else {
			$this->response->Json('error', '댓글이 없습니다.', '', $result);
		}
	}

	// 커뮤니티 등록
	public function addCommunity(){
		// form check
		$this->load->library('form_validation');

		$this->form_validation->set_rules('subject', '제목', FV_SUBJECT);
		$this->form_validation->set_rules('content', '내용', FV_CONTENT);

		if ($this->form_validation->run() == false) {
			$this->response->Json('error', ERROR_06);
			exit;
		}

		$boardData = [
			'nUserNo'   => $this->session->userdata('UNO'),
			'nCategoryNo' => $this->input->post('category'),
			'vType'     => 'community',
			'vSubject'  => $this->input->post('subject'),
			'txContent' => $this->input->post('content'),
			'txTag'     => $this->input->post('hashtag'),
			'vIp'       => $_SERVER['REMOTE_ADDR'],
		];

		// DB 처리
		$result = $this->Board->addBoard($boardData);

		if ($result) {
			$this->response->Json('success', '정상적으로 등록되었습니다.', '', $boardData);
			exit;
		} else {
			$this->response->Json('error', '글등록 중에 오류발생', '', $boardData);
			exit;
		}
	}

    // 커뮤니티 수정
    public function setCommunity()
    {
        // form check
        $this->load->library('form_validation');

        $this->form_validation->set_rules('subject', '제목', FV_SUBJECT);
        $this->form_validation->set_rules('content', '내용', FV_CONTENT);

        if ($this->form_validation->run() == false) {
            $this->response->Json('error', ERROR_06);
            exit;
        }

        // DB 처리
        $set = [
            'vSubject'  => $this->input->post('subject'),
            'txContent' => $this->input->post('content'),
            'txTag'     => $this->input->post('hashtag'),
            'vIp'       => $_SERVER['REMOTE_ADDR'],
        ];
        $where  = ['nSeqNo =' => $this->input->post('boardNo')];
        $result = $this->Board->setBoard($set, $where);

        if ($result) {
            $this->response->Json('success', '정상적으로 수정되었습니다.');
            exit;
        } else {
            $this->response->Json('error', '글등록 중에 오류발생');
            exit;
        }
    }

    // 커뮤니티 글 삭제 처리
    public function delCommunity()
    {
        $idx = $this->input->post('idx');

        if (! isset($idx)) {
            $this->response->Json('error', 'idx가 없습니다.');
        }

        // DB 처리
        $this->Board->delReply(['nBoardNo' => $idx]);
        $this->Board->delBoard(['nSeqNo' => $idx]);

        $this->response->Json('success', '정상적으로 등록되었습니다.', '/info/community', '');
        exit;
    }


	// 공유체크 
	public function boardShare() {
		$boardNo    = $this->input->post('boardNo');
		$userNo     = $this->session->userdata('UNO');
		$type       = $this->input->post('type');

		if(!$boardNo) {
			$this->response->Json('error', '필수 값이 없습니다.', '/info/community', '');
			exit;
		}

		if(!$userNo) {
			$this->response->Json('success', '비회원 공유', '/info/community', '');
			exit;
		}

		// share DB 조회 
		$result = $this->Share->getShare($userNo, $boardNo, $type);
		
		if(empty($result)) {
			$insertData = [
				'nUserNo' => $userNo,
				'nBoardNo' => $boardNo,
				'vShareObj' => $type,
			];
			$s_result = $this->Share->addShare($insertData);

			if($s_result) {
				$bs_result = $this->Board->setBoardShare($boardNo);
				if($bs_result){
					$this->response->Json('success', '정상적으로 등록되었습니다.', '', '');exit;
				}
				else{
					$this->response->Json('error', '공유수 업데이트 실패', '', '');exit;
				}
			}
			else{
				$this->response->Json('error', '공유 저장 실패', '', '');exit;
			}
		}
		else{
			// 카운팅안되게 
			$this->response->Json('success', '이미 공유하였습니다', '', '');exit;
		}
	}

	// @param[nType] : 팝업 위치 
	function _popupCheck($nType) {
		// 팝업가져오기 
		$popup = $this->Board->getBannerPopup(['emKind' => 'wide', 'vType' => $nType]);
		// 팝업 시간체크 
		$isPopup = $this->session->userdata($nType.'_POPUP_TIME');

		// 팝업정보가 있으며 시 5분 시간체크가 없을때 
		if($popup && !$isPopup){
			$this->session->set_tempdata($nType.'_POPUP_TIME', true, 300);
			return $popup;
		}
		else{
			return false;
		}
	}



	// 스크랩 추가
	public function boardScrap(){
		$rtn = null;
		$nBoardNo	= $this->input->post('nBoardNo');
		$nUserNo		= $this->session->userdata('UNO');
		$vType		= $this->input->post('vType');


		if(!$nBoardNo) {
			$rtn = array(
				'status' => 'FAIL',
				'code' => '9100',
				'msg' => '필수 값이 없습니다'
			);
		}
		else if(!$nUserNo) {
			$rtn = array(
				'status' => 'FAIL',
				'code' => '9200',
				'msg' => '비회원은 스크랩이 불가능 합니다.'
			);
		}
		else{
			$rtn = $this->Scrap->postScrap();
		}
		header("Content-Type: application/json;charset=utf-8");
		echo json_encode($rtn, true);
		exit;
	}

	// 스크랩 리스트 출력
	public function getScrap(){
		$rtn = $this->Scrap->getScrap();
		header("Content-Type: application/json;charset=utf-8");
		echo json_encode($rtn, true);
		exit;

	}

	// 본문 컨텐츠 불러오기
	public function loadContents(){
		$param = [
			'boardNo' => $this->input->post("boardNo")
		];
		$rtn = $this->Board->getTxContent($param);
		header("Content-Type: application/json;charset=utf-8");
		echo json_encode($rtn, true);
		exit;
	}

	public function csw(){
		echo '<pre>';
		print_r($_SERVER);
		if(stristr($_SERVER['HTTP_USER_AGENT'],'ipad') ) {
			$device ="ipad";
		}else if(stristr($_SERVER['HTTP_USER_AGENT'],'iphone') || strstr($_SERVER['HTTP_USER_AGENT'],'iphone') ) {
			$device ="iphone";
		}else if(stristr($_SERVER['HTTP_USER_AGENT'],'blackberry') ) {
			$device ="blackberry";
		}else if(stristr($_SERVER['HTTP_USER_AGENT'],'android') ) {
			$device ="android";
		}else {
			$device ="etc";
		}
		echo $device;
		exit;
	}

}

 // function likeCheck()
    // {
    //     $nBoardNo = $this->input->post('idx');
    //     $nUserNo = $this->session->userdata('UNO');

    //     if( isset($nBoardNo) && isset($nUserNo) ) {
    //         // select
    //         $idx = $this->Board->getLike($nBoardNo, $nUserNo);

    //         if( isset($idx) ){
    //             # delete
    //             $param = [ 'nBoardNo' => $nBoardNo, 'nUserNo' => $nUserNo ];
    //             $this->Board->delLike($param);
    //             $this->response->Json('success', '좋아요 삭제', '', 'del');
    //             return false;
    //         }
    //         else{
    //             # insert
    //             $param = [ 'nBoardNo' => $nBoardNo, 'nUserNo' => $nUserNo ];
    //             $this->Board->addLike($param);
    //             $this->response->Json('success', '좋아요 완료', '', 'add');
    //             return false;
    //         }

    //     }
    //     else {
    //         $this->response->Json('error', '글번호가 없습니다.'); return false;
    //     }

    // }
