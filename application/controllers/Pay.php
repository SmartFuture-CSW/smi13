<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
    * 결제 Controller
    * @author 임정원 / 2020-02-14
    * @since  Version 1.0.0
    * 
    * index() -> 구독결제, 종목결제 분기처리 로직 
    * result() -> 구독결제, 종목결제 리턴 프로세스 kcp 결제 완료후 노다지 결제처리 
    * pointPay() -> 전체금액 포인트로 처리할때 -> kcp 결제를 타지 않는다. 
    *
*/

class Pay extends CI_Controller
{
	# stock 테이블 ntype 컬럼 변환 
	protected $_nType = [
		'theme' => 2,
		'period' => 3,
		'rumor' => 4,
		'news' => 5
	];

    # 생성자
    function __construct() 
    {
        parent::__construct();
        $this->load->model('User');
        $this->load->model('Board');
        $this->load->model('StockF');
		$this->load->library('Secret');
        $this->load->model('Payment');
        $this->load->model('Point_model');
        $this->load->model('PurchaseModel');
		$this->load->model('Site');
		
        # 로그인 상태체크
        if(!$this->session->userdata('UNO')) redirect('/auth');
    }

    /**
    *   
    */
	public function index($type, $price='', $stockNo='', $stockType='') {
		if( 'result' == $type ){
			$this->response->View('payment/index');
		}


		// 결제 후 리다이렉트 주소 이동 
		$this->session->set_userdata('REFERER', $_SERVER['HTTP_REFERER']);

		switch ($type) {
			case 'point_test':
				// 종목 조회 
				$stockData = $this->Board->getStockView($stockNo);

				// 종목 정보 세션 저장 
				$this->session->set_userdata('STCK_ID', $stockNo);
				$this->session->set_userdata('STCK_NAME', $stockData['vStockName']);
				$this->session->set_userdata('STCK_TYPE', $this->_nType[$stockData['vType']]);
				// 종목 결제 -> 포인트 사용가능, 포인트 완납 가능 
				$data['amount'] = $stockData['nPrice'];
				$data['nStockNo'] = $stockNo;

				// 포인트조회
				$where  = ['nSeqNo =' => $this->session->userdata('UNO')];
				$result = $this->User->getUser($where, 'nPoint');
				if($result) $data['point'] = $result['nPoint'];

				$this->response->View('payment/point', $data);
				break;

			case 'point' : 

				// 종목 조회 
				$stockData = $this->Board->getStockView($stockNo);

				// 종목 정보 세션 저장 
				$this->session->set_userdata('STCK_ID', $stockNo);
				$this->session->set_userdata('STCK_NAME', $stockData['vStockName']);
				$this->session->set_userdata('STCK_TYPE', $this->_nType[$stockData['vType']]);
				// 종목 결제 -> 포인트 사용가능, 포인트 완납 가능 
				$data['amount'] = $stockData['nPrice'];
				$data['nStockNo'] = $stockNo;

				// 통합 포인트 조회
				$param = ['nUserNo' => $this->session->userdata('UNO')];
				$result = $this->Point_model->getUserPoint($param);
				$data['point'] = $result['nPoint'];

				$this->response->View('payment/point_test', $data);

				break;

			default :
				$this->response->View('payment/index');
				break;
		}
		
	}

    public function result()
    {   
        $kcp_data = [
            'site_cd'           => $this->input->post('site_cd'),
            'req_tx'            => $this->input->post('req_tx'),
            'use_pay_method'    => $this->input->post('use_pay_method'),
            'bSucc'             => $this->input->post('bSucc'),
            'res_cd'            => $this->input->post('res_cd'),
            'res_mag'           => $this->input->post('res_mag'),
            'escw_yn'           => $this->input->post('escw_yn'),
            'ordr_idxx'         => $this->input->post('ordr_idxx'),
            'tno'               => $this->input->post('tno'),
            'good_name'         => $this->input->post('good_name'),
            'buyr_name'         => $this->input->post('buyr_name'),
            'buyr_tel1'         => $this->input->post('buyr_tel1'),
            'app_time'          => $this->input->post('app_time'),
            'amount'            => $this->input->post('amount'),
            'card_cd'           => $this->input->post('card_cd'),
            'card_name'         => $this->input->post('card_name'),
            'app_no'            => $this->input->post('app_no'),
            'noinf'             => $this->input->post('noinf'),
            'quota'             => $this->input->post('quota'),
            'param_opt_1'       => $this->input->post('param_opt_1'),       // TYPE 정기구독 : subscription, 종목구매 : point
            'param_opt_2'       => $this->input->post('param_opt_2'),       // 사용자 point 
            'param_opt_3'       => $this->input->post('param_opt_3'),       // 최초원금 
        ];
        //exit;
        //print_r($kcp_data);
        if( $kcp_data['req_tx'] == "pay" && $kcp_data['bSucc'] != "false" && $kcp_data['res_cd'] == "0000" )
        {
        
            if( $kcp_data['use_pay_method'] == "100000000000" )
            {
                // 정기 구독 결제 처리 
                if( 'subscription' == $this->input->post('param_opt_1') )
                {
                    $set = [
                        'nUserNo'       => $this->session->userdata('UNO'),
                        'vKcpIdx'       => $kcp_data['tno'],
                        'vOrderIdx'     => $kcp_data['ordr_idxx'],
                        'vGoodName'     => '일반구독',
                        'vName'         => $this->session->userdata('UNM'),
                        'nType'         => 1,
                        'nPrice'        => $kcp_data['amount'],
                        'nAmount'       => $kcp_data['amount'],
                        'dtEndDate'     => date("Y-m-d H:i:s", strtotime("+1 months")),
                    ];
                    //print_r($set);
                    // 결제 테이블 처리 
                    $result = $this->Payment->addPayment($set);
                    
                    if($result){
						$smsPhone = $this->secret->secretDecode($this->session->userdata('UID'));
						$smsName = $this->session->userdata('UNM');
						$smsStockNo = $this->session->userdata('STCK_ID');
						$this->postStockSMS( $smsPhone, $smsName, $smsStockNo );
                        $this->util->alert('결제가 정상처리되었습니다.', $this->session->userdata('REFERER'));
                    }else{
                        $this->util->alert('결제가 중단되었습니다.', $this->session->userdata('REFERER'));
                    }

                }
                // 포인트 결제처리 
                else 
                {
                    $use_point  = $kcp_data['param_opt_3'] - $kcp_data['amount'];
                    
                    $set = [
                        'nUserNo'       => $this->session->userdata('UNO'),
                        'nStockNo'      => $this->session->userdata('STCK_ID'),
                        'vKcpIdx'       => $kcp_data['tno'],
                        'vOrderIdx'     => $kcp_data['ordr_idxx'],
                        'vGoodName'     => $this->session->userdata('STCK_NAME'), // 기간종목, 테마종목, 찌라시종목
                        'vName'         => $this->session->userdata('UNM'),
                        'nType'         => $this->session->userdata('STCK_TYPE'),
                        'nPrice'        => $kcp_data['amount'],         // 최종금액 - 사용자포인트 차감 = 사용자 실결제액
                        'nAmount'       => $kcp_data['param_opt_3'],    // 최종금액
                        'nPoint'        => $use_point,
                        //'dtEndDate'     => date("Y-m-d H:i:s", strtotime("+1 months")),
                    ];
                    // echo "<pre>";print_r($kcp_data);exit;    
                    // 결제 테이블 처리 
                    $result = $this->Payment->addPayment($set);
                    
                    if($result){

                        // 사용자 포인트 차감 처리
                        $point      = $kcp_data['param_opt_2'] - $use_point;
                    
                        $set    = [ 'nPoint' => $point ];
                        $where  = [ 'nSeqNo =' => $this->session->userdata('UNO') ];
                        $data   = $this->User->setUser($set, $where);
                    
						$smsPhone = $this->secret->secretDecode($this->session->userdata('UID'));
						$smsName = $this->session->userdata('UNM');
						$smsStockNo = $this->session->userdata('STCK_ID');
						$this->postStockSMS( $smsPhone, $smsName, $smsStockNo );

                        $this->util->alert('결제가 정상처리되었습니다.', $this->session->userdata('REFERER'));
                    }else{
                        $this->util->alert('결제가 중단되었습니다.', $this->session->userdata('REFERER'));
                    }

                }
                
            }

        }
        else
        {
            //$this->util->alert('결제중 오류', '');            

        }
       
    }


	/**
	* @breif: 종목추천 포인트 구매
	* @author: csw
	*/
	public function pointPay() {

		// 주문번호
		$ordr_idxx = date("YmdHis").'_'.rand(10000000,99999999);
		// 회원고유번호
		$userNo     = $this->session->userdata('UNO');
		// 사용 포인트
		$point      = $this->input->post('point');
		// 상품 가격
		$amount		= $this->input->post('price');
		// 유저 보유 포인트
		$userPoint	= $this->input->post('userPoint');

		if($amount != $point){
			$this->util->alert('처리중 오류확인', $this->session->userdata('REFERER'));
			exit;
		}

		$check = $this->PurchaseModel->getPayCheck();
		if($check == false){
			redirect('/stock');
			exit;
		}

		$set = [
			'nUserNo'       => $userNo,
			'nStockNo'      => $this->session->userdata('STCK_ID'),
			'vKcpIdx'       => 0,
			'vOrderIdx'     => $ordr_idxx,
			'vGoodName'     => $this->session->userdata('STCK_NAME'),
			'vName'         => $this->session->userdata('UNM'),
			'nType'         => $this->session->userdata('STCK_TYPE'),
			'nPrice'        => 0,         // 실결제액 = 상품가 - 포인트 > 포인트 구매이니 0원임
			'nAmount'       => $amount,    // 상품가격
			'nPoint'        => $point,
		];




		// 결제 테이블 처리 
		$result = $this->Payment->addPayment($set);
		
		if($result){
			// 사용자 포인트 차감 처리    
			/*
			$set    = [ 'nPoint' => ($userPoint - $point) ];
			$where  = [ 'nSeqNo =' => $this->session->userdata('UNO') ];
			$data   = $this->User->setUser($set, $where);
			*/

			$smsPhone = $this->secret->secretDecode($this->session->userdata('UID'));

			$smsName = $this->session->userdata('UNM');
			$smsStockNo = $this->session->userdata('STCK_ID');

			$msg = MSG_POINT_USE_FOR_STOCK."<br>[종목명 : ".$this->session->userdata('STCK_NAME')."]";
			// 유/무료 분리 포인트 차감처리
			$param = [
				'nUserNo' => $userNo
				, 'changePoint' => $point
				, 'changeType' => 'dud'
				, 'nType' => $this->session->userdata('STCK_TYPE')
				, 'nBuyNo' => $smsStockNo // 테스트
				, 'msg' => $msg
				, 'nPointType' => null
				, 'nFreePoint' => null
				, 'nPayPoint' => null
			];

			$data = $this->Point_model->setChangePoint($param);
			// function putUserPoint($nUserNo, $nPoint, $nPointKind, $nType = null, $nBuyNo = null, $msg = null)
			$check = $this->PurchaseModel->putUserPoint($userNo, $point * -1, 2, $this->session->userdata('STCK_TYPE'), $this->session->userdata('STCK_ID'), $msg);


			$this->postStockSMS( $smsPhone, $smsName, $smsStockNo );
			$this->util->alert('구매 완료되었습니다.', $this->session->userdata('REFERER'));
		}
		else{
			$this->util->alert('처리중 오류확인', $this->session->userdata('REFERER'));
		}
	}


	public function card(){
		header("Progma:no-cache");
		header("Cache-Control:no-cache,must-revalidate");

		$check = $this->PurchaseModel->getPayCheck();
		if($check == false){
			redirect('/stock');
			exit;
		}

		
		$data = $this->PurchaseModel->getPayInfo();
		$data['emPayYN'] = ($data['result'] == "SUCCESS") ? "Y" : "N";
		// 유저의 현재 잔여 포인트
		$data['userPoint'] = $this->input->post('userPoint');
		// 결제 사용 포인트
		$data['usePoint'] = $this->input->post('point');
		// 상품 가격
		$data['nAmount'] = $this->input->post('price');

		if($data['emPayYN'] == "Y"){
			$data['vCardNo'] = substr($data['vCardNo'], 0, 4)."************";
			$data['vExpdt'] = "****";
			$data['vPhone'] = "***********";
		}else{
			$data['vCardNo'] = "";
			$data['vExpdt'] = "";
			$data['vPhone'] = $this->secret->secretDecode($this->session->userdata('UID'));
			$data['vName'] = $this->session->userdata('UNM');
		}
		$data['terms'] = $this->Site->getSiteInfo(['nSeqNo' => 1]);

		$this->response->View('payment/payment', $data);
	}


	// 종목추천 결제정보 데이터 베이스 입력
	public function postPayInfo(){
		$rtn = array(
			"result" => "FAIL",
			"msg" => "",
			"data" => ""
		);
		$phone = $this->input->post('vPhone');
		$phone = $this->secret->secretEncode($phone);
		if($phone=="xKKfxZS9GHPdzNWtcOCUuQ==" || $phone=="01063506416"){//201223 송연미 처리
			$rtn = [
				'result'=> 'FAIL'
				, 'msg' => ERROR_14
			];
			header('Content-Type: application/json;charset=utf-8');
			print json_encode($rtn, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		$check = $this->PurchaseModel->getPayCheck();
		if($check == false){
			$rtn['result'] = 'DOUBLE';
			$rtn['msg'] = '이미 구매하신 추천종목 입니다.';
			header('Content-Type: application/json;charset=utf-8');
			print json_encode($rtn, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}


		$agree = $this->input->post('agree');
		for($i=0; $i<count($agree); $i++){
			if($agree[$i] != "y"){
				$rtn['msg'] = "약관 동의를 확인 해주세요";
				echo json_encode($rtn);
				exit;
			}
		}

		$data = $this->PurchaseModel->postPayInfoStock();

		$rtn = $data;


		if($data['result'] == "SUCCESS"){
			$usePoint = (!empty($this->input->post("usePoint"))) ? $this->input->post("usePoint") : '';
			$userPoint = (!empty($this->input->post("userPoint"))) ? $this->input->post("userPoint") : '';

			// 사용자 포인트 차감 처리 
			/*
			$set    = [ 'nPoint' => ($userPoint - $usePoint) ];
			$where  = [ 'nSeqNo =' => $this->session->userdata('UNO') ];
			$data   = $this->User->setUser($set, $where);
			*/

			$smsPhone = $this->secret->secretDecode($this->session->userdata('UID'));
			$smsName = $this->session->userdata('UNM');
			$smsStockNo = $this->session->userdata('STCK_ID');

			$msg = MSG_POINT_USE_FOR_STOCK."<br>[종목명 : ".$this->session->userdata('STCK_NAME')."]";
			// function putUserPoint($nUserNo, $nPoint, $nPointKind, $nType = null, $nBuyNo = null, $msg = null)
			if($usePoint > 0){

				
				// 유/무료 분리 포인트 차감처리
				$param = [
					'nUserNo' => $this->session->userdata('UNO')
					, 'changePoint' => $usePoint
					, 'changeType' => 'dud'
					, 'nType' => $this->session->userdata('STCK_TYPE')
					, 'nBuyNo' => $smsStockNo // 테스트
					, 'msg' => $msg
					, 'nPointType' => null
					, 'nFreePoint' => null
					, 'nPayPoint' => null
				];
				$data = $this->Point_model->setChangePoint($param);

				$check = $this->PurchaseModel->putUserPoint($this->session->userdata('UNO'), $usePoint * -1, 2, $this->session->userdata('STCK_TYPE'), $this->session->userdata('STCK_ID'), $msg);
			}

			$ddd = $this->postStockSMS( $smsPhone, $smsName, $smsStockNo );


			$rtn['referer'] = $this->session->userdata('REFERER');
//		$this->util->alert('구매 완료되었습니다.', $this->session->userdata('REFERER'));
		}
//exit;
		header('Content-Type: application/json;charset=utf-8');
		print json_encode($rtn, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
	}


	// 종목추천 SMS 문자 발송
	protected function postStockSMS($vPhone, $vName, $nStockNo){

		// 구매 종목 데이터
		$data = $this->StockF->getStockData($nStockNo);

		$vStockName = $data['vStockName'];	// 종목명
		$nBuyPrice1 = $data['nBuyPrice1'];	// 1차 구매
		$nBuyPrice2 = $data['nBuyPrice2'];	// 2차 구매
		$nLossPrice = $data['nLossPrice'];	// 손절가
		$nAimPrice = $data['nAimPrice']; // 목표가
		$txRecomReason = (!empty($data['txRecomReason'])) ? $data['txRecomReason'] : ''; // 추천사유

		$vType = $data['vType']; // 추천종목 타입
		$nSeqNo = $data['nSeqNo'];

		$nAimPercent = $data['nAimPercent'];
		$txTag = $data['txTag'];
		$txContent = $data['txContent'];
		$txContent = strip_tags($txContent, "<br>");
		$txContent = str_replace("<br>", "\r\n", $txContent);

		$title = "노다지 구매 추천종목 안내";
		$msg = "안녕하세요 노다지 추천종목을 구매 해주셔서 감사합니다.
		구매하신 종목은 다음과 같습니다.

		종목명 : {$vStockName}{$txTag}
		1차 매수가 : {$nBuyPrice1}
		2차 매수가 : {$nBuyPrice2}
		목표가 : {$nAimPrice}({$nAimPercent}%)
		손절가 : {$nLossPrice}
		추천사유 : {$txRecomReason}
		";
		$ch = curl_init();//문자발송
		$url = "http://www.sendmon.com/_REST/smsApi.asp";
		$category = "send";
		$send_num='18995445';
		$param = "lms";
		$apikey = "7F5724409BB21A870C576B4AEB2DDC6D176473415CD638F3C2CDE8C91C132152";  // API KEY	
		$sendparams = "apikey=" . urlencode($apikey)
		. "&category=" . urlencode($category)
		. "&param=" . urlencode($param)
		. "&send_num=" . urlencode($send_num)
		. "&receive_nums=" . urlencode($vPhone)
		. "&title=" . urlencode($title)
		. "&message=" . urlencode($msg);
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $sendparams);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
		curl_setopt($ch,CURLOPT_TIMEOUT, 20);
		$response = curl_exec($ch);
		curl_close ($ch);	
		unset($ch);
	}
}