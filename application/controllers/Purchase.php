<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Purchase extends CI_Controller {


	// 생성자
	public function __construct() {
		parent::__construct();
		$this->load->library('Secret');
		$this->load->model('SiteinfoModel');
		$this->load->model('PurchaseModel');
		$this->load->model('PaymentModel');
		$this->load->model('App_model');
		$this->load->model('Point_model');
        $this->load->model('User');
		$this->load->model('Site');
/*		$data = $this->PurchaseModel->getPayInfo(1);
		if($data){
			$startDate = strtotime($data['vStartDate']);
			$endDate = strtotime($data['vEndDate']);
			$current = time();

			if($startDate <= $current && $endDate >= $current){
				redirect('subscribe/paycomplete','location');
				exit;
			}
		}
		*/
	}

	public function index() {
		if(!$this->session->userdata('UNO')) redirect('/auth');

		$data = $this->PurchaseModel->getPayInfo();
		$kindHistory = [];

		$payHistory = $this->PurchaseModel->getCurrentPayGoods();
		foreach($payHistory as $row){
			$kindHistory[] = $row['vGoodsKind'];
		}

		// 구매 상품 리스트
		$data['buyGoods'] = $kindHistory;
		// 구매 이력 확인
		$data['emPayYN'] = ($data['result'] == "SUCCESS") ? "Y" : "N";
		// 유저의 현재 잔여 포인트
		$data['userPoint'] = $this->input->post('userPoint');
		// 결제 사용 포인트
		$data['usePoint'] = $this->input->post('point');
		// 상품 가격
		$data['nAmount'] = $this->input->post('price');

		$data['nStockNo'] = $this->session->userdata('STCK_ID');
		$data['topay'] = $this->input->get('topay');
		$data['paytype'] = $this->input->get('paytype');

		if($data['emPayYN'] == "Y"){
			$data['vCardNo'] = substr($data['vCardNo'], 0, 4)."************";
			$data['vExpdt'] = "****";
			$data['vPhone'] = "***********";
			$data['vName'] = $data['vName'];
		}
		else{
			$data['vCardNo'] = '';
			$data['vExpdt'] = '';
			$data['vPhone'] = $this->secret->secretDecode($this->session->userdata('UID'));
			$data['vName'] = $this->session->userdata('UNM');
		}

		$data['write'] = $this->SiteinfoModel->getPrivacy();
		$data['goods'] = $this->PurchaseModel->getGoodsInfo();
		$data['terms'] = $this->Site->getSiteInfo(['nSeqNo' => 1]);

		$this->response->View('purchase/index',$data);
	}




	public function test2() {
		if(!$this->session->userdata('UNO')) redirect('/auth');

		$data = $this->PurchaseModel->getPayInfo();
		$kindHistory = [];

		$payHistory = $this->PurchaseModel->getCurrentPayGoods();
		foreach($payHistory as $row){
			$kindHistory[] = $row['vGoodsKind'];
		}

		// 구매 상품 리스트
		$data['buyGoods'] = $kindHistory;
		// 구매 이력 확인
		$data['emPayYN'] = ($data['result'] == "SUCCESS") ? "Y" : "N";
		// 유저의 현재 잔여 포인트
		$data['userPoint'] = $this->input->post('userPoint');
		// 결제 사용 포인트
		$data['usePoint'] = $this->input->post('point');
		// 상품 가격
		$data['nAmount'] = $this->input->post('price');

		$data['nStockNo'] = $this->session->userdata('STCK_ID');
		$data['topay'] = $this->input->get('topay');
		$data['paytype'] = $this->input->get('paytype');

		if($data['emPayYN'] == "Y"){
			$data['vCardNo'] = substr($data['vCardNo'], 0, 4)."************";
			$data['vExpdt'] = "****";
			$data['vPhone'] = "***********";
			$data['vName'] = $data['vName'];
		}
		else{
			$data['vCardNo'] = '';
			$data['vExpdt'] = '';
			$data['vPhone'] = $this->secret->secretDecode($this->session->userdata('UID'));
			$data['vName'] = $this->session->userdata('UNM');
		}

		$data['write'] = $this->SiteinfoModel->getPrivacy();
		$data['goods'] = $this->PurchaseModel->getGoodsInfo();
		$data['terms'] = $this->Site->getSiteInfo(['nSeqNo' => 1]);

		$this->response->View('purchase/index_test',$data);
	}


	// 결제정보 데이터 베이스 입력
	public function postPayInfo(){
		$rtn = array(
			"result" => "FAIL",
			"msg" => "",
			"data" => ""
		);
		$phone = $this->input->post('vPhone');
		$phone = $this->secret->secretEncode($phone);
		if($phone=="xKKfxZS9GHPdzNWtcOCUuQ==" || $phone=="01063506416"){//201223 송연미 처리
			$rtn = [
				'result'=> 'FAIL'
				, 'msg' => ERROR_14
			];
			header('Content-Type: application/json;charset=utf-8');
			print json_encode($rtn, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}


		$agree = $this->input->post('agree');
		for($i=0; $i<count($agree); $i++){
			if($agree[$i] != "y"){
				$rtn['msg'] = "약관 동의를 확인 해주세요";
				echo json_encode($rtn);
				exit;
			}
		}
		// 같은 상품 중복결제 방지
		$cnt = $this->PurchaseModel->getPayGoodsInfo();
		if($cnt['cnt'] > 0){
			header('Content-Type: application/json;charset=utf-8');
			$rtn['msg'] = "이미 결제하신 상품 입니다.";
			echo json_encode($rtn);
			exit;
		}
		$data = $this->PurchaseModel->postPayInfo();
		
		//프리미엄VIP 첫구매자에게 포인트 지급 201203
		if(($this->input->post("nGoodsNo")=='7' || $this->input->post("nGoodsNo")=='9')){
			if($this->input->post("nGoodsNo")=='7') $nPoint=99000; else $nPoint=990000;
			$data['goodsKind']=3;
			$data['nPoint']=$nPoint;
			$data['msg']="프리미엄VIP 가입 이벤트";
			$result=$this->PurchaseModel->addPoint($data);

			$cnt = $this->Point_model->getCheckFirstPay($data['goodsKind'], $data['payInfo']['nUserNo']);
			if($cnt == 1){
				// 유/무료 분리 포인트 적립 처리
				$param = [
					'nUserNo' => $data['payInfo']['nUserNo']
					, 'changePoint' => $nPoint
					, 'changeType' => 'add'
					, 'nType' => 1
					, 'nBuyNo' => $data['payInfo']['nBuyNo']
					, 'msg' => '프리미엄VIP 가입 이벤트'
					, 'nPointType' => 1
					, 'nFreePoint' => 0
					, 'nPayPoint' => $nPoint
				];
				$this->Point_model->setChangePoint($param);
			}
		}
		//프리미엄VIP 첫구매자에게 포인트 지급

		if($data['result'] == "SUCCESS"){
			// referer 결제 체크
			$param = [
				'nPay' => 1
			];
			$this->App_model->setReferer($param);
		}
		$rtn = $data;
		header('Content-Type: application/json;charset=utf-8');
		print json_encode($rtn, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
	}


	// 포인트 결제정보 데이터 베이스 입력
	public function postPointPayInfo(){
		$rtn = array(
			"result" => "FAIL",
			"msg" => "",
			"data" => ""
		);
		$phone = $this->input->post('vPhone');
		$phone = $this->secret->secretEncode($phone);
		if($phone=="xKKfxZS9GHPdzNWtcOCUuQ==" || $phone=="01063506416"){//201223 송연미 처리
			$rtn = [
				'result'=> 'FAIL'
				, 'msg' => ERROR_14
			];
			header('Content-Type: application/json;charset=utf-8');
			print json_encode($rtn, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}
		$agree = $this->input->post('agree');
		for($i=0; $i<count($agree); $i++){
			if($agree[$i] != "y"){
				$rtn['msg'] = "약관 동의를 확인 해주세요";
				echo json_encode($rtn);
				exit;
			}
		}

		$data = $this->PurchaseModel->postPointPayInfo();

		$rtn = $data;
		header('Content-Type: application/json;charset=utf-8');
		print json_encode($rtn, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
	}



	public function getPayInfo(){
		$vPhone=$this->input->post('vPhone');
		if($vPhone=="xKKfxZS9GHPdzNWtcOCUuQ==" || $vPhone=="01063506416"){//201223 송연미 처리
			$rtn = [
				'result'=> 'error'
				, 'msg' => ERROR_14
			];
		}

		$data = $this->PurchaseModel->getPayInfo();
		if($data == null){
			$rtn = [
				'result'=> 'error'
				, 'msg' => '기존 결제 정보가 없습니다.'
			];
		}
		else{
			$rtn = $data;
		}
		header('Content-Type: application/json;charset=utf-8');
		print json_encode($rtn, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
	}

	public function postRefundInfo(){
		$rtn = array(
			"result" => "FAIL",
			"msg" => "",
			"data" => ""
		);

		$data = $this->PurchaseModel->postRefundInfo();

		$rtn['data'] = $data;
		header('Content-Type: application/json;charset=utf-8');
		print json_encode($rtn, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
	}


	public function payment(){

		$data = $this->PurchaseModel->getPayInfo();
		$data['emPayYN'] = ($data['result'] == "SUCCESS") ? "Y" : "N";
		$data['nStockNo'] = $this->session->userdata('STCK_ID');

		$data['topay'] = $this->input->get('topay');
		$data['paytype'] = $this->input->get('paytype');
		$data['vCardNo'] = '';
		$data['vExpdt'] = '';
		$data['vPhone'] = $this->secret->secretDecode($this->session->userdata('UID'));
		$data['vName'] = $this->session->userdata('UNM');
		$data['write'] = $this->SiteinfoModel->getPrivacy();
		$data['goods'] = $this->PurchaseModel->getGoodsInfo();
		$data['terms'] = $this->Site->getSiteInfo(['nSeqNo' => 1]);

		$this->response->View('purchase/payment',$data);
	}


	public function postPayInfoNoMember(){
		$phone = $this->input->post('vPhone');
		$phone = $this->secret->secretEncode($phone);
		if($phone=="xKKfxZS9GHPdzNWtcOCUuQ==" || $phone=="01063506416"){//201223 송연미 처리
			$rtn = [
				'result'=> 'FAIL'
				, 'msg' => ERROR_14
			];
			header('Content-Type: application/json;charset=utf-8');
			print json_encode($rtn, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
			exit;
		}

		// DB 조회
		$select = 'nSeqNo, vPhone, vName, vNick, vDevice, nLevel, vPushKey';
		$where  = ['vPhone =' => $phone];
		$result = $this->User->getUser($where, $select);

		if (! empty($result)) {

			// session 처리
			$this->_userSession($result);

			// 접속 로그 처리
			$setData = [
				'nUserNo'    => $result['nSeqNo'],
				'vUserIp'    => $_SERVER['REMOTE_ADDR'],
				'vUserAgent' => $_SERVER['HTTP_USER_AGENT'],
			];
			$this->User->addUserLogin($setData);


			// referer 회원가입 체크
			$param = [
				'nUserNo' => $result['nSeqNo']
				, 'vPhone' => $result['vPhone']
				, 'nJoin' => 1
			];
			$this->App_model->setReferer($param);

			$rtn = array(
				"result" => "success",
				"msg" => "login",
				"data" => $result
			);


		} else {

			$set = [
				'vPhone'  => $phone,
				'vPwd'    => $this->secret->secretEncode(''),
				'vName'   => $this->input->post('vName'),
				'vIp'     => $_SERVER['REMOTE_ADDR'],
				'vDevice' => '',
				'vPushKey' => $this->session->userdata('deviceKey'),
				'nPoint' => 50000
			];

			$data = $this->User->addUser($set);

			if (isset($data)) {
				// session 처리
				$this->_userSession($data);

				// referer 회원가입 체크
				$param = [
					'nUserNo' => $data['nSeqNo']
					, 'vPhone' => $data['vPhone']
					, 'nJoin' => 1
				];
				$this->App_model->setReferer($param);

				// return
				$rtn = array(
					"result" => "success",
					"msg" => "join",
					"data" => $data
				);

			}
			else {
				$this->response->Json('error', ERROR_06);
			}
		}

		$rtn = array(
			"result" => "FAIL",
			"msg" => "",
			"data" => ""
		);
		$agree = $this->input->post('agree');
		for($i=0; $i<count($agree); $i++){
			if($agree[$i] != "y"){
				$rtn['msg'] = "약관 동의를 확인 해주세요";
				echo json_encode($rtn);
				exit;
			}
		}
		// 같은 상품 중복결제 방지
		$cnt = $this->PurchaseModel->getPayGoodsInfo();
		if($cnt['cnt'] > 0){
			header('Content-Type: application/json;charset=utf-8');
			$rtn['msg'] = "이미 결제하신 상품 입니다.";
			echo json_encode($rtn);
			exit;
		}
		$data = $this->PurchaseModel->postPayInfo();
		
		//프리미엄VIP 첫구매자에게 포인트 지급 201203
		if(($this->input->post("nGoodsNo")=='7' || $this->input->post("nGoodsNo")=='9')){
			if($this->input->post("nGoodsNo")=='7') $nPoint=99000; else $nPoint=990000;
			$data['goodsKind']=3;
			$data['nPoint']=$nPoint;
			$data['msg']="프리미엄VIP 가입 이벤트";
			$result=$this->PurchaseModel->addPoint($data);


			$cnt = $this->Point_model->getCheckFirstPay($data['goodsKind']);
			if($cnt == 1){
				// 유/무료 분리 포인트 적립 처리
				$param = [
					'nUserNo' => $data['payInfo']['nUserNo']
					, 'changePoint' => $nPoint
					, 'changeType' => 'add'
					, 'nType' => 1
					, 'nBuyNo' => $data['payInfo']['nBuyNo']
					, 'msg' => '프리미엄VIP 가입 이벤트'
					, 'nPointType' => 1
					, 'nFreePoint' => 0
					, 'nPayPoint' => $nPoint
				];
				$this->Point_model->setChangePoint($param);
			}
		}
		//프리미엄VIP 첫구매자에게 포인트 지급

		if($data['result'] == "SUCCESS"){
			// referer 결제 체크
			$param = [
				'nPay' => 1
			];
			$this->App_model->setReferer($param);
		}

		$rtn = $data;
		header('Content-Type: application/json;charset=utf-8');
		print json_encode($rtn, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
	}


	// session 생성
	public function _userSession($data) {
		// session 처리
		$sessionData = [
			'UNO'  => $data['nSeqNo'],
			'UID'  => $data['vPhone'],
			'UNK'  => $data['vNick'],
			'UNM'  => $data['vName'],
		];
		set_cookie('UNO', $data['nSeqNo'], 86400*30, '.nodajistock.co.kr', '/', '');
		set_cookie('UID', $data['vPhone'], 86400*30, '.nodajistock.co.kr', '/', '');
		set_cookie('UNK', $data['vNick'], 86400*30, '.nodajistock.co.kr', '/', '');
		set_cookie('UNM', $data['vName'], 86400*30, '.nodajistock.co.kr', '/', '');
		$this->session->set_userdata($sessionData);
	}



	public function point() {
		if(!$this->session->userdata('UNO')) redirect('/auth');
		$data = $this->PurchaseModel->getPayInfo();

		// 구매 이력 확인
		$data['emPayYN'] = ($data['result'] == "SUCCESS") ? "Y" : "N";

		if($data['emPayYN'] == "Y"){
			$data['vCardNo'] = substr($data['vCardNo'], 0, 4)."************";
			$data['vExpdt'] = "****";
			$data['vPhone'] = "***********";
			$data['vName'] = $data['vName'];
		}
		else{
			$data['vCardNo'] = '';
			$data['vExpdt'] = '';
			$data['vPhone'] = $this->secret->secretDecode($this->session->userdata('UID'));
			$data['vName'] = $this->session->userdata('UNM');
		}

		$data['write'] = $this->SiteinfoModel->getPrivacy();
		$data['goods'] = $this->PurchaseModel->getGoodsPoint();
		$data['terms'] = $this->Site->getSiteInfo(['nSeqNo' => 1]);

		$this->response->View('purchase/point',$data);
	}

	public function refund() {
		$this->response->View('purchase/refund');
	}
	public function PostRefund() {
		$data['code']='1';
		echo json_encode($data);
		exit;
	}
	public function SetCert() {
		$gap=substr(trim($this->input->post("gap")),0,11);
		if(strlen($gap)<10){
			$data['code']="2";
			$data['msg']="전화번호 오류";
			echo json_encode($data);
			exit;
		}
		if($_SESSION['cert_no']=='') $_SESSION['cert_no']=rand(10000,99999);
		
		$ch = curl_init();
		$url = "http://www.sendmon.com/_REST/smsApi.asp";
		$category = "send";
		$send_num='18995445';
		$param = "sms";
		$apikey = "7F5724409BB21A870C576B4AEB2DDC6D176473415CD638F3C2CDE8C91C132152";  // API KEY	
		$msg="정기결제 해지신청 인증번호는 [".$_SESSION['cert_no']."] 입니다.";
		$sendparams = "apikey=" . urlencode($apikey)
		. "&category=" . urlencode($category)
		. "&param=" . urlencode($param)
		. "&send_num=" . urlencode($send_num)
		. "&receive_nums=" . urlencode($gap)
		. "&title=" . urlencode($title)
		. "&message=" . urlencode($msg);
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $sendparams);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
		curl_setopt($ch,CURLOPT_TIMEOUT, 20);
		$response = curl_exec($ch);
		curl_close ($ch);	
		unset($ch);

		$data['code']='1';
		echo json_encode($data);
		exit;
	}
	public function GetCert() {
		$gap=substr(trim($this->input->post("gap")),0,10);
		if($_SESSION['cert_no']!=$gap){
			$data['code']="2";
			$data['msg']="인증번호 불일치";
			echo json_encode($data);
			exit;
		}
		$_SESSION['cert_ok']='ok';

		$data['code']='1';
		echo json_encode($data);
		exit;
	}
}