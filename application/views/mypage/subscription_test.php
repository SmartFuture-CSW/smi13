<style>
.pop_main{width:100%; display:block;  border-top:1px solid #aaa;  border-bottom:1px solid #aaa;  padding:10px 0px;}
.refund_receipt{width:100%; table-layout:auto;}
.refund_receipt tbody tr th{width:50%; text-align:left; padding-bottom:10px;}
.refund_receipt tbody tr td{width:50%; text-align:right;}
.pop_result{width:100%; font-weight:700; text-align:right; padding:10px 0px;}
.pop_result strong{color:#fa2829 !important}
.pop_bottom .btn_m{width:46.5%;}

</style>

<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="/mypage" aria-label="뒤로가기"></a>
				<h2>정기구독 이용 내역</h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main has_bottom_banner">
			<div class="table_container" style="margin-top:14px;">
				<table>
					<colgroup>
						<col style="width:18%">
						<col style="width:18%">
						<col>
						<col style="width:20%">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">시작일</th>
							<th scope="col">종료일</th>
							<th scope="col">구독내역</th>
							<th scope="col">구독상태</th>
						</tr>
					</thead>
					<tbody>

					<?php foreach($list as $row){ ?>
						<tr>
							<td><?=$row['vStartDate']?></td>
							<td><?=$row['vEndDate']?></td>
							<td class="f_bold"><?=$row['vGoodName']?></td>
							<td>
						<?php if($row['nOrderStatus'] == "1"){?>
								<button type="button" class="btn_s_round bg_red" disabled>종료</button>
						<?php } else {  ?>
							<?php if($row['refundNo'] == null){?>
								<button type="button" class="btn_s_round type_long bg_red request_refund" data-idx="<?=$row['nSeqNo']?>">취소 신청</button>
							<?php } else { ?>
								<button type="button" class="btn_s_round bg_blue" disabled>처리중</button>
							<?php }?>
							</td>
						<?php }?>
						</tr>
					<?php }?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
				<?php foreach ($banner as $key => $value) { ?>
				<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
				<?php }?>
			</div>
			<!-- gnb -->
			<? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>
	
<?/*	
	<div class="remodal confirm" data-remodal-id="confirmMsg">
		<p>노다지 정기구독을<br>해지하시겠습니까?</p>
		<div class="pop_bottom">
			<button class="btn_m cancel" data-remodal-action="cancel">취소</button>
			<button class="btn_m" data-remodal-action="confirm" id="modifyBtn">해지</button>
		</div>
	</div>
*/?>	
	<div class="remodal alert" data-remodal-id="confirmMsg">
		<h2 class="tit_type1">취소신청</h2>
		<div class="pop_main">
			<table class="refund_receipt">
				<tbody>
					<tr>
						<th colspan="2" class="goods_name"></th>
					</tr>
					<tr>
						<th>구매가</th>
						<td class="pay_price"></td>
					</tr>
					<tr>
						<th>이용기간</th>
						<td class="use_date"></td>
					</tr>
					<tr>
						<th>이용기간 차감금액</th>
						<td class="day_commission"></td>
					</tr>
					<tr>
						<th>사용포인트</th>
						<td class="use_point"></td>
					</tr>
					<tr>
						<th>취소 수수료(10%)</th>
						<td class="refund_commission"></td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div class="pop_result">
			취소 시 환불 금액 : <strong class="refund_price">99,000</strong>원
		</div>

		<div class="pop_bottom signIn">
			<button class="btn_m bg_gray" data-remodal-action="cancel">취소</button>
			<button class="btn_m bg_red" data-remodal-action="confirm" id="confirm_request">신청완료</button>
		</div>
	</div>



	<div class="remodal alert cancel_completed" data-remodal-id="completeMsg" id="completeMsg">
		<p>노다지 정기구독<br><em>해지가 완료되었습니다</em></p>
		<div class="pop_bottom">
			<button class="btn_s_round bg_red" data-remodal-action="confirm" id="">SEE YOU LATER!</button>
		</div>
	</div>

	
<script type="text/javascript">
var idx = 0;

$(function(){

	$(".back > a").on("click", function(){
		history.back(-1);
	});

	$(".request_refund").off("click").on("click", function(){
		var nPayNo = $(this).data("idx");
		var data = {
			'nPayNo' : nPayNo
		}
		$.ajax({
			type : "post",
			url : "/mypage/getRefundReceipt",
			dataType : 'json',
			data : data,
			success : function(response)  {
				$(".goods_name").text(response.goods_name);
				$(".use_date").text(response.use_date);
				$(".use_point").text(response.use_point+"P");
				$(".refund_commission").text(response.refund_commission + "원");
				$(".day_commission").text(response.day_commission + "원");
				$(".pay_price").text(response.pay_price+"원");
				$(".refund_price").text(response.refund_price);
				$("#confirm_request").data("nPayNo", response.nPayNo);
				

				$('[data-remodal-id=confirmMsg]').remodal().open();
			},
			error : function(xhr, status, error) {},
		});

	});

	$('#confirm_request').on('click', function() {
		var data = {
			'idx' : $(this).data("nPayNo")
		}
		$.ajax({
			type : "post",
			url : "/mypage/postRefundData",
			dataType : 'json',
			data : data,
			success : function(response)  {
				if( response.result == 'success') {
					location.reload();
				}
				if( response.result == 'error' ) Cmmn.alertMsg(response.msg);
			},
			error : function(xhr, status, error) {},
		})

	});
});


</script>