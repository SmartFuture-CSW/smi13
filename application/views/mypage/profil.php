<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="#" aria-label="뒤로가기"></a>
				<h2>프로필 관리</h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main has_bottom_banner">

			<div class="profile_edit_container clearfix">
		
		<?  
			if($user['vImage']){ 
				$img = '/data/user/profile/'.$user['vImage'];  
		?>
				<button class="btn_edit_img" type="button" aria-label="프로필 사진" style="background-image: url('<?=$img?>');background-size:cover;">
		<?	}else { ?>
				<button class="btn_edit_img" type="button" aria-label="프로필 사진">
		<?	}   ?>
				</button>
				<div class="f_right">
					<em class="nickname"><?=$user['vNick']?></em>
					<div class="input_box has_btn">
						<input type="text" name="nick" id="nick" placeholder="닉네임(한/영 10자 이내)" maxlength="10">
						<button type="button" id="modifyBtn">변경</button>
					</div>
				</div>
				<input type='file' id="userImg" name="userImg" accept="image/gif,image/jpeg,image/png" style="display: none;"/>

			</div>
<?php /*
			<div class="user_personal_info">
				<div class="item">
					<em>휴대폰번호</em>
					<span><?=$user['vPhone']?></span>
				</div>
				<?php /* <a href="/mypage/passwordChange" class="item">비밀번호 변경하기</a>?>
			</div>
*/?>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
			<? foreach ($banner as $key => $value) { ?>
            	<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a> 
        	<?}?>
			</div>

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>

	<script type="text/javascript">

		$(document).ready(function(){

			$(".back > a").on("click", function(){
				history.back(-1);
			});
		    // 로그인버튼 
		    $("#modifyBtn").click(function(){ 
		    	//Cmmn.alertMsg('test'); 
		    	$.ajax({
	                type : "post",
	                url : "/mypage/modifyNick",
	                dataType : 'json',
	                data : { 'nick': $('#nick').val() },

	                success : function(response) {
	                	//Cmmn.alertMsg(response.msg);
	                    if( response.result == 'success') {
	                    	//Cmmn.alertMsg(response.msg);
	                        location.replace(response.url);
	                    }else{
	                    	Cmmn.alertMsg(response.msg);
	                    }

	                },

	                error : function(xhr, status, error) {},
	            })  
	        
	    	});

		    $("#userImg").on('change', function(){
                readURL(this);
            });

            $('.btn_edit_img').click(function (e) {

				e.preventDefault();

				$('#userImg').click();

			});

		});
    

        function changeValue(obj){

	        alert(obj.value);

        }

		function readURL(input)
		{
            if (input.files && input.files[0]) 
            {
	            var reader = new FileReader();

	            reader.onload = function (e) {
                    $('.btn_edit_img').css("background-image", "url(" + e.target.result + ")");
                }
              	reader.readAsDataURL(input.files[0]);

              	// 원하는 것만 넘겨주는 방식 
              	var formData = new FormData(); 
              	formData.append("userImg", $("#userImg")[0].files[0]);

              	$.ajax({ 
              		type: 'POST', 
              		url: '/mypage/imgFileUpload', 
              		processData: false, // 필수 
              		contentType: false, // 필수 
              		data: formData,
              		dataType : 'json',

              		success: function(response) { 
              			Cmmn.log(response);
              			Cmmn.alertMsg(response.msg);
              		} 

              	});

             //  	$.ajax({	
	            //     type : "post",
	            //     url : "/mypage/imgFileUpload",
	            //     dataType : 'json',
	            //     data : { 'nick': $('#nick').val() },

	            //     success : function(response) {
	            //     	//Cmmn.alertMsg(response.msg);
	            //         if( response.result == 'success') {
	            //         	//Cmmn.alertMsg(response.msg);
	            //             location.replace(response.url);
	            //         }else{
	            //         	Cmmn.alertMsg(response.msg);
	            //         }

	            //     },

	            //     error : function(xhr, status, error) {},
	            // })

            }
        }

</script>
