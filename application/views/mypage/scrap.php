<div class="app_wrapper">
	<!-- [Start] App Header -->
	<header class="app_header">
		<div class="back">
			<a href="javascript:window.history.back(-2)" aria-label="뒤로가기"></a>
			<h2>스크랩 컨텐츠 확인</h2>
		</div>
	</header>
	<!-- [End] App Header -->

	<!-- [Start] App Main -->
	<form name="frm" id="frm" method="post" action="/store/addPaymentInfo" onsubmit="return submitCheck()">
	<div class="app_main">
		<section class="result_container">
			<div class="essential_list">
				<?php
				foreach ($scrap as $key => $value) { 
					$imgPath = '/data/board/thumb/'.$value['vImage'];
				?>
				<a class="clearfix " href="/boardView/<?=$value['vType']?>/<?=$value['nSeqNo']?>">
					<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
					<div class="info">
						<h5><?=$value['vSubject']?></h5>
						<p>
							<span><?=$value['dtRegDate']?></span>
							<span>조회수 <?=$value['nHit']?>회</span>
							<span>공유수 <?=$value['nShare']?>회</span>
						</p>
					</div>
				</a>
				<?  } ?>
			</div>
		</section>	
	</div>
	</form>
	<!-- [End] App Main -->

	<!-- [Start] App Bottom -->
	<div class="app_bottom">
		<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
	</div>
	<!-- [End] App Bottom -->
</div>

<script type="text/javascript">

$(function(){
	
	$(".back > a").on("click", function(){
		history.back(-1);
	});

	
});

</script>