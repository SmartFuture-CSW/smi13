<?
	$_nType =[
		2 => '테마별',
		3 => '기간별',
		4 => '찌라시',
		5 => '뉴스별'
	];

?>

<style>
.stock{cursor:pointer}
</style>


<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="/mypage" aria-label="뒤로가기"></a>
				<h2>구매 종목 내역</h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main has_bottom_banner">
			<div class="table_container" style="margin-top:14px;">
				<table>
					<colgroup>
						<col style="width:18%">
						<col>
						<col style="width:22%">
						<col style="width:18%">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">상품구분</th>
							<th scope="col">종목명</th>
							<th scope="col">상품가</th>
							<th scope="col">상태</th>
						</tr>
					</thead>
					<tbody>
			
		<?  	if(!empty($list)){
					foreach ($list as $key => $value) {
						?>
						<tr class="stock" data-stock_no="<?=$value['nStockNo']?>" data-stock_type="<?=$value['vType']?>">
							<td>추천종목</td>
							<td><?=$value['vGoodName']?></td>
							<td class="f_emphasis"><?=number_format($value['nAmount'])?>원</td>
							<td><?=($value['nOrderStatus'] == 0) ? "구매완료" : "구매취소"; ?></td>
						</tr>

		<?  		}
				}else{  ?>

					<tr><td colspan="4">이용 내역이 없습니다.</td></tr>

		<?		} 		?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
				<? foreach ($banner as $key => $value) { ?>

            	<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
        
        		<?}?>
			</div>

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

<script>
$(function(){
	$(".stock").on("click", function(){
		var stock_no = $(this).data('stock_no');
		var stock_type = $(this).data('stock_type');
		window.location.href = "/stock/view/"+stock_type+"/" + stock_no;
	});
	$(".back > a").on("click", function(){
		history.back(-1);
	});
});
</script>
