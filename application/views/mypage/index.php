<div class="app_wrapper">
		<? include_once(VIEW_PATH.'/include/header_app.php'); ?>

		<!-- [Start] App Main -->
		<div class="app_main mypg has_bottom_banner">
			<div class="heading_container has_back layout_center">
				<a href="/info" class="back" aria-label="뒤로가기"></a>
				<h1>마이페이지</h1>
			</div>

			<div class="user_profile layout_center clearfix">

		<?php
		if($user['vImage']){ 
			$img = '/data/user/profile/'.$user['vImage'];  
		?>
				<div class="user_img" style="background-image: url('<?=$img?>">
		<?php
		}
		else {
		?>
				<div class="user_img">
		<?php
		}?>
				
				</div>
				<div class="user_info">
					<span class="nickname"><?=$_SESSION['UNK'];?></span>
					<p>
						<span><?=number_format($point['nPayPoint'])?>P / <?=number_format($point['nFreePoint'])?>BP</span>
						<span><? if($isVip){ ?>VIP 구독 이용권<?}else if($user['nLevel']=='2'){?>전문가 <? }else{ ?>일반 회원<?}?></span>
					</p>
				</div>
				<button class="logout">로그아웃</button>
				<div class="user_setting">
					<a href="/profile">노다지 프로필 관리</a>
				</div>
			</div>

			<div class="tabContainer myPageTabs">
				<div class="tabList" role="tablist" aria-label="Mypage Menu Tabs">
					<a href="#p=1" id="tab-button-1" class="tabButton active" role="tab" aria-controls="tab-panel-1"><?=($_SERVER['ios'] == "N")? "구독/구매 관리" : "스크랩"; ?></a>
					<a href="#p=2" id="tab-button-2" class="tabButton" role="tab" aria-controls="tab-panel-2">설정</a>
					<a href="#p=3" id="tab-button-3" class="tabButton" role="tab" aria-controls="tab-panel-3">고객센터</a>
					<a href="#p=4" id="tab-button-4" class="tabButton" role="tab" aria-controls="tab-panel-4">기타</a>
				</div>

				<div id="tab-panel-1" class="tabPanel active" role="tabpanel" aria-labelledby="tab-button-1">
					<div class="mypg_list">
<?php if($_SERVER['ios']=="N"){?>
						<a href="/mypage/subscription" class="mypg_item">
							<h3>정기구독 이용 내역</h3>
						</a>
						<a href="/mypage/purchase" class="mypg_item">
							<h3>구매 종목 내역</h3>
						</a>
						<a href="/mypage/point" class="mypg_item">
							<h3>포인트 내역</h3>
						</a>
						<a href="/mypage/paymentInfo" class="mypg_item">
							<h3>결제정보 변경 등록</h3>
						</a>
<?php }?>
						<a href="/mypage/scrap" class="mypg_item">
							<h3>스크랩 컨텐츠 확인</h3>
						</a>
					</div>
				</div>

				<div id="tab-panel-2" class="tabPanel" role="tabpanel" aria-labelledby="tab-button-2" hidden>
					<div class="mypg_list">
						<div class="mypg_item">
							<h3>알림 서비스</h3>
							<small>(거부시 중요 알림을 못 받습니다)</small>
							<label class="btn_toggle">
								<input type="checkbox" name="push" id="push" <?if($user['vPushOs']==1) echo 'checked';?>>
								<span></span>
							</label>
						</div>
						<button class="mypg_item" data-remodal-target="popDeleteCache">
							<h3>미디어 파일 모두 삭제</h3>
						</button>
					</div>
				</div>

				<div id="tab-panel-3" class="tabPanel" role="tabpanel" aria-labelledby="tab-button-3" hidden>
					<div class="mypg_list">
						<a href="/mypage/notice" class="mypg_item">
							<h3>공지사항</h3>
						</a>
<?php if($_SERVER['ios']=="N"){?>
						<a href="tel:18995445" class="mypg_item">
							<h3>빠른 전화 문의 1899-5445</h3>
						</a>
<?php }?>
					</div>
				</div>

				<div id="tab-panel-4" class="tabPanel" role="tabpanel" aria-labelledby="tab-button-4" hidden>
					<div class="mypg_list">
						<a href="/mypage/terms_01" class="mypg_item">
							<h3>서비스 이용약관</h3>
						</a>
						<a href="/mypage/terms_02" class="mypg_item">
							<h3>개인정보 취급방침</h3>
						</a>
						<a href="/mypage/terms_03" class="mypg_item">
							<h3>마케팅 활용 동의</h3>
						</a>
						<div class="mypg_item">
							<h3>버전정보 - <em>현재버전 2.4</em></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
			<? foreach ($banner as $key => $value) { ?>
            	<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a> 
        	<?}?>
			</div>

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<!-- [Start] Popup - Simple Alert -->
	<div class="remodal confirm" data-remodal-id="popDeleteCache">
		<h2>미디어파일 삭제</h2>
		<p>
			임시 저장된 미디어 파일을 모두 삭제합니다.
		</p>
		<div class="pop_bottom">
			<button class="btn_m cancel" data-remodal-action="cancel">취소</button>
			<button class="btn_m" data-remodal-action="confirm" id="clearcache">삭제</button>
		</div>
	</div>
	<!-- [End] Popup - Simple Alert -->



<script type="text/javascript">

var authFlag = false;

$(document).ready(function() {

	var u = window.location.href;
	au = u.split("#");
	var t = au[au.length - 1];

	switch(t){

		case 'p=1' : $("#tab-button-1").click(); break;
		case 'p=2' : $("#tab-button-2").click(); break;
		case 'p=3' : $("#tab-button-3").click(); break;
		case 'p=4' : $("#tab-button-4").click(); break;
	}

	$("#clearcache").on("click", function(){
		window.CacheClear(
			function(success){
				alert('캐시 데이터가 삭제 됐습니다.'); },
			function(error){
				alert('Error: ' + status);
			});
	});

//window.CacheClear(function(success){ alert('Message: ' + success); }, function(error){ alert('Error: ' + status); });


	// 로그인버튼 
	$(".logout").click(function(){ location.replace('/logout'); });
	$("#push").click(function(){ 
		console.log(Is.checkBox( 'push' ));
		$.ajax({
			type : "post",
			url : "/mypage/modifyPush",
			dataType : 'json',
			data : { 'push': Is.checkBox('push'), },
			success : function(response) {
				Cmmn.log(response);
				if( response.result == 'success') {
					
				}else{
					
				}
			},
			error : function(xhr, status, error) {},
		});
	});
});

</script>