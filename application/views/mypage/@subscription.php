

<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="/mypage" aria-label="뒤로가기"></a>
				<h2>정기구독 이용 내역</h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main has_bottom_banner">
			<div class="table_container" style="margin-top:14px;">
				<table>
					<colgroup>
						<col style="width:18%">
						<col style="width:18%">
						<col>
						<col style="width:20%">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">시작일</th>
							<th scope="col">종료일</th>
							<th scope="col">구독내역</th>
							<th scope="col">구독상태</th>
						</tr>
					</thead>
					<tbody id="dataList">
		
		

						<!-- <tr>
							<td>20.02.21</td>
							<td>20.03.20</td>
							<td class="f_bold">노다지 정기구독</td>
							<td>
								<button type="button" class="btn_s_round bg_red" disabled>종료</button>
							</td>
						</tr>
						<tr>
							<td>20.02.21</td>
							<td>20.03.20</td>
							<td class="f_bold">노다지 정기구독</td>
							<td>
								<button type="button" class="btn_s_round bg_red" disabled>종료</button>
							</td>
						</tr> -->
					</tbody>
				</table>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
				<? foreach ($banner as $key => $value) { ?>

           		<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
        
       			 <?}?>
			</div>

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>
	
<?/*	
	<div class="remodal confirm" data-remodal-id="confirmMsg">
		<p>노다지 정기구독을<br>해지하시겠습니까?</p>
		<div class="pop_bottom">
			<button class="btn_m cancel" data-remodal-action="cancel">취소</button>
			<button class="btn_m" data-remodal-action="confirm" id="modifyBtn">해지</button>
		</div>
	</div>
*/?>	
	<div class="remodal confirm" data-remodal-id="confirmMsg">
		<p>현재 해지및 취소는 개편중입니다. 고객센터로 문의해주시기 바랍니다.</p>
	</div>
	<div class="remodal alert cancel_completed" data-remodal-id="completeMsg" id="completeMsg">
		<p>노다지 정기구독<br><em>해지가 완료되었습니다</em></p>
		<div class="pop_bottom">
			<button class="btn_s_round bg_red" data-remodal-action="confirm" id="">SEE YOU LATER!</button>
		</div>
	</div>

	
	<script type="text/javascript">
	var idx = 0;

	$(document).ready(function(){

		$(".back > a").on("click", function(){
			history.back(-1);
		});

		getList();
<?php /*
		$('#modifyBtn').on('click', function() {

			var data = {
				'idx' : idx
			}

			$.ajax({
				type : "post",
				url : "/mypage/modifySubscription",
				dataType : 'json',
				data : data,
				success : function(response)  {
					if( response.result == 'success') {
						Cmmn.alertId('completeMsg');
						getList();
						//location.reload();
					}
					if( response.result == 'error' ) Cmmn.alertMsg(response.msg);
				},
				error : function(xhr, status, error) {},
			})
		});
*/?>
	});
	
	function getList(){
		$.ajax({
			type : "post",
			url : "/mypage/getPaymentList",
			dataType : 'json',
			//data : { 'idx': idx },
			success : function(response)  {
				if( response.result == 'success') {
					console.log(response.list);
					var html = '';
					$.each(response.list, function (index, item) {
						html += list_html(item.nSeqNo, item.vStartDate, item.vEndDate, item.nOrderStatus, item.vGoodName);
					});
					$('#dataList').html(html);
				}
				if( response.result == 'error' ) Cmmn.alertMsg(response.msg);
				
			},
			error : function(xhr, status, error) {},
		});
	}

	function list_html(idx, stdate, endate, state, goods) {
		var html = '',
			str = '';


		if( stdate == null ) stdate = '';
		if( endate == null ) endate = '';

		if( state == '0' ) str = "<td><button type=\"button\" class=\"btn_s_round type_long bg_red\" data-remodal-target=\"confirmMsg\" data-idx=\"%%IDX%%\" onclick=\"setIdx('%%NO%%')\">해지 신청</button></td>";
		else str = "<td><button type=\"button\" class=\"btn_s_round bg_red\" disabled>종료</button></td>";

		html +=	"<tr>";
		html +=	"	<td>%%STDATE%%</td>";
		html +=	"	<td>%%ENDATE%%</td>";
		html +=	"	<td class='f_bold'>%%GOODNAME%%</td>";
		html +=	str;
		html +='</tr>';			
					
		html = html.replace("%%STDATE%%", stdate.substr(0,10));
		html = html.replace("%%ENDATE%%", endate.substr(0,10));
		html = html.replace("%%GOODNAME%%", goods);
		html = html.replace("%%IDX%%", idx);
		html = html.replace("%%NO%%", idx);
		
		return html;	
	}


	function setIdx(no){
		idx = no;
	}


	</script>