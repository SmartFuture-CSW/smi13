<style>
	.stock .vleft{text-align:left;padding:0px 6px;}
</style>
<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="/mypage" aria-label="뒤로가기"></a>
				<h2>포인트 내역</h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main has_bottom_banner">
			
		<form name="filter" method="post">
			<select class="form-control" name="pointKind" id="pointKind" style="appearance:revert; height:30px;padding:0px 3px; margin:10px 0px 0px 5px;">
				<option value="">구분</option>
				<option value="1" <?=($pointKind == 1) ? "selected" : ""; ?>>적립</option>
				<option value="2" <?=($pointKind == 2) ? "selected" : ""; ?>>사용</option>
			</select>
		</form>

			<div class="table_container" style="margin-top:14px;">
				<table>
					<colgroup>
						<col style="width:12%">
						<col>
						<col style="width:18%">
						<col style="width:18%">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">구분</th>
							<th scope="col">내역</th>
							<th scope="col">포인트 현황</th>
							<th scope="col">날짜</th>
						</tr>
					</thead>
					<tbody>
			
		<?php	if(!empty($list)){
					foreach ($list as $key => $value) {
						?>
						<tr class="stock">
							<td><?=$arrPointKind[$value['nPointKind']]?></td>
							<td><?=$value['vDescription']?></td>
							<td class="f_emphasis"><?=number_format($value['nPoint'])?></td>
							<td><?=substr($value['dtRegDate'],0,10)?></td>
						</tr>

		<?php		}
				}
				else{  ?>
					<tr><td colspan="3">내역이 없습니다.</td></tr>
		<?php
				}
		?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
				<?php foreach ($banner as $key => $value) { ?>
				<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
				<?php }?>
			</div>
			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>
<script>
$(function(){
	$(".back > a").on("click", function(){
		history.back(-1);
	});

	$("#pointKind").off("change").on("change", function(){
		
		$("form[name=filter]").submit();
	});
});
</script>
