
	<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="#none" aria-label="뒤로가기"></a>
				<h2>공지사항</h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main mypg_edit_card has_bottom_banner">
			<div class="board_list">

<?php
	foreach($notice as $row){
		
?>
				<a href="/mypage/view/<?php echo $row['nSeqNo']?>" class="<?php echo ($row['dayDiff'] < 7) ? 'new' : ''; ?>">
					<h5><?php echo $row['vSubject']?></h5>
					<p>
						<span>조회수 <?php echo $row['nHit']?></span>
						<span><?=substr($row['dtRegDate'], 0, 10)?></span>
					</p>
				</a>
<?php }?>

			</div>

		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
			<?php foreach ($banner as $key => $value) { ?>
				<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a> 
			<?php }?>
			</div>
			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

<script>

$(function(){
	$(".back > a").on("click", function(){
		history.back(-1);
	});
	/* Body background-color change */
	$('body').css('background-color', '#fff');
});

</script>