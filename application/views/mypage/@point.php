<style>
	.stock .vleft{text-align:left;padding:0px 6px;}
</style>
<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="/mypage" aria-label="뒤로가기"></a>
				<h2>포인트 내역</h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main has_bottom_banner">
			<div class="table_container" style="margin-top:14px;">
				<table>
					<colgroup>
						<col>
						<col style="width:12%">
						<col style="width:18%">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">내용</th>
							<th scope="col">포인트</th>
							<th scope="col">날짜</th>
						</tr>
					</thead>
					<tbody>
			
		<?  	if(!empty($list)){
					foreach ($list as $key => $value) {
						?>
						<tr class="stock">
							<td class="vleft"><?=$value['vDescription']?></td>
							<td class="f_emphasis"><?=number_format($value['nPoint'])?></td>
							<td><?=substr($value['dtRegDate'],0,10)?></td>
						</tr>

		<?  		}
				}else{  ?>

					<tr><td colspan="3">내역이 없습니다.</td></tr>

		<?		} 		?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
				<? foreach ($banner as $key => $value) { ?>

            	<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
        
        		<?}?>
			</div>

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

<script>
$(function(){
	$(".back > a").on("click", function(){
		history.back(-1);
	});
});
</script>
