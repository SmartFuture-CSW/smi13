
	<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="#none" aria-label="뒤로가기"></a>
				<h2><?=$view['vSubject']?></h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main has_bottom_banner">

			<div class="board_header">
				<h2  class="<?php echo ($view['dayDiff'] < 7) ? 'new' : ''; ?>"><?=$view['vSubject']?></h2>
				<p>
					<span>조회수 <?=$view['nHit']?></span>
					<span><?=substr($view['dtRegDate'], 0, 10)?></span>
				</p>
			</div>

			<div class="board_content">
				<p><?=$view['txContent']?>
			</div>
			
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
			<?php foreach ($banner as $key => $value) { ?>
				<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a> 
			<?php }?>
			</div>
			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

<script>

$(function(){
	$(".back > a").on("click", function(){
		history.back(-1);
	});
	/* Body background-color change */
	$('body').css('background-color', '#fff');
});

</script>