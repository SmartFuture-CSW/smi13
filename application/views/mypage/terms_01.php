

<div class="app_wrapper">
	
	<!-- [Start] App Header -->
    <header class="app_header">
      <div class="back">
        <a href="#" aria-label="뒤로가기"></a>
        <h2>서비스 이용약관</h2>
      </div>
    </header>
    <!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main">
			<div class="layout_center" style="white-space: pre-line;">
				<?=$terms['txTerms']?>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<!-- In Script -->
	<script>
		(function(){
			/* Body background-color change */
			$('body').css('background-color', '#fff');
			$(".back > a").on("click", function(){
				history.back(-1);
			});
		})();
	</script>