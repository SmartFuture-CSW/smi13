	

	<div class="app_wrapper">
		
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="confirm">
				<a href="#" class="history_back" aria-label="뒤로가기"></a>
				<h2>비밀번호 변경</h2>
				<button class="completed" type="button">완료</button>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main mypg has_bottom_banner">

			<div class="edit_password_container">
				<div class="input_box">
					<input type="password" name="passwd" id="passwd" placeholder="현재 비밀번호 입력">
				</div>
				<div class="input_box">
					<input type="password" name="passwd_new" id="passwd_new" placeholder="새 비밀번호 입력">
				</div>
				<P class="input_notice">- 비밀번호는 알파벳, 숫자 조합 6자리 이상</P>
				<div class="input_box">
					<input type="password" name="passwd_new_confirm" id="passwd_new_confirm" placeholder="새 비밀번호 확인">
				</div>
			</div>

		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
				<? foreach ($banner as $key => $value) { ?>
            		<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a> 
        		<?}?>
			</div>

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>

	<!-- In Script -->
	<script>
		$(document).ready(function(){
			/* Body background-color change */
			$('body').css('background-color', '#fff');

			// 로그인버튼 
		    $(".completed").click(function(){ 
		    	//Cmmn.alertMsg('test'); 
		    	$.ajax({
	                type : "post",
	                url : "/mypage/modifyPasswd",
	                dataType : 'json',
	                data : { 
	                	'passwd': $('#passwd').val(), 
	                	'passwd_new': $('#passwd_new').val(), 
	                	'passwd_new_confirm': $('#passwd_new_confirm').val(),
	                },

	                success : function(response) {
	                	Cmmn.log(response);
	                    if( response.result == 'success') {
	                    	//Cmmn.alertMsg(response.msg);
	                    	alert(response.msg);
	                        location.replace(response.url);
	                    }else{
	                    	Cmmn.alertMsg(response.msg);
	                    }

	                },

	                error : function(xhr, status, error) {},
	            })  
	        
	    	});

			$(".history_back").on("click", function(){
				history.back(-1);
			});

		});
	</script>