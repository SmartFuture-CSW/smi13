<?
	//setlocale(LC_CTYPE, 'ko_KR.euc-kr');
    // 결제 환경설정파일
    include $_SERVER["DOCUMENT_ROOT"]."/kcp/cfg/site_conf_inc.php";
   
	/* kcp와 통신후 kcp 서버에서 전송되는 결제 요청 정보 */
    $req_tx          = (isset($_POST[ "req_tx"         ]) == true) ? $_POST[ "req_tx"         ] : ''; // 요청 종류         
    $res_cd          = (isset($_POST[ "res_cd"         ]) == true) ? $_POST[ "res_cd"         ] : ''; // 응답 코드         
    $tran_cd         = (isset($_POST[ "tran_cd"        ]) == true) ? $_POST[ "tran_cd"        ] : ''; // 트랜잭션 코드     
    $ordr_idxx       = (isset($_POST[ "ordr_idxx"      ]) == true) ? $_POST[ "ordr_idxx"      ] : ''; // 쇼핑몰 주문번호   
    $good_name       = (isset($_POST[ "good_name"      ]) == true) ? $_POST[ "good_name"      ] : ''; // 상품명            
    $good_mny        = (isset($_POST[ "good_mny"       ]) == true) ? $_POST[ "good_mny"       ] : ''; // 결제 총금액       
    $buyr_name       = (isset($_POST[ "buyr_name"      ]) == true) ? $_POST[ "buyr_name"      ] : ''; // 주문자명          
    $buyr_tel1       = (isset($_POST[ "buyr_tel1"      ]) == true) ? $_POST[ "buyr_tel1"      ] : ''; // 주문자 전화번호   
    $buyr_tel2       = (isset($_POST[ "buyr_tel2"      ]) == true) ? $_POST[ "buyr_tel2"      ] : ''; // 주문자 핸드폰 번호
    $buyr_mail       = (isset($_POST[ "buyr_mail"      ]) == true) ? $_POST[ "buyr_mail"      ] : ''; // 주문자 E-mail 주소
    $use_pay_method  = (isset($_POST[ "use_pay_method" ]) == true) ? $_POST[ "use_pay_method" ] : ''; // 결제 방법         
    $enc_info        = (isset($_POST[ "enc_info"       ]) == true) ? $_POST[ "enc_info"       ] : ''; // 암호화 정보       
    $enc_data        = (isset($_POST[ "enc_data"       ]) == true) ? $_POST[ "enc_data"       ] : ''; // 암호화 데이터     
    $cash_yn         = (isset($_POST[ "cash_yn"        ]) == true) ? $_POST[ "cash_yn"        ] : '';
    $cash_tr_code    = (isset($_POST[ "cash_tr_code"   ]) == true) ? $_POST[ "cash_tr_code"   ] : '';
    /* 기타 파라메터 추가 부분 - Start - */
    $param_opt_1    = (isset($_POST[ "param_opt_1"     ]) == true) ? $_POST[ "param_opt_1"     ] : ''; // 기타 파라메터 추가 부분
    $param_opt_2    = (isset($_POST[ "param_opt_2"     ]) == true) ? $_POST[ "param_opt_2"     ] : ''; // 기타 파라메터 추가 부분
    $param_opt_3    = (isset($_POST[ "param_opt_3"     ]) == true) ? $_POST[ "param_opt_3"     ] : ''; // 기타 파라메터 추가 부분
    /* 기타 파라메터 추가 부분 - End -   */

	$tablet_size     = "1.0"; // 화면 사이즈 고정
	$url = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>

<!-- 거래등록 하는 kcp 서버와 통신을 위한 스크립트-->
<script type="text/javascript" src="<?=KCP_JS?>/approval_key.js"></script>

<form name="order_info" method="post" accept-charset="euc-kr">
	<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back"><a href="javascript:window.history.back(-1)" aria-label="뒤로가기"></a><h2>상품구매</h2></div>
		</header><!-- [End] App Header -->
		
		<!-- [Start] App Main -->
		<div class="app_main has_bottom_button">
			<div class="item_purchase">
				<dl class="clearfix">
					<dt>상품명</dt>
					<dd>노다지 종목구매</dd>
				</dl>
				<dl class="clearfix">
					<dt>상품금액</dt>
					<dd><?=number_format($amount)?>원</dd>
				</dl>
				<dl class="clearfix">
					<dt>보유 포인트</dt>
					<dd><?=$point?>P</dd>
				</dl>
				<dl class="clearfix">
					<dt>사용 포인트</dt>
					<dd>
						<!-- 일반 상품은 input엘리먼트를 사용해주시고 포인트 사용이 불가한 구독 상품은 input과 단위(P)를 제외한 strong 엘리먼트만 사용해주세요 -->
						<input type="tel" id="point" name="point" value="" onkeyup="pointCheck(this)">P
						<!-- <strong>*구독 상품은 포인트 사용불가합니다</strong> -->
					</dd>
				</dl> 
				<dl class="clearfix">
					<dt>부가세</dt>
					<dd>0원</dd>
				</dl>
				<dl class="total clearfix">
					<dt>총 구매 금액</dt>
					<dd id="total"></dd>
				</dl>
			</div>

			<? include_once('inc_purchase.php'); ?>

		</div><!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<button type="button" class="btn_l full bg_red" onclick="return submitCheck();">구매 유의사항 확인 및 결제하기</button>
			<!-- 금액 조건이 만족될시에만 disabled 제거 -->

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>
	<!-- 공통정보 -->

    <input type="hidden" name="ordr_idxx"  		value="">
    <input type="hidden" name="good_name" 		value="노다지 종목구매">
    <input type="hidden" name="good_mny"  		value="">
    <input type="hidden" name="buyr_mail"  		value="">
    <input type="hidden" name="buyr_tel2" 		value="">
    <input type="hidden" name="ActionResult" 	value="card">
  	<input type="hidden" name="req_tx"          value="pay">                           <!-- 요청 구분 -->
    <input type="hidden" name="shop_name"       value="<?= $g_conf_site_name ?>">      <!-- 사이트 이름 --> 
	<input type="hidden" name="site_cd"         value="<?= $g_conf_site_cd   ?>">      <!-- 사이트 코드 -->
	<input type="hidden" name="currency"        value="410"/>                          <!-- 통화 코드 -->
	<input type="hidden" name="eng_flag"        value="N"/>                            <!-- 한 / 영 -->
	<input type="hidden" name="approval_key"    id="approval">						   <!-- 결제등록 키 -->
	<!-- 인증시 필요한 파라미터(변경불가)-->
	<input type="hidden" name="escw_used"       value="N">
	<input type="hidden" name="pay_method"      value="">
	<input type="hidden" name="van_code"        value="">
	<!-- 신용카드 설정 -->
	<input type="hidden" name="quotaopt"        value="12"/>                           <!-- 최대 할부개월수 -->
	<!-- 가상계좌 설정 -->
	<input type="hidden" name="ipgm_date"       value=""/>
	<!-- 가맹점에서 관리하는 고객 아이디 설정을 해야 합니다.(필수 설정) -->
	<input type="hidden" name="shop_user_id"    value=""/>
	<!-- 복지포인트 결제시 가맹점에 할당되어진 코드 값을 입력해야합니다.(필수 설정) -->
	<input type="hidden" name="pt_memcorp_cd"   value=""/>
	<!-- 현금영수증 설정 -->
	<input type="hidden" name="disp_tax_yn"     value="Y"/>
	<!-- 리턴 URL (kcp와 통신후 결제를 요청할 수 있는 암호화 데이터를 전송 받을 가맹점의 주문페이지 URL) -->
	<input type="hidden" name="Ret_URL"         value="<?=$url?>">
	<!-- 화면 크기조정 -->
	<input type="hidden" name="tablet_size"     value="<?=$tablet_size?>">
	<!-- 추가 파라미터 ( 가맹점에서 별도의 값전달시 param_opt 를 사용하여 값 전달 ) -->
	<input type="hidden" name="param_opt_1"     value="point">
	<input type="hidden" name="param_opt_2"     value="<?=$point?>">    <!-- user 포인트 -->
	<input type="hidden" name="param_opt_3"     value="<?=$amount?>">	<!-- 최초 원금 --> 

</form>



<link href="/kcp/mobile_sample/css/style_mobile.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">

var price = <?=$amount?>;		// 상품 금액 나중에 컨트롤러에서 넘겨줘야댐
var userPoint = <?=$point?>;	// 유저가 소유한 포인트 
var point = 0;					// 유저가 사용하고 남은 포인트 
var totalPrice = <?=$amount?>;				// 최종 결제 금액 -> price - point = 

$("#total").html(price.toLocaleString()+'원');

function pointCheck(obj){
	
	if( userPoint < obj.value ){
		Cmmn.alertMsg('보유 포인트를 초과하였습니다.');
		obj.value = '';
		$("#total").html(price.toLocaleString()+'원');
		return false; 
	}else if( obj.value > price ){
		Cmmn.alertMsg('결제 금액을 초과하였습니다.');
		obj.value = '';
		$("#total").html(price.toLocaleString()+'원');
		return false;
	}else{
		totalPrice = price - obj.value;
		point = userPoint - obj.value;
		$("#total").html(totalPrice.toLocaleString()+'원');
	}
	
}

function submitCheck(){
	var form = document.order_info;
	form.good_mny.value = totalPrice;

	// 포인트 완납 프로세스 
	if( price == form.point.value ){
		//Cmmn.alertMsg(price+' : '+form.point.value);
		form.action = '/pay/pointPay';
		form.submit();
	}
	// 최소 결제금액 1000원 이상 
	else if( totalPrice < 1000 ){
		Cmmn.alertMsg('최소 결제금액은 1000원이상 입니다.');
	}
	// 결제 프로세스 
	else{
        kcp_AJAX('point');
    }
	

}

</script>

<script type="text/javascript">
 	$(document).ready(function(){
 		jsf__chk_type();
 		init_orderid();
 		chk_pay();
 	});

	/* 주문번호 생성 예제 */
	function init_orderid()
	{
		var today = new Date();
		var year  = today.getFullYear();
		var month = today.getMonth() + 1;
		var date  = today.getDate();
		var time  = today.getTime();

		if (parseInt(month) < 10)
		  month = "0" + month;

		if (parseInt(date) < 10)
		  date  = "0" + date;

		var order_idxx = "NDG_TEST" + year + "" + month + "" + date + "" + time;
		var ipgm_date  = year + "" + month + "" + date;

		document.order_info.ordr_idxx.value = order_idxx;
		document.order_info.ipgm_date.value = ipgm_date;
	}

	/* kcp web 결제창 호츨 (변경불가) */
	function call_pay_form()
	{
		var v_frm = document.order_info; 

		//v_frm.action = PayUrl;
		if(v_frm.encoding_trans == undefined) {
        	v_frm.action = PayUrl;
	    } else {
	        if(v_frm.encoding_trans.value == "UTF-8") {
	            v_frm.action = PayUrl.substring(0,PayUrl.lastIndexOf("/")) + "/jsp/encodingFilter/encodingFilter.jsp";
	            v_frm.PayUrl.value = PayUrl;
	        } else {
	            v_frm.action = PayUrl;
	        }
	    }

		if (v_frm.Ret_URL.value == "")
		{
		  /* Ret_URL값은 현 페이지의 URL 입니다. */
		  alert("연동시 Ret_URL을 반드시 설정하셔야 됩니다.");
		  return false;
		}
		else
		{
		  v_frm.submit();
		}
	}

	/* kcp 통신을 통해 받은 암호화 정보 체크 후 결제 요청 (변경불가) */
	function chk_pay()
	{
		self.name = "tar_opener";
		var pay_form = document.pay_form;

		if (pay_form.res_cd.value == "3001" )
		{
		  alert("사용자가 취소하였습니다.");
		  pay_form.res_cd.value = "";
		}

		if (pay_form.enc_info.value)
		  pay_form.submit();
	}

	function jsf__chk_type()
	{
		document.order_info.ActionResult.value == "card";
		document.order_info.pay_method.value = "CARD";
	}
</script>


<form name="pay_form" method="post" action="/kcp/mobile_sample/pp_cli_hub.php" accept-charset="euc-kr">
    <input type="hidden" name="req_tx"         value="<?=$req_tx?>">               <!-- 요청 구분          -->
    <input type="hidden" name="res_cd"         value="<?=$res_cd?>">               <!-- 결과 코드          -->
    <input type="hidden" name="tran_cd"        value="<?=$tran_cd?>">              <!-- 트랜잭션 코드      -->
    <input type="hidden" name="ordr_idxx"      value="<?=$ordr_idxx?>">            <!-- 주문번호           -->
    <input type="hidden" name="good_mny"       value="<?=$good_mny?>">             <!-- 휴대폰 결제금액    -->
    <input type="hidden" name="good_name"      value="<?=$good_name?>">            <!-- 상품명             -->
    <input type="hidden" name="buyr_name"      value="<?=$buyr_name?>">            <!-- 주문자명           -->
    <input type="hidden" name="buyr_tel1"      value="<?=$buyr_tel1?>">            <!-- 주문자 전화번호    -->
    <input type="hidden" name="buyr_tel2"      value="<?=$buyr_tel2?>">            <!-- 주문자 휴대폰번호  -->
    <input type="hidden" name="buyr_mail"      value="<?=$buyr_mail?>">            <!-- 주문자 E-mail      -->
	<input type="hidden" name="cash_yn"		   value="<?=$cash_yn?>">              <!-- 현금영수증 등록여부-->
    <input type="hidden" name="enc_info"       value="<?=$enc_info?>">
    <input type="hidden" name="enc_data"       value="<?=$enc_data?>">
    <input type="hidden" name="use_pay_method" value="<?=$use_pay_method?>">
    <input type="hidden" name="cash_tr_code"   value="<?=$cash_tr_code?>">

    <!-- 추가 파라미터 -->
	<input type="hidden" name="param_opt_1"	   value="<?=$param_opt_1?>">
	<input type="hidden" name="param_opt_2"	   value="<?=$param_opt_2?>">
	<input type="hidden" name="param_opt_3"	   value="<?=$param_opt_3?>">
</form>
