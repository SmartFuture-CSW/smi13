
<div class="app_wrapper">	
		<!-- [Start] App Header -->
		<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
		<!-- [End] App Header -->
		
		<!-- [Start] App Main -->
		<div class="app_main subs02">
			<div class="subscribe_order layout_center">
<form name="pay" method="post">
	<input type="hidden" name="userPoint" value="<?=$userPoint?>">
	<input type="hidden" name="usePoint" value="<?=$usePoint?>">
	<input type="hidden" name="nAmount" value="<?=$nAmount?>">
	<input type="hidden" name="emPayYN" value="<?=$emPayYN?>">
				<div class="form_container">
					<div class="input_box">
						<label for="vCardNo">카드번호</label>
						<input type="tel" name="vCardNo" id="vCardNo" placeholder="'-'없이 숫자만 입력하세요" value="<?=$vCardNo?>" <?=($emPayYN == "Y")?"readonly" : "" ?>>
					</div>
					<div class="input_box">
						<label for="vExpdt">유효기간</label>
						<input type="tel" name="vExpdt" id="vExpdt" placeholder="입력예시(월/년) : 0525" value="<?=$vExpdt?>"  <?=($emPayYN == "Y")?"readonly" : "" ?>>
					</div>
					<div class="input_box">
						<label for="vName">주문자명</label>
						<input type="text" name="vName" id="vName" placeholder="특수문자 사용금지" value="<?=$vName?>"  <?=($emPayYN == "Y")?"readonly" : "" ?>>
					</div>
					<div class="input_box">
						<label for="vPhone">전화번호</label>
						<input type="tel" name="vPhone" id="vPhone" placeholder="'-'없이 숫자만 입력하세요" value="<?=$vPhone?>"  <?=($emPayYN == "Y")?"readonly" : "" ?>>
					</div>
				</div>

<?php /*				
				<label class="checkbox type_agree">
					<input type="checkbox" name="getPayInfo" id="getPayInfo">
					<i></i>기존 결제 정보 사용
				</label>
*/?>
				<div class="bottom_check">
					<label class="checkbox type_agree">
						<input type="checkbox" name="allCheck" id="allCheck">
						<i></i>아래 사항에 모두 동의 합니다.
					</label><br>
					<label class="checkbox type_agree" for="service_agree">
						<input type="checkbox" name="agree[]" class="agree" id="service_agree" value="y" >
						<i></i>서비스 이용약관 동의
					</label>
					<button type="button" data-remodal-target="popTermService" style="margin-right:0.5em">[필수]</button><br>
					<label class="checkbox type_agree" for="info_agree">
						<input type="checkbox" name="agree[]" class="agree" id="info_agree" value="y" >
						<i></i>개인정보 수집 및 이용동의
					</label>
					<button type="button" data-remodal-target="popTermPersonalInfo">[필수]</button><br>
					<label class="checkbox type_agree">
						<input type="checkbox" name="agree[]" class="agree" id="marketing_agree" value="y" >
						<i></i>마케팅정보 수신 동의
					</label>
					<button type="button" data-remodal-target="popTermMarketing">[선택]</button>
				</div>

				<div class="btn_container" style="margin-top:20px;">
					<button type="button" class="btn_l full bg_red btn-buy">추천종목 구매하기</button>
				</div>

				<img src="../asset/img/banner_authentication.jpg" alt="" class="img_banner">
</form>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<!-- [Start] Popup - Terms:서비스 이용약관 -->
	<div class="remodal terms" data-remodal-id="popTermService">
		<div class="term_container">
		<p><?=$terms['txTerms']?><?//php include_once (TEXT."/service_agree.txt"); ?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:서비스 이용약관 -->

	<!-- [Start] Popup - Terms:개인정보 수집 및 이용 -->
	<div class="remodal terms" data-remodal-id="popTermPersonalInfo">
		<div class="term_container">
		<p><?=$terms['txPrivacy']?><?//php include_once (TEXT."/info_agree.txt"); ?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:개인정보 수집 및 이용 -->

	<!-- [Start] Popup - Terms:마케팅정보 수신 동의 -->
	<div class="remodal terms" data-remodal-id="popTermMarketing">
		<div class="term_container">
		<p><?=$terms['txAdterms']?><?//php include_once (TEXT."/marketing_agree.txt"); ?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:마케팅정보 수신 동의 -->
<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>
<div class="remodal alert" data-remodal-id="ing" id="ing"><p>결제가 진행중 입니다.</p></div>
<!-- in script -->
<script>
(function(){


	var process = false;

	// 전체 체크 
	$("#allCheck").change(function(){
		if($(this).is(':checked') == true){
			$(".agree").prop('checked', true);
		}
		else{
			$(".agree").prop('checked', false);
		}
	});

	// 체크박스 한개라도 체크시 전체 체크사라지게 
	$("input[name='agree[]']").change(function(){ 
		var agree_1 = Is.checkBox( 'info_agree'),
			agree_2 = Is.checkBox( 'service_agree'),
			agree_3 = Is.checkBox( 'marketing_agree'); 
		if(agree_1==true && agree_2==true && agree_3==true) $('#allCheck').prop('checked', true);
		else $('#allCheck').prop('checked', false);
	});


	// 구독처리
	$(".btn-buy").on("click", function(){

		var card_no = $("#vCardNo").val();
		var expdt = $("#vExpdt").val();
		var name = $("#vName").val();
		var phone = $("#vPhone").val();

		if ( !card_no || card_no == "" || card_no == null || card_no == undefined ) {
			Cmmn.alertMsg("카드번호를 작성해주세요.");
			return false;
		}
		<?php if($emPayYN != "Y"){?>
		if ( !(/^[0-9]+$/).test(card_no) ){
			Cmmn.alertMsg("카드번호는 숫자만 가능합니다.");
			return false;
		}
		<?php }?>

		if ( !expdt || expdt == "" || expdt == null || expdt == undefined ) {
			Cmmn.alertMsg("유효기간을 작성해주세요.");
			return false;
		}
		<?php if($emPayYN != "Y"){?>
		if ( !(/^[0-9]+$/).test(expdt) ){
			Cmmn.alertMsg("유효기간은 숫자만 가능합니다.");
			return false;
		}
		<?php }?>

		if ( !name || name == "" || name == null || name == undefined ) {
			Cmmn.alertMsg("이름을 작성해주세요.");
			return false;
		}

		if ( !phone || phone == "" || phone == null || phone == undefined ) {
			Cmmn.alertMsg("전화번호를 작성해주세요.");
			return false;
		}
		<?php if($emPayYN != "Y"){?>
		if ( !(/^[0-9]+$/).test(phone) ){
			Cmmn.alertMsg("전화번호는 숫자만 가능합니다.");
			return false;
		}
		<?php }?>

		// 동의 체크 
		if( !Is.checkBox( 'service_agree') ) {
			Cmmn.alertMsg('서비스 이용약관 이용에 동의해야합니다.');
			return false;
		}
		if( !Is.checkBox( 'info_agree') ) {
			Cmmn.alertMsg('개인정보 수집 이용에 동의해야합니다.');
			return false;
		}

		var u = "/pay/postPayInfo";
		var data = $("form[name=pay]").serialize();

		Cmmn.alertId('ing');
		if(process != false){
			Cmmn.alertId('ing');
			return;
		}

		process = true;

		$.post({
			type : "post", url : u, dataType : 'json', data : data,
			success : function(response) {
				console.log(response);
				if( response.result == 'DOUBLE' ) {  
					location.href = "/stock";
					process = false;
				}
				if( response.result == 'FAIL' ) {  
					Cmmn.alertMsg(response.msg);
					process = false;
				}
				if( response.result == 'SUCCESS') {
					location.replace(response.referer);
				}
			},
			error : function(xhr, status, error) {},
		})
	});


	$("#getPayInfo").on("click", function(){
		if($(this).is(":checked") == true){
			//console.log('check');
			var u = "/purchase/getPayInfo";
			var data = {};
			$.post({
				type : "post", url : u, dataType : 'json', data : data,
				success : function(response) {
					console.log(response);
					if( response.result == 'FAIL' ) {  
						Cmmn.alertMsg(response.msg);
				//		$("#getPayInfo").prop("checked", false);
						return;
					}else if( response.result == 'SUCCESS') {
						
						var vCardNo = response.vCardNo
						, vName = response.vName
						, vExpdt = response.vExpdt
						, vPhone = response.vPhone;
						$("#vCardNo").val(vCardNo);
						$("#vExpdt").val(vExpdt);
						$("#vName").val(vName);
						$("#vPhone").val(vPhone);

					//	Cmmn.alertMsg('결제가 완료 되었습니다.');
					}
				},
				error : function(xhr, status, error) {},
			})
		}
		else{
			$("#vCardNo").val('');
			$("#vExpdt").val('');
			$("#vName").val('');
			$("#vPhone").val('');
		}
	});
})();
</script>