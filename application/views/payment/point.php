
<form name="order_info" method="post" accept-charset="euc-kr">
	<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back"><a href="javascript:window.history.back(-1)" aria-label="뒤로가기"></a><h2>상품구매</h2></div>
		</header><!-- [End] App Header -->
		
		<!-- [Start] App Main -->
		<div class="app_main has_bottom_button">
			<div class="item_purchase">
				<dl class="clearfix">
					<dt>상품명</dt>
					<dd>노다지 종목구매</dd>
				</dl>
				<dl class="clearfix">
					<dt>상품금액</dt>
					<dd><?=number_format($amount)?>원</dd>
				</dl>
				<dl class="clearfix">
					<dt>보유 포인트</dt>
					<dd><?=number_format($point)?>P</dd>
				</dl>
				<dl class="clearfix">
					<dt>사용 포인트</dt>
					<dd>
						<!-- 일반 상품은 input엘리먼트를 사용해주시고 포인트 사용이 불가한 구독 상품은 input과 단위(P)를 제외한 strong 엘리먼트만 사용해주세요 -->
						<input type="tel" id="point" name="point" value="0" onkeyup="pointCheck(this)">P
						<!-- <strong>*구독 상품은 포인트 사용불가합니다</strong> -->
					</dd>
				</dl> 
				<dl class="clearfix">
					<dt>부가세</dt>
					<dd>0원</dd>
				</dl>
				<dl class="total clearfix">
					<dt>총 구매 금액</dt>
					<dd id="total"></dd>
				</dl>
			</div>

			<? include_once('inc_purchase.php'); ?>

		</div><!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<button type="button" class="btn_l full bg_red" onclick="return submitCheck();">구매 유의사항 확인 및 결제하기</button>
			<!-- 금액 조건이 만족될시에만 disabled 제거 -->

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>
	<!-- 공통정보 -->


	<input type="hidden" name="price" value="<?=$amount?>">
	<input type="hidden" name="userPoint" value="<?=$point?>">
	<input type="hidden" name="stockNo" value="<?=$nStockNo?>">
</form>


<link href="/kcp/mobile_sample/css/style_mobile.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">

var price = <?=$amount?>;		// 상품 금액 나중에 컨트롤러에서 넘겨줘야댐
var userPoint = <?=$point?>;	// 유저가 소유한 포인트 
var point = 0;					// 유저가 사용하고 남은 포인트 
var totalPrice = <?=$amount?>;				// 최종 결제 금액 -> price - point = 

$("#total").html(price.toLocaleString()+'원');

function pointCheck(obj){
	if( userPoint < obj.value ){
		Cmmn.alertMsg('보유 포인트를 초과하였습니다.');
		obj.value = '';
		$("#total").html(price.toLocaleString()+'원');
		return false; 
	}
	else if( obj.value > price ){
		Cmmn.alertMsg('결제 금액을 초과하였습니다.');
		obj.value = '';
		$("#total").html(price.toLocaleString()+'원');
		return false;
	}
	else{
		totalPrice = price - obj.value;
		point = userPoint - obj.value;
		$("#total").html(totalPrice.toLocaleString()+'원');
	}
}

function submitCheck(){
	var form = document.order_info;

	// 포인트 완납 프로세스 
	if( price == form.point.value ){
		//Cmmn.alertMsg(price+' : '+form.point.value);
		form.action = '/pay/pointPay';
		form.submit();
	}
	// 최소 결제금액 1000원 이상 
	else if( totalPrice < 1000 ){
		Cmmn.alertMsg('최소 결제금액은 1000원이상 입니다.');
	}
	// 결제 프로세스 
	else{
		form.action = '/pay/card';
		form.submit();
	}
}
</script>

