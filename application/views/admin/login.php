<!DOCTYPE html>
<html lang="ko">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
  <title>관리자 페이지</title>

  <link rel="stylesheet" href="<?=MODULES_PATH?>/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=MODULES_PATH?>/ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=MODULES_PATH?>/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css">

  <link rel="stylesheet" href="<?=CSS_PATH?>/demo.css">
  <link rel="stylesheet" href="<?=CSS_PATH?>/style.css">
</head>

<body>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="login-brand">노다지 어드민 로그인</div>

            <div class="card card-primary">
              <div class="card-header"><h4>Login</h4></div>

              <div class="card-body">
                <form method="POST" id="frm" name="frm" action="/admin/loginCheck" class="needs-validation" novalidate="">
                  <div class="form-group">
                    <label for="userid">User ID</label>
                    <input type="text" class="form-control" id="userid" name="userid" tabindex="1" required autofocus>
                    <div class="invalid-feedback">Please fill in your email</div>
                  </div>

                  <div class="form-group">
                    <label for="password" class="d-block">Password
                    </label>
                    <input id="passwd" type="password" class="form-control" name="passwd" tabindex="2" required>
                    <div class="invalid-feedback">please fill in your password</div>
                  </div>

                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                      <label class="custom-control-label" for="remember-me">아이디 기억</label>
                    </div>
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block" tabindex="4">Login</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <script src="<?=MODULES_PATH?>/jquery.min.js"></script>
  <script src="<?=MODULES_PATH?>/popper.js"></script>
  <script src="<?=MODULES_PATH?>/tooltip.js"></script>
  <script src="<?=MODULES_PATH?>/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?=MODULES_PATH?>/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="<?=MODULES_PATH?>/moment.min.js"></script>
  <script src="<?=MODULES_PATH?>/scroll-up-bar/public/scroll-up-bar.min.js"></script>
  <script src="<?=JS_PATH?>/sa-functions.js"></script>

  <script src="<?=JS_PATH?>/js/scripts.js"></script>
  <script src="<?=JS_PATH?>/js/custom.js"></script>
  <script src="<?=JS_PATH?>/js/demo.js"></script>
</body>
</html>