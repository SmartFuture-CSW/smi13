<style>
.view_price{padding:5px 10px;border:1px solid silver;border-radius:4px;}
</style>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div><?=$title?></div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-header">
						<div class="float-right" style="width:100%;">
					<form name="frm" id="frm" method="post" action="/admin/payment/<?=$payType?>" style="width:100%;">
						<input type="hidden" name="pageNo" id="pageNo" value="1<?//=$pageNo;?>">
						<input type="hidden" name="ex" id="ex" value="">
							<div class="input-group" style="width:100%;">
								<span style="padding:0px;background-color:green;color:#fff;width:50px;border:0px;cursor:pointer;text-align:center;" onclick="ex()">엑셀</span>
								<select class="form-control" name="pmethod" id="pmethod" style="height:30px;padding:0px 3px;">
									<option value="">결제수단</option>
									<option value="C" <?php if(@$_POST['pmethod']=='C') echo 'selected';?>>신용카드</option>
									<option value="M" <?php if(@$_POST['pmethod']=='M') echo 'selected';?>>무통장입금</option>
									<option value="T" <?php if(@$_POST['pmethod']=='T') echo 'selected';?>>계좌이체</option>
								</select>
								<select class="form-control" name="gn" id="gn" style="height:30px;padding:0px 3px;">
									<option value="">상품구분</option>
									<option value="1" <?php if(@$_POST['gn']==1) echo 'selected';?>>기본구독</option>
									<option value="2" <?php if(@$_POST['gn']==2) echo 'selected';?>>프리미엄 문자반</option>
									<option value="3" <?php if(@$_POST['gn']==3) echo 'selected';?>>프리미엄VIP</option>
								</select>
								<select class="form-control" name="limit" id="limit" style="height:30px;padding:0px 3px;">
									<option value="">목록수</option>
									<option value="20" <?php if(@$_POST['limit']=='20') echo 'selected';?>>20개</option>
									<option value="50" <?php if(@$_POST['limit']=='50') echo 'selected';?>>50개</option>
								</select>
								<input type="text" name="reg1" id="reg1" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@($_POST['reg1']!=''?$_POST['reg1']:date("Y-m-01"))?>" placeholder="시작일 시작" autocomplete="off"> ~ 
								<input type="text" name="reg2" id="reg2" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@($_POST['reg2']!=''?$_POST['reg2']:date("Y-m-d"))?>" placeholder="시작일 종료" autocomplete="off">
								<select name="keyfield" class="form-control"  style="height:30px;padding:0px 3px; width:30px;">
									<option value="vName" <?php if(@$_POST['keyfield'] == 'vName') echo 'selected'; ?>>이름</option>
									<option value="vPhone" <?php if(@$_POST['keyfield'] == 'vPhone') echo 'selected'; ?>>연락처</option>
								</select>
								<input type="text" name="keyword" id="keyword" class="form-control" placeholder="검색어" value="<?=@$_POST['keyword'];?>">
								<div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
							</div>
							<div style="padding-top:10px;">
								<span class="view_price">조회기간 결제액 : <b style="color:#757575;"><?=number_format($searchDate)?>원</b></span>
								<?php if($payType == "vip"){?>
									<span class="view_price">당월 예상금액 : <b style="color:#757575;"><?=number_format($currentMonth)?>원</b></span>
									<span class="view_price"><?=date("m",strtotime("+1 month", time()))?>월 예상금액 : <b style="color:#757575;"><?=number_format($nextMonth)?>원</b></span>
								<?php }?>
							</div>
					</form>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
								<?$style=' style="padding:4px 10px;text-align:center;"'?>
										<th width="4%"<?=$style?>>번호</th>
										<th<?=$style?>>이름</th>
										<th<?=$style?>>전화번호</th>
										<?php if($payType != "point"){?>
										<th<?=$style?>>상품구분</th>
										<?php }?>
										<th<?=$style?>>상품명</th>
										<th<?=$style?>>상품가</th>
										<th<?=$style?>>실결제액</th>
										<?php if($payType != "point"){?>
										<th<?=$style?>>사용포인트</th>
										<?php }?>
										<th<?=$style?>>결제일</th>
										<?php if($payType != "point"){?>
										<th<?=$style?>>시작일</th>
										<th<?=$style?>>종료일</th>
										<?php }?>
										<th<?=$style?>>주문번호</th>
										<th<?=$style?>>결제수단</th>
									</tr>
								</thead>
								<tbody>
<?php

$arrGoodsKind = [
"1"=>"노다지 기본 구독",
"2"=>"프리미엄 문자반",
"3"=>"프리미엄 VIP",
null=>"종목구매"
];
$ca='';
foreach ($list as $value) {
	$title=$value['bank'].'&#13;'.$value['cardno'];
	if($value['nPrice']==0 && $value['nAmount']==$value['nPoint'] && $value['nPoint'] > 0){
		$method='전액 포인트결제';
		$title='';
	}
	else if($value['emMethod']=='C'){
		$method='신용카드';
	}
	else if($value['emMethod']=='M'){
		$method='무통장입금';
	}
	else if($value['emMethod']=='T'){
		$method='계좌이체';
	}

if($value['nOrderStatus']==0) $ca.=",".$value['nUserNo'];
?>
									<tr <?=($value['nOrderStatus']==1)? 'style="background-color:#ffeaea"' : ($value['nOrderStatus']==2?'style="background-color:yellow"':'') ?>>
										<td<?=$style?>><?=$no--?></td>
										<td<?=$style?> title="회원명 : <?=$value['uName']?> [<?=$value['nUserNo']?>]"><a href="/admin/payment/view/<?=$value['nSeqNo']?>"><?=$value['vName'];?></a></td>
										<td<?=$style?>>보안상 미기재</td>
										<?php if($payType != "point"){?>
										<td<?=$style?>><?=$arrGoodsKind[$value['vGoodsKind']];?></td>
										<?php }?>
										<td<?=$style?>><?=$value['vGoodName'];?></td>
										<td<?=$style?> align='center'><?=number_format($value['nAmount'])?>원</td>
										<td<?=$style?> align='center'><?=number_format($value['nPrice'])?>원</td>
										<?php if($payType != "point"){?>
										<td<?=$style?> align='center'><?=number_format($value['nPoint'])?></td>
										<?php }?>
										<td<?=$style?>><?=$value['dtRegDate'];?></td>
										<?php if($payType != "point"){?>
										<td<?=$style?>><?=$value['vStartDate'];?></td>
										<td<?=$style?>><?=$value['vEndDate'];?></td>
										<?php }?>
										<td<?=$style?>><?=$value['vOrderIdx'];?></td>
										<td<?=$style?> title="<?=$title?>"><?=$method?></td>
									</tr>
<?php } #foreach 
if(count($list)==0){?>
									<tr>
										<td colspan="12" align='center' valign='middle' height='120'><br><br>No Data.</td>
									</tr>
<?php
}
?>
								</tbody>
							</table>
						</div>
						<?=$paging?>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
					</div>
					<div class="modal-body">
						<p>Modal body text goes here.ddd
							<button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Closezzzz</button>
							<button type="button" class="btn btn-primary">Save changes</button>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">

function pageRemote(pageNo){
	$('#pageNo').val(pageNo);
	document.frm.submit();
}

function ex(){
	$('#ex').val('ex');
	document.frm.submit();
	$('#ex').val('');
}

function change_stat(no,agrno,name){
	if(agrno=='1') stat='미승인'; else stat='승인';
	$.post("/admin/payment/changestat",{cno:no,agrno:agrno},function(data){alert(name+'님의 결제 처리 상태가 '+stat+'으로 변경되었습니다.');document.location.href='/admin/payment';},'json');
}

</script>