
<style>


.btn_cancel{
	display: inline-block;
	height:34px;
    color: #fff;
    text-decoration: none;
    vertical-align: middle;
    line-height: 35px;
    height: 35px;
    padding: 0 10px;
    text-align: center;
    font-weight: bold;
    -webkit-transition: background-color 0.3s ease-out;
    -moz-transition: background-color 0.3s ease-out;
    -o-transition: background-color 0.3s ease-out;
    transition: background-color 0.3s ease-out;
	cursor:pointer;
}

.cancel_half{
	border:1px solid silver;
	/*#d13f4a*/
	background-color:yellow;
	color:#000 !important;
}

.cancel_all{

	border:1px solid silver;
	background-color:#d13f4a;
	color:#fff !important;
}
</style>
<script src="/public/admin/modules/jquery.min.js"></script>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"  style="background-color:<?=$view['bgcolor']?>"><div>결제 상세정보</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary"  style="background-color:<?=$view['bgcolor']?>">
					<div class="card-body">
						<div class="form-divider">주문 정보</div>
						<div class="row">
							<div class="form-group col-6">
								<label>주문번호</label>
								<div class="input-group"><?=$view['vOrderIdx']?></div>
							</div>
							<div class="form-group col-6">
								<label>결제수단</label>
								<div class="input-group"><?=$method[$view['emMethod']]?></div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label>구매상품명</label>
								<div class="input-group"><?=$view['vGoodName']?></div>
							</div>
							<div class="form-group col-6">
								<label>실결제금액</label>
								<div class="input-group"><?=number_format($view['nAmount'])?>원</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label>구매지</label>
								<div class="input-group"><?=$view['vName']?></div>
							</div>
							<div class="form-group col-6">
								<label>연락처</label>
								<div class="input-group">보안상 미기재</div>
							</div>
						</div>
					<?php if($view['nType'] != 6){?>
						<div class="row">
							<div class="form-group col-6">
								<label>구독 시작일</label>
								<div class="input-group"><?=$view['vStartDate']?></div>
							</div>
							<div class="form-group col-6">
								<label>구독 종료일</label>
								<div class="input-group"><?=$view['vEndDate']?></div>
							</div>
						</div>
					<?php }?>
						<div class="form-divider">결제 정보</div>
						<div class="row">
							<div class="form-group col-6">
								<label>결제상태</label>
								<div class="input-group"><?=$orderStatus[$view['nOrderStatus']]?><?if($view['nOrderStatus']==2){?> (취소액 : <?=number_format($view['nCancelPrice'])?>원)<?}?></div>
							</div>
							<div class="form-group col-6">
								<label>결제 디바이스</label>
								<div class="input-group"><?=$view['vDevice']?></div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label>Uid</label>
								<div class="input-group"><?=$view['vUid']?></div>
							</div>
							<div class="form-group col-6">
								<label>TNO</label>
								<div class="input-group"><?=$view['tno']?></div>
							</div>
						</div>

			<?php if($view['nRefundNo']){?>
						<div class="form-divider">환불 신청 정보</div>
						<div class="row">
							<div class="form-group col-6">
								<label>30일 이전 환불신청</label>
								<div class="input-group"><?=($view['nMonthCancel'] == "1") ? 'X' : 'O'; ?></div>
							</div>
							<div class="form-group col-6">
								<label>해지요청일</label>
								<div class="input-group"><?=$view['dtRefundDate']?></div>
							</div>
						</div>

						<div class="form-divider">환불 금액 정보</div>
						<div class="row">
							<div class="form-group col-3">
								<label>이용기간 및 차감금액</label>
								<div class="input-group"><?=number_format($view['nUseDatePrice'])?>원 (<?=$view['vUseDate']?>)</div>
							</div>
							<div class="form-group col-3">
								<label>사용 포인트 차감금액</label>
								<div class="input-group"><?=number_format($view['nUsePointPrice'])?>원</div>
							</div>
							<div class="form-group col-3">
								<label>취소 수수료 차감금액</label>
								<div class="input-group"><?=number_format($view['nCancelPrint'])?>원</div>
							</div>
							<div class="form-group col-3">
								<label><strong style="color:red;">실제 환불 금액</strong></label>
								<div class="input-group"><strong style="color:red;"><?=number_format($view['nRefundPrice'])?></strong>원</div>
							</div>
						</div>
			<?php } ?>
				
						<div class="row">
							<div class="form-group col-12" style="text-align:right;">
								<a class="btn" style="background-color:#f0f0f0;padding:5px 8px;border:1px solid silver;" href="javascript:history.back();"><i class="fa fa-newspaper" aria-hidden="true"></i> 목록</a>
								<?php if($view['nOrderStatus'] == 0 || $view['nOrderStatus'] == 3){ ?>
									<?php if($view['nType'] != 6){?>
										<a class="btn_cancel cancel_half"><i class="fa fa-times-circle" aria-hidden="true"></i> 부분취소</a>
										<!--a class="btn_cancel cancel_all"><i class="fa fa-times-circle" aria-hidden="true"></i> 취소</a-->
									<?php } ?>
									</div>
									<div id="cancelPrice" class="form-group col-12" style="text-align:right;display:none">
										<label for="cancel_price" style="display:inline; font-size:14px;">취소금액</label>
										<input type="text" name="cancel_price" id="cancel_price" class="form-control" style="width:150px; display:inline; height:32px;" value="<?=$view['nRefundPrice']?>"> 
										<a class="btn btn-primary btn-action cancel_half_action">확인</a>
								<?php } ?>
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


<script>

$(function(){

	var nPayNo = '<?=$view['nPayNo']?>';
	

	$('.cancel_all').on('click', function(){
		var u = '/admin/payment/postRefundInfo';
		var data = {
			'nPayNo' : nPayNo
			, 'sType' : 'all'
			, 'cancel_price ' : 0
		};
		$.post(u,data,function(response){
			alert('전체취소 하였습니다.');
			location.reload();
		});
	});

	$('.cancel_half').on('click', function(){

		if($("#cancelPrice").css("display") != "block"){
			$("#cancelPrice").css("display", "block");
		}else{
			$("#cancelPrice").css("display", "none");
		}

	});

	$(".cancel_half_action").on("click", function(){
		
		var cancel_price = $("#cancel_price").val();

		var u = '/admin/payment/postRefundInfo';
		var data = {
			'nPayNo' : nPayNo
			, 'sType' : 'half'
			, 'cancel_price' : cancel_price
		};

		$.post(u,data,function(response){
			alert('부분취소 하였습니다.');
			location.reload();
		});

		
	});
	
});

</script>