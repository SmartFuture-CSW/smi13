<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>정기구독 해지신청</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-header">
						<div class="float-right" style="width:100%;">
<form name="frm" id="frm" method="post" action="/admin/payment/refund" style="width:100%;">
                        <input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo;?>">
							<div class="input-group" style="width:100%;">
								<select class="form-control" name="limit" id="limit" style="height:30px;padding:0px 3px;">
									<option value="">목록수</option>
									<option value="20" <?php if(@$_POST['limit']=='20') echo 'selected';?>>20개</option>
									<option value="50" <?php if(@$_POST['limit']=='50') echo 'selected';?>>50개</option>
								</select>
								<input type="text" name="reg1" id="reg1" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@$_POST['reg1'];?>" placeholder="시작일 시작" autocomplete="off"> ~ 
								<input type="text" name="reg2" id="reg2" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@$_POST['reg2'];?>" placeholder="시작일 종료" autocomplete="off">
								<select name="keyfield" class="form-control"  style="height:30px;padding:0px 3px; width:30px;">
									<option value="vName" <?php if(@$_POST['keyfield'] == 'vName') echo 'selected'; ?>>이름</option>
									<option value="vPhone" <?php if(@$_POST['keyfield'] == 'vPhone') echo 'selected'; ?>>연락처</option>
								</select>
								<input type="text" name="keyword" id="keyword" class="form-control" placeholder="검색어" value="<?=@$_POST['keyword'];?>">
								<div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
							</div>
</form>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
								<?$style=' style="padding:4px 10px;text-align:center;"'?>
										<th width="4%"<?=$style?>>번호</th>
										<th<?=$style?>>이름</th>
										<th<?=$style?>>전화번호</th>
										<th<?=$style?>>상품명</th>
										<th<?=$style?>>실결제액</th>
										<th<?=$style?>>30일이전 해지요청</th>
										<th<?=$style?>>해지신청일</th>
										<th<?=$style?>>시작일</th>
										<th<?=$style?>>종료일</th>
										<th<?=$style?>>주문번호</th>
									</tr>
								</thead>
								<tbody>


<?php 
	
$today = strtotime(date('Y-m-d', time())." 23:59:59");
foreach($list as $value){ 

?>
									<tr <?=($value['nOrderStatus']==1)? 'style="background-color:#ffeaea"' : ($value['nOrderStatus']==2?'style="background-color:yellow"':'') ?>>
										<td<?=$style?>><?=$no--?></td>
										<td<?=$style?>><a href="/admin/payment/view/<?=$value['nPayNo']?>"><?=$value['vName'];?></a></td>
										<td<?=$style?>>보안상 미기재</td>
										<td<?=$style?>><?=$value['vGoodName'];?></td>
										<td<?=$style?>><?=number_format($value['nPrice']);?>원</td>
										<td<?=$style?>><?=($value['nRefund'] == 1) ? 'O' : 'X'; ?></td>
										<td<?=$style?>><?=$value['dtRegDate'];?></td>
										<td<?=$style?>><?=$value['vStartDate'];?></td>
										<td<?=$style?>><?=$value['vEndDate'];?></td>
										<td<?=$style?>><?=$value['vOrderidx'];?></td>
									</tr>
<?php } ?>

								</tbody>
							</table>
						</div>
						<?=$paging?>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
					</div>
					<div class="modal-body">
						<p>Modal body text goes here.ddd
							<button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Closezzzz</button>
							<button type="button" class="btn btn-primary">Save changes</button>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">

function pageRemote(pageNo){
	$('#pageNo').val(pageNo);
	document.frm.submit();
}

function ex(){
	$('#ex').val('ex');
	document.frm.submit();
	$('#ex').val('');
}

function change_stat(no,agrno,name){
	if(agrno=='1') stat='미승인'; else stat='승인';
	$.post("/admin/payment/changestat",{cno:no,agrno:agrno},function(data){alert(name+'님의 결제 처리 상태가 '+stat+'으로 변경되었습니다.');document.location.href='/admin/payment';},'json');
}

</script>