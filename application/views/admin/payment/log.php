<style>
	.fred{color:red;}
	.fsilver{color:silver;}
</style>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>정기구독 실패목록</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-header">
						<div class="float-right" style="width:100%;">
<?/*
<form name="frm" id="frm" method="post" action="/admin/payment/<?=$payType?>" style="width:100%;">
	<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo;?>">
	<input type="hidden" name="ex" id="ex" value="">
							<div class="input-group" style="width:100%;">
								<span style="padding:0px;background-color:green;color:#fff;width:50px;border:0px;cursor:pointer;text-align:center;" onclick="ex()">엑셀</span>
								<select class="form-control" name="pmethod" id="pmethod" style="height:30px;padding:0px 3px;">
									<option value="">결제수단</option>
									<option value="C" <?php if(@$_POST['pmethod']=='C') echo 'selected';?>>신용카드</option>
									<option value="M" <?php if(@$_POST['pmethod']=='M') echo 'selected';?>>무통장입금</option>
									<option value="T" <?php if(@$_POST['pmethod']=='T') echo 'selected';?>>계좌이체</option>
								</select>

								<select class="form-control" name="limit" id="limit" style="height:30px;padding:0px 3px;">
									<option value="">목록수</option>
									<option value="20" <?php if(@$_POST['limit']=='20') echo 'selected';?>>20개</option>
									<option value="50" <?php if(@$_POST['limit']=='50') echo 'selected';?>>50개</option>
								</select>
								<input type="text" name="reg1" id="reg1" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@$_POST['reg1'];?>" placeholder="시작일"> ~ 
								<input type="text" name="reg2" id="reg2" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@$_POST['reg2'];?>" placeholder="종료일">
								<input type="text" name="keyword" id="keyword" class="form-control" placeholder="이름 or 연락처" value="<?=@$_POST['keyword'];?>">
								<div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
							</div>
</form>
*/?>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
								<?$style=' style="padding:4px 10px;text-align:center;"'?>
										<th width="6%"<?=$style?>>번호</th>
										<th<?=$style?>>이름</th>
										<th<?=$style?>>전화번호</th>
										<th<?=$style?>>상품명</th>
										<th<?=$style?>>금액</th>
										<th<?=$style?>>결제일</th>
										<th<?=$style?>>시작일</th>
										<th<?=$style?>>종료일</th>
										<th<?=$style?>>사유</th>
										<th<?=$style?>>처리상태</th>
									</tr>
								</thead>
								<tbody>
<?php
foreach ($list as $value) { 
?>
									<tr>
										<td<?=$style?>><?=$no--?></td>
										<td<?=$style?>><a href="javascript:;" class="golink"><?=$value['vName'];?></a></td>
										<td<?=$style?>>보안상 미기재</td>
										<td<?=$style?>><?=$value['vGoodName'];?></td>
										<td<?=$style?> align='center'><?=number_format($value['nAmount'])?>원</td>
										<td<?=$style?>><?=$value['dtRegDate'];?></td>
										<td<?=$style?>><?=$value['vStartDate'];?></td>
										<td<?=$style?>><?=$value['vEndDate'];?></td>
										<td<?=$style?>><?=$value['vReason'];?></td>
										<td<?=$style?>>
											<select class="change_state <?if($value['nState']==1) echo 'fsilver'; else echo 'fred';?>" style="width:120px;padding:0px;" data-log_no="<?=$value['nSeqNo']?>">
												<option <?=($value['nState'] == 1) ? 'selected' : ''?> value="1">처리완료</option>
												<option <?=($value['nState'] == 0) ? 'selected' : ''?> value="0">대기중</option>
											</select>
										</td>
									</tr>
<?php } #foreach  ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<form name="frm" id="frm" method="post" action="/admin/payment/vip">
	<input type="hidden" name="pageNo" id="pageNo" value="1">
	<input type="hidden" name="ex" id="ex" value="">
	<input type="hidden" name="limit" id="limit" value="20">
	<input type="hidden" name="pmethod" id="pmethod" value="C">
	<input type="hidden" name="reg1" id="reg1" value="2020-07-01">
	<input type="hidden" name="reg2" id="reg2" value="<?=date("Y-m-d")?>">
	<input type="hidden" name="keyfield" id="keyfield" value="vName">
	<input type="hidden" name="keyword" id="keyword" value="">
</form>
<script src="/asset/js/lib/jquery-3.4.1.min.js"></script>
<script>
	$(function(){
		$(".change_state").on('change', function(){
			var u = "/admin/payment/putLogState";
			var data = {
				'nState' : $(this).val()
				, 'nSeqNo' : $(this).data("log_no")
			}
			$.post({ 
				url : u,
				dataType : 'json',
				data : data,
				success : function(response) {
					document.location.reload();//console.log(response);
				},
				error : function(xhr, status, error) {},
			})
		});

		$('.golink').on('click',function(){
			var vname=$(this).html();
			document.frm.keyword.value=vname;
			document.frm.submit();
		});
	});
</script>