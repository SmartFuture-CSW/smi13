<script src="/public/admin/modules/jquery.min.js"></script>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>공개정보</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-body">
					<form name="frm" id="frm" method="post" action="/admin/category/postCategory" onsubmit="return send(this)" enctype="multipart/form-data">
						<input type="hidden" name="mode" value="<?=$mode?>">
						<input type="hidden" name="vName" value="<?=$writer?>">
						<input type="hidden" name="nSeqNo" value="<?=@$category['nSeqNo']?>">

						<div class="form-divider"> 카테고리 작성</div>
						<div class="row">
							<div class="form-group col-12">
								<label>사용 게시판</label>
								<div class="input-group">
								<select name="vType" class="form-control">
								<?php foreach($board as $value){?>
									<option <?=(@$category['vType'] == $value['vType'])?'selected':''?> value="<?=$value['vType']?>"><?=$value['vName']?></option>
								<?php }?>
								</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<label>카테고리명</label>
								<div class="input-group"><input type="text" id="vSubject" name="vSubject" class="form-control" value="<?=@$category['vSubject']?>" required></div>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">등록</button>
							<button type="button" onclick="go_back()" class="btn btn-light btn-block">취소</button>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	function send(f){
		if(f.vSubject.value==''){
			alert('카테고리명을 입력 해주세요.');
			return false;
		}
		return true;
	}

	function go_back(){
		document.frm.action="/admin/Category/";
		document.frm.submit();
	}
</script>