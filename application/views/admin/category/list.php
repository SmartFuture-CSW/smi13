<style>
.auto{background-color:#5D5D5D;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
</style>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>카테고리 관리</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-body">
						<div class="table-responsive">
		<form name="frm" id="frm" method="post" action="/admin/Category" style="width:100%;">
			<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo;?>">
			<input type="hidden" name="nSeqNo" id="nSeqNo" value="">

							<table class="table table-hover">
								<thead>
									<tr>
										<th width="6%">번호</th>
										<th width="20%">사용 게시판</th>
										<th>카테고리명</th>
										<th width="10%">작성자</th>
										<th width="10%">등록일</th>
										<th width="10%">관리</th>
									</tr>
								</thead>
								<tbody>
						<?php foreach ($list as $value) { ?>
									<tr<?if($value['emDelFlag']=='Y'){?> style="background-color:#b0b0b0;" title="삭제된 카테고리 입니다."<?}?>>
										<td><?=$no--?></td>
										<td><?=$board[$value['vType']]?></td>
										<td><?=$value['vSubject']?></td>
										<td><?=$value['vName']?></td>
										<td><?=$value['dtRegDate'];?></td>
										<td>
											<a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit" onclick="proc_bd('Edit','<?=$value['nSeqNo']?>')"><i class="ion ion-edit"></i></a>
											<a class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete" onclick="proc_bd('Del','<?=$value['nSeqNo']?>')"><i class="ion ion-trash-b"></i></a>
										</td>
									</tr>
						<?php } ?>
								</tbody>
							</table>
		</form>
						</div>
						<div>
							<div style="float:right;"><a href="/admin/category/writeCategory" class="btn btn-primary"> 생성</a></div>
						</div>
						<?=$paging?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">

function pageRemote(pageNo){
	$('#pageNo').val(pageNo);
	document.frm.submit();
}

function proc_bd(mode,no){

	if(mode == 'Edit'){
		$('#nSeqNo').val(no);
		document.frm.action='/admin/Category/editCategory';
		document.frm.submit();
	}
	else if(mode == 'Del'){
		if(confirm("정말로 삭제하시겠습니까?")){
			$('#nSeqNo').val(no);
			document.frm.action='/admin/Category/removeCategory';
			document.frm.submit();
		}
	}
}

function all_save(cate){
	var str='';
	if(cate=='today'){
		for(j=1;j<=10;j++){
			if(j > 2 && j < 9){ continue; }
			for(i=1;i<=5;i++){ str+=i+','+$('#vType_select_'+j+'_'+i+' option:selected').val()+','+$('#bdno_'+j+'_'+i+' option:selected').val()+'|'; }
			str+='<nodaji>';
		}
	}
	else if(cate=='important'){
		for(i=1;i<=5;i++){
			str+=i+','+$('#vType_select_3_'+i+' option:selected').val()+','+$('#bdno_3_'+i+' option:selected').val()+'|';
		}
	}
	else if(cate=='invest'){
		for(i=1;i<=5;i++){
			str+=i+','+$('#vType_select_7_'+i+' option:selected').val()+','+$('#bdno_7_'+i+' option:selected').val()+'|';
		}
	}
	else if(cate=='study'){
		for(i=1;i<=5;i++){
			str+=i+','+$('#vType_select_8_'+i+' option:selected').val()+','+$('#bdno_8_'+i+' option:selected').val()+'|';
		}
	}
	$.post("/admin/board/popupdate",{fval:str,category:cate},function(data){$('.close').trigger('click');},'json');
}


function list_refresh(f){
	f.submit();
}
</script>