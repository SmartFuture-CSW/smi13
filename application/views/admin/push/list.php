 <style>
	.resulttotal{background-color:#FFEAEA;text-align:right;font-weight:bold;font-size:18px;}
	.tdstyle{text-align:center;width:70px;background-color:#f0f0f0;}
 </style>
 <div class="main-content">
        <section class="section">
          <h1 class="section-header"><div>PUSH발송 내역</div></h1>
         	<div class="row">
              <div class="col-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <div class="float-right" style="width:100%;">
                      <form name="frm" id="frm" method="post" action="/admin/push/pushlist" style="width:100%;">
                        <input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo;?>">
                        <div class="input-group" style="width:100%;">
                        	<select class="form-control" name="limit" id="limit" style="height:30px;padding:0px 3px;">
		                        <option value="">목록수</option>
		                        <option value="20" <?php if(@$_POST['limit']=='20') echo 'selected';?>>20개</option>
								<option value="50" <?php if(@$_POST['limit']=='50') echo 'selected';?>>50개</option>
                      		</select>
							<input type="text" name="reg1" id="reg1" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@$_POST['reg1'];?>" placeholder="푸시일 시작"> ~ 
							<input type="text" name="reg2" id="reg2" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@$_POST['reg2'];?>" placeholder="푸시일 종료">
                          <div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                        	<tr>
							  <th width="6%">번호</th>
		                      <th>제목</th>
		                      <th>링크</th>
		                      <th>대상</th>
		                      <th>건수</th>
							  <th>푸시일</th>
                        </tr>
                        </thead>
                        <tbody>
						<?php foreach ($list as $var) {
							if($var['vEtc4']=='all') $ds='전체'; 
							else if($var['vEtc4']=='free') $ds='무료회원';
							else if($var['vEtc4']=='charge') $ds='유료회원';
							else if($var['vEtc4']=='exp') $ds='무료체험신청';
							else if($var['vEtc4']=='tester') $ds='테스터';
							else if($var['vEtc4']=='nonmem') $ds='비회원';
						?>
							<tr>
								<td><?=$no--?></td>
								<td><?=$var['vSubject']?></td>
								<td><?=$var['vEtc2']?></td>
								<td><?=$ds?></td>
								<td><?=$var['vMemo']?>건</td>
								<td><?=$var['dtRegDate']?></td>
							</tr>
						<?}?>
                      </tbody>
                    </table>
                    </div>
                    <?=$paging?>
                  </div>
                </div>
              </div>
            </div>
	</section>
</div>
<script type="text/javascript">
  function pageRemote(pageNo){
    $('#pageNo').val(pageNo);
    document.frm.submit();
  }

	function set_modal(uid,ucd){
		$('.close').remove();
		$('.modal-body').empty();
		var str=tit='';
		$('.modal-dialog').css('max-width','800px');
		tit='발송내역';
		$.post("/admin/adminsms/popdetail",{"uniq_id":uid,"used_cd":ucd},function(data){
			str='<table border="1" width="100%">';
			str+='<tr><th class="tdstyle">제목</th><td style="padding:6px;" colspan="5" id="title">'+data.detail[0].title+'</td></tr>';
			str+='<tr><th class="tdstyle">내용</th><td style="padding:6px;" colspan="5" id="msg">'+data.detail[0].msg+'</td></tr>';
			str+='<tr><th class="tdstyle">발송일시</th><td style="padding:6px;" colspan="5" id="senddate">'+data.detail[0].senddate+'</td></tr>';
			str+='<tr><th class="tdstyle">발신번호</th><td style="padding:6px;" colspan="5" id="caller">'+data.detail[0].caller+'</td></tr>';
			str+='<tr><th class="tdstyle">전송유형</th><td style="padding:6px;" colspan="5" id="type">'+data.detail[0].type+'</td></tr>';
			str+='<tr><th class="tdstyle">총건수</th><td style="padding:6px;" colspan="5" id="total">'+data.detail[0].total+'건</td></tr>';
			str+='<tr><th class="tdstyle">성공</th><td style="padding:6px;width:197px;" id="success">'+data.detail[0].success+'건</td><th class="tdstyle">진행중</th><td style="padding:6px;width:197px;" id="wait">'+data.detail[0].wait+'건</td><th class="tdstyle">실패</th><td style="padding:6px;width:196px;" id="fail">'+data.detail[0].fail+'건</td></tr>';
			str+='<tr><th class="tdstyle">소요캐시</th><td style="padding:6px;" colspan="5" id="cash">'+data.detail[0].cash+'원</td></tr>';
			str+='<tr><td colspan="6" height="1">&nbsp;</td></tr>';
			for(var i=1;i<data.detail.length;i++){
				status = data.detail[i].status;
				if(status=="success")status="성공";
				if(status=="ing")status="대기";
				str+='<tr><th class="tdstyle">수신번호</th><td style="padding:6px;">'+data.detail[i].rno+'</td><th class="tdstyle">결과</th><td style="padding:6px;">'+status+'</td><th class="tdstyle">발송일시</th><td style="padding:6px;">'+data.detail[i].sendtime+'</td></tr>';
			}
			str+='</table><div style="height:30px;"></div>';
			$('.modal-body').append(str);
		},'json');
		$('.modal-header').empty().append('<h5 class="modal-title">'+tit+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color:#fff;"> x </button>');
	}
</script>