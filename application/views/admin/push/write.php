
<style>
.receive_list input{
float:left;
}
.receive_list label{
	margin:0px 10px;
	float:left;
}
</style>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>PUSH 발송</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-body">
						<div class="row">
							<div class="form-group col-12">
								<label>발송대상</label>
								<div class="input-group receive_list">
									<input type="checkbox" name="receive_list" value="tester" id="rl_tester" ><label for="rl_tester">테스트-최상운/황욱</label>
									<input type="checkbox" name="receive_list" value="all" id="rl_all" ><label for="rl_all">전체회원(<?=count($mcount['all'])?>)</label>
									<input type="checkbox" name="receive_list" value="charge" id="rl_charge" ><label for="rl_charge">유료회원(<?=count($mcount['charge'])?>)</label>
									<input type="checkbox" name="receive_list" value="free" id="rl_free" ><label for="rl_free">무료회원(<?=count($mcount['free'])?>)</label>
									<input type="checkbox" name="receive_list" value="nonmem" id="rl_nonmem" ><label for="rl_nonmem">비회원(<?=count($mcount['nonmem'])?>)</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<div class="input-group">
									<input type="text" id="vEtc2" name="vEtc2" class="form-control" placeholder="탭링크 주소를 입력 해주세요. ex: /boardView/invest/1398">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<div class="input-group">
									<input type="text" id="vSubject" name="vSubject" class="form-control"  placeholder="제목을 입력 해주세요">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<div class="input-group">
									<textarea id="txContent" name="txContent" class="form-control" required maxlength="65536" style="width:100%;height:250px" placeholder="내용을 입력 해주세요"></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div style="clear:both;"></div>
							<div style="float:right;"><button type="button" class="btn btn-primary btn-block" onclick="sendpush()">전송</button></div>
							<div style="clear:both;"></div>
						</div>
					</div>
                    <div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	function sendpush(){
		var receive_list=$("input[name=receive_list]:checked").length;

		if(receive_list == 0){
			alert('발송대상을 선택해주세요.');
			return false;
		}
		if($("input[name=vSubject]").val().trim() ==''){
			alert('제목을 입력해주세요.');
			return false;
		}
		if($("textarea[name=txContent]").val().trim() ==''){
			alert('내용을 입력해주세요.');
			return false;
		}

		//var para=$('#frm').serialize();


		var rl = [];

		$("input[name=receive_list]:checked").each(function(){
			rl.push($(this).val());
		})

		var para = {
			'receive_list' : rl
			, 'vEtc2' : $("input[name=vEtc2]").val()
			, 'vSubject' : $("input[name=vSubject]").val()
			, 'txContent' : $("textarea[name=txContent]").val()
			, 'vType' : 'push'
		}

		
		$.ajax({
			type : "post",
			url : "/admin/push/pushInsert",
			dataType : 'json',
			data : para,
			success : function(response) {
				if(response.code=='1'){
					alert("발송되었습니다.");
					document.location.href='/admin/push/';
				}else{
					alert("오류! - "+response.msg);
					return false;
				}
			},
			error : function(response) {
				console.log("ERROR : " + response)
			}
		});

			/*
		$.post("/admin/push/pushInsert",para,function(data){
			if(data.code=='1'){
				alert("발송되었습니다.");
				document.location.href='/admin/push/';
			}else{
				alert("오류! - "+data.msg);
				return false;
			}
		},'json');
			*/
	}
</script>