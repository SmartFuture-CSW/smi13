
      <div class="main-content">
        <section class="section">
          <h1 class="section-header">
            <div>메인화면</div>
          </h1>
          <div class="row">
            <div class="col-lg-3 col-md-6 col-12">
              <div class="card card-sm-3">
                <div class="card-icon bg-primary">
                  <i class="ion ion-person"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>전체회원</h4>
                  </div>
                  <div class="card-body">
                    <?=$total?>명
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
              <div class="card card-sm-3">
                <div class="card-icon bg-danger">
                  <i class="ion ion-ios-paper-outline"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>매출 건</h4>
                  </div>
                  <div class="card-body">
                    <?=$paycount?>건
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
              <div class="card card-sm-3">
                <div class="card-icon bg-warning">
                  <i class="ion ion-paper-airplane"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>게시물</h4>
                  </div>
                  <div class="card-body">
                    <?=$bbscount?>건
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
              <div class="card card-sm-3">
                <div class="card-icon bg-success">
                  <i class="ion ion-record"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>댓글</h4>
                  </div>
                  <div class="card-body">
                    <?=$rplcount?>건
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="row">
			<div class="col-lg-6 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <div class="float-right">
                    <a href="/admin/payment/vip" class="btn btn-primary">전체보기</a>
                  </div>
                  <h4>매출관리</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th style='text-align:center;'>이름</th>
                          <th style='text-align:center;'>전화번호</th>
                          <th style='text-align:center;'>상품명</th>
                          <th style='text-align:center;'>실결제액</th>
                        </tr>
                      </thead>
                      <tbody>
					  <?$n=0;
					  foreach ($paylist as $v) { 
						  if($n>4) break;?>
                        <tr>
                          <td align='center'><?=$v['vName']?></td>
                          <td align='center'>보안상 미기재</td>
                          <td align='center'><?=$v['vGoodName']?></td>
                          <td align='center'><?=number_format($v['nAmount'])?>원</td>
                        </tr>
					  <?$n++;
					  }?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
			<div class="col-lg-6 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <div class="float-right">
                    <a href="/admin/user" class="btn btn-primary">전체보기</a>
                  </div>
                  <h4>회원관리</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th style='text-align:center;'>이름</th>
                          <th style='text-align:center;'>전화번호</th>
                          <th style='text-align:center;'>그룹</th>
                          <th style='text-align:center;'>구분</th>
                        </tr>
                      </thead>
                      <tbody>
					  <?$n=0;
					  foreach ($userlist as $v) { 
						  if($n>4) break;?>
                        <tr>
                          <td align='center'><?=$v['vName']?></td>
                          <td align='center'>보안상 미기재</td>
                          <td align='center'><?=($v['mgroup']>0?'구독':'무료');?>그룹</td>
                          <td align='center'><?=($v['nLevel']>1?'전문가':'일반');?>회원</td>
                        </tr>
					  <?$n++;
					  }?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
			<div class="col-lg-6 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <div class="float-right">
                    <a href="/admin/stock" class="btn btn-primary">전체보기</a>
                  </div>
                  <h4>종목추천</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th style='text-align:center;'>구분</th>
                          <th style='text-align:center;'>제목</th>
                          <th style='text-align:center;'>판매가</th>
                          <th style='text-align:center;'>등록일</th>
                        </tr>
                      </thead>
                      <tbody>
					  <?$n=0;
					  foreach ($stocklist as $v) { 
						  if($n>4) break;?>
                        <tr>
                          <td align='center'><?=$v['vType']?></td>
                          <td align='center'><?=$this->util->cut_str($v['vSubject'],10)?></td>
                          <td align='center'><?=number_format($v['nPrice'])?>원</td>
                          <td align='center'><?=$v['dtRegDate']?></td>
                        </tr>
					  <?$n++;
					  }?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
			<div class="col-lg-6 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <div class="float-right">
                    <a href="/admin/board/reply" class="btn btn-primary">전체보기</a>
                  </div>
                  <h4>댓글관리</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th style='text-align:center;'>이름</th>
                          <th style='text-align:center;'>제목</th>
                          <th style='text-align:center;'>좋아요수</th>
                          <th style='text-align:center;'>등록일</th>
                        </tr>
                      </thead>
                      <tbody>
					  <?$n=0;
					  foreach ($replylist as $v) { 
						  if($n>4) break;?>
                        <tr>
                          <td align='center'><?=$v['vNick']?></td>
                          <td align='center'><?=$this->util->cut_str(strip_tags($v['txComment']),10)?></td>
                          <td align='center'><?=number_format($v['nLike'])?></td>
                          <td align='center'><?=$v['dtRegDate']?></td>
                        </tr>
					  <?$n++;
					  }?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </section>
      </div>

