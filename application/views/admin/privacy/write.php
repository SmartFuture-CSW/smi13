<script src="/public/admin/modules/jquery.min.js"></script>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>사이트관리</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-body">
				<?if($mode=='1'){?>
					<form name="frm" id="frm" method="post" action="/admin/privacy/privacyInsert" onsubmit="return send(this)">
						<input type='hidden' name='mode' id='mode' value='<?=$mode?>'>
						<div class="row">
							<div class="form-group col-12">
								<label>개인정보보호정책</label>
								<div class="input-group">
									<script src="/public/smarteditor2/js/service/HuskyEZCreator.js"></script>
									<script>var g5_editor_url = "/public/smarteditor2", oEditors = [], ed_nonce = "dhHcWl0suO|1593457283|d96b66cdfb538c66cc7d47f7ca87b3e0eeadacd1";</script>
									<script src="/public/smarteditor2/config.js"></script>
									<textarea id="txPrivacy" name="txPrivacy" class="smarteditor2 form-control" style="width:100%;height:300px"><?=@$write[0]['txPrivacy']?></textarea>
								</div>
							</div>
							<div class="form-group col-12">
								<label>이용약관</label>
								<div class="input-group">
									<textarea id="txTerms" name="txTerms" class="smarteditor2 form-control" style="width:100%;height:300px"><?=@$write[0]['txTerms']?></textarea>
								</div>
							</div>
							<div class="form-group col-12">
								<label>광고성 정보 수신동의</label>
								<div class="input-group">
									<textarea id="txAdterms" name="txAdterms" class="smarteditor2 form-control" style="width:100%;height:300px"><?=@$write[0]['txAdterms']?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">확인</button>
						</div>
					</form>
				<?}else if($mode=='2'){?>
					<form name="frm" id="frm" method="post" action="/admin/privacy/privacyInsert" onsubmit="return send(this)">
						<input type='hidden' name='mode' id='mode' value='<?=$mode?>'>
						<div class="row">
							<div class="form-group col-12">
								<label>허용IP</label>
								<div class="input-group">
									<textarea id="txIpAllow" name="txIpAllow" class="form-control" maxlength="65536" style="width:100%;height:300px"><?=@$write[0]['txIpAllow']?></textarea>
								</div>
							</div>
							<div class="form-group col-12">
								<label>Ver.</label>
								<div class="input-group"><input type="text" id="vVersion" name="vVersion" class="form-control" value="<?=@$write[0]['vVersion']?>" required></div>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">확인</button>
						</div>
					</form>
				<?}?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	function send(f){
		<?if($mode=='1'){?>
			var txPrivacy_editor_data = oEditors.getById['txPrivacy'].getIR();
			oEditors.getById['txPrivacy'].exec('UPDATE_CONTENTS_FIELD', []);
			if(jQuery.inArray(document.getElementById('txPrivacy').value.toLowerCase().replace(/^\s*|\s*$/g, ''), ['&nbsp;','<p>&nbsp;</p>','<p><br></p>','<div><br></div>','<p></p>','<br>','']) != -1){document.getElementById('txPrivacy').value='';}
			var txTerms_editor_data = oEditors.getById['txTerms'].getIR();
			oEditors.getById['txTerms'].exec('UPDATE_CONTENTS_FIELD', []);
			if(jQuery.inArray(document.getElementById('txTerms').value.toLowerCase().replace(/^\s*|\s*$/g, ''), ['&nbsp;','<p>&nbsp;</p>','<p><br></p>','<div><br></div>','<p></p>','<br>','']) != -1){document.getElementById('txTerms').value='';}
			var txAdterms_editor_data = oEditors.getById['txAdterms'].getIR();
			oEditors.getById['txAdterms'].exec('UPDATE_CONTENTS_FIELD', []);
			if(jQuery.inArray(document.getElementById('txAdterms').value.toLowerCase().replace(/^\s*|\s*$/g, ''), ['&nbsp;','<p>&nbsp;</p>','<p><br></p>','<div><br></div>','<p></p>','<br>','']) != -1){document.getElementById('txAdterms').value='';}
		<?}?>
		return true;
	}
</script>