 <style>
.auto{background-color:#5D5D5D;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
.stat_1{background-color:red;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
.stat_2{background-color:blue;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
 </style>
 <div class="main-content">
        <section class="section">
          <h1 class="section-header"><div>종목추천</div></h1>
         	<div class="row">
              <div class="col-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <div class="float-right" style="width:100%;">
                      <form name="frm" id="frm" method="post" action="/admin/stock" style="width:100%;">
                        <input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo;?>">
                        <input type="hidden" name="bd_no" id="bd_no" value="">
						<input type="hidden" name="mode" id="mode" value="">
						<input type="hidden" name="udname" id="udname" value="">
						<input type="hidden" name="udval" id="udval" value="">
                        <div class="input-group" style="width:100%;">
							<select class="form-control" name="vType" id="vType" style="height:30px;padding:0px 3px;">
		                        <option value="theme">테마별 종목</option>
		                        <option value="period" <?php if(@$_POST['vType']=='period') echo 'selected';?>>기간별 종목</option>
								<option value="rumor" <?php if(@$_POST['vType']=='rumor') echo 'selected';?>>찌라시 종목</option>
                      		</select>
                        	<select class="form-control" name="limit" id="limit" style="height:30px;padding:0px 3px;">
		                        <option value="">목록수</option>
		                        <option value="20" <?php if(@$_POST['limit']=='20') echo 'selected';?>>20개</option>
								<option value="50" <?php if(@$_POST['limit']=='50') echo 'selected';?>>50개</option>
                      		</select>
	                        <input type="text" name="keyword" id="keyword" class="form-control" placeholder="제목 or 내용" value="<?=@$_POST['keyword'];?>">
                          <div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                        	<tr>
							  <th width="6%">번호</th>
		                      <th>제목</th>
		                      <th>상태변경</th>
		                      <th>작성자</th>
		                      <th>등록일</th>
							  <th>조회수</th>
							  <th>공유수</th>
		                      <th>관리</th>
                        </tr>
                        </thead>
                        <tbody>
						<?php foreach ($list as $value) { ?>
                        <tr>
							  <td><?=$no--?></td>
		                      <td><?=$value['vSubject']?></td>
							  <td><span class="stat_<?=$value['nStat']?>" onclick="change_stat('<?=$value['nStat']?>','<?=$value['nSeqNo']?>', this)"><?if($value['nStat']=='1') echo '게시'; else echo '종료';?></span></td>
		                      <td><?=$value['vName']?></td>
		                      <td><?=$value['dtRegDate'];?></td>
		                      <td><?=$value['nHit'];?></td>
							  <td><?=$value['nShare'];?></td>
							  <td>
								<a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit" onclick="proc_bd('Edit','<?=$value['nSeqNo']?>','')"><i class="ion ion-edit"></i></a>
								<a class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete" onclick="proc_bd('Del','<?=$value['nSeqNo']?>','')"><i class="ion ion-trash-b"></i></a>
							  </td>
                        </tr>
						<?php } #foreach ?>
                      </tbody>
                    </table>
                    </div>
                    <div>
						<div style="clear:both;"></div>
						<div style="float:left;margin-left:5px;"><a href="javascript:void(0);" class="btn btn-primary modal-go" onclick="set_modal()">종목추천 메인설정</a></div>
						<div style="float:right;margin-left:5px;"><a href="javascript:void(0);" class="btn btn-primary modal-go" onclick="proc_bd('Edit','','theme')">테마종목 생성</a></div>
						<div style="float:right;margin-left:5px;"><a href="javascript:void(0);" class="btn btn-primary modal-go" onclick="proc_bd('Edit','','period')">기간종목 생성</a></div>
						<div style="float:right;"><a href="javascript:void(0);" class="btn btn-primary" onclick="proc_bd('Edit','','rumor')">찌라시 생성</a></div>
						<div style="clear:both;"></div>
					</div>
                    <?=$paging?>
                  </div>
                </div>
              </div>
            </div>
	</section>
</div>

<script type="text/javascript">
  function pageRemote(pageNo){
    $('#pageNo').val(pageNo);
    document.frm.submit();
  }
  function proc_bd(mode,no,prod){
	if(mode=='Del'){
		if(confirm("정말로 삭제하시겠습니까?")){
			$('#mode').val(mode);
			$('#bd_no').val(no);
			document.frm.action='/admin/stock/stock'+mode;
			document.frm.submit();
		}
	}else{
		$('#mode').val(mode);
		$('#bd_no').val(no);
		$('#vType').val(prod).prop('selected',true);
		document.frm.action='/admin/stock/stock'+mode;
		document.frm.submit();
	}
  }

  function set_modal(){
	$('.close').remove();
	$('.modal-body').empty();
	var sstr=tit='';
	var str='<div style="padding-bottom:10px;">테마별 종목추천 <span class="auto" onclick="auto_select(4)">최신순 자동선택</span></div><table style="width:100%;">';
	for(i=1;i<=3;i++){
		str+='<tr><td>'+i+'순위</td><td><select class="form-control" id="vType_select_4_'+i+'" style="height:30px;padding:0px" onchange="change_board(this.value,4,'+i+')"><option value="">게시판을 선택해주십시오.</option><option value="theme">테마별 종목</option><option value="period">기간별 종목</option><option value="rumor">찌라시 종목</option></select></td><td><select id="bdno_4_'+i+'" class="form-control" style="height:30px;padding:0px;width:211px;"><option value="">컨텐츠를 선택해주십시오.</option></select></td><tr>';
	}
	str+='</table>';
	str+='<div style="padding-top:25px;padding-bottom:10px;">기간별 종목추천 <span class="auto" onclick="auto_select(5)">최신순 자동선택</span></div><table style="width:100%;">';
	for(i=1;i<=3;i++){
		str+='<tr><td>'+i+'순위</td><td><select class="form-control" id="vType_select_5_'+i+'" style="height:30px;padding:0px" onchange="change_board(this.value,5,'+i+')"><option value="">게시판을 선택해주십시오.</option><option value="theme">테마별 종목</option><option value="period">기간별 종목</option><option value="rumor">찌라시 종목</option></select></td><td><select id="bdno_5_'+i+'" class="form-control" style="height:30px;padding:0px;width:211px;"><option value="">컨텐츠를 선택해주십시오.</option></select></td><tr>';
	}
	str+='</table>';
	str+='<div style="padding-top:25px;padding-bottom:10px;">찌라시 종목추천 <span class="auto" onclick="auto_select(6)">최신순 자동선택</span></div><table style="width:100%;">';
	for(i=1;i<=5;i++){
		str+='<tr><td>'+i+'순위</td><td><select class="form-control" id="vType_select_6_'+i+'" style="height:30px;padding:0px" onchange="change_board(this.value,6,'+i+')"><option value="">게시판을 선택해주십시오.</option><option value="theme">테마별 종목</option><option value="period">기간별 종목</option><option value="rumor">찌라시 종목</option></select></td><td><select id="bdno_6_'+i+'" class="form-control" style="height:30px;padding:0px;width:211px;"><option value="">컨텐츠를 선택해주십시오.</option></select></td><tr>';
	}
	str+='</table>';
	str+='<div style="text-align:center;padding-top:15px;"><button type="button" class="btn btn-primary" onclick="all_save()">저장</button></div>';
	$.post("/admin/stock/popmain",{mainval:"4"},function(data){
		for(i=1;i<=3;i++){
			if(data.cont[i].vType!=''){
				$('#vType_select_4_'+i).val(data.cont[i].vType).prop("selected",true);
			}
			if(data.cont[i].bdno!=''){
				sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
				$.each(data.cont[i].vTypeList, function(idx,item){
					sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
				});
				$('#bdno_4_'+i).empty().append(sstr);
				$('#bdno_4_'+i).val(data.cont[i].bdno).prop("selected",true);
			}
		}
	},'json');
	$.post("/admin/stock/popmain",{mainval:"5"},function(data){
		for(i=1;i<=3;i++){
			if(data.cont[i].vType!=''){
				$('#vType_select_5_'+i).val(data.cont[i].vType).prop("selected",true);
			}
			if(data.cont[i].bdno!=''){
				sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
				$.each(data.cont[i].vTypeList, function(idx,item){
					sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
				});
				$('#bdno_5_'+i).empty().append(sstr);
				$('#bdno_5_'+i).val(data.cont[i].bdno).prop("selected",true);
			}
		}
	},'json');
	$.post("/admin/stock/popmain",{mainval:"6"},function(data){
		for(i=1;i<=5;i++){
			if(data.cont[i].vType!=''){
				$('#vType_select_6_'+i).val(data.cont[i].vType).prop("selected",true);
			}
			if(data.cont[i].bdno!=''){
				sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
				$.each(data.cont[i].vTypeList, function(idx,item){
					sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
				});
				$('#bdno_6_'+i).empty().append(sstr);
				$('#bdno_6_'+i).val(data.cont[i].bdno).prop("selected",true);
			}
		}
	},'json');
	$('.modal-body').append(str);
	$('.modal-header').empty().append('<h5 class="modal-title">'+tit+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color:#fff;"> x </button>');
  }

  function change_board(v,m,n){
	var sstr='';
	$('#bdno_'+m+'_'+n).empty();
	if(v==''){
		sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
		$('#bdno_'+m+'_'+n).append(sstr);
	}else{
		$.post("/admin/stock/popmain2",{vType:v,mainval:m},function(data){
			sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
			if(data.cont){
				$.each(data.cont, function(idx,item){
					sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
				});
			}
			$('#bdno_'+m+'_'+n).append(sstr);
		},'json');
	}
  }

  function all_save(){
	var str='';
	for(j=4;j<=6;j++){
		if(j==6) var ii=5; else var ii=3;
		for(i=1;i<=ii;i++){
			str+=i+','+$('#vType_select_'+j+'_'+i+' option:selected').val()+','+$('#bdno_'+j+'_'+i+' option:selected').val()+'|';
		}
		str+='<nodaji>';
	}
	$.post("/admin/stock/popupdate",{fval:str},function(data){$('.close').trigger('click');},'json');
  }

  function auto_select(no){
	if(no==6) var ii=5; else var ii=3;
	$.post("/admin/stock/popautolist",{mainval:no},function(data){
		for(i=1;i<=ii;i++){
			if(data.cont[i].vType!=''){
				$('#vType_select_'+no+'_'+i).val(data.cont[i].vType).prop("selected",true);
			}
			if(data.cont[i].bdno!=''){
				sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
				$.each(data.cont[i].vTypeList, function(idx,item){
					sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
				});
				$('#bdno_'+no+'_'+i).empty().append(sstr);
				$('#bdno_'+no+'_'+i).val(data.cont[i].bdno).prop("selected",true);
			}
		}
	},'json');
  }

  function change_stat(stat,no,target){
	  var now_txt=$(target).html();
	  if(now_txt=='게시'){
		  var new_txt='종료';
		  var new_stat='2';
	  }else if(now_txt=='종료'){
		  var new_txt='게시';
		  var new_stat='1';
	  }
	  if(confirm("'"+now_txt+"'상태에서 '"+new_txt+"'상태로 변경하시겠습니까?")){
		$.post("/admin/stock/changestat",{no:no,stat:stat},function(data){
			$(target).html(new_txt).removeClass('stat_'+stat).addClass('stat_'+new_stat);
		},'json');
	  }
  }
</script>