<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
	<title>노다지 관리자</title>
	<link rel="stylesheet" href="<?=MODULES_PATH?>/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=MODULES_PATH?>/ionicons/css/ionicons.min.css">
	<link rel="stylesheet" href="<?=MODULES_PATH?>/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="<?=MODULES_PATH?>/summernote/summernote-lite.css">
	<link rel="stylesheet" href="<?=MODULES_PATH?>/flag-icon-css/css/flag-icon.min.css">
	<link rel="stylesheet" href="<?=CSS_PATH?>/demo.css">
	<link rel="stylesheet" href="<?=CSS_PATH?>/style.css">
</head>
<body>
<div id="app">
	<div class="main-wrapper">
		<div class="navbar-bg"></div>
		<nav class="navbar navbar-expand-lg main-navbar">
	<form class="form-inline mr-auto">
			<ul class="navbar-nav mr-3">
				<li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="ion ion-navicon-round"></i></a></li>
				<li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="ion ion-search"></i></a></li>
			</ul>
			<div class="search-element">
<!--input class="form-control" type="search" placeholder="Search" aria-label="Search">
<button class="btn" type="submit"><i class="ion ion-search"></i></button-->
			</div>
	</form>

			<ul class="navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg">
						<i class="ion ion-android-person d-lg-none"></i>
						<div class="d-sm-none d-lg-inline-block"><?=$anick?>님, 반갑습니다!</div>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a href="/" class="dropdown-item has-icon" target="_blank"> <i class="ion ion-log-out"></i> 사이트로가기</a>
						<a href="/admin/logOut" class="dropdown-item has-icon"> <i class="ion ion-log-out"></i> Logout</a>
					</div>
				</li>
			</ul>
		</nav>
		<div class="main-sidebar">
			<aside id="sidebar-wrapper">
				<div class="sidebar-brand"><a href="/admin">ADMIN</a></div>
				<div class="sidebar-user">
					<div class="sidebar-user-picture"><img alt="image" src="<?=IMG_PATH?>/avatar/avatar-1.jpeg"></div>
					<div class="sidebar-user-details">
						<div class="user-name"><?=$aname?></div>
						<div class="user-role"><?if($this->session->userdata("ULV")==10){?>관리자<?}else if($this->session->userdata("ULV")==3){?>업체회원<?}else if($this->session->userdata("ULV")==2){?>전문가<?}?></div>
					</div>
				</div>
				<ul class="sidebar-menu">
					<li class="menu-header">Menu</li>
	<?php if($this->session->userdata("ULV") >= 4){?>
					<li<?if($this->uri->segment(2)==''){?> class="active"<?}?>><a href="/admin"><i class="ion ion-home"></i><span>메인으로</span></a></li>
					<li<?if($this->uri->segment(2)=='user'){?> class="active"<?}?>><a href="/admin/user"><i class="ion ion-android-contacts"></i><span>회원관리</span></a></li>
	<?}
	if($this->session->userdata("ULV") >= 3){?>
					<li<?if($this->uri->segment(2)=='campain'){?> class="active"<?}?>>
						<a href="javascript:;" class="has-dropdown"><i class="ion ion-map"></i><span>캠페인관리</span></a>
						<ul class="menu-dropdown">
							<li <?if($this->uri->segment(3)=='manage'){?> class="active"<?}?>><a href="/admin/campain/manage"><i class="ion ion-ios-minus-empty"></i>캠페인등록</a></li>
							<li <?if($this->uri->segment(3)=='list'){?> class="active"<?}?>><a href="/admin/campain/list"><i class="ion ion-ios-minus-empty"></i>캠페인현황</a></li>
							<li <?if($this->uri->segment(3)=='stats'){?> class="active"<?}?>><a href="/admin/campain/stats"><i class="ion ion-ios-minus-empty"></i>캠페인통계</a></li>
						</ul>
					</li>
	<?}else if($this->session->userdata("ULV") == 2){?>
					<li<?if($this->uri->segment(2)=='adminsms' && $this->uri->segment(3)==''){?> class="active"<?}?>><a href="/admin/adminsms"><i class="ion ion-home"></i><span>문자발송</span></a></li>
					<li<?if($this->uri->segment(2)=='adminsms' && $this->uri->segment(3)=='smslist'){?> class="active"<?}?>><a href="/admin/adminsms/smslist"><i class="ion ion-home"></i><span>문자발송 내역</span></a></li>
	<?php }?>

	<?php if($this->session->userdata("ULV") == 10){?>
					<li<?if($this->uri->segment(2)=='payment'){?> class="active"<?}?>>
						<a href="javascript:;" class="has-dropdown"><i class="ion ion-clipboard"></i><span>매출관리</span></a>
						<ul class="menu-dropdown">
							<li <?if($this->uri->segment(3)=='vip'){?> class="active"<?}?>><a href="/admin/payment/vip"><i class="ion ion-ios-minus-empty"></i>정기구독</a></li>
							<li <?if($this->uri->segment(3)=='refund'){?> class="active"<?}?>><a href="/admin/payment/refund"><i class="ion ion-ios-minus-empty"></i>구독해지</a></li>
							<li <?if($this->uri->segment(3)=='stock'){?> class="active"<?}?>><a href="/admin/payment/stock"><i class="ion ion-ios-minus-empty"></i>종목구매</a></li>
							<li <?if($this->uri->segment(3)=='paylog'){?> class="active"<?}?>><a href="/admin/payment/paylog"><i class="ion ion-ios-minus-empty"></i>정기구독실패</a></li>

							<li <?if($this->uri->segment(3)=='point'){?> class="active"<?}?>><a href="/admin/payment/point"><i class="ion ion-ios-minus-empty"></i>포인트 구매</a></li>
						</ul>
					</li>
					<li<?if($this->uri->segment(2)=='board' || $this->uri->segment(2)=='stock'  || $this->uri->segment(2)=='category' || $this->uri->segment(2)=='topboard'){?> class="active"<?}?>>
						<a href="javascript:;" class="has-dropdown"><i class="ion ion-android-list"></i><span>게시판관리</span></a>
						<ul class="menu-dropdown"<?if($this->uri->segment(2)=='board' || $this->uri->segment(2)=='stock'){?> style="display:block;"<?}?>>
							<li <?if($this->uri->segment(2)=='board'){?> class="active"<?}?>><a href="/admin/board"><i class="ion ion-ios-minus-empty"></i>공개정보</a></li>
							<li <?if($this->uri->segment(2)=='stock'){?> class="active"<?}?>><a href="/admin/stock"><i class="ion ion-ios-minus-empty"></i>종목추천</a></li>
							<li <?if($this->uri->segment(2)=='category'){?> class="active"<?}?>><a href="/admin/category"><i class="ion ion-ios-minus-empty"></i>카테고리 관리</a></li>
							<li <?if($this->uri->segment(2)=='topboard'){?> class="active"<?}?>><a href="/admin/topboard"><i class="ion ion-ios-minus-empty"></i>상단 게시물 관리</a></li>
						</ul>
					</li>
					<li<?if($this->uri->segment(2)=='privacy' || $this->uri->segment(2)=='banner' || $this->uri->segment(2)=='adminsms' || $this->uri->segment(2)=='push' || $this->uri->segment(2)=='point'){?> class="active"<?}?>>
						<a href="javascript:;" class="has-dropdown"><i class="ion ion-ios-gear"></i><span>사이트관리</span></a>
						<ul class="menu-dropdown">
							<li<?if($this->uri->segment(2)=='privacy' && $this->uri->segment(3)==''){?> class="active"<?}?>><a href="/admin/privacy"><i class="ion ion-ios-minus-empty"></i>약관/개인정보/기타</a></li>
							<li<?if($this->uri->segment(3)=='setperm'){?> class="active"<?}?>><a href="/admin/privacy/setperm"><i class="ion ion-ios-minus-empty"></i>admin권한관리</a></li>
							<li<?if($this->uri->segment(2)=='banner'){?> class="active"<?}?>><a href="/admin/banner"><i class="ion ion-ios-minus-empty"></i>배너 및 광고관리</a></li>
							<li<?if($this->uri->segment(2)=='adminsms' && $this->uri->segment(3)==''){?> class="active"<?}?>><a href="/admin/adminsms"><i class="ion ion-ios-minus-empty"></i>문자발송</a></li>
							<li<?if($this->uri->segment(2)=='adminsms' && $this->uri->segment(3)=='smslist'){?> class="active"<?}?>><a href="/admin/adminsms/smslist"><i class="ion ion-ios-minus-empty"></i>문자발송 내역</a></li>
							<li<?if($this->uri->segment(2)=='push' && $this->uri->segment(3)==''){?> class="active"<?}?>><a href="/admin/push"><i class="ion ion-ios-minus-empty"></i>PUSH발송</a></li>
							<li<?if($this->uri->segment(2)=='push' && $this->uri->segment(3)=='pushlist'){?> class="active"<?}?>><a href="/admin/push/pushlist"><i class="ion ion-ios-minus-empty"></i>PUSH발송 내역</a></li>
							<li<?if($this->uri->segment(2)=='point'){?> class="active"<?}?>><a href="/admin/point"><i class="ion ion-ios-minus-empty"></i>고객별 컨텐츠 공유확인</a></li>
						</ul>
					</li>
	<?php }?>
				</ul>
			</aside>
		</div>


<!--li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="ion ion-ios-bell-outline"></i></a>
<div class="dropdown-menu dropdown-list dropdown-menu-right">
<div class="dropdown-header">Notifications
<div class="float-right">
<a href="#">View All</a>
</div>
</div>
<div class="dropdown-list-content">
<a href="#" class="dropdown-item dropdown-item-unread">
<img alt="image" src="<?=IMG_PATH?>/avatar/avatar-1.jpeg" class="rounded-circle dropdown-item-img">
<div class="dropdown-item-desc">
<b>Kusnaedi</b> has moved task <b>Fix bug header</b> to <b>Done</b>
<div class="time">10 Hours Ago</div>
</div>
</a>
<a href="#" class="dropdown-item dropdown-item-unread">
<img alt="image" src="<?=IMG_PATH?>/avatar/avatar-2.jpeg" class="rounded-circle dropdown-item-img">
<div class="dropdown-item-desc">
<b>Ujang Maman</b> has moved task <b>Fix bug footer</b> to <b>Progress</b>
<div class="time">12 Hours Ago</div>
</div>
</a>
<a href="#" class="dropdown-item">
<img alt="image" src="<?=IMG_PATH?>/avatar/avatar-3.jpeg" class="rounded-circle dropdown-item-img">
<div class="dropdown-item-desc">
<b>Agung Ardiansyah</b> has moved task <b>Fix bug sidebar</b> to <b>Done</b>
<div class="time">12 Hours Ago</div>
</div>
</a>
<a href="#" class="dropdown-item">
<img alt="image" src="<?=IMG_PATH?>/avatar/avatar-4.jpeg" class="rounded-circle dropdown-item-img">
<div class="dropdown-item-desc">
<b>Ardian Rahardiansyah</b> has moved task <b>Fix bug navbar</b> to <b>Done</b>
<div class="time">16 Hours Ago</div>
</div>
</a>
<a href="#" class="dropdown-item">
<img alt="image" src="<?=IMG_PATH?>/avatar/avatar-5.jpeg" class="rounded-circle dropdown-item-img">
<div class="dropdown-item-desc">
<b>Alfa Zulkarnain</b> has moved task <b>Add logo</b> to <b>Done</b>
<div class="time">Yesterday</div>
</div>
</a>
</div>
</div>
</li-->