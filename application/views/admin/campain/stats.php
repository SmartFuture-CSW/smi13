<style>
	.txtCopy{cursor:pointer;}
</style>

<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>캠페인 통계</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>캠페인명</th>
										<th>디바이스</th>
										<th>유입수</th>
										<th>앱설치수</th>
										<th>인증수</th>
										<th>구독수</th>
									</tr>
								</thead>
								<tbody>
								<?php foreach($stats as $row){?>
									<tr>
										<td><?=$row['vCampain']?></td>
										<td><?=($row['vAdDevice'] == 1) ? "WEB" : "APP"; ?></td>
										<td><?=$row['click']?></td>
										<td><?=($row['vAdDevice'] == 2) ? $row['install'] : "해당없음"; ?></td>
										<td><?=(empty($row['join'])) ? '0' : $row['join'];?></td>
										<td><?=(empty($row['pay'])) ? '0' : $row['pay'];?></td>
									</tr>
								<?php }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="card card-primary">
					<div class="card-header">
						<div class="float-right" style="width:70%;">
	<form name="frm" id="frm" method="post" action="/admin/campain/stats" style="width:100%;">
		<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo;?>">
		<input type="hidden" name="ex" id="ex" value="">
							<div class="input-group" style="width:100%;">
								<select class="form-control searchField" name="limit" id="limit" style="height:30px;padding:0px 3px; font-size:11px; color:#000;">
									<option value="">목록수</option>
									<option value="20" <?php if(@$_POST['limit']=='20') echo 'selected';?>>20개</option>
									<option value="50" <?php if(@$_POST['limit']=='50') echo 'selected';?>>50개</option>
								</select>
								<select class="form-control  searchField" name="campain" id="campain" style="height:30px;padding:0px 3px; font-size:11px; color:#000;">
									<option value="">캠페인</option>
									<?php foreach($campain as $row){?>
									<option value="<?=$row['nSeqNo']?>" <?php if(@$_POST['campain']==$row['nSeqNo']) echo 'selected';?>><?=$row['vCampain']?></option>
									<?php } ?>
								</select>
								<select class="form-control  searchField" name="nInstall" id="nInstall" style="height:30px;padding:0px 3px; font-size:11px; color:#000;">
									<option value="">디바이스</option>
									<option value="0" <?php if(@$_POST['nInstall']=='0') echo 'selected';?>>WEB</option>
									<option value="1" <?php if(@$_POST['nInstall']=='1') echo 'selected';?>>APP</option>
								</select>
								<select class="form-control  searchField" name="nJoin" id="nJoin" style="height:30px;padding:0px 3px; font-size:11px; color:#000;">
									<option value="">인증여부</option>
									<option value="0" <?php if(@$_POST['nJoin']=='0') echo 'selected';?>>인증안함</option>
									<option value="1" <?php if(@$_POST['nJoin']=='1') echo 'selected';?>>인증완료</option>
								</select>
								<select class="form-control  searchField" name="nPay" id="nPay" style="height:30px;padding:0px 3px; font-size:11px; color:#000;">
									<option value="">구독여부</option>
									<option value="0" <?php if(@$_POST['nPay']=='0') echo 'selected';?>>구독안함</option>
									<option value="1" <?php if(@$_POST['nPay']=='1') echo 'selected';?>>구독완료</option>
								</select>
								<select name="keyfield" class="form-control"  style="height:30px;padding:0px 3px; width:30px;  font-size:11px; color:#000;">
									<option value="vReferer" <?php if(@$_POST['keyfield'] == 'vReferer') echo 'selected'; ?>>REFERER KEY</option>
								</select>
								<input type="text" name="keyword" id="keyword" class="form-control" placeholder="검색어" value="<?=@$_POST['keyword'];?>">
								<div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
							</div>
	</form>							
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th width="6%">번호</th>
										<th>캠페인명</th>
										<th>Referer</th>
										<th>광고 디바이스</th>
										<th>이름(닉네임)</th>
										<th>가입일</th>
										<th>앱설치 여부</th>
										<th>인증 여부</th>
										<th>구독 여부</th>
										<th>Full URL</th>
									</tr>
								</thead>
								<tbody>
					<?php foreach ($list as $value) { ?>
									<tr>
										<td><?=$no--?></td>
										<td><?php
										if(array_key_exists($value['vReferer'], $aCamapin)){
											echo $aCamapin[$value['vReferer']];
										} else {
											echo '직접설치';
										}
										?>
										</td>
										<td><?=$value['vReferer']?></td>
										<td><?=($value['nInstall'] == 1) ? 'APP' : "WEB"; ?></td>

										<td><?=empty($value['vName']) ? "-" : $value['vName']."(".$value['vNick'].")"?></td>
										<td><?=$value["dtJoinDate"]?></td>

										<td><?=($value['nInstall'] == 1) ? '앱설치함' : "해당없음"; ?></td>
										<td><?=($value['nJoin'] == 1) ? "<span style='color:blue; font-weighgt:700;'>인증완료</span>" : "인증안함"; ?></td>
										<td><?=($value['nPay'] == 1) ? "<span style='color:blue; font-weighgt:700;'>구독완료</span>" : "구독안함"; ?></td>
										<td>
											<?
											if($value['nInstall'] == 1){
												echo "https://play.google.com/store/apps/details?id=com.stockboss.stock&referrer=". $value['vReferer'];
											}else{
												echo $value['fullReferer'];
											}
											?>
										</td>

									</tr>
					<?php } #foreach 
						if(count($list)==0){?>
									<tr>
										<td colspan="10" align='center' valign='middle' height='120'><br><br>No Data.</td>
									</tr>`
					<?php }?>
								</tbody>
							</table>
							<?=$paging?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script  src="//code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('.txtCopy').on('click',function(){
			url=$(this).html();
			var IE=(document.all)?true:false;
			if(IE){
				window.clipboardData.setData("Text",url);
			}else{
				temp = prompt("Ctrl+C를 눌러 복사하세요",url);
			}
		});

		$(".searchField").off("change").on("change", function(){
			$("#frm").submit();
			
		});

	});

	function pageRemote(pageNo){
		$('#pageNo').val(pageNo);
		document.frm.submit();
	}

	function go_del(){
		var ddata='';
		var isFirst = true;
		$('input:checkbox[name=chkchk]').each(function() {

			if($(this).is(':checked')){
				if(isFirst == true){
					ddata += ($(this).val())
					isFirst = false;
				} else {
					ddata += ","+ ($(this).val())
				}
			}
		});
		if(ddata==''){
			alert("삭제할 회원을 선택해주세요.");
			return false;
		}
		if(confirm("삭제를 진행하시겠습니까?")){
			$.post("/admin/campain/removeCampain",{delno:ddata},function(data){
				alert('삭제완료!');
				document.location.href='/admin/campain/list';
			},'json');
		}
	}
</script>