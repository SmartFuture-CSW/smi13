
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>캠페인 등록</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-body">
						<div class="row">
							<div class="form-group col-6">
								<label>캠페인 이름</label>
								<div class="input-group">
									<input type="text" id="vCampain" name="vCampain" class="form-control"  placeholder="">
								</div>
							</div>
							<div class="form-group col-3">
								<label>REFERER KEY</label>
								<div class="input-group">
									<input type="text" id="vReferer" name="vReferer" class="form-control" placeholder="URL을 결정하는 중요키">
								</div>
							</div>
							<div class="form-group col-3">
								<label>캠페인 디바이스</label>
								<div class="input-group">
									<select class="form-control" name="vAdDevice" id="vAdDevice" required="">
										<option value="1">WEB</option>
										<option value="2">APP</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label>담당자 연락처</label>
								<div class="input-group">
									<input type="text" id="vHp" name="vHp" class="form-control"  placeholder="">
								</div>
							</div>
							<div class="form-group col-6">
								<label>기본URL</label>
								<div class="input-group">
									<input type="text" id="vAdUrl" name="vAdUrl" class="form-control"  placeholder="" maxlength="100" value="https://www.nodajistock.co.kr/auth">
								</div>
							</div>
						</div>

<?php if($this->session->userdata('ULV') >= 10 ){ ?>
						<div class="row">
							<div class="form-group col-12">
								<label>업체회원</label>
								<div class="input-group">
									<select class="form-control" name="nUserNo" id="nUserNo" required="">
										<option value="">업체회원을 선택 해주세요</option>
									<?php foreach($adlist as $row){?>
										<option value="<?=$row['nSeqNo']?>">[<?=$row['vNick']?>] <?=$row['vName']?> </option>
									<?php } ?>
									</select>
								</div>
							</div>
						</div>
<?php } else { ?>
						<input type="hidden" name="nUserNo" id="nUserNo" value="<?=$this->session->userdata("UNO")?>">
<?php }?>

						<div class="form-group">
							<div style="clear:both;"></div>
							<div style="float:right;"><button type="button" class="btn btn-primary btn-block" onclick="postCampain()">등록</button></div>
							<div style="clear:both;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	function postCampain(){

		if($("input[name=vCampain]").val().trim() ==''){
			alert('캠페인명을 입력 해주세요.');
			return false;
		}
		if($("input[name=vReferer]").val().trim() ==''){
			alert('REFERER KEY를 입력 해주세요.');
			return false;
		}

		if($("input[name=vHp]").val().trim() ==''){
			alert('담당자 연락처를 입력 해주세요.');
			return false;
		}


<?php if($this->session->userdata('ULV') >= 10 ){ ?>

		if($("#nUserNo").val() == ""){
			alert("업체회원을 선택 해주세요");
			return false;
		}
<?php } ?>

		var data = {
			'vCampain' : $("input[name=vCampain]").val().trim()
			, 'vReferer' : $("input[name=vReferer]").val().trim()
			, 'vHp' : $("input[name=vHp]").val().trim()
			, 'vAdDevice' : $("select[name=vAdDevice]").val()
			, 'nUserNo' : $("#nUserNo").val()
			, 'vAdUrl' : $("#vAdUrl").val()
		}

		var u = "/admin/campain/postCampain";

		$.ajax({
			type : "post",
			url : u,
			dataType : 'json',
			data : data,
			success : function(response) {
				if(response.status == "FAIL"){
					alert(response.message);
				}
				else{
					alert('등록 완료');
					window.location.reload();
				}


			},
			error : function(response) {
				console.log("ERROR : " + response)
			}
		});

	}
</script>