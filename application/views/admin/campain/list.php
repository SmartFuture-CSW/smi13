<style>
	.txtCopy{cursor:pointer;}
</style>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>캠페인 현황</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-header">
						<div class="float-right" style="width:50%;">
	<form name="frm" id="frm" method="post" action="/admin/campain/list" style="width:100%;">
		<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo;?>">
		<input type="hidden" name="ex" id="ex" value="">
							<div class="input-group" style="width:100%;">
								<select class="form-control" name="limit" id="limit" style="height:30px;padding:0px 3px; font-size:11px; color:#000;">
									<option value="">목록수</option>
									<option value="20" <?php if(@$_POST['limit']=='20') echo 'selected';?>>20개</option>
									<option value="50" <?php if(@$_POST['limit']=='50') echo 'selected';?>>50개</option>
								</select>
								<select name="keyfield" class="form-control"  style="height:30px;padding:0px 3px; width:30px;  font-size:11px; color:#000;">
									<option value="vName" <?php if(@$_POST['keyfield'] == 'vName') echo 'selected'; ?>>담당자명</option>
									<option value="vCampain" <?php if(@$_POST['keyfield'] == 'vCampain') echo 'selected'; ?>>캠페인명</option>
									<option value="vReferer" <?php if(@$_POST['keyfield'] == 'vReferer') echo 'selected'; ?>>REFERER KEY</option>
								</select>
								<input type="text" name="keyword" id="keyword" class="form-control" placeholder="검색어" value="<?=@$_POST['keyword'];?>">
								<div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
							</div>
	</form>							
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th class="text-center">
											<div class="custom-checkbox custom-control">
												<input type="checkbox" class="custom-control-input" id="checkbox-all">
												<label for="checkbox-all" class="custom-control-label"></label>
											</div>
										</th>
										<th width="60px">번호</th>
										<th>캠페인명</th>
										<th width="120px">담당자명</th>
										<th width="120px">회사명(닉네임)</th>
										<th width="80px">디바이스</th>
										<th width="160px">등록일</th>
										<th>광고URL</th>
									</tr>
								</thead>
								<tbody>
					<?php foreach ($list as $value) { ?>
									<tr>
										<td width="40">
											<div class="custom-checkbox custom-control">
												<input type="checkbox" id="checkbox<?=$value['nSeqNo'];?>" name="chkchk" class="custom-control-input" value="<?=$value['nSeqNo'];?>" >
												<label for="checkbox<?=$value['nSeqNo'];?>" class="custom-control-label"></label>
											</div>
										</td>
										<td><?=$no--?></td>
										<td><?=$value['vCampain']?></td>
										<td><?=$value['vName'];?></td>
										<td><?=$value['vNick'];?></td>
										<td><?=($value['vAdDevice'] == 1) ? "WEB" : "APP"; ?></td>
										<td><?=$value['dtRegDate']?></td>
										<td class="txtCopy"><?=$value['vAdUrl'];?></td>

									</tr>
					<?php } #foreach 
						if(count($list)==0){?>
									<tr>
										<td colspan="9" align='center' valign='middle' height='120'><br><br>No Data.</td>
									</tr>`
					<?php }?>
								</tbody>
							</table>
						</div>
						<div><a href="javascript:void(0);" class="btn btn-danger btn-action" onclick="go_del()"><i class="ion ion-trash-b"></i> 선택삭제</a></div>
						<?=$paging?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script  src="//code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('.txtCopy').on('click',function(){
			url=$(this).text();
			var IE=(document.all)?true:false;
			if(IE){
				window.clipboardData.setData("Text",url);
			}else{
				temp = prompt("Ctrl+C를 눌러 복사하세요",url);
			}
		});
	});

	function pageRemote(pageNo){
		$('#pageNo').val(pageNo);
		document.frm.submit();
	}

	function go_del(){
		var ddata='';
		var isFirst = true;
		$('input:checkbox[name=chkchk]').each(function() {

			if($(this).is(':checked')){
				if(isFirst == true){
					ddata += ($(this).val())
					isFirst = false;
				} else {
					ddata += ","+ ($(this).val())
				}
			}
		});
		if(ddata==''){
			alert("삭제할 회원을 선택해주세요.");
			return false;
		}
		if(confirm("삭제를 진행하시겠습니까?")){
			$.post("/admin/campain/removeCampain",{delno:ddata},function(data){
				alert('삭제완료!');
				document.location.href='/admin/campain/list';
			},'json');
		}
	}
</script>