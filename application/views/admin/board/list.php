 <style>
.auto{background-color:#5D5D5D;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
 </style>
 <div class="main-content">
        <section class="section">
          <h1 class="section-header"><div>공개정보</div></h1>
         	<div class="row">
              <div class="col-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <div class="float-right" style="width:100%;">
                      <form name="frm" id="frm" method="post" action="/admin/board" style="width:100%;">
                        <input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo;?>">
                        <input type="hidden" name="bd_no" id="bd_no" value="">
						<input type="hidden" name="mode" id="mode" value="">
						<input type="hidden" name="udname" id="udname" value="">
						<input type="hidden" name="udval" id="udval" value="">
                        <div class="input-group" style="width:100%;">
							<select class="form-control" name="vType" id="vType" style="height:30px;padding:0px 3px;" onchange="list_refresh(this.form)">
		                        <option value="today">TODAY</option>
		                        <option value="important" <?php if(@$_POST['vType']=='important') echo 'selected';?>>추천종목</option>
								<option value="invest" <?php if(@$_POST['vType']=='invest') echo 'selected';?>>투자정보</option>
								<option value="study" <?php if(@$_POST['vType']=='study') echo 'selected';?>>주식공부</option>
								<!--option value="news" <?php if(@$_POST['vType']=='news') echo 'selected';?>>뉴스</option-->
								<option value="community" <?php if(@$_POST['vType']=='community') echo 'selected';?>>커뮤니티</option>
								<option value="reply" <?php if(@$_POST['vType']=='reply') echo 'selected';?>>댓글관리</option>
								<option value="notice" <?php if(@$_POST['vType']=='notice') echo 'selected';?>>공지사항</option>
								<option value="key" <?php if(@$_POST['vType']=='key') echo 'selected';?>>핵심정보</option>
                      		</select>
                        	<select class="form-control" name="limit" id="limit" style="height:30px;padding:0px 3px;">
		                        <option value="">목록수</option>
		                        <option value="20" <?php if(@$_POST['limit']=='20') echo 'selected';?>>20개</option>
								<option value="50" <?php if(@$_POST['limit']=='50') echo 'selected';?>>50개</option>
                      		</select>
						<?if($bo_table=='community' || $bo_table=='reply'){?>
                        	<select class="form-control" name="orderby" id="orderby" style="height:30px;padding:0px 3px;">
		                        <option value="nSeqNo" <?php if(@$_POST['orderby']=='nSeqNo') echo 'selected';?>>최신순</option>
								<option value="nLike" <?php if(@$_POST['orderby']=='nLike') echo 'selected';?>>좋아요순</option>
                      		</select>
							<input type="text" name="reg1" id="reg1" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@$_POST['reg1'];?>" placeholder="등록일 시작"> ~ 
							<input type="text" name="reg2" id="reg2" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@$_POST['reg2'];?>" placeholder="등록일 종료">
						<?}?>
	                        <input type="text" name="keyword" id="keyword" class="form-control" placeholder="<?if($bo_table=='reply'){?>내용 or 작성자<?}else{?>제목 or 내용<?}?>" value="<?=@$_POST['keyword'];?>">
                          <div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                        	<tr>
	                        <th width="6%">번호</th>
		                      <th>제목</th>
		                      <th>작성자</th>
		                      <th>등록일</th>
							 <?if($bo_table=='community' || $bo_table=='reply'){?>
								<th>좋아요수</th>
							 <?}?>
							 <?if($bo_table!='reply'){?>
								<th>조회수</th>
							  <?}?>
							  <?if($bo_table=='today' || $bo_table=='important'){?>
								<th>공유수</th>
							  <?}?>

							  <?if($bo_table == 'key'){?>
								  <th>URL</th>
							<?}?>
		                      <th>관리</th>
                        </tr>
                        </thead>
                        <tbody>
						<?php foreach ($list as $value) { ?>
                        <tr<?if($value['emBlind']=='Y'){?> style="background-color:#b0b0b0;" title="해당 게시물은 블라인드 처리되었습니다."<?}?>>
							  <td><?=$no--?></td>
		                      <td>
								<?if($bo_table=='community' || $bo_table=='reply'){
										if($bo_table=='reply'){
											$title=$this->util->cut_str($value['txComment'],45);
											$cont=$value['txComment'];
										}else{
											$title=$value['vSubject'];
											$cont=$value['txContent'];
										}?>
									<span class="modal-go tit_<?=$value['nSeqNo']?>" onclick="set_modal('<?=$bo_table?>|<?=$value['nSeqNo']?>')" style="cursor:pointer;"><?=$title?></span><div style='display:none;' id="cont_<?=$value['nSeqNo']?>"><?=$cont?></div>
								<?}else{
									echo $value['vSubject'];
								}?>
							  </td>
		                      <td><?=$value['vName'];?></td>
		                      <td><?=$value['dtRegDate'];?></td>
							 <?if($bo_table=='community' || $bo_table=='reply'){?>
								 <td><?=$value['nLike'];?></td>
							 <?}?>
							 <?if($bo_table!='reply'){?>
			                      <td><?=$value['nHit'];?></td>
							  <?}?>
							  <?if($bo_table=='today' || $bo_table=='important'){?>
								  <td><?=$value['nShare'];?></td>
							  <?}?>
							  <?if($bo_table=='key'){?>
							  <td><?=$value['encurl']?></td>
							<?}?>
							  <td>
							  <?if($bo_table=='community' || $bo_table=='reply'){?>
								<a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Blind" onclick="proc_bd('Update','<?=$value['nSeqNo']?>|emBlind|<?=($value['emBlind']=='Y'?'N':'Y')?>')"><i class="fa fa-eye-slash"></i></a>
							  <?}else{?>
							  <?}?>
								<a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit" onclick="proc_bd('Edit','<?=$value['nSeqNo']?>')"><i class="ion ion-edit"></i></a>
								<a class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete" onclick="proc_bd('Del','<?=$value['nSeqNo']?>')"><i class="ion ion-trash-b"></i></a>
							  </td>
                        </tr>
						<?php } #foreach ?>
                      </tbody>
                    </table>
                    </div>
                    <div>
						<div style="clear:both;"></div>
						<?php /*if($bo_table=='today'){?><div style="float:left;margin-left:5px;"><a href="javascript:void(0);" class="btn btn-primary modal-go" onclick="set_modal('today')"> Today</a></div><?}?>
						<?php if($bo_table=='important'){?><div style="float:left;margin-left:5px;"><a href="javascript:void(0);" class="btn btn-primary modal-go" onclick="set_modal('important')"> 추천종목</a></div><?}?>

						<?php if($bo_table=='invest'){?><div style="float:left;margin-left:5px;"><a href="javascript:void(0);" class="btn btn-primary modal-go" onclick="set_modal('invest')"> 투자정보</a></div><?}?>

						<?php if($bo_table=='study'){?><div style="float:left;margin-left:5px;"><a href="javascript:void(0);" class="btn btn-primary modal-go" onclick="set_modal('study')"> 주식공부</a></div><?} */?>


						<?if( $bo_table!='reply' && $bo_table!='today'){?><div style="float:right;"><a href="javascript:void(0);" class="btn btn-primary" onclick="proc_bd('Edit','')"> 생성</a></div><?}?>
						<div style="clear:both;"></div>
					</div>
                    <?=$paging?>
                  </div>
                </div>
              </div>
            </div>
	</section>
</div>

<script type="text/javascript">
  function pageRemote(pageNo){
    $('#pageNo').val(pageNo);
    document.frm.submit();
  }
  function proc_bd(mode,no){
	if(mode=='Del'){
		if(confirm("정말로 삭제하시겠습니까?")){
			$('#mode').val(mode);
			$('#bd_no').val(no);
			document.frm.action='/admin/board/bbs'+mode;
			document.frm.submit();
		}
	}else if(mode=='Update'){
		if(confirm("정말로 상태를 변경하시겠습니까?")){
			no=no.split('|');
			$('#mode').val(mode);
			$('#bd_no').val(no[0]);
			$('#udname').val(no[1]);
			$('#udval').val(no[2]);
			document.frm.action='/admin/board/bbs'+mode;
			document.frm.submit();
		}
	}else{
		$('#mode').val(mode);
		$('#bd_no').val(no);
		document.frm.action='/admin/board/bbs'+mode;
		document.frm.submit();
	}
  }

	function set_modal(cate){
		$('.close').remove();
		$('.modal-body').empty();
		var sstr='';


		// 공개정보 > Today 메인 설정
		if(cate=='today'){
			var tit='TODAY 메인설정';
			var str='<div style="padding-bottom:10px;">TODAY <span class="auto" onclick="auto_select(1)">최신순 자동선택</span></div><table style="width:100%;">';
			for(i=1;i<=5;i++){
				str += '<tr><td>'+i+'순위</td><td>';
				str += '<select class="form-control" id="vType_select_1_'+i+'" style="height:30px;padding:0px" onchange="change_board(this.value,1,'+i+')">';
				str += '<option value="">게시판을 선택해주십시오.</option>';
				str += '<option value="today">TODAY</option>';
				//str += '<option value="important">추천종목</option>';
				//str += '<option value="news">뉴스</option>';
				str += '<!--option value="community">커뮤니티</option-->';
				str += '</select></td><td>';
				str += '<select id="bdno_1_'+i+'" class="form-control" style="height:30px;padding:0px;width:211px;"><option value="">컨텐츠를 선택해주십시오.</option></select></td><tr>';
			}
			str+='</table>';
			str+='<div style="padding-top:25px;padding-bottom:10px;">추천종목 <span class="auto" onclick="auto_select(2)">최신순 자동선택</span></div><table style="width:100%;">';
			// TODAY 하단 메인 게시글 5개 @ 4개로 수정 > 투자정보 게시판에서 추출
			for(i=1;i<=4;i++){
				str += '<tr><td>'+i+'순위</td>';
				str += '<td><select class="form-control" id="vType_select_2_'+i+'" style="height:30px;padding:0px" onchange="change_board(this. value, 2 ,'+i+')">';
				str += '<option value="">게시판을 선택해주십시오.</option>';
				str += '<option value="important">추천종목</option>';
				str += '</select></td>';
				str += '<td><select id="bdno_2_'+i+'" class="form-control" style="height:30px;padding:0px;width:211px;"><option value="">컨텐츠를 선택해주십시오.</option></select></td>';
				str += '<tr>';
			}
			str+='</table>';


			str+='<div style="padding-top:25px;padding-bottom:10px;">투자정보 <span class="auto" onclick="auto_select(9)">최신순 자동선택</span></div><table style="width:100%;">';
			// TODAY 하단 메인 게시글 5개 @ 4개로 수정 > 투자종목 게시판에서 추출
			for(i=1;i<=4;i++){
				str += '<tr><td>'+i+'순위</td>';
				str += '<td><select class="form-control" id="vType_select_9_'+i+'" style="height:30px;padding:0px" onchange="change_board(this. value, 9 ,'+i+')">';
				str += '<option value="">게시판을 선택해주십시오.</option>';
				str += '<option value="invest">투자정보</option>';
				str += '</select></td>';
				str += '<td><select id="bdno_9_'+i+'" class="form-control" style="height:30px;padding:0px;width:211px;"><option value="">컨텐츠를 선택해주십시오.</option></select></td>';
				str += '<tr>';
			}
			str+='</table>';


			str+='<div style="padding-top:25px;padding-bottom:10px;">주식공부 <span class="auto" onclick="auto_select(10)">최신순 자동선택</span></div><table style="width:100%;">';
			// TODAY 하단 메인 게시글 5개 @ 4개로 수정 > 투자정보 게시판에서 추출
			for(i=1;i<=4;i++){
				str += '<tr><td>'+i+'순위</td>';
				str += '<td><select class="form-control" id="vType_select_10_'+i+'" style="height:30px;padding:0px" onchange="change_board(this. value, 10 ,'+i+')">';
				str += '<option value="">게시판을 선택해주십시오.</option>';
				str += '<option value="study">주식공부</option>';
				str += '</select></td>';
				str += '<td><select id="bdno_10_'+i+'" class="form-control" style="height:30px;padding:0px;width:211px;"><option value="">컨텐츠를 선택해주십시오.</option></select></td>';
				str += '<tr>';
			}
			str+='</table>';


			str+='<div style="text-align:center;padding-top:15px;"><button type="button" class="btn btn-primary" onclick="all_save(\''+cate+'\')">저장</button></div>';

			$.post("/admin/board/popmain",{mainval:"1"},function(data){
				for(i=1;i<=5;i++){
					if(data.cont[i].vType!=''){
						$('#vType_select_1_'+i).val(data.cont[i].vType).prop("selected",true);
					}
					if(data.cont[i].bdno!=''){
						sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
						$.each(data.cont[i].vTypeList, function(idx,item){
							sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
						});
						$('#bdno_1_'+i).empty().append(sstr);
						$('#bdno_1_'+i).val(data.cont[i].bdno).prop("selected",true);
					}
				}
			},'json');

			$.post("/admin/board/popmain",{mainval:"2"},function(data){
				for(i=1;i<=4;i++){
					if(data.cont[i].vType!=''){
						$('#vType_select_2_'+i).val(data.cont[i].vType).prop("selected",true);
					}
					if(data.cont[i].bdno != ''){
						sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
						$.each(data.cont[i].vTypeList, function(idx,item){
							sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
						});
						$('#bdno_2_'+i).empty().append(sstr);
						$('#bdno_2_'+i).val(data.cont[i].bdno).prop("selected",true);
					}
				}
			},'json');

			
			$.post("/admin/board/popmain",{mainval:"9"},function(data){
				for(i=1;i<=4;i++){
					if(data.cont[i].vType!=''){
						$('#vType_select_9_'+i).val(data.cont[i].vType).prop("selected",true);
					}
					if(data.cont[i].bdno != ''){
						sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
						$.each(data.cont[i].vTypeList, function(idx,item){
							sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
						});
						$('#bdno_9_'+i).empty().append(sstr);
						$('#bdno_9_'+i).val(data.cont[i].bdno).prop("selected",true);
					}
				}
			},'json');

			$.post("/admin/board/popmain",{mainval:"10"},function(data){

				console.log(data);

				for(i=1;i<=4;i++){
					if(data.cont[i].vType!=''){
						$('#vType_select_10_'+i).val(data.cont[i].vType).prop("selected",true);
					}
					if(data.cont[i].bdno != ''){
						sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
						$.each(data.cont[i].vTypeList, function(idx,item){
							sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
						});
						$('#bdno_10_'+i).empty().append(sstr);
						$('#bdno_10_'+i).val(data.cont[i].bdno).prop("selected",true);
					}
				}
			},'json');

		// 추천종목 리스트 메인페이지 상위 5개 게시물 설정
		}else if(cate=='important'){
			var tit='추천종목 메인설정';
			var str='<div style="padding-bottom:10px;">추천종목 메인 <span class="auto" onclick="auto_select(3)">조회수순 자동선택</span></div><table style="width:100%;">';
			for(i=1;i<=5;i++){
				str += '<tr><td>'+i+'순위</td>';
				str += '<td><select class="form-control" id="vType_select_3_'+i+'" style="height:30px;padding:0px" onchange="change_board(this.value, 3,'+i+')">';
				str += '<option value="">게시판을 선택해주십시오.</option>';
				//str += '<option value="today">TODAY</option>';
				str += '<option value="important">추천종목</option>';
				//str += '<option value="news">뉴스</option>';
				//str += '<option value="community">커뮤니티</option>';
				str += '</select></td>';
				str += '<td><select id="bdno_3_'+i+'" class="form-control" style="height:30px;padding:0px;width:211px;">';
				str += '<option value="">컨텐츠를 선택해주십시오.</option></select></td><tr>';
			}
			str+='</table>';
			str+='<div style="text-align:center;padding-top:15px;"><button type="button" class="btn btn-primary" onclick="all_save(\''+cate+'\')">저장</button></div>';
			$.post("/admin/board/popmain",{mainval:"3"},function(data){
				for(i=1;i<=5;i++){
					if(data.cont[i].vType!=''){
						$('#vType_select_3_'+i).val(data.cont[i].vType).prop("selected",true);
					}
					if(data.cont[i].bdno!=''){
						sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
						$.each(data.cont[i].vTypeList, function(idx,item){
							sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
						});
						$('#bdno_3_'+i).empty().append(sstr);
						$('#bdno_3_'+i).val(data.cont[i].bdno).prop("selected",true);
					}
				}
			},'json');

		// 투자종목 리스트 메인페이지 상위 5개 게시물 설정
		}else if(cate=='invest'){
			var tit='투자정보 메인설정';
			var str='<div style="padding-bottom:10px;">투자정보 메인 <span class="auto" onclick="auto_select(7)">조회수순 자동선택</span></div><table style="width:100%;">';
			for(i=1;i<=5;i++){
				str += '<tr><td>'+i+'순위</td><td>';
				str += '<select class="form-control" id="vType_select_7_'+i+'" style="height:30px;padding:0px" onchange="change_board(this.value,7,'+i+')">';
				str += '<option value="">게시판을 선택해주십시오.</option>';
				//str += '<option value="today">TODAY</option>';
				str += '<option value="invest">투자정보</option>';
				//str += '<option value="news">뉴스</option>';
				//str += '<option value="community">커뮤니티</option>';
				str += '</select></td><td>';
				str += '<select id="bdno_7_'+i+'" class="form-control" style="height:30px;padding:0px;width:211px;">';
				str += '<option value="">컨텐츠를 선택해주십시오.</option>';
				str += '</select></td><tr>';
			}
			str+='</table>';
			str+='<div style="text-align:center;padding-top:15px;"><button type="button" class="btn btn-primary" onclick="all_save(\''+cate+'\')">저장</button></div>';
			$.post("/admin/board/popmain",{mainval:"7"},function(data){

				console.log(data);
				for(i=1;i<=5;i++){
					if(data.cont[i].vType!=''){
						$('#vType_select_7_'+i).val(data.cont[i].vType).prop("selected",true);
					}
					if(data.cont[i].bdno!=''){
						sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
						$.each(data.cont[i].vTypeList, function(idx,item){
							sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
						});
						$('#bdno_7_'+i).empty().append(sstr);
						$('#bdno_7_'+i).val(data.cont[i].bdno).prop("selected",true);
					}
				}
			},'json');
		// 투자종목 리스트 메인페이지 상위 5개 게시물 설정
		}else if(cate=='study'){
			var tit='주식공부 메인설정';
			var str='<div style="padding-bottom:10px;">주식공부 메인 <span class="auto" onclick="auto_select(8)">조회수순 자동선택</span></div><table style="width:100%;">';
			for(i=1;i<=5;i++){
				str += '<tr><td>'+i+'순위</td><td>';
				str += '<select class="form-control" id="vType_select_8_'+i+'" style="height:30px;padding:0px" onchange="change_board(this.value,8,'+i+')">';
				str += '<option value="">게시판을 선택해주십시오.</option>';
				//str += '<option value="today">TODAY</option>';
				str += '<option value="study">주식공부</option>';
				//str += '<option value="news">뉴스</option>';
				//str += '<option value="community">커뮤니티</option>';
				str += '</select></td><td>';
				str += '<select id="bdno_8_'+i+'" class="form-control" style="height:30px;padding:0px;width:211px;">';
				str += '<option value="">컨텐츠를 선택해주십시오.</option>';
				str += '</select></td><tr>';
			}
			str+='</table>';
			str+='<div style="text-align:center;padding-top:15px;"><button type="button" class="btn btn-primary" onclick="all_save(\''+cate+'\')">저장</button></div>';
			$.post("/admin/board/popmain",{mainval:"8"},function(data){
				for(i=1;i<=5;i++){
					if(data.cont[i].vType!=''){
						$('#vType_select_8_'+i).val(data.cont[i].vType).prop("selected",true);
					}
					if(data.cont[i].bdno!=''){
						sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
						$.each(data.cont[i].vTypeList, function(idx,item){
							sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
						});
						$('#bdno_8_'+i).empty().append(sstr);
						$('#bdno_8_'+i).val(data.cont[i].bdno).prop("selected",true);
					}
				}
			},'json');
		}else if(cate.substring(0,5)=='commu' || cate.substring(0,5)=='reply'){
			var cate_arr=cate.split('|');
			var tit=$('.tit_'+cate_arr[1]).html();
			var str='<div style="width:100%;height:500px;overflow-x:hidden;overflow-y:scroll;border:1px solid silver;">'+$('#cont_'+cate_arr[1]).html()+'</div>';
		}
		$('.modal-body').append(str);
		$('.modal-header').empty().append('<h5 class="modal-title">'+tit+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color:#fff;"> x </button>');
	}

	function change_board(v,m,n){
		var sstr='';
		$('#bdno_'+m+'_'+n).empty();
		if(v==''){
			sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
			$('#bdno_'+m+'_'+n).append(sstr);
		}else{
			var data = {
				vType : v,
				mailval : m
			}
			$.post("/admin/board/popmain2",data,function(response){
				sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
				if(response.cont){
					$.each(response.cont, function(idx,item){
						sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
					});
				}
				$('#bdno_'+m+'_'+n).append(sstr);
			},'json');
		}
	}

	function all_save(cate){
		var str='';
		if(cate=='today'){
			for(j=1;j<=10;j++){
				if(j > 2 && j < 9){
					continue;
				}
				for(i=1;i<=5;i++){
					str+=i+','+$('#vType_select_'+j+'_'+i+' option:selected').val()+','+$('#bdno_'+j+'_'+i+' option:selected').val()+'|';
				}
				str+='<nodaji>';
			}
		}else if(cate=='important'){
			for(i=1;i<=5;i++){
				str+=i+','+$('#vType_select_3_'+i+' option:selected').val()+','+$('#bdno_3_'+i+' option:selected').val()+'|';
			}
		}else if(cate=='invest'){
			for(i=1;i<=5;i++){
				str+=i+','+$('#vType_select_7_'+i+' option:selected').val()+','+$('#bdno_7_'+i+' option:selected').val()+'|';
			}
		}else if(cate=='study'){
			for(i=1;i<=5;i++){
				str+=i+','+$('#vType_select_8_'+i+' option:selected').val()+','+$('#bdno_8_'+i+' option:selected').val()+'|';
			}
		}


		$.post("/admin/board/popupdate",{fval:str,category:cate},function(data){$('.close').trigger('click');},'json');
	}

	function auto_select(no){
		$.post("/admin/board/popautolist",{mainval:no},function(data){
			for(i=1;i<=5;i++){
				if(data.cont[i].vType!=''){
					$('#vType_select_'+no+'_'+i).val(data.cont[i].vType).prop("selected",true);
				}
				if(data.cont[i].bdno!=''){
					sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
					$.each(data.cont[i].vTypeList, function(idx,item){
						sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
					});
					$('#bdno_'+no+'_'+i).empty().append(sstr);
					$('#bdno_'+no+'_'+i).val(data.cont[i].bdno).prop("selected",true);
				}
			}
		},'json');
	}


	function list_refresh(f){
		f.submit();
	}


</script>