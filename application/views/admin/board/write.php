<script src="/public/admin/modules/jquery.min.js"></script>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>공개정보</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-body">
					<form name="frm" id="frm" method="post" action="/admin/board/bbsInsert" onsubmit="return send(this)" enctype="multipart/form-data">
						<input type="hidden" name="bo_table" id="bo_table" value="<?=$bo_table?>">
						<input type="hidden" name="vType" id="vType" value="<?=$bo_table?>">
						<input type="hidden" name="bd_no" id="bd_no" value="<?=@$write[0]['nSeqNo']?>">
						<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>">
						<input type="hidden" name="vImage_ori" id="vImage_ori" value="<?=@$write[0]['vImage']?>">
						<input type="hidden" name="vImage2_ori" id="vImage2_ori" value="<?=@$write[0]['vImage2']?>">
						<div class="form-divider"> 컨텐츠 작성 (<?=$bdname[$bo_table]?>) </div>
						<div class="row">
							<div class="form-group col-6">
								<label>작성자</label>
								<div class="input-group"><input type="text" id="vName" name="vName" class="form-control" value="<?=@$write[0]['vName']?>" required></div>
							</div>
							<div class="form-group col-4">
								<label>해시태그</label>
								<div class="input-group"><input type="text" id="txTag" name="txTag" class="form-control" value="<?=@$write[0]['txTag']?>" placeholder="ex) 주식,고수"></div>
							</div>
							<div class="form-group col-2">
								<div class="input-group" style="padding-top:40px;">
									<label><input type="checkbox" name='emPush' id='emPush' value='1'<?if($write[0]['emPush']=='1'){?> checked<?}?>> 푸쉬발송</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-8">
								<label>제목</label>
								<div class="input-group"><input type="text" id="vSubject" name="vSubject" class="form-control" value="<?=@$write[0]['vSubject']?>" required></div>
							</div>

							<?php if(count($category) > 0){ ?>
							<div class="form-group col-4">
								<label>카테고리</label>
								<div class="input-group">
									<select name="nCategoryNo" class="form-control">
								<?php	foreach($category as $row){ ?>
										<option <?=($write[0]['nCategoryNo'] == $row['nSeqNo']) ? 'selected' : ''; ?> value="<?=$row['nSeqNo']?>"><?=$row['vSubject']?></option>
								<?php	} ?>
									</select>
								</div>
							</div>
							<?php } ?>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<label>내용</label>
								<div class="input-group">
									<script src="/public/smarteditor2/js/service/HuskyEZCreator.js"></script>
									<script>var g5_editor_url = "/public/smarteditor2", oEditors = [], ed_nonce = "dhHcWl0suO|1593457283|d96b66cdfb538c66cc7d47f7ca87b3e0eeadacd1";</script>
									<script src="/public/smarteditor2/config.js"></script>
									<textarea id="txContent" name="txContent" class="smarteditor2 form-control" maxlength="65536" style="width:100%;height:600px"><?=@$write[0]['txContent']?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label>이미지첨부</label>
								<div class="input-group">
									<div class="input-group-prepend"><div class="input-group-text"><i class="far fa-file"></i></div></div>
									<input type="file" id="vImage" name="vImage" class="form-control" style="font-size:12px;padding:7px 0px 0px 5px;">
								</div>
							</div>
							<div class="form-group col-6">
								<div class="input-group" style="padding-top:30px;">
									<label><?if($write[0]['vImage']!=''){?><img src="/data/board/thumb/<?=$write[0]['vImage']?>" style="width:50px;height:45px;" title="<?=$write[0]['vImage']?>"> <input type="checkbox" id="vImageDel" name="vImageDel" value="<?=$write[0]['vImage']?>"> 삭제<?}?></label>
								</div>
							</div>
						</div>
					<?if($bo_table=='important'){?>
						<div class="row">
							<div class="form-group col-6">
								<label>인앱배너</label>
								<div class="input-group">
									<div class="input-group-prepend"><div class="input-group-text"><i class="far fa-file"></i></div></div>
									<input type="file" id="vImage2" name="vImage2" class="form-control" style="font-size:12px;padding:7px 0px 0px 5px;">
								</div>
							</div>
							<div class="form-group col-6">
								<div class="input-group" style="padding-top:30px;">
									<label><?if($write[0]['vImage2']!=''){?><img src="/data/board/thumb/<?=$write[0]['vImage2']?>" style="width:50px;height:45px;" title="<?=$write[0]['vImage2']?>"> <input type="checkbox" id="vImage2Del" name="vImage2Del" value="<?=$write[0]['vImage2']?>"> 삭제<?}?></label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label>추천컨텐츠</label>
								<div style="clear:both;"></div>
								<input type="text" id="vSearchHash" name="vSearchHash" class="form-control" style="width:50%;float:left;ime-mode:active;" placeholder="검색할 해시태그값 입력" value="<?=@$write[0]['vSearchHash']?>"> <a href="javascript:void(0);" onclick="searchhash()" class="btn btn-primary btn-action mr-1" style="float:left;margin-left:10px;margin-top:5px;">검색후 자동정렬</a><div style="clear:both;"></div><br>
								1순위 <select class="form-control" name="vRecom1" id="vRecom1"><option value="">컨텐츠를 선택해주세요</option>
									<?foreach($ctzlist as $row){?>
										<option value="<?=$row['nSeqNo']?>"<?if($write[0]['vRecom1']==$row['nSeqNo']) echo ' selected';?>><?=($this->util->cut_str($row['vSubject'],50))?></option>
									<?}?>
								</select><br>
								2순위 <select class="form-control" name="vRecom2" id="vRecom2"><option value="">컨텐츠를 선택해주세요</option>
									<?foreach($ctzlist as $row){?>
										<option value="<?=$row['nSeqNo']?>"<?if($write[0]['vRecom2']==$row['nSeqNo']) echo ' selected';?>><?=($this->util->cut_str($row['vSubject'],50))?></option>
									<?}?>
								</select><br>
								3순위 <select class="form-control" name="vRecom3" id="vRecom3"><option value="">컨텐츠를 선택해주세요</option>
									<?foreach($ctzlist as $row){?>
										<option value="<?=$row['nSeqNo']?>"<?if($write[0]['vRecom3']==$row['nSeqNo']) echo ' selected';?>><?=($this->util->cut_str($row['vSubject'],50))?></option>
									<?}?>
								</select><br>
								4순위 <select class="form-control" name="vRecom4" id="vRecom4"><option value="">컨텐츠를 선택해주세요</option>
									<?foreach($ctzlist as $row){?>
										<option value="<?=$row['nSeqNo']?>"<?if($write[0]['vRecom4']==$row['nSeqNo']) echo ' selected';?>><?=($this->util->cut_str($row['vSubject'],50))?></option>
									<?}?>
								</select>
							</div>
						</div>
					<?}else if($bo_table=='news'){?>
						<div class="row">
							<div class="form-group col-4">
								<label>기본종목</label>
								<div class="input-group"><input type="text" id="vBasicEvent" name="vBasicEvent" class="form-control" value="<?=@$write[0]['vBasicEvent']?>" required></div>
							</div>
							<div class="form-group col-4">
								<label>종목링크</label>
								<div class="input-group"><input type="text" id="vEventLink" name="vEventLink" class="form-control" value="<?=@$write[0]['vEventLink']?>" required></div>
							</div>
							<div class="form-group col-4">
								<label>기사링크</label>
								<div class="input-group"><input type="text" id="vNewsLink" name="vNewsLink" class="form-control" value="<?=@$write[0]['vNewsLink']?>" required></div>
							</div>
						</div>
					<?}?>
						<!--div class="form-group">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" name="agree" class="custom-control-input" id="agree">
								<label class="custom-control-label" for="agree">I agree with the terms and conditions</label>
							</div>
						</div-->
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">등록</button>
							<button type="button" onclick="go_back()" class="btn btn-light btn-block">취소</button>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	function send(f){
		var txContent_editor_data = oEditors.getById['txContent'].getIR();
		oEditors.getById['txContent'].exec('UPDATE_CONTENTS_FIELD', []);
		if(jQuery.inArray(document.getElementById('txContent').value.toLowerCase().replace(/^\s*|\s*$/g, ''), ['&nbsp;','<p>&nbsp;</p>','<p><br></p>','<div><br></div>','<p></p>','<br>','']) != -1){document.getElementById('txContent').value='';}
		if (!txContent_editor_data || jQuery.inArray(txContent_editor_data.toLowerCase(), ['&nbsp;','<p>&nbsp;</p>','<p><br></p>','<p></p>','<br>']) != -1) { alert("내용을 입력해 주십시오."); oEditors.getById['txContent'].exec('FOCUS'); return false; }
		if(f.vName.value==''){
			alert('작성자를 입력해주세요.');
			return false;
		}
		if(f.vSubject.value==''){
			alert('제목을 입력해주세요.');
			return false;
		}
		if(f.txContent.value==''){
			alert('내용을 입력해주세요.');
			return false;
		}
		return true;
	}

	function searchhash(){
		var str=$('#vSearchHash').val();
		if(str==''){
			alert('검색할 해시태그를 입력해 주십시오.');
			$('#vSearchHash').focus();
			return false;
		}
		$.post("/admin/board/searchhash", {vType:'<?=$bo_table?>',searchhash:str},function(data){
			var split_str=data.str.split(",");
			$('#vRecom1, #vRecom2, #vRecom3').val("").prop("selected",true);
			$.each(split_str,function(key,value){
				$('#vRecom'+(key+1)).val(value).prop("selected",true);
			});
		},'json');
	}

	function go_back(){
		document.frm.action="/admin/board/";
		document.frm.submit();
	}
</script>