 <style>
.auto{background-color:#5D5D5D;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
.stat_1{background-color:red;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
.stat_2{background-color:#d0d0d0;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
.stat_3{background-color:#4b3f82;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
 </style>
 <div class="main-content">
        <section class="section">
          <h1 class="section-header"><div>배너관리 및 광고관리</div></h1>
         	<div class="row">
              <div class="col-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <div class="float-right" style="width:100%;">
                      <form name="frm" id="frm" method="post" action="/admin/banner" style="width:100%;">
                        <input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo;?>">
                        <input type="hidden" name="bd_no" id="bd_no" value="">
						<input type="hidden" name="mode" id="mode" value="">
						<input type="hidden" name="udname" id="udname" value="">
						<input type="hidden" name="udval" id="udval" value="">
                        <div class="input-group" style="width:100%;">
							<select class="form-control" name="vType" id="vType" style="height:30px;padding:0px 3px;">
		                        <option value="">페이지구분</option>
		                        <option value="app_start" <?php if(@$_POST['vType']=='app_start') echo 'selected';?>>app 시작시</option>
								<option value="app_end" <?php if(@$_POST['vType']=='app_end') echo 'selected';?>>app 종료시</option>
								<option value="today" <?php if(@$_POST['vType']=='today') echo 'selected';?>>TODAY</option>
								<option value="important" <?php if(@$_POST['vType']=='important') echo 'selected';?>>핵심정보</option>
								<option value="news" <?php if(@$_POST['vType']=='news') echo 'selected';?>>뉴스</option>
								<option value="community" <?php if(@$_POST['vType']=='community') echo 'selected';?>>커뮤니티</option>
								<option value="recommand" <?php if(@$_POST['vType']=='recommand') echo 'selected';?>>종목추천</option>
								<option value="subscript" <?php if(@$_POST['vType']=='subscript') echo 'selected';?>>구독</option>
								<option value="store" <?php if(@$_POST['vType']=='store') echo 'selected';?>>스토어</option>
                      		</select>
                        	<select class="form-control" name="limit" id="limit" style="height:30px;padding:0px 3px;">
		                        <option value="">목록수</option>
		                        <option value="20" <?php if(@$_POST['limit']=='20') echo 'selected';?>>20개</option>
								<option value="50" <?php if(@$_POST['limit']=='50') echo 'selected';?>>50개</option>
                      		</select>
	                        <input type="text" name="keyword" id="keyword" class="form-control" placeholder="제목" value="<?=@$_POST['keyword'];?>">
                          <div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                        	<tr>
							  <th width="6%">번호</th>
		                      <th>배너이미지</th>
		                      <th>페이지구분</th>
		                      <th>배너종류</th>
							  <th>상태변경</th>
							  <th>제목</th>
		                      <th>클릭수</th>
		                      <th>등록일</th>
		                      <th>관리</th>
                        </tr>
                        </thead>
                        <tbody>
						<?php foreach ($list as $value) { ?>
                        <tr>
							  <td><?=$no--?></td>
							  <td><img src="/data/banner/<?=$value['vImage']?>" onerror="this.src='/asset/img/noimg_banner.png';" style="max-width:300px;"></td>
							  <td><?=$vType_arr[$value['vType']]?></td>
							  <td><?=($value['emKind']=='band'?'띠배너':'전체화면배너')?></td>
							  <td><span class="stat_<?=$value['nStat']?>" onclick="change_stat('<?=$value['nSeqNo']?>', this)"><?if($value['nStat']=='1') echo '사용'; else echo '미사용';?></span></td>
							  <td><?=$value['vSubject']?></td>
		                      <td><span class="click_<?=$value['nSeqNo']?>"><?=$value['nClick']?></span> <span class="stat_3" onclick="reset_click('<?=$value['nSeqNo']?>')">리셋</span></td>
		                      <td><?=$value['dtRegDate'];?></td>
							  <td>
								<a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit" onclick="proc_bd('Edit','<?=$value['nSeqNo']?>','<?=@$_POST['vType']?>')"><i class="ion ion-edit"></i></a>
								<a class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete" onclick="proc_bd('Del','<?=$value['nSeqNo']?>','<?=@$_POST['vType']?>')"><i class="ion ion-trash-b"></i></a>
							  </td>
                        </tr>
						<?php } #foreach ?>
                      </tbody>
                    </table>
                    </div>
                    <div>
						<div style="clear:both;"></div>
						<div style="float:right;"><a href="javascript:void(0);" class="btn btn-primary" onclick="proc_bd('Edit','','')">생성</a></div>
						<div style="clear:both;"></div>
					</div>
                    <?=$paging?>
                  </div>
                </div>
              </div>
            </div>
	</section>
</div>

<script type="text/javascript">
  function pageRemote(pageNo){
    $('#pageNo').val(pageNo);
    document.frm.submit();
  }
  function proc_bd(mode,no,prod){
	if(mode=='Del'){
		if(confirm("정말로 삭제하시겠습니까?")){
			$('#mode').val(mode);
			$('#bd_no').val(no);
			$('#vType').val(prod).prop('selected',true);
			document.frm.action='/admin/banner/banner'+mode;
			document.frm.submit();
		}
	}else{
		$('#mode').val(mode);
		$('#bd_no').val(no);
		$('#vType').val(prod).prop('selected',true);
		document.frm.action='/admin/banner/banner'+mode;
		document.frm.submit();
	}
  }

  function change_stat(no,target){
	  var now_txt=$(target).html();
	  if(now_txt=='사용'){
		  var new_txt='미사용';
		  var new_stat='2';
		  var stat='1';
	  }else if(now_txt=='미사용'){
		  var new_txt='사용';
		  var new_stat='1';
		  var stat='2';
	  }
	  if(confirm("'"+now_txt+"'상태에서 '"+new_txt+"'상태로 변경하시겠습니까?")){
		$.post("/admin/banner/updatefield",{no:no,field:'nStat',fval:new_stat},function(data){
			$(target).html(new_txt).removeClass('stat_'+stat).addClass('stat_'+new_stat);
		},'json');
	  }
  }

  function reset_click(no){
	  if(confirm("해당 배너의 클릭수를 리셋하시겠습니까?")){
		$.post("/admin/banner/updatefield",{no:no,field:'nClick',fval:'0'},function(data){
			$('.click_'+no).html('0');
		},'json');
	  }
  }
</script>