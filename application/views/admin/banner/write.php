<script src="/public/admin/modules/jquery.min.js"></script>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>배너관리 및 광고관리</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-body">
					<form name="frm" id="frm" method="post" action="/admin/banner/bannerInsert" onsubmit="return send(this)" enctype="multipart/form-data">
						<input type="hidden" name="bd_no" id="bd_no" value="<?=@$write[0]['nSeqNo']?>">
						<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>">
						<input type="hidden" name="vImage_ori" id="vImage_ori" value="<?=@$write[0]['vImage']?>">
						<div class="row">
							<div class="form-group col-6">
								<label>페이지구분</label>
								<div class="input-group"><select id="vType" name="vType" class="form-control">
										<option value="app_start" <?php if($write[0]['vType']=='app_start') echo 'selected';?>>app 시작시</option>
										<option value="app_end" <?php if($write[0]['vType']=='app_end') echo 'selected';?>>app 종료시</option>
										<option value="today" <?php if($write[0]['vType']=='today') echo 'selected';?>>TODAY</option>
										<option value="important" <?php if($write[0]['vType']=='important') echo 'selected';?>>핵심정보</option>
										<option value="news" <?php if($write[0]['vType']=='news') echo 'selected';?>>뉴스</option>
										<option value="community" <?php if($write[0]['vType']=='community') echo 'selected';?>>커뮤니티</option>
										<option value="recommand" <?php if($write[0]['vType']=='recommand') echo 'selected';?>>종목추천</option>
										<option value="subscript" <?php if($write[0]['vType']=='subscript') echo 'selected';?>>구독</option>
										<option value="store" <?php if($write[0]['vType']=='store') echo 'selected';?>>스토어</option>
										<option value="login" <?php if($write[0]['vType']=='login') echo 'selected';?>>인증/로그인시</option>
									</select>
								</div>
							</div>
							<div class="form-group col-6">
								<label>배너종류</label>
								<div class="input-group"><select id="emKind" name="emKind" class="form-control">
										<option value="band" <?php if($write[0]['emKind']=='band') echo 'selected';?>>띠배너</option>
										<option value="wide" <?php if($write[0]['emKind']=='wide') echo 'selected';?>>전체화면배너</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<label>제목</label>
								<div class="input-group"><input type="text" id="vSubject" name="vSubject" class="form-control" value="<?=@$write[0]['vSubject']?>" required></div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label>이미지</label>
								<div class="input-group">
									<div class="input-group-prepend"><div class="input-group-text"><i class="far fa-file"></i></div></div>
									<input type="file" id="vImage" name="vImage" class="form-control" style="font-size:12px;padding:7px 0px 0px 5px;">
								</div>
							</div>
							<div class="form-group col-6">
								<div class="input-group" style="padding-top:30px;">
									<label><?if($write[0]['vImage']!=''){?><img src="/data/banner/<?=$write[0]['vImage']?>" style="width:50px;height:45px;" title="<?=$write[0]['vImage']?>"> <input type="checkbox" id="vImageDel" name="vImageDel" value="<?=$write[0]['vImage']?>"> 삭제<?}?></label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<label>링크</label>
								<div class="input-group">
									<input type="text" id="vLink" name="vLink" class="form-control" style="font-size:12px;padding:7px 0px 0px 5px;" value="<?=$write[0]['vLink']?>">
								</div>
							</div>
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">등록</button>
							<button type="button" onclick="go_back()" class="btn btn-light btn-block">취소</button>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	function send(f){
		if(f.vSubject.value==''){
			alert('제목을 입력해주세요.');
			return false;
		}
		if(f.vImage.value=='' && f.vImage_ori.value==''){
			alert('이미지를 선택해주세요.');
			return false;
		}
		return true;
	}

	function go_back(){
		document.frm.action="/admin/banner/";
		document.frm.submit();
	}
</script>