<style>
.auto{background-color:#5D5D5D;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
.category{cursor:pointer;}
.category-off{background-color:#aaa}
.category-on{background-color:#574B90}
.table tbody tr td{vertical-align:middle;}
</style>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>상단 게시물 관리</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-header">
						<div class="float-right" style="width:100%;">
	<form name="frm" id="frm" method="post" action="/admin/topboard" style="width:100%;">
		<input type="hidden" name="categoryNo" value="<?=$categoryNo?>">
							<div class="input-group" style="width:100%;">
							
								<select class="form-control" name="vType" id="vType" style="height:30px;padding:0px 3px;" >
									<option value="today"		<?php if(@$_POST['vType']=='today') echo 'selected';?>>공개정보 - TODAY</option>
									<option value="important"	<?php if(@$_POST['vType']=='important') echo 'selected';?>>공개정보 - 추천종목</option>
									<option value="invest"		<?php if(@$_POST['vType']=='invest') echo 'selected';?>>공개정보 - 투자정보</option>
									<option value="study"		<?php if(@$_POST['vType']=='study') echo 'selected';?>>공개정보 - 주식공부</option>
									<option value="stock"		<?php if(@$_POST['vType']=='stock') echo 'selected';?>>종목추천 - 전체보기</option>
								</select>

								<div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
							</div>
					<?php if(count($category) > 0){?>
							<div class="input-group" style="width:100%; padding-top:10px;">
								<a class=" btn-action   <?=($categoryNo == 0) ? "category-on" : "category-off"; ?> category" categoryNo="0" title="">전체</a>
							<?php foreach($category as $row){?>
								<a class=" btn-action <?=($categoryNo == $row['nSeqNo']) ? "category-on" : "category-off"; ?> category" categoryNo="<?=$row['nSeqNo']?>"  title=""><?=$row['vSubject']?></a>
							<?php }?>
							</div>
					<?php }?>
	</form>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
	<form name="insert" id="insert" method="post" action="/admin/topboard/postTopBoard">
							<table class="table table-hover">
									<thead>
										<tr>
											<th width="6%">노출순서</th>
											<th width="10%">게시판명</th>
											<th>선택게시물</th>
											<th width="10%">
												<span class="auto">최신순 자동선택</span> 
											</th>
										</tr>
									</thead>
									<tbody>
		<?php for($i=0; $i< count($listArr); $i++){ ?>

									<?php
									foreach($list as $row){
										if($row['vType'] != $listArr[$i]){
											continue;
										}
										?>
										<tr id="<?=$row['vType']?>_<?=$row['nSort']?>">
											<td><?=$row['nSort']?></td>
											<td><?=$boardName[$row['vType']]?></td>
											<td colspan="2">
												<select name="nBoardNo[]" class="form-control">
													<option value="">사용안함</option>
												<?php foreach($cont[$listArr[$i]] as $subRow){?>
													<option <?=($row['nBoardNo'] == $subRow['nSeqNo']) ? 'selected' : ''; ?> value="<?=$subRow['nSeqNo']?>"><?=$subRow['vSubject']?></option>
												<?php }?>
												</select>
												<input type="hidden" name="vType[]" value="<?=$row['vType']?>">
												<input type="hidden" name="nSort[]" value="<?=$row['nSort']?>">
												<input type="hidden" name="mainNo[]" value="<?=$mainNo[$i]?>">
												<input type="hidden" name="categoryNo[]" value="<?=$categoryNo?>">
											</td>
										</tr>
									<?php } ?>
		<?php }?>
									</tbody>
								</table>

								<input type="submit" class="btn btn-primary" value="저장" style="width:100%;">
		</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script src="/asset/js/lib/jquery-3.4.1.min.js"></script>
<script type="text/javascript">

function auto_select(no){
	$.post("/admin/board/popautolist",{mainval:no},function(data){
		for(i=1;i<=5;i++){
			if(data.cont[i].vType!=''){
				$('#vType_select_'+no+'_'+i).val(data.cont[i].vType).prop("selected",true);
			}
			if(data.cont[i].bdno!=''){
				sstr='<option value="">컨텐츠를 선택해주십시오.</option>';
				$.each(data.cont[i].vTypeList, function(idx,item){
					sstr+='<option value="'+item['nSeqNo']+'">'+item['vSubject']+'</option>';
				});
				$('#bdno_'+no+'_'+i).empty().append(sstr);
				$('#bdno_'+no+'_'+i).val(data.cont[i].bdno).prop("selected",true);
			}
		}
	},'json');
}

$(document).ready(function(){

	// 카테고리 클릭 submit
	$(".category").off('click').on("click", function(){
		var categoryNo = $(this).attr("categoryNo");
		$("#frm > input[name=categoryNo]").val(categoryNo);
		$("#frm").submit();
	});


	// 상단 Select 자동 Submit
	$("select[name=vType]").off("change").on("change", function(){
		$("#frm > input[name=categoryNo]").val('');
		$("#frm").submit();
	});


	// 최신순 자동선택
	$(".auto").off("click").on("click", function(){
		var data = {
			vType : $("select[name=vType]").val(),
			categoryNo : $("input[name=categoryNo]").val()
		}

		$.post("/admin/topboard/getAutoTopList", data ,function(response){

			$.each(response, function(key, value){

				$("#" + key).find("select[name^=nBoardNo]").val(value);


			});


			console.log(response)

		}, 'json');
	});

});
</script>