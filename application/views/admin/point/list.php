 <style>
.auto{background-color:#5D5D5D;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
.stat_1{background-color:red;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
.stat_2{background-color:blue;border-radius:3px;padding:3px 6px;color:#fff;cursor:pointer;}
 </style>
 <div class="main-content">
        <section class="section">
          <h1 class="section-header"><div>고객별 컨텐츠 공유 확인</div></h1>
         	<div class="row">
              <div class="col-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <div class="float-right" style="width:100%;">
                      <form name="frm" id="frm" method="post" action="/admin/point" style="width:100%;">
                        <input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo;?>">
                        <input type="hidden" name="point_setting" id="point_setting">
                        <div class="input-group" style="width:100%;">
							<select class="form-control" name="vType" id="vType" style="height:30px;padding:0px 3px;">
		                        <option value="">검색할 유형을 선택해주십시오.</option>
		                        <option value="all"<?php if(@$_POST['vType']=='all') echo ' selected';?>>전체</option>
		                        <option value="charge"<?php if(@$_POST['vType']=='charge') echo ' selected';?>>구독회원</option>
		                        <option value="free"<?php if(@$_POST['vType']=='free') echo ' selected';?>>무료회원</option>
                      		</select>
                        	<select class="form-control" name="limit" id="limit" style="height:30px;padding:0px 3px;">
		                        <option value="">목록수</option>
		                        <option value="20" <?php if(@$_POST['limit']=='20') echo 'selected';?>>20개</option>
								<option value="50" <?php if(@$_POST['limit']=='50') echo 'selected';?>>50개</option>
                      		</select>
							<input type="text" name="reg1" id="reg1" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@$_POST['reg1'];?>" placeholder="공유일 시작"> ~ 
							<input type="text" name="reg2" id="reg2" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@$_POST['reg2'];?>" placeholder="공유일 종료">
	                        <input type="text" name="keyword" id="keyword" class="form-control" placeholder="이름 or 전화번호" value="<?=@$_POST['keyword'];?>">
                          <div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                        	<tr>
							  <th width="6%">번호</th>
		                      <th>고객명</th>
		                      <th>전화번호</th>
		                      <th>페이스북</th>
		                      <th>카카오톡</th>
							  <th>문자</th>
							  <th>종합</th>
		                      <th>포인트증정</th>
                        </tr>
                        </thead>
                        <tbody>
						<?php foreach ($list as $value) { ?>
                        <tr>
							  <td><?=$no--?></td>
		                      <td><?=$value['vName']?></td>
							  <td>보안상 미기재</td>
		                      <td><?=$value['nFb']?></td>
		                      <td><?=$value['nKakao']?></td>
		                      <td><?=$value['nSms']?></td>
		                      <td><?=($value['nFb']+$value['nKakao']+$value['nSms'])?></td>
							  <td><a class="btn btn-danger btn-action modal-go" data-toggle="tooltip" title="" data-original-title="Delete" onclick="set_modal('<?=$value['nSeqNo']?>')">포인트증정</a></td>
                        </tr>
						<?php } #foreach ?>
                      </tbody>
                    </table>
                    </div>
                    <?=$paging?>
                  </div>
                </div>
              </div>
            </div>
	</section>
</div>

<script type="text/javascript">
	function pageRemote(pageNo){
		$('#pageNo').val(pageNo);
		document.frm.submit();
	}

	function set_modal(no){
		$('.close').remove();
		//$('#fire-modal-1').remove();
		$('.modal-body').empty();
		var tit='증정/삭감 포인트를 선택해 주십시오.';
		var str='<div style="padding-top:10px;text-align:center;"><input type="text" name="point" class="putpoint form-control" placeholder="숫자만 입력"></div><div style="padding-top:10px;text-align:center;"><input type="text" name="vdesc" class="vdesc form-control" placeholder="내용을 입력해주세요." value=""></div><div style="padding:20px 0px 10px;text-align:center;"><a href="javascript:void(0);" class="btn btn-primary modal-go" onclick="savePoint('+no+')">확인</a></div>';
		$('.modal-body').append(str);
		$('.modal-header').empty().append('<h5 class="modal-title">'+tit+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color:#fff;"> x </button>');
	}

	function savePoint(no){
		var arr = $(".putpoint").length;
		var arr2 = $(".vdesc").length;
		var point=0;
		var msg='';
		for(var i=0; i<arr; i++){
			if($(".putpoint").eq(i).val()!=''){
				point+=parseInt($(".putpoint").eq(i).val());
			}
			if($(".vdesc").eq(i).val()!=''){
				msg=$(".vdesc").eq(i).val();
			}
		}
		if(msg!=''){
			if(confirm("포인트를 등록하시겠습니까?")){
				$.post("/admin/point/savePoint",{no:no,point:point,msg:msg},function(data){
					if(data.code=='1'){
						alert('정상적으로 등록되었습니다.');
						location.href="/admin/point";
					}else{
						alert("오류! - 잠시후 다시 시도해 주십시오.");
						return false;
					}
				},'json');
			}
		}
	}
</script>