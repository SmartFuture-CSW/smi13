<script src="/public/admin/modules/jquery.min.js"></script>
<script src="/public/smarteditor2/js/service/HuskyEZCreator.js"></script>
<script>var g5_editor_url = "/public/smarteditor2", oEditors = [], ed_nonce = "dhHcWl0suO|1593457283|d96b66cdfb538c66cc7d47f7ca87b3e0eeadacd1";</script>
<script src="/public/smarteditor2/config.js"></script>

<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>종목추천</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-body">
					<form name="frm" id="frm" method="post" action="/admin/stock/stockInsert" onsubmit="return send(this)" enctype="multipart/form-data">
						<input type="hidden" name="bo_table" id="bo_table" value="<?=$bo_table?>">
						<input type="hidden" name="vType" id="vType" value="<?=$bo_table?>">
						<input type="hidden" name="bd_no" id="bd_no" value="<?=@$write[0]['nSeqNo']?>">
						<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>">
						<input type="hidden" name="vImage_ori" id="vImage_ori" value="<?=@$write[0]['vImage']?>">
						<input type="hidden" name="vImage2_ori" id="vImage2_ori" value="<?=@$write[0]['vImage2']?>">
						<div class="form-divider"> <?=$bdname[$bo_table]?> 생성</div>
						<div class="row">
							<div class="form-group col-3">
								<label>작성자</label>
								<div class="input-group"><input type="text" id="vName" name="vName" class="form-control" value="<?=@$write[0]['vName']?>" required></div>
							</div>
							<div class="form-group col-4">
								<label>해시태그</label>
								<div class="input-group"><input type="text" id="txTag" name="txTag" class="form-control" value="<?=@$write[0]['txTag']?>" placeholder="ex) 주식,고수"></div>
							</div>
							<div class="form-group col-4">
								<label>판매가격</label>
								<div class="input-group"><input type="tel" id="nPrice" name="nPrice" class="form-control" value="<?=@$write[0]['nPrice']?>" placeholder="숫자만 입력"></div>
							</div>
							<div class="form-group col-1">
								<div class="input-group" style="padding-top:40px;">
									<label><input type="checkbox" name='emPush' id='emPush' value='1'<?if($write[0]['emPush']=='1'){?> checked<?}?>> 푸쉬발송</label>
								</div>
							</div>
						</div>
					<?if($bo_table!='rumor'){?>
						<div class="row">
							<div class="form-group col-4">
								<label>종목명</label>
								<div class="input-group"><input type="text" id="vStockName" name="vStockName" class="form-control" value="<?=@$write[0]['vStockName']?>" required></div>
							</div>
							<div class="form-group col-4">
								<label>목표일</label>
								<div class="input-group"><input type="tel" id="nAimDate" name="nAimDate" class="form-control" value="<?=@$write[0]['nAimDate']?>" placeholder="숫자만 입력" required></div>
							</div>
							<div class="form-group col-4">
								<label>목표수익률</label>
								<div class="input-group"><input type="tel" id="nAimPercent" name="nAimPercent" class="form-control" value="<?=@$write[0]['nAimPercent']?>" placeholder="숫자만 입력" required></div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-4">
								<label>매수가</label>
								<div class="input-group"><input type="text" id="nBuyPrice1" name="nBuyPrice1" class="form-control caculAim" value="<?=@$write[0]['nBuyPrice1']?>" placeholder="1차, 숫자만 입력" required></div>
								<div class="input-group"><input type="text" id="nBuyPrice2" name="nBuyPrice2" class="form-control" value="<?=@$write[0]['nBuyPrice2']?>" placeholder="2차, 숫자만 입력" required></div>
							</div>
							<div class="form-group col-4">
								<label>목표가</label>
								<div class="input-group"><input type="text" id="nAimPrice" name="nAimPrice" class="form-control caculAim" value="<?=@$write[0]['nAimPrice']?>" placeholder="숫자만 입력" required></div>
							</div>
							<div class="form-group col-4">
								<label>손절가</label>
								<div class="input-group"><input type="text" id="nLossPrice" name="nLossPrice" class="form-control" value="<?=@$write[0]['nLossPrice']?>" placeholder="숫자만 입력" required></div>
							</div>
						</div>
					<?}?>
						<div class="row">
							<div class="form-group col-8">
								<label>제목</label>
								<div class="input-group"><input type="text" id="vSubject" name="vSubject" class="form-control" value="<?=@$write[0]['vSubject']?>" required></div>
							</div>
							<div class="form-group col-4">
								<label>시장주목도</label>
								<div class="input-group"><input type="tel" id="nMarketAtt" name="nMarketAtt" class="form-control" value="<?=@$write[0]['nMarketAtt']?>" placeholder="숫자만 입력" required></div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<label>구매전 내용</label>
								<div class="input-group">
									<textarea id="txContentBefore" name="txContentBefore" class="smarteditor2 form-control" maxlength="65536" style="width:100%;height:600px"><?=@$write[0]['txContentBefore']?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<label>구매후 내용</label>
								<div class="input-group">
									<textarea id="txContent" name="txContent" class="smarteditor2 form-control" maxlength="65536" style="width:100%;height:600px"><?=@$write[0]['txContent']?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<label>추천사유</label>
								<div class="input-group">
									<textarea id="txRecomReason" name="txRecomReason" class="form-control" maxlength="65536" style="width:100%;height:60px"><?=@$write[0]['txRecomReason']?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label><?if($bo_table!='rumor'){?>종목그래프<?}else{?>찌라시이미지<?}?></label>
								<div class="input-group">
									<div class="input-group-prepend"><div class="input-group-text"><i class="far fa-file"></i></div></div>
									<input type="file" id="vImage" name="vImage" class="form-control" style="font-size:12px;padding:7px 0px 0px 5px;">
								</div>
							</div>
							<div class="form-group col-6">
								<div class="input-group" style="padding-top:30px;">
									<label><?if($write[0]['vImage']!=''){?><img src="/data/stock/thumb/<?=$write[0]['vImage']?>" style="width:50px;height:45px;" title="<?=$write[0]['vImage']?>"> <input type="checkbox" id="vImageDel" name="vImageDel" value="<?=$write[0]['vImage']?>"> 삭제<?}?></label>
								</div>
							</div>
						</div>
					<?if($bo_table!='rumor'){?>
						<div class="row">
							<div class="form-group col-6">
								<label>배너</label>
								<div class="input-group">
									<div class="input-group-prepend"><div class="input-group-text"><i class="far fa-file"></i></div></div>
									<input type="file" id="vImage2" name="vImage2" class="form-control" style="font-size:12px;padding:7px 0px 0px 5px;">
								</div>
							</div>
							<div class="form-group col-6">
								<div class="input-group" style="padding-top:30px;">
									<label><?if($write[0]['vImage2']!=''){?><img src="/data/stock/thumb/<?=$write[0]['vImage2']?>" style="width:50px;height:45px;" title="<?=$write[0]['vImage2']?>"> <input type="checkbox" id="vImage2Del" name="vImage2Del" value="<?=$write[0]['vImage2']?>"> 삭제<?}?></label>
								</div>
							</div>
						</div>
					<?}?>
<?php if($bo_table == "news") { ?>
						<div class="row">
						<?php	/*
							<div class="form-group col-4">
								<label>기본종목</label>
								<div class="input-group"><input type="text" id="vBasicEvent" name="vBasicEvent" class="form-control" value="<?=@$write[0]['vBasicEvent']?>" required></div>
							</div>
							<div class="form-group col-4">
								<label>종목링크</label>
								<div class="input-group"><input type="text" id="vEventLink" name="vEventLink" class="form-control" value="<?=@$write[0]['vEventLink']?>" required></div>
							</div>
						*/?>
							<div class="form-group col-12">
								<label>기사링크</label>
								<div class="input-group"><input type="text" id="vNewsLink" name="vNewsLink" class="form-control" value="<?=@$write[0]['vNewsLink']?>" required></div>
							</div>
						</div>
<?php }?>
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">등록</button>
							<button type="button" onclick="go_back()" class="btn btn-light btn-block">취소</button>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	function send(f){
		var txContent_editor_data = oEditors.getById['txContent'].getIR();
		var txContentBefore_editor_data = oEditors.getById['txContentBefore'].getIR();
		oEditors.getById['txContent'].exec('UPDATE_CONTENTS_FIELD', []);
		oEditors.getById['txContentBefore'].exec('UPDATE_CONTENTS_FIELD', []);

		if(jQuery.inArray(document.getElementById('txContent').value.toLowerCase().replace(/^\s*|\s*$/g, ''), ['&nbsp;','<p>&nbsp;</p>','<p><br></p>','<div><br></div>','<p></p>','<br>','']) != -1) {
			document.getElementById('txContent').value='';
		}

		if (!txContentBefore_editor_data || jQuery.inArray(txContentBefore_editor_data.toLowerCase(), ['&nbsp;','<p>&nbsp;</p>','<p><br></p>','<p></p>','<br>']) != -1) {
			alert("구매전 글내용 입력해 주십시오."); 
			oEditors.getById['txContentBefore'].exec('FOCUS');
			return false;
		}

		if (!txContent_editor_data || jQuery.inArray(txContent_editor_data.toLowerCase(), ['&nbsp;','<p>&nbsp;</p>','<p><br></p>','<p></p>','<br>']) != -1) {
			alert("구매후 글내용 입력해 주십시오."); 
			oEditors.getById['txContent'].exec('FOCUS');
			return false;
		}


		if(f.vName.value==''){
			alert('작성자를 입력해주세요.');
			return false;
		}
		if(f.vSubject.value==''){
			alert('제목을 입력해주세요.');
			return false;
		}
		if(f.txContentBefore.value==''){
			alert("구매전 글내용 입력해 주십시오."); 
			return false;
		}
		if(f.txContent.value==''){
			alert("구매후 글내용 입력해 주십시오."); 
			return false;
		}
		return true;
	}

	function go_back(){
		document.frm.action="/admin/stock/";
		document.frm.submit();
	}


$(function(){
	$(".caculAim").on('keyup', function(){
		var buy1 = ($("#nBuyPrice1").val() != "") ? $("#nBuyPrice1").val() : 0;
		var buy2 = ($("#nBuyPrice2").val() != "") ? $("#nBuyPrice2").val() : 0;
		var buy = (parseInt(buy1,10) + parseInt(buy2,10))/2;

		var sell = ($("#nAimPrice").val() != "") ? $("#nAimPrice").val() : 0;
		sell = parseInt(sell,10)
		var aim = Math.round((sell/buy -1) * 100);
		$("#nAimPercent").val(aim);
	});
});

</script>