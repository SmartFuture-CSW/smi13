<style>
.modal-dialog{max-width:800px;}
	.modal-table{border-spacing:2px; background-color:#eee; border:1px solid #eee;}
	.modal-table tbody tr th{ font-size:12px; background-color:#eee; padding-left:10px;}
	.modal-table tbody tr td{ font-size:12px; background-color:#fff;}
</style>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>회원관리</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-header">
						<div class="float-right" style="width:100%;">
	<form name="frm" id="frm" method="post" action="/admin/user" style="width:100%;">
		<input type="hidden" name="pageNo" id="pageNo" value="1<?//=$pageNo;?>">
		<input type="hidden" name="pageNoEx" id="pageNoEx" value="<?=$pageNo;?>">
		<input type="hidden" name="ex" id="ex" value="">
							<div class="input-group" style="width:100%;">
								<span style="padding:0px;background-color:green;color:#fff;width:50px;border:0px;cursor:pointer;text-align:center;" onclick="ex()">엑셀</span>
								<select class="form-control" name="limit" id="limit" style="height:30px;padding:0px 3px;">
									<option value="">목록수</option>
									<option value="20" <?php if(@$_POST['limit']=='20') echo 'selected';?>>20개</option>
									<option value="50" <?php if(@$_POST['limit']=='50') echo 'selected';?>>50개</option>
								</select>
								<select class="form-control" name="mgroup" id="mgroup" style="height:30px;padding:0px 3px;">
									<option value="">그룹구분</option>
									<option value="1" <?php if(@$_POST['mgroup']=='1') echo 'selected';?>>구독그룹</option>
									<option value="2" <?php if(@$_POST['mgroup']=='2') echo 'selected';?>>무료그룹</option>
								</select>
								<select class="form-control" name="auth" id="auth" style="height:30px;padding:0px 3px;">
									<option value="">최초인증구분</option>
									<option value="app" <?php if(@$_POST['auth']=='app') echo 'selected';?>>APP</option>
									<option value="web" <?php if(@$_POST['auth']=='web') echo 'selected';?>>WEB</option>
								</select>
								<select class="form-control" name="nlevel" id="nlevel" style="height:30px;padding:0px 3px;">
									<option value="">회원구분</option>
									<option value="1" <?php if(@$_POST['nlevel']=='1') echo 'selected';?>>일반</option>
									<option value="2" <?php if(@$_POST['nlevel']=='2') echo 'selected';?>>전문가</option>
									<option value="3" <?php if(@$_POST['nlevel']=='3') echo 'selected';?>>업체회원</option>
								</select>
								<select class="form-control" name="reception" id="reception" style="height:30px;padding:0px 3px;">
									<option value="">수신여부</option>
									<option value="1" <?php if(@$_POST['reception']=='1') echo 'selected';?>>수신동의</option>
									<option value="2" <?php if(@$_POST['reception']=='2') echo 'selected';?>>수신거부</option>
								</select>
								<input type="text" name="reg1" id="reg1" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@$_POST['reg1'];?>" placeholder="가입일 시작"> ~ 
								<input type="text" name="reg2" id="reg2" class="form-control" style="padding:0px 3px;width:60px;" value="<?=@$_POST['reg2'];?>" placeholder="가입일 종료">
								<select name="keyfield" class="form-control"  style="height:30px;padding:0px 3px; width:30px;">
									<option value="vName" <?php if(@$_POST['keyfield'] == 'vName') echo 'selected'; ?>>이름</option>
									<option value="vPhone" <?php if(@$_POST['keyfield'] == 'vPhone') echo 'selected'; ?>>연락처</option>
									<option value="vNick" <?php if(@$_POST['keyfield'] == 'vNick') echo 'selected'; ?>>닉네임</option>
								</select>
								<input type="text" name="keyword" id="keyword" class="form-control" placeholder="검색어" value="<?=@$_POST['keyword'];?>">
								<div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
							</div>
	</form>
							<div style="width:100%; text-align:right; padding:10px 0px 0px;">
								<a href="javascript:void(0);" onclick="set_modal('create')" class="btn btn-primary modal-go" style="float:right;">업체회원 생성</a>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th class="text-center">
											<div class="custom-checkbox custom-control">
												<input type="checkbox" class="custom-control-input" id="checkbox-all">
												<label for="checkbox-all" class="custom-control-label"></label>
											</div>
										</th>
										<th width="6%">번호</th>
										<th>이름</th>
										<th>아이디</th>
										<th>접속정보</th>
										<th>가입일</th>
										<th>그룹</th>
										<th>구분</th>
										<th>수신</th>
										<!-- <th>Edit</th> -->
									</tr>
								</thead>
								<tbody>
					<?php foreach ($list as $value) { ?>
									<tr>
										<td width="40">
											<div class="custom-checkbox custom-control">
												<input type="checkbox" id="checkbox<?=$value['nSeqNo'];?>" name="chkchk" class="custom-control-input" value="<?=$value['nSeqNo'];?>" >
												<label for="checkbox<?=$value['nSeqNo'];?>" class="custom-control-label"></label>
											</div>
										</td>
										<td><?=$no--?></td>
										<td title="<?=$value['nSeqNo']?> | <?=$value['vPhone']?>"><a href="javascript:void(0);" class="modal-go" onclick="set_modal('member', '<?=$value['vName'];?>','<?=$value['mgroup']?>','<?=number_format($value['nFreePoint'])?>','<?=$value['nSeqNo']?>', '<?=number_format($value['nPayPoint'])?>')"><u><?=$value['vName'];?></u></a></td>
										<td><?=$value['UserId']?></td>
										<td><?=$value['vDevice'];?></td>
										<td><?=$value['dtRegDate'];?></td>
										<td><?=($value['mgroup']>0?'<span style="color:red;">구독':'<span>무료');?>그룹</span></td>
										<td>
											<select onchange="change_level('<?=$value['nSeqNo']?>',this.value)" class="form-control" style="width:90px;padding:0px;">
											<option value="1"<?if($value['nLevel']==1) echo ' selected';?>>일반</option>
											<option value="2"<?if($value['nLevel']==2) echo ' selected';?>>전문가</option>
											<option value="3"<?if($value['nLevel']==3) echo ' selected';?>>업체회원</option>
										</select></td>
										<td><?=($value['vPushOs']=='1'?'<span style="color:red;">동의</span>':'거부');?></td>
<!--td>
<a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ion ion-edit"></i></a>
<a class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion ion-trash-b"></i></a>
</td--> 
									</tr>
					<?php } #foreach 
						if(count($list)==0){?>
									<tr>
										<td colspan="9" align='center' valign='middle' height='120'><br><br>No Data.</td>
									</tr>
					<?php }?>
								</tbody>
							</table>
						</div>
						<div><a href="javascript:void(0);" class="btn btn-danger btn-action" onclick="go_del()"><i class="ion ion-trash-b"></i> 선택삭제</a></div>
						<?=$paging?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	function pageRemote(pageNo){
		$('#pageNo').val(pageNo);
		document.frm.submit();
	}

	function ex(){
		$('#ex').val('ex');
		document.frm.submit();
		$('#ex').val('');
	}

	function set_modal(cate, a = null,b = null,c = null,d = null, e=null){
		$('.modal-body').empty();
		if(cate == 'member'){// 회원 히스토리 클릭
			$('.modal-header').empty().append('<h5 class="modal-title">회원상세정보</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color:#fff;"> X </button>');
			$('.modal-body').append('<div style="padding-bottom:5px;">고객명 : <span class="popuname"></span>&nbsp;&nbsp;|&nbsp;&nbsp;고객구분 : <span class="popugroup"></span></div>');
			$('.modal-body').append('<div class="poptb"></div>');
			$('.modal-body').append('<div style="padding-bottom:5px; padding-top:15px; color:red;">포인트 : <span class="payPoint"></span>, &nbsp;&nbsp;<span style="color:brown;">BP : <span class="freePoint"></span></span></div>');
			$('.modal-body').append('<div class="postp"></div>');
			$('.popuname').html(a);
			if(b=='0') bb='무료그룹'; else bb='유료그룹';
			$('.popugroup').html(bb);
			//$('.popupoint').html(c+"포인트");

			$(".freePoint").html(c+'P');
			$(".payPoint").html(e+'P');

			$.post("/admin/user/poptb",{no:d},function(data){
				console.log(data);
				$('.poptb').html(data.cont)
				$('.postp').html(data.pLog)
			},'json');


		}else if(cate == 'create'){// 업체 회원 생성
			var str = "<form name='frm'>";
			str += '<table width="100%" class="modal-table"><tbody>';
			str += '<tr><th> 이름</th><td><input type="text" name="createName" class="form-control" placeholder="이름 또는 담당자명"></td></tr>';
			str += '<tr><th> 회사명(닉네임)</th><td><input type="text" name="createNick" class="form-control" placeholder="회사명 또는 닉네임"></td></tr>';
			str += '<tr><th> 전화번호(아이디)</th><td><input type="text" name="createPhone" class="form-control" placeholder="-를 제외한 숫자만 입력 해주세요" maxlength="11" autocomplete="no"></td></tr>';
			str += '<tr><th> 비밀번호</th><td><input type="password" name="createPwd" class="form-control" placeholder="" autocomplete="no"></td></tr>';
			str += '</tbody></table>';
			str += '<div style="width:100%; text-align:center; padding:10px 0px 0px;"><button type="button" onclick="crete_member(this.form)" class="btn btn-primary" >생성</div></form>';
			$('.modal-body').append(str);
			$('.modal-header').empty().append('<h5 class="modal-title">업체회원 생성</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color:#fff;"> x </button>');
		}
		$('.close').remove();
	}

	function go_del(){
		var ddata='';
		$('input:checkbox[name=chkchk]').each(function() {
			if($(this).is(':checked'))
			ddata += ($(this).val())+",";
		});
		if(ddata==''){
			alert("삭제할 회원을 선택해주세요.");
			return false;
		}


		if(confirm("삭제를 진행하시겠습니까?")){
			$.post("/admin/user/delmem",{delno:ddata},function(data){alert('삭제완료!');document.location.href='/admin/user';},'json');
		}
	}

	function change_level(no,levelval){
		$.post("/admin/user/changelevel",{no:no,levelval:levelval},function(data){;},'json');
	}

	function crete_member(frm){
		var data = {
			'vName' : frm.createName.value
			, 'vNick' : frm.createNick.value
			, 'vPhone' : frm.createPhone.value
			, 'vPwd' : frm.createPwd.value
		}
		var u = "/admin/user/postMember";
		$.ajax({
			type : "post",
			url : u,
			dataType : 'json',
			data : data,
			success : function(response) {
				if(response.status == "SUCCESS"){
					alert("등록완료");
					window.location.reload();
				}
			},
			error : function(response){
				console.log(response)
			}
		});
	}
</script>