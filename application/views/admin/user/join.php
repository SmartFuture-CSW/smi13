
<div class="main-content">
  <section class="section">
    <h1 class="section-header"><div>회원관리</div></h1>
<div class="row">
  <div class="col-6">
    <div class="card card-primary">
    <div class="card-body">
      <form method="post" action="/admin/user/setUserJoin">
        <div class="form-divider">
          User Info *
        </div>
        <div class="row">
          <div class="form-group col-6">
            <label>User ID</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fas fa-user"></i>
                </div>
              </div>
              <input type="text" id="userid" name="userid" class="form-control">
            </div>
          </div>
          <div class="form-group col-6">
            <label>Name</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fas fa-user"></i>
                </div>
              </div>
              <input type="text" id="username" name="username" class="form-control">
            </div>
          </div>
        </div>

         <div class="row">
          <div class="form-group col-6">
            <label>Password</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fas fa-lock"></i>
                </div>
              </div>
              <input type="password" id="passwd" name="passwd" class="form-control">
            </div>
          </div>
          <div class="form-group col-6">
            <label>Password Confirm</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fas fa-lock"></i>
                </div>
              </div>
              <input type="password" id="passwd_confirm" name="passwd_confirm" class="form-control">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-6">
            <label>Email</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="far fa-envelope"></i>
                </div>
              </div>
              <input type="text" id="email" name="email" class="form-control phone-number">
            </div>
          </div>
          <div class="form-group col-6">
            <label>Phone Number (US Format)</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fas fa-phone"></i>
                </div>
              </div>
              <input type="text" id="phone" name="phone" class="form-control phone-number">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-6">
            <label>level</label>
            <select class="form-control" name="level">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </div>
         <!--  <div class="form-group col-6">
            <label>Province</label>
            <select class="form-control">
              <option>West Java</option>
              <option>East Java</option>
            </select>
          </div> -->
        </div>



        <div class="form-group">
          <div class="custom-control custom-checkbox">
          <!--   <input type="checkbox" name="agree" class="custom-control-input" id="agree">
            <label class="custom-control-label" for="agree">I agree with the terms and conditions</label> -->
          </div>
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block">등록</button>
          <button type="submit" class="btn btn-light btn-block">취소</button>
        </div>
      </form>
    </div>

  </div>
</div>
</div>

  </section>
</div>