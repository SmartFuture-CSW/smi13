
 <div class="main-content">
        <section class="section">
          <h1 class="section-header"><div>회원관리</div></h1>

         	<div class="row">
              <div class="col-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <div class="float-right">
                      <form name="frm" id="frm" method="post" action="/admin/userLogin">
                        <input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo;?>">
                        <div class="input-group">
                        	<select class="form-control" name="searchType" id="searchType">
		                        <option value="vUserId" <?php if(@$_POST['searchType']=='vUserId') echo 'selected';?>>회원아이디</option>
		                        <option value="vName" <?php if(@$_POST['searchType']=='vUserIp') echo 'selected';?>>아이피</option>
		                        <option value="vHp" <?php if(@$_POST['searchType']=='vUserAgent') echo 'selected';?>>접속정보</option>
                      		</select>
                          <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Search" value="<?=@$_POST['keyword'];?>">
                          <div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
                        </div>
                      </form>
                    </div>
                    <h4>접속기록</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                        	<tr>
	                        <th width="6%">No.</th>
                          <th>User ID</th>
		                      <th>User IP</th>
		                      <th>User Agent</th>
		                      <th>JoinDate</th>
                        </tr>
                        </thead>
                        <tbody>

								<?php foreach ($list as $value) { ?>

                        <tr>
                          <td><?=$no--;?></td>
                          <td><?=$value['vUserId'];?></td>
		                      <td><?=$value['vUserIp'];?></td>
		                      <td><?=$value['vUserAgent'];?></td>
		                      <td><?=$value['dtRegDate'];?></td>
                        </tr>

								<?php } #foreach ?>

                      </tbody>
                    </table>


                    </div>
                    <?=$paging?>
                  </div>
                </div>
              </div>
            </div>
        </section>
      </div>

<script type="text/javascript">
  function pageRemote(pageNo){
    $('#pageNo').val(pageNo);
    document.frm.submit();
  }
</script>