<script src="/public/admin/modules/jquery.min.js"></script>
<div class="main-content">
	<section class="section">
		<h1 class="section-header"><div>문자발송</div></h1>
		<div class="row">
			<div class="col-12">
				<div class="card card-primary">
					<div class="card-body">
					<form name="frm" id="frm" method="post" onsubmit="return send(this)">
						<div class="row">
							<div class="form-group col-1">
								<label style="color:red;"><b>CASH</b></label>
								<div class="input-group"><b><?=number_format($cash)?>원</b></div>
							</div>
							<div class="form-group col-1">
								<label style="color:red;"><b>SMS</b></label>
								<div class="input-group"><b><?=number_format($sms)?>건</b></div>
							</div>
							<div class="form-group col-1">
								<label style="color:red;"><b>LMS</b></label>
								<div class="input-group"><b><?=number_format($lms)?>건</b></div>
							</div>
							<div class="form-group col-1">
								<label style="color:red;"><b>MMS</b></label>
								<div class="input-group"><b><?=number_format($mms)?>건</b></div>
							</div>
						</div>

						<div class="row">
							<div class="form-group col-2">
								<label>기간</label>
								<div class="input-group">
									<input type="text" name="reg1" id="reg1" class="form-control" style="padding:0px 3px;width:60px;line-height:44px;" value="<?=@$_POST['reg1'];?>" placeholder="가입일 시작" autocomplete="off">&nbsp;&nbsp;~&nbsp;&nbsp;
									<input type="text" name="reg2" id="reg2" class="form-control" style="padding:0px 3px;width:60px;line-height:44px;" value="<?=@$_POST['reg2'];?>" placeholder="가입일 종료" autocomplete="off">
								</div>
							</div>
							<div class="form-group col-1">
								<label>&nbsp;</label>
								<div class="input-group"><button type="button" class="btn mem-search">조회</button></div>
							</div>
							<div class="form-group col-3">
								<label>발송대상</label>
								<div class="input-group">
									<select class="form-control" name="receive_list" id="receive_list" required><option value="">발송대상 유형을 선택하세요</option>
											<option value="test">Test - 황욱/최상운</option>
											<option value="research">리서치팀</option>
											<option value="">--------------------------------------------------------------------------------------------</option>
											<option value="premiumSMS">프리미엄SMS회원(<?=number_format(count($premiumSMS))?>)</option>
											<option value="">--------------------------------------------------------------------------------------------</option>
											<option value="all">전체회원(<?=number_format($mcount['all'][0]['cnt'])?>)</option>
											<option value="charge">유료회원(<?=number_format($mcount['charge'][0]['cnt'])?>)</option>
											<option value="free">무료회원(<?=number_format($mcount['all'][0]['cnt']-$mcount['charge'][0]['cnt'])?>)</option>							
											<option value="">--------------------------------------------------------------------------------------------</option>
											<?php foreach($stockUser as $row) { ?>
												<option value="<?=$row['nSeqNo']?>">추천종목 - <?=$row['vStockName']?> [<?=$row['cnt']?>]</option>
											<?php } ?>
											<option value="">--------------------------------------------------------------------------------------------</option>
									</select>
								</div>
							</div>
							<div class="form-group col-6">
								<label>발송타입</label>
								<div class="input-group">
									<select class="form-control" name="vType" id="vType">
										<option value="sms">sms</option>
										<option value="lms">lms</option>
										<!--option value="push">앱푸시</option>
										<option value="sms_push">sms+앱푸시</option>
										<option value="lms_push">lms+앱푸시</option-->
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-6">
								<label>제목</label>
								<div class="input-group"><input type="text" id="vSubject" name="vSubject" class="form-control"></div>
							</div>
							<div class="form-group col-6">
								<label>내용</label>
								<div class="input-group">
									<textarea id="txContent" name="txContent" class="form-control" required maxlength="65536" style="width:100%;height:250px"></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div style="clear:both;"></div>
							<div style="float:left;"><a href="javascript:void(0);" class="btn btn-primary modal-go" onclick="set_modal(1)">결제내역</a></div>
							<div style="float:left;margin-left:5px;"><a href="javascript:void(0);" class="btn btn-primary modal-go" onclick="set_modal(2)">충전하기</a></div>
							<div style="float:right;"><button type="button" class="btn btn-primary btn-block" onclick="send(document.frm)">전송</button></div>
							<div style="clear:both;"></div>
						</div>
					</form>
					</div>
                    <div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div><input type='hidden' id='charge_price' value='10'>
<script>
	function send(f){
		var receive_list=$('#receive_list option:selected').val();
		if(receive_list==''){
			alert('발송대상을 선택해주세요.');
			return false;
		}
		if(f.txContent.value==''){
			alert('내용을 입력해주세요.');
			return false;
		}
		var para=$('#frm').serialize();
		$.post("/admin/adminsms/smsInsert",para,function(data){
			if(data.code=='1'){
				alert("발송되었습니다.");
				document.location.href='/admin/adminsms/';
			}else{
				alert("오류! - "+data.msg);
				return false;
			}
		},'json');
	}

	function set_modal(no){
		$('.close').remove();
		$('.modal-body').empty();
		var sstr=tit='';
		if(no==1){
			$('.modal-dialog').css('max-width','800px');
			tit='결제내역';
			var str='<table style="width:100%;" border="1"><tr style="background-color:#d0d0d0;"><td align="center">번호</td><td align="center">결제일</td><td align="center">결제수단</td><td align="center">금액</td><td align="center">CASH</td><td align="center">상태</td><td align="center">요청일</td></tr>';
			$.post("/admin/adminsms/popmain",'',function(data){
				$.each(data.list.result,function(idx,item){
					str+='<tr>';
					$.each(item,function(index,gap){
						str+='<td align="center">'+item[index]+'</td>';
					});
					str+='</tr>';
				});
				str+='</table><div style="height:30px;"></div>';
				$('.modal-body').append(str);
			},'json');
		}else if(no==2){
			$('.modal-dialog').css('max-width','500px');
			tit='충전하기';
			//var str='<div style="width:100%;text-align:center;"><select name="price" class="form-control" onchange="$(\'#charge_price\').val(this.value);"><option value="10">10만원</option><option value="30">30만원</option><option value="50">50만원</option><option value="100">100만원</option><option value="300">300만원</option><option value="500">500만원</option><option value="1000">1,000만원</option></select></div><div style="padding-top:10px;text-align:center;"><b>입금정보</b>&nbsp;&nbsp;&nbsp;&nbsp;국민 / 468001-01-153740 / (주)예스빗</div><div style="padding:20px 0px 10px;text-align:center;"><a href="javascript:void(0);" class="btn btn-primary modal-go" onclick="callCharge()">충전요청하기</a></div>';
			var str='<div style="width:100%;text-align:center;"><b>http://www.sendmon.com/settle/settle.asp<br>입금자명 : 청홀노다지</b></div><div style="padding:20px 0px 10px;text-align:center;"><a href="http://www.sendmon.com/settle/settle.asp" class="btn btn-primary modal-go" target="_blank">충전요청하러가기</a></div>';
			$('.modal-body').append(str);
		}	
		$('.modal-header').empty().append('<h5 class="modal-title">'+tit+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color:#fff;"> x </button>');
	}

	function callCharge(){
		var price = $("#charge_price").val();
		if(confirm(price+"만원 충전요청을 하시겠습니까?")){
			$.post("/admin/adminsms/popmain",{price:price},function(data){
				if(data.code=='1'){
					alert("신청이 완료되었습니다.");
					document.location.href='/admin/adminsms';
				}
			});
		}
	}

	$(function(){
		$('.mem-search').on('click',function(){
			var reg1=$('#reg1').val();
			var reg2=$('#reg2').val();
			$.post("/admin/adminsms/refresh",{reg1:reg1,reg2:reg2},function(data){
				if(data.code=='1'){
					$('#receive_list').children('option:eq(4)').remove();
					$('#receive_list').children('option:eq(5)').remove();
					$('#receive_list').children('option:eq(5)').remove();
					$('#receive_list').children('option:eq(5)').remove();
					$('#receive_list').children('option:eq(3)').after("<option value='premiumSMS'>프리미엄SMS회원("+data.premiumSMS+")</option>");
					$('#receive_list').children('option:eq(5)').after("<option value='all'>전체회원("+data.all+")</option>");
					$('#receive_list').children('option:eq(6)').after("<option value='charge'>유료회원("+data.charge+")</option>");
					$('#receive_list').children('option:eq(7)').after("<option value='free'>무료회원("+data.free+")</option>");
					$('#receive_list').hide();
					$('#receive_list').fadeIn();
				}
			},'json');
		});
	});
</script>