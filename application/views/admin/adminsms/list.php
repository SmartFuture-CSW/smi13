 <style>
	.resulttotal{background-color:#FFEAEA;text-align:right;font-weight:bold;font-size:18px;}
	.tdstyle{text-align:center;width:70px;background-color:#f0f0f0;}
 </style>
 <div class="main-content">
        <section class="section">
          <h1 class="section-header"><div>문자발송 내역</div></h1>
         	<div class="row">
              <div class="col-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <div class="float-right" style="width:100%;">
                      <form name="frm" id="frm" method="post" action="/admin/adminsms/smslist" style="width:100%;">
                        <input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo;?>">
                        <input type="hidden" name="bd_no" id="bd_no" value="">
						<input type="hidden" name="mode" id="mode" value="">
						<input type="hidden" name="udname" id="udname" value="">
						<input type="hidden" name="udval" id="udval" value="">
                        <div class="input-group" style="width:100%;">
							<select class="form-control" name="vType" id="vType" style="height:30px;padding:0px 3px;">
		                        <option value="">검색할 유형을 선택해주십시오.</option>
		                        <option value="0"<?php if(@$_POST['vType']=='0') echo ' selected';?>>전체</option>
		                        <option value="-2"<?php if(@$_POST['vType']=='-2') echo ' selected';?>>문자인증</option>
		                        <option value="1"<?php if(@$_POST['vType']=='1') echo ' selected';?>>유료회원(<?=number_format($mcount['charge'][0]['cnt'])?>)</option>
		                        <option value="-1"<?php if(@$_POST['vType']=='-1') echo ' selected';?>>무료회원(<?=number_format($mcount['all'][0]['cnt']-$mcount['charge'][0]['cnt'])?>)</option>
                      		</select>
                        	<select class="form-control" name="sdate" id="sdate" style="height:30px;padding:0px 3px;">
								<?for($i=0;$i<12;$i++){
									$months[]=date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
									$thisMonth = str_replace("-","",$months[$i]);?>
									<option value="<?=$thisMonth?>" <?if(@$_POST['sdate']==$thisMonth){echo " selected";}?>><?=$months[$i]?>월</option>
								<?}?>
                      		</select>
                          <div class="input-group-btn"><button type="submit" class="btn btn-secondary"><i class="ion ion-search"></i></button></div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                        	<tr>
							  <th width="6%">번호</th>
		                      <th>메세지 내용</th>
		                      <th>발송일</th>
		                      <th>타입</th>
		                      <th>총건수</th>
							  <th>성공/대기/실패</th>
                        </tr>
                        </thead>
                        <tbody>
							<?if(!empty($vType) && count($list) &&  $list != "no result"){
									$no = count($list);?>
								<tr>
									<td colspan="6" class="resulttotal">
										총건수 : <?=number_format($total)?>건, 성공 : <?=number_format($success)?>건, 대기 : <?=number_format($wait)?>건, 실패 : <?=number_format($fail)?>건
									</td>
								</tr>
								<?foreach($list as $var){?>
								<tr class="modal-go" onclick="set_modal('<?=$var->uniq_id?>','<?=$var->used_cd?>')">
									<td class="text-center"><?=$no?></td>
									<td><?=$var->title==""?"[제목없음]":$var->title?></td>
									<td class="nick curp"><?=$var->senddate?></td>
									<td><?=$var->type?></td>
									<td><?=$var->total?></td>
									<td><?=$var->success?>/<?=$var->wait?>/<?=$var->fail?></td>
								</tr>
								<?	$no--;
									}
							}else{?>
								<tr><td colspan="6" class="text-center" height="200"><br><br><br><br>발송내역이 없습니다.</td></tr>
							<?}?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
	</section>
</div>
<script type="text/javascript">
  function pageRemote(pageNo){
    $('#pageNo').val(pageNo);
    document.frm.submit();
  }

	function set_modal(uid,ucd){
		$('.close').remove();
		$('.modal-body').empty();
		var str=tit='';
		$('.modal-dialog').css('max-width','800px');
		tit='발송내역';
		$.post("/admin/adminsms/popdetail",{"uniq_id":uid,"used_cd":ucd},function(data){
			str='<table border="1" width="100%">';
			str+='<tr><th class="tdstyle">제목</th><td style="padding:6px;" colspan="5" id="title">'+data.detail[0].title+'</td></tr>';
			str+='<tr><th class="tdstyle">내용</th><td style="padding:6px;" colspan="5" id="msg">'+data.detail[0].msg+'</td></tr>';
			str+='<tr><th class="tdstyle">발송일시</th><td style="padding:6px;" colspan="5" id="senddate">'+data.detail[0].senddate+'</td></tr>';
			str+='<tr><th class="tdstyle">발신번호</th><td style="padding:6px;" colspan="5" id="caller">'+data.detail[0].caller+'</td></tr>';
			str+='<tr><th class="tdstyle">전송유형</th><td style="padding:6px;" colspan="5" id="type">'+data.detail[0].type+'</td></tr>';
			str+='<tr><th class="tdstyle">총건수</th><td style="padding:6px;" colspan="5" id="total">'+data.detail[0].total+'건</td></tr>';
			str+='<tr><th class="tdstyle">성공</th><td style="padding:6px;width:197px;" id="success">'+data.detail[0].success+'건</td><th class="tdstyle">진행중</th><td style="padding:6px;width:197px;" id="wait">'+data.detail[0].wait+'건</td><th class="tdstyle">실패</th><td style="padding:6px;width:196px;" id="fail">'+data.detail[0].fail+'건</td></tr>';
			str+='<tr><th class="tdstyle">소요캐시</th><td style="padding:6px;" colspan="5" id="cash">'+data.detail[0].cash+'원</td></tr>';
			str+='<tr><td colspan="6" height="1">&nbsp;</td></tr>';
			for(var i=1;i<data.detail.length;i++){
				status = data.detail[i].status;
				if(status=="success")status="성공";
				if(status=="ing")status="대기";
				str+='<tr><th class="tdstyle">수신번호</th><td style="padding:6px;">'+data.detail[i].rno+'</td><th class="tdstyle">결과</th><td style="padding:6px;">'+status+'</td><th class="tdstyle">발송일시</th><td style="padding:6px;">'+data.detail[i].sendtime+'</td></tr>';
			}
			str+='</table><div style="height:30px;"></div>';
			$('.modal-body').append(str);
		},'json');
		$('.modal-header').empty().append('<h5 class="modal-title">'+tit+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color:#fff;"> x </button>');
	}
</script>