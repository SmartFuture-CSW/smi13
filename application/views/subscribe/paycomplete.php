<?if($this->pcmode=='' && $this->andapp=='1') echo '<script src="/cordova/android/cordova.js"></script>'; else if($this->pcmode=='ios') echo '<script src="/cordova/ios/cordova.js"></script>';?>
<div class="app_wrapper">	
	<!-- [Start] App Header -->
	<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
	<!-- [End] App Header -->
	
	<!-- [Start] App Main -->
	<div class="app_main subs03">
		<div class="layout_center">
			<div class="inner_container">

<?php if($vGoodsKind == "1"){	// 노다지 기본 구독 영역 시작?>

				<h1 class="title_check">
					<em>노다지 주식정보</em>를<br>
					구독해 주셔서 감사합니다
				</h1>
				<p>
					노다지 주식정보를 더 완벽하게할<br>
					구독자 필수 APP
				</p>
				<button type="button" class="btn_l full bg_red dwld">구독자 전용 앱 다운로드</button>
				<p>
					노다지 VIP구독 서비스는<br> 
					<em>구독자 전용 앱</em>을 통해서 받으실 수 있습니다.
				</p>
				<p>
					<a href="/purchase?topay=pay&paytype=sms"><img src="/asset/img/banner_paycomplete_sms.png" width="100%" /></a>
					<a href="/purchase?topay=pay&paytype=vip"><img src="/asset/img/banner_paycomplete_vip.png" width="100%" /></a>
				</p>
<?php
} 	// 노다지 기본 구독 영역 끝
else if($vGoodsKind == "2"){ // 프리미엄 문자반 영역 시작?>
				<h1 class="title_check">
					<em>프리미엄 문자반</em>을<br>
					구독해 주셔서 감사합니다
				</h1>
				<p>
					프리미엄 문자반은 종목 매수매도 정보 및<br>
					주식 정보를 <em>100% 실시간 문자</em>로
					제공받아 볼 수 있는 상품입니다.
				</p>
				<p>
					프리미엄 문자반 서비스는<br>
					<em>가입 시 등록한 번호</em>통해서 <em>문자</em>로<br>
					받으실 수 있습니다.
				</p>
				<p>
					<a href="/purchase?topay=pay&paytype=basic"><img src="/asset/img/banner_paycomplete_basic.png" width="100%" /></a>
					<a href="/purchase?topay=pay&paytype=vip"><img src="/asset/img/banner_paycomplete_vip.png" width="100%" /></a>
				</p>
<?php
} else if($vGoodsKind == "3"){ // 프리미엄 VIP 구독 영역 시작 ?>

				<h1 class="title_check">
					<em>프리미엄 VIP</em>를<br>
					구독해 주셔서 감사합니다
				</h1>
				<p>
					프리미엄 VIP 더 완벽하게할<br>
					구독자 필수 APP
				</p>
				<button type="button" class="btn_l full bg_red dwld">구독자 전용 앱 다운로드</button>
				<p>
					프리미엄 VIP 구독 서비스는<br> 
					<em>구독자 전용 앱</em>을 통해서 받으실 수 있습니다.
				</p>
				<p>
					<a href="/purchase?topay=pay&paytype=basic"><img src="/asset/img/banner_paycomplete_basic.png" width="100%" /></a>
					<a href="/purchase?topay=pay&paytype=sms"><img src="/asset/img/banner_paycomplete_sms.png" width="100%" /></a>
				</p>
<?php } else {  // 프리미엄 VIP 구독 영역 끝?>

				<h1 class="title_check">
					<em>노다지 주식정보</em>를<br>
					구독해 주셔서 감사합니다
				</h1>
				<p>
					노다지 주식정보를 더 완벽하게할<br>
					구독자 필수 APP
				</p>
				<button type="button" class="btn_l full bg_red dwld">구독자 전용 앱 다운로드</button>
				<p>
					노다지 VIP구독 서비스는<br> 
					<em>구독자 전용 앱</em>을 통해서 받으실 수 있습니다.
				</p>
				<p>
					<a href="/purchase?topay=pay&paytype=sms"><img src="/asset/img/banner_paycomplete_sms.png" width="100%" /></a>
					<a href="/purchase?topay=pay&paytype=vip"><img src="/asset/img/banner_paycomplete_vip.png" width="100%" /></a>
				</p>
<?php } ?>
			</div>
		</div>
	</div>
	<!-- [End] App Main -->

	<!-- [Start] App Bottom -->
	<div class="app_bottom">
		<? include_once(VIEW_PATH.'/include/gnb.php'); ?>
	</div>
	<!-- [End] App Bottom -->
</div>

<!-- [Start] Popup - Advertisement -->
<?if(!empty($popup)){ ?>
	<div class="remodal advertisement" data-remodal-id="pop_ad">
		<button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
		<a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
	</div>
<?}?>
<!-- [End] Popup - Advertisement -->
<!-- in script -->
<script>
	// Make Noscroll One Page 
	(function(){
		var deviceType = localStorage.getItem("deviceType");
		var viewportHeight = $(window).height();
		var headerHeight = $('.app_header').height();
		var bottomHeight = $('.app_bottom').height();
		$('.app_main').height(viewportHeight-headerHeight-bottomHeight); // .img_full를 선택하여도 무방하다

		$(".dwld").on('click', function(){
			if(deviceType == "iOS" || deviceType == "Android") {
				cordova.InAppBrowser.open('/subscribe/vipapp', '_system', 'location=no');
			}else{
				window.location.href = "/subscribe/vipapp";
			}
		});
<?if(!empty($popup)){ ?>
	$('[data-remodal-id=pop_ad]').remodal().open();
<?}?>
	})();
</script>