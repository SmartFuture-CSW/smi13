<?if($this->pcmode=='' && $this->andapp=='1') echo '<script src="/cordova/android/cordova.js"></script>'; else if($this->pcmode=='ios') echo '<script src="/cordova/ios/cordova.js"></script>';?>
<style>
.app_wrapper{padding-bottom:0px}
.goodsInfo{
width:90%; 
padding:0px 5% 10% 5%;
margin-top:100px;
background-image:url(/asset/img/bn_bg.gif);
background-repeat:repeat;
background-position:left 52%;
float:left;
height:auto;
}
.goodsInfo a{margin-top:10px;  float:left;}
.goodsInfo a:first-child{margin-top:-50px;}

</style>
<div class="app_wrapper">	
	<!-- [Start] App Header -->
	<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
	<!-- [End] App Header -->
	
	<!-- [Start] App Main -->
	<div class="app_main subs04">
		<div class="layout_center" style="">
			<div class="inner_container">

				<h1 class="title_check" style="margin-top:30px;">
					<em>노다지 포인트</em>를<br>
					구매해 주셔서 감사합니다
				</h1>
				<p>
					유료 종목 구매 및 <br>
					단일 제공 컨텐츠를 <br>
					<em>포인트로 구매</em>하실 수 있습니다 <br>
<br>
					유료 종목 추천 구매 후 <br>
					<em>목표 기간 내 손절가 도달 시</em><br>
<br>
					결제한 만큼의 <em>포인트로 <br>
					환불</em> 받으실 수 있습니다. 
				</p>
				<p>
					노다지 VIP구독 서비스는<br> 
					<em>구독자 전용 앱</em>을 통해서 받으실 수 있습니다.
				</p>

			</div>
		</div>
		<div class="goodsInfo">
			<p>
				<a href="/stock"><img src="/asset/img/banner_paycomplete_stock.png" width="100%" /></a>
				<a href="/purchase?topay=pay&paytype=vip"><img src="/asset/img/banner_paycomplete_vip.png" width="100%" /></a>
			</p>
		</div>
	</div>
	<!-- [End] App Main -->

	<!-- [Start] App Bottom -->
	<div class="app_bottom">
		<? include_once(VIEW_PATH.'/include/gnb.php'); ?>
	</div>
	<!-- [End] App Bottom -->
</div>

<!-- [Start] Popup - Advertisement -->
<?if(!empty($popup)){ ?>
	<div class="remodal advertisement" data-remodal-id="pop_ad">
		<button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
		<a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
	</div>
<?}?>
<!-- [End] Popup - Advertisement -->
<!-- in script -->
<script>
	// Make Noscroll One Page 
	(function(){
		var deviceType = localStorage.getItem("deviceType");
		var viewportHeight = $(window).height();
		var headerHeight = $('.app_header').height();
		var bottomHeight = $('.app_bottom').height();
		/*$('.app_main').height(viewportHeight-headerHeight-bottomHeight); // .img_full를 선택하여도 무방하다*/

<?if(!empty($popup)){ ?>
	$('[data-remodal-id=pop_ad]').remodal().open();
<?}?>
	})();
</script>