<div class="app_wrapper">	
		<!-- [Start] App Header -->
		<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
		<!-- [End] App Header -->
		
		<!-- [Start] App Main -->
		<div class="app_main subs03">
			<div class="layout_center">
				<div class="inner_container">
					<h1 class="title_check">
						<em>노다지 주식정보</em>를<br>
						구독해 주셔서 감사합니다
					</h1>
					<p>
						노다지 주식정보를 더 완벽하게할<br>
						구독자 필수 APP
					</p>
					<button type="button" class="btn_l full bg_red dwld">노다지VIP 다운로드</button>
					<p>
						노다지 구독 서비스의 <em>기본 리딩 서비스</em>는 <em>노다지 VIP</em>를 통해 받으실 수 있습니다.<br>
						꼭 다운로드 받으세요!
					</p>
				</div>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<!-- in script -->
	<script>
		// Make Noscroll One Page 
		(function(){
			var viewportHeight = $(window).height();
			var headerHeight = $('.app_header').height();
			var bottomHeight = $('.app_bottom').height();
			$('.app_main').height(viewportHeight-headerHeight-bottomHeight); // .img_full를 선택하여도 무방하다
		})();
	</script>