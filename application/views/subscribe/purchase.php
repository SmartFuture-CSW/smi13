<div class="app_wrapper">	
		<!-- [Start] App Header -->
		<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
		<!-- [End] App Header -->
		
		<!-- [Start] App Main -->
		<div class="app_main">
			<a class="img_banner" href="">
				<img src="../asset/img/subs/subs_banner.png" alt="">
			</a>
			<div class="subscribe_order layout_center">
				<div class="radio_container clearfix">
					<input type="radio" name="method" id="method1" class="a11y_hidden" checked>
					<label for="method1">기간별 1회 결제 구독</label>
					<input type="radio" name="method" id="method2" class="a11y_hidden">
					<label for="method2">정기 구독</label>
				</div>

				<div class="subs_description">
					<p>
						<em>기간별 1회 결제 구독</em>은 선택하신 기간에 대한 구독료를<br><em>할인가로 한 번에 결제</em> 후 사용하게 됩니다.
					</p>
				</div>

				<div class="choice_box">
					<h3>노다지 구독 서비스 상품 선택</h3>
					<div class="radio_container select_service clearfix case1" style="display:block;">
						<input type="radio" name="subs1" id="item1" class="a11y_hidden" checked>
						<label for="item1" class="clearfix">
							<span>2년 구독권</span>
							<p class="right_side">
								<strong>237,600</strong>원
								<small>( 하루 325원 상당 / 730일 기준 )</small>
							</p>
						</label>

						<input type="radio" name="subs1" id="item2" class="a11y_hidden">
						<label for="item2" class="clearfix">
							<span>1년 구독권</span>
							<p class="right_side">
								<strong>237,600</strong>원
								<small>( 하루 325원 상당 / 730일 기준 )</small>
							</p>
						</label>

						<input type="radio" name="subs1" id="item3" class="a11y_hidden">
						<label for="item3" class="clearfix">
							<span>6개월 구독권</span>
							<p class="right_side">
								<strong>78,000</strong>원
								<small>( 하루 433원 상당 / 180일 기준 )</small>
							</p>
						</label>
					</div>

					<div class="radio_container select_service type2 case2">
						<input type="radio" name="subs2" id="item4" class="a11y_hidden" checked>
						<label for="item4">
							매월 <strong>15,000</strong>원
						</label>
					</div>
				</div>

				<div class="form_container">
					<div class="input_box">
						<label for="input1">카드번호</label>
						<input type="tel" id="input1" placeholder="'-'없이 숫자만 입력하세요">
					</div>
					<div class="input_box">
						<label for="input2">유효기간</label>
						<input type="tel" id="input2" placeholder="입력예시(월/년) : 0522">
					</div>
					<div class="input_box">
						<label for="input3">주문자명</label>
						<input type="text" id="input3" placeholder="특수문자 사용금지">
					</div>
					<div class="input_box">
						<label for="input4">전화번호</label>
						<input type="text" id="input4" placeholder="'-'없이 숫자만 입력하세요">
					</div>
				</div>

				
				<label class="checkbox type_agree">
					<input type="checkbox" name="" id="">
					<i></i>기존 결제 정보 사용
				</label>

				<div class="bottom_check">
					<label class="checkbox type_agree">
						<input type="checkbox" name="" id="">
						<i></i>아래 사항에 모두 동의 합니다.
					</label><br>
					<label class="checkbox type_agree">
						<input type="checkbox" name="" id="">
						<i></i>이용약관 동의
					</label>
					<button type="button" data-remodal-target="popTermService" style="margin-right:0.5em">[필수]</button>
					<label class="checkbox type_agree">
						<input type="checkbox" name="" id="">
						<i></i>개인정보 수집 및 이용동의
					</label>
					<button type="button" data-remodal-target="popTermPersonalInfo">[필수]</button><br>
					<label class="checkbox type_agree">
						<input type="checkbox" name="" id="">
						<i></i>마케팅정보 수신 동의
					</label>
					<button type="button" data-remodal-target="popTermMarketing">[선택]</button>
				</div>

				<div class="btn_container" style="margin-top:20px;">
					<button type="button" class="btn_l full bg_red">노다지 주식정보 구독하기</button>
				</div>

			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<!-- [Start] Popup - Terms:서비스 이용약관 -->
	<div class="remodal terms" data-remodal-id="popTermService">
		<div class="term_container">
		<p><?php include_once (TEXT."/service_agree.txt"); ?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:서비스 이용약관 -->

	<!-- [Start] Popup - Terms:개인정보 수집 및 이용 -->
	<div class="remodal terms" data-remodal-id="popTermPersonalInfo">
		<div class="term_container">
		<p><?php include_once (TEXT."/info_agree.txt"); ?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:개인정보 수집 및 이용 -->

	<!-- [Start] Popup - Terms:마케팅정보 수신 동의 -->
	<div class="remodal terms" data-remodal-id="popTermMarketing">
		<div class="term_container">
			<p>광고성 정보수신 동의

(1) 서비스 안내 및 이용권유 등



① 제공받는 자 : (주)엔더블유홀딩스

② 제공목적 : 서비스 안내 및 이용권유, 사은·판촉행사 등의 마케팅 활동, 시장조사 및 상품·서비스 개발연구 등 고객데이터 수집 및 관리

③ 수집항목 : 이름, 휴대폰번호

④ 수집 및 이용기간 : 정기결제 종료 시


회원님은 동의를 거부할 시 이용자 식별이 어려워 서비스 제공이 불가합니다. 또한 서비스 이용권유, 판촉행사 등의 유익한 정보를 받으실 수 없습니다.
			</p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:마케팅정보 수신 동의 -->
