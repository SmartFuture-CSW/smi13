<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="javascript:window.history.back(-2)" aria-label="뒤로가기"></a>
				<h2>결제정보 변경등록</h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main mypg_edit_card">
			<div class="layout_center">
				<div class="inner_container">
		<form name="frm" id="frm" method="post">
					<div class="form_container">
						<h2 class="form_heading">결제정보 변경등록</h2>

						<div class="input_box2">
							<label for="vCardNo">카드번호</label>
							<input id="vCardNo" class="onlyNum" type="tel" name="vCardNo" placeholder="카드번호를 입력해주세요" maxlength="16" value="<?=$vCardNo?>">
						</div>
						<div class="input_box2">
							<label for="vExpdt">만료날짜</label>
							<input id="vExpdt" class="onlyNum" type="tel" name="vExpdt" placeholder="MM/YY" maxlength="4" value="<?=$vExpdt?>">
						</div>
						<!--div class="input_box2">
							<label for="birth">생년월일</label>
							<input id="birth" class="onlyNum" type="tel" name="birth" placeholder="생년월일 6자리를 입력해주세요" maxlength="6">
						</div-->


						<!-- [Start] 기존 소스 
						<div class="input_box has_4_input">
							<input class="onlyNum" name="cardNo1" type="tel" maxlength="4">
							<input class="onlyNum" name="cardNo2" type="password" maxlength="4">
							<input class="onlyNum" name="cardNo3" type="password" maxlength="4">
							<input class="onlyNum" name="cardNo4" type="tel" maxlength="4">
						</div>

						<div class="input_box">
							<input class="onlyNum" name="cardDate" type="tel" placeholder="만료날짜 (예: 05월 22년 = 0522)" maxlength="4">
						</div>
						<div class="input_box">
							<input class="onlyNum" name="birth" type="tel" placeholder="생년월일 (990101)" maxlength="6">
						</div>
						<!-- [End] 기존 소스 -->

						<p class="product_info">
							노다지 주식정보입니다.<br>
							변경하신 카드정보로<br>
							결제가 진행됩니다.<br>
							노다지 주식정보를 사용해주셔서 감사합니다.
						</p>
						
						<div class="bottom_check">
							<label class="checkbox type_agree">
								<input type="checkbox" name="allCheck" id="allCheck">
								<i></i>아래 사항에 모두 동의 합니다.
							</label><br>
							<label class="checkbox type_agree">
								<input type="checkbox" name="agree[]" id="service_agree">
								<i></i>서비스 이용약관 동의
							</label>
							<button type="button" data-remodal-target="popTermService" id="popTermService">[보기]</button><br>
							<label class="checkbox type_agree">
								<input type="checkbox" name="agree[]" id="info_agree">
								<i></i>개인정보 수집 및 이용동의
							</label>
							<button type="button" data-remodal-target="popTermPersonalInfo" id="popTermPersonalInfo">[보기]</button><br>
							<label class="checkbox type_agree">
								<input type="checkbox" name="agree[]" id="pay_agree">
								<i></i>개인 정보를 결제 서비스업체에 제공하는데에 동의합니다.
							</label>
						</div>

					
					</div>
				</div>
		</form>
				<div class="btn_container">
					<button  class="btn_l full bg_black" id="submitBtn">변경완료</button>
				</div>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div><!-- 알럿메세 -->
	<!-- [Start] Popup - Terms:서비스 이용약관 -->
	<div class="remodal terms" data-remodal-id="popTermService">
		<div class="term_container">
			<p><?php include_once (TEXT."/service_agree.txt"); ?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:서비스 이용약관 -->

	<!-- [Start] Popup - Terms:개인정보 수집 및 이용 -->
	<div class="remodal terms" data-remodal-id="popTermPersonalInfo">
		<div class="term_container">
			<p><?php include_once (TEXT."/info_agree.txt"); ?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:개인정보 수집 및 이용 -->

<script type="text/javascript">
// 전체 체크 

$(function(){
	$("#allCheck").change(function(){ Cmmn.checkAll('allCheck'); });

	// 체크박스 한개라도 체크시 전체 체크사라지게 
	$("input[name='agree[]']").change(function(){ 
		var agree_1 = Is.checkBox( 'info_agree'),
		agree_2 = Is.checkBox( 'service_agree'),
		agree_3 = Is.checkBox( 'pay_agree');
		if(agree_1==true && agree_2==true && agree_3==true) $('#allCheck').prop('checked', true);
		else $('#allCheck').prop('checked', false);
	});

	
	$("#submitBtn").off('click').on('click', function(){

		var vCardNo = $("#vCardNo").val();
		var vExpdt = $("#vExpdt").val();
		if(!vCardNo) {  Cmmn.alertMsg('카드 번호를 입력해주세요'); $("#vCardNo").focus(); return false; }
		if(!vExpdt) {  Cmmn.alertMsg('카드 만료날짜를 입력해주세요'); $("#vExpdt").focus(); return false; }
		// 동의 체크 
		if( !Is.checkBox( 'service_agree') ) { Cmmn.alertMsg('서비스 이용약관 이용에 동의해야합니다.'); return false; }
		if( !Is.checkBox( 'info_agree') ) { Cmmn.alertMsg('개인정보 수집 이용에 동의해야합니다.'); return false; }
		if( !Is.checkBox( 'pay_agree') ) { Cmmn.alertMsg('결제 서비스업체에 제공 이용에 동의해야합니다.'); return false; }

		var u = "/store/addPaymentInfo"
		var data = {
			'vCardNo' : vCardNo
			, 'vExpdt' : vExpdt
		}
		
		$.post({ url : u, dataType : 'json', data : data,
			success : function(response) {
				alert('변경 되었습니다.');
				window.location.reload();
			},
			error : function(resposne){
				console.log(response);
			}
		});
	});
});

</script>