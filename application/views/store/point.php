

	<div class="app_wrapper">
		
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="/store" aria-label="뒤로가기"></a>
				<h2>출석 포인트 받기</h2>
			</div>
		</header>
		<!-- [End] App Header -->
		
		<!-- [Start] App Main -->
		<div class="app_main has_bottom_button">
			<h3 class="attendance_heading">누적포인트 <em id="pointTotal">2,700P</em></h3>
			<ol class="attendance_point_trip clearfix">
				<li data-point="10">
					<em>10</em>
					<span>1Day</span>
				</li>
				<li data-point="10">
					<em>10</em>
					<span>2Day</span>
				</li>
				<li data-point="10">
					<em>10</em>
					<span>3Day</span>
				</li>
				<li data-point="10">
					<em>10</em>
					<span>4Day</span>
				</li>
				<li data-point="50">
					<em>50</em>
					<span>추가보너스</span>
				</li>
				<li data-point="20">
					<em>20</em>
					<span>6Day</span>
				</li>
				<li data-point="20">
					<em>20</em>
					<span>7Day</span>
				</li>
				<li data-point="20">
					<em>20</em>
					<span>8Day</span>
				</li>
				<li data-point="20">
					<em>20</em>
					<span>9Day</span>
				</li>
				<li data-point="100">
					<em>100</em>
					<span>추가보너스</span>
				</li>
				<li data-point="30">
					<em>30</em>
					<span>11Day</span>
				</li>
				<li data-point="30">
					<em>30</em>
					<span>12Day</span>
				</li>
				<li data-point="30">
					<em>30</em>
					<span>13Day</span>
				</li>
				<li data-point="30">
					<em>30</em>
					<span>14Day</span>
				</li>
				<li data-point="150">
					<em>150</em>
					<span>추가보너스</span>
				</li>
				<li data-point="40">
					<em>40</em>
					<span>16Day</span>
				</li>
				<li data-point="40">
					<em>40</em>
					<span>17Day</span>
				</li>
				<li data-point="40">
					<em>40</em>
					<span>18Day</span>
				</li>
				<li data-point="40">
					<em>40</em>
					<span>19Day</span>
				</li>
				<li data-point="200">
					<em>200</em>
					<span>추가보너스</span>
				</li>
				<li data-point="50">
					<em>50</em>
					<span>21Day</span>
				</li>
				<li data-point="50">
					<em>50</em>
					<span>22Day</span>
				</li>
				<li data-point="50">
					<em>50</em>
					<span>23Day</span>
				</li>
				<li data-point="50">
					<em>50</em>
					<span>24Day</span>
				</li>
				<li data-point="250">
					<em>250</em>
					<span>추가보너스</span>
				</li>
				<li data-point="60">
					<em>60</em>
					<span>26Day</span>
				</li>
				<li data-point="60">
					<em>60</em>
					<span>27Day</span>
				</li>
				<li data-point="60">
					<em>60</em>
					<span>28Day</span>
				</li>
				<li data-point="60">
					<em>60</em>
					<span>29Day</span>
				</li>
				<li data-point="300">
					<em>300</em>
					<span>추가보너스</span>
				</li>
				<li data-point="400">
					<em>400</em>
					<span>31Day</span>
				</li>
			</ol>
			
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<button type="button" class="btn_l full bg_red" id="submitBtn" <?if($isPoint) echo 'disabled';?> >포인트 받기</button>
			<!-- <button type="button" class="btn_l full bg_red" data-remodal-target="popPurchase" disabled>포인트 받기</button> -->

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>


	<!-- In Script -->
	<script>
		$(document).ready(function(){

			var pointCnt = <?=$pointCnt?>;
			var pointTotal = 0;
			// 포인트 획득한 순서대로 색채우기 
			for (var i = 0; i < pointCnt; i++) {
				$('.clearfix').children('li').eq(i).addClass('active');
				pointTotal = pointTotal + $('.clearfix').children('li').eq(i).data('point');
			}
			$('#pointTotal').html(pointTotal+'P');
			//var pointCnt = 4;
			/* Body background-color change */
			$('body').css('background-color', '#fff');

			$('#submitBtn').on('click', function(){
				
				$.ajax({
	                type : "post",
	                url : "/store/addPoint",
	                dataType : 'json',
	                data : { 'point': $('.clearfix').children('li').eq(pointCnt).data('point') },

	                success : function(response) {

	                    if( response.result == 'success') 
	                    {
	                       location.replace('/store/point');
	                    }

	                    if( response.result == 'error' ) Cmmn.alertMsg(response.msg);
	                    
	                },

	                error : function(xhr, status, error) {},
	            })

			});


			// 아래 코드는 지우시고 작업해주세요. 단순 애니메이션 확인용 코드입니다
			// $('.attendance_point_trip li').on('click', function(){
			// 	$(this).toggleClass('active');
			// });
			
			
			

		});
	</script>