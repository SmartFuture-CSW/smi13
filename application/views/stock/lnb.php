
<?php if($page == "search"){ ?>
		<div class="lnb_main type02 layout_center clearfix">
			<a href="/stock/search/all/<?=$keyword?>" <? if( 'all' == $this->uri->segment(3) || null == $this->uri->segment(3) ) echo 'class="active"';?>>전체</a>
			<a href="/stock/search/period/<?=$keyword?>" <? if( 'period' == $this->uri->segment(3) ) echo 'class="active"';?>>이슈종목</a>
			<a href="/stock/search/news/<?=$keyword?>" <? if( 'news' == $this->uri->segment(3) ) echo 'class="active"';?>>뉴스종목</a>
			<a href="/stock/search/theme/<?=$keyword?>" <? if( 'theme' == $this->uri->segment(3) ) echo 'class="active"';?>>테마종목</a>
			<a href="/stock//search/result/<?=$keyword?>" <? if( 'result' == $this->uri->segment(3) ) echo 'class="active"';?>>투자성과</a>
		</div>
<?php } else { ?>
		<div class="lnb_main type02 layout_center clearfix">
			<a href="/stock" <? if( '' == $this->uri->segment(2) ) echo 'class="active"';?>>전체</a>
			<a href="/stock/period" <? if( 'period' == $this->uri->segment(2) ) echo 'class="active"';?>>이슈종목</a>
			<a href="/stock/news" <? if( 'news' == $this->uri->segment(2) ) echo 'class="active"';?>>뉴스종목</a>
			<a href="/stock/theme" <? if( 'theme' == $this->uri->segment(2) ) echo 'class="active"';?>>테마종목</a>
			<a href="/stock/result" <? if( 'result' == $this->uri->segment(2) ) echo 'class="active"';?>>투자성과</a>
		</div>
<?php } ?>