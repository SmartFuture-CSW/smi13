<style>
.stck{position:relative;}
</style>
<div class="app_wrapper">
		<!-- [Start] App Header -->
		<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
		<!-- [End] App Header -->
		<!-- [Start] App Main -->
		<div class="app_main stck has_bottom_banner">
			<!-- lnb -->
			<div class="psuedo_container">
				<div class="search_container layout_center">
					<div class="search_box">
						<form action="/stock/search" method="get">
							<input type="search" name="keyword" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
							<button type="submit" aria-label="검색"></button>
						</form>
					</div>
				</div>
				<!-- lnb --><? include_once(VIEW_PATH.'/stock/lnb.php'); ?>
			</div>
			<section class="stck_section theme_main_container">
				<h2><em>테마별</em> 추천종목</h2>
	<?php
		$p = 0;
		foreach ($theme as $key => $value) { 
			$tmp = trim($value['txTag']);
			$tag = explode("#", $tmp);
			$arrColorPalette = array('#fef4ea', '#fffcc9', '#e1f8fe', '#dcf9e7');

	?>
				<a href="/stock/view/theme/<?=$value['nSeqNo']?>" class="theme_item" style="background-color: <?=$arrColorPalette[$p%4]?>;">
					<div class="center_box">
						<h4>목표 수익률 <?=$value['nAimPercent']?>%</h4>
						<p><?=$value['vSubject']?></p>
						<?  // 태그 
							if(count($tag) > 1 ) { 
								echo '<ul class="hashtags">'; 
								for ($i=1; $i<count($tag) ; $i++) echo '<li>'.$tag[$i].'</li>'; 
								echo '</ul>';
							}
						?>
					</div>
				</a>

	<?php
		$p++;
	}
	?>
			</section>
			<div class="banner_point"><a href="/purchase/point"><img src="/asset/img/point.png"></a></div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] Popup - Advertisement -->
		<?if(!empty($popup)){ ?>
			<div class="remodal advertisement" data-remodal-id="pop_ad">
				<button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
				<a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
			</div>
		<?}?>
		<!-- [End] Popup - Advertisement -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
				<? foreach ($banner as $key => $value) { ?>
					<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
				<?}?>
			</div>
			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

<!-- in script -->
<script>
$(".banner_point").css("top", $(window).scrollTop() + $(window).height() - 300 +"px");
	$(window).scroll(function() { 
		/*$('.banner_point').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 100});*/
		$(".banner_point").css("top", $(window).scrollTop() + $(window).height() - 300 +"px");
	}); 

(function(){
		
<?if(!empty($popup)){ ?>
	$('[data-remodal-id=pop_ad]').remodal().open();
<?}?>

})();
</script>