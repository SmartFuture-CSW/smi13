<?
	// 게시글 날짜 + 종목일 = 종료일
	$regdate= substr($view['dtRegDate'], 0, 10);
	$rate 	= $view['nAimDate'];
	$str_date = strtotime($regdate.'+'.$rate.' days');

	// 종료일 
	$endDate = date('m월 d일', $str_date);
	$endDate2 = date('Y.m.d', $str_date);

	// 달성일 구하기 종료일 - 오늘
	if($str_date >= time()){
//		echo ($str_date - time()) / (60*60*24);
		//$day = date('d', $str_date - time());
		$day = round(($str_date - time()) / (60*60*24));
		$per = (int)$day/(int)$rate*100;
	}else{
		$day = 0;
		$per = 0;
	}
	
?>


	<div class="app_wrapper">	
		<header class="app_header">
			<div class="back">
				<a href="javascript:window.history.back(-2)" aria-label="뒤로가기"></a>
				<h2><?=$view['vSubject']?></h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main">
			<div class="heading_container layout_center">
				<h1>추천종목</h1>
			</div>


		<?php	if(empty($isAccess)){?>
			<div class="before_period_item_buy" >
				<section class="stck_section">
					<h2><em>추천종목</em> 기본정보</h2>
					<div class="item_basic_info">
						<h3><?=$view['vSubject']?></h3>
						<p>달성 <?=$day?>일전 ( <?=$endDate?>까지 )</p>
					</div>
				</section>
	
				<div class="stock_instructions type_period clearfix">
					<dl class="target_return_rate">
						<dt>목표수익률</dt>
						<dd><?=$view['nAimPercent']?>%</dd>
					</dl>
					<dl class="target_time">
						<dt>목표기간</dt>
						<dd><?=$view['nAimDate']?>일</dd>
					</dl>
					<dl class="price_range">
						<dt>종목가격대</dt>
						<dd>20만원 이하</dd>
					</dl>
				</div>
				<div>
					<?=$view['txContentBefore']?>
				</div>
			</div>
		<?php	} else { ?>

			<div class="stock_information_container ">
				<section class="stock_summary layout_center">
					<div class="clearfix">
						<h2>
						<span>종목명</span>
						<small><?=$view['txTag']?></small>
						<?=$view['vStockName']?>
					</h2>
	
					<div class="fluctuation">
						<small>목표수익률</small>
						<span class="up"><?=$view['nAimPercent']?>%</span>
						<!-- .fluctuation > span 태그의 상태로 normal은 적용되는 클래스가 없고, .fluctuation > span.up이면 상승 .fluctuation > span.down이면 하락 입니다. -->
					</div>
					</div>

					<div class="market_watching">
						<span>시장주목도</span>
						<em><?=$view['nMarketAtt']?>%</em>
						<div class="progress_box">
							<span style="width:<?=$view['nMarketAtt']?>%;"></span>
						</div>
					</div>
				</section>
	
				<div class="recommended_item">
					<div class="stock_instructions clearfix">
					<dl>
						<dt>매수가</dt>
						<dd>
							<small>1차</small><?=number_format($view['nBuyPrice1'])?>원<br>
							<small>2차</small><?=number_format($view['nBuyPrice2'])?>원
						</dd>
					</dl>
					<dl>
						<dt>목표가</dt>
						<dd>
							<span><?=number_format($view['nAimPrice'])?>원</span>
						</dd>
					</dl>
					<dl>
						<dt>손절가</dt>
						<dd>
							<span><?=number_format($view['nLossPrice'])?>원</span>
							이탈 시
						</dd>
					</dl>
				</div>
					
					<div class="instruction_content">
					<img src="/data/stock/thumb/<?=$view['vImage']?>" alt="">
				</div>
				<div>
					<?=$view['txContent']?>
				</div>
				</div>
			</div>
			<?php	}?>


			<div class="item_period_info">
				<p>
					종료일 <em><?=$endDate2?></em>
					<b>남은기간 까지 <em><?=$day?>일</em></b>
				</p>
				<div class="progress_box">
					<span style="width:<?=$per?>%;"></span>
				</div>
			</div>
			<div class="banner_type1">
				<? foreach ($banner as $key => $value) { ?>
            		<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
        		<?}?>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<!-- [Start] Stock Information Purchase -->
		<?  if(empty($isAccess)){ ?>
			<div class="stockinfo_purchase clearfix">
				<h2>종목 확인</h2>
				<div class="f_right">
					<em><?=number_format($view['nPrice'])?>원</em>
					<a href="/pay/point/<?=$view['nPrice']?>/<?=$view['nSeqNo']?>/day">구매하기</a>
					<!-- <button type="button">구매하기</button> -->
					<!-- a태그 button 태그 둘 중 어떤걸로 작업하셔도 상관 없습니다 -->
				</div>
			</div>
	<?	}   ?>
			<!-- [End] Stock Information Purchase -->

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>

		</div>
		<!-- [End] App Bottom -->
	</div>
	
	<!-- in script -->
	<script>
		(function(){
			/* Advertisement Slider */
			/*
			$('.banner_type1').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				infinite: true,
				speed: 500,
				// dots: true,
				arrows: false,
			});
			*/

		})();
	</script>

</body>
</html>