	
	
	<div class="app_wrapper">
		
		<!-- [Start] App Header -->
		<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
		<!-- [End] App Header -->
		
		<!-- [Start] App Main -->
		<div class="app_main stck has_bottom_banner">
	
			<div class="lnb_main clearfix">
				<a href="/stock" >전체보기</a>
				<a href="/stock/theme">테마별 종목</a>
				<a href="/stock/day" class="active">기간별 종목</a>
				<a href="/stock/rumor">찌라시 종목</a>
			</div>
		
			<section class="stck_section">
				<h2><em>기간별</em> 추천종목</h2>
				<div class="period_item_container">
					<table>
						<colgroup>
							<col style="width:20%">
							<col>
							<col style="width:22%">
						</colgroup>
						<thead>
							<tr>
								<th>목표기간</th>
								<th>종목컨셉</th>
								<th>목표수익률</th>
							</tr>
						</thead>
						<tbody>
					<?  
				        foreach ($day as $key => $value) {           
	      			?>
							<tr>
								<td><?=$value['nAimDate']?>일</td>
								<td>
									<a href="/stock/view/day/<?=$value['nSeqNo']?>" class="ellipsis"><?=$value['vSubject']?></a>
									<span><?=substr($value['dtRegDate'], 0,10);?></span>
								</td>
								<td><?=$value['nAimPercent']?>%</td>
							</tr>
					<?}?>
						</tbody>
					</table>
				</div>
			</section>

		</div>
		<!-- [End] App Main -->

		<!-- [Start] Popup - Advertisement -->
	    <?if(!empty($popup)){ ?>
	        <div class="remodal advertisement" data-remodal-id="pop_ad">
	            <button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
	            <a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
	        </div>
	    <?}?>
	    <!-- [End] Popup - Advertisement -->
	    
		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
				<? foreach ($banner as $key => $value) { ?>
            		<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
        		<?}?>
			</div>

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<!-- in script -->
	<script>
		(function(){
			/* Theme Item Slider */
			$('.banner_theme_recommend').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				infinite: true,
				speed: 500,
				autoplaySpeed: 5000,
				dots: true,
				arrows: false
			});

			/* Tabloid Item Slider */
			$('.banner_tabloid').slick({
				slidesToShow: 3,
				slidesToScroll: 2,
				infinite: false,
				speed: 500,
				dots: true,
				variableWidth: true,
				arrows: false
			});

		<?if(!empty($popup)){ ?>
        	$('[data-remodal-id=pop_ad]').remodal().open();
    	<?}?>
		
		})();
	</script>