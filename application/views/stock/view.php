<?
	// 게시글 날짜 + 종목일 = 종료일
	$regdate= substr($view['dtRegDate'], 0, 10);
	$rate 	= $view['nAimDate'];
	$str_date = strtotime($regdate.'+'.$rate.' days');

	// 종료일 
	$endDate = date('m월 d일', $str_date);
	$endDate2 = date('Y.m.d', $str_date);

	// 달성일 구하기 종료일 - 오늘
	if($str_date >= time()){
//		echo ($str_date - time()) / (60*60*24);
		//$day = date('d', $str_date - time());
		$day = round(($str_date - time()) / (60*60*24));
		$per = (int)$day/(int)$rate*100;
	}else{
		$day = 0;
		$per = 0;

	}
	$thumNailImage = null;?>
	<style>
		.imgCont img{width:100%;}
	</style>
	<div class="app_wrapper" style="position:relative">	
		<!-- [Start] App Header -->
		<header class="app_header type02">
			<a href="/stock/<?=$view['vType']?>" class="btn_back" aria-label="뒤로가기"></a>
			<div class="right_container">
				<!-- .btnLike 와 .btnBmk 에 active 클래스를 추가하면 활성화 스타일이 적용됩니다 -->
				<button class="btnLike" type="button" aria-label="좋아요"><?=$view['nLike']?></button>
				<button class="btnBmk" type="button" aria-label="북마크"><?=$view['nScrap']?></button>
				<button class="btnSns" type="button" aria-label="공유하기"><?=$view['nShare']?></button>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main" style="padding-bottom:30px;">
			<div class="heading_container layout_center">
				<h1>추천종목</h1>
			</div>


		<?php	if(empty($isAccess)){?>
			<div class="before_period_item_buy" >
				<section class="stck_section">
					<h2><em>추천종목</em> 기본정보</h2>
					<div class="item_basic_info">
						<h3><?=$view['vSubject']?></h3>
						<p>달성 <?=$day?>일전 ( <?=$endDate?>까지 )</p>
					</div>
				</section>
	
				<div class="stock_instructions type_period clearfix">
					<dl class="target_return_rate">
						<dt>목표수익률</dt>
						<dd><?=$view['nAimPercent']?>%</dd>
					</dl>
					<dl class="target_time">
						<dt>목표기간</dt>
						<dd><?=$view['nAimDate']?>일</dd>
					</dl>
					<dl class="price_range">
						<dt>종목가격대</dt>
						<dd>20만원 이하</dd>
					</dl>
				</div>

				<img src="/asset/img/ndz_bn_0924.jpg" alt="" class="img_banner">

				<!-- 기존 배너 200924
					<div class="banner_type1">
					<? foreach ($banner as $key => $value) { ?>
						<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
					<?}?>
				</div> -->

				<div class="layout_center imgCont" style="padding-top:20px">
					<?=$view['txContentBefore']?>
				</div>
			</div>
		<?php	} else { ?>

			<div class="stock_information_container ">
				<section class="stock_summary layout_center">
					<div class="clearfix">
						<h2>
						<span>종목명</span>
						<small><?=$view['txTag']?></small>
						<?=$view['vStockName']?>
					</h2>
	
					<div class="fluctuation">
						<small>목표수익률</small>
						<span class="up"><?=$view['nAimPercent']?>%</span>
						<!-- .fluctuation > span 태그의 상태로 normal은 적용되는 클래스가 없고, .fluctuation > span.up이면 상승 .fluctuation > span.down이면 하락 입니다. -->
					</div>
					</div>

					<div class="market_watching">
						<span>시장주목도</span>
						<em><?=$view['nMarketAtt']?>%</em>
						<div class="progress_box">
							<span style="width:<?=$view['nMarketAtt']?>%;"></span>
						</div>
					</div>
				</section>
	
				<div class="recommended_item">
					<div class="stock_instructions clearfix">
						<dl>
							<dt>매수가</dt>
							<dd>
								<small>1차</small><?=number_format($view['nBuyPrice1'])?>원<br>
								<small>2차</small><?=number_format($view['nBuyPrice2'])?>원
							</dd>
						</dl>
						<dl>
							<dt>목표가</dt>
							<dd>
								<span><?=number_format($view['nAimPrice'])?>원</span>
							</dd>
						</dl>
						<dl>
							<dt>손절가</dt>
							<dd>
								<span><?=number_format($view['nLossPrice'])?>원</span>
								이탈 시
							</dd>
						</dl>
					</div>
					
					<div class="instruction_content">
						<img src="/data/stock/thumb/<?=$view['vImage']?>" alt="">
					</div>
					<div class="layout_center imgCont">
						<?=$view['txContent']?>
					</div>
				</div>
			</div>
		<?php	}?>


			<div class="item_period_info">
				<p>
					종료일 <em><?=$endDate2?></em>
					<b>남은기간 까지 <em><?=$day?>일</em></b>
				</p>
				<div class="progress_box">
					<span style="width:<?=$per?>%;"></span>
				</div>
			</div>
			
			<div class="banner_point"><a href="/purchase/point"><img src="/asset/img/point.png"></a></div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<!-- [Start] Stock Information Purchase -->
		<?  if(empty($isAccess)){ ?>
			<div class="stockinfo_purchase clearfix">
				<h2>종목 확인</h2>
				<div class="f_right">
					<em><?=number_format($view['nPrice'])?>원</em>
					<a href="/pay/point/<?=$view['nPrice']?>/<?=$view['nSeqNo']?>/<?=$view['vType']?>">구매하기</a>
					<!-- <button type="button">구매하기</button> -->
					<!-- a태그 button 태그 둘 중 어떤걸로 작업하셔도 상관 없습니다 -->
				</div>
			</div>
	<?	}   ?>
			<!-- [End] Stock Information Purchase -->

			<!-- [Start] SNS Share -->
			<div class="share_container">
				<div class="layout_center">
					<h4>친구에게 공유해볼까요?</h4>
					<button type="button" class="btnFcb">페이스북 공유하기</button>
					<button type="button" class="btnKtk">카카오톡 공유하기</button>
					<button type="button" class="btnMsg">메세지 공유하기</button>
					<button type="button" class="btnUrl">URL 복사하기</button>
				</div>
				<button class="btn_close" aria-label="공유하기 닫기"></button>
			</div>
			<!-- [End] SNS Share -->

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>

		</div>
		<!-- [End] App Bottom -->
	</div>
	
	<!-- in script -->
<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>
<textarea id="NeisTextArea" style="position:absolute; left:-9999px;"></textarea>
<script type="text/JavaScript" src="https://developers.kakao.com/sdk/js/kakao.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.6/clipboard.min.js"></script>
	<script>
		Kakao.init("7b111ba912ef21c93ce5c281b6d40f62");      // 사용할 앱의 JavaScript 키를 설정
		Kakao.isInitialized();
		$(".banner_point").css("top", $(window).scrollTop() + $(window).height() - 250 +"px");
		$(window).scroll(function() { 
			/*$('.banner_point').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 100});*/
			$(".banner_point").css("top", $(window).scrollTop() + $(window).height() - 250 +"px");
		}); 

		$(document).ready(function(){
			var start_record = 0;
			var countLike = <?=$view['nLike']?>;
			var boardNo = <?=$view['nSeqNo']?>;
			var boardType = "<?=$view['vType']?>";
			var isLike = Cookie.get('like_stock_'+boardNo);
			if(isLike) $('#btn_like, .btnLike').addClass('active'); // 좋아요 활성화 
			//replyList(0); // 처음댓글 가져오기 

			var isScrap = '<?=$isScrap?>';
			if(isScrap == 'Y') $('.btnBmk').addClass('active'); // 좋아요 활성화 

			// 스크랩 버튼 클릭
			$(".btnBmk").off("click").on("click", function(){
<?php if(!$this->session->userdata('UNO')){ ?>
				window.location.href = "/auth";
				//$(".lg").click();
<?php } else {  ?>
				var u = "/stock/stockScrap";
				var data = {
					nBoardNo : boardNo,
					vType : boardType
				}
				$.ajax({
					type : "post",
					url : u,
					dataType : 'json',
					data : data,
					success : function(response) {
						if(response.status == "SUCCESS"){
							// 스크랩 완료
							$(".btnBmk").text(response.cnt.nScrap);
							$('.btnBmk').addClass('active'); // 좋아요 활성화 

						}else{
						// 스크랩 실패
							Cmmn.alertMsg(response.msg);
						}
					}
				});
<?php } ?>
			});

			$(".btnUrl").off("click").on("click", function(){
				var deviceType = localStorage.getItem("deviceType");
				$.ajax({
					type : "post",
					url : "/stock/stockShare",
					dataType : 'json',
					data : { 'boardNo': boardNo, 'type' : 'url' },
					success : function(response) {

						var target = $(this).attr("target-id");
						var text = window.location.href;

						$("#NeisTextArea").val(text);
						$("#NeisTextArea").select();

						if(deviceType == "iOS") {
							//alert(document.execCommand('copy'))
							//var text = "Hello World!";
							cordova.plugins.clipboard.copy(text);
							cordova.plugins.clipboard.paste(function (text) {
									alert("보시고 계신 페이지의 URL이 복사되었습니다.\n\n붙여넣기를 통해  공유 해주세요.");
							});
							//cordova.plugins.clipboard.clear();
						}
						else{

							try {
								var successful = document.execCommand('copy');
							}
							catch (err) {
								Cmmn.alertMsg('이 브라우저는 지원하지 않습니다.');
							}

							// 액션이 있으면
							if (text == "") {
								Cmmn.alertMsg("복사할 내용이 없습니다.");
								$("#"+target).focus();
								return false;
							} else {
								if (successful == true && text != "") {
									Cmmn.alertMsg("보시고 계신 페이지의 URL이 복사되었습니다.\n\n붙여넣기를 통해  공유 해주세요.");
								}
							}
						}
					}
				});
			});

			// 좋아요 버튼 클릭  
			$('.btnLike, .btn_like').on('click', function(){
				if(isLike){ 
					Cmmn.alertMsg('이미 좋아요한 상태입니다.'); 
					return false; 
				}

				var data = {
					idx : boardNo
				}
				

				$.ajax({
					type : "post",
					url : "/stock/likeCheck",
					dataType : 'json',
					data : data,
					success : function(response) {
						console.log(response)
						//Cmmn.log('좋아요 클릭 이벤트');
						if( response.result == 'success')  {
							//$('.btnLike').toggleClass('active');
							$('.btnLike').text(countLike+1);
							$('#countLike').text(countLike+1);
							$(".btn_like").addClass("active");
							$(".btnLike").addClass("active")
								
						
							isLike = Cookie.get('like_board_'+boardNo);
							//Cmmn.log('좋아요 수 : '+countLike);
						}

						if( response.result == 'error' ) Cmmn.alertMsg(response.msg);
						
					},

					error : function(xhr, status, error) {},
				})
			});

			/* SNS Share Button Control */
			$('.reaction .btn_share > button').on('click', function(){
				$('.share_container').toggleClass('active');
			});

			// FaceBook 공유
			$(".btnFcb").click(function() {
				var deviceType = localStorage.getItem("deviceType");

				// 댓글 가져오기 
				$.ajax({
					type : "post",
					url : "/stock/stockShare",
					dataType : 'json',
					data : { 'boardNo': boardNo, 'type' : 'facebook' },
					success : function(response) {
						Cmmn.log('페이스북 공유하기');
//						console.log(response)
						if( response.result == 'error' ) {  
							Cmmn.alertMsg(response.msg);
							return false;
						}


						var fb_url = "http://www.facebook.com/sharer/sharer.php?u="+window.location.href;

						if(deviceType == "iOS"){
							cordova.InAppBrowser.open(fb_url, '_system', 'location=no');
						} else {
							window.open(fb_url);
						}
					},
					error : function(xhr, status, error) {},
				})
			});

			$(".btnKtk").click(function() {
				var deviceType = localStorage.getItem("deviceType");

				$.ajax({
					type : "post",
					url : "/stock/stockShare",
					dataType : 'json',
					data : { 'boardNo': boardNo, 'type' : 'kakao' },
					success : function(response) {
						if( response.result == 'error' ) {  
							alert(response.msg)
							return false;
						}
						if(deviceType == "Android"){
							var url = window.location.href;
							var feedLink = { webURL: url }
							var feedSocial = {
								likeCount:0,
								commentCount:0,
								sharedCount:0
							}
							var feedButtons1 = { title: '게시글 확인하기', link: { mobileWebURL: url } }
							var feedButtons2 = { title: '게시글 확인하기', link: { iosExecutionParams: 'param1=value1&param2=value2', androidExecutionParams: 'param1=value1&param2=value2', } }
							var feedContent = {
								title:"<?=addslashes($view['vSubject']);?>",
								description:"<?=SITE_TITLE?>",
								link: feedLink,
								imageURL: "<?=($thumNailImage != null) ? $thumNailImage : ''; ?>"
							};
							var feedTemplate = { content: feedContent, social: feedSocial, buttons: [feedButtons1] };
							KakaoTalk.share(feedTemplate, function (success) {
								/**
								*/
							}, function (error) {
								//alert(error)
							});
						} else if(deviceType == "iOS"){
							var url = window.location.href;
							var imgUrl = '<?=($thumNailImage != null) ? $thumNailImage : ''; ?>';
							imgUrl = encodeURI(imgUrl);
							var feedLink = { webURL: url }
							var feedSocial = { likeCount: 0 }
							var feedButtons1 = { title: '게시글 확인하기', link: { mobileWebURL: url } }
							var feedButtons2 = { title: '게시글 확인하기', link: { iosExecutionParams: 'param1=value1&param2=value2', androidExecutionParams: 'param1=value1&param2=value2', } }
							var feedContent = {
								title:"<?=addslashes($view['vSubject']);?>",
								link: feedLink,
								imageURL: imgUrl
							};
							var feedTemplate = { content: feedContent, social: feedSocial, buttons: [feedButtons1] };
							KakaoCordovaSDK.sendLinkFeed(feedTemplate, function (success) {
								alert('kakao share success');
							}, function (error) {
								alert('t')
								alert(JSON.stringify(error)) ;
							});
						} else {


							Kakao.Link.sendDefault({
								objectType:"feed", 
								content : {
									title:"<?=addslashes($view['vSubject']);?>",
									description:"<?=SITE_TITLE?>",
									imageUrl:"<?=($thumNailImage != null) ? $thumNailImage : 'illidan_stormrage.jpg' ?>",
									link : {
										mobileWebUrl:window.location.href,
										webUrl:window.location.href
									}
								},
								social : {
									likeCount:0,
									commentCount:0,
									sharedCount:0
								},
								buttons : [{
									title:"게시글 확인하기",
									link : {
										mobileWebUrl:window.location.href,
										webUrl:window.location.href // PC버전 카카오톡에서 사용하는 웹 링크 URL
									}
								}]
							});
						}
					},
					error : function(xhr, status, error) {},
				})
			});

			// 문자 공유
			$(".btnMsg").click(function() {
				var deviceType = localStorage.getItem("deviceType");
				$.ajax({
					type : "post",
					url : "/stock/stockShare",
					dataType : 'json',
					data : { 'boardNo': boardNo, 'type' : 'sms' },
					success : function(response) {
						if( response.result == 'error' ) {  
							Cmmn.alertMsg(response.msg);
							return false;
						}
						var sms_url = 'sms:?body=[노다지 주식정보의 수익이 나는 콘텐츠!] 링크 : '+window.location.href;
						if(deviceType == "iOS"){
							var sms_url = 'sms:&body=[노다지 주식정보의 수익이 나는 콘텐츠!] 링크 : '+window.location.href;
							sms_url = encodeURI(sms_url)
							alert(sms_url)
							cordova.InAppBrowser.open(sms_url, '_system', 'location=no');
						}else{
							location.href = sms_url;
						}
					},
					error : function(xhr, status, error) {},
				})
			});

		});
	</script>

</body>
</html>