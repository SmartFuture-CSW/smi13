<style>

.result tbody tr td:nth-child(1) {
font-size: 14px !important;
color:#333 !important;
letter-spacing: -0.05em;
text-align:left;
padding-left:1em;
}
.result tbody tr td:nth-child(2) {
font-size: 14px;
color: #f62d0f;
text-align:center;
}
.result tbody tr td:nth-child(3) {
font-size: 14px;
color: #333 !important;
text-align:center;
}
</style>
<div class="app_wrapper">
	<!-- [Start] App Header -->
	<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
	<!-- [End] App Header -->

	<!-- [Start] App Main -->
	<div class="app_main">
		<div class="psuedo_container">
			<div class="search_container layout_center">
				<div class="search_box">
					<form action="/stock/search" method="get">
						<input type="search" name="keyword" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" value="<?=$keyword?>">
						<button type="submit" id="searchBtn" aria-label="검색"></button>
					</form>
				</div>
			</div>
			<!-- lnb --><? include_once(VIEW_PATH.'/stock/lnb.php'); ?>
		</div>

		<h1 class="a11y_hidden">'<?=$keyword?>' 에 대한 검색 결과</h1>

<?
/* @ 주의 : 전체보기와 게시판별 검색결과가 달라 분기처리 함. */

if($pageType == "all"){
?>

		<section class="stck_section">
			<h2><em>이슈별</em> 종목</h2>
			<div class="period_item_container">
				<table>
					<colgroup>
						<col style="width:20%">
						<col>
						<col style="width:22%">
					</colgroup>
					<thead>
						<tr>
							<th>목표기간</th>
							<th>종목컨셉</th>
							<th>목표수익률</th>
						</tr>
					</thead>
					<tbody>
				<?php	foreach ($period as $key => $value) { ?>
						<tr>
							<td><?=$value['nAimDate']?>일</td>
							<td>
								<a href="/stock/view/day/<?=$value['nSeqNo']?>" class="ellipsis"><?=$value['vSubject']?></a>
								<span><?=substr($value['dtRegDate'], 0,10);?></span>
							</td>
							<td><?=$value['nAimPercent']?>%</td>
						</tr>
				<?php	}?>
					</tbody>
				</table>
			</div>
			<a href="/stock/search/period/<?=$keyword?>" class="link_more_result type02">이슈별 종목 <span>검색결과</span> 더보기</a>
		</section>

		<section class="stck_section">
			<h2><em>뉴스별</em> 종목</h2>

			<div class="news_list layout_center">
<?php	foreach ($news as $key => $value) { ?>
				<article>
					<h4><?=$value['vSubject']?></h4>
					<div class="btn_container clearfix">
						<a href="/stock/view/news/<?=$value['nSeqNo']?>">VIP 종목 확인하기</a>
						<a href="<?=$value['vNewsLink']?>">기사원문보기</a>
					</div>
				</article>
<?php  } ?>
			</div>

			<a href="/stock/search/news/<?=$keyword?>" class="link_more_result type02">뉴스별 종목 <span>검색결과</span> 더보기</a>
		</section>
	

		<section class="stck_section">
			<h2><em>테마별</em> 종목</h2>
			<div class="banner_theme_recommend banner_type2">
<?php
			$p = 0;
			foreach ($theme as $key => $value) { 
				$tmp = trim($value['txTag']);
				$tag = explode("#", $tmp);
				//print_r($tag);
				$arrColorPalette = array('#fef4ea', '#fffcc9', '#e1f8fe', '#dcf9e7');
?>
				<a href="/stock/view/theme/<?=$value['nSeqNo']?>" class="theme_item" style="background-color: <?php echo $arrColorPalette[$p % 4]; ?>;">
					<div class="center_box">
						<h4>목표 수익률 <?=$value['nAimPercent']?>%</h4>
						<p><?=$value['vSubject']?></p>
						<?  // 태그 
							if(count($tag) > 1 ) { 
								echo '<ul class="hashtags">'; 
								for ($i=1; $i<count($tag) ; $i++) echo '<li>'.$tag[$i].'</li>'; 
								echo '</ul>';
							}
						?>
					</div>
				</a>
<?php
	$p++;
}
?>
			</div>
			<a href="/stock/search/theme/<?=$keyword?>" class="link_more_result type02"> 테마별 종목 <span>검색결과</span> 더보기</a>
		</section>

		<section class="stck_section">
			<h2>노다지 <em>추천 종목 종료 내역</em></h2>
			<div class="period_item_container infinity result">
				<table>
					<colgroup>
						<col>
						<col style="width:10%">
						<col style="width:22%">
					</colgroup>
					<thead>
						<tr>
							<th>추천 종목 제목</th>
							<th>목표수익률</th>
							<th>비고</th>
						</tr>
					</thead>
					<tbody class="infinity_body">
				<?php	foreach ($result as $key => $value) { ?>
						<tr>
							<td><?=mb_substr($value['vSubject'], 0, 66)?><?=(mb_strlen($value['vSubject']) > 66)?"...":"";?></td>
							<td><?=$value['nAimPercent']?>%</td>
							<td><?=$arrStockResult[$value['nResult']]?></td>
						</tr>
				<?php }?>
					</tbody>
				</table>
			</div>
			<a href="/stock/search/result/<?=$keyword?>" class="link_more_result type02"> 투자 성과 <span>검색결과</span> 더보기</a>
		</section>

		

<?php
// 이슈별 종목 검색결과
}else if($pageType == "period"){
?>
			<section class="stck_section">
				<h2>이슈별 종목</h2>
				<div class="period_item_container">
					<table>
						<colgroup>
							<col style="width:20%">
							<col>
							<col style="width:22%">
						</colgroup>
						<thead>
							<tr>
								<th>목표기간</th>
								<th>종목컨셉</th>
								<th>목표수익률</th>
							</tr>
						</thead>
						<tbody>
					<?php	foreach ($period as $key => $value) { ?>
							<tr>
								<td><?=$value['nAimDate']?>일</td>
								<td>
									<a href="/stock/view/day/<?=$value['nSeqNo']?>" class="ellipsis"><?=$value['vSubject']?></a>
									<span><?=substr($value['dtRegDate'], 0,10);?></span>
								</td>
								<td><?=$value['nAimPercent']?>%</td>
							</tr>
					<?php }?>
						</tbody>
					</table>
				</div>
			</section>



<?php 
// 뉴스별 종목 검색결과
}else if($pageType == "news"){
?>
			<section class="stck_section">
				<h2><em>뉴스별</em> 종목</h2>
				<div class="news_list layout_center"> 
	<?php  foreach ($news as $key => $value) { ?> 
				<article>
					<h4><?=$value['vSubject']?></h4>
					<div class="btn_container clearfix">
						<a href="/stock/view/news/<?=$value['nSeqNo']?>">VIP 종목 확인하기</a>
						<a href="<?=$value['vNewsLink']?>">기사원문보기</a>
					</div>
				</article>
	<?php  } ?>
			</section>
<?php
}else if($pageType == "theme"){
?>

			<section class="stck_section theme_main_container">
				<h2><em>테마별</em> 종목</h2>
	<?php
		$p = 0;
		foreach ($theme as $key => $value) { 
			$tmp = trim($value['txTag']);
			$tag = explode("#", $tmp);
			$arrColorPalette = array('#fef4ea', '#fffcc9', '#e1f8fe', '#dcf9e7');

	?>
				<a href="/stock/view/theme/<?=$value['nSeqNo']?>" class="theme_item" style="background-color: <?=$arrColorPalette[$p%4]?>;">
					<div class="center_box">
						<h4>목표 수익률 <?=$value['nAimPercent']?>%</h4>
						<p><?=$value['vSubject']?></p>
						<?  // 태그 
							if(count($tag) > 1 ) { 
								echo '<ul class="hashtags">'; 
								for ($i=1; $i<count($tag) ; $i++) echo '<li>'.$tag[$i].'</li>'; 
								echo '</ul>';
							}
						?>
					</div>
				</a>

	<?php
		$p++;
	}
	?>
			</section>
<?php
} else if($pageType == "result") { 
?>
<style>
.stck{position:relative;}
.period_item_container tbody tr td:nth-child(1) {
font-size: 14px !important;
color:#333 !important;
letter-spacing: -0.05em;
text-align:left;
padding-left:1em;
}
.period_item_container tbody tr td:nth-child(2) {
font-size: 14px;
color: #f62d0f;
text-align:center;
}
.period_item_container tbody tr td:nth-child(3) {
font-size: 14px;
color: #333 !important;
text-align:center;
}
.link_more_result::after{
content:'\e942';
cursor:pointer;
}
</style>


			<section class="stck_section">
				<h2>노다지 <em>추천 종목 종료 내역</em></h2>
				<div class="period_item_container infinity" data-page="<?=$infinity_page?>">
					<table>
						<colgroup>
							<col>
							<col style="width:10%">
							<col style="width:22%">
						</colgroup>
						<thead>
							<tr>
								<th>추천 종목 제목</th>
								<th>목표수익률</th>
								<th>비고</th>
							</tr>
						</thead>
						<tbody class="infinity_body">
					<?php	foreach ($result as $key => $value) { ?>
							<tr>
								<td><?=mb_substr($value['vSubject'], 0, 66)?><?=(mb_strlen($value['vSubject']) > 66)?"...":"";?></td>
								<td><?=$value['nAimPercent']?>%</td>
								<td><?=$arrStockResult[$value['nResult']]?></td>
							</tr>
					<?php }?>
						</tbody>
					</table>
				</div>
			</section>
			<a class="link_more_result"><span class="switch" style="color:#333;">더보기</span></a>
<?php
}
?>

	</div>
	<!-- [End] App Main -->

	<!-- [Start] App Bottom -->
	<div class="app_bottom"><? include_once(VIEW_PATH.'/include/gnb.php'); ?></div>
</div>
<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>

<!-- In Script -->
<script>
var start_record = 0;
var keyword = '<?=$keyword?>';

$(document).ready(function(){
	
		$(".link_more_result").off('click').on("click", function(){

			var page = $(".infinity").data('page');
			$(".infinity").data('page', page+1);
			page = $(".infinity").data('page');
			var data = {
				page : page
				, 'keyword' : keyword
			}
			$.post("/stock/getInfinityResult", data ,function(response){
				//console.log(response)

				if(response.status == "SUCCESS"){
					
					var arrResultStatus = response.arrStockResult;
					//console.log(arrResultStatus);

					var addHTML = "";
					$.each(response.list, function(idx, row){
						addHTML += '<tr>';
						addHTML += '<td>';
						addHTML += row.vSubject;
						addHTML += '</td>';
						addHTML += '<td>' + row.nAimPercent+ '%</td>';
						addHTML += '<td>' + arrResultStatus[row.nResult]+ '</td>';
						addHTML += '</tr>';
					});
					$(".infinity_body").append(addHTML);
				}
				else{
					alert(response.msg);
				}
			}, 'json');
			
		});
});


function list_html(idx, subject, type, img){
	var html = '';
		html +=	" <a href=\"/boardView/%%TYPE%%/%%IDX%%\">";
		html +=	"   <div class=\"thumbnail\" style=\"background-image:url('../data/board/thumb/%%IMG%%')\"></div>";
		html +=	"   <p>%%SUBJECT%%</p>";
		html +=	" </a>";		
		html = html.replace("%%IDX%%", idx);
		html = html.replace("%%TYPE%%", type);
		html = html.replace("%%SUBJECT%%", subject);
		html = html.replace("%%IMG%%", img);
	return html;	
}
</script>