<div class="app_wrapper">	
<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="javascript:window.history.back(-2)" aria-label="뒤로가기"></a>
				<h2><?=$view['vSubject']?></h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main">
			<div class="heading_container layout_center">
				<h1>찌라시 추천종목</h1>
			</div>

			<div class="banner_type1">
				<? foreach ($banner as $key => $value) { ?>
            		<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
        		<?}?>
			</div>

			<div class="recommended_item type_tabloid <? if(empty($isAccess)) echo 'hidden';?>">
				<!-- .recommended_item에서 hidden 클래스를 제거되면,
				.cover엘리먼트가 자동으로 사라지고 컨텐츠가 보이는 구조입니다. -->
				<div class="instruction_content">
					<?if(!empty($view['vImage'])){?>
						<img src="/data/stock/thumb/<?=$view['vImage']?>" alt="">
					<?}else{?>
					<img src="/asset/img/sample/sample_tabloid.png" alt="">
					<?}?>
				</div>

				<div class="cover">
					<strong>
						종목 확인은 개별<br>
						구매를 하셔야 확인 가능합니다
					</strong>
				</div>
			</div>

		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<!-- [Start] Stock Information Purchase -->
	<?  if(empty($isAccess)){ ?>
			<div class="stockinfo_purchase clearfix">
				<h2>종목 확인</h2>
				<div class="f_right">
					<em><?=number_format($view['nPrice'])?>원</em>
					<a href="/pay/point/<?=$view['nPrice']?>/<?=$view['nSeqNo']?>/rumor">구매하기</a>
					<!-- <button type="button">구매하기</button> -->
					<!-- a태그 button 태그 둘 중 어떤걸로 작업하셔도 상관 없습니다 -->
				</div>
			</div>
	<?	}   ?>
			<!-- [End] Stock Information Purchase -->

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>
	
	<!-- in script -->
	<script>
		(function(){
			/* Advertisement Slider */
			$('.banner_type1').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				infinite: true,
				speed: 500,
				// dots: true,
				arrows: false,
			});

		})();
	</script>