<style>
.stck{position:relative;}
.result tbody tr td:nth-child(1) {
font-size: 14px !important;
color:#333 !important;
letter-spacing: -0.05em;
text-align:left;
padding-left:1em;
}
.result tbody tr td:nth-child(2) {
font-size: 14px;
color: #f62d0f;
text-align:center;
}
.result tbody tr td:nth-child(3) {
font-size: 14px;
color: #333 !important;
text-align:center;
}
</style>
<div class="app_wrapper">
	<!-- [Start] App Header -->
	<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
	<!-- [End] App Header -->
	<!-- [Start] App Main -->
	<div class="app_main stck">
		<div class="psuedo_container">
			<div class="search_container layout_center">
				<div class="search_box">
					<form action="/stock/search" method="get">
						<input type="search" name="keyword" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
						<button type="submit" aria-label="검색"></button>
					</form>
				</div>
			</div>
			<!-- lnb --><? include_once(VIEW_PATH.'/stock/lnb.php'); ?>
		</div>

		<section class="stck_section">
			<h2><em>이슈별</em> 종목</h2>
			<div class="period_item_container">
				<table>
					<colgroup>
						<col style="width:20%">
						<col>
						<col style="width:22%">
					</colgroup>
					<thead>
						<tr>
							<th>목표기간</th>
							<th>종목컨셉</th>
							<th>목표수익률</th>
						</tr>
					</thead>
					<tbody>
				<?php	foreach ($period as $key => $value) {?>
						<tr>
							<td><?=$value['nAimDate']?>일</td>
							<td>
								<a href="/stock/view/period/<?=$value['nSeqNo']?>" class=""><?=mb_substr($value['vSubject'], 0, 66)?><?=(mb_strlen($value['vSubject']) > 66)?"...":"";?></a>
								<span><?=substr($value['dtRegDate'], 0,10);?></span>
							</td>
							<td><?=$value['nAimPercent']?>%</td>
						</tr>
				<?php	}?>
					</tbody>
				</table>
			</div>
		</section>

		<section class="stck_section">
			<h2><em>뉴스별</em> 추천종목</h2>


			<div class="news_list layout_center">
<?php	foreach ($news as $key => $value) { ?>
				<article>
					<h4><?=$value['vSubject']?></h4>
					<div class="btn_container clearfix">
						<a href="/stock/view/news/<?=$value['nSeqNo']?>">VIP 종목 확인하기</a>
						<a href="<?=$value['vNewsLink']?>">기사원문보기</a>
					</div>
				</article>
<?php  } ?>
			</div>
		</section>
	

		<section class="stck_section">
			<h2><em>테마별</em> 추천종목</h2>
			<div class="theme_main_container">
<?php
			$p = 0;
			foreach ($theme as $key => $value) { 
				$tmp = trim($value['txTag']);
				$tag = explode("#", $tmp);
				//print_r($tag);
				$arrColorPalette = array('#fef4ea', '#fffcc9', '#e1f8fe', '#dcf9e7');
?>
				<a href="/stock/view/theme/<?=$value['nSeqNo']?>" class="theme_item" style="background-color: <?php echo $arrColorPalette[$p % 4]; ?>;">
					<div class="center_box">
						<h4>목표 수익률 <?=$value['nAimPercent']?>%</h4>
						<p><?=$value['vSubject']?></p>
						<?  // 태그 
							if(count($tag) > 1 ) { 
								echo '<ul class="hashtags">'; 
								for ($i=1; $i<count($tag) ; $i++) echo '<li>'.$tag[$i].'</li>'; 
								echo '</ul>';
							}
						?>
					</div>
				</a>
<?php
	$p++;
}
?>
			</div>
		</section>


		<section class="stck_section">
			<h2>노다지 <em>추천 종목 종료 내역</em></h2>
			<div class="period_item_container infinity result">
				<table>
					<colgroup>
						<col>
						<col style="width:10%">
						<col style="width:22%">
					</colgroup>
					<thead>
						<tr>
							<th>추천 종목 제목</th>
							<th>목표수익률</th>
							<th>비고</th>
						</tr>
					</thead>
					<tbody class="infinity_body">
				<?php	foreach ($result as $key => $value) { ?>
						<tr>
							<td><?=mb_substr($value['vSubject'], 0, 66)?><?=(mb_strlen($value['vSubject']) > 66)?"...":"";?></td>
							<td><?=$value['nAimPercent']?>%</td>
							<td><?=$arrStockResult[$value['nResult']]?></td>
						</tr>
				<?php }?>
					</tbody>
				</table>
			</div>
		</section>

		<div class="banner_point"><a href="/purchase/point"><img src="/asset/img/point.png"></a></div>
	</div>
	<!-- [End] App Main -->
	<!-- [Start] Popup - Advertisement -->
	<?if(!empty($popup)){ ?>
		<div class="remodal advertisement" data-remodal-id="pop_ad">
			<button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
			<a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
		</div>
	<?}?>
	<!-- [End] Popup - Advertisement -->
	<!-- [Start] App Bottom -->
	<div class="app_bottom"><? include_once(VIEW_PATH.'/include/gnb.php'); ?></div>
</div>

<!-- in script -->
<script>

	$(".banner_point").css("top", $(window).scrollTop() + $(window).height() - 300 +"px");
	$(window).scroll(function() { 
		/*$('.banner_point').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 100});*/
		$(".banner_point").css("top", $(window).scrollTop() + $(window).height() - 300 +"px");
	}); 


(function(){
	/* Theme Item Slider */
	/*
	$('.banner_theme_recommend').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		infinite: true,
		speed: 500,
		autoplaySpeed: 5000,
		dots: true,
		arrows: false
	});
	*/

	/* Tabloid Item Slider */
	/*
	$('.banner_tabloid').slick({
		slidesToShow: 3,
		slidesToScroll: 2,
		infinite: false,
		speed: 500,
		dots: true,
		variableWidth: true,
		arrows: false
	});
	*/
	<?if(!empty($popup)){ ?>
	$('[data-remodal-id=pop_ad]').remodal().open();
	<?}?>


})();
</script>