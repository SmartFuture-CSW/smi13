<style>
.stck{position:relative;}
</style>
<div class="app_wrapper">
		<!-- [Start] App Header -->
		<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
		<!-- [End] App Header -->
		<!-- [Start] App Main -->
		<div class="app_main stck has_bottom_banner">
			<!-- lnb -->
			<div class="psuedo_container">
				<div class="search_container layout_center">
					<div class="search_box">
						<form action="/stock/search" method="get">
							<input type="search" name="keyword" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
							<button type="submit" aria-label="검색"></button>
						</form>
					</div>
				</div>
				<!-- lnb --><? include_once(VIEW_PATH.'/stock/lnb.php'); ?>
			</div>
			<section class="stck_section">
				<h2><em>이슈별</em> 종목</h2>
				<div class="period_item_container">
					<table>
						<colgroup>
							<col style="width:20%">
							<col>
							<col style="width:22%">
						</colgroup>
						<thead>
							<tr>
								<th>목표기간</th>
								<th>종목컨셉</th>
								<th>목표수익률</th>
							</tr>
						</thead>
						<tbody>
					<?php	foreach ($period as $key => $value) { ?>
							<tr>
								<td><?=$value['nAimDate']?>일</td>
								<td>
									<a href="/stock/view/period/<?=$value['nSeqNo']?>" class=""><?=mb_substr($value['vSubject'], 0, 66)?><?=(mb_strlen($value['vSubject']) > 66)?"...":"";?></a>
									<span><?=substr($value['dtRegDate'], 0,10);?></span>
								</td>
								<td><?=$value['nAimPercent']?>%</td>
							</tr>
					<?php }?>
						</tbody>
					</table>
				</div>
			</section>
			<div class="banner_point"><a href="/purchase/point"><img src="/asset/img/point.png"></a></div>
		</div>
		<!-- [End] App Main -->
		<!-- [Start] Popup - Advertisement -->
	    <?if(!empty($popup)){ ?>
	        <div class="remodal advertisement" data-remodal-id="pop_ad">
	            <button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
	            <a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
	        </div>
	    <?}?>
	    <!-- [End] Popup - Advertisement -->
	    
		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
				<? foreach ($banner as $key => $value) { ?>
					<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
				<?}?>
			</div>
			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<!-- in script -->
	<script>
$(".banner_point").css("top", $(window).scrollTop() + $(window).height() - 300 +"px");
	$(window).scroll(function() { 
		/*$('.banner_point').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 100});*/
		$(".banner_point").css("top", $(window).scrollTop() + $(window).height() - 300 +"px");
	}); 

		(function(){
			/* Theme Item Slider 
			$('.banner_theme_recommend').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				infinite: true,
				speed: 500,
				autoplaySpeed: 5000,
				dots: true,
				arrows: false
			});

			/* Tabloid Item Slider 
			$('.banner_tabloid').slick({
				slidesToShow: 3,
				slidesToScroll: 2,
				infinite: false,
				speed: 500,
				dots: true,
				variableWidth: true,
				arrows: false
			});
			*/

			<?if(!empty($popup)){ ?>
				$('[data-remodal-id=pop_ad]').remodal().open();
			<?}?>
		
		})();
	</script>