<style>
.stck{position:relative;}
.period_item_container tbody tr td:nth-child(1) {
font-size: 14px !important;
color:#333 !important;
letter-spacing: -0.05em;
text-align:left;
padding-left:1em;
}
.period_item_container tbody tr td:nth-child(2) {
font-size: 14px;
color: #f62d0f;
text-align:center;
}
.period_item_container tbody tr td:nth-child(3) {
font-size: 14px;
color: #333 !important;
text-align:center;
}
.link_more_result::after{
content:'\e942';
cursor:pointer;
}
</style>
<div class="app_wrapper">
		<!-- [Start] App Header -->
		<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
		<!-- [End] App Header -->
		<!-- [Start] App Main -->
		<div class="app_main stck has_bottom_banner">
			<!-- lnb -->
			<div class="psuedo_container">
				<div class="search_container layout_center">
					<div class="search_box">
						<form action="/stock/search" method="get">
							<input type="search" name="keyword" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
							<button type="submit" aria-label="검색"></button>
						</form>
					</div>
				</div>
				<!-- lnb --><? include_once(VIEW_PATH.'/stock/lnb.php'); ?>
			</div>
			<section class="stck_section">
				<h2>노다지 <em>추천 종목 종료 내역</em></h2>
				<div class="period_item_container infinity" data-page="<?=$infinity_page?>">
					<table>
						<colgroup>
							<col>
							<col style="width:10%">
							<col style="width:22%">
						</colgroup>
						<thead>
							<tr>
								<th>추천 종목 제목</th>
								<th>목표수익률</th>
								<th>비고</th>
							</tr>
						</thead>
						<tbody class="infinity_body">
					<?php	foreach ($result as $key => $value) { ?>
							<tr>
								<td><?=mb_substr($value['vSubject'], 0, 66)?><?=(mb_strlen($value['vSubject']) > 66)?"...":"";?></td>
								<td><?=$value['nAimPercent']?>%</td>
								<td><?=$arrStockResult[$value['nResult']]?></td>
							</tr>
					<?php }?>
						</tbody>
					</table>
				</div>
			</section>
			<a class="link_more_result"><span class="switch" style="color:#333;">더보기</span></a>
			<div class="banner_point"><a href="/purchase/point"><img src="/asset/img/point.png"></a></div>
		</div>
		<!-- [End] App Main -->
		<!-- [Start] Popup - Advertisement -->
	    <?if(!empty($popup)){ ?>
	        <div class="remodal advertisement" data-remodal-id="pop_ad">
	            <button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
	            <a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
	        </div>
	    <?}?>
	    <!-- [End] Popup - Advertisement -->
	    
		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
				<? foreach ($banner as $key => $value) { ?>
					<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
				<?}?>
			</div>
			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<!-- in script -->
	<script>
$(".banner_point").css("top", $(window).scrollTop() + $(window).height() - 300 +"px");
	$(window).scroll(function() { 
		/*$('.banner_point').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 100});*/
		$(".banner_point").css("top", $(window).scrollTop() + $(window).height() - 300 +"px");
	}); 

	(function(){
		<?if(!empty($popup)){ ?>
		$('[data-remodal-id=pop_ad]').remodal().open();
		<?}?>


		$(".link_more_result").off('click').on("click", function(){

			var page = $(".infinity").data('page');
			$(".infinity").data('page', page+1);
			page = $(".infinity").data('page');
			var data = {
				page : page
			}
			$.post("/stock/getInfinityResult", data ,function(response){
				//console.log(response)

				if(response.status == "SUCCESS"){
					
					var arrResultStatus = response.arrStockResult;
					//console.log(arrResultStatus);

					var addHTML = "";
					$.each(response.list, function(idx, row){
						addHTML += '<tr>';
						addHTML += '<td>';
						addHTML += row.vSubject;
						addHTML += '</td>';
						addHTML += '<td>' + row.nAimPercent+ '%</td>';
						addHTML += '<td>' + arrResultStatus[row.nResult]+ '</td>';
						addHTML += '</tr>';
					});
					$(".infinity_body").append(addHTML);
				}
				else{
					alert(response.msg);
				}
			}, 'json');
			
		});
		
	})();
	</script>