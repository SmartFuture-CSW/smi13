<div class="gnb">
	<div class="layout_center clearfix">
		<a href="/info" class="menu_1 <? if( 'info' == $this->uri->segment(1) || '' == $this->uri->segment(1) || 'boardView' == $this->uri->segment(1) && 'community' != $this->uri->segment(2) ) echo 'active';?>">공개정보</a>
	<?php if($_SERVER['ios']== "N"){?>
		<a href="/stock" class="menu_2 <? if( 'stock' == $this->uri->segment(1) ) echo 'active';?>">추천종목</a>
		<a href="/subscribe" class="menu_3 <? if( 'subscribe' == $this->uri->segment(1) || 'purchase' == $this->uri->segment(1)) echo 'active';?>">VIP구독</a>
	<?php }?>
		<a href="/community" class="menu_4 <? if( 'community' == $this->uri->segment(1) || 'boardView' == $this->uri->segment(1) && 'community' == $this->uri->segment(2) ) echo 'active';?>">커뮤니티</a>
		<a href="/mypage" class="menu_5 <? if( 'mypage' == $this->uri->segment(1) ) echo 'active';?>">마이페이지</a>
	</div>
</div>
<button class="btnTop" type="button" aria-label="최상단으로 이동"></button>


<?php
if(!$this->session->userdata('UNO')){
?>

<!-- [Start] Popup - signIn-->
<div class="remodal signIn" data-remodal-id="signInPopup">
	<form id="frm" name="frm" method="post">
		<input type="hidden" name="device" id="device" value="">
		<h2 class="form_heading">인증 회원 등록 후 이용 가능합니다</h2>
		<div class="input_box">
			<input type="text" placeholder="이름" id="username" name="username" >
		</div>
		<div class="input_box has_btn">
			<input type="tel" id="phone" name="phone" placeholder="휴대폰 번호">
			<button type="button" id="phoneCheck" >인증하기</button>
		</div>
		<div class="input_box has_btn">
			<input type="tel" id="code" name="code" placeholder="인증번호">
			<button type="button" id="codeCheck">확 인</button>
		</div>

		<div class="bottom_check" style="margin-top:5px;">
			<label class="checkbox type_agree">
				<input type="checkbox" name="allCheck" id="allCheck">
				<i></i>아래 사항에 모두 동의 합니다.
			</label><br>
			<label class="checkbox type_agree">
				<input type="checkbox" name="agree[]" id="service_agree">
				<i></i>서비스 이용약관 동의
			</label>
			<button type="button" data-remodal-target="popTermService" id="popTermService">[보기]</button><br>
			<label class="checkbox type_agree">
				<input type="checkbox" name="agree[]" id="info_agree">
				<i></i>개인정보 수집 및 이용동의
			</label>
			<button type="button" data-remodal-target="popTermPersonalInfo">[보기]</button><br>
		</div>

		<div class="btn_container clearfix" style="margin-top:5px;">
			<button type="button" class="btn_m bg_black btn_next">다음에 등록하기</button>
			<button type="button" class="btn_m bg_red btn_join">확인</button>
		</div>
	</form>

	<div class="benefit_banner">
		<h4>인증시 얻을 수 있는 혜택 <img src="<?=IMG?>/label_free100.png" alt="100% 무료"></h4>
		<div class="benefitSlider">
			<p>
				<i>1</i><em>하루 1종목</em> 급등 예상 종목 추천
			</p>
			<p>
				<i>2</i><em>이슈별/뉴스별/테마별</em> 종목 정보 제공
			</p>
			<p>
				<i>3</i>궁금한 종목 관련 <em>분석 자료 무료제공</em>
			</p>
			<p>
				<i>4</i>기초부터 실전까지 <em>주식교육 컨텐츠</em> 제공
			</p>
			<p>
				<i>5</i>개인 투자자라면 <em>필요한 모든 정보</em>제공
			</p>
		</div>
	</div>
</div>
<!-- [End] Popup - signIn-->

<!-- [Start] Popup - Nickname Popup-->
<div class="remodal nickname" data-remodal-id="nickname">
	<h2>닉네임 설정</h2>
	<div class="input_box">
		<input type="text" name="nick" id="nick" placeholder="10글자 내외 (한글 영문 상관없이 10자 이내)">
	</div>
	<button type="button" class="btn_l full bg_red btn_nick">로그인</button>
</div>
<!-- [End] Popup - Nickname Popup-->




<!-- [Start] Popup - Terms:서비스 이용약관 -->
<div class="remodal terms" data-remodal-id="popTermService">
	<div class="term_container">
		<p><?php include_once (TEXT."/service_agree.txt"); ?></p>
	</div>
	<div class="pop_bottom">
		<button class="btn_m full"  data-remodal-target="signInPopup">OK</button>
	</div>
</div>
<!-- [End] Popup - Terms:서비스 이용약관 -->


<!-- [Start] Popup - Terms:개인정보 수집 및 이용 -->
<div class="remodal terms" data-remodal-id="popTermPersonalInfo">
	<div class="term_container">
		<p><?php include_once (TEXT."/info_agree.txt"); ?></p>
	</div>
	<div class="pop_bottom">
		<button class="btn_m full" data-remodal-target="signInPopup">OK</button>
	</div>
</div>
<!-- [End] Popup - Terms:개인정보 수집 및 이용 -->



<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>
<script type="text/javascript" src="//js.frubil.info/"></script>
<script>
var brand = FRUBIL.device.brand;         // Samsung
var market = FRUBIL.device.marketname;    // Galaxy A5 (2016)
var authFlag = false;

$(function(){

	// 회원가입 팝업창
	$(".lg").off("click").on('click', function(e){
		e.preventDefault();
		/* Popup - signIn */
		$('[data-remodal-id=signInPopup]').remodal().open();

		var $slider = $('.benefitSlider');
		$slider.slick({
			vertical: true,
			verticalSwiping: true,
			slidesToShow: 1,
			// slidesToScroll: 1,
			swipeToSlide: true,
			autoplay: true,
			autoplaySpeed: 2000,
			infinite: false,
			speed: 1000,
			arrows: false,
			dots: false,
		});

		// Slider Rewinding Helper
		// $slider.on('afterChange', function(event, slick, currentSlide, nextSlide) {
		// 	if (currentSlide === 2) {
		// 		// console.log(currentSlide);
		// 		$slider.slick('slickGoTo', 0);
		// 	}
		// });
	});
	

	// 전체 체크 
	$("#allCheck").change(function(){ Cmmn.checkAll('allCheck'); });

	// 체크박스 한개라도 체크시 전체 체크사라지게 
	$("input[name='agree[]']").change(function(){ 
		var agree_1 = Is.checkBox( 'info_agree'),
			agree_2 = Is.checkBox( 'service_agree'); 
		if(agree_1==true && agree_2==true) $('#allCheck').prop('checked', true);
		else $('#allCheck').prop('checked', false);
	});

	// 다음에 등록하기
	$(".btn_next").off('click').on('click', function(){
		$('[data-remodal-id=signInPopup]').remodal().close();
	});


	// 회원가입 확인
	$(".btn_join").off('click').on('click', function(){
		
	});

	// 인증하기
	$('#phoneCheck').off('click').on('click', function(){
		//if( Is.phone( $('#phone').val() ) ) {
		var phone = $('#phone').val();
		if ( !phone || phone == "" || phone == null || phone == undefined ) {
			Cmmn.alertMsgRemodal("휴대폰 번호를 작성 해주세요.", "signInPopup");
			return false;
		}
		if ( !(/^[0-9]+$/).test(phone) ){
			Cmmn.alertMsgRemodal("숫자만 가능합니다.", "signInPopup");
			return false;
		}
		if ( !(/^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/).test(phone) ){
			Cmmn.alertMsgRemodal("휴대폰 번호를 확인해 주세요", "signInPopup");
			return false;
		}

		var u = "/main/phoneCheck";
		var data = {
			'phone': phone,
			'mode' : 'login'
		};

		$.ajax({
			type : "post",
			url : u,
			dataType : 'json',
			data : data,
			success : function(response) {
				//Cmmn.log(response);
				if( response.result == 'error' ) {  
					Cmmn.alertMsgRemodal(response.msg, "signInPopup");
				}
				if( response.result == 'success') {
					Cmmn.alertMsgRemodal(response.msg, "signInPopup");
				}
			},
			error : function(xhr, status, error) {},
		})
	});

	// 인증번호체크 버튼
	$("#codeCheck").click(function(){

		var code = $('#code').val();
		var u = "/main/codeCheck";
		var data = {
			'code': code
		};

		$.ajax({
			type : "post",
			url : u,
			dataType : 'json',
			data : data,
			success : function(response) {
				//Cmmn.log(response);
				if( response.result == 'error' ) {  

					alert(response.msg);
					//Cmmn.alertMsg(response.msg); 
					authFlag = false;
				}
				if( response.result == 'success') {

					$('#code').attr('readonly', true);  // 성공일때 readonly
					alert(response.msg);
					//Cmmn.alertMsg(response.msg);
					authFlag = true;
				}
			},
			error : function(xhr, status, error) {},
		});
	});

	// 회원가입버튼 
	$(".btn_join").click(function(){
		// 입력 폼 체크 

		var phone = $('#phone').val();
		var name = $('#username').val()
		var code = $('#code').val()


		if ( !name || name == "" || name == null || name == undefined ) {
			Cmmn.alertMsgRemodal("이름을 작성해주세요.", "signInPopup");
			return false;
		}

		if ( !phone || phone == "" || phone == null || phone == undefined ) {
			Cmmn.alertMsgRemodal("휴대폰 번호를 작성 해주세요.", "signInPopup");
			return false;
		}
		if ( !(/^[0-9]+$/).test(phone) ){
			Cmmn.alertMsgRemodal("숫자만 가능합니다.", "signInPopup");
			return false;
		}
		if ( !(/^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/).test(phone) ){
			Cmmn.alertMsgRemodal("휴대폰 번호를 확인해 주세요", "signInPopup");
			return false;
		}
		if ( !code || code == "" || code == null || code == undefined ) {
			Cmmn.alertMsgRemodal("인증번호를 작성해주세요.", "signInPopup");
			return false;
		}

		// 인증번호가 맞는지 체크 
		if(!authFlag) {
			Cmmn.alertMsgRemodal('인증번호가 일치하지 않습니다. 다시확인해주세요.', "signInPopup");
			return false;
		}
		// 동의 체크 
		if( !Is.checkBox( 'service_agree') ) {
			Cmmn.alertMsgRemodal('서비스 이용약관 이용에 동의해야합니다.', "signInPopup");
			return false;
		}
		if( !Is.checkBox( 'info_agree') ) {
			Cmmn.alertMsgRemodal('개인정보 수집 이용에 동의해야합니다.', "signInPopup");
			return false;
		}
			
		$('#device').val(brand+'-'+market);

		$.ajax({
			type : "post",
			url : "/main/loginjoin",
			dataType : 'json',
			data : $('#frm').serialize(),
			success : function(response) {
				if( response.result == 'error' ) {
					Cmmn.alertMsgRemodal(response.msg, "signInPopup");
				}
				if( response.result == 'success') {
					var nick = response.data.vNick;
					if(!nick){
						$('[data-remodal-id=nickname]').remodal().open();
					}
					else{
						window.location.reload();
					}
				}
			},
			error : function(xhr, status, error) {},
		})
//		}
	});


	// 회원가입 버튼
	$(".btn_nick").click(function(){  
		var u = "/main/modifyNick";
		var nick = $('#nick').val();
		var data = {
			'nick' : nick
		};
		$.ajax({
			type : "post",
			url : u,
			dataType : 'json',
			data : data,
			success : function(response) {
				if( response.result == 'error' ) {  
					Cmmn.alertMsgRemodal(response.msg, 'nickname');
				}
				if( response.result == 'success') {
					window.location.reload();
				}
			},
			error : function(xhr, status, error) {},
		})
	});
});


</script>
<?php
}
?>

