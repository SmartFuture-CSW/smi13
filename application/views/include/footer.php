	<script src="<?=JS?>/common.js"></script>
	<script src="<?=JS?>/test.js"></script> 
	<?php if(isset($_GET['app'])) { ?>
		<?php if($_GET['app'] == 1) { ?>
			<?php if(stristr($_SERVER['HTTP_USER_AGENT'],'android') ) { ?>
				<script src="/cordova/android/cordova.js"></script>
			<?php } else if( stristr($_SERVER['HTTP_USER_AGENT'],'ipad') || stristr($_SERVER['HTTP_USER_AGENT'],'iphone') || strstr($_SERVER['HTTP_USER_AGENT'],'iphone') ){  ?>
				<script src="/cordova/ios/cordova.js"></script>
			<?php } ?>
			<script src="/cordova/app.js"></script>
		<?php } ?>
	<?}else if(isset($_GET['first'])) { ?>
		<?php if($_GET['first'] == 'Y') { ?>
			<?php if(stristr($_SERVER['HTTP_USER_AGENT'],'android') ) { ?>
				<script src="/cordova/android/cordova.js"></script>
			<?php } else if( stristr($_SERVER['HTTP_USER_AGENT'],'ipad') || stristr($_SERVER['HTTP_USER_AGENT'],'iphone') || strstr($_SERVER['HTTP_USER_AGENT'],'iphone') ){  ?>
				<script src="/cordova/ios/cordova.js"></script>
			<?php } ?>
			<script src="/cordova/app.js"></script>
		<?php } ?>
	<?}else{?>
		<style>
			.footer_web{background-color:#74757a;padding:20px 20px 80px;display:none;}
			.footer_web h2{color:#fff;font-size:1.4em;font-weight:bold;font-style:oblique;letter-spacing:-3px;}
			.footer_web p{color:#e3e4e9;font-size:0.8em;}
			.footer_web a{background-color:#e3e4e9;color:#74757a;font-size:0.8em;padding:4px 8px;border-radius:4px;}
		</style>
		<div class="footer_web">
			<h2>노다지 주식정보</h2>
			<p>&nbsp</p>
			<p>Copyright (주) 스마트퓨처, ALL RIGHTS RESERVED.</p>
			<p>&nbsp</p>
			<p><a href="/mypage/terms_02">개인정보취급방침</a> <a href="/mypage/terms_01">이용약관</a></p>
			<p>&nbsp</p>
			<p>상호명 : 주식회사 스마트퓨쳐</p>
			<p>대표자 : 홍운기 / 사업자번호 : 616-86-29368</p>
			<p>주소 : 서울특별시 서초구 강남대로275 강남메인타워 8층</p>
			<!--p>상호명 : 주식회사 엔더블유홀딩스</p>
			<p>대표자 : 남성욱 / 사업자번호 : 264-81-32202</p>
			<p>주소 : 서울특별시 서초구 강남대로275 강남메인타워 15층</p-->
			<p>&nbsp</p>
			<p>(주)스마트퓨쳐는 종목발굴 서비스 플랫폼의 제공자로서 종목발굴 컨텐츠 제공의 당사자가 아니며 종목발굴 컨텐츠의 제공, 운영 및 관리 등과 관련한 의무와 책임은 (주)엔더블유홀딩스에게 있습니다.</p
		</div>
	<?}?>
	<script>
		$(function(){
			$.post("/info/visitlog");
			var f_deviceType = localStorage.getItem("deviceType");
			if(f_deviceType!='Android' && f_deviceType!='iOS'){
				$('.footer_web').show();
			}
			if(document.location.href.indexOf('stock/view') > -1 || (document.location.href.indexOf('/subscribe') > -1 && document.location.href.indexOf('/payco') < 0) || document.location.href.indexOf('/mypage') > -1 || document.location.href.indexOf('/pay/point') > -1){
				$('.footer_web').css('padding-bottom','140px');
			}
		});
	<?php if(!empty($_GET['adref'])){ ?>
		if( localStorage.getItem("visitLogCheck") != "Y" ) {
			var data = {
				"adref" : "<?=$_GET['adref']?>"
				, "fullReferer" : window.location.href
			}
			$.ajax({
				type : "post",
				url : "/app/postRefererPc",
				dataType : 'json',
				data : data,
				success : function(response) {
					localStorage.setItem("visitLogCheck", 'Y');
				},
				error : function(xhr, status, error) {},
			})
		}
	<?php }?>
	</script>
</body>
</html>