<?php

$css_ver = date("YmdH", time());

?>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?=SITE_TITLE?></title>
	<meta property="og:locale" content="ko_KR">
	<meta property="og:title" content="<?=SITE_TITLE?>">
	<meta property="og:site_name" content="<?=SITE_NAME?>">
	<meta property="og:description" content="<?=SITE_TITLE?>">
	<meta property="og:type" content="website">
	<link rel="shortcut icon" type="image/x-icon" href="<?=IMG?>/favicon.ico">
	<link rel="stylesheet" href="<?=CSS?>/reset.css">
	<!-- CSS libs -->
	<link rel="stylesheet" href="<?=CSS?>/webfont.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css">
	<link rel="stylesheet" href="<?=CSS?>/lib/slick.css">
	<link rel="stylesheet" href="<?=CSS?>/lib/slick-theme.css">
	<link rel="stylesheet" href="<?=CSS?>/lib/remodal.css">
	<link rel="stylesheet" href="<?=CSS?>/lib/remodal-default-theme.css">
	<!-- Style -->
	<link rel="stylesheet" href="<?=CSS?>/common.css">
	<link rel="stylesheet" href="<?=CSS?>/style.css?ver=<?=$css_ver?>">

	<!-- JS libs & Common JS -->
	<script src="<?=JS?>/lib/jquery-3.4.1.min.js"></script>
	<script src="<?=JS?>/lib/slick.min.js"></script>
	<script src="<?=JS?>/lib/remodal.js"></script>
	<script src="<?=JS?>/lib/gsap.min.js"></script>
	<script data-ad-client="ca-pub-2278074739265312" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!--201201 owner call-->
	<!-- Global site tag (gtag.js) - Google Analytics //201214 ymkt-->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-52060714-17"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-52060714-17');
	</script>
	<?if($this->pcmode=='1') echo $this->pcmode_style;//201008 ksg?>
</head>
<body>