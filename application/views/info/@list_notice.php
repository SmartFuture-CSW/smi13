<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="/mypage" aria-label="뒤로가기"></a>
				<h2>공지사항</h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main mypg_edit_card has_bottom_banner">
			<div class="board_list">
				
		<?  foreach ($notice as $key => $value) { ?> 		

				<a href="/boardView/notice/<?=$value['nSeqNo']?>" class="new">
					<h5><?=$value['vSubject']?></h5>
					<p>
						<span>조회수 <?=$value['nHit']?></span>
						<span><?=$value['dtRegDate']?></span>
					</p>
				</a>

		<?	}   ?>

			</div>

		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
				<? foreach ($banner as $key => $value) { ?>

            	<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
        
        		<?}?> 
			</div>

			<!-- gnb --><? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>