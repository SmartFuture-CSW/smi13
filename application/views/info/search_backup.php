<div class="app_wrapper">
	<!-- [Start] App Header -->
	<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
	<!-- [End] App Header -->

	<!-- [Start] App Main -->
	<div class="app_main">
		<div class="psuedo_container">
			<div class="search_container layout_center">
				<div class="search_box">
					<form action="/info/search" method="post">
						<input type="search" name="keyword" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" value="<?=$keyword?>">
						<button type="submit" id="searchBtn" aria-label="검색"></button>
					</form>
				</div>
			</div>
			<!-- lnb --><? include_once(VIEW_PATH.'/include/lnb.php'); ?>
		</div>

		<div class="category_container ">
			<div class="layout_center">
				<a href="/info/search/all" class="<?=($pageType == "all" || $pageType == "")?"active" : ""; ?>">전체</a>
				<a href="/info/search/info" class="<?=($pageType == "info")?"active" : ""; ?>">공개정보</a>
				<a href="/info/search/stock" class="<?=($pageType == "stock")?"active" : ""; ?>">추천종목</a>
				<a href="/info/search/community" class="<?=($pageType == "community")?"active" : ""; ?>">커뮤니티</a>
			</div>
		</div>
		<h1 class="a11y_hidden">'<?=$keyword?>' 에 대한 검색 결과</h1>

<?
/* @ 주의 : 전체보기와 게시판별 검색결과가 달라 분기처리 함. */

if($pageType == "all"){
?>


		<section class="result_container">
			<h2 class="layout_center">추천종목</h2>
			<div class="essential_list">
	<?php
		if(count($recommend) == 0){
	?>
				<p class="txt_no_result">'<?=$keyword?>' 에 대한 검색 결과가 없습니다.</p>
	<?php
		}
		else {

			foreach ($recommend as $key => $value) { 
				$imgPath = '/data/board/thumb/'.$value['vImage'];
		?>
				<a class="clearfix" href="/boardView/important/<?=$value['nSeqNo']?>">
					<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
					<div class="info">
						<h5><?=$value['vSubject']?></h5>
						<p>
							<span><?=$value['dtRegDate']?></span>
							<span>조회수 <?=$value['nHit']?>회</span>
							<span>공유수 <?=$value['nShare']?>회</span>
						</p>
					</div>
				</a>
	<?php
			}
		}
	?>
			</div>
			<a href="/info/search/important/<?=$keyword?>" class="link_more_result">추천종목 <span>검색결과</span> 더보기</a>
		</section>


		<section class="result_container">
			<h2 class="layout_center">투자정보</h2>
			<div class="essential_list">
	<?php
		if(count($invest) == 0){
	?>
				<p class="txt_no_result">'<?=$keyword?>' 에 대한 검색 결과가 없습니다.</p>
	<?php
		}
		else {

			foreach ($invest as $key => $value) { 
				$imgPath = '/data/board/thumb/'.$value['vImage'];
		?>
				<a class="clearfix" href="/boardView/invest/<?=$value['nSeqNo']?>">
					<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
					<div class="info">
						<h5><?=$value['vSubject']?></h5>
						<p>
							<span><?=$value['dtRegDate']?></span>
							<span>조회수 <?=$value['nHit']?>회</span>
							<span>공유수 <?=$value['nShare']?>회</span>
						</p>
					</div>
				</a>
	<?php
			}
		}
	?>
			</div>
			<a href="/info/search/invest/<?=$keyword?>" class="link_more_result">투자정보 <span>검색결과</span> 더보기</a>
		</section>
		<section class="result_container">
			<h2 class="layout_center">주식공부</h2>
			<div class="essential_list">
	<?php	if(count($study) == 0){?>
				<p class="txt_no_result">'<?=$keyword?>' 에 대한 검색 결과가 없습니다.</p>
	<?php	} else {
			foreach ($study as $key => $value) { 
				$imgPath = '/data/board/thumb/'.$value['vImage'];
	?>
				<a class="clearfix" href="/boardView/study/<?=$value['nSeqNo']?>">
					<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
					<div class="info">
						<h5><?=$value['vSubject']?></h5>
						<p>
							<span><?=$value['dtRegDate']?></span>
							<span>조회수 <?=$value['nHit']?>회</span>
							<span>공유수 <?=$value['nShare']?>회</span>
						</p>
					</div>
				</a>
	<?php
			}
		}
	?>
			</div>
			<a href="/info/search/study/<?=$keyword?>" class="link_more_result">주식공부 <span>검색결과</span> 더보기</a>
		</section>

<?php

// 아래의 분기처리된 HTML은 게시판별 검색결과 처리
}else{
?>
	<section class="result_container">
		<div class="essential_list infinity" data-page="<?=$infinity_page?>">
<?  
foreach ($infinity as $key => $value) { 
	$imgPath = '/data/board/thumb/'.$value['vImage'];
?>
			<a class="clearfix " href="/boardView/<?=$value['vType']?>/<?=$value['nSeqNo']?>">
				<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
				<div class="info">
					<h5><?=$value['vSubject']?></h5>
					<p>
						<span><?=$value['dtRegDate']?></span>
						<span>조회수 <?=$value['nHit']?>회</span>
						<span>공유수 <?=$value['nShare']?>회</span>
					</p>
				</div>
			</a>
<?  } ?>
		</div>
	</section>
<?
}
?>

	</div>
	<!-- [End] App Main -->

	<!-- [Start] App Bottom -->
	<div class="app_bottom"><? include_once(VIEW_PATH.'/include/gnb.php'); ?></div>
</div>
<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>

<!-- In Script -->
<script>
var start_record = 0;
var keyword = '<?=$keyword?>';

$(document).ready(function(){
	
	/**
	* @brief : inifnity scroll
	* @author : csw
	*/
	var busy = false;
	$(window).scroll(function() {
		var winHeight = parseInt($(this).height(), 10);
		var docHeight = parseInt($(document).height(), 10);
		var scrTop = parseInt($(this).scrollTop(), 10);
		if( (docHeight - winHeight - 150) <= scrTop ){

			if(busy){
				return;
			}
			busy = true;

			// 다음에 보여줄 리스트 출력
			var page = $(".infinity").data('page');
			$(".infinity").data('page', page+1);
			page = $(".infinity").data('page');
			var data = {
				type : '<?=$pageType?>',
				page : page,
				keyword : keyword
			}

			$.post("/info/getBoardInfinitySearch", data ,function(response){
				if(response.status == "SUCCESS"){
					$.each(response.list, function(idx, row){
						var anchor = $('<a />', {
							'class' : 'clearfix', 
							href : '/boardView/' + row.vType + '/' + row.nSeqNo
						});

						var thumbnail = $('<div />', {
							'class' : 'thumbnail',
							style : 'background-image:url(\'/data/board/thumb/' + row.vImage + '\')'
						});

						var info =  $('<div />', {
							'class' : 'info',
						});

						var subject = $('<h5 />', {
							text : row.vSubject
						});

						var contents = $('<p />', {});

						var regdate = $('<span />', {
							text : row.dtRegDate
						});

						var hit = $('<span />', {
							text : row.nHit
						});

						var share = $('<span />', {
							text : row.nShare
						});

						contents.append(regdate);
						contents.append(hit);
						contents.append(share);

						info.append(subject);
						info.append(contents);

						anchor.append(thumbnail);
						anchor.append(info);

						$(".infinity").append(anchor)

						busy = false;
					})
				}
			});


		}
	});


});

</script>