<?php




$time_lag = time() - strtotime($view['dtRegDate']);
//echo $time_lag;exit;
if($time_lag < 60)  $posting_time = "방금";
elseif($time_lag >= 60 and $time_lag < 3600) $posting_time = floor($time_lag/60)."분 전";
elseif($time_lag >= 3600 and $time_lag < 86400) $posting_time = floor($time_lag/3600)."시간 전";
elseif($time_lag >= 86400 and $time_lag < 2419200) $posting_time = floor($time_lag/86400)."일 전";
else  $posting_time = date("y-m-d", strtotime($view['dtRegDate']));  
$thumNailImage = null;
?>
<style>
.list_reply img{ width:100%;}
</style>
<div class="app_wrapper">
	<!-- [Start] App Header -->
	<header class="app_header type02">
		<a href="javascript:window.history.back(-2)" class="btn_back" aria-label="뒤로가기"></a>
		<div class="right_container">
			<button class="btnLike" type="button" aria-label="좋아요"><?=$view['nLike']?></button>
			<button class="btnBmk" type="button" aria-label="북마크"><?=$view['nScrap']?></button>
			<button class="btnSns" type="button" aria-label="공유하기"><?=$view['nShare']?></button>
		</div>
	</header>
	<!-- [End] App Header -->

	<!-- [Start] App Main -->
	<div class="app_main is_reply">
		<div class="post_header">
			<h1><?=$view['vSubject'];?></h1>
			<div class="sub_info clearfix">
				<p><?=$view['dtRegDate'];?> <span>조회수 <?=$view['nHit'];?>회</span></p>
			</div>
		</div>
		<div class="community_content">
			<div class="writers_info clearfix">
				<div class="writer">
					<?php if($view['vImage']){ $img = '/data/user/profile/'.$view['vImage'];  ?>
					<div class="user_img" style="background-image: url('<?=$img?>')" ></div>
					<?php } else { ?>
					<div class="user_img"></div>
					<?php } ?>
					<span class="nickname"><?=$view['vNick'];?></span>
					<span class="time">· <?=$posting_time;?></span>
				</div>
				<?php  if( $view['nUserNo'] == $this->session->userdata('UNO') ) { ?>
				<div class="editbox">
					<button type="button" id="delBtn">삭제</button>
					<button type="button" id="modifyBtn">수정</button>
				</div>
				<?php  } ?>
			</div>
			<div class="community_post">
				<p><?=nl2br($view['txContent']);?></p>
			</div>
			<?php
			# tag data 처리 
			$tmp = trim($view['txTag']);
			$tag = explode(",", $tmp);

			if(count($tag) > 1 ) { 
				echo '<p style="margin-bottom:1em">'; for ($i=0; $i<count($tag) ; $i++) echo '<span class="tag_label">#'.$tag[$i].'</span>'; echo '</p>';
			} 
			?>

			<div class="reaction">
				<div class="btn_like" id="btn_like">
					<button type="button" aria-label="좋아요"></button>
					<span id="countLike"><?=$view['nLike'];?></span>
				</div>
				<div class="btn_reply">
					<a href="/info/reply/<?=$view['nSeqNo']?>" aria-label="댓글보기"></a>
					<span><?=$countReply;?></span>
				</div>
				<div class="btn_share">
					<button type="button" aria-label="공유하기"></button>
					<span><?=$view['nShare'];?></span>
				</div>
			</div>
		</div>
		<div class="reply_container">
			<div class="reply_top clearfix">
				<span class="label"><?=$countReply;?>개의 댓글</span>
				<div class="f_right">
					<label class="btn_toggle_txt">
						<input type="checkbox" name="order" id="order">
						<p>
							<span data-order="new">최신순</span>
							<span data-order="like">인기순</span>
						</p>
					</label>
					<label class="btn_toggle_txt">
						<input type="checkbox" name="level" id="level">
						<p>
							<span data-lv="1">전체</span>
							<span data-lv="2">전문가</span>
						</p>
					</label>
				</div>
			</div>
			<ul class="list_reply"></ul>
			<button class="btn_more_reply" aria-label="댓글 더보기" id="moreBtn"></button>
		</div>
	</div>
	<!-- [End] App Main -->

	<!-- [Start] App Bottom -->
	<div class="app_bottom">
			<div class="writing_area">
<?php if(!$this->session->userdata('UNO')){ ?>
				<div class="<?=(!$this->session->userdata('UNO'))?' writing_box_disabled' : 'writing_box'; ?>">
					<textarea name="reply" id="reply" data-idx="<?=$view['nSeqNo']?>" placeholder="로그인 후 입력 가능합니다." readonly="readonly"></textarea>
					<button type="button" id="addReply">댓글등록</button>
<?php } else { ?>
			<?if($this->session->userdata("ULV")){?>
				<div class="" style="padding:20px 0px; border:none;">
					<script src="/public/smarteditor2/js/service/HuskyEZCreator.js"></script>
					<script>var g5_editor_url = "/public/smarteditor2", oEditors = [], ed_nonce = "dhHcWl0suO|1593457283|d96b66cdfb538c66cc7d47f7ca87b3e0eeadacd1";</script>
					<script src="/public/smarteditor2/m.config.js"></script>
					<textarea name="reply" id="reply" data-idx="<?=$view['nSeqNo']?>" placeholder="댓글을 입력해주세요" class="smarteditor2" style="width:100%;"></textarea>
					<button type="button" id="addReply" style="width: 100%; font-size: 14px;color: #fff;background-color: #999; float:left; height: 30px; transition: all 0.2s ease-in-out;">댓글등록</button>
			<?php }else{ ?>
				<div class="<?=(!$this->session->userdata('UNO'))?' writing_box_disabled' : 'writing_box'; ?>">
					<textarea name="reply" id="reply" data-idx="<?=$view['nSeqNo']?>" placeholder="댓글을 입력해주세요" ></textarea>
					<button type="button" id="addReply">댓글등록</button>
			<?php }?>
<?php } ?>
				</div>
	</div>

	<!-- [Start] SNS Share -->
	<div class="share_container">
		<div class="layout_center">
			<h4>친구에게 공유해볼까요?</h4>
			<button type="button" class="btnFcb">페이스북 공유하기</button>
			<button type="button" class="btnKtk">카카오톡 공유하기</button>
			<button type="button" class="btnMsg">메세지 공유하기</button>
			<button type="button" class="btnUrl">URL 복사하기</button>
		</div>
		<button class="btn_close" aria-label="공유하기 닫기"></button>
	</div>
	<!-- [End] SNS Share -->

	<!-- gnb -->
	<?php include_once(VIEW_PATH.'/include/gnb.php'); ?>
<!-- [End] App Bottom -->
</div>


<textarea id="NeisTextArea" style="position:absolute; left:-9999px;"></textarea>

<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>
<script type="text/JavaScript" src="https://developers.kakao.com/sdk/js/kakao.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.6/clipboard.min.js"></script>

<!-- In Script -->
<script>
Kakao.init("7b111ba912ef21c93ce5c281b6d40f62");      // 사용할 앱의 JavaScript 키를 설정
Kakao.isInitialized();

$(document).ready(function(){

	var start_record = 0;
	var countLike = <?=$view['nLike']?>;
	var boardNo = <?=$view['nSeqNo']?>;
	var boardType = 'community';
	var isLike = Cookie.get('like_board_'+boardNo);
	var order = 'new';
	var level = '1';
	if(isLike) $('.btn_like, .btnLike').addClass('active'); // 좋아요 활성화 
	replyList(0, order, level); // 처음댓글 가져오기 

	var isScrap = '<?=$isScrap?>';
	if(isScrap == 'Y') $('.btnBmk').addClass('active'); // 좋아요 활성화 

	// 최신, 인기순  
	$('#order').on('click', function() {
		if( 'new' == order ) order = 'like';
		else order = 'new';
		replyList(0, order, level);
	});

	// 전체, 전문가  
	$('#level').on('click', function() {
		if( '1' == level ) level = '2';
		else level = '1';
		replyList(0, order, level);
	});

	// 좋아요 버튼 클릭  
	$('.btnLike, .btn_like').on('click', function(){	
		if(isLike){ 
			Cmmn.alertMsg('이미 좋아요한 상태입니다.'); 
			return false; 
		}
		$.ajax({
			type : "post",
			url : "/info/likeCheck",
			dataType : 'json',
			data : { 'idx': $('#reply').data('idx') },
			success : function(response) {
				console.log(response)
				//Cmmn.log('좋아요 클릭 이벤트');
				if( response.result == 'success')  {
					//$('.btnLike').toggleClass('active');
					$('.btnLike').text(countLike+1);
					$('#countLike').text(countLike+1);
					$(".btn_like").addClass("active")
					$(".btnLike").addClass("active")
					isLike = Cookie.get('like_board_'+boardNo);
					//Cmmn.log('좋아요 수 : '+countLike);
				}
				if( response.result == 'error' ) Cmmn.alertMsg(response.msg);	
			},
			error : function(xhr, status, error) {},
		})
	});

	// FaceBook 공유
	$(".btnFcb").click(function() {
		var deviceType = localStorage.getItem("deviceType");
		// 댓글 가져오기 
		$.ajax({
			type : "post",
			url : "/info/boardShare",
			dataType : 'json',
			data : { 'boardNo': boardNo, 'type' : 'facebook' },
			success : function(response) {
				Cmmn.log('페이스북 공유하기');
				if( response.result == 'error' ) {  
					Cmmn.alertMsg(response.msg);
					return false;
				}
				var fb_url = "https://www.facebook.com/sharer/sharer.php?u="+window.location.href;
				if(deviceType == "iOS"){
					cordova.InAppBrowser.open(fb_url, '_system', 'location=no');
				} else {
					window.open(fb_url);
				}
			},
			error : function(xhr, status, error) {},
		})
	});

	$(".btnKtk").click(function() {
//		alert('kakao start');
		var deviceType = localStorage.getItem("deviceType");
//		alert(deviceType)
		$.ajax({
			type : "post",
			url : "/info/boardShare",
			dataType : 'json',
			data : { 'boardNo': boardNo, 'type' : 'kakao' },
			success : function(response) {
				if( response.result == 'error' ) {  
					alert(response.msg)
					return false;
				}
				if(deviceType == "Android"){
					var url = window.location.href;
					var feedLink = { webURL: url }
					var feedSocial = {
						likeCount:0,
						commentCount:0,
						sharedCount:0
					}
					var feedButtons1 = { title: '게시글 확인하기', link: { mobileWebURL: url } }
					var feedButtons2 = { title: '게시글 확인하기', link: { iosExecutionParams: 'param1=value1&param2=value2', androidExecutionParams: 'param1=value1&param2=value2', } }
					var feedContent = {
						title:"<?=addslashes($view['vSubject']);?>",
						description:"<?=SITE_TITLE?>",
						link: feedLink,
						imageURL: "<?=($thumNailImage != null) ? $thumNailImage : ''; ?>"
					};
					var feedTemplate = { content: feedContent, social: feedSocial, buttons: [feedButtons1] };
					KakaoTalk.share(feedTemplate, function (success) {
						/**
						*/
					}, function (error) {
						//alert(error)
					});
				} else if(deviceType == "iOS"){
					var url = window.location.href;
					var imgUrl = '<?=($thumNailImage != null) ? $thumNailImage : ''; ?>';
					imgUrl = encodeURI(imgUrl);
					var feedLink = { webURL: url }
					var feedSocial = { likeCount: 0 }
					var feedButtons1 = { title: '게시글 확인하기', link: { mobileWebURL: url } }
					var feedButtons2 = { title: '게시글 확인하기', link: { iosExecutionParams: 'param1=value1&param2=value2', androidExecutionParams: 'param1=value1&param2=value2', } }
					var feedContent = {
						title:"<?=addslashes($view['vSubject']);?>",
						link: feedLink,
						imageURL: imgUrl
					};
					var feedTemplate = { content: feedContent, social: feedSocial, buttons: [feedButtons1] };
					KakaoCordovaSDK.sendLinkFeed(feedTemplate, function (success) {
						// alert('kakao share success');
					}, function (error) {
						alert(JSON.stringify(error)) ;
					});
				} else {
					Kakao.Link.sendDefault({
						objectType:"feed", 
						content : {
							title:"<?=addslashes($view['vSubject']);?>",
							description:"<?=SITE_TITLE?>",
							imageUrl:"<?=($thumNailImage != null) ? $thumNailImage : 'illidan_stormrage.jpg' ?>",
							link : {
								mobileWebUrl:window.location.href,
								webUrl:window.location.href
							}
						},
						social : {
							likeCount:0,
							commentCount:0,
							sharedCount:0
						},
						buttons : [{
							title:"게시글 확인하기",
							link : {
								mobileWebUrl:window.location.href,
								webUrl:window.location.href // PC버전 카카오톡에서 사용하는 웹 링크 URL
							}
						}]
					});
				}
			},
			error : function(xhr, status, error) {},
		})
	});

	// 문자 공유
	$(".btnMsg").click(function() {
		var deviceType = localStorage.getItem("deviceType");
		$.ajax({
			type : "post",
			url : "/info/boardShare",
			dataType : 'json',
			data : { 'boardNo': boardNo, 'type' : 'sms' },
			success : function(response) {
				if( response.result == 'error' ) {  
					Cmmn.alertMsg(response.msg);
					return false;
				}
				var sms_url = 'sms:?body=[노다지 주식정보의 수익이 나는 콘텐츠!] 링크 : '+window.location.href;
				if(deviceType == "iOS"){
					var sms_url = 'sms:&body=[노다지 주식정보의 수익이 나는 콘텐츠!] 링크 : '+window.location.href;
					sms_url = encodeURI(sms_url)
					//alert(sms_url)
					cordova.InAppBrowser.open(sms_url, '_system', 'location=no');
				}else{
					location.href = sms_url;
				}
			},
			error : function(xhr, status, error) {},
		})
	});

	// 스크랩 버튼 클릭
	$(".btnBmk").off("click").on("click", function(){
<?php if(!$this->session->userdata('UNO')){ ?>
		location.href = "/auth";
<?php } else {  ?>
		var u = "/info/boardScrap";
		var data = {
			nBoardNo : boardNo,
			vType : boardType
		}
		$.ajax({
			type : "post",
			url : u,
			dataType : 'json',
			data : data,
			success : function(response) {
				if(response.status == "SUCCESS"){
					// 스크랩 완료
					$(".btnBmk").text(response.cnt.nScrap);
					$(".btnBmk").addClass('active');
				}else{
				// 스크랩 실패
					alert(response.msg);
				}
			}
		});
<?php } ?>
	});

	$(".btnUrl").off("click").on("click", function(){
		var deviceType = localStorage.getItem("deviceType");
		$.ajax({
			type : "post",
			url : "/info/boardShare",
			dataType : 'json',
			data : { 'boardNo': boardNo, 'type' : 'url' },
			success : function(response) {
				var target = $(this).attr("target-id");
				var text = window.location.href;
				$("#NeisTextArea").val(text);
				$("#NeisTextArea").select();
				if(deviceType == "iOS") {
					//alert(document.execCommand('copy'))
					//var text = "Hello World!";
					cordova.plugins.clipboard.copy(text);
					cordova.plugins.clipboard.paste(function (text) {
							alert("보시고 계신 페이지의 URL이 복사되었습니다.\n\n붙여넣기를 통해  공유 해주세요.");
					});
					//cordova.plugins.clipboard.clear();
				}
				else{

					try {
						var successful = document.execCommand('copy');
					}
					catch (err) {
						alert('이 브라우저는 지원하지 않습니다.') 
					}

					// 액션이 있으면
					if (text == "") {
						alert("복사할 내용이 없습니다.");
						$("#"+target).focus();
						return false;
					} else {
						if (successful == true && text != "") {
							alert("보시고 계신 페이지의 URL이 복사되었습니다.\n\n붙여넣기를 통해  공유 해주세요.");
						}
					}
				}
			}
		});
	});

	// 더보기 버튼 5개씩 가져오기 
	$("#moreBtn").click(function(){
		start_record = start_record + 5;
		replyList(start_record, order, level);
	});

<?php if(!$this->session->userdata('UNO')){ ?> 
	$("#reply").click(function(){     
		window.location.href = "/auth";
	});
<?php }?>
	// 댓글 등록 
	$("#addReply").click(function(){     
<?php if(!$this->session->userdata('UNO')){ ?> 
		window.location.href = "/auth";
<?php } else { ?>
	<?if($this->session->userdata("ULV")==10){?>
		
		var txContent_editor_data = oEditors.getById['reply'].getIR();
		oEditors.getById['reply'].exec('UPDATE_CONTENTS_FIELD', []);
		if(jQuery.inArray(document.getElementById('reply').value.toLowerCase().replace(/^\s*|\s*$/g, ''), ['&nbsp;','<p>&nbsp;</p>','<p><br></p>','<div><br></div>','<p></p>','<br>','']) != -1){document.getElementById('reply').value='';}
		if (!txContent_editor_data || jQuery.inArray(txContent_editor_data.toLowerCase(), ['&nbsp;','<p>&nbsp;</p>','<p><br></p>','<p></p>','<br>']) != -1) { alert("내용을 입력해 주십시오."); oEditors.getById['reply'].exec('FOCUS'); return false; }

		var data = {
			'reply': $('#reply').val()
			, 'idx': $('#reply').data('idx')
		}

	<?} else {?>
		var data = {
			'reply': $('#reply').val()
			, 'idx': $('#reply').data('idx')
		}
	<?} ?>

		$.ajax({
			type : "post",
			url : "/info/addReply",
			dataType : 'json',
			data : data,
			success : function(response) {
			// Cmmn.log(response);
				if( response.result == "error_lg"){
					$(".lg").click();
				}
				if( response.result == 'error' ) {  
					Cmmn.alertMsg(response.msg);
					$('.app_bottom').removeClass('hide');
				}
				if( response.result == 'success') {
					Cmmn.alertMsg(response.msg);
					replyList(0, order, level);
					// 초기화
					$('#reply').val('');
					start_record = 0;
					$('.writing_box').removeClass('focused');
					$('.app_bottom').removeClass('hide');
				}
			},
			error : function(xhr, status, error) {},
		})
<?php } ?>
	});

	// 삭제버튼
	$("#delBtn").click(function(){
		if( confirm('정말 삭제하겠습니까?') == true ){
			$.ajax({
				type : "post",
				url : "/info/delCommunity",
				dataType : 'json',
				data : { 'idx': <?=$view['nSeqNo'];?> },
				success : function(response) {
					if( response.result == 'success' ) {
						location.replace(response.url);
					}
				},
				error : function(xhr, status, error) {
					console.log(xhr,error);
				},
			})
		}
	});


	// 삭제버튼
	$("#modifyBtn").click(function(){
		location.href='/community/reg/'+boardNo;
	});
});


function reply_html(idx, nick, regDate, reply, like, img, lv) {
	var html = "";
	var isReplylike = Cookie.get('replyLike_'+idx);
	var path = '';
	var profile_state = '';
	var profile_tag = '';

	if(!img) path = '../../asset/img/user_default.png';
	else path = '/data/user/profile/'+img;

	if(lv == 2){
		profile_state = 'expert';
		profile_tag = '<em>주식전문가</em>';
	} 

	//if(start_record>=0 && start_record<3)
	html +="<li><div class=\"profile_img %%LV%%\">";//
	html +="    <div style=\"background-image: url('%%IMG%%');\"></div>";
	html +="  </div>";
	html +="  <div class=\"info\">";
	html +="    <span class=\"nickname\">"+profile_tag+"%%NICK%%</span>";
	html +="    <span class=\"time\">%%REGDATE%%</span>";
	html +="    <button type=\"button\" class=\"btn_like %%STATE%%\" id=\"reply_id_%%NO%%\" onclick=\"replyLikeCount('%%IDX%%')\">%%LIKE%%</button>";
	html +="  </div>";
	html +="  <p>%%REPLY%%</p>";
	html +="</li>";

	html = html.replace("%%NO%%", idx);
	html = html.replace("%%IDX%%", idx);
	html = html.replace("%%NICK%%", nick);
	html = html.replace("%%REGDATE%%", regDate);
	html = html.replace("%%REPLY%%", nl2br(reply));
	html = html.replace("%%LIKE%%", like);
	html = html.replace("%%IMG%%", path);
	html = html.replace("%%LV%%", profile_state);

	if(isReplylike) html = html.replace("%%STATE%%", 'active');

	return html;
}

function nl2br(str){  
	return str.replace(/\n/g, "<br />");  
}


function replyList(start_record, orderby, level) {
	// 댓글 가져오기 
	console.log(start_record);

	var data = {
			'idx': $('#reply').data('idx'),
			'start_record' : start_record,
			'orderby' : orderby,
			'level' : level
	}
		console.log(data);

	$.ajax({
		type : "post",
		url : "/info/getReplyList",
		dataType : 'json',
		data : data,
		success : function(response) {
			Cmmn.log(response);
			if( response.result == 'error' ) {  
				if( start_record != 0 ) Cmmn.alertMsg(response.msg);
				//$('.list_reply').html('');
			}
			if( response.result == 'success') {
				var html = '';
				$.each(response.list, function (index, item) {
					html += reply_html(item.nSeqNo, item.vNick, item.dtRegDate, item.txComment, item.nLike, item.vImage, item.nLevel);
				})
				if( start_record == 0 ) $('.list_reply').html(html);
				else $('.list_reply').append(html); 
			}
		},
		error : function(xhr, status, error) {},
	})
}

function replyLikeCount(idx) {
	var isReplylike = Cookie.get('replyLike_'+idx);
	if(isReplylike) {
		Cmmn.alertMsg('이미 좋아요한 상태입니다.'); return false;
	}

	// 댓글 가져오기 
	$.ajax({
		type : "post",
		url : "/info/replyLikeCheck",
		dataType : 'json',
		data : { 'idx': idx },
		success : function(response) {
			Cmmn.log(response);
			if( response.result == 'success') {
				location.reload();
			}
			if( response.result == 'error' ) Cmmn.alertMsg(response.msg);
		},
		error : function(xhr, status, error) {},
	})
}
</script>
