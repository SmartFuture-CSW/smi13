<div class="app_wrapper">
<!-- [Start] App Header -->
<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
<!-- [End] App Header -->
<!-- [Start] App Main -->
<div class="app_main">
	<div class="psuedo_container">
		<div class="search_container layout_center">
			<div class="search_box">
				<form action="/info/search" method="get">
					<input type="search" name="keyword" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
					<button type="submit" aria-label="검색"></button>
				</form>
			</div>
		</div>
		<!-- lnb --><? include_once(VIEW_PATH.'/include/lnb.php'); ?>
	</div>


	<div class="category_container ">
		<div class="layout_center">
		<?php foreach($category as $row){?>
			<a href="/info/<?=$row['vType']?>/<?=$row['nSeqNo']?>" <?=($nCategoryNo == $row['nSeqNo']) ? 'class="active"' : ''; ?>><?=$row['vSubject']?></a>
		<?php }?>
			<a href="/community" class="link">주식질문</a>
		</div>
	</div>

	<section>
		<h1 class="a11y_hidden">주식공부</h1>
		<div class="essential_list">
<?  
foreach ($study as $key => $value) { 
	$imgPath = '/data/board/thumb/'.$value['vImage'];
?>
			<a class="clearfix" href="/boardView/study/<?=$value['nSeqNo']?>">
				<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
				<div class="info">
					<h5><?=$value['vSubject']?></h5>
					<p>
						<span><?=$value['dtRegDate']?></span>
						<span>조회수 <?=$value['nHit']?>회</span>
						<span>공유수 <?=$value['nShare']?>회</span>
					</p>
				</div>
			</a>
<?  } ?>
		</div>
	</section>

	<?//<div id="text" style="position:fixed; top:10px; width:500px; height:80px; background-color:white; z-index:9999;"></div>/?>

	<div class="banner_type1">
	<? foreach ($banner as $key => $value) { ?>
		<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>    
	<?}?>
	</div>

	<section class="investment_information layout_center infinity" data-page="<?=$infinity_page?>">
		<h2>주식공부</h2>
		<div class="list_article clearfix">
<?
foreach ($infinity as $key => $value) { 
	$imgPath = '/data/board/thumb/'.$value['vImage'];
?>
			<a href="/boardView/<?=$value['vType']?>/<?=$value['nSeqNo']?>">
				<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
				<p><?=$value['vSubject']?></p>
			</a>
<?  } ?>
		</div>
	</section>

	<!-- [Start] Popup - Advertisement -->
	<?if(!empty($popup)){ ?>
	<div class="remodal advertisement" data-remodal-id="pop_ad">
		<button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
		<a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
	</div>
	<?}?>
	<!-- [End] Popup - Advertisement -->

	<!-- [Start] App Bottom -->
	<div class="app_bottom"><? include_once(VIEW_PATH.'/include/gnb.php'); ?></div>
	<!-- [End] App Bottom -->
</div>


<!-- In Script -->
<script>
(function(){
	/* Advertisement Slider */
	/*$('.banner_type1').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		infinite: true,
		speed: 500,
		dots: true,
		arrows: false
	});*/

	<?if(!empty($popup)){ ?>
		$('[data-remodal-id=pop_ad]').remodal().open();
	<?}?>


	/**
	* @brief : inifnity scroll
	* @author : csw
	*/
	var busy = false;
	$(window).scroll(function() {
		var winHeight = parseInt($(this).height(), 10);
		var docHeight = parseInt($(document).height(), 10);
		var scrTop = parseInt($(this).scrollTop(), 10);
		if( (docHeight - winHeight - 150) <= scrTop ){

			if(busy){
				return;
			}
			busy = true;

			// 다음에 보여줄 리스트 출력
			var type = 'study';
			var page = $(".infinity").data('page');
			$(".infinity").data('page', page+1);
			page = $(".infinity").data('page');
			var data = {
				type : type,
				page : page,
				category : '<?=$nCategoryNo?>',
				mainVal :  8
			}
			$.post("/info/get_infinity_data", data ,function(response){
				//console.log(response)
				if(response.status == "SUCCESS"){
					$.each(response.list, function(idx, row){
						var anchor = $('<a />', {
							href : '/boardView/' + row.vType + '/' + row.nSeqNo
						});
						var contents = $('<div />', {
							'class' : 'thumbnail',
							style : 'background-image:url(\'/data/board/thumb/' + row.vImage + '\')'
						});
						anchor.append(contents);
						var subject = $('<p />', {
							text : row.vSubject
						});
						anchor.append(subject);
						$(".infinity > .list_article").append(anchor)
						busy = false;
					})
				}
			});
		}
	});
})();
</script>