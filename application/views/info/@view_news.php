<div class="app_wrapper">
	<!-- [Start] App Header -->
	<header class="app_header">
		<div class="back">
			<a href="javascript:window.history.back(-2)" aria-label="뒤로가기"></a>
			<h2><?=$view['vSubject'];?></h2>
		</div>
	</header>
	<!-- [End] App Header -->
	
    <!-- [Start] App Main -->
    <div class="app_main">

    	<? $view['vNewsLink'] = (empty($view['vNewsLink'])) ? '/':$view['vNewsLink']; ?>
    	<iframe src="<?=$view['vNewsLink']?>" class="iframe_news"></iframe>
      
    </div>
    <!-- [End] App Main -->

    <!-- [Start] App Bottom -->
    <div class="app_bottom"><? include_once(VIEW_PATH.'/include/gnb.php'); ?></div>
  </div>