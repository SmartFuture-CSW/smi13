<? //print_r($view); ?>
<style>
.list_reply img{width:100%;}
</style>
<div class="app_wrapper">
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="javascript:window.history.back(-1)" aria-label="뒤로가기"></a>
				<h2><?=$view['vSubject'];?></h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main is_reply">
			<div class="post_header">
				<h1><?=$view['vSubject'];?></h1>
				<div class="sub_info clearfix">
					<p><?=$view['dtRegDate'];?><span>조회수 <?=$view['nHit'];?> 회</span></p>
					<div class="sns_share">
						<button type="button" class="btn_fcb" aria-label="페이스북 공유하기"></button>
						<button type="button" class="btn_kko" aria-label="카카오톡 공유하기"></button>
						<button type="button" class="btn_msg" aria-label="메세지 공유하기"></button>
					</div> 
				</div>
			</div>

			<div class="reply_container">
				<ul class="list_reply"></ul>
				<button class="btn_more_reply" aria-label="댓글 더보기" id="moreBtn"></button>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="writing_area">
				<div class="<?=(!$this->session->userdata('UNO'))?' writing_box_disabled' : 'writing_box'; ?>">
<?php if(!$this->session->userdata('UNO')){ ?>
					<textarea name="reply" id="reply" placeholder="로그인 후 입력 가능합니다." readonly="readonly"></textarea>
					<button type="submit" id="addReply">댓글등록</button>
<?php } else {  ?>
					<textarea name="reply" id="reply" placeholder="댓글을 입력해주세요"></textarea>
					<button type="submit" id="addReply">댓글등록</button>
<?php } ?>
				</div>
			</div>

			
		</div>
		<!-- [End] App Bottom -->
	</div>

	<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>

<script type="text/javascript">
	$( document ).ready(function() {
		var start_record = 0;

		replyList(0); // 처음댓글 가져오기

		// 더보기 버튼 5개씩 가져오기 
		$("#moreBtn").click(function(){
			start_record = start_record + 5;
			replyList(start_record);
		});

<?php if(!$this->session->userdata('UNO')){ ?> 
	$("#reply").click(function(){     
		window.location.href = "/auth";
	});
<?php }?>
		// 댓글 등록 
		$("#addReply").click(function(){
<?php if(!$this->session->userdata('UNO')){ ?>
			location.href = "/auth";
<?php } else { ?>
			$.ajax({
				type : "post",
				url : "/info/addReply",
				dataType : 'json',
				data : { 'reply': $('#reply').val(), 'idx': '<?=$view['nSeqNo']?>' },
				success : function(response) {
					Cmmn.log(response);
					if( response.result == 'error' ) {  
						Cmmn.alertMsg(response.msg);
						$('.app_bottom').removeClass('hide');
					}
					if( response.result == 'success') {
						Cmmn.alertMsg(response.msg);
						replyList(0);
						// 초기화
						$('#reply').val('');
						start_record = 0;
						$('.writing_box').removeClass('focused');
						$('.app_bottom').removeClass('hide');
					}
				},
				error : function(xhr, status, error) {},
			});
<?php } ?>
		});
		
		// FaceBook 공유
			$(".btn_fcb").click(function() {
				// 댓글 가져오기 
				$.ajax({
					type : "post",
					url : "/info/boardShare",
					dataType : 'json',
					data : { 'boardNo': '<?=$view['nSeqNo']?>', 'type' : 'facebook' },

					success : function(response) {
						Cmmn.log('페이스북 공유하기');
						if( response.result == 'error' ) {  
							Cmmn.alertMsg(response.msg);
							return false;
						}
						console.log(response);
						window.open("http://www.facebook.com/sharer/sharer.php?u="+window.location.href);
					},
					error : function(xhr, status, error) {},
				})
				
			});
		    
		    $(".btn_kko").click(function()
		    {
				$.ajax({
	                type : "post",
	                url : "/info/boardShare",
	                dataType : 'json',
	                data : { 'boardNo': '<?=$view['nSeqNo']?>', 'type' : 'kakao' },

	                success : function(response) {
	                	
	                	Cmmn.log('페이스북 공유하기');

	                    if( response.result == 'error' ) {  
	                       Cmmn.alertMsg(response.msg);
	                       return false;
	                    }
	                    console.log(response);
	                    Kakao.init("7b111ba912ef21c93ce5c281b6d40f62");      // 사용할 앱의 JavaScript 키를 설정
				        Kakao.Link.sendDefault({
				            objectType:"feed", 
				            content : {
				                title:"<?=$view['vSubject'];?>",
				                description:"노다지",
				                imageUrl:"illidan_stormrage.jpg",
				                link : {
				                    mobileWebUrl:window.location.href,
				                    webUrl:window.location.href
				                }
				            },
				            social : {
				                likeCount:0,
				                commentCount:0,
				                sharedCount:0
				            },
				            buttons : [{
				                title:"게시글 확인하기",
				                link : {
				                    mobileWebUrl:window.location.href,
				                    webUrl:window.location.href // PC버전 카카오톡에서 사용하는 웹 링크 URL
				                }
				            }]
				        });

	                },

	                error : function(xhr, status, error) {},
	            })
		    	
		    });

		    // 문자 공유
		    $(".btn_msg").click(function()
		    {
				$.ajax({
	                type : "post",
	                url : "/info/boardShare",
	                dataType : 'json',
	                data : { 'boardNo': '<?=$view['nSeqNo']?>', 'type' : 'sms' },

	                success : function(response) {
	                	
	                	Cmmn.log('페이스북 공유하기');

	                    if( response.result == 'error' ) {  
	                       Cmmn.alertMsg(response.msg);
	                       return false;
	                    }
	                    console.log(response);
	                    location.href = 'sms:?body=[노다주식정보의 수익이 나는 콘텐츠!] 링크 : '+window.location.href;

	                },
	                error : function(xhr, status, error) {},
	            })
		    	
		    });

	});



	function reply_html(idx, nick, regDate, reply, like, img, lv)
	{
	    var html = "";
		var isReplylike = Cookie.get('replyLike_'+idx);
		var path = '';
		var profile_state = '';
        var profile_tag = '';
		
		if(!img) path = '../../asset/img/user_default.png';
		else path = '/data/user/profile/'+img;

		if(lv == 2){
        	profile_state = 'expert';
        	profile_tag = '<em>주식전문가</em>';
        }

		html +="<li><div class=\"profile_img %%LV%%\">";//
		html +="		<div style=\"background-image: url('%%IMG%%');\"></div>";
	    html +="	</div>";
	    html +="	<div class=\"info\">";
	    html +="    <span class=\"nickname\">"+profile_tag+"%%NICK%%</span>";
	    html +="		<span class=\"time\">%%REGDATE%%</span>";
	    html +="		<button type=\"button\" class=\"btn_like %%STATE%%\" id=\"reply_id_%%NO%%\" onclick=\"replyLikeCount('%%IDX%%')\">%%LIKE%%</button>";
	    html +="	</div>";
	  	html +="	<p>%%REPLY%%</p>";
	    html +="</li>";

	    html = html.replace("%%NO%%", idx);
    	html = html.replace("%%IDX%%", idx);
	    html = html.replace("%%NICK%%", nick);
	    html = html.replace("%%REGDATE%%", regDate);
	    html = html.replace("%%REPLY%%", reply);
		html = html.replace("%%LIKE%%", like);
		html = html.replace("%%IMG%%", path);
		html = html.replace("%%LV%%", profile_state);

	    if(isReplylike) html = html.replace("%%STATE%%", 'active');

	    return html;
	}

	function replyList(start_record)
	{
		// 댓글 가져오기 
		$.ajax({
            type : "post",
            url : "/info/getReplyList",
            dataType : 'json',
            data : { 'idx': '<?=$view['nSeqNo']?>', 'start_record' : start_record, 'orderby' : 'new' },

            success : function(response) {
            	
            	Cmmn.log('onload event 댓글리스트가져오기');

                if( response.result == 'error' ) {  
                   	if( start_record != 0 ) Cmmn.alertMsg(response.msg);
                }

                if( response.result == 'success') {
                    
                	var html = '';
                	
                	$.each(response.list, function (index, item) {
                		html += reply_html(item.nSeqNo, item.vNick, item.dtRegDate, item.txComment, item.nLike, item.vImage, item.nLevel);
                	})

                	if( start_record == 0 ) $('.list_reply').html(html);
                	else $('.list_reply').append(html); 
					

                }

            },

            error : function(xhr, status, error) {},
        })
	}

	function replyLikeCount(idx)
	    {
	    	var isReplylike = Cookie.get('replyLike_'+idx);

			if(isReplylike) {
				Cmmn.alertMsg('이미 좋아요한 상태입니다.'); return false;
			}

	        // 댓글 가져오기 
	      	$.ajax({
		        type : "post",
		        url : "/info/replyLikeCheck",
		        dataType : 'json',
		        data : { 'idx': idx },

		        success : function(response) {
		          
		          	Cmmn.log(response);

		            if( response.result == 'success') {
		            	location.reload();
		            }

		            if( response.result == 'error' ) Cmmn.alertMsg(response.msg);


		        },

		        error : function(xhr, status, error) {},
		    })

	    }
</script>