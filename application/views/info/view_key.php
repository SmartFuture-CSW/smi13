<?php

//$view['subject'] = str_replace("\"", "", $view['subject']);
$sns_title = preg_replace("/[\"\']/i", "&quot;", $view['vSubject']);
$thumNailImage = SITE_URL.'/data/board/thumb/'.$view['vImage'];
?>
<style>
.loading_mask img{width:6em;}
.loading_mask{
position:absolute;
top:50%; left:50%;
margin-left:-3em;
margin-top:-3em;
width:6em;
}
</style>


<div class="app_wrapper">
	<!-- [Start] App Header -->
	<header class="app_header type02">
		<a href="/info/today" class="btn_back" aria-label="뒤로가기"></a>
	</header>
	<!-- [End] App Header -->
		
	<!-- [Start] App Main -->
	<div class="app_main has_bottom_banner">
		<!-- .has_bottom_banner 클래스는 하단 GNB에 배너가 있을 경우에 .app_main엘리먼트에 추가되는 클래스 입니다. 정기구독중인 사용자에게는 이 클래스가 제거되어 보여야 합니다 -->
		<div class="post_header">
			<h1><?=$view['vSubject'];?></h1>
			<div class="sub_info clearfix">
				<p><?=$view['dtRegDate'];?> <span>조회수 <?=$view['nHit'];?>회</span></p>
			</div>
		</div>

		<div class="post_contents" style="border:none;">
			<div class="writing_container ">
<?php
$thumNailImage = null;
if($view['vImage'] != ""){
	$thumNailImage = SITE_URL.'/data/board/thumb/'.$view['vImage'];
}
?>
				<?=$view['txContent'];?>
			</div>
			
			
		</div>



	</div>
	<!-- [End] App Main -->

	<!-- [Start] App Bottom -->
	<div class="app_bottom">	
		<? include_once(VIEW_PATH.'/include/gnb.php'); ?>
	</div>

</div>

<script>
$(function(){
	$("body").height($(window).height()-56);
});
</script>
