

	<div class="app_wrapper">
		
		<!-- [Start] App Header -->
		<header class="app_header">
			<div class="back">
				<a href="javascript:window.history.back(-2)" aria-label="뒤로가기"></a>
				<h2><?=$view['vSubject'];?></h2>
			</div>
		</header>
		<!-- [End] App Header -->

		<!-- [Start] App Main -->
		<div class="app_main has_bottom_banner">

			<div class="board_header">
				<h2 class="new"><?=$view['vSubject'];?></h2>
				<p>
					<span>조회수 <?=$view['nHit'];?></span>
					<span><?=$view['dtRegDate'];?></span>
				</p>
			</div>

			<div class="board_content"><?=$view['txContent'];?></div>
			
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<div class="banner_type1">
				<a href="#none"><img src="../asset/img/sample/sample_ad01.png" alt=""></a>
				<a href="#none"><img src="../asset/img/sample/sample_ad01.png" alt=""></a>
				<a href="#none"><img src="../asset/img/sample/sample_ad01.png" alt=""></a>
			</div>

			<div class="gnb">
				<div class="layout_center clearfix">
					<a href="/info" class="menu_1">공개정보</a>
					<a href="/stock" class="menu_2">추천종목</a>
					<a href="/subscribe" class="menu_3">구독</a>
					<a href="/community" class="menu_4">커뮤니티</a>
					<a href="/store" class="menu_5">스토어</a>
				</div>
			</div>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<!-- In Script -->
	<script>
		(function(){
			/* Body background-color change */
			$('body').css('background-color', '#fff');

		})();
	</script>