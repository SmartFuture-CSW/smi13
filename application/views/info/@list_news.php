

  <div class="app_wrapper">
    
    <!-- [Start] App Header -->
    <? include_once(VIEW_PATH.'/include/header_app.php'); ?>
    <!-- [End] App Header -->

    <!-- [Start] App Main -->
    <div class="app_main">
      <div class="search_container layout_center">
        <div class="search_box">
          <form action="/info/search" method="get">
          <input type="search" name="keyword" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
          <button type="submit" aria-label="검색"></button>
        </form>
        </div>
      </div>

      <!-- lnb --><? include_once(VIEW_PATH.'/include/lnb.php'); ?>

      <div class="banner_type1 gnb_banner">
        <? foreach ($banner as $key => $value) { ?>
          <a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>    
        <?}?>
      </div>

      <div class="news_list layout_center">
  <?  foreach ($news as $key => $value) { ?> 
        <article>
          <h4><?=$value['vSubject']?></h4>
          <?=$value['txContent']?>
          <!-- <ul class="list_type01">
            <li>국무회의서 예비비255억 지원 의결... 지방비 108억 추가</li>
            <li>멧돼지 이동 차단 울타리 설치... 포획 활동 강화에 지출</li>
          </ul>
          <section>
            <h5>아프리카 돼지 열병 테마</h5>
            <ul>
              <li class="emphasis">[VIP회원 단독공개]</li>
              <li>이글벳(044960)</li>
            </ul>
          </section> -->
          <div class="btn_container clearfix">
            <a href="<?=$value['vEventLink']?>">VIP 종목 확인하기</a>
            <a href="/boardView/news/<?=$value['nSeqNo']?>">기사원문보기</a>
          </div>
        </article>
  <?  } ?>
      </div>

    </div>
    <!-- [End] App Main -->

    <!-- [Start] Popup - Advertisement -->
    <?if(!empty($popup)){ ?>
        <div class="remodal advertisement" data-remodal-id="pop_ad">
            <button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
            <a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
        </div>
    <?}?>
    <!-- [End] Popup - Advertisement -->

    <!-- [Start] App Bottom -->
    <div class="app_bottom"><? include_once(VIEW_PATH.'/include/gnb.php'); ?></div>
</div>

<script>
    (function(){
      /* Advertisement Slider */
      /*$('.banner_type1').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        infinite: true,
        speed: 500,
        arrows: false,
      });
	  */

     <?if(!empty($popup)){ ?>
        $('[data-remodal-id=pop_ad]').remodal().open();
    <?}?>
      
    })();
  </script>
