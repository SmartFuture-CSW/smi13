<div class="app_wrapper">
	<!-- [Start] App Header -->
	<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
	<!-- [End] App Header -->
	<!-- [Start] App Main -->
	<div class="app_main">
		<div class="search_container layout_center">
			<div class="search_box">
	<form action="/info/search" method="get">
		<input type="search" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
				<button type="submit" aria-label="검색"></button>
	</form>
			</div>
		</div>
		<h1 class="a11y_hidden">커뮤니티</h1>
		<div class="category_container ">
			<div class="layout_center">
			<?php
			foreach($category as $row){
				$arrCategory[$row['nSeqNo']] = $row['vSubject'];
			?>
				<a href="/info/<?=$row['vType']?>/<?=$row['nSeqNo']?>" <?=($nCategoryNo == $row['nSeqNo']) ? 'class="active"' : ''; ?>><?=$row['vSubject']?></a>
			<?php }?>
			</div>
		</div>

		<div class="community_container layout_center">
			<strong class="top_notice ellipsis">욕설, 비방 선정적인 내용은 삭제될 수 있습니다</strong>
			<ul class="community_list">
<?php
foreach ($community as $key => $value) { 
	$tmp = trim($value['txTag']);
	$tag = explode(",", $tmp);
	
	
	if($value['nCategoryNo'] == "12"){
		$subject = $value['vSubject'];
		$contents = $this->util->cut_str(str_replace("&nbsp;"," ",strip_tags($value['txContent'])),80);
	}
	else{
		if($value['nUserNo'] == $this->session->userdata('UNO')){
			$subject = $value['vSubject'];
			$contents = $this->util->cut_str(str_replace("&nbsp;"," ",strip_tags($value['txContent'])),80);
		}
		else{
			$subject =  $this->util->cut_str(str_replace("&nbsp;","",strip_tags($value['vNick'])),1, "**").'님의 '.$arrCategory[$value['nCategoryNo']].' 신청 완료';
			$contents = "작성자만 확인 가능합니다.";
		}
	}

?>
				<li>
					<?php	if(in_array($value['nSeqNo'], $best)){?><span class="mk_best" aria-label="BEST"></span><?php }?>
<?php
	if(count($tag) > 1 ) { 
		echo '<p>'; for ($i=0; $i<count($tag) ; $i++) echo '<span class="tag_label">#'.$tag[$i].'</span>'; echo '</p>';
}

	
				if($value['nCategoryNo'] == "12"){
				?>
					<a href="<?='/boardView/community/'.$value['nSeqNo'];?>">
				<?
				} else{
					if($value['nUserNo'] == $this->session->userdata('UNO')){?>
					<a href="<?='/boardView/community/'.$value['nSeqNo'];?>">
					<?php } else {?>
					<a class="nonRead">
					<?php }
				}?>
						<h5><?=$subject?></h5>
						<p><?=$contents?></p>
					</a>
					<div class="bottom">
						<span class="user">조회수 <?=$value['nHit']?></span>
						<span class="like"><?=$value['nLike']?></span>
						<span class="reply"><?=$value['cnt']?></span>
					</div>
				</li>
<?php
	}
?>
			</ul>
		</div>
		<button class="writing_button" aria-label="글쓰기" onclick="javascript:location.href = '/community/reg';"></button>
	</div>
	<!-- [End] App Main -->

	<!-- [Start] Popup - Advertisement -->
<?php	if(!empty($popup)){ ?>
	<div class="remodal advertisement" data-remodal-id="pop_ad">
		<button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
		<a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
	</div>
<?php	}?>
	<!-- [End] Popup - Advertisement -->

<!-- [Start] App Bottom -->
	<div class="app_bottom"><? include_once(VIEW_PATH.'/include/gnb.php'); ?></div>
</div>

<!-- in script -->
<script type="text/javascript">
(function(){
/* Advertisement Slider */
/*    $('.banner_type1').slick({
slidesToShow: 1,
slidesToScroll: 1,
autoplay: true,
infinite: true,
speed: 500,
dots: true,
arrows: false
});*/
	<?php	if(!empty($popup)){ ?>
	$('[data-remodal-id=pop_ad]').remodal().open();
	<?}?>

		$(".nonRead").off("click").on("click", function(){
			alert("본인의 게시물만 열람 가능합니다.");
		});
})();
</script>