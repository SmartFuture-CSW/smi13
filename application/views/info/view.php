<?php

//$view['subject'] = str_replace("\"", "", $view['subject']);
$sns_title = preg_replace("/[\"\']/i", "&quot;", $view['vSubject']);
$thumNailImage = SITE_URL.'/data/board/thumb/'.$view['vImage'];
?>
<style>
.loading_mask img{width:6em;}
.loading_mask{
position:absolute;
top:50%; left:50%;
margin-left:-3em;
margin-top:-3em;
width:6em;
}
.list_reply img{ width:100%;}
</style>


<div class="app_wrapper">
	<!-- [Start] App Header -->
	<header class="app_header type02">
		<a href="/info/<?=$type?>" class="btn_back" aria-label="뒤로가기"></a>
		<div class="right_container">
			<!-- .btnLike 와 .btnBmk 에 active 클래스를 추가하면 활성화 스타일이 적용됩니다 -->
			<button class="btnLike" type="button" aria-label="좋아요"><?=$view['nLike']?></button>
			<button class="btnBmk" type="button" aria-label="북마크"><?=$view['nScrap']?></button>
			<button class="btnSns" type="button" aria-label="공유하기"><?=$view['nShare']?></button>
		</div>
	</header>
	<!-- [End] App Header -->
		
	<!-- [Start] App Main -->
	<div class="app_main ">
		<!-- .has_bottom_banner 클래스는 하단 GNB에 배너가 있을 경우에 .app_main엘리먼트에 추가되는 클래스 입니다. 정기구독중인 사용자에게는 이 클래스가 제거되어 보여야 합니다 -->
		<div class="post_header">
			<h1><?=$view['vSubject'];?></h1>
			<div class="sub_info clearfix">
				<p><?=$view['dtRegDate'];?> <span>조회수 <?=$view['nHit'];?>회</span></p>
			</div>
		</div>

		<div class="post_contents">
			<div class="loading_mask"><img src="/asset/img/loding01.gif" /></div>
			<div class="writing_container is_summarized" style="visibility:hidden">
<?php
$thumNailImage = null;
if($view['vImage'] != ""){
	$thumNailImage = SITE_URL.'/data/board/thumb/'.$view['vImage'];
}
?>
				<!-- [Start] 사용자 등록 영역 -->
				<!-- <img src="../asset/img/sample/sample_essay.jpg" alt=""> -->
				<?=$view['txContent'];?>
				<!-- [End] 사용자 등록 영역 -->
				<!--div class="banner_type1">
					<a href="https://pf.kakao.com/_ncIXK" target="_blank"><img src="/asset/img/banner_kakaoplus.jpg" alt=""></a>
				</div-->
				<button class="btn_show_all btn_l bg_red" type="button">전체보기</button>
			</div>
			
			<div class="reaction">
				<div class="btn_like" id="btn_like"><!-- .btn_like에 active 클래스를 추가하면 활성화 상태로 변합니다 -->
					<button type="button" aria-label="좋아요"></button>
					<span id="countLike"><?=$view['nLike'];?></span>
				</div>
				<div class="btn_reply">
					<a href="/info/reply/<?=$view['nSeqNo']?>" aria-label="댓글보기"></a>
					<span><?=$countReply;?></span>
				</div>
				<div class="btn_share">
					<button type="button" aria-label="공유하기"></button>
					<span><?=$view['nShare'];?></span>
					<!-- <div class="btn_container">
						<button type="button" class="btnFcb" aria-label="페이스북 공유하기"></button>
						<button type="button" class="btnKtk" aria-label="카카오톡 공유하기"></button>
						<button type="button" class="btnMsg" aria-label="메세지 공유하기"></button>
					</div> -->
				</div>
			</div>
			
		</div>

	<?  if(!empty($recom1) && !empty($recom2) && !empty($recom3)){ ?>
		
		<section class="refer_contents">
			<h3>추천콘텐츠</h3>
			<? if(!empty($recom1)){ ?><a href="/boardView/<?=$recom1['vType']?>/<?=$recom1['nSeqNo']?>"><?=$recom1['vSubject']?></a><?}?>
			<? if(!empty($recom2)){ ?><a href="/boardView/<?=$recom2['vType']?>/<?=$recom2['nSeqNo']?>"><?=$recom2['vSubject']?></a><?}?>
			<? if(!empty($recom3)){ ?><a href="/boardView/<?=$recom3['vType']?>/<?=$recom3['nSeqNo']?>"><?=$recom3['vSubject']?></a><?}?>
			<? if(!empty($recom4)){ ?><a href="/boardView/<?=$recom4['vType']?>/<?=$recom4['nSeqNo']?>"><?=$recom4['vSubject']?></a><?}?>
		</section>

	<?  } ?>

		<div id="excepted" class="banner_type1" style="margin: 10px 0;">
		<? foreach ($banner as $key => $value) { ?>
			<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
		<?}?> 
		</div>

		<div class="reply_container">
			<strong>악성, 욕설, 비방 등의 댓글은 삭제조치 할 수 있습니다</strong>

			<div class="writing_area">
<?php if(!$this->session->userdata('UNO')){ ?>
				<div class="<?=(!$this->session->userdata('UNO'))?' writing_box_disabled' : 'writing_box'; ?>">
					<textarea name="reply" id="reply" data-idx="<?=$view['nSeqNo']?>" placeholder="로그인 후 입력 가능합니다." readonly="readonly"></textarea>
					<button type="button" id="addReply">댓글등록</button>
<?php } else { ?>
			<?if($this->session->userdata("ULV") == 10){?>
				<div class="" style="padding:20px 0px; border:none;">
					<script src="/public/smarteditor2/js/service/HuskyEZCreator.js"></script>
					<script>var g5_editor_url = "/public/smarteditor2", oEditors = [], ed_nonce = "dhHcWl0suO|1593457283|d96b66cdfb538c66cc7d47f7ca87b3e0eeadacd1";</script>
					<script src="/public/smarteditor2/m.config.js"></script>
					<textarea name="reply" id="reply" data-idx="<?=$view['nSeqNo']?>" placeholder="댓글을 입력해주세요" class="smarteditor2" style="width:100%;"></textarea>
					<button type="button" id="addReply" style="width: 100%; font-size: 14px;color: #fff;background-color: #999; float:left; height: 30px; transition: all 0.2s ease-in-out;">댓글등록</button>
			<?php }else{ ?>
				<div class="<?=(!$this->session->userdata('UNO'))?' writing_box_disabled' : 'writing_box'; ?>">
					<textarea name="reply" id="reply" data-idx="<?=$view['nSeqNo']?>" placeholder="댓글을 입력해주세요" ></textarea>
					<button type="button" id="addReply">댓글등록</button>
			<?php }?>
<?php } ?>
				</div>
			</div>

			<ul class="list_reply"></ul>

			<a href="#none" class="link_more_reply" id="moreBtn"><?=$countReply;?>개의 댓글 더보기</a>
		</div>


		<section class="investment_information layout_center">
			<h2>추천종목</h2>
			<div class="list_article clearfix">
		<?php
			foreach ($etc1 as $key => $value) { 
				$imgPath = '/data/board/thumb/'.$value['vImage'];
		?>
				<a href="/boardView/<?=$value['vType']?>/<?=$value['nSeqNo']?>">
					<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
					<p><?=$value['vSubject']?></p>
				</a>
		<?php
			}
		?>

			</div>
		</section>

		<section class="investment_information layout_center">
			<h2>투자정보</h2>
			<div class="list_article clearfix">
		
		<?php
			foreach ($etc2 as $key => $value) { 
				$imgPath = '/data/board/thumb/'.$value['vImage'];
		?>
				<a href="/boardView/<?=$value['vType']?>/<?=$value['nSeqNo']?>">
					<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
					<p><?=$value['vSubject']?></p>
				</a>

		<?php  } ?>

			</div>
		</section>


		<section class="investment_information layout_center">
			<h2>주식공부</h2>
			<div class="list_article clearfix">
		
		<?php
			foreach ($etc3 as $key => $value) { 
				$imgPath = '/data/board/thumb/'.$value['vImage'];
		?>
				<a href="/boardView/<?=$value['vType']?>/<?=$value['nSeqNo']?>">
					<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
					<p><?=$value['vSubject']?></p>
				</a>

		<?php  } ?>

			</div>
		</section>


	</div>
	<!-- [End] App Main -->

	<!-- [Start] App Bottom -->
	<div class="app_bottom">
		<!-- [Start] 비구독자용 광고배너 -->
		<div class="banner_type1 gnb_banner">
			<? foreach ($banner as $key => $value) { ?>
            	<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
        	<?}?> 
		</div>
		<!-- [End] 비구독자용 광고배너 -->

		<!-- [Start] SNS Share -->
		<div class="share_container">
			<div class="layout_center">
				<h4>친구에게 공유해볼까요?</h4>
				<button type="button" class="btnFcb">페이스북 공유하기</button>
				<button type="button" class="btnKtk">카카오톡 공유하기</button>
				<button type="button" class="btnMsg">메세지 공유하기</button>
				<button type="button" class="btnUrl">URL 복사하기</button>
			</div>
			<button class="btn_close" aria-label="공유하기 닫기"></button>
		</div>
		<!-- [End] SNS Share -->
	
		<? include_once(VIEW_PATH.'/include/gnb.php'); ?>
	</div>

</div>


  	<!-- [Start] Popup - Advertisement -->

	<?if(!empty($popup)){ ?>
		<div class="remodal advertisement" data-remodal-id="pop_ad">
			<button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
			<a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
		</div>
	<?}?>
	<!-- [End] Popup - Advertisement -->

<textarea id="NeisTextArea" style="position:absolute; left:-9999px;"></textarea>

<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>
<script type="text/JavaScript" src="https://developers.kakao.com/sdk/js/kakao.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.6/clipboard.min.js"></script>

<!-- In Script -->
<script>
Kakao.init("7b111ba912ef21c93ce5c281b6d40f62");      // 사용할 앱의 JavaScript 키를 설정
Kakao.isInitialized();

$(document).ready(function(){


	var start_record = 0;
	var countLike = <?=$view['nLike']?>;
	var boardNo = <?=$view['nSeqNo']?>;
	var boardType = "<?=$view['vType']?>";
	var isLike = Cookie.get('like_board_'+boardNo);
	if(isLike) $('.btnLike, #btn_like').addClass('active'); // 좋아요 활성화 
	replyList(0); // 처음댓글 가져오기 


	var isScrap = '<?=$isScrap?>';
	if(isScrap == 'Y') $('.btnBmk').addClass('active'); // 좋아요 활성화 

<?php if(!$this->session->userdata('UNO')){ ?> 
	$("#reply").click(function(){     
		window.location.href = "/auth";
	});
<?php }?>

	// 스크랩 버튼 클릭
	$(".btnBmk").off("click").on("click", function(){
<?php if(!$this->session->userdata('UNO')){ ?>
		//$(".lg").click();

		//alert("로그인 후에 사용 가능합니다.");
		window.location.href = "/auth";

<?php } else { ?>
		var u = "/info/boardScrap";
		var data = {
			nBoardNo : boardNo,
			vType : boardType
		}
		$.ajax({
			type : "post",
			url : u,
			dataType : 'json',
			data : data,
			success : function(response) {
				if(response.status == "SUCCESS"){
					// 스크랩 완료
					$(".btnBmk").text(response.cnt.nScrap);
					$(".btnBmk").addClass('active');

				}else{
				// 스크랩 실패
					Cmmn.alertMsg(response.msg);
//					alert(response.msg);
				}
			}
		});
<?php } ?>
	});

	$(".btnUrl").off("click").on("click", function(){
		var deviceType = localStorage.getItem("deviceType");
		$.ajax({
			type : "post",
			url : "/info/boardShare",
			dataType : 'json',
			data : { 'boardNo': boardNo, 'type' : 'url' },
			success : function(response) {
				var target = $(this).attr("target-id");
				var text = window.location.href;
				$("#NeisTextArea").val(text);
				$("#NeisTextArea").select();
				if(deviceType == "iOS") {
					//alert(document.execCommand('copy'))
					//var text = "Hello World!";
					cordova.plugins.clipboard.copy(text);
					cordova.plugins.clipboard.paste(function (text) {
							alert("보시고 계신 페이지의 URL이 복사되었습니다.\n\n붙여넣기를 통해  공유 해주세요.");
					});
					//cordova.plugins.clipboard.clear();
				}
				else{

					try {
						var successful = document.execCommand('copy');
					}
					catch (err) {
						alert('이 브라우저는 지원하지 않습니다.') 
					}

					// 액션이 있으면
					if (text == "") {
						alert("복사할 내용이 없습니다.");
						$("#"+target).focus();
						return false;
					} else {
						if (successful == true && text != "") {
							alert("보시고 계신 페이지의 URL이 복사되었습니다.\n\n붙여넣기를 통해  공유 해주세요.");
						}
					}
				}
			}
		});
	});


<?if(!empty($popup)){ ?>
	$('[data-remodal-id=pop_ad]').remodal().open();
<?}?>

	
	// 좋아요 버튼 클릭  
	$('.btnLike, .btn_like').on('click', function(){

		if(isLike){ 
			Cmmn.alertMsg('이미 좋아요한 상태입니다.'); 
			return false; 
		}
		$.ajax({
			type : "post",
			url : "/info/likeCheck",
			dataType : 'json',
			data : { 'idx': $('#reply').data('idx') },

			success : function(response) {
				console.log(response)
				//Cmmn.log('좋아요 클릭 이벤트');
				if( response.result == 'success')  {
					//$('.btnLike').toggleClass('active');
					$('.btnLike').text(countLike+1);
					$('#countLike').text(countLike+1);
					$(".btn_like").addClass("active")
					$(".btnLike").addClass("active")
					isLike = Cookie.get('like_board_'+boardNo);
					//Cmmn.log('좋아요 수 : '+countLike);
				}

				if( response.result == 'error' ) Cmmn.alertMsg(response.msg);
				
			},

			error : function(xhr, status, error) {},
		})
	});

	/* Reply Like Button Control */
	// $('.list_reply li .info .btn_like').on('click', function(){
	// 	$(this).toggleClass('active');
	// });


	/* Advertisement Slider - not app bottom banner */
	$('#excepted').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		infinite: true,
		speed: 500,
		// dots: true,
		arrows: false,
	});

	// 더보기 버튼 5개씩 가져오기 
	$("#moreBtn").click(function(){
		location.replace('/info/reply/<?=$view['nSeqNo']?>');
		// start_record = start_record + 5;
		// replyList(start_record);
	});

	// 댓글 등록 
	$("#addReply").click(function() {
<?php if(!$this->session->userdata('UNO')){ ?>
		location.href = "/auth";
<?php } else {  ?>

	<?if($this->session->userdata("ULV")==10){?>

		var txContent_editor_data = oEditors.getById['reply'].getIR();
		oEditors.getById['reply'].exec('UPDATE_CONTENTS_FIELD', []);
		if(jQuery.inArray(document.getElementById('reply').value.toLowerCase().replace(/^\s*|\s*$/g, ''), ['&nbsp;','<p>&nbsp;</p>','<p><br></p>','<div><br></div>','<p></p>','<br>','']) != -1){document.getElementById('reply').value='';}
		if (!txContent_editor_data || jQuery.inArray(txContent_editor_data.toLowerCase(), ['&nbsp;','<p>&nbsp;</p>','<p><br></p>','<p></p>','<br>']) != -1) { alert("내용을 입력해 주십시오."); oEditors.getById['reply'].exec('FOCUS'); return false; }

		var data = {
			'reply': $('#reply').val()
			, 'idx': $('#reply').data('idx')
		}

	<?} else {?>
		var data = {
			'reply': $('#reply').val()
			, 'idx': $('#reply').data('idx')
		}
	<?} ?>

		$.ajax({
			type : "post",
			url : "/info/addReply",
			dataType : 'json',
			data : data,
			success : function(response) {
				Cmmn.log(response);
				if( response.result == 'error' ) {  
					Cmmn.alertMsg(response.msg);
											$('.app_bottom').removeClass('hide');
				}
				if( response.result == 'success') {
					Cmmn.alertMsg(response.msg);
					replyList(0);
					// 초기화
					$('#reply').val('');
					start_record = 0;
					$('.writing_box').removeClass('focused');
					$('.app_bottom').removeClass('hide');
				}
			},
			error : function(xhr, status, error) {},
		})
<?php } ?>
		
	});


	// FaceBook 공유
	$(".btnFcb").click(function() {
		var deviceType = localStorage.getItem("deviceType");
		// 댓글 가져오기 
		$.ajax({
			type : "post",
			url : "/info/boardShare",
			dataType : 'json',
			data : { 'boardNo': boardNo, 'type' : 'facebook' },
			success : function(response) {
				Cmmn.log('페이스북 공유하기');
				if( response.result == 'error' ) {  
					Cmmn.alertMsg(response.msg);
					return false;
				}
				var fb_url = "https://www.facebook.com/sharer/sharer.php?u="+window.location.href;
				if(deviceType == "iOS"){
					cordova.InAppBrowser.open(fb_url, '_system', 'location=no');
				} else {
					window.open(fb_url);
				}
			},
			error : function(xhr, status, error) {},
		})
	});

	$(".btnKtk").click(function() {
//		alert('kakao start');
		var deviceType = localStorage.getItem("deviceType");
//		alert(deviceType)
		$.ajax({
			type : "post",
			url : "/info/boardShare",
			dataType : 'json',
			data : { 'boardNo': boardNo, 'type' : 'kakao' },
			success : function(response) {
				if( response.result == 'error' ) {  
					alert(response.msg)
					return false;
				}
				if(deviceType == "Android"){
					var url = window.location.href;
					var feedLink = { webURL: url }
					var feedSocial = {
						likeCount:0,
						commentCount:0,
						sharedCount:0
					}
					var feedButtons1 = { title: '게시글 확인하기', link: { mobileWebURL: url } }
					var feedButtons2 = { title: '게시글 확인하기', link: { iosExecutionParams: 'param1=value1&param2=value2', androidExecutionParams: 'param1=value1&param2=value2', } }
					var feedContent = {
						title:"<?=addslashes($view['vSubject']);?>",
						description:"<?=SITE_TITLE?>",
						link: feedLink,
						imageURL: "<?=($thumNailImage != null) ? $thumNailImage : ''; ?>"
					};
					var feedTemplate = { content: feedContent, social: feedSocial, buttons: [feedButtons1] };
					KakaoTalk.share(feedTemplate, function (success) {
					//	alert(success)
						/**
						*/
					}, function (error) {
					//	alert(error)
					});
				} else if(deviceType == "iOS"){
					var url = window.location.href;
					var imgUrl = '<?=($thumNailImage != null) ? $thumNailImage : ''; ?>';
					imgUrl = encodeURI(imgUrl);
					var feedLink = { webURL: url }
					var feedSocial = { likeCount: 0 }
					var feedButtons1 = { title: '게시글 확인하기', link: { mobileWebURL: url } }
					var feedButtons2 = { title: '게시글 확인하기', link: { iosExecutionParams: 'param1=value1&param2=value2', androidExecutionParams: 'param1=value1&param2=value2', } }
					var feedContent = {
						title:"<?=addslashes($view['vSubject']);?>",
						link: feedLink,
						imageURL: imgUrl
					};
					var feedTemplate = { content: feedContent, social: feedSocial, buttons: [feedButtons1] };
					KakaoCordovaSDK.sendLinkFeed(feedTemplate, function (success) {
//						alert('kakao share success');
					}, function (error) {
						alert(JSON.stringify(error)) ;
					});
				} else {
					Kakao.Link.sendDefault({
						objectType:"feed", 
						content : {
							title:"<?=addslashes($view['vSubject']);?>",
							description:"<?=SITE_TITLE?>",
							imageUrl:"<?=($thumNailImage != null) ? $thumNailImage : 'illidan_stormrage.jpg' ?>",
							link : {
								mobileWebUrl:window.location.href,
								webUrl:window.location.href
							}
						},
						social : {
							likeCount:0,
							commentCount:0,
							sharedCount:0
						},
						buttons : [{
							title:"게시글 확인하기",
							link : {
								mobileWebUrl:window.location.href,
								webUrl:window.location.href // PC버전 카카오톡에서 사용하는 웹 링크 URL
							}
						}]
					});
				}
			},
			error : function(xhr, status, error) {},
		})
	});

	// 문자 공유
	$(".btnMsg").click(function() {
		var deviceType = localStorage.getItem("deviceType");
		$.ajax({
			type : "post",
			url : "/info/boardShare",
			dataType : 'json',
			data : { 'boardNo': boardNo, 'type' : 'sms' },
			success : function(response) {
				if( response.result == 'error' ) {  
					Cmmn.alertMsg(response.msg);
					return false;
				}
				var sms_url = 'sms:?body=[노다지 주식정보의 수익이 나는 콘텐츠!] 링크 : '+window.location.href;
				if(deviceType == "iOS"){
					var sms_url = 'sms:&body=[노다지 주식정보의 수익이 나는 콘텐츠!] 링크 : '+window.location.href;
					sms_url = encodeURI(sms_url)
				//	alert(sms_url)
					cordova.InAppBrowser.open(sms_url, '_system', 'location=no');
				}else{
					location.href = sms_url;
				}
			},
			error : function(xhr, status, error) {},
		})
	});
					$('.btn_show_all').on('click', function(){
<?php if(!$this->session->userdata('UNO')){ ?>
						location.href="/auth";
<?php } else { ?>
						$('.writing_container').removeClass('is_summarized');
<?php }?>
					});
});


function reply_html(idx, nick, regDate, reply, like, img, lv){
	var html = "";
	var isReplylike = Cookie.get('replyLike_'+idx);
	var path = '';
	var profile_state = '';
	var profile_tag = '';

	if(!img) path = '../../asset/img/user_default.png';
	else path = '/data/user/profile/'+img;

	if(lv == 2){
		profile_state = 'expert';
		profile_tag = '<em>주식전문가</em>';
	}

	html +="<li><div class=\"profile_img %%LV%%\">";//
	html +="		<div style=\"background-image: url('%%IMG%%');\"></div>";
	html +="	</div>";
	html +="	<div class=\"info\">";
	html +="    <span class=\"nickname\">"+profile_tag+"%%NICK%%</span>";
	html +="		<span class=\"time\">%%REGDATE%%</span>";
	html +="		<button type=\"button\" class=\"btn_like %%STATE%%\" id=\"reply_id_%%NO%%\" onclick=\"replyLikeCount('%%IDX%%')\">%%LIKE%%</button>";
	html +="	</div>";
	html +="	<p class='replyText'>%%REPLY%%</p>";
	html +="</li>";

	html = html.replace("%%NO%%", idx);
	html = html.replace("%%IDX%%", idx);
	html = html.replace("%%NICK%%", nick);
	html = html.replace("%%REGDATE%%", regDate);
	html = html.replace("%%REPLY%%", reply);
	html = html.replace("%%LIKE%%", like);
	html = html.replace("%%IMG%%", path);
	html = html.replace("%%LV%%", profile_state);

	if(isReplylike) html = html.replace("%%STATE%%", 'active');

	return html;
}

function replyList(start_record){
	// 댓글 가져오기 
	$.ajax({
		type : "post",
		url : "/info/getReplyList",
		dataType : 'json',
		data : { 'idx': $('#reply').data('idx'), 'start_record' : start_record, 'orderby':'new' },

		success : function(response) {
			Cmmn.log('onload event 댓글리스트가져오기');
			if( response.result == 'error' ) {  
				if( start_record != 0 ) Cmmn.alertMsg(response.msg);
			}
			if( response.result == 'success') {
				var html = '';
				Cmmn.log(response);
				$.each(response.list, function (index, item) {
					html += reply_html(item.nSeqNo, item.vNick, item.dtRegDate, item.txComment, item.nLike, item.vImage, item.nLevel);
				})
				if( start_record == 0 ) $('.list_reply').html(html);
				else $('.list_reply').append(html); 
			}
		},
		error : function(xhr, status, error) {},
	})
}

function replyLikeCount(idx) {
	var isReplylike = Cookie.get('replyLike_'+idx);
	if(isReplylike) {
		Cmmn.alertMsg('이미 좋아요한 상태입니다.'); return false;
	}
	// 댓글 가져오기 
	$.ajax({
		type : "post",
		url : "/info/replyLikeCheck",
		dataType : 'json',
		data : { 'idx': idx },
		success : function(response) {
			Cmmn.log(response);
			if( response.result == 'success') {
				location.reload();
			}

			if( response.result == 'error' ) Cmmn.alertMsg(response.msg);


		},
		error : function(xhr, status, error) {},
	})
}

function select_all_and_copy(el) {
	// http://www.seabreezecomputers.com/tips/copy2clipboard.htm
	
	// ++ added line for table
	var el = document.getElementById(el);
	// Copy textarea, pre, div, etc.
	if (document.body.createTextRange) {// IE 
		var textRange = document.body.createTextRange();
		textRange.moveToElementText(el);
		textRange.select();
		textRange.execCommand("Copy");   
		alert('Copied to clipboard');
	} else if (window.getSelection && document.createRange) {// non-IE

		var editable = el.contentEditable; // Record contentEditable status of element
		var readOnly = el.readOnly; // Record readOnly status of element
		var range = document.createRange();

		el.contentEditable = true; // iOS will only select text on non-form elements if contentEditable = true;
		el.readOnly = false; // iOS will not select in a read only form element
		range.selectNodeContents(el);

		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range); // Does not work for Firefox if a textarea or input

		el.setSelectionRange(0, 999999); // iOS only selects "form" elements with SelectionRange
/*
		if (el.nodeName == "TEXTAREA" || el.nodeName == "INPUT") 
			el.select(); // Firefox will only select a form element with select()
		if (el.setSelectionRange && navigator.userAgent.match(/ipad|ipod|iphone/i))
			el.setSelectionRange(0, 999999); // iOS only selects "form" elements with SelectionRange
*/
		el.contentEditable = editable; // Restore previous contentEditable status
		el.readOnly = readOnly; // Restore previous readOnly status 


		//alert(document.execCommand('copy'))

		if (document.queryCommandSupported("copy")) {
			var successful = document.execCommand('copy');
			if (successful) alert('Copied to clipboard');
			else alert('Press Ctrl+C to copy');
		}
		else {
			if (!navigator.userAgent.match(/ipad|ipod|iphone|android|silk/i))
				alert('Press Ctrl+C to copy');
		}
	}
}
	/* Banner Scroll Control */
	(function(){
		var presentTop = 0;
		var windowHeight = 0;
		var gnbHeight = 0;
		var $standard = {};
		var activePoint = 0;
		var $gnbBanner = {};
		
		$(window).on('scroll', function(){
			presentTop = $(window).scrollTop();
			windowHeight = $(window).height();
			gnbHeight = $('.gnb').height();
			$standard = $('.app_main .banner_type1');
			activePoint = $standard.offset().top + $standard.height();
			$gnbBanner = $('.app_bottom .banner_type1');
			
			if(presentTop+windowHeight-gnbHeight < activePoint || presentTop > activePoint ) {
				$gnbBanner.show();
			} else {
				$gnbBanner.hide();
			}
		});


		var boardNo = <?=$view['nSeqNo']?>;
		$(window).on('load', function(){
			setTimeout(function() {
				$(".loading_mask").css("display", "none");
				$(".writing_container").css("visibility", 'visible').hide().fadeIn(300);
			}, 1000);
		});

	})()

</script>
