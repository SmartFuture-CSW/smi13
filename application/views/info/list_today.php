<div class="app_wrapper">
	<!-- [Start] App Header -->
	<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
	<!-- [End] App Header -->

	<!-- [Start] App Main -->
	<div class="app_main">
		<div class="psuedo_container">
			<div class="search_container layout_center">
				<div class="search_box">
					<form action="/info/search" method="get">
						<input type="search" name="keyword" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
						<button type="submit" id="searchBtn" aria-label="검색"></button>
					</form>
				</div>
			</div>
			<!-- lnb --><? include_once(VIEW_PATH.'/include/lnb.php'); ?>
		</div>

		<section class="main_topic">
			<h1 class="a11y_hidden">TODAY</h1>
<?// 2020-09-01 css 수정해주세요 csw?>
			<div class="category_container ">
				<div class="layout_center">
					<a href="/info/today/date" class="<?=($category == "date")?"active" : ""; ?>">최신순</a>
					<a href="/info/today/hit" class="<?=($category == "hit")?"active" : ""; ?>">조회수순</a>
					<a href="/info/today/share" class="<?=($category == "share")?"active" : ""; ?>">공유순</a>
					<a href="/info/today/reply" class="<?=($category == "reply")?"active" : ""; ?>">댓글순</a>
					<a href="/info/today/rec" class="<?=($category == "rec")?"active" : ""; ?>">추천순</a>
				</div>
			</div>

			<div class="ctnt_list">
		<?php 
		foreach ($today as $key => $value) {
			$diff = ($value['dayDiff'] == 0) ? $value['hourDiff']."시간전" : $value['dayDiff']."일전" ;
			$imgPath = '/data/board/thumb/'.$value['vImage'];
		?>
				<a class="clearfix" href="/boardView/<?=$value['vType']?>/<?=$value['nSeqNo']?>">
					<div class="info">
						<h5><?=$value['vSubject']?></h5>
						<p><span><?=$diff?></span><span>조회수 <?=$value['nHit']?></span><span>공유수 <?=$value['nShare']?></span><?/*댓글수 : <?=$value['nReply']*/?></p>
					</div>
					<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
				</a>
		<?php } ?>
			</div>
				
			<a class="link_more_result"><span class="categoryName" style="color:#333;"><?=$order?></span> <span class="switch" style="color:#333;">펼쳐보기</span></a>
		</section>

		<div class="banner_type1" style="margin-bottom:5px;">
			<a href="https://pf.kakao.com/_ncIXK" target="_blank"><img src="/asset/img/banner_kakaoplus.jpg" alt=""></a>
		</div>
		<div class="banner_type1" style="margin-bottom:18px;">
		<? foreach ($banner as $key => $value) { ?>
			<a href="<?=$value['vLink']?>"><img src="/data/banner/<?=$value['vImage']?>" alt=""></a>
		<?}?>
		</div>

		<section class="investment_information layout_center">
			<h2>추천종목</h2>
			<div class="list_article clearfix">
		<?php
			foreach ($recommend as $key => $value) { 
				$imgPath = '/data/board/thumb/'.$value['vImage'];
		?>
				<a href="/boardView/<?=$value['vType']?>/<?=$value['nSeqNo']?>">
					<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
					<p><?=$value['vSubject']?></p>
				</a>
		<?php
			}
		?>

			</div>
		</section>

		<section class="investment_information layout_center">
			<h2>투자정보</h2>
			<div class="list_article clearfix">
		
		<?php
			foreach ($invest as $key => $value) { 
				$imgPath = '/data/board/thumb/'.$value['vImage'];
		?>
				<a href="/boardView/<?=$value['vType']?>/<?=$value['nSeqNo']?>">
					<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
					<p><?=$value['vSubject']?></p>
				</a>

		<?php  } ?>

			</div>
		</section>


		<section class="investment_information layout_center">
			<h2>주식공부</h2>
			<div class="list_article clearfix">
		
		<?php
			foreach ($study as $key => $value) { 
				$imgPath = '/data/board/thumb/'.$value['vImage'];
		?>
				<a href="/boardView/<?=$value['vType']?>/<?=$value['nSeqNo']?>">
					<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
					<p><?=$value['vSubject']?></p>
				</a>

		<?php  } ?>

			</div>
		</section>


		<section class="investment_information layout_center">
			<h2>커뮤니티</h2>
			<ul class="community_list">
<?php
$arrCategory[12] = '공지사항';
$arrCategory[13] = '질문하기';
$arrCategory[14] = '질문하기';
$arrCategory[15] = '건의하기';

foreach ($community as $key => $value) { 
	$tmp = trim($value['txTag']);
	$tag = explode(",", $tmp);

	if($value['nCategoryNo'] == "12" || $this->session->userdata('UNO') == '12' || $this->session->userdata('UNO') == '1'){
		$subject = $value['vSubject'];
		$contents = $this->util->cut_str(str_replace("&nbsp;"," ",strip_tags($value['txContent'])),80);
	}
	else{
		if($value['nUserNo'] == $this->session->userdata('UNO')){
			$subject = $value['vSubject'];
			$contents = $this->util->cut_str(str_replace("&nbsp;"," ",strip_tags($value['txContent'])),80);
		}
		else{
			$subject =  $this->util->cut_str(str_replace("&nbsp;","",strip_tags($value['vNick'])),1, "**").'님의 '.$arrCategory[$value['nCategoryNo']].' 신청 완료';
			$contents = "작성자만 확인 가능합니다.";
		}
	}
?> 
				<li>
<?php
	if(count($tag) > 1 ) { 
		echo '<p>'; for ($i=0; $i<count($tag) ; $i++) echo '<span class="tag_label">#'.$tag[$i].'</span>'; echo '</p>';
}
?>
					<a href="<?='/boardView/community/'.$value['nSeqNo'];?>">
						<h5><?=$subject?></h5>
						<p><?=$contents?></p>
					</a>
					<div class="bottom">
						<span class="user">조회수 <?=$value['nHit']?></span>
						<span class="like"><?=$value['nLike']?></span>
						<span class="reply"><?=$value['cnt']?></span>
					</div>
				</li>
<?php
	}
?>
			</ul>
		</section>
		<!--a class="advertisement_bottom" href="#none"><img src="<?=IMG?>/sample/sample_app_advertisement.jpg" alt=""></a-->
	</div>
	<!-- [End] App Main -->

	<!-- [Start] App Bottom -->
	<div class="app_bottom"><? include_once(VIEW_PATH.'/include/gnb.php'); ?></div>
</div>
<!-- [Start] Popup - Advertisement -->
<?if(!empty($popup)){ ?>
	<div class="remodal advertisement" data-remodal-id="pop_ad">
		<button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
		<a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
	</div>
<?}?>
<!-- [End] Popup - Advertisement -->

<!-- in script -->
<script type="text/javascript">
(function(){
	// 리스트 탭메뉴
	/*
	$(".category_container > .layout_center > a").off("click").on("click", function(e){

		e.preventDefault();

		$(".category_container > .layout_center > a").removeClass('active');
		$(this).addClass('active')


		var page = $(this).attr("href").split("/");
		var order = page[page.length-1];
		var full = $(".link_more_result").attr("full");
		var data = {
			'order' : order,
			'full' : full
		}
		var u = '/info/todaylist/'+order;
		$.post(u,data,function(response){

			$(".ctnt_list > a").remove();
			var i =1;
			$.each(response, function(idx, row){
				setTodayList(row, i);
				i++;

			});
			$(".categoryName").text($(".category_container > .layout_center > a.active").text());
			if($(".link_more_result").attr("full") == "on"){
				bbb();
			}else{
				aaa();
			}
		},'json');
	});
	*/
	var setTodayList = function(row, i){
		var diff = (row.dayDiff == 0) ? row.hourDiff + "시간전" : row.dayDiff + "일전";
		var link = "/boardView/" + row.vType + "/" + row.nSeqNo;
		var text = row.vSubject;
		var img = "/data/board/thumb/" + row.vImage;
		var hit = "조회수 " + row.nHit;
		var share = "공유수 " + row.nShare;
		var anchor = $('<a />', {
			'class' : (i > 20) ? 'clearfix tg' : 'clearfix',
			href : link
		});
		var container = $('<div />', {
			'class' : 'info'
		});
		var heading = $('<h5 />', {
			text : text
		});
		var paragraph = $('<p />');
		var diff = $('<span />', {
			text : diff
		});
		var hit = $('<span />', {
			text : hit
		});
		var share = $('<span />', {
			text : share
		});

		var thumbnail = $('<div />', {
			'class' : 'thumbnail'
		});

		thumbnail.attr('style', "background-image:url('"+img+"'")
		paragraph.append(diff);
		paragraph.append(hit);
		paragraph.append(share);
		container.append(heading);
		container.append(paragraph);
		anchor.append(container);
		anchor.append(thumbnail);
		$(".ctnt_list").append(anchor);
	};

	var aaa = function(){
		$(".link_more_result").off("click").on("click", function(){
			var page = $(".category_container > .layout_center > a.active").attr('href').split('/');
			var order = page[page.length-1];
			var data = {
				'order' : order,
				'full' : 'half'
			}
			console.log(data);
			var u = '/info/todaylist/'+order;
			$.post(u,data,function(response){
				i = 21;
				$.each(response, function(idx, row){
					setTodayList(row, i);
					$(".switch").text('닫기');
					$(".link_more_result").attr("full", 'on');
					bbb();
					i++;
				});
			},'json');
		});
	}

	var bbb = function(){
		$(".link_more_result").off("click").on("click", function(){
			$(".ctnt_list > a.tg").remove();
			$(".switch").text('펼쳐보기');
			$(".link_more_result").attr("full", 'off');
			aaa();
		});
	}
	aaa();

<?if(!empty($popup)){ ?>
	$('[data-remodal-id=pop_ad]').remodal().open();
<?}?>
})();
</script>