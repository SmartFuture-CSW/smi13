<div class="app_wrapper">
	<!-- [Start] App Header -->
	<header class="app_header">
		<div class="back">
			<a href="/community" aria-label="뒤로가기"></a>
			<h2>글쓰기</h2>
		</div>
	</header>
	<!-- [End] App Header -->

	<!-- [Start] App Main -->
	<div class="app_main">
		<form id="frm" name="frm" method="post">
<?php if(empty($view)){ ?>

		<div class="community_content">
			<div class="selectbox">
				<select name="category" id="category" required>
					<option value="">카테고리를 선택 해주세요</option>
				<?php
				foreach($category as $row){
				?>
					<option value="<?=$row['nSeqNo']?>"><?=$row['vSubject']?></option>
				<?php
				}
				?>
				</select>
			</div>
			<div class="input_box post_subject"><input type="text" name="subject" id="subject" placeholder="제목을 입력해주세요"></div>
			<div class="post_contents"><textarea name="content" id="content" placeholder="내용을 입력해주세요"></textarea></div>
			<div class="input_box hashtag">
				<label for="hashtag">해시태그</label>
				<input type="text" id="hashtag" name="hashtag" placeholder="해시태그 입력 (,로 구분)">
			</div>
			<div class="btn_container post">
				<button class="btn_m_pd bg_red" type="button" onclick="javascript:location.replace('/info/community')">취소</button>
				<button class="btn_m_pd bg_gray" type="button" id="submitBtn" >등록</button>
			</div>
		</div>
<?php }else{ ?>
		<div class="community_content">
			<div class="input_box post_subject">
				<select name="category" id="category">
					<option value="">카테고리를 선택 해주세요</option>
				<?php
				foreach($category as $row){
				?>
					<option value="<?=$row['nSeqNo']?>" <?=($view[0]['nCategoryNo']==$row['nSeqNo']) ? 'selected' : ''; ?>><?=$row['vSubject']?></option>
				<?php
				}
				?>
				</select>
			</div>
			<input type="hidden" name="boardNo" value="<?=$view[0]['nSeqNo']?>">
			<div class="input_box post_subject"><input type="text" name="subject" id="subject" placeholder="제목을 입력해주세요" value="<?=$view[0]['vSubject']?>"></div>
			<div class="post_contents"><textarea name="content" id="content" placeholder="내용을 입력해주세요"><?=$view[0]['txContent']?></textarea></div>
			<div class="input_box hashtag">
				<label for="hashtag">해시태그</label>
				<input type="text" id="hashtag" name="hashtag" placeholder="해시태그 입력 (,로 구분)" value="<?=$view[0]['txTag']?>">
			</div>
			<div class="btn_container post">
				<button class="btn_m_pd bg_red" type="button" onclick="javascript:location.replace('/info/community')">취소</button>
				<button class="btn_m_pd bg_gray" type="button" id="modifyBtn" >수정</button>
			</div>
		</div>

<?php } ?>
		</form>
	</div>
	<!-- [End] App Main -->

	<!-- [Start] App Bottom -->
	<div class="app_bottom">
	<!-- gnb -->
		<? include_once(VIEW_PATH.'/include/gnb.php'); ?>
	</div>
<!-- [End] App Bottom -->
</div>

<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>
<script type="text/javascript">
/* form submit */
$('#submitBtn').on('click', function(){
	if( !$('#category option:selected').val() ){
		Cmmn.alertMsg("카테고리를 선택해주세요.");
		return false;
	}
	if( !$('#subject').val() ){
		Cmmn.alertMsg("제목을 작성해주세요.");
		return false;
	}
	if( !$('#content').val() ){
		Cmmn.alertMsg("내용을 작성해주세요.");
		return false;
	}
	$.ajax({
		type : "post",
		url : "/info/addCommunity",
		dataType : 'json',
		data : $('#frm').serialize(),
		success : function(response) {
			Cmmn.log(response);
			if( response.result == 'error' ) {  
				Cmmn.alertMsg(response.msg);
			}
			if( response.result == 'success') {
				alert(response.msg);
				location.replace('/info/community');                        
			}
		},
		error : function(xhr, status, error) {},
	});
});

/* form submit */
$('#modifyBtn').on('click', function(){
	if( !$('#subject').val() ){
		Cmmn.alertMsg("제목을 작성해주세요."); return false;
	}
	if( !$('#content').val() ){
		Cmmn.alertMsg("내용을 작성해주세요."); return false;
	}
	$.ajax({
		type : "post",
		url : "/info/setCommunity",
		dataType : 'json',
		data : $('#frm').serialize(),
		success : function(response) {
			Cmmn.log(response);
			if( response.result == 'error' ) {  
				Cmmn.alertMsg(response.msg);
			}
			if( response.result == 'success') {
				alert(response.msg);
				location.replace('/info/community');                        
			}
		},
		error : function(xhr, status, error) {},
	});
});
</script>