<div class="app_wrapper">
	<!-- [Start] App Header -->
	<? include_once(VIEW_PATH.'/include/header_app.php'); ?>
	<!-- [End] App Header -->

	<!-- [Start] App Main -->
	<div class="app_main">
		<div class="psuedo_container">
			<div class="search_container layout_center">
				<div class="search_box">
					<form action="/info/search" method="post">
						<input type="search" name="keyword" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" value="<?=$keyword?>">
						<button type="submit" id="searchBtn" aria-label="검색"></button>
					</form>
				</div>
			</div>
			<!-- lnb --><? include_once(VIEW_PATH.'/include/lnb.php'); ?>
		</div>

		<div class="category_container ">
			<div class="layout_center">
				<a href="/info/search/all/<?=$keyword?>" class="<?=($pageType == "all" || $pageType == "")?"active" : ""; ?>">전체</a>
				<a href="/info/search/info/<?=$keyword?>" class="<?=($pageType == "info")?"active" : ""; ?>">공개정보</a>
<?php if($_SERVER['ios'] == "N"){?>
				<a href="/info/search/stock/<?=$keyword?>" class="<?=($pageType == "stock")?"active" : ""; ?>">추천종목</a>
<?php }?>
				<a href="/info/search/community/<?=$keyword?>" class="<?=($pageType == "community")?"active" : ""; ?>">커뮤니티</a>
			</div>
		</div>
		<h1 class="a11y_hidden">'<?=$keyword?>' 에 대한 검색 결과</h1>

<?
/* @ 주의 : 전체보기와 게시판별 검색결과가 달라 분기처리 함. */
if($pageType == "all"){
?>


		<section class="result_container">
			<h2 class="layout_center">공개정보</h2>
			<div class="essential_list">
	<?php
		if(count($info) == 0){
	?>
				<p class="txt_no_result">'<?=$keyword?>' 에 대한 검색 결과가 없습니다.</p>
	<?php
		}
		else {

			foreach ($info as $key => $value) { 
				$imgPath = '/data/board/thumb/'.$value['vImage'];
		?>
				<a class="clearfix" href="/boardView/important/<?=$value['nSeqNo']?>">
					<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
					<div class="info">
						<h5><?=$value['vSubject']?></h5>
						<p>
							<span><?=$value['dtRegDate']?></span>
							<span>조회수 <?=$value['nHit']?>회</span>
							<span>공유수 <?=$value['nShare']?>회</span>
						</p>
					</div>
				</a>
	<?php
			}
		}
	?>
			</div>
			<a href="/info/search/info/<?=$keyword?>" class="link_more_result">공개정보 <span>검색결과</span> 더보기</a>
		</section>


<?php

$p = 1;
?>
			<section class="result_container">
				<h2 class="layout_center">추천종목</h2>
<?
foreach($stock as $row){
	if($row['vType'] == "period"){
?>
				<div class="period_item_container">
					<table>
						<colgroup>
							<col style="width:20%">
							<col>
							<col style="width:22%">
						</colgroup>
						<tbody>
							<tr>
								<td><?=$row['nAimDate']?>일</td>
								<td>
									<a href="/stock/view/day/<?=$row['nSeqNo']?>" class="ellipsis"><?=$row['vSubject']?></a>
									<span><?=substr($row['dtRegDate'], 0,10);?></span>
								</td>
								<td><?=$row['nAimPercent']?>%</td>
							</tr>
						</tbody>
					</table>
				</div>



<?php 
// 뉴스별 종목 검색결과
}else if($row['vType'] == "news"){
?>
				<div class="news_list layout_center"> 
				<article>
					<h4><?=$row['vSubject']?></h4>
					<div class="btn_container clearfix">
						<a href="/stock/view/news/<?=$row['nSeqNo']?>">VIP 종목 확인하기</a>
						<a href="<?=$row['vNewsLink']?>">기사원문보기</a>
					</div>
				</article>
				</div>
<?php
}else if($row['vType'] == "theme"){
?>

			<section class="stck_section theme_main_container">
	<?php
		$tmp = trim($row['txTag']);
		$tag = explode("#", $tmp);
		$arrColorPalette = array('#fef4ea', '#fffcc9', '#e1f8fe', '#dcf9e7');

	?>
				<a href="/stock/view/theme/<?=$row['nSeqNo']?>" class="theme_item" style="background-color: <?=$arrColorPalette[$p%4]?>;">
					<div class="center_box">
						<h4>목표 수익률 <?=$row['nAimPercent']?>%</h4>
						<p><?=$row['vSubject']?></p>
						<?  // 태그 
							if(count($tag) > 1 ) { 
								echo '<ul class="hashtags">'; 
								for ($i=1; $i<count($tag) ; $i++) echo '<li>'.$tag[$i].'</li>'; 
								echo '</ul>';
							}
						?>
					</div>
				</a>
<?php
		$p++;
	}
?>
<?php
}
?>
				<a href="/info/search/stock/<?=$keyword?>" class="link_more_result" style="border-top:none;">추천종목 <span>검색결과</span> 더보기</a>
			</section>


			<section class="result_container">
				<h2 class="layout_center">커뮤니티</h2>

				<div class="community_container layout_center">
					<ul class="community_list">
<?php
foreach ($community as $key => $value) { 
	$tmp = trim($value['txTag']);
	$tag = explode(",", $tmp);

?>
						<li><?php	if(in_array($value['nSeqNo'], $best)){?><span class="mk_best" aria-label="BEST"></span><?php }?>
<?php
	if(count($tag) > 1 ) { 
		echo '<p>'; for ($i=0; $i<count($tag) ; $i++) echo '<span class="tag_label">#'.$tag[$i].'</span>'; echo '</p>';
}
?>
							<a href="<?='/boardView/community/'.$value['nSeqNo'];?>">
								<h5><?=$value['vSubject']?></h5>
								<p><?=$value['txContent']?></p>
							</a>
							<div class="bottom">
								<span class="user">조회수 <?=$value['nHit']?></span>
								<span class="like"><?=$value['nLike']?></span>
								<span class="reply"><?=$value['cnt']?></span>
							</div>
						</li>
<?php
	}
?>
					</ul>
				</div>
				<a href="/info/search/community/<?=$keyword?>" class="link_more_result">커뮤니티 <span>검색결과</span> 더보기</a>
			</section>
<?php
// 아래의 분기처리된 HTML은 게시판별 검색결과 처리
}
else{

	/**
	* @breif: 해당 문단의 소스 설명
	* @detail: 상단 검색 결과에서 '공개정보' 카테고리 클릭했을시에 보여지는 페이지 내용. 무한스크롤 입니다. 하단의 스크립트 참고
	* @author: csw
	*/ 
	if($pageType == "info"){
?>
		<section class="result_container">
			<div class="essential_list infinity" data-page="<?=$infinity_page?>">
<?  
foreach ($infinity as $key => $value) { 
	$imgPath = '/data/board/thumb/'.$value['vImage'];
?>
			<a class="clearfix " href="/boardView/<?=$value['vType']?>/<?=$value['nSeqNo']?>">
				<div class="thumbnail" style="background-image:url('<?=$imgPath?>')"></div>
				<div class="info">
					<h5><?=$value['vSubject']?></h5>
					<p>
						<span><?=$value['dtRegDate']?></span>
						<span>조회수 <?=$value['nHit']?>회</span>
						<span>공유수 <?=$value['nShare']?>회</span>
					</p>
				</div>
			</a>
<?  } ?>
		</div>
	</section>
<?
	}


	/**
	* @breif: 해당 문단의 소스 설명
	* @detail: 상단 검색 결과에서 '추천종목' 카테고리 클릭했을시에 보여지는 페이지 내용. 무한스크롤 아닙니다. 일단 검색결과 전부 다 노출 예정 *추후 변경될 수 있음*
	* @author: csw
	*/ 
	else if($pageType == "stock"){
?>
		<section class="result_container">
		<?php
		$p = 0;
		foreach($stock as $row) {
			if($row['vType'] == "period"){
		?>
			<div class="period_item_container">
				<table>
					<colgroup>
						<col style="width:20%">
						<col>
						<col style="width:22%">
					</colgroup>
					<tbody>
						<tr>
							<td><?=$row['nAimDate']?>일</td>
							<td>
								<a href="/stock/view/day/<?=$row['nSeqNo']?>" class="ellipsis"><?=$row['vSubject']?></a>
								<span><?=substr($row['dtRegDate'], 0,10);?></span>
							</td>
							<td><?=$row['nAimPercent']?>%</td>
						</tr>
					</tbody>
				</table>
			</div>

			<?php 
			}
			// 뉴스별 종목 검색결과
			else if($row['vType'] == "news"){
			?>

			<div class="news_list layout_center"> 
				<article>
					<h4><?=$row['vSubject']?></h4>
					<div class="btn_container clearfix">
						<a href="/stock/view/news/<?=$row['nSeqNo']?>">VIP 종목 확인하기</a>
						<a href="<?=$row['vNewsLink']?>">기사원문보기</a>
					</div>
				</article>
			</div>

			<?php
			}
			else if($row['vType'] == "theme"){
				$tmp = trim($row['txTag']);
				$tag = explode("#", $tmp);
				$arrColorPalette = array('#fef4ea', '#fffcc9', '#e1f8fe', '#dcf9e7');

			?>
			<a href="/stock/view/theme/<?=$row['nSeqNo']?>" class="theme_item" style="background-color: <?=$arrColorPalette[$p%4]?>;">
				<div class="center_box">
					<h4>목표 수익률 <?=$row['nAimPercent']?>%</h4>
					<p><?=$row['vSubject']?></p>
					<?  // 태그 
						if(count($tag) > 1 ) { 
							echo '<ul class="hashtags">'; 
							for ($i=1; $i<count($tag) ; $i++) echo '<li>'.$tag[$i].'</li>'; 
							echo '</ul>';
						}
					?>
				</div>
			</a>
<?php
				$p++;
			}
		}
?>
			</section>
<?
	} else if($pageType == "community"){
?>
		<div class="community_container layout_center">
			<ul class="community_list">
<?php

foreach ($community as $key => $value) { 
	$tmp = trim($value['txTag']);
	$tag = explode(",", $tmp);

?>
				<li>
					<?php	if(in_array($value['nSeqNo'], $best)){?><span class="mk_best" aria-label="BEST"></span><?php }?>
<?php
	if(count($tag) > 1 ) { 
		echo '<p>'; for ($i=0; $i<count($tag) ; $i++) echo '<span class="tag_label">#'.$tag[$i].'</span>'; echo '</p>';
}
?>
					<a href="<?='/boardView/community/'.$value['nSeqNo'];?>">
						<h5><?=$value['vSubject']?></h5>
						<p><?=$value['txContent']?></p>
					</a>
					<div class="bottom">
						<span class="user">조회수 <?=$value['nHit']?></span>
						<span class="like"><?=$value['nLike']?></span>
						<span class="reply"><?=$value['cnt']?></span>
					</div>
				</li>
<?php
	}
?>
			</ul>
		</div>
<?
	}
}
?>

	</div>
	<!-- [End] App Main -->

	<!-- [Start] App Bottom -->
	<div class="app_bottom"><? include_once(VIEW_PATH.'/include/gnb.php'); ?></div>
</div>
<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>

<!-- In Script -->
<script>
var start_record = 0;
var keyword = '<?=$keyword?>';

$(document).ready(function(){
	
	/**
	* @brief : inifnity scroll
	* @author : csw
	*/
	var busy = false;
	$(window).scroll(function() {
		var winHeight = parseInt($(this).height(), 10);
		var docHeight = parseInt($(document).height(), 10);
		var scrTop = parseInt($(this).scrollTop(), 10);
		if( (docHeight - winHeight - 150) <= scrTop ){

			if(busy){
				return;
			}
			busy = true;

			// 다음에 보여줄 리스트 출력
			var page = $(".infinity").data('page');
			$(".infinity").data('page', page+1);
			page = $(".infinity").data('page');
			var data = {
				type : '<?=$pageType?>',
				page : page,
				keyword : keyword
			}

			$.post("/info/getBoardInfinitySearch", data ,function(response){
				//console.log(response)
				if(response.status == "SUCCESS"){
					$.each(response.list, function(idx, row){
						var anchor = $('<a />', {
							'class' : 'clearfix', 
							href : '/boardView/' + row.vType + '/' + row.nSeqNo
						});

						var thumbnail = $('<div />', {
							'class' : 'thumbnail',
							style : 'background-image:url(\'/data/board/thumb/' + row.vImage + '\')'
						});

						var info =  $('<div />', {
							'class' : 'info',
						});

						var subject = $('<h5 />', {
							text : row.vSubject
						});

						var contents = $('<p />', {});

						var regdate = $('<span />', {
							text : row.dtRegDate
						});

						var hit = $('<span />', {
							text : row.nHit
						});

						var share = $('<span />', {
							text : row.nShare
						});

						contents.append(regdate);
						contents.append(hit);
						contents.append(share);

						info.append(subject);
						info.append(contents);

						anchor.append(thumbnail);
						anchor.append(info);

						$(".infinity").append(anchor)

						busy = false;
					})
				}
			});


		}
	});


});

</script>