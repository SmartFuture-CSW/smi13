<script src="/asset/js/cnt.js" type="text/javascript"></script>
<script src="/asset/js/jquery.progress.js" type="text/javascript"></script>
<style>
	body {background-color:#fff;max-width:640px !important;}
	.hidden { position: absolute; width: 0; height: 0; font-size: 0; line-height: 0; overflow: hidden; visibility: hidden; }
	.imgbox {background-color:#03080c;}
	.imgbox img { display: block; width: 100%; }
	.container { max-width:640px; margin: 0 auto; }
	.container .co {background-size: cover; }
	.container .co .countdown { position: relative; text-align: center; padding:5% 0px;background-color:#03080c;}
	.container .co .countdown span { display: block; width: 50%; margin: 0 auto; height: 300px; background: url('/asset/img/ymkt/2/time_03_3.png') no-repeat 50% 50%; background-size: contain; }
	.container .co .countdown.s-3 span { background-image: url('/asset/img/ymkt/2/time_03_3.png'); }
	.container .co .countdown.s-2 span { background-image: url('/asset/img/ymkt/2/time_03_2.png'); }
	.container .co .countdown.s-1 span { background-image: url('/asset/img/ymkt/2/time_03_1.png'); }
	.container .co .countdown.end span { background: none; display: none; }
	.bottom {background: none; display: none;background-color:#03080c;}
	.bottom .progress-bar { position: relative; width: 90%; margin: 0 5% 2%; }
	.bottom .progress-bar .bar { border: 3px solid #333; border-radius: 3px; background: #000; height: 3px; }
	.bottom .progress-bar .bar i { display: block; width: 0; height: 3px; background: #fff; }
	.bottom .progress-bar .per { position: absolute; right: 0; top: 13px; font-size: 20px; color: #fff; font-weight: bold; }
	.bottom .progress-bar .status { height: 25px; padding-top: 5px; text-align: center; color: #fff; font-size: 20px; font-weight: bold; text-shadow: 0 0 5px #fffca4; }
	.bottom .submit { display: block; width: 90%; max-width: 490px; margin: 0 auto; }
	/* db form */
	.dbform { display: none; position: absolute; top: 50px; left: 5px; right: 5px; }
	.dbform > .bg { position: fixed; left: 0; top: 0; right: 0; bottom: 0; background: rgba(0, 0, 0, 0.6); }
	.dbform.is-active { display: block; }
	.form-title > img { display: block; width: 100%; }
	/* input */
	.text, .textarea, .select { width: 100%; padding: 5px; border: 0 none; background: #fff; background-image: none !important; height: 58px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #000; }
	.textarea { height: 140px; resize: none; }
	.select { padding: 5px; }
	.bt1{width:30%; height: inherit; background-color: #313131; border-radius:20px; color: #fff; padding: 6px; font-size: 13px;}
	.bt2{width:30%; height: inherit; background-color: #313131; border-radius:20px; color: #fff; padding: 6px; font-size: 13px;}
	/* label, field */
	.label, .field { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; }
	.label { position: absolute; width: 70px; padding: 0; text-align: left; font-weight: bold; font-size: 18px; height: 58px; line-height: 0; }
	.label:before { content: ''; display: inline-block; height: 100%; vertical-align: middle; }
	.field { margin-left: 0; vertical-align: top; }
	.comment0 {font-size:15px;font-weight:bold;}
	.comment1 {font-size:11px;}
	#mask {position:absolute;z-index:1000;background:rgba(0, 0, 0, 0.7);display:none;left:0;top:0;cursor:pointer;z-index:1000;}
	.popup{position:fixed;top:20%;width:90%;left:50%;margin-left:-320px;text-align:center;display:none;z-index:1001;max-width:640px;}
	input {
		margin: 0;
		border: block;
		box-sizing: border-box;
		-webkit-appearance: checkbox;
		-moz-appearance: checkbox;
		appearance: checkbox;
		-webkit-border-radius: 0;
	}

	header,footer,section,article,aside,nav,hgroup,details,menu,figure,figcaption{display:block;}
	html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,address{margin:0; padding:0; border:0; outline:0; vertical-align:baseline; background:transparent; list-style:none;}
	html,body{margin:0; width:100%; height:100%; line-height:normal; -webkit-overflow-scrolling:touch;}
	body{font-family:"NanumBarunGothic","Nanum BarunGothic","Nanum Gothic",monospace;color:#000;letter-spacing:-1px;-webkit-text-size-adjust:none;vertical-align:top;}img{border:0;max-width:100%;vertical-align:top;}form{display:inline;}fieldset{border:0;}legend{display:none;}input,select,radio,div{vertical-align:top;}input[type=submit],button{cursor:pointer;}input[type=radio]{padding:0;margin:0;vertical-align:middle;}input[type=checkbox]{margin:0; vertical-align:middle;}h1,h2,h3,h4,h5,h6{font-weight:normal;}
	@import url(//fonts.googleapis.com/earlyaccess/nanumgothic.css);
	*{ padding:0; margin:0;font-family:'Nanum Gothic', '나눔고딕', 'Dotum', '돋움', 'arial';}
	.wrap_in{max-width:640px; text-align:center; margin:0 auto; padding-bottom: 20px;}
	.wrap_on{width:100%; text-align:center; margin:0 auto;}
	.layer1 {margin: 0; padding: 0; width: 100%;}
	.content{background-color:#111;}
	.content img{display:block; margin:0 auto;}
	.item-cols:before,
	.item-cols:after { content: ''; display: block; }
	.item-cols:after { clear: both; }
	.item-list { letter-spacing: -0.07em; width: 92%; margin: 0 auto; }
	.item-list .item-cols { position: relative; margin-bottom: 15px; }
	.item-list .item-cols .item-cols { margin-bottom: 0; }
	.item-list .item-cols.agree { margin-bottom: 0; margin-top: 30px; text-align: center; font-size: 14px; letter-spacing: -0.1em; }
	.date {text-align:center; color:#000; font-size:55px; font-weight:bold; width:100%; letter-spacing:-6px;}
	.pages > div{display:none;}
	.pages > div.active{display:block; width:85%; margin:0 auto;}
	.page-2 .text-ing{}
	.page-2 .text-complate img{margin-top:-10px;}
	.page-2 .text-complate{display:none;font-weight: bold;color: #f00;}
	.page-2 .complate .text-ing{display:none !important}
	.page-2 .complate .text-complate{display:inline-block !important}
	.page-2 span[id^=count-data]{font-weight: bold;}
	.page-2 .progress-text{font-size:18px; color:#000;line-height:26px;height:26px;font-weight: bold;}
	.page-2 text{font-style: italic;font-weight: bold;font-size: 1.2em;}
	.container1{width:100%; background-color:#cfd3db;}
	.container1 img{display:block; margin:0 auto;}
	@media (max-width:768px){
		.container1 img{width:100%; display:block; margin:0 auto;}
		.popup{position:fixed;top:50%;width:100%;left:0%;margin-left:0px;margin-top:-142px;text-align:center;display:none;z-index:1001;max-width:640px;}
	}
	.container2{background-color:#1c1b20; text-align: center;}
	.container2 p{color:#bebebe; margin: 20px 0 0 0px; text-align: center; font-family: "맑은고딕"; font-size: 13px;}
	.input_wrap2 {border:0 #767676 solid; width:80%; margin:10px auto; text-align:center; padding:10px;font-size:15px; color:#000;}
	.input_wrap3 {text-align:left;}
	.input_wrap4 {border:0 #767676 solid; width:80%; margin:1px auto; text-align:center; padding:3px 5px;font-size:15px; color:#000;}
	.input_wrap5 {text-align:left;}
	.input_wrap {border:1px #767676 solid; width:80%; margin:5px auto; text-align:left; padding:5px;font-size:20px; color:#000; border-radius:30px;}
	.input_wrap input, .input_wrap select{ width:60%; background:#ebebeb; border:0; color:#000; font-size:16px;text-align:left;height:30px; }
	.input_wrap label { color:#000; width:22%; display:inline-block; padding-left:5px; padding-top: 6px;}
	.tel input, select { width:18%;}
	.fff { color:#fff;}
	.footer {width:100%; background:#fefefe;border-top:1px solid #ddd;}
	.footer div{margin:0 auto; max-width:640px;}
	.footer button{background:#fff;border:1px solid #999;margin-left:10px;padding:3px 5px;}
	footer{max-width:640px; margin:0 auto;}
	/* Overlay / Mask */
	#overlay-mask{display:none;position:fixed;top:0px;bottom:0px;left:0px;right:0px;width:100%;height:100%;z-index:1000;background:rgba(0,0,0,.7);}
	#overlay-contents{display:none;position:fixed;background:#fff;z-index:1003;overflow:hidden;padding:10px;left:50%;margin-left:-170px;top:5%;}
	#overlay-contents textarea{width:320px; height:400px;font-size: 12px;padding: 5px;resize: none;}
	#overlay-contents .close-btn{float:right;display:block;border:1px solid #000001; padding:3px 5px;color:#000001;font-size:12px;}
	#overlay-contents .close-btn:hover,#overlay-contents .close-btn:active{color:#000001;}
	#usingFunction{margin: 0 auto; text-align: center; font-size: 15px; font-weight: 600; word-spacing: 0.1em; letter-spacing: -0.01em; z-index: 500; padding: 3px; background-color: #272524;}
	.logo{font-size: 2em;color: #fff;letter-spacing: -0.1em;font-weight: bold;}
	.logo {}
	.title{padding:5% 0px;}
	.loading {z-index: 1;width: 150px;height: 150px;margin:0 auto;border: 16px solid #f3f3f3;border-radius: 50%;border-top: 16px solid #960808;width: 120px;height: 120px;-webkit-animation: spin 1s linear infinite;animation: spin 1s linear infinite;}
	.list-inline-item {display: inline-block;}
	li, ol, ul {list-style: none;}
	li {text-align: -webkit-match-parent;}
	.list-inline {padding-left: 0;list-style: none;}
	.list-inline li a{text-decoration: none; color:rgba(255,255,255,.3); font-family: "맑은고딕"; font-size: 13px;}
	.list-inline li{padding: 0 5px;}
	.list-inline li a:hover{color:orange; transition-duration: 0.7s;}
	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}
	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}
	.date_time{color:#fff; text-align:center; font-size:24px; font-weight:bold;}
	.pdb80 {padding-bottom:0px;}
	.bbb {padding:10px 0;}
	.ccc {max-width:640px; padding:15px 0; margin:0 auto; background:#ebebeb; color:#fff; font-size:27px;}
	/*move page menu */
	#movepage {width: 99%;padding: 10px;background: rgba(0,0,0,0.1);position: fixed;	-webkit-border-radius: 100px;-moz-border-radius: 100px;border-radius: 100px;-webkit-box-shadow: inset 0px 1px 2px 0px rgba(0,0,0,0.4), 0px 1px 1px 0px rgba(255,255,255,.1);-moz-box-shadow: inset 0px 1px 2px 0px rgba(0,0,0,0.4), 0px 1px 1px 0px rgba(255,255,255,.1);box-shadow: inset 0px 1px 2px 0px rgba(0,0,0,0.4), 0px 1px 1px 0px rgba(255,255,255,.1);-webkit-transition: all .2s ease;-moz-transition: all .2s ease;-ms-transition: all .2s ease;-o-transition: all .2s ease;transition: all .2s ease;z-index: 500;}
	#movepage ul {display:block;padding: 0;height: 40px;margin: 0 auto;list-style: none;overflow: hidden;-webkit-box-shadow: inset 0px 1px 1px 0px rgba(255,255,255,.2), 0px 1px 2px 0px rgba(0,0,0,0.5);-moz-box-shadow: inset 0px 1px 1px 0px rgba(255,255,255,.2), 0px 1px 2px 0px rgba(0,0,0,0.5);box-shadow: inset 0px 1px 1px 0px rgba(255,255,255,.2), 0px 1px 2px 0px rgba(0,0,0,0.5);background: #280000;background-image: -moz-linear-gradient(top, rgba(0,0,0,0) 0%, rgba(0,0,0,0.3) 100%);background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.3)));background-image: -webkit-linear-gradient(top, rgba(0,0,0,0) 0%,rgba(0,0,0,0.3) 100%);background-image: -o-linear-gradient(top, rgba(0,0,0,0) 0%,rgba(0,0,0,0.3) 100%);background-image: -ms-linear-gradient(top, rgba(0,0,0,0) 0%,rgba(0,0,0,0.3) 100%);background-image: linear-gradient(top, rgba(0,0,0,0) 0%,rgba(0,0,0,0.3) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#4d000000',GradientType=0 );-webkit-border-radius: 100px;-moz-border-radius: 100px;border-radius: 100px;}
	#movepage ul li {display: inline-block;width:49%;background: rgba(255,255,255,0.0);	text-align: center;border-right: 1px solid #666;border-left: 1px solid #444;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;-webkit-transition: all .2s ease;-moz-transition: all .2s ease;-ms-transition: all .2s ease;-o-transition: all .2s ease;transition: all .2s ease;}
	#movepage ul li:nth-child(1){background: rgba(255,255,255,0.2);}
	#movepage ul li:hover {background: rgba(255,255,255,0.08);}
	#movepage ul li:active {background: rgba(0,0,0,0.08);}
	#container ul li:first-child {border-left: none;-webkit-border-radius: 100px 0 0 100px;	-moz-border-radius: 100px 0 0 100px;	border-radius: 100px 0 0 100px;}
	#movepage ul li:last-child {border-right: none;-webkit-border-radius: 0 100px 100px 0;-moz-border-radius: 0 100px 100px 0;border-radius: 0 100px 100px 0;}
	#movepage ul li a{width: 100%;text-decoration: none;font: 16px/41px Arial, sans-serif;color: #ddd;text-transform: uppercase;text-shadow: 0px 1px rgba(0,0,0,0.5);display: inline-block;}		
	/*	탑버튼추가 디비박스*/
	.scrolltop2 {width:100%;height: 71px;	max-width: 100%;display:none;margin:0 auto;position:fixed;left:-50px;margin-left: -50;bottom:32px;}
	.scroll {width: 100%;max-width: 100%;text-align: center;margin: 0 0 0 0;cursor:pointer;transition: 0.5s;-moz-transition: 0.5s;-webkit-transition: 0.5s;-o-transition: 0.5s;}
	.scroll_img {width: 720px;height: 211px;background-image: url(/asset/img/ymkt/2/tbd3_banner.gif);background-size: 100%;background-repeat: no-repeat;margin-top: -50px;	display: inline-block;}
	/*	movepage 반응형*/	
	@media screen and (max-width: 1910px) {
		#movepage ul {width: 99%;display: inline-flex;}
		#movepage ul li a{text-decoration: none;font: 15px/41px Arial, sans-serif;color: #ddd;text-transform: uppercase;text-shadow: 0px 1px rgba(0,0,0,0.5);
	}
	@media screen and (max-width: 960px) {
		#movepage ul li{width:100%;}
		#movepage ul li a{text-decoration: none;font: 15px/41px Arial, sans-serif;color: #ddd;text-transform: uppercase;text-shadow: 0px 1px rgba(0,0,0,0.5);padding: 2px;}
	}
	/*	탑버튼추가 디비박스 반응형*/		
	@media screen and (max-width: 640px) {
		.scroll_img {width: 100%;height: 200px;background-size: 100%;background-repeat: no-repeat;background-position: center;margin-top: -60px;}
	}
</style>
<div class="container">
	<div class="co pdb80">
		<div class="logo">
			<div id="usingFunction"></div>
			<img src="/asset/img/ymkt/2/tbd3_01.jpg" alt=''>
		</div><!--Logo-->
		<div class="title" style="padding:0;">
			<img src="/asset/img/ymkt/2/time_02.jpg" alt=''>
		</div>
		<div class="countdown s-3" id="countdown">
			<span style="width:22%; height:130px;"></span>
		</div>
		<div class="imgbox" style="padding:5% 0px;">
			<img src="/asset/img/ymkt/2/time_03_4.png" alt=''>
		</div>
		<div class="bottom" id="wrap">
			<div class="wrap_in">
				<div class="pages">
					<div class="page-1 active">
						<div class="padding30" style="max-width:640px; margin:0 auto">
							<div style="width: 100%;padding:12px 0; ">
								<p style="font-size:20px; font-weight:normal; color:#fff;letter-spacing:0px; ">실시간 종목 분석 데이터</p>
								<p style="font-size:30px;  color:#fff;letter-spacing:-1px;">총<strong><span id="total-count"></span></strong>건</p>
							</div>
						</div>
						<div style="position:relative;top:10%; margin:0 auto;">
							<div class="loading"></div>
						</div>
						<div style="position:relative;top:10%;">
							<p style="font-size:20px; font-weight:normal; color:#fff;letter-spacing:-1px;padding:30px 0; text-shadow: 1px 1px 2px rgba(150, 150, 150, 1);">
								서버에서 데이터 전송 중...
							</p>
						</div>
					</div>
					<div class="page-2">
						<div style="width: 100%;padding:12px 0; ">
							<p style="font-size:20px; font-weight:normal; color:#fff;letter-spacing:0px; ">실시간 종목 분석 데이터</p>
							<p style="font-size:30px; color:#fff;letter-spacing:-1px;">총<strong><span id="total-count">2,504,372,190</span></strong>건</p>
						</div>
						<div style="position:relative;top:10%;">
							<div id="aaa1" style="max-width:640px; padding:2px 0; margin:0 auto; background:#960808; color:#fff;font-size:19px;">
								- 분석데이터 수집현황 -
							</div>
							<div class="ccc">
								<div class="bbb">
									<p class="progress-text progress-text-1">
										실시간 이슈 종목 <span id="count-data-1">0</span> 건 <span class="text-ing">분석중</span><span class="text-complate">분석완료</span>
									</p>
									<p>
										<svg id="progress-bar-1" width="80%" height="20"><rect x="0" rx="8" width="80%" height="20" fill="#000"></rect><rect x="0" rx="8" height="20" fill="#960808" width="0"></rect><g fill="#fff" text-anchor="middle" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="12"><text x="235.5" y="12" fill="#fff">0%</text></g></svg>
									</p>
								</div>
								<div class="bbb">
									<p class="progress-text progress-text-2">
										실시간 급상승 종목 <span id="count-data-2">0</span> 건 <span class="text-ing">분석중</span><span class="text-complate">분석완료</span>
									</p>
									<p>
										<svg id="progress-bar-2" width="80%" height="20"><rect x="0" rx="8" width="80%" height="20" fill="#000"></rect><rect x="0" rx="8" height="20" fill="#960808" width="0"></rect><g fill="#fff" text-anchor="middle" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="12"><text x="235.5" y="12" fill="#fff">0%</text></g></svg>
									</p>
								</div>
								<div class="bbb">
									<p class="progress-text progress-text-3">
										최근 3개월 시장상황, 기업분석, 관련뉴스 <span id="count-data-3">0</span> 건 <span class="text-ing">분석중</span><span class="text-complate">분석완료</span>
									</p>
									<p>
										<svg id="progress-bar-3" width="80%" height="20"><rect x="0" rx="8" width="80%" height="20" fill="#000"></rect><rect x="0" rx="8" height="20" fill="#960808" width="0"></rect><g fill="#fff" text-anchor="middle" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="12"><text x="235.5" y="12" fill="#fff">0%</text></g></svg>
									</p>
								</div>
								<div class="bbb">
									<p class="progress-text progress-text-4">
										업종별 상승예상 종목 <span id="count-data-4">0</span> 건 <span class="text-ing">분석중</span><span class="text-complate">분석완료</span>
									</p>
									<p>
										<svg id="progress-bar-4" width="80%" height="20"><rect x="0" rx="8" width="80%" height="20" fill="#000"></rect><rect x="0" rx="8" height="20" fill="#960808" width="0"></rect><g fill="#fff" text-anchor="middle" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="12"><text x="235.5" y="12" fill="#fff">0%</text></g></svg>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="page-3">
						<div style="width: 100%;padding:12px 0; ">
							<p style="font-size:20px; font-weight:normal; color:#fff;letter-spacing:0px; ">실시간 종목 분석 데이터</p>
							<p style="font-size:30px;  color:#fff;letter-spacing:-1px;">총<strong><span id="total-count">2,504,372,190</span></strong>건</p>
						</div>
						<div style="position:relative;top: 10%; max-width:640px; margin:0 auto;">
							<div style="color:#fff; letter-spacing:-4px; padding:8px 0;  background:#960808;">
								<p style="font-size:20px; font-weight:bold; color:#fff; letter-spacing:-1px;">실시간 급등주 종목 분석 완료</p>
								<p style="font-size:12px; font-family:'NanumGothic'; padding-top:3px; letter-spacing:0;">-평생 무료이벤트-</p>
							</div>
							<div style="padding:2px 0px; background: #ebebeb;">
								<div class="input_wrap"><label>이름</label><input id="ndg_name_1" name="ndg_name_1" type="text" maxlength="6"></div>
								<div class="input_wrap tel"><label>연락처</label>
									<select type="text" name="ndg_tel1_1" id="ndg_tel1_1" maxlength="3" style="width:18%;">
										<option value="010">010</option>
										<option value="011">011</option>
										<option value="016">016</option>
										<option value="017">017</option>
										<option value="018">018</option>
										<option value="019">019</option>
									</select>
									-<input type="tel" name="ndg_tel2_1" id="ndg_tel2_1" maxlength="4" class="onlyNum" />
									-<input type="tel" name="ndg_tel3_1" id="ndg_tel3_1" maxlength="4" class="onlyNum" />
								</div>
								<div class="input_wrap4">
									<div class="input_wrap5">
										<input type="checkbox" id='agree_all_1' checked value="y" valign="top" onClick="check_all(this,'_1')">
										<label for="agree_all_1"><span class='comment0' onClick="check_all2('2','y')">아래 약관에 모두 동의합니다.</span><label>
									</div>
								</div>
								<div class="input_wrap4">
									<div class="input_wrap5">
										<input type="checkbox" id='agree_1' value="필수" valign="center" checked>
										<label for="agree_1"><span class='comment1'>서비스 이용약관동의</span></label><a style="color:red;cursor:pointer"><span class='comment1' data-remodal-target="popTermService">[보기]</span></a>
										<input type="checkbox" id='agree2_1' value="선택" valign="center" checked>
										<label for="agree2_1"><span class='comment1'>개인정보 수집동의 및 이용동의</span>
										</label><a style="color:red;cursor:pointer"><span class='comment1' data-remodal-target="popTermPersonalInfo">[보기]</span></a>
									</div>
								</div>
							</div>
							<div class="padding30" style="background: #ebebeb;padding:5px 0;">
								<img src="/asset/img/ymkt/2/time_button.png" class="btn_join" data-opt="_1" width="90%" style="cursor:pointer;">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--div class= bottom-->
		<div id="ta" class="content" style="max-width:640px;padding-top:25px;">
			<img src="/asset/img/ymkt/2/tbd3_03.jpg">
			<img src="/asset/img/ymkt/2/tbd3_04.jpg">
			<img src="/asset/img/ymkt/2/tbd3_05.jpg">
			<a href="http://www.ymktgroup.com/default/cpa_db_portfolio/sub1.php" target="_blank"><img src="/asset/img/ymkt/2/tbd3_06.jpg" style="padding-bottom: 150px;">	</a>
			<div class='scrolltop2'>
				<a style="cursor:pointer;">
					<div class='scroll'><div class="scroll_img"></div></div>				
				</a>
			</div>
		</div>
	</div><!--co pdb80-->
</div><!--container-->
<div class="popup">
	<div style="color:#fff; letter-spacing:-4px; padding:8px 0;  background:#960808;">
		<p style="font-size:20px; font-weight:bold; color:#fff; letter-spacing:-1px;">노다지 주식정보 평생무료 이벤트</p>
	</div>
	<div style="padding:2px 0px; background: #ebebeb;">
		<div class="input_wrap"><label>이름</label><input id="ndg_name_2" name="ndg_name_2" type="text" maxlength="6"></div>
		<div class="input_wrap tel"><label>연락처</label>
			<select type="text" name="ndg_tel1_2" id="ndg_tel1_2" maxlength="3" style="width:20%;">
				<option value="010">010</option>
				<option value="011">011</option>
				<option value="016">016</option>
				<option value="017">017</option>
				<option value="018">018</option>
				<option value="019">019</option>
			</select>
			-<input type="tel" name="ndg_tel2_2" id="ndg_tel2_2" maxlength="4" class="onlyNum">
			-<input type="tel" name="ndg_tel3_2" id="ndg_tel3_2" maxlength="4" class="onlyNum">
		</div>
		<div class="input_wrap4">
			<div class="input_wrap5">
				<input type="checkbox" id="agree_all_2" checked="" value="y" valign="top" onclick="check_all(this,'_2')">
				<label for="agree_all_2"><span class="comment0">아래 약관에 모두 동의합니다.</span></label>
			</div>
		</div>
		<div class="input_wrap4">
			<div class="input_wrap5">
				<input type="checkbox" id="agree_2" value="필수" valign="center" checked>
				<label for="agree_2"><span class="comment1">서비스 이용약관동의</span></label><a style="color:red;cursor:pointer"><span class="comment1" data-remodal-target="popTermService">[보기]</span></a>	<br/>
				<input type="checkbox" id="agree2_2" value="선택" valign="center" checked>
				<label for="agree2_2"><span class="comment1">개인정보 수집동의 및 이용동의</span></label><a style="color:red;cursor:pointer"><span class="comment1" data-remodal-target="popTermPersonalInfo">[보기]</span></a>
			</div>
		</div>
	</div>
	<div class="padding30" style="background: #ebebeb;padding:5px 0;">
		<img src="/asset/img/ymkt/2/time_button.png" class="btn_join" data-opt="_2" width="90%" style="cursor:pointer;">
	</div>
</div>
<div id="mask"></div>

<!-- [Start] Popup - Terms:서비스 이용약관 -->
<div class="remodal terms" data-remodal-id="popTermService">
	<div class="term_container">
		<p><?=$terms['txTerms']?></p>
	</div>
	<div class="pop_bottom">
		<button class="btn_m full" data-remodal-action="confirm">OK</button>
	</div>
</div>
<!-- [End] Popup - Terms:서비스 이용약관 -->

<!-- [Start] Popup - Terms:개인정보 수집 및 이용 -->
<div class="remodal terms" data-remodal-id="popTermPersonalInfo">
	<div class="term_container">
		<p><?=$terms['txPrivacy']?></p>
	</div>
	<div class="pop_bottom">
		<button class="btn_m full" data-remodal-action="confirm">OK</button>
	</div>
</div>
<!-- [End] Popup - Terms:개인정보 수집 및 이용 -->

<script type="text/javascript" src="//js.frubil.info/"></script>
<script>
	$(function(){
		$(".scroll").click(function(){
			var maskWidth = $(document).width(); 
			var maskHeight = $(document).height();
			$('#mask').css({'width':maskWidth,'height':maskHeight}); 
			$("#mask").show();
			$(".popup").show();
		});
		$('#mask').click(function () {
			$('#mask').hide();
			$('.popup').hide();
		});

		var count = 3;
		function countdown() {
			count--;
			if (count <= 0) {
				countdown_end();
				return  false;
			}
			$('#countdown').prop('class', 'countdown').addClass('s-'+count);
			setTimeout(countdown, 700);
		}

		function countdown_end() {
			$('#countdown').prop('class', 'countdown').addClass('end');
		}
		// progress
		var status = new Array();
		var progress_per = 0;
		var $bar = $('.progress-bar .bar i');
		var $status = $('.progress-bar .status');
		var $per = $('.progress-bar .per');
		var progress_hd = 0;

		setTimeout(function() {
			$('#countdown').hide();
			$('.bottom').css('opacity','1').fadeIn(200);
			$('.imgbox').css('display','none');
			progress_go();
		}, 2200)
		
		function progress_go(){
			var mwv = 0;
			var usrAgent_check = navigator.userAgent, usrAgent_match = usrAgent_check.match(/iPad|BlackBerry|Android|LG|MOT|SAMSUNG|SonyEricsson|Mobile|Windows CE|Windows Phone|Opera Mini|POLARIS/);
			if (usrAgent_check.match(usrAgent_match) != null ){ 
				mwv = 230;
				$(".page-2 .progress-text").css("font-size","12px");
				$(".input_wrap").css("font-size","15px");
				$("#brr").html("<br />");
				$("#aaa1").css("padding","2px 0");
				$(".bbb").css("padding","0");
				$(".ccc").css("padding","5px 0");
			}else{
				mwv = 420;
			}
			$(".title").hide();
			var options = {
				useEasing: true,
				useGrouping: true,
				separator: ',',
				decimal: '.',
			};
			var count1 = new CountUp('total-count', 0, 2504372190, 0, 1.1, options);
			var count2 = new CountUp('count-data-1', 0, 13253, 0, 1.0, options);
			var count3 = new CountUp('count-data-2', 0, 362468, 0, 1.0, options);
			var count4 = new CountUp('count-data-3', 0, 936794, 0, 1.0, options);
			var count5 = new CountUp('count-data-4', 0, 558433, 0, 1.0, options);
			var progressOption = {
				percent: 0,
				width: mwv,
				height: 20,
				fontSize: 12,
				backgroundColor: '#000',
				barColor: '#960808',
				fontColor: '#fff',
				radius: 8,
				increaseTime: 1000.00/100.00,
				increaseSpeed: 7
			}
			var progress1 = $("#progress-bar-1").Progress(progressOption);
			var progress2 = $("#progress-bar-2").Progress(progressOption);
			var progress3 = $("#progress-bar-3").Progress(progressOption);
			var progress4 = $("#progress-bar-4").Progress(progressOption);

			function viewPage(page){
				$('.pages .active').removeClass('active');
				switch(page){
					case 2:
						$('.pages .page-2').addClass('active');
						count2.start();
						progress1.percent(100);
						setTimeout(function(){
							$('.progress-text-1').addClass('complate');
							count3.start();
							progress2.percent(100);
							setTimeout(function(){
								$('.progress-text-2').addClass('complate');
								count4.start();
								progress3.percent(100);
								setTimeout(function(){
									$('.progress-text-3').addClass('complate');
									count5.start();
									progress4.percent(100);
									setTimeout(function(){
										$('.progress-text-4').addClass('complate');

										setTimeout(function(){
											viewPage(3);
										}, 300);
									}, 300);
								}, 400);
							}, 300);
						}, 300);
						break;
					case 3:
						$('.pages .page-3').addClass('active');
						break;
				}
			}
			jQuery(function($){
				count1.start();
				setTimeout(function(){
					$('.loader').remove();
					setTimeout(function(){
						viewPage(2);
					}, 1000);
				}, 1000);
			});
		}
		$('.footer_web').remove();
		setTimeout(countdown, 1000);
		$('.onlyNum').on('input', inputOnlyNumber);
		function inputOnlyNumber(){
			$(this).val( $(this).val().replace(/\D/g,''));
		}
	});

	$(window).scroll(function() {
		if($(this).scrollTop() > 900 ) {   // 버튼표시 화면비율
			$('.scrolltop2:hidden').stop(true, true).fadeIn();
		} else {
			$('.scrolltop2').stop(true, true).fadeOut();
		}
	});

	var brand = FRUBIL.device.brand;         // Samsung
	var market = FRUBIL.device.marketname;    // Galaxy A5 (2016)

	$(".btn_join").click(function(){
		var opt=$(this).data("opt");
		var phone = $('#ndg_tel1'+opt).val()+$('#ndg_tel2'+opt).val()+$('#ndg_tel3'+opt).val();
		var name = $('#ndg_name'+opt).val();

		if ( !name || name == "" || name == null || name == undefined ) {
			alert("이름을 작성해주세요.");
			return false;
		}

		if ( !phone || phone == "" || phone == null || phone == undefined ) {
			alert("휴대폰 번호를 정확히 작성 해주세요.");
			return false;
		}
		if ( !(/^[0-9]+$/).test(phone) ){
			alert("숫자만 가능합니다.");
			return false;
		}
		if ( !(/^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/).test(phone) ){
			alert("휴대폰 번호를 확인해 주세요");
			return false;
		}
		if(!$('#agree'+opt).prop('checked')) {
			alert('서비스 이용약관 이용에 동의해야합니다.');
			return false;
		}
		if(!$('#agree2'+opt).prop('checked')) {
			alert('개인정보 수집 이용에 동의해야합니다.');
			return false;
		}
		var dev=brand+'-'+market;
		var data={
			"device":dev,
			"username":name,
			"phone":phone,
			"code":"ymkt_2"
		}
		$.ajax({
			type : "post",
			url : "/main/loginjoin",
			dataType : 'json',
			data : data,
			success : function(response) {
//				console.log(response)
				if( response.result == 'error' ) {
					alert(response.msg);
				}
				if( response.result == 'success') {
					if(response.msg=='join'){
						$.post("/main/modifyNick", {nick:name+'<?=rand(100,999)?>'},function(jqXHR) {;},'json');
						alert('이벤트 신청이 완료 되었습니다.\n더 편리하고 빠른 노다지App 버전(설치만 해도 평생무료) 이용 가이드를 문자로 보내드립니다.');
						if(window.adbrix) window.adbrix.userRegister(encodeURI(name), encodeURI(phone));//201217 adbrix
						gtag('event', 'login', {'event_category' : 'success', 'event_action' : 'input', 'event_label' : 'db'});//201214 ymkt
					}else{
						alert('회원님은 이미 가입하셨습니다.');
					}
					window.location.href = '/info/important';
				}
			},
			error : function(xhr, status, error) {},
		})
	});

	function check_all(target,opt){
		if($(target).prop('checked') == true){
			$("#agree"+opt).prop("checked",true);
			if($("#agree2"+opt).length>0) $("#agree2"+opt).prop("checked",true);
			if($("#agree3"+opt).length>0) $("#agree3"+opt).prop("checked",true);
		}else{
			$("#agree"+opt).prop("checked",false);
			if($("#agree2"+opt).length>0) $("#agree2"+opt).prop("checked",false);
			if($("#agree3"+opt).length>0) $("#agree3"+opt).prop("checked",false);
		}
	}
</script>
<script type="text/javascript">
	function locale (){ 	 return new Date().toLocaleString(); 	 } 
	document.getElementById( 'usingFunction' ).innerHTML = locale(); 
	// 추가로, 실시간 타이머 표시 방법 ㅡ 1000 밀리초(=1초)에 한번씩 함수 실행하기 
	setInterval ( function() { document.getElementById("usingFunction").innerHTML = locale(); } , 1000 );
</script>