<div class="purchase_cautions">
	<h2 class="tit_caution">구매 유의사항</h2>
	<ul class="txt_list type2">
		<li>스마트퓨쳐는 서비스 플랫폼의 제공자로서, 포인트로 구매하는 종목의 컨텐츠의 제공 당사자가 아니며 종목발굴 컨텐츠의 제공, 운영 및 관리 등과 관련한 의무와 책임은 ㈜엔더블유홀딩스에게 있습니다</li>
		<li>포인트로는 정기구독 및 기간 구독 상품 구매가 불가 합니다.  </li>
		<li>주식투자는 원금손실의 위험이 있으며, 본 서비스는 단순히 투자조언에 불과할 뿐 그 정확성을 담보할 수 없습니다.</li>
		<li>포인트로 유료종목 구매 후 목표기간 내 손절가 도달 시, 구매 포인트에 상응하는 포인트로 지급합니다. </li>
		<li><button type="button" data-remodal-target="popTermPoint">[자세히보기]</button></li>
	</ul>
	
</div>









