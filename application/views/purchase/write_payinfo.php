<div class="form_container">
	<div class="input_box">
		<label for="vCardNo">카드번호</label>
		<input type="tel" name="vCardNo" id="vCardNo" placeholder="'-'없이 숫자만 입력하세요" value="<?=$vCardNo?>" >
	</div>
	<div class="input_box">
		<label for="vExpdt">유효기간</label>
		<input type="tel" name="vExpdt" id="vExpdt" placeholder="입력예시(월/년) : 0525" value="<?=$vExpdt?>">
	</div>
	<div class="input_box">
		<label for="vName">주문자명</label>
		<input type="text" name="vName" id="vName" placeholder="특수문자 사용금지" value="<?=$vName?>">
	</div>
	<div class="input_box">
		<label for="vPhone">전화번호</label>
		<input type="tel" name="vPhone" id="vPhone" placeholder="'-'없이 숫자만 입력하세요" value="<?=$vPhone?>">
	</div>
</div>
<div class="bottom_check">
	<label class="checkbox type_agree">
		<input type="checkbox" name="allCheck" id="allCheck">
		<i></i>아래 사항에 모두 동의 합니다.
	</label><br>
	<label class="checkbox type_agree" for="service_agree">
		<input type="checkbox" name="agree[]" class="agree" id="service_agree" value="y" >
		<i></i>서비스 이용약관 동의
	</label>
	<button type="button" data-remodal-target="popTermService" style="margin-right:0.5em">[필수]</button><br>
	<label class="checkbox type_agree" for="info_agree">
		<input type="checkbox" name="agree[]" class="agree" id="info_agree" value="y" >
		<i></i>개인정보 수집 및 이용동의
	</label>
	<button type="button" data-remodal-target="popTermPersonalInfo">[필수]</button><br>
	<label class="checkbox type_agree">
		<input type="checkbox" name="agree[]" class="agree" id="marketing_agree" value="y" >
		<i></i>마케팅정보 수신 동의
	</label>
	<button type="button" data-remodal-target="popTermMarketing">[선택]</button>
</div>
<?php
if(isset($page)){
	if($page == "point"){
		include_once(VIEW_PATH.'/purchase/inc_purchase.php');
	}
}
?>
<div class="btn_container" style="margin-top:20px;">
	<button type="button" class="btn_l full bg_red btn-buy">노다지 주식정보 구독하기</button>
</div>

<img src="../asset/img/banner_authentication.jpg" alt="" class="img_banner">