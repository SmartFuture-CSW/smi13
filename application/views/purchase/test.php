<?if($this->pcmode=='' && $this->andapp=='1') echo '<script src="/cordova/android/cordova.js"></script>'; else if($this->pcmode=='ios') echo '<script src="/cordova/ios/cordova.js"></script>';?>
<style>

/*.goods_icon{float:left; width:20%; height:100%; }*/
/*.goods_icon img{ width:80%;vertical-align:middle;  }*/
.point_name{float:left; width:50%; }

.point_icon:nth-of-type(1){background:url("/asset/img/subs/img_point_10.png"); background-size:10%; background-position:2% 50%; background-repeat:no-repeat; background-color:#fff;}
.point_icon:nth-of-type(2){background:url("/asset/img/subs/img_point_25.png"); background-size:10%; background-position:2% 50%; background-repeat:no-repeat; background-color:#fff;}
.point_icon:nth-of-type(3){background:url("/asset/img/subs/img_point_50.png"); background-size:10%; background-position:2% 50%; background-repeat:no-repeat; background-color:#fff;}
.point_icon:nth-of-type(4){background:url("/asset/img/subs/img_point_70.png"); background-size:10%; background-position:2% 50%; background-repeat:no-repeat; background-color:#fff;}
.point_icon:nth-of-type(5){background:url("/asset/img/subs/img_point_100.png"); background-size:10%; background-position:2% 50%; background-repeat:no-repeat; background-color:#fff;}
.point_icon{ padding-left:15% !important; float:left; }

.point_name h4{font-size:15px; font-weight:700; float:left; width:100%; }

.choice_box{background-color:red;}

.point_add{
background-color: #f62d0f;
color: #fff;
float:left;
line-height: 28px;
margin-top:0.1em;
font-size:12px !important;
border-radius: 5px;
box-shadow: 0 1px 3px #d4d4d4;
box-sizing: border-box;
text-align: center;
width:45%;
}

.point_price{
background-color: #f62d0f;
color: #fff;
display: inline-block;
line-height: 35px;
margin-top:0.5em;
padding: 0 0.9em;
font-size: 15px;
border-radius: 5px;
box-shadow: 0 1px 3px #d4d4d4;
box-sizing: border-box;
text-align: center;
float:right;
min-width:24%;
}

.point_choise{ width:100%; height:auto; padding:0.9em 0 0.5em 0;  float:left;}
.point_choise dl{width:100%; float:left; height:auto;}
.point_choise dt, dd{letter-spacing: -0.09em; margin:0; padding:0; line-height:24px; float:left;}
.point_choise dt{width:30%; height:30px; font-size:16px; font-weight:700; color:#000; }
.point_choise dd{width:70%;  text-align:right; color:#000; font-weight:700; font-size:16px;}
.point_choise dd:last-child{width:100%; text-align:right; color:#f62d0f;}

.point_result{  height:auto; padding:0.9em 0 1.5em 0;  float:left; width:100%;}
.point_resut_center{margin:0px 15px;}
.point_result dl{width:100%; float:left; height:auto; }
.point_result dt, dd{letter-spacing: -0.01em; margin:0; padding:0; line-height:24px; float:left;}
.point_result dt{width:30%; height:30px; font-size:18px; font-weight:700; color:#f62d0f; }
.point_result dd{width:70%;  text-align:right; color:#f62d0f; font-weight:700; font-size:18px;}


.purchase_cautions{padding:10px 0px;}

</style>

	<div class="app_wrapper">	
		<? include_once(VIEW_PATH.'/include/header_app.php'); ?>

		<!-- [Start] App Main -->
		<div class="app_main subs03" style="display:block;">

<form name="pay" method="post">
	<input type="hidden" name="emPayYN" value="<?=$emPayYN?>">
			<div class="choice_box">
				<a class="img_banner" href="" style="margin-top:4px;">
					<img src="../asset/img/subs/subs_banner4.jpg" alt="" class="goodsImg">
				</a>
				<div class="subscribe_order layout_center" style="background-color:#eee; float:left;">
					<div>
						<div class="radio_container select_service clearfix">
<?php 
		$i=1;
		foreach($goods as $row){
		?>
								<input type="radio" name="nGoodsNo" id="item1_<?=$i?>"  value="<?=$row['nSeqNo']?>" class="a11y_hidden" <?=($i==1)?'checked' : ''; ?> data-point="<?=$row['nPoint']?>" data-addpoint="<?=$row['nAddPoint']?>" data-price="<?=$row['nPrice']?>">
								<label for="item1_<?=$i?>" class="clearfix point_icon">
									<span class="point_name">
										<h4><?=$row['vGoodsName']?></h4>
										<p class="point_add">
											+<?=$row['nAddPoint']/10000?>만 포인트
										</p>
									</span>
									<p class="point_price">
										<?=number_format($row['nPrice'])?>원
									</p>
								</label>
		<?php
			$i++;
		}
		?>
						</div>
					</div>
					<div class="point_choise">
						<dl>
							<dt>선택상품</dt>
							<dd class="chosie_point">10만 포인트 + 1만 포인트 추가</dd>
							<dd><strong class="chosie_total_point">총 11만 포인트</strong></dd>
						</dl>
					</div>
				</div>
			</div>
			<div class="point_result">
				<div class="point_resut_center">
					<dl>
						<dt>총 구매금액</dt>
						<dd><strong class="chosie_price">100,000원</strong></dd>
					</dl>
				</div>
			</div>


			
			<div class="write_form">
				<div class="subscribe_order layout_center">
					<?php $page='point'; include(VIEW_PATH.'/purchase/write_payinfo.php');  // 결제 정보 입력 폼?>
				</div>
			</div>
</form>

		</div>
		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<!-- [Start] Popup - Terms:서비스 이용약관 -->
	<div class="remodal terms" data-remodal-id="popTermService">
		<div class="term_container">
		<p><?=$terms['txTerms']?><?//php include_once (TEXT."/service_agree.txt"); ?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:서비스 이용약관 -->

	<!-- [Start] Popup - Terms:개인정보 수집 및 이용 -->
	<div class="remodal terms" data-remodal-id="popTermPersonalInfo">
		<div class="term_container">
		<p><?=$terms['txPrivacy']?><?//php include_once (TEXT."/info_agree.txt"); ?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:개인정보 수집 및 이용 -->

	<!-- [Start] Popup - Terms:마케팅정보 수신 동의 -->
	<div class="remodal terms" data-remodal-id="popTermMarketing">
		<div class="term_container">
		<p><?=$terms['txAdterms']?><?//php include_once (TEXT."/marketing_agree.txt"); ?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:마케팅정보 수신 동의 -->

	<!-- [Start] Popup - Terms:마케팅정보 수신 동의 -->
	<div class="remodal terms" data-remodal-id="popTermPoint">
		<div class="term_container">
		<p><?=$terms['txPointterms']?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:마케팅정보 수신 동의 -->





<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>
<div class="remodal alert" data-remodal-id="ing" id="ing">
	<p>결제가 진행중 입니다.</p>
</div>
<!-- in script -->

<script>
(function(){
	var deviceType = localStorage.getItem("deviceType");

	$(".btn-buy").text("포인트 구매하기");


	$(".dwld").on('click', function(){
		if(deviceType == "iOS" || deviceType == "Android") {
			cordova.InAppBrowser.open('/subscribe/vipapp', '_system', 'location=no');
		}else{
			window.location.href = "/subscribe/vipapp";
		}
	});

	var number_format = function(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	$("input[name=nGoodsNo]").on("click", function(){
		var point = $(this).data("point");
		var addpoint = $(this).data("addpoint");
		var price = $(this).data("price");
		$(".chosie_point").text(point/10000 + "만 포인트 + " + addpoint/10000 + "만 포인트 추가");
		$(".chosie_total_point").text("총 "+(point + addpoint)/10000 + "만 포인트" );
		$(".chosie_price").text(number_format(price) + "원");
	});


	// 전체 체크 
	$("#allCheck").change(function(){
		if($(this).is(':checked') == true){
			$(".agree").prop('checked', true);
		}
		else{
			$(".agree").prop('checked', false);
		}
	});

	// 체크박스 한개라도 체크시 전체 체크사라지게 
	$("input[name='agree[]']").change(function(){ 
		var agree_1 = Is.checkBox( 'info_agree'),
			agree_2 = Is.checkBox( 'service_agree'),
			agree_3 = Is.checkBox( 'marketing_agree'); 
		if(agree_1==true && agree_2==true && agree_3==true) $('#allCheck').prop('checked', true);
		else $('#allCheck').prop('checked', false);
	});


	// 구독처리
	$(".btn-buy").on("click", function(){
		

		var card_no = $("#vCardNo").val();
		var expdt = $("#vExpdt").val();
		var name = $("#vName").val();
		var phone = $("#vPhone").val();


		if ( !card_no || card_no == "" || card_no == null || card_no == undefined ) {
			Cmmn.alertMsg("카드번호를 작성해주세요.");
			return false;
		}
		<?php if($emPayYN != "Y"){?>
		if ( !(/^[0-9]+$/).test(card_no) ){
			Cmmn.alertMsg("카드번호는 숫자만 가능합니다.");
			return false;
		}
		<?php } ?>

		if ( !expdt || expdt == "" || expdt == null || expdt == undefined ) {
			Cmmn.alertMsg("유효기간을 작성해주세요.");
			return false;
		}
		<?php if($emPayYN != "Y"){?>
		if ( !(/^[0-9]+$/).test(expdt) ){
			Cmmn.alertMsg("유효기간은 숫자만 가능합니다.");
			return false;
		}
		<?php }?>

		if ( !name || name == "" || name == null || name == undefined ) {
			Cmmn.alertMsg("이름을 작성해주세요.");
			return false;
		}

		if ( !phone || phone == "" || phone == null || phone == undefined ) {
			Cmmn.alertMsg("전화번호를 작성해주세요.");
			return false;
		}
		<?php if($emPayYN != "Y"){?>
		if ( !(/^[0-9]+$/).test(phone) ){
			Cmmn.alertMsg("전화번호는 숫자만 가능합니다.");
			return false;
		}
		<?php }?>

		// 동의 체크 
		if( !Is.checkBox( 'service_agree') ) {
			Cmmn.alertMsg('서비스 이용약관 이용에 동의해야합니다.');
			return false;
		}
		if( !Is.checkBox( 'info_agree') ) {
			Cmmn.alertMsg('개인정보 수집 이용에 동의해야합니다.');
			return false;
		}

		var u = "/purchase/postPointPayInfo";
		var data = $("form[name=pay]").serialize();

		Cmmn.alertId('ing');

		$.post({
			type : "post", url : u, dataType : 'json', data : data,
			success : function(response) {
				console.log(response);
				if( response.result == 'SUCCESS') {
					//Cmmn.alertMsg('결제가 완료 되었습니다.');
					location.href = "/subscribe/pointcomplete";
				} else {
					Cmmn.alertMsg(response.msg);
				}
			},
			error : function(xhr, status, error) {},
		})
	});


})();
</script>