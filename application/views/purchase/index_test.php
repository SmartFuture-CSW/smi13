<?if($this->pcmode=='' && $this->andapp=='1') echo '<script src="/cordova/android/cordova.js"></script>'; else if($this->pcmode=='ios') echo '<script src="/cordova/ios/cordova.js"></script>';?>
	<style>
		.choice_box{display:none;}
		.choice_box.active{display:block;}
		.subs_description{text-align:left; }
		.subs_description p{margin-top:0px;}
		.subs_description em{color:#F52E0F;}
		.sub_descr{display:none;}
		.pop_bottom btn-m
	</style>
	<div class="app_wrapper">	
		<? include_once(VIEW_PATH.'/include/header_app.php'); ?>

		<!-- [Start] App Main -->
		<div class="app_main subs03" style="display:block;">
			<div class="category_container ">
				<div class="layout_center" style="padding:10px 5px 1px 5px;">
					<a href="" data-kind="1" class="goods_kind active pay_basic" style="width:32.5%;">노다지 구독</a>
					<a href="" data-kind="2" class="goods_kind pay_sms" style="width:32.5%; margin:0px">프리미엄 문자반</a>
					<a href="" data-kind="3" class="goods_kind pay_vip" style="width:32.5%; margin:0px">프리미엄 VIP</a>
				</div>
			</div>

<form name="pay" method="post">
	<input type="hidden" name="emPayYN" value="<?=$emPayYN?>">
<?php if(in_array('1', $buyGoods)){ ?>
	<input type="hidden" name="buyGoodsKind1" value="Y">
			<div class="layout_center choice_box active" >
				<div class="inner_container" style="margin-top:40px;">
					<h1 class="title_check">
						<em>노다지 주식정보</em>를<br>
						구독해 주셔서 감사합니다
					</h1>
					<p>
						노다지 주식정보를 더 완벽하게할<br>
						구독자 필수 APP
					</p>
					<button type="button" class="btn_l full bg_red dwld">구독자 전용 앱 다운로드</button>
					<p>
						노다지 VIP구독 서비스는<br> 
						<em>구독자 전용 앱</em>을 통해서 받으실 수 있습니다.
					</p>
					<p>
						<a href="/purchase?topay=pay&paytype=sms"><img src="/asset/img/banner_paycomplete_sms.png" width="100%" /></a>
						<a href="/purchase?topay=pay&paytype=vip"><img src="/asset/img/banner_paycomplete_vip.png" width="100%" /></a>
					</p>
				</div>
			</div>
<?php } else { ?>
	<input type="hidden" name="buyGoodsKind1" value="N">
			<div class="choice_box active">
				<a class="img_banner" href="" style="margin-top:4px;">
					<img src="../asset/img/subs/subs_banner1.jpg" alt="" class="goodsImg">
				</a>
				<div class="subscribe_order layout_center">
					<div class="subs_description"><p></p></div>
					<div>
						<h3>노다지 구독 서비스 상품 선택</h3>
						<div class="radio_container select_service clearfix">

		<?php 
		$i=1;
		foreach($goods[1] as $row){
		?>
								<input type="radio" name="nGoodsNo" id="item1_<?=$i?>"  value="<?=$row['nSeqNo']?>" class="a11y_hidden" <?=($i==1)?'checked' : ''; ?>>
								<label for="item1_<?=$i?>" class="clearfix">
									<span><?=$row['vGoodsName']?></span>
									<p class="right_side">
										<small><?=$row['vGoodsInfo']?></small>
										<strong><?=number_format($row['nPrice'])?></strong>원
									</p>
								</label>
		<?php
			$i++;
		}
		?>
						</div>
					</div>
				</div>
			</div>
<?php } ?>

<?php if(in_array('2', $buyGoods)){ ?>
	<input type="hidden" name="buyGoodsKind2" value="Y">
			<div class="layout_center choice_box">
				<div class="inner_container" style="margin-top:40px;">
					<h1 class="title_check">
						<em>프리미엄 문자반</em>을<br>
						구독해 주셔서 감사합니다
					</h1>
					<p>
						프리미엄 문자반은 종목 매수매도 정보 및<br>
						주식 정보를 <em>100% 실시간 문자</em>로
						제공받아 볼 수 있는 상품입니다.
					</p>
					<p>
						프리미엄 문자반 서비스는<br>
						<em>가입 시 등록한 번호</em>통해서 <em>문자</em>로<br>
						받으실 수 있습니다.
					</p>
					<p>
						<a href="/purchase?topay=pay&paytype=basic"><img src="/asset/img/banner_paycomplete_basic.png" width="100%" /></a>
						<a href="/purchase?topay=pay&paytype=vip"><img src="/asset/img/banner_paycomplete_vip.png" width="100%" /></a>
					</p>
				</div>
			</div>
<?php } else {?>
	<input type="hidden" name="buyGoodsKind2" value="N">
			<div class="choice_box">
				<a class="img_banner" href="" style="margin-top:4px;">
					<img src="../asset/img/subs/subs_banner1.jpg" alt="" class="goodsImg">
				</a>
				<div class="subscribe_order layout_center">
					<div class="subs_description"><p></p></div>
					<div>
						<h3>프리미엄 문자반 정기구독 결제</h3>
						<div class="radio_container select_service clearfix">
<?php 
	$i=1;
	foreach($goods[2] as $row){
?>
							<input type="radio" name="nGoodsNo" id="item2_<?=$i?>"  value="<?=$row['nSeqNo']?>" class="a11y_hidden" <?=($i==1)?'checked' : ''; ?>>
							<label for="item2_<?=$i?>" class="clearfix">
								<span><?=$row['vGoodsName']?></span>
								<p class="right_side">
									<small><?=$row['vGoodsInfo']?></small>
									<strong><?=number_format($row['nPrice'])?></strong>원
								</p>
							</label>
<?php
		$i++;
	}
	?>
						</div>
					</div>
				</div>
			</div>
<?php } ?>

<?php if(in_array('3', $buyGoods)){ ?>
	<input type="hidden" name="buyGoodsKind3" value="Y">

			<div class="layout_center choice_box">
				<div class="inner_container" style="margin-top:40px;">
					<h1 class="title_check">
						<em>프리미엄 VIP</em>를<br>
						구독해 주셔서 감사합니다
					</h1>
					<p>
						프리미엄 VIP 더 완벽하게할<br>
						구독자 필수 APP
					</p>
					<button type="button" class="btn_l full bg_red dwld">구독자 전용 앱 다운로드</button>
					<p>
						프리미엄 VIP 구독 서비스는<br> 
						<em>구독자 전용 앱</em>을 통해서 받으실 수 있습니다.
					</p>
					<p>
						<a href="/purchase?topay=pay&paytype=basic"><img src="/asset/img/banner_paycomplete_basic.png" width="100%" /></a>
						<a href="/purchase?topay=pay&paytype=sms"><img src="/asset/img/banner_paycomplete_sms.png" width="100%" /></a>
					</p>
				</div>
			</div>

<?php } else {?>
	<input type="hidden" name="buyGoodsKind3" value="N">
			<div class="choice_box">
				<a class="img_banner" href="" style="margin-top:4px;">
					<img src="../asset/img/subs/subs_banner1.jpg" alt="" class="goodsImg">
				</a>
				<div class="subscribe_order layout_center">
					<div class="subs_description"><p></p></div>
					<div>
						<h3>프리미엄 VIP 정기구독 결제</h3>
						<div class="radio_container select_service clearfix">

<?php 
	$i=1;
	foreach($goods[3] as $row){
?>
							<input type="radio" name="nGoodsNo" id="item3_<?=$i?>"  value="<?=$row['nSeqNo']?>" class="a11y_hidden" <?=($i==1)?'checked' : ''; ?>>
							<label for="item3_<?=$i?>" class="clearfix">
								<span><?=$row['vGoodsName']?></span>
								<p class="right_side">
									<small><?=$row['vGoodsInfo']?></small>
									<strong><?=number_format($row['nPrice'])?></strong>원
								</p>
							</label>
<?php
		$i++;
	}
	?>
						</div>
					</div>
				</div>
			</div>
<?php } ?>
			<div class="write_form" style="display:none">
				<div class="subscribe_order layout_center">
					<?php include(VIEW_PATH.'/purchase/write_payinfo.php');  // 결제 정보 입력 폼?>
				</div>
			</div>
</form>

		</div>
		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<? include_once(VIEW_PATH.'/include/gnb.php'); ?>
		</div>
		<!-- [End] App Bottom -->
	</div>

	<!-- [Start] Popup - Terms:서비스 이용약관 -->
	<div class="remodal terms" data-remodal-id="popTermService">
		<div class="term_container">
		<p><?=$terms['txTerms']?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:서비스 이용약관 -->

	<!-- [Start] Popup - Terms:개인정보 수집 및 이용 -->
	<div class="remodal terms" data-remodal-id="popTermPersonalInfo">
		<div class="term_container">
		<p><?=$terms['txPrivacy']?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:개인정보 수집 및 이용 -->

	<!-- [Start] Popup - Terms:마케팅정보 수신 동의 -->
	<div class="remodal terms" data-remodal-id="popTermMarketing">
		<div class="term_container">
		<p><?=$terms['txAdterms']?></p>
		</div>
		<div class="pop_bottom">
			<button class="btn_m full" data-remodal-action="confirm">OK</button>
		</div>
	</div>
	<!-- [End] Popup - Terms:마케팅정보 수신 동의 -->
<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>
<div class="remodal alert" data-remodal-id="ing" id="ing">
	<p>결제가 진행중 입니다.</p>
</div>
<div class="remodal alert" data-remodal-id="refundMsg" id="refundMsg">
	<h2 class="tit_type1" style="color:#666;">30일 이내 취소 시 <strong style="color:red;">전액환불</strong></h2>
	<p style="font-size:12px;">
		결제 <strong style="color:red;">금액 만큼 포인트가 선 지급</strong> 됩니다. <br>
		30일 이내 환불 시 <br>
		포인트 사용 금액 차감 후 환불 됩니다. <br>
		<strong style="color:red;">포인트 미사용 시 결제 금액 100% 환불</strong> <br>
	</p>
	<div class="pop_bottom">
		<button class="btn_m full bg_red" data-remodal-action="confirm" id="confirm_request">확인</button>
	</div>
</div>
<!-- in script -->

<div class="sub_descr case1">
	<em>오전,오후 2종목</em> 종목추천 및 이슈별, 뉴스별 관련종목제공<br>
	주식투자를 한다면 누구나 <em>꼭 이용해야 하는 서비스</em>
</div>
<div class="sub_descr case2">
	<em>100% 문자</em>를 통해 실시간 종목추천 및 매수매도 정보 제공<br>
	<em>안정적 수익을 원하는 주식투자자</em>에게 권장하는 서비스
</div>
<div class="sub_descr case3">
	<em>메신저 및 문자</em>를 통한 실시간 종목추천 및 매수매도 정보제공<br>
	<em>단기고수익을 원하는 주식투자자</em>에게 권장하는 서비스
</div>
<script>
(function(){
	var deviceType = localStorage.getItem("deviceType");
	$(".subs_description >p").html($(".case1").html());
	var currentMenu = 1;
	var refundYn = 'N';
	var a_btn = new Array();
	a_btn[0] = "노다지 주식정보 구독하기";
	a_btn[1] = "프리미엄 문자반 구독하기";
	a_btn[2] = "프리미엄 VIP 구독하기";

	$(".dwld").on('click', function(){
		if(deviceType == "iOS" || deviceType == "Android") {
			cordova.InAppBrowser.open('/subscribe/vipapp', '_system', 'location=no');
		}else{
			window.location.href = "/subscribe/vipapp";
		}
	});

	$(".goods_kind").off('click').on("click", function(e){
		e.preventDefault();
		var tno = $(this).data('kind');
		currentMenu = tno;
		refundYn = 'N';
		if($("input[name=buyGoodsKind"+tno+"]").val() == "N"){
			$(".write_form").css("display", "block");
		}else{
			$(".write_form").css("display", "none");
		}
		$(".goods_kind").removeClass('active');
		$(".goods_kind").eq(tno-1).addClass('active');
		$(".subs_description >p").html($(".case" + tno).html());
		$(".choice_box").removeClass('active');
		$(".choice_box").eq(tno-1).addClass('active');
		$(".choice_box").eq(tno-1).find('input:first').prop('checked', true);
		$(".goodsImg").attr("src", "../asset/img/subs/subs_banner"+tno+".jpg");
		$(".btn-buy").text(a_btn[tno-1])
		$(this).data("kind")

	});

<?php if($paytype == "sms"){?>
	$(".pay_sms").click();
	currentMenu = 2;
<?php } else if($paytype == 'vip') { ?>
	$(".pay_vip").click();
	currentMenu = 3;
<?php } else { ?>
	$(".pay_basic").click();
	var currentMenu = 1;
<?php } ?>


	// 전체 체크 
	$("#allCheck").change(function(){
		if($(this).is(':checked') == true){
			$(".agree").prop('checked', true);
		}
		else{
			$(".agree").prop('checked', false);
		}
	});

	// 체크박스 한개라도 체크시 전체 체크사라지게 
	$("input[name='agree[]']").change(function(){ 
		var agree_1 = Is.checkBox( 'info_agree'),
			agree_2 = Is.checkBox( 'service_agree'),
			agree_3 = Is.checkBox( 'marketing_agree'); 
		if(agree_1==true && agree_2==true && agree_3==true) $('#allCheck').prop('checked', true);
		else $('#allCheck').prop('checked', false);
	});



	$("#confirm_request").on("click", function(){
		refundYn = 'Y';
		$(".btn-buy").click();
	});

	// 구독처리
	$(".btn-buy").on("click", function(){

		if(currentMenu != 1 && refundYn == 'N'){
			Cmmn.alertId('refundMsg');
			return;
		}

		var card_no = $("#vCardNo").val();
		var expdt = $("#vExpdt").val();
		var name = $("#vName").val();
		var phone = $("#vPhone").val();

		if ( !card_no || card_no == "" || card_no == null || card_no == undefined ) {
			Cmmn.alertMsg("카드번호를 작성해주세요.");
			return false;
		}
		<?php if($emPayYN != "Y"){?>
		if ( !(/^[0-9]+$/).test(card_no) ){
			Cmmn.alertMsg("카드번호는 숫자만 가능합니다.");
			return false;
		}
		<?php } ?>

		if ( !expdt || expdt == "" || expdt == null || expdt == undefined ) {
			Cmmn.alertMsg("유효기간을 작성해주세요.");
			return false;
		}
		<?php if($emPayYN != "Y"){?>
		if ( !(/^[0-9]+$/).test(expdt) ){
			Cmmn.alertMsg("유효기간은 숫자만 가능합니다.");
			return false;
		}
		<?php }?>

		if ( !name || name == "" || name == null || name == undefined ) {
			Cmmn.alertMsg("이름을 작성해주세요.");
			return false;
		}

		if ( !phone || phone == "" || phone == null || phone == undefined ) {
			Cmmn.alertMsg("전화번호를 작성해주세요.");
			return false;
		}
		<?php if($emPayYN != "Y"){?>
		if ( !(/^[0-9]+$/).test(phone) ){
			Cmmn.alertMsg("전화번호는 숫자만 가능합니다.");
			return false;
		}
		<?php }?>

		// 동의 체크 
		if( !Is.checkBox( 'service_agree') ) {
			Cmmn.alertMsg('서비스 이용약관 이용에 동의해야합니다.');
			return false;
		}
		if( !Is.checkBox( 'info_agree') ) {
			Cmmn.alertMsg('개인정보 수집 이용에 동의해야합니다.');
			return false;
		}

		var u = "/purchase/postPayInfo";
		var data = $("form[name=pay]").serialize();

		Cmmn.alertId('ing');

		$.post({
			type : "post", url : u, dataType : 'json', data : data,
			success : function(response) {
				console.log(response);
				if( response.result == 'SUCCESS') {
					//Cmmn.alertMsg('결제가 완료 되었습니다.');
					if(window.adbrix){
						window.adbrix.purchase(response.payInfo.tno,"G0"+response.payInfo.nGoodsNo,encodeURI(response.payInfo.vGoodsKindName+" "+response.payInfo.vGoodName),response.payInfo.nPrice,1,"KRW","CATE0"+response.payInfo.nGoodsNo,"credit");
						//alert(response.payInfo.tno+" | G0"+response.payInfo.nGoodsNo+" | "+encodeURI(response.payInfo.vGoodsKindName+" "+response.payInfo.vGoodName)+" | "+response.payInfo.nPrice+" | 1 | KRW | CATE"+response.payInfo.nGoodsNo+" | credit");
					}//201217 adbrix
					location.href = "/subscribe/paycomplete";
				} else {
					Cmmn.alertMsg(response.msg);
				}
			},
			error : function(xhr, status, error) {},
		})
	});


	$("#getPayInfo").on("click", function(){
		if($(this).is(":checked") == true){
			//console.log('check');
			var u = "/purchase/getPayInfo";
			var data = {};
			$.post({
				type : "post", url : u, dataType : 'json', data : data,
				success : function(response) {
					console.log(response);
					if( response.result == 'error' ) {  
						Cmmn.alertMsg(response.msg);
						$("#getPayInfo").prop("checked", false);
					}
					if( response.result == 'SUCCESS') {
						
						var vCardNo = response.vCardNo
						, vName = response.vName
						, vExpdt = response.vExpdt
						, vPhone = response.vPhone;
						$("#vCardNo").val(vCardNo);
						$("#vExpdt").val(vExpdt);
						$("#vName").val(vName);
						$("#vPhone").val(vPhone);

					//	Cmmn.alertMsg('결제가 완료 되었습니다.');
					}
				},
				error : function(xhr, status, error) {},
			})
		}
		else{
			$("#vCardNo").val('');
			$("#vExpdt").val('');
			$("#vName").val('');
			$("#vPhone").val('');
		}
	});
})();
</script>