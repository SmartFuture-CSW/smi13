	<style>
		/*** Reset style ***/
		html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video, button {
			margin: 0;
			padding: 0;
			border: 0;
			font-size: 100%;
			font: inherit;
			vertical-align: baseline;
		}
		/* HTML5 display-role reset for older browsers */
		article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {
			display: block;
		}
		ol, ul, li {
			list-style: none;
		}
		blockquote, q {
			quotes: none;
		}
		blockquote:before, blockquote:after, q:before, q:after {
			content: '';
			content: none;
		}
		table {
			table-layout: fixed;
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
		}
		th, td {
			vertical-align: middle;
		}
		button {
			border: none;
			font-family: inherit;
			color: inherit;
			background-color: transparent;
			cursor: pointer;
		}
		a {
			text-decoration: none;
			color: inherit;
		}
		input {
			margin: 0;
			border: none;
			box-sizing: border-box;
		}
		input[type="text"]::-ms-clear,
		input[type="text"]::-ms-reveal,
		input[type="search"]::-ms-clear,
		input[type="search"]::-ms-reveal {
			display: none;
		}
		input[type="search"]::-webkit-search-cancel-button,
		input[type="search"]::-webkit-search-decoration {
			-webkit-appearance: none;
		}
		select {
			-webkit-appearance: none;
			-moz-appearance: none;
			appearance: auto;
		}
		select::-ms-expand {
			display: none;
		}
		button, input, select {
			vertical-align: top;
		}
		img {
			vertical-align: top;
		}
		iframe {
			border: none;
		}

		/*scroll*/

		body::-webkit-scrollbar,
		div::-webkit-scrollbar {
			width: 10px;
		}
		body::-webkit-scrollbar-thumb,
		div::-webkit-scrollbar-thumb {
			background-color:#555;
		}
		body,
		div	{
			scrollbar-base-color: #555;
			scrollbar-3dlight-color: #888;
			scrollbar-highlight-color: #C0C0C0;
			scrollbar-track-color: #111;
			scrollbar-shadow-color: #333;
			scrollbar-arrow-color: #444;
		}
		/* remodal JS reset */
		/* body {
			padding-right: 0 !important;
		}
		html.remodal-is-locked {
			overflow: auto;
		} */

		/*Common Class*/
		.a11y_hidden {
			position: absolute !important;
			width: 1px !important;
			height: 1px !important;
			margin: -1px !important;
			padding: 0 !important;
			border: 0 !important;
			overflow: hidden !important;
			clip: rect(0,0,0,0) !important;
		}
		.clearfix::after {
			content: '';
			display: block;
			clear: both;
		}
		.pc_hide {
			display: none !important;
		}

		/* Unsubscribe */
		body {
			background-color: #f0f0f0;
		}
		.unsub_container {
			/* font-family: 'Noto Sans KR', sans-serif; */
			font-family: 'NanumSquare', sans-serif;
			width: 480px;
			margin: 50px auto 0;
			padding: 30px 20px 25px;
			box-sizing: border-box;
			background-color: #fff;
			line-height: 1.5;
			color: #333;
			word-break: keep-all;
		}
		@media all and (max-width: 480px) {
			body {
				background-color: #fff;
			}
			.unsub_container {
				width: 100%;
				margin: 0;
			}
		}
		h1 {
			font-size: 30px;
			line-height: 1.3;
		}
		h1 + p {
			margin-top: 1em;
			font-size: 18px;
		}
		.cautions li {
			position: relative;
			margin-bottom: 0.2em;
			padding-left: 1.2em;
			font-size: 15px;
			color: #666;
		}
		.cautions li::before {
			content: '※';
			position: absolute;
			top: 0.2em;
			left: 0;
			font-size: 0.8em;
		}
		.info_container {
			margin-top: 20px;
			padding: 25px 20px;
			background-color: #fff;
			color: #333;
			border: 1px solid #a3a3a3;
		}
	
		.info_container .form_container label {
			display: block;
			margin-top: 5px;
			font-size: 14px;
			font-weight: bold;
			line-height: 2;
		}
		.info_container .form_container label input,.info_container .form_container label select {
			display: block;
			width: 100%;
			height: 40px;
			text-indent: 1em;
			border: 1px solid #a3a3a3;
		}

		.form_container .has_btn_box {
			position: relative;width:100%;
		}
		.form_container .has_btn_box input {
			padding-right: 80px;
		}
		.form_container .has_btn_box button {
			position: absolute;
			top: 0;
			right: 0;
			width: 76px;
			height: 40px;
			font-size: 16px;
			color: #fff;
			background-color: #333;
		}
		.btn_container {
			margin-top: 15px;
		}
		.btn_gray,
		.btn_red {
			display: inline-block;
			width: 130px;
			font-size: 18px;
			line-height: 40px;
			color: #fff;
			letter-spacing: -0.02em;
			background-color: #e70000;
			text-align: center;
		}
		.btn_gray {
			background-color: #999;
		}
	</style>
	<!-- [Start] Unsubscribe -->
	<div class="unsub_container">
		<h1>
			정기 서비스 사용을<br> 
			중단하시겠어요?
		</h1>
		<p>
			정기결제를 해지하시려면<br>
			아래 내용을 확인 및 작성해 주세요
		</p>
		<div class="info_container">
			<ul class="cautions">
				<li>30일 이내 해지 시 결제 금액은 전액 환불 처리 됩니다. </li>
				<li>30일 이후 해지 신청 시 결제 금액은 환불이 불가합니다. <br>단, 정기결제는 해지가 됩니다.
				</li>
			</ul>
			<div class="form_container">
				<form name="sform" id="sform" method="post">
					<label>
						상품종류
						<select id="goodskind" name="goodskind">
							<option value="1">노다지 구독</option><option value="2">프리미엄 문자반</option><option value="3">프리미엄 VIP</option>
						</select>
					</label>
					<label>
						이름
						<input type="text" placeholder="특수문자 사용금지" id="ordername" name="ordername" maxlength="10" required>
					</label>
					<label>
						전화번호
						<div class="has_btn_box">
							<input type="tel" placeholder="'-'없이 숫자만 입력하세요" id="phoneno" name="phoneno" maxlength="11" class="onlyNum" required>
							<button onclick="cert_proc('hp');return false;">인증번호</button>
						</div>
					</label>
					<label>
						인증번호
						<div class="has_btn_box">
							<input type="text" id="cert_no" name="cert_no" placeholder="없이 숫자만 입력하세요" class="onlyNum">
							<button onclick="cert_proc('no');return false;">확인</button>
						</div>
					</label>
				</form>
			</div>
		</div>
		<div class="btn_container">
			<button class="btn_red sendsms">해지신청완료</button>
		</div>
	</div>
	<!-- [End] Unsubscribe -->

	<script>
		$(function(){
			$('.footer_web').remove();
			$(".sendsms").on("click",function(){
				if($('#ordername').val()==''){
					alert('이름을 입력해 주십시오.');
					$('#ordername').focus();
					return false;
				}
				if($('#phoneno').val().length < 9 || $('#phoneno').val().length > 11){
					$('#phoneno').focus();
					alert('전화번호를 정확히 입력해 주십시오.');
					return false;
				}
				if($('#cert_no').val()==''){
					$('#cert_no').focus();
					alert('인증번호를 정확히 입력해 주십시오.');
					return false;
				}
				if(confirm("해지신청을 하시겠습니까?")){
					$('.btn_red').prop('disabled',true);
					var para=$('#sform').serialize();
					$.ajax({
						type: "POST",
						url : "/purchase/postrefund",
						dataType : "json",
						data : para,
						success : function(data){
							if(data.code=='1'){
								alert('해지신청이 완료되었습니다.\n\n그동안 이용해 주셔서 감사합니다.');
								document.location.replace(document.location.href);
							}else{
								alert('오류메세지 : '+data.msg+'\n\n다시 확인후 신청해 주십시오.');
								$('.btn_red').prop('disabled',false);
								return false;
							}
						},
						error:function(request,status,error){
							alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
							$('.btn_red').prop('disabled',false);
							return false;
						}
					});
				}
			});
			$('.onlyNum').on('input', inputOnlyNumber);
			function inputOnlyNumber(){
				$(this).val( $(this).val().replace(/\D/g,''));
			}
		});

		function cert_proc(mode){
			if(mode=='hp'){
				if($('#phoneno').val().length < 9 || $('#phoneno').val().length > 11){
					$('#phoneno').focus();
					alert('전화번호를 정확히 입력해 주십시오.');
					return false;
				}
				$.post("/purchase/setcert.php",{gap:$('#phoneno').val()},function(data){
					alert('해당번호로 인증번호가 전송되었습니다.');
				},'json');
			}else if(mode=='no'){
				if($('#cert_no').val()==''){
					$('#cert_no').focus();
					alert('인증번호를 정확히 입력해 주십시오.');
					return false;
				}
				$.post("/purchase/getcert",{gap:$('#cert_no').val(),gap2:$('#phoneno').val()},function(data){
					if(data.code=='1'){
						alert('인증이 정상적으로 되었습니다.');
						$('#phoneno, #cert_no').prop('readonly',true).css("background-color","silver");
						return;
					}else{
						alert('인증번호가 일치하지 않습니다. 정확히 입력해 주십시오.');
						return false;
					}
				},'json');
			}
		}
	</script>