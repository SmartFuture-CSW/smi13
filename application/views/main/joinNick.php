<div class="app_wrapper">
    <!-- [Start] App Header -->
    <header class="app_header">
        <a href="/" class="home_logo"><img src="<?=IMG?>/logo.png" alt="증권찌라시 노다지"></a>
    </header>
    
<!-- [Start] App Main -->
    <div class="app_main sign05">
      <div class="layout_center">
        <h1>닉네임 설정</h1>
        <div class="input_box">
          <input type="text" placeholder="10글자 내외 (한글 영문 상관없이 10자 이내)" name="nick" id="nick" maxlength="10">
        </div>
        <button type="button" class="btn_l full bg_red" id="nextButton">설정완료</button>
      </div>
    </div>
    <!-- [End] App Main -->

    <!-- [Start] App Bottom -->
    <!-- [End] App Bottom -->
  </div>


<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div><!-- 알럿메세 -->


<script type="text/javascript">

$(document).ready(function()
{
    // 회원가입 버튼
    $("#nextButton").click(function(){  
        $.ajax({
            type : "post",
            url : "/main/modifyNick",
            dataType : 'json',
            data : { 'nick' : $('#nick').val() },

            success : function(response) {

                Cmmn.log(response);

                if( response.result == 'error' ) {  
                    Cmmn.alertMsg(response.msg);
                }

                if( response.result == 'success') {
                    location.href = response.url;                        
                }

            },

            error : function(xhr, status, error) {},
        })

    });

});


</script>