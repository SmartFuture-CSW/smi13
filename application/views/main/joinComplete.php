 <!-- [Start] App Main -->
 <div class="app_main sign04">
			<div class="layout_center">
				<div class="inner_container">
					<i class="xi-check"></i>
					<h1>가입완료</h1>
					<ul class="txt_list is_check">
						<li>무료 이용이 종료되기 전 까지는 요금 청구가 없으니 안심하세요.</li>
						<li>월 9,900원에 하루 세 번 주식투자에 꼭 필요한 정보를 제공 받을 수 있습니다.</li>
						<li>무약정, 무위약금. 해지도 마음대로 언제든지 진행하세요.</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- [End] App Main -->

		<!-- [Start] App Bottom -->
		<div class="app_bottom">
			<button type="button" class="btn_l full bg_red" id="nextButton">확인</button>
		</div>
		<!-- [End] App Bottom -->
	</div>


<script type="text/javascript">
	$(document).ready(function()
	{
			// 회원가입 버튼
			$("#nextButton").click(function(){  location.href="/joinNick";  });

	});

	/* Make Noscroll One Page  */
	(function(){
		var viewportHeight = $(window).height();
		// var headerHeight = $('.app_header').height();
		var bottomHeight = $('.app_bottom').height();
		$('.app_main').height(viewportHeight-bottomHeight);
	})();
</script>