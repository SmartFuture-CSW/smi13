
<?php
if(empty($referer)){
	$referer = (!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '/info'; ;
}
?>

<div class="app_wrapper">
	<!-- [Start] App Header -->
	<header class="app_header">

		<a href="<?=($first=="Y")? "/auth?first=Y" : "/"; ?>" class="home_logo"><img src="<?=IMG?>/logo.png" alt="증권찌라시 노다지"></a>
	</header>
	<!-- [End] App Header -->
	<!-- [Start] App Main -->
	<form name="frm" id="frm" method="post">
		<input type="hidden" name="device" id="device" value="">
		<div class="app_main sign02">
			<div class="layout_center" style="padding-top: 20px;">
				<h1>
					<em>100%무료</em>
					인증 회원 등록 후 이용 가능합니다
					<small>( 기존 인증 회원일 경우 재인증 시 로그인 됩니다. )</small>
				</h1>
				<?/*
				<div class="input_box">
					<input type="tel" placeholder="전화번호" name="phone" id="phone" inputmode="numeric" pattern="[0-9]*" maxlength="11">
				</div>
				<div class="input_box">
					<input type="password" placeholder="비밀번호" name="passwd" id="passwd" maxlength="15">
				</div>
				*/?>

				<div class="input_box">
					<input type="text" name="username" id="username" placeholder="이름" maxlength="8">
				</div>

				<div class="input_box has_btn">
					<input type="tel" name="phone" id="phone" placeholder="휴대폰 번호" maxlength="11">
					<button type="button" id="phoneCheck">인증하기</button>
				</div>

				<div class="input_box has_btn">
					<input type="tel" name="code" id="code" placeholder="인증번호" maxlength="4">
					<button type="button" id="codeCheck">확 인</button>
				</div>

				<div class="bottom_check">
					<label class="checkbox type_agree">
						<input type="checkbox" name="allCheck" id="allCheck">
						<i></i>아래 사항에 모두 동의 합니다.
					</label><br>
					<label class="checkbox type_agree">
						<input type="checkbox" name="agree[]" id="service_agree">
						<i></i>서비스 이용약관 동의
					</label>
					<button type="button" data-remodal-target="popTermService" id="popTermService">[보기]</button><br>
					<label class="checkbox type_agree">
						<input type="checkbox" name="agree[]" id="info_agree">
						<i></i>개인정보 수집 및 이용동의
					</label>
					<button type="button" data-remodal-target="popTermPersonalInfo">[보기]</button>
				</div>

<?php if($first == "Y"){ ?>
				<div class="btn_container clearfix">
					<button type="button" class="btn_l full bg_red btn_join">로그인</button>
				</div>
<?php } else { ?>
				<div class="btn_container clearfix">
					<button type="button" class="btn_l bg_black btn_tobecontinue">다음에 등록하기</button>
					<button type="button" class="btn_l bg_red btn_join">로그인</button>
				</div>
<?php }?>

				
			</div>

			<section class="benefit_container">
				<h2>
					인증 후 <em>100% 무료</em> 혜택 <img src="../asset/img/icon_logo.png" alt="">
				</h2>
				<ul class="accordion">
					<li>
						<button type="button" class="acdnButton" aria-label="메뉴 열기">
							<em>하루 1개</em> 급등 예상 종목 무료추천
						</button>
						<div class="acdnPanel">
							<img src="../asset/img/sample_screenshot_1.jpg" alt="">
							<p>
								<em>하루 1개 급등 예상 종목</em>을 엄선하여<br>
								<em>무료로 제공</em>을 해드리고 있습니다.<br>
								종목의 매수가, 손절가 및 종목을 선택하여 드리는 이유부터 기업의 근황 및 뉴스까지 한 번에 정리하여 제공합니다.
							</p>
						</div>
					</li>
					<li>
						<button type="button" class="acdnButton" aria-label="메뉴 열기">
							<em>이슈별, 뉴스별, 테마별</em> 종목정보 제공
						</button>
						<div class="acdnPanel">
							<img src="../asset/img/sample_screenshot_2.jpg" alt="">
							<p>
								지금 주목받고 있는 <em>이슈, 뉴스, 테마와 관련한 종목</em>의 기본적인 정보를 제공해 드립니다.<br>
								<em>기본적인 정보를 제공</em>함으로써 종목 선택을<br> 
								막힘없이 할 수 있도록 지원해 드립니다. 
							</p>
						</div>
					</li>
					<li>
						<button type="button" class="acdnButton" aria-label="메뉴 열기">
							궁금한 종목 관련 <em>전문가 분석</em> 제공
						</button>
						<div class="acdnPanel">
							<img src="../asset/img/sample_screenshot_3.jpg" alt="">
							<p>
								궁금한 종목 정보가 있으신가요?<br>
								궁금한 부분에 대하여 <em>커뮤니티 게시판</em>에 올려 주세요.<br>
								<em>노다지 최고의 전문가분들이 성실히 답변</em>해 드립니다.<br>
								다른 유저의 종목 관련 질문도 함께 확인할 수 있습니다.
							</p>
						</div>
					</li>
					<li>
						<button type="button" class="acdnButton" aria-label="메뉴 열기">
							기초부터 실전까지 <em>주식교육 콘텐츠</em> 제공
						</button>
						<div class="acdnPanel">
							<img src="../asset/img/sample_screenshot_4.jpg" alt="">
							<p>
								당신이 주식 왕초보라도 상관없습니다.<br>
								주식을 하고 싶은 열정 하나만으로도 충분히 시작할 수 있도록
								<em>이해하기 쉬운 주식교육 콘텐츠들을 제공</em>해 드립니다.<br>
								기초부터 실전까지 따라만 와주세요.
							</p>
						</div>
					</li>
					<li>
						<button type="button" class="acdnButton" aria-label="메뉴 열기">
							개인투자자라면 꼭 필요한 <em>필수 정보</em> 제공
						</button>
						<div class="acdnPanel">
							<img src="../asset/img/sample_screenshot_5.jpg" alt="">
							<p>
								주식 투자자가 반드시 알아야하는 정보를<br> 손쉽게 찾아 보실 수있도록 정리하여 제공해 드립니다.<br> 
								<em>거기에 매일매일 새롭게 업데이트 까지 진행하여 제공</em>해 드리는 정보의 신뢰도 까지 신경쓰고 있습니다. 
							</p>
						</div>
					</li>
				</ul>
			</section>
		</div>
	</form>
	<!-- [End] App Main -->

	<!-- <button class="btnTop" type="button" aria-label="최상단으로 이동"></button> -->

	<!-- [Start] App Bottom -->
	<?php if($first != "Y"){ ?>
	<div class="app_bottom">
			<? include_once(VIEW_PATH.'/include/gnb.php'); ?>
	</div>
	<?php } ?>
	<!-- [End] App Bottom -->
</div>

<!-- [Start] Popup - simple alert -->
<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>
<!-- [End] Popup - simple alert -->


<!-- [Start] Popup - Terms:서비스 이용약관 -->
<div class="remodal terms" data-remodal-id="popTermService">
	<div class="term_container">
		<p><?=$terms['txTerms']?><?//php include_once (TEXT."/service_agree.txt"); ?></p>
	</div>
	<div class="pop_bottom">
		<button class="btn_m full" data-remodal-action="confirm">OK</button>
	</div>
</div>
<!-- [End] Popup - Terms:서비스 이용약관 -->


<!-- [Start] Popup - Terms:개인정보 수집 및 이용 -->
<div class="remodal terms" data-remodal-id="popTermPersonalInfo">
	<div class="term_container">
		<p><?=$terms['txPrivacy']?><?//php include_once (TEXT."/info_agree.txt"); ?></p>
	</div>
	<div class="pop_bottom">
		<button class="btn_m full" data-remodal-action="confirm">OK</button>
	</div>
</div>
<!-- [End] Popup - Terms:개인정보 수집 및 이용 -->

<!-- [Start] Popup - Nickname Popup-->
<div class="remodal nickname" data-remodal-id="nickname">
	<h2>닉네임 설정</h2>
	<div class="input_box">
		<input type="text" name="nick" id="nick" placeholder="10글자 내외 (한글 영문 상관없이 10자 이내)">
	</div>
	<button type="button" class="btn_l full bg_red btn_nick">로그인</button>
</div>
<!-- [End] Popup - Nickname Popup-->


<!-- [Start] Popup - Advertisement -->
<?if(!empty($popup)){ ?>
	<div class="remodal advertisement" data-remodal-id="pop_ad">
		<button type="button" data-remodal-action="cancel" class="btn_pop_close" aria-label="팝업 닫기"></button>
		<a href="<?=$popup['vLink']?>"><img src="/data/banner/<?=$popup['vImage']?>" alt="광고 타이틀"></a>
	</div>
<?}?>
<!-- [End] Popup - Advertisement -->

<?php
$app = $this->input->get('app', TRUE);
if($app) {
	if($app == 1) {
		echo '<script src="/cordova/android/cordova.js"></script>';
	}
	else if($app == 2) {
		echo '<script src="/cordova/ios/cordova.js"></script>';
	}
	echo '<script>localStorage.setItem("app", "'.$app.'");</script>';
	echo '<script src="/cordova/app.js"></script>';
}
?>

<script type="text/javascript" src="//js.frubil.info/"></script>
<script type="text/javascript">
$(document).ready(function(){


<?php if($first == "Y"){ ?>
	$(".footer_web").remove();
<?php }?>

	var brand = FRUBIL.device.brand;         // Samsung
	var market = FRUBIL.device.marketname;    // Galaxy A5 (2016)
	var authFlag = false;
	var referer = '<?=$referer?>';

	// 아이디 저장처리 
	var save_id = Cookie.get('save_id');
	var save_state = Cookie.get('save_state');
	if( save_id ) {  
		$('#phone').val( save_id ); $('#save_id').prop('checked', true); 
	}
	else{  
		$('#save_id').prop('checked', false);  
	}

	if( save_state ) {  
		$('#save_state').prop('checked', true); 
	}
	else{  
		$('#save_state').prop('checked', false);  
	}


	

	$(".checkbox").on("click", function(){
		if ( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
			console.log('t')
		}
	});


	$(".btn_tobecontinue").on("click", function(){
		window.location.href = referer;
	});

	// 휴대폰인증하기 버튼
	$("#phoneCheck").click(function(){      
		if( Is.phone( $('#phone').val() ) ) {
			$.ajax({
				type : "post",
				url : "/main/phoneCheck",
				dataType : 'json',
				data : { 'phone': $('#phone').val(), 'mode' : 'login' },
				success : function(response) {
					Cmmn.log(response);
					if( response.result == 'error' ) {  
						Cmmn.alertMsg(response.msg);
					}
					if( response.result == 'success') {
						Cmmn.alertMsg(response.msg);
					}
				},
				error : function(xhr, status, error) {},
			})  
		}
	});

	// 인증번호체크 버튼
	$("#codeCheck").click(function(){
		$.ajax({
			type : "post",
			url : "/main/codeCheck",
			dataType : 'json',
			data : { 'code' : $('#code').val() },
			success : function(response) {
				Cmmn.log(response);
				if( response.result == 'error' ) {  
					Cmmn.alertMsg(response.msg); authFlag = false;
				}
				if( response.result == 'success') {
					$('#code').attr('readonly', true);  // 성공일때 readonly
					Cmmn.alertMsg(response.msg); authFlag = true;
				}
			},
			error : function(xhr, status, error) {},
		});
	});

	// 회원가입 버튼
	$("#joinButton").click(function(){  location.href="/join";  });

	// 로그인버튼 
	$("#loginButton").click(function()
	{
		if( Is.phone( $('#phone').val() ) ) {
			$.ajax({
				type : "post",
				url : "/main/loginCheck",
				dataType : 'json',
				data : $('#frm').serialize(),
				success : function(response) {
					if( response.result == 'success') {
						if($('#save_id').is(":checked")) {
							Cookie.set('save_id', $('#phone').val(), 30, 'login');
						}else{
							Cookie.del('save_id');
						}
						if($('#save_state').is(":checked")) {
							Cookie.set('save_state', true, 30, 'login');
						}else{
							Cookie.del('save_state');
						}
						location.href = response.url;
					}
					if( response.result == 'error' ) { 
					 
						Cmmn.alertMsg(response.msg);
					}
				},
				error : function(xhr, status, error) {},
			})
		}
	});

	// 전체 체크 
	$("#allCheck").change(function(){ Cmmn.checkAll('allCheck'); });

	// 체크박스 한개라도 체크시 전체 체크사라지게 
	$("input[name='agree[]']").change(function(){ 
		var agree_1 = Is.checkBox( 'info_agree'),
			agree_2 = Is.checkBox( 'service_agree'); 
		if(agree_1==true && agree_2==true) $('#allCheck').prop('checked', true);
		else $('#allCheck').prop('checked', false);
	});
	
	// 회원가입버튼 
	$(".btn_join").click(function(){
		// 입력 폼 체크 
		var phone = $('#phone').val();
		var name = $('#username').val()
		var code = $('#code').val()

		if ( !name || name == "" || name == null || name == undefined ) {
			Cmmn.alertMsg("이름을 작성해주세요.");
			return false;
		}

		if ( !phone || phone == "" || phone == null || phone == undefined ) {
			Cmmn.alertMsg("휴대폰 번호를 작성 해주세요.");
			return false;
		}
		if ( !(/^[0-9]+$/).test(phone) ){
			Cmmn.alertMsg("숫자만 가능합니다.");
			return false;
		}
		if ( !(/^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/).test(phone) ){
			Cmmn.alertMsg("휴대폰 번호를 확인해 주세요");
			return false;
		}
		if ( !code || code == "" || code == null || code == undefined ) {
			Cmmn.alertMsg("인증번호를 작성해주세요.");
			return false;
		}

		// 인증번호가 맞는지 체크 
		if(!authFlag) {
			Cmmn.alertMsg('인증번호가 일치하지 않습니다. 다시확인해주세요.');
			return false;
		}
		// 동의 체크 
		if( !Is.checkBox( 'service_agree') ) {
			Cmmn.alertMsg('서비스 이용약관 이용에 동의해야합니다.');
			return false;
		}
		if( !Is.checkBox( 'info_agree') ) {
			Cmmn.alertMsg('개인정보 수집 이용에 동의해야합니다.');
			return false;
		}
			
		$('#device').val(brand+'-'+market);


		$.ajax({
			type : "post",
			url : "/main/loginjoin",
			dataType : 'json',
			data : $('#frm').serialize(),
			success : function(response) {
//				console.log(response)
				if( response.result == 'error' ) {
					Cmmn.alertMsg(response.msg);
				}
				if( response.result == 'success') {
					var nick = response.data.vNick;
					if(response.msg=='join'){
						if(window.adbrix) window.adbrix.userRegister(encodeURI($('#username').val()), encodeURI($('#phone').val()));//201217 adbrix
						gtag('event', 'login', {'event_category' : 'success', 'event_action' : 'input', 'event_label' : 'db'});//201214 ymkt
					}
					if(!nick){
						$('[data-remodal-id=nickname]').remodal().open();
					}
					else{
						window.location.href = referer;
					}
				}
			},
			error : function(xhr, status, error) {},
		})
	});


	// 회원가입 버튼
	$(".btn_nick").click(function(){  
		var u = "/main/modifyNick";
		var nick = $('#nick').val();
		var data = {
			'nick' : nick
		};
		$.ajax({
			type : "post",
			url : u,
			dataType : 'json',
			data : data,
			success : function(response) {
				if( response.result == 'error' ) {  
					Cmmn.alertMsgRemodal(response.msg, 'nickname');
				}
				if( response.result == 'success') {
						window.location.href = referer;
				}
			},
			error : function(xhr, status, error) {},
		})
	});

	

<?if(!empty($popup)){ ?>
	$('[data-remodal-id=pop_ad]').remodal().open();
<?}?>

});
</script>