<div class="app_wrapper">
	<!-- [Start] App Header -->
	<header class="app_header">
	  <a href="/" class="home_logo"><img src="<?=IMG?>/logo.png" alt="증권찌라시 노다지"></a>
	</header>
	<!-- [End] App Header -->
	<div class="app_main sign03 "> <!-- [Start] App Main -->
		<div class="layout_center">
			<div class="inner_container">
				<span class="red_bar"></span>
				<?/*
				<h1>지금 '증권가찌라시 노다지'에<br>가입하고 30일 동안<br>무료로 이용하세요</h1>
				<p>증권가찌라시 노다지 가입<br>정말 복잡하지 않게 해보세요</p>
				*/?>
				<form id="frm" name="frm" method="post">
					<input type="hidden" name="device" id="device" value="">
					<div class="form_container">
						<h2 class="form_heading">정보를 등록하세요</h2>

						<div class="input_box">
							<input type="text" name="username" id="username" placeholder="이름" maxlength="8">
						</div>

						<div class="input_box has_btn">
							<input type="tel" name="phone" id="phone" placeholder="휴대폰 번호" maxlength="11">
							<button type="button" id="phoneCheck">인증하기</button>
						</div>

						<div class="input_box has_btn">
							<input type="text" name="code" id="code" placeholder="인증번호" maxlength="4">
							<button type="button" id="codeCheck">확 인</button>
						</div>

	<?/*
						<div class="input_box has_btn">
							<input type="password" name="passwd" id="passwd" placeholder="비밀번호 (알파벳 숫자조합 6자리 이상)" maxlength="15">
						</div>
	*/?>
						<div class="bottom_check">
							<label class="checkbox type_agree"><input type="checkbox" name="allCheck" id="allCheck"><i></i>아래 사항에 모두 동의 합니다.</label>
							<br>
							<label class="checkbox type_agree"><input type="checkbox" name="agree[]" id="service_agree"><i></i>서비스 이용약관 동의</label>
							<button type="button" data-remodal-target="popTermService" id="popTermService">[보기]</button><br>
							<label class="checkbox type_agree"><input type="checkbox" name="agree[]" id="info_agree"><i></i>개인정보 수집 및 이용동의</label>
							<button type="button" data-remodal-target="popTermPersonalInfo" id="popTermPersonalInfo">[보기]</button><br>
						</div>

					</div>
				</form>

			</div>
			<div class="btn_container" style="margin-top:20px;">
				<button type="button" class="btn_l full bg_black" id="joinButton">다 음</button>
			</div>
			<div class="btn_container" style="margin-top:10px;">
				<button type="button" class="btn_l full bg_red" id="loginButton">로그인</button>
			</div>
		</div>
	</div><!-- [End] App Main -->
</div>


<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div><!-- 알럿메세 -->

<!-- [Start] Popup - Terms:서비스 이용약관 -->
    <div class="remodal terms" data-remodal-id="popTermService">
        <div class="term_container">
            <p><?php include_once (TEXT."/service_agree.txt"); ?></p>
        </div>
        <div class="pop_bottom">
            <button class="btn_m full" data-remodal-action="confirm">OK</button>
        </div>
    </div>
    <!-- [End] Popup - Terms:개인정보 수집 및 이용 -->


<script type="text/javascript" src="//js.frubil.info/"></script>

<?php
$app = $this->input->get('app', TRUE);
if($app)
{
	if($app == 1)
	{
		echo '<script src="/cordova/android/cordova.js"></script>';
	}
	else if($app == 2)
	{
		echo '<script src="/cordova/ios/cordova.js"></script>';
	}
	echo '<script>localStorage.setItem("app", "'.$app.'");</script>';
	echo '<script src="/cordova/app.js"></script>';
}
?>

<script type="text/javascript">

// var dc = FRUBIL.device.class;
// var cc = FRUBIL.client.class;         // Browser
// var cn = FRUBIL.client.name;          // Chrome
var brand = FRUBIL.device.brand;         // Samsung
var market = FRUBIL.device.marketname;    // Galaxy A5 (2016)
var authFlag = false;

$(document).ready(function()
{
	// 전체 체크 
	$("#allCheck").change(function(){ Cmmn.checkAll('allCheck'); });

	// 체크박스 한개라도 체크시 전체 체크사라지게 
	$("input[name='agree[]']").change(function(){ 
		var agree_1 = Is.checkBox( 'info_agree'),
			agree_2 = Is.checkBox( 'service_agree'); 
		if(agree_1==true && agree_2==true) $('#allCheck').prop('checked', true);
		else $('#allCheck').prop('checked', false);
	});

	// 휴대폰인증하기 버튼
	$("#phoneCheck").click(function(){      
		if( Is.phone( $('#phone').val() ) ) {
			$.ajax({
				type : "post",
				url : "/main/phoneCheck",
				dataType : 'json',
				data : { 'phone': $('#phone').val(), 'mode' : 'join' },
				success : function(response) {
				//	Cmmn.log(response);
					if( response.result == 'error' ) {  
						Cmmn.alertMsg(response.msg);
					}
					if( response.result == 'success') {
						Cmmn.alertMsg(response.msg);
					}
				},
				error : function(xhr, status, error) {},
			})
		}
	});

	// 인증번호체크 버튼
	$("#codeCheck").click(function(){
		$.ajax({
			type : "post",
			url : "/main/codeCheck",
			dataType : 'json',
			data : { 'code' : $('#code').val() },
			success : function(response) {
//				Cmmn.log(response);
				if( response.result == 'error' ) {  
					Cmmn.alertMsg(response.msg); authFlag = false;
				}
				if( response.result == 'success') {
					$('#code').attr('readonly', true);  // 성공일때 readonly
					Cmmn.alertMsg(response.msg); authFlag = true;
				}
			},
			error : function(xhr, status, error) {},
		})
	});

	$("#loginButton").off().on("click", function(){
		location.href = "/main/login";
	});

	// 회원가입버튼 
	$("#joinButton").click(function(){
		// 입력 폼 체크 
		if( Is.name( $('#username').val() )
			&& Is.phone( $('#phone').val() ) 
			&& Is.code( $('#code').val() )
		//	&& Is.pwd( $('#passwd').val() )
		) {
			// 인증번호가 맞는지 체크 
			if(!authFlag) { Cmmn.alertMsg('인증번호가 일치하지 않습니다. 다시확인해주세요.'); return false; }
			// 동의 체크 
			if( !Is.checkBox( 'service_agree') ) { Cmmn.alertMsg('서비스 이용약관 이용에 동의해야합니다.'); return false; }
			if( !Is.checkBox( 'info_agree') ) { Cmmn.alertMsg('개인정보 수집 이용에 동의해야합니다.'); return false; }
				
			$('#device').val(brand+'-'+market);

			$.ajax({
				type : "post",
				url : "/main/doJoin",
				dataType : 'json',
				data : $('#frm').serialize(),
				success : function(response) {
					if( response.result == 'error' ) {
						Cmmn.alertMsg(response.msg);
					}
					if( response.result == 'success') {
						location.href = response.url;
					}
				},
				error : function(xhr, status, error) {},
			})
		}
	});
});

</script>