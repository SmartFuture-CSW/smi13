<!-- [Start] App Main -->
    <div class="app_main">
      <div class="search_container">
        <div class="search_box">
          <input type="search" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
          <button type="submit">검색</button>
        </div>
      </div>

      <div class="lnb_main layout_center clearfix">
        <a href="#none" class="active">TODAY</a>
        <a href="#none">핵심정보</a>
        <a href="#none">뉴스</a>
        <a href="#none">커뮤니티</a>
      </div>

      <section class="main_topic">
        <h1 class="a11y_hidden">TODAY</h1>
        <div class="tabContainer main_tab">
          <div class="tabList" role="tablist" aria-label="Today 최신·인기 정보">
            <a href="#none" id="tab-button-1" class="tabButton active" role="tab" aria-controls="tab-panel-1">최신정보</a>
            <a href="#none" id="tab-button-2" class="tabButton" role="tab" aria-controls="tab-panel-2">최신 추천종목</a>
          </div>

          <div id="tab-panel-1" class="tabPanel active" role="tabpanel" aria-labelledby="tab-button-1">
            <div class="today_list">
              <a href="#none" class="new">외국인 기관투자자들이 매일같이 환장하고 사 모으는외국인 기관투자자들이 매일같이 환장하고 사 모으는</a>
              <a href="#none">외국인 기관투자자들이 매일같이 환장하고 사 모으는외국인 기관투자자들이 매일같이 환장하고 사 모으는</a>
              <a href="#none">외국인 기관투자자들이 매일같이 환장하고 사 모으는외국인 기관투자자들이 매일같이 환장하고 사 모으는</a>
              <a href="#none">외국인 기관투자자들이 매일같이 환장하고 사 모으는외국인 기관투자자들이 매일같이 환장하고 사 모으는</a>
              <a href="#none">외국인 기관투자자들이 매일같이 </a>
            </div>
          </div>

          <div id="tab-panel-2" class="tabPanel" role="tabpanel" aria-labelledby="tab-button-2" hidden>
            <div class="today_list">
              <a href="#none" class="new">외국인 기관투자자들이 매일같이 환장하고 사 모으는외국인 기관투자자들이 매일같이 환장하고 사 모으는</a>
              <a href="#none" class="new">외국인 기관투자자들이 매일같이 환장하고 사 모으는외국인 기관투자자들이 매일같이 환장하고 사 모으는</a>
              <a href="#none" class="new">외국인 기관투자자들이 매일같이 환장하고 사 모으는외국인 기관투자자들이 매일같이 환장하고 사 모으는</a>
              <a href="#none" class="new">외국인 기관투자자들이 매일같이 환장하고 사 모으는외국인 기관투자자들이 매일같이 환장하고 사 모으는</a>
              <a href="#none" class="new">외국인 기관투자자들이</a>
            </div>
          </div>
        </div>
      </section>

      <div class="banner_type1">
        <a href="#none">
          <img src="../asset/img/sample/sample_ad01.png" alt="">
        </a>
        <a href="#none">
          <img src="../asset/img/sample/sample_ad01.png" alt="">
        </a>
        <a href="#none">
          <img src="../asset/img/sample/sample_ad01.png" alt="">
        </a>
        <a href="#none">
          <img src="../asset/img/sample/sample_ad01.png" alt="">
        </a>
      </div>

      <section class="investment_information layout_center">
        <h2>투자정보</h2>
        <div class="list_article clearfix">
          <a href="#none">
            <div class="thumbnail" style="background-image:url('../asset/img/sample/sample_article_thumb.jpg')"></div>
            <p>[종목분석1] 09.25 정부가 밀어주는 반도체 산업의 결과는어떻게 될 것인가</p>
          </a>
          <a href="#none">
            <div class="thumbnail" style="background-image:url('../asset/img/sample/sample_article_thumb.jpg')"></div>
            <p>[종목분석2] 09.25 정부가 밀어주는 반도체 산업의 결과는어떻게 될 것인가</p>
          </a>
          <a href="#none">
            <div class="thumbnail" style="background-image:url('../asset/img/sample/sample_article_thumb.jpg')"></div>
            <p>[종목분석3] 09.25 정부가 밀어주는 반도체 산업의 결과는어떻게 될 것인가</p>
          </a>
          <a href="#none">
            <div class="thumbnail" style="background-image:url('../asset/img/sample/sample_article_thumb.jpg')"></div>
            <p>[종목분석4] 09.25 정부가 밀어주는 반도체 산업의 결과는어떻게 될 것인가</p>
          </a>
        </div>
      </section>

      <section class="section_community">
        <h2>커뮤니티</h2>
        <ul class="list_post">
          <li>
            <a href="#none">이글벳으로 떡상하고 1억 벌었으~!!</a>
            <p>
              <span>싸랑싸랑</span>
              <span>20.01.28</span>
              <span>댓글 0</span>
              <span>조회 1000</span>
            </p>
          </li>
          <li>
            <a href="#none">이글벳으로 떡상하고 1억 벌었으~!!이글벳으로 떡상하고 1억 벌었으~!!</a>
            <p>
              <span>싸랑싸랑</span>
              <span>20.01.28</span>
              <span>댓글 0</span>
              <span>조회 1000</span>
            </p>
          </li>
          <li>
            <a href="#none">이글벳!!</a>
            <p>
              <span>싸랑싸랑</span>
              <span>20.01.28</span>
              <span>댓글 0</span>
              <span>조회 1000</span>
            </p>
          </li>
          <li>
            <a href="#none">이글벳으로 떡상하고 1억 벌었으~!!</a>
            <p>
              <span>싸랑싸랑</span>
              <span>20.01.28</span>
              <span>댓글 0</span>
              <span>조회 1000</span>
            </p>
          </li>
          <li>
            <a href="#none">이글벳으로 떡상하고 1억 벌었으~!!이글벳으로 떡상하고 1억 벌었으~!!</a>
            <p>
              <span>싸랑싸랑</span>
              <span>20.01.28</span>
              <span>댓글 0</span>
              <span>조회 1000</span>
            </p>
          </li>
          <li>
            <a href="#none">이글벳!!</a>
            <p>
              <span>싸랑싸랑</span>
              <span>20.01.28</span>
              <span>댓글 0</span>
              <span>조회 1000</span>
            </p>
          </li>
        </ul>
      </section>
      <a class="advertisement_bottom" href="#none">
        <img src="../asset/img/sample/sample_app_advertisement.jpg" alt="">
      </a>
    </div>
    <!-- [End] App Main -->

    <!-- [Start] App Bottom -->
    <div class="app_bottom">
      <div class="gnb">
        <div class="layout_center clearfix">
          <a href="#none" class="menu_1 active">공개정보</a>
          <a href="#none" class="menu_2">추천종목</a>
          <a href="#none" class="menu_3">실시간리딩</a>
          <a href="#none" class="menu_4">돈받는기법</a>
          <a href="#none" class="menu_5">커뮤니티</a>
        </div>
      </div>
    </div>
    <!-- [End] App Bottom -->
  </div>