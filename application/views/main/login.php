<div class="app_wrapper">
	<!-- [Start] App Header -->
	<header class="app_header">
		<a href="/" class="home_logo"><img src="<?=IMG?>/logo.png" alt="증권찌라시 노다지"></a>
	</header>
	<!-- [End] App Header -->
	<!-- [Start] App Main -->
	<form name="frm" id="frm" method="post">
		<div class="app_main sign02">
			<div class="layout_center">
				<h1>로그인</h1>
				<?/*
				<div class="input_box">
					<input type="tel" placeholder="전화번호" name="phone" id="phone" inputmode="numeric" pattern="[0-9]*" maxlength="11">
				</div>
				<div class="input_box">
					<input type="password" placeholder="비밀번호" name="passwd" id="passwd" maxlength="15">
				</div>
				*/?>


				<div class="input_box">
					<input type="text" name="username" id="username" placeholder="이름" maxlength="8">
				</div>

				<div class="input_box has_btn">
					<input type="tel" name="phone" id="phone" placeholder="휴대폰 번호" maxlength="11">
					<button type="button" id="phoneCheck">인증하기</button>
				</div>

				<div class="input_box has_btn">
					<input type="tel" name="code" id="code" placeholder="인증번호" maxlength="4">
					<button type="button" id="codeCheck">확 인</button>
				</div>


				<button type="button" class="btn_l full bg_red" id="loginButton">로그인</button>
				<button type="button" class="btn_l full bg_black" id="joinButton">회원가입</button>
				<div class="bottom_check">
					<label class="checkbox type_agree"><input type="checkbox" name="save_id" id="save_id">
					<i></i>아이디 저장</label><br>
					
					<label class="checkbox type_agree"><input type="checkbox" name="save_state" id="save_state" >
					<i></i>로그인 상태 유지</label>
					<div class="right_side">
						<a href="/loginOrigin">기존회원 로그인</a>
						<!--a href="/fdPasswd">비밀번호 찾기</a-->
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- [End] App Main -->
</div>


<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div>

<?php
$app = $this->input->get('app', TRUE);
if($app)
{
	if($app == 1)
	{
		echo '<script src="/cordova/android/cordova.js"></script>';
	}
	else if($app == 2)
	{
		echo '<script src="/cordova/ios/cordova.js"></script>';
	}
	echo '<script>localStorage.setItem("app", "'.$app.'");</script>';
	echo '<script src="/cordova/app.js"></script>';
}
?>

<script type="text/javascript">
$(document).ready(function(){
	// 아이디 저장처리 
	var save_id = Cookie.get('save_id');
	var save_state = Cookie.get('save_state');
	if( save_id ) {  
		$('#phone').val( save_id ); $('#save_id').prop('checked', true); 
	}
	else{  
		$('#save_id').prop('checked', false);  
	}

	if( save_state ) {  
		$('#save_state').prop('checked', true); 
	}
	else{  
		$('#save_state').prop('checked', false);  
	}

	// 휴대폰인증하기 버튼
	$("#phoneCheck").click(function(){      
		if( Is.phone( $('#phone').val() ) ) {
			$.ajax({
				type : "post",
				url : "/main/phoneCheck",
				dataType : 'json',
				data : { 'phone': $('#phone').val(), 'mode' : 'login' },
				success : function(response) {
					Cmmn.log(response);
					if( response.result == 'error' ) {  
						Cmmn.alertMsg(response.msg);
					}
					if( response.result == 'success') {
						Cmmn.alertMsg(response.msg);
					}
				},
				error : function(xhr, status, error) {},
			})  
		}
	});

	// 인증번호체크 버튼
	$("#codeCheck").click(function(){
		$.ajax({
			type : "post",
			url : "/main/codeCheck",
			dataType : 'json',
			data : { 'code' : $('#code').val() },
			success : function(response) {
				Cmmn.log(response);
				if( response.result == 'error' ) {  
					Cmmn.alertMsg(response.msg); authFlag = false;
				}
				if( response.result == 'success') {
					$('#code').attr('readonly', true);  // 성공일때 readonly
					Cmmn.alertMsg(response.msg); authFlag = true;
				}
			},
			error : function(xhr, status, error) {},
		});
	});

	// 회원가입 버튼
	$("#joinButton").click(function(){  location.href="/join";  });

	// 로그인버튼 
	$("#loginButton").click(function()
	{
		if( Is.phone( $('#phone').val() ) ) {
			$.ajax({
				type : "post",
				url : "/main/loginCheck",
				dataType : 'json',
				data : $('#frm').serialize(),
				success : function(response) {
					if( response.result == 'success') {
						if($('#save_id').is(":checked")) {
							Cookie.set('save_id', $('#phone').val(), 30, 'login');
						}else{
							Cookie.del('save_id');
						}
						if($('#save_state').is(":checked")) {
							Cookie.set('save_state', true, 30, 'login');
						}else{
							Cookie.del('save_state');
						}
						location.href = response.url;
					}
					if( response.result == 'error' ) { 
					 
						Cmmn.alertMsg(response.msg);
					}
				},
				error : function(xhr, status, error) {},
			})
		}
	});
});
</script>

