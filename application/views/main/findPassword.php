<div class="app_wrapper">
    <!-- [Start] App Header -->
    <header class="app_header">
      <a href="/" class="home_logo"><img src="<?=IMG?>/logo.png" alt="증권찌라시 노다지"></a>
    </header>
    <!-- [End] App Header -->

<!-- [Start] App Main -->
  <form id="frm" name="frm" method="post">
    <div class="app_main sign02">
      <div class="layout_center">
        <h1>비밀번호 변경</h1>
        <div class="input_box has_btn">
          <input type="tel" name="phone" id="phone" placeholder="휴대폰 번호" maxlength="11">
          <button type="button" id="phoneCheck">인증하기</button>
        </div>
        <div class="input_box has_btn">
          <input type="text" name="code" id="code" placeholder="인증번호" maxlength="4">
          <button type="button" id="codeCheck">확 인</button>
        </div>
        <div class="input_box">
          <input type="password" name="passwd" id="passwd" placeholder="비밀번호 (알파벳 숫자조합 6자리 이상)" maxlength="15">
        </div>
        <button type="button" class="btn_l full bg_red" id="submitButton">확인</button>
      </div>
    </div>
  </form>
    <!-- [End] App Main -->
  </div>


<div class="remodal alert" data-remodal-id="alertMsg" id="alertMsg"></div><!-- 알럿메세지 -->


<script type="text/javascript">

var authFlag = false;

$(document).ready(function()
{
    
    // 전체 체크 
    $("#allCheck").change(function(){ Cmmn.checkAll('allCheck'); });

    // 휴대폰인증하기 버튼
    $("#phoneCheck").click(function(){  
        
        if( Is.phone( $('#phone').val() ) ) {
            $.ajax({
                type : "post",
                url : "/main/phoneCheck",
                dataType : 'json',
                data : { 'phone': $('#phone').val(), 'mode' : 'find' },

                success : function(response) {

                    Cmmn.log(response);

                    if( response.result == 'error' ) {  
                        Cmmn.alertMsg(response.msg);
                    }

                    if( response.result == 'success') {
                        Cmmn.alertMsg(response.msg);
                    }

                },

                error : function(xhr, status, error) {},
            })
        }

    });

    // 인증번호체크 버튼
    $("#codeCheck").click(function(){

        $.ajax({
            type : "post",
            url : "/main/codeCheck",
            dataType : 'json',
            data : { 'code' : $('#code').val() },

            success : function(response) {

                if( response.result == 'error' ) {  
                    Cmmn.alertMsg(response.msg); authFlag = false;
                }

                if( response.result == 'success') {
                    $('#code').attr('readonly', true);  // 성공일때 readonly
                    Cmmn.alertMsg(response.msg); authFlag = true;
                }

            },

            error : function(xhr, status, error) {},
        })

    });

    
    // submitButton 
    $("#submitButton").click(function()
    {
      //Cmmn.log(authFlag);    
      if( Is.phone( $('#phone').val() ) && Is.code( $('#code').val() ) && Is.pwd( $('#passwd').val() ) ) {
        
        // 인증번호가 맞는지 체크 
        if(!authFlag) { Cmmn.alertMsg('인증번호가 일치하지 않습니다. 다시확인해주세요.'); return false; }

        $.ajax({
            type : "post",
            url : "/main/modifyPasswd",
            dataType : 'json',
            data : $('#frm').serialize(),

            success : function(response) {

                if( response.result == 'error' ) {  
                    Cmmn.alertMsg(response.msg);
                }

                if( response.result == 'success') {
                    alert(response.msg);
                    location.href = response.url;
                }

            },

            error : function(xhr, status, error) {},
        })

      }

    });

});

</script>