<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * 페이징 Class
  * @author 임정원 / 2019-11-12
  * @since  Version 1.0.1
  *
*/

class Paging
{

	/**
     * 관리자 페이지 페이징
     *
     * @author deanlim
     * @date 2020-02-17
     * @param[limit] 한 페이지에 보여질 row 수
     * @param[page] 현재 페이지
     * @param[total] 총 갯수
     */
    public function admin_pagination($limit, $page, $total)
    {
        $page_num = 10; # 보여질 페이지 갯수
        $total_page = ceil($total / $limit); # 총보여질갯수
        $page_block = ceil($total_page / $page_num); # 총페이지수에서 5개씩 보이이게 20 페이지면 1 2 3 4 5
        $block = ceil($page / $page_num); # 현재 블록
        $first = ($block - 1) * $page_num; # 페이지 블록이 시작하는 첫 페이지
        $last = $block * $page_num; # 페이지 블록의 끝 페이지

        $pageStr = '<ul class="pagination d-flex justify-content-center pagination-warning" id="pagination">';
        $pageStr .= '<li class=page-item><a class=page-link href=javascript:pageRemote(1)><<</a></li>';
        if ($block >= $page_block) {
            $last = $total_page;
        }

        # < 이전 페이지
        if ($page > 1) {
            $go_page = $page - 1;
            $pageStr .= '<li class=page-item><a class=page-link href=javascript:pageRemote("' . $go_page . '")><</a></li>';
        }

        # 1 2 3 4 5 중간 페이지
        for ($page_link = $first + 1; $page_link <= $last; $page_link++) {
            if ($page_link == $page) {
                $pageStr .= '<li class="page-item active"><a class="page-link" href=javascript:pageRemote("' . $page_link . '")>' . $page_link . '</a></li>';
            } else {
                $pageStr .= '<li class="page-item"><a class="page-link" href=javascript:pageRemote("' . $page_link . '")>' . $page_link . '</a></li>';
            }
        }

        # > 다음 페이지
        if ($total_page > $page) {
            $go_page = $page + 1;
            $pageStr .= '<li class="page-item"><a class="page-link" href=javascript:pageRemote("' . $go_page . '")>></a></li>';
        }
        $pageStr .= '<li class=page-item><a class=page-link href=javascript:pageRemote("' . $total_page . '")>>></a></li>';
        $pageStr .= '</ul>';
        return $pageStr;
    }








}
