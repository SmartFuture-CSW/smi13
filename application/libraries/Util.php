<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
  * 각종 기능 Class
  * @author 임정원 / 2019-11-12
  * @since  Version 1.0.1
  * @filesource
  *  v 1.0.0 최초 개발  v 1.0.1 alert , imageUpload , random_pwd 추가
  *
*/
class Util
{

	protected $CI;

	 # 생성자
    function __construct() 
    {
    	$this->CI =& get_instance();
		
		$this->CI->load->library('session');
    }


	# 경고메세지 후 리다이렉트 처리
	function alert($message = '', $location = '') {
		header('Content-type: text/html; charset=utf-8'); echo '<script type="text/javascript">';
		if($message) echo 'alert("'.$message.'");';
		if($location == 'self.close') echo 'self.close();';
		else if($location == 'self.close+reload' || $location == 'reload+self.close')   echo 'opener.location.reload(); self.close();';
		else if($location == 'none') {}
		else if($location){
			$referer = base64_encode($_SERVER['HTTP_REFERER']);

			$location .= "?ref=".$referer;
			echo 'location.href = "'.$location.'";';
		}
		else echo 'window.history.back(-2);';
		echo '</script>';
		if($location != 'none') exit;
	}


    # 로그인 체크 
    function isLogin($boolval, $msg='')
    {
    	if($boolval){
            if( $this->CI->session->userdata('UID') ) $this->alert(ERROR_05, '/main');
        }
        else {
            if( !$this->CI->session->userdata('UID') ) $this->alert(ERROR_04, '/login');
        }
    }


	# 랜덤 패스워드 8자리 처리
	function random_pwd()
	{
		$chars = RANDOM_PWD;
		return substr( str_shuffle( $chars ), 0, 8 );
	}
	

	function convert_phone($phone){

        $sub_str = '';

        if( strlen($phone) == 10 ) 
        	$sub_str = substr($phone, 3, 9);
        else 
        	$sub_str = substr($phone, 3, 10);

        return base64_encode($sub_str);
       
    }
	
	function cut_str($str, $len, $suffix="…"){//추가 채원만
		$arr_str = preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
		$str_len = count($arr_str);

		if ($str_len >= $len) {
			$slice_str = array_slice($arr_str, 0, $len);
			$str = join("", $slice_str);

			return $str . ($str_len > $len ? $suffix : '');
		} else {
			$str = join("", $arr_str);
			return $str;
		}
	}
    
}
