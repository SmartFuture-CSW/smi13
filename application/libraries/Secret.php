<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * 복호화, 암호화 
  * @author 임정원 / 2019-11-12
  * @since  Version 1.0.1
  *
*/

class Secret
{

    const SecretKey     = 'ndg_smi13';				# 64비트 변환
    const SecretAESKey  = 'ndg_smi13_asdfghjklpoiuytrew'; 	# 256비트 변환
    const Secret_iv     = "#@$%^&*()_+=-";

	protected $securus_key = 'aqwemfkrmbkfsdfkljglq14932mfkrqw';

	// DB 저장시 암호화
    public function encrypt($string)
    {
        $key = self::SecretKey;

        $result = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) + ord($keychar));
            $result .= $char;
        }

        return base64_encode($result);
    }

    // DB 조회시 복호화
    public function decrypt($string)
    {
        $key = self::SecretKey;

        $result = '';
        $string = base64_decode($string);
        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) - ord($keychar));
            $result .= $char;
        }

        return $result;
    }

    // AES 암호화 / 복호화
    function aes_encrypt($string)
    {
        $key = self::SecretAESKey;

        $secret_solt    = substr($key, 0, 20);
        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', $key);
        $iv = substr(hash('sha256', $secret_solt), 0, 16);
        $output = openssl_encrypt($string, $encrypt_method, $key, true, $iv);
        return base64_encode($output);
    }

    // AES 복호화
    function aes_decrypt($string)
    {
        $key = self::SecretAESKey;

        $secret_solt    = substr($key, 0, 20);
        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', $key);
        $iv = substr(hash('sha256', $secret_solt), 0, 16);
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, true, $iv);
        return $output;
    }

    function encrypt256($str)
    {
        $key = hash('sha256', self::SecretAESKey);
        $iv = substr(hash('sha256', self::Secret_iv), 0, 16)    ;

        return str_replace("=", "", base64_encode(
                    openssl_encrypt($str, "AES-256-CBC", $key, 0, $iv))
        );
    }

    function decrypt256($str)
    {
        $key = hash('sha256', self::SecretAESKey);
        $iv = substr(hash('sha256', self::Secret_iv), 0, 16);

        return openssl_decrypt(
                base64_decode($str), "AES-256-CBC", $key, 0, $iv
        );
    }


	// AES 인코딩
	public function secretEncode($plain_text) {
		return base64_encode(openssl_encrypt($plain_text, "aes-256-cbc", $this->securus_key, true, str_repeat(chr(0), 16)));
	}

	// AES 디코딩
	public function secretDecode($base64_text) {
		return openssl_decrypt(base64_decode($base64_text), "aes-256-cbc", $this->securus_key, true, str_repeat(chr(0), 16));
	}

	//노다지vip 메신저로 기간 갱신 전송
	public function call_ndg_vip($phone, $startdate, $enddate, $roomNo = 1){
		$ch = curl_init();
		$param="updatekey=".urlencode("da9cb07e9229b00cbb0037b71ac143be")."&phone=".urlencode($phone)."&startdate=".urlencode($startdate)."&enddate=".urlencode($enddate)."&room_idx=".$roomNo;
		curl_setopt($ch,CURLOPT_URL, 'https://nodaji.yesbit.co.kr/AdminApi/membersUpdate');
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $param);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,5);
		curl_setopt($ch,CURLOPT_TIMEOUT, 20);
		curl_setopt($ch,CURLOPT_MAXREDIRS,10);
		curl_setopt($ch,CURLOPT_FOLLOWLOCATION, true);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}


}