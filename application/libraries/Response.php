<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
  * Response Class 
  * @author 임정원 / 2019-11-12
  * @since  Version 1.0.1
  * @filesource
  *   ajax, view, array 등 요청에 대한 응답 return
  *
*/

class Response
{

	protected $CI;

	# 생성자
    function __construct() 
    {
    	$this->CI =& get_instance();
		//$this->CI->load->library('session');
    }

    function View($viewpage, $data='')
    {
        $this->CI->load->view('include/header');
        $this->CI->load->view($viewpage, $data);
        $this->CI->load->view('include/footer');
    }


    function Json($result, $msg, $url='', $data='')
    {
        
        $return = array();

        $return['result']   = $result;      # 성공 : success, 실패 : error  
        $return['msg']      = $msg;         # 메시지 

        if( '' != $url ) 
          $return['url'] = $url;          # location.href
        if( '' != $data ) 
          $return['list'] = $data;        # data list 

        echo json_encode($return); 

    }




}
