<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * 복호화, 암호화 
  * @author 임정원 / 2019-11-12
  * @since  Version 1.0.1
  *
*/

class Sms
{
	const ApiUrl 		= "http://www.sendmon.com/_REST/smsApi.asp";	 						# API url 
	const ApiKey 		= "7F5724409BB21A870C576B4AEB2DDC6D176473415CD638F3C2CDE8C91C132152";   # API KEY 
    const Category 		= "send";
    const SendNum 	= "18995445";


    # 회원가입, 비밀번호 재설정 인증번호 코드 SMS 발송 
    public function authCodeSend($receive_number, $auth_code)
    {
        
        if(!isset($receive_number)) exit;

        # 초기화 
        $ch = curl_init();
        # 메세지 
        $msg = "[노다지모바일인증] 인증코드는 [".$auth_code."] 입니다.";
        # 설정값 
        $sendparams = "apikey=" . urlencode(self::ApiKey) . "&category=" . urlencode(self::Category)
                                . "&param=" . urlencode("sms") . "&send_num=" . urlencode(self::SendNum)
                                . "&receive_nums=" . urlencode($receive_number) . "&title=" . urlencode("")
                                . "&message=" . urlencode($msg);
        # curl 호출
        curl_setopt($ch,CURLOPT_URL, self::ApiUrl);
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $sendparams);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        
        $response = curl_exec($ch);
        
        curl_close ($ch);   
        
        unset($ch);

    }

    public function smsSend($receive_num, $vType, $title, $msg,$url=self::ApiUrl,$add='',$cate=self::Category)
    {      
        if($receive_num=='' || $msg=='') return false;
        $ch = curl_init();
        $sendparams = "apikey=" . urlencode(self::ApiKey) . "&category=" . urlencode($cate)
                                . "&param=" . urlencode($vType) . "&send_num=" . urlencode(self::SendNum)
                                . "&receive_nums=" . urlencode($receive_num) . "&title=" . urlencode($title)
                                . "&message=" . urlencode($msg).$add;
        # curl 호출
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $sendparams);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        $res = curl_exec($ch);
        curl_close ($ch);   
        unset($ch);
		return $res;
    }



    # 원본 메세지 소스 
    function test()
    {
        $ch = curl_init();
        
        $url = "http://www.sendmon.com/_REST/smsApi.asp";
        $category = "send";
        $param = "sms";
        $send_num="18995445";
        $receive_nums="01029708961";
        $msg="test";
        if(mb_strwidth($msg,"UTF-8") > 90) $param = "lms";
        $apikey = "F380CB2A6EBC18A45E47E6D58D0B53FF1249A112250BD0E426EB4019FD2BF7A1";  // API KEY   
        $title = "lms일때 제목쓰면 됩니다";      // 제목 : sms일경우 공백 
        if($param=="sms") $title="";
        $sendparams = "apikey=" . urlencode($apikey)
                                . "&category=" . urlencode($category)
                                . "&param=" . urlencode($param)
                                . "&send_num=" . urlencode($send_num)
                                . "&receive_nums=" . urlencode($receive_nums)
                                . "&title=" . urlencode($title)
                                . "&message=" . urlencode($msg);
        
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $sendparams);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        $response = curl_exec($ch);       
        curl_close ($ch);      
        unset($ch);

    }

}