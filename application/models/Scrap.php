<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
*/

class Scrap extends CI_Model {


	const TABLE_SCRAP = 'ndg_Scrap';
	const TABLE_BOARD = 'ndg_Board';
	const TABLE_STOCK = 'ndg_Stock';

	# 생성자
	function __construct(){
		parent::__construct();
		$this->load->database();
	}


	public function getScrap(){

		$nUserNo		= $this->session->userdata('UNO');
		$vType			= $this->input->post('vType');

/*
		$nUserNo = 706;
		$vType = 'invest';
*/

		$query = $this->db->from(self::TABLE_SCRAP.' as s')
					->join(self::TABLE_BOARD.' AS b', 'b.nSeqNo = s.nBoardNo', 'left')
					->where('s.nUserNo', $nUserNo)
					->order_by('s.nSeqNo desc')
					->get();

		return $query->result_array();
	}


	public function postScrap($mode = "board") {

		$rtn = null;

		$nBoardNo		= $this->input->post('nBoardNo');
		$nUserNo		= $this->session->userdata('UNO');
		$vType			= $this->input->post('vType');

		$where = [
			"nBoardNo" => $nBoardNo, 
			"nUserNo" => $nUserNo, 
			"vType" => $vType
		];

		$this->db->where($where);
		$this->db->from(self::TABLE_SCRAP);
		$cnt = $this->db->get()->num_rows();


		if($cnt > 0){
			$rtn = [
				"status" => "FAIL",
				"code" => "9300",
				"msg" => "이미 스크랩 하였습니다"
			];
		}
		else{

			$this->db->trans_start(); // 트랜잭션 시작
			// 스크립 테이블의 회원 스크랩 이력 등록
			$set = ["nBoardNo" => $nBoardNo, "nUserNo" => $nUserNo, "vType" => $vType, "vIp" => $_SERVER['REMOTE_ADDR']];
			$result = $this->db->set($set)->insert(self::TABLE_SCRAP);

			// 게시물의 스크랩수 갱신
			$where = [ "nSeqNo" => $nBoardNo,  "vType" => $vType ];
			if($mode == "stock"){
				$this->db->set("nScrap", 'nScrap+1', false)->where($where)->update(self::TABLE_STOCK);
			}else{
				$this->db->set("nScrap", 'nScrap+1', false)->where($where)->update(self::TABLE_BOARD);
			}

			$where = [ "nSeqNo" => $nBoardNo,  "vType" => $vType ];
			$this->db->select('nScrap');
			$this->db->where($where);
			if($mode == "stock"){
				$this->db->from(self::TABLE_STOCK);
			}else{
				$this->db->from(self::TABLE_BOARD);
			}
			$cnt = $this->db->get()->row_array();
			

			$this->db->trans_complete(); // 트랜잭션 종료

			$rtn = [
				"status" => 'SUCCESS' ,
				'code' => '0000',
				'cnt' => $cnt
			];
		}


		return $rtn;

		//return $this->db->set($set)->insert(self::TABLE_SCRAP);
	}

	public function getIsScrap($nBoardNo, $type){


		$this->db->from(self::TABLE_SCRAP);
		$this->db->where('nUserNo', $this->session->userdata('UNO'));
		$this->db->where('nBoardNo', $nBoardNo);
		$this->db->where('vType', $type);


		if($this->db->get()->num_rows() > 0){
			return 'Y';
		}
		else{
			return 'N';
		}

	}

}