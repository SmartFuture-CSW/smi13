<?php defined('BASEPATH') OR exit('No direct script access allowed');


class PaymentModel extends CI_Model {

	const table_paymentLog = 'ndg_PaymentLog';
	const table_payment = 'ndg_Payment';
	const table_refund = 'ndg_Refund';
	const table_goods = 'goodds';

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	/**
	* Payment 회원 테이블 DB 가져오기
	* @author 채원만
	* @param[pageNo] 페이지 번호
	* @param[total] 총 ROW 갯수
	* @param[limit] 한페이지에 보여질 갯수 */
	public function getPaymentList($pageNo, $total, $limit, $nType = null){

		# search
		$keyfield = $this->input->post('keyfield') ? $this->input->post('keyfield') : '';
		$keyword    = $this->input->post('keyword') ? $this->input->post('keyword'):'';
		if($keyfield == "vPhone" && !empty($keyword)){
			$keyword = $this->secret->secretEncode($keyword);
		}

		$limit = $this->input->post('limit')?$this->input->post('limit'):'20';
		$ex = $this->input->post('ex') ? $this->input->post('ex'):'';
		$limit=$ex=='ex'?50000:20;

		$pagree = $this->input->post('pagree')?$this->input->post('pagree'):'';
		$pmethod = $this->input->post('pmethod')?$this->input->post('pmethod'):'';
		$gn = $this->input->post('gn')?$this->input->post('gn'):'';
		$reg1 = $this->input->post('reg1') ? $this->input->post('reg1'):date("Y-m-01");
		$reg2 = $this->input->post('reg2') ? $this->input->post('reg2'):date("Y-m-d");

		# 검색 처리
		if($keyword && $keyfield){
			$this->db->like('A.'.$keyfield, $keyword, 'both');
		}
		if($pmethod) $this->db->where("A.emMethod", $pmethod);
		if($pagree) $this->db->where("A.emAgree", $pagree);
		if($gn){
			if($gn=='1'){
				$this->db->where("A.nGoodsNo IN (1,2,3,4,5) ",NULL,FALSE);
			}else if($gn=='2'){
				$this->db->where("A.nGoodsNo IN (6,8) ",NULL,FALSE);
			}else if($gn=='3'){
				$this->db->where("A.nGoodsNo IN (7,9) ",NULL,FALSE);
			}
		}
		if($reg1){
			if($nType == 1){
				$this->db->where("CAST(A.vStartDate AS DATE) >= ".$this->db->escape($reg1),NULL,FALSE);
			}else{
				$this->db->where("CAST(A.dtRegDate AS DATE) >= ".$this->db->escape($reg1),NULL,FALSE);
			}
		}
		if($reg2){
			if($nType == 1){
				$this->db->where("CAST(A.vStartDate AS DATE) <= ".$this->db->escape($reg2),NULL,FALSE);
			}else{
				$this->db->where("CAST(A.dtRegDate AS DATE) <= ".$this->db->escape($reg2),NULL,FALSE);
			}
		}
		if($nType) $this->db->where_in("A.nType", $nType);


		# 페이징처리
		$start_record = ($pageNo - 1) * $limit;

		$this->db->select('A.*, B.vPhone, B.vName AS uName, IFNULL((SELECT vBank FROM ndg_PaymentInfo WHERE nUserNo=A.nUserNo LIMIT 1),0) AS bank, IFNULL((SELECT CONCAT(vCardNo_01,"-",vCardNo_02,"-",vCardNo_03,"-",vCardNo_04) FROM ndg_PaymentInfo WHERE nUserNo=A.nUserNo LIMIT 1),0) AS cardno, G.vGoodsKind, G.nMonth, G.nRoomNo');
		$this->db->from('ndg_Payment AS A');
		$this->db->join('ndg_User AS B', 'A.nUserNo=B.nSeqno', 'LEFT');
		$this->db->join('goods AS G', 'A.nGoodsNo=G.nSeqno', 'LEFT');
		$this->db->where('A.emDelFlags', 'N');
		$this->db->limit($limit, $start_record);
		$this->db->order_by('A.nSeqNo', 'DESC');

		return $this->db->get()->result_array();

	}

	/**
	* Payment 회원 테이블 카운팅
	* @author 채원만 / 2019-11-12 */
	public function getPaymentCount($nType = null) {
		# search
		$keyfield = $this->input->post('keyfield') ? $this->input->post('keyfield') : '';
		$keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
		if($keyfield == "vPhone" && !empty($keyword)){
			$keyword = $this->secret->secretEncode($keyword);
		}
		$limit = $this->input->post('limit')?$this->input->post('limit'):'20';
		$pagree = $this->input->post('pagree')?$this->input->post('pagree'):'';
		$pmethod = $this->input->post('pmethod')?$this->input->post('pmethod'):'';
		$gn = $this->input->post('gn')?$this->input->post('gn'):'';
		$reg1 = $this->input->post('reg1')?$this->input->post('reg1'):date("Y-m-01");
		$reg2 = $this->input->post('reg2')?$this->input->post('reg2'):date("Y-m-d");
		if($nType) $this->db->where_in("A.nType", $nType);

		# 검색 처리
		if($keyword && $keyfield){
			$this->db->like('A.'.$keyfield, $keyword, 'both');
		}
		if($pmethod) $this->db->where("A.emMethod", $pmethod);
		if($pagree) $this->db->where("A.emAgree", $pagree);
		if($gn){
			if($gn=='1'){
				$this->db->where("A.nGoodsNo IN (1,2,3,4,5) ",NULL,FALSE);
			}else if($gn=='2'){
				$this->db->where("A.nGoodsNo IN (6,8) ",NULL,FALSE);
			}else if($gn=='3'){
				$this->db->where("A.nGoodsNo IN (7,9) ",NULL,FALSE);
			}
		}
		if($reg1){
			if($nType == 1){
				$this->db->where("CAST(A.vStartDate AS DATE) >= ".$this->db->escape($reg1),NULL,FALSE);
			}else{
				$this->db->where("CAST(A.dtRegDate AS DATE) >= ".$this->db->escape($reg1),NULL,FALSE);
			}
		}
		if($reg2){
			if($nType == 1){
				$this->db->where("CAST(A.vStartDate AS DATE) <= ".$this->db->escape($reg2),NULL,FALSE);
			}else{
				$this->db->where("CAST(A.dtRegDate AS DATE) <= ".$this->db->escape($reg2),NULL,FALSE);
			}
		}

		$this->db->select( 'A.nSeqNo' );
		$this->db->from('ndg_Payment AS A');
		//$this->db->join('ndg_User AS B', 'A.nUserNo=B.nSeqno', 'LEFT');
		$this->db->where('A.emDelFlags', 'N');

		return $this->db->get()->num_rows();
	}

	public function payRefundList($pageNo, $total, $limit){

		# search
		$keyfield = $this->input->post('keyfield') ? $this->input->post('keyfield') : '';
		$keyword    = $this->input->post('keyword') ? $this->input->post('keyword'):'';
		if($keyfield == "vPhone" && !empty($keyword)){
			$keyword = $this->secret->secretEncode($keyword);
		}

		$limit = $this->input->post('limit')?$this->input->post('limit'):'20';
		$ex = $this->input->post('ex') ? $this->input->post('ex'):'';
		$limit=$ex=='ex'?50000:20;

		$pagree = $this->input->post('pagree')?$this->input->post('pagree'):'';
		$pmethod = $this->input->post('pmethod')?$this->input->post('pmethod'):'';
		$gn = $this->input->post('gn')?$this->input->post('gn'):'';
		$reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';
		$reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';

		# 검색 처리
		if($keyword && $keyfield){
			$this->db->like('P.'.$keyfield, $keyword, 'both');
		}
		if($pmethod) $this->db->where("P.emMethod", $pmethod);
		if($pagree) $this->db->where("P.emAgree", $pagree);
		if($gn) $this->db->where("P.nType", $gn); // dean 수정 vGoodName -> nType 변경 
		if($reg1) $this->db->where("CAST(P.vStartDate AS DATE) >= ".$this->db->escape($reg1),NULL,FALSE);
		if($reg2) $this->db->where("CAST(P.vStartDate AS DATE) <= ".$this->db->escape($reg2),NULL,FALSE);

		# 페이징처리
		$start_record = ($pageNo - 1) * $limit;

		$this->db->select('R.*, P.nAmount, P.vGoodName, P.vStartDate, P.vEndDate, P.vOrderidx, P.nPrice, P.nOrderStatus')
			->from(self::table_refund.' as R')
			->join(self::table_payment.' as P', 'R.nPayNo = P.nSeqNo', 'inner join')
			->limit($limit, $start_record)
			->order_by('R.nSeqNo DESC');
		return $this->db->get()->result_array();
	}

	public function getRefundCount(){

		$keyfield = $this->input->post('keyfield') ? $this->input->post('keyfield') : '';
		$keyword    = $this->input->post('keyword') ? $this->input->post('keyword'):'';
		if($keyfield == "vPhone" && !empty($keyword)){
			$keyword = $this->secret->secretEncode($keyword);
		}

		$limit = $this->input->post('limit')?$this->input->post('limit'):'20';
		$pagree = $this->input->post('pagree')?$this->input->post('pagree'):'';
		$pmethod = $this->input->post('pmethod')?$this->input->post('pmethod'):'';
		$gn = $this->input->post('gn')?$this->input->post('gn'):'';
		$reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';
		$reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';

		# 검색 처리
		if($keyword && $keyfield){
			$this->db->like('P.'.$keyfield, $keyword, 'both');
		}
		if($pmethod) $this->db->where("P.emMethod", $pmethod);
		if($pagree) $this->db->where("P.emAgree", $pagree);
		if($gn) $this->db->where("P.nType", $gn); // dean 수정 vGoodName -> nType 변경 
		if($reg1) $this->db->where("CAST(P.vStartDate AS DATE) >= ".$this->db->escape($reg1),NULL,FALSE);
		if($reg2) $this->db->where("CAST(P.vStartDate AS DATE) <= ".$this->db->escape($reg2),NULL,FALSE);

		$this->db->select('R.*, P.nAmount, P.vGoodName, P.vStartDate, P.vEndDate, P.vOrderidx, P.nPrice')
			->from(self::table_refund.' as R')
			->join(self::table_payment.' as P', 'R.nPayNo = P.nSeqNo', 'inner join')
			->order_by('R.nSeqNo DESC');
		return $this->db->get()->num_rows();
	}


  public function setPaymentJoin()
  {
    $setData = array(
      'vUserId' => $this->input->post('userid'),
      'vUserPwd' => md5($this->input->post('passwd')),
      'vName' => $this->input->post('username'),
      'vHp' => $this->input->post('phone'),
      'vEmail' => $this->input->post('email'),
      'nLevel' =>  $this->input->post('level'),
    );

    if($this->db->insert('Payment', $setData)){
      $this->util->alert('정상적으로 등록되었습니다.', '/admin/payment');
    }else{
      $this->util->alert('등록중 오류가 발생하였습니다. 다시확인해주세요.', '/admin/payment');
    }


  }

  public function getPaymentLogin($pageNo, $total, $limit)
  {
    # search
    $keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
    $searchType = $this->input->post('searchType')?$this->input->post('searchType'):'';

    # 검색 처리
    if($searchType)
      $this->db->like($searchType, $keyword, 'both');

    # 페이징처리
    $start_record = ($pageNo - 1) * $limit;

    $this->db->select('UserLogin.*, User.vPhone');
    $this->db->from('ndj_UserLogin');
    $this->db->join('ndj_User', 'UserLogin.nUserNo = User.nSeqNo');
    $this->db->limit($limit, $start_record);
    $this->db->order_by('UserLogin.nSeqNo', 'DESC');

    return $this->db->get()->result_array();

  }

  public function getPaymentLoginCount()
  {
    # search
    $keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
    $searchType = $this->input->post('searchType')?$this->input->post('searchType'):'';

    $this->db->select('UserLogin.*, User.vUserId');
    $this->db->from('UserLogin');
    $this->db->join('User', 'UserLogin.nUserNo = User.nSeqNo');

    if($searchType) $this->db->like($searchType, $keyword, 'after');

    return $this->db->get()->num_rows();
  }

	public function getPaymentBuyList($no){
		$this->db->select('*');
		$this->db->from('ndg_Payment');
		$this->db->where('nUserNo', $no);
		$this->db->order_by('dtRegDate', 'DESC');
		return $this->db->get()->result_array();
	}

	public function changeStat($no,$agrno) {
		if($agrno=='1') $chgno='0'; else $chgno='1';
		$data=array('emAgree'=>$chgno);
		$this->db->where('nSeqNo', $no);
		$this->db->update('ndg_Payment',$data);
	}


	public function payFailList(){
		$this->db->select('L.*, P.nAmount, P.vGoodName, P.vStartDate, P.vEndDate')
			->from(self::table_paymentLog.' as L')
			->join(self::table_payment.' as P', 'L.nPayNo = P.nSeqNo', 'inner join')
			->order_by('L.nState ASC, L.dtRegdate DESC');
		return $this->db->get()->result_array();
	}

	public function payFailState($nSeqNo, $set){

		$this->db->where('nSeqNo', $nSeqNo);
		$this->db->update(self::table_paymentLog,$set);

		return true;
	}

	public function getDetailPayment($payNo){

		$this->db->select('P.nSeqNo as nPayNo, P.*, R.nSeqNo AS nRefundNo , R.nRefund, R.dtRegDate as dtRefundDate, R.nUseDatePrice, R.nUsePointPrice, R.nCancelPrint, R.nMonthCancel, R.nRefundPrice, R.vUseDate')
			->from(self::table_payment.' as P')
			->join(self::table_refund.' as R', 'R.nPayNo = P.nSeqNo', 'left outer')
			->where('P.nSeqNo', $payNo)
			->order_by('R.nSeqNo DESC');
		return $this->db->get()->row_array();

	}

	public function getEstiPayment($nType, $date){
		$sql_nType = "AND nType in (".implode(", ", $nType).")";
		$sql_date = "";

		/*$reg1 = ($this->input->post("reg1")) ? str_replace("-", "", $this->input->post("reg1")) : date("Ym01", time());
		$reg2 = ($this->input->post("reg2")) ? str_replace("-", "", $this->input->post("reg2")) : date("Ymd", time());

		switch($date){
			case "currentMonth" : 
				$sql_date = "AND (LEFT(REPLACE(vEndDate, '-', ''),6) = '".date('Ym', time())."' OR LEFT(DATE_FORMAT(A.dtRegDate, '%Y%c%d'),6) = '".date('Ym', time())."')";
			break;
			case "nextMonth" : 
				$sql_date = "AND LEFT(REPLACE(vEndDate, '-', ''),6) = '".date('Ym', strtotime("1 month", time()))."'";
			break;
			case "searchDate" : 
				$sql_date = " AND (LEFT(REPLACE(vEndDate, '-', ''),8)  between ". $reg1. " AND ". $reg2;
				$sql_date .=  " OR  LEFT(DATE_FORMAT(A.dtRegDate, '%Y%c%d'),8) between ".$reg1." AND ".$reg2." )";
			break;
		}
		$query = $this->db->query("SELECT SUM(nPrice) as price FROM (
			SELECT nPrice FROM ndg_Payment AS A
			LEFT OUTER JOIN ndg_Refund AS B ON A.nSeqNo = B.nPayNo
			WHERE B.nSeqNo IS NULL
			".$sql_date."
			".$sql_nType."
			AND nOrderStatus = 0
			AND vCardNo != '' AND vExpdt != ''
			GROUP BY A.nUserNo
		) AS A");*/

		$reg1 = ($this->input->post("reg1")) ? $this->input->post("reg1") : date("Y-m-01");
		$reg2 = ($this->input->post("reg2")) ? $this->input->post("reg2") : date("Y-m-d");
		$gn = ($this->input->post("gn")) ? $this->input->post("gn") : '';
		if($gn){
			if($gn=='1'){
				$sql_nType.=" AND nGoodsNo IN (1,2,3,4,5) ";
			}else if($gn=='2'){
				$sql_nType.=" AND nGoodsNo IN (6,8) ";
			}else if($gn=='3'){
				$sql_nType.=" AND nGoodsNo IN (7,9) ";
			}
		}
		$mon1 = date("Y-m-01");
		$mon2 = date("Y-m-", time()).date("t", time());

		$aftermon1 = date('Y-m-01', strtotime("-1 month", time()));
		$aftermon2 = date('Y-m-', strtotime("-1 month", time())).date("t",  strtotime("-1 month", time()));

		$nextmon1 = date('Y-m-01', strtotime("1 month", time()));
		$nextmon2 = date('Y-m-', strtotime("1 month", time())).date("t",  strtotime("1 month", time()));

		$data['price']=0;

		switch($date){
			case "currentMonth" : 
				//$sql_date = "AND (LEFT(REPLACE(vEndDate, '-', ''),6) = '".date('Ym', time())."' OR LEFT(DATE_FORMAT(A.dtRegDate, '%Y%c%d'),6) = '".date('Ym', time())."')";
				// 첫번째 쿼리 = 해당월의 1일부터 말일까지의 결제ㅇ 총액 ex 2020-11-01~2020-11-02의 결제액의 총 합 (결제성공건만)
				// 두번째 쿼리 = 해당월의 1일부터 말일까지의 [결제취소건만] 결제액-취소금액을 합한것의 총 합
				// 세번째 쿼리 = 지난달의 1일부터 말일까지의 결제성공건의 총 합중 이번달에 정기결제가 없는 회원의 건

				$sql_date = "
					SELECT 
						(SELECT IFNULL(SUM(nPrice),0) FROM ndg_Payment WHERE nPrice > 0 AND nType IN (1) AND ((dtRegDate >= '".$mon1." 00:00:00' AND dtRegDate <= '".$mon2." 23:59:59')) AND nOrderStatus='0')
						+
						(SELECT IFNULL(SUM(nPrice - nCancelPrice),0) FROM ndg_Payment WHERE nPrice > 0 AND nType IN (1) AND ((dtRegDate >= '".$mon1." 00:00:00' AND dtRegDate <= '".$mon2." 23:59:59')) AND nOrderStatus='2')
						+
						(SELECT IFNULL(SUM(nPrice),0) FROM ndg_Payment WHERE nPrice > 0 AND nType IN (1) AND dtRegDate >= '".$aftermon1." 00:00:00' AND dtRegDate <= '".$aftermon2." 23:59:59' AND nOrderStatus='0' AND LEFT(vEndDate,7) = '".date("Y-m", time())."' AND nUserNo NOT IN (  SELECT nUserNo FROM ndg_Payment  WHERE nPrice > 0  AND nType IN (1)  AND dtRegDate >= '".$mon1." 00:00:00'  AND dtRegDate <= '".$mon2." 23:59:59' AND nOrderStatus='0')) AS price";
			break;
			case "nextMonth" : 

				// 첫번째 쿼리 = 이번달 결제 성공건중의 다음달결제인것들의 결제총액(6개월,12개월등 제외)
				// 두번째 쿼리 = 아직 진행하기전 지난달 결제 성공건중에 이번달 결제한것들을 제외한 결제총액의 합
				$sql_date = "
				SELECT 
					( SELECT  SUM(nPrice) FROM ndg_Payment  WHERE nPrice > 0 AND nType IN (1) AND dtRegDate >= '".$mon1." 00:00:00' AND dtRegDate <= '".$mon2." 23:59:59' AND nOrderStatus='0' AND LEFT(vEndDate,7) = '". date('Y-m', strtotime("1 month", time()))."' )	
					+
					( SELECT SUM(nPrice) FROM ndg_Payment WHERE nPrice > 0 AND nType IN (1) AND dtRegDate >= '".$aftermon1." 00:00:00' AND dtRegDate <= '".$aftermon2." 23:59:59' AND nOrderStatus='0' AND LEFT(vEndDate,7) = '".date("Y-m", time())."' AND nUserNo NOT IN ( SELECT nUserNo FROM ndg_Payment WHERE nPrice > 0 AND nType IN (1) AND dtRegDate >= '".$mon1." 00:00:00' AND dtRegDate <= '".$mon2." 23:59:59' AND nOrderStatus='0')
					) AS price";
			break;
			case "searchDate" : 
				$sql_date="SELECT
								(SELECT IFNULL(SUM(nPrice),0) FROM ndg_Payment
								WHERE nPrice > 0 ".$sql_nType."
								AND ((dtRegDate >= '".$reg1." 00:00:00' AND dtRegDate <= '".$reg2." 23:59:59'))
								AND nOrderStatus='0')
							+ (SELECT IFNULL(SUM(nPrice - nCancelPrice),0) FROM ndg_Payment
								WHERE nPrice > 0 ".$sql_nType."
								AND ((dtRegDate >= '".$reg1." 00:00:00' AND dtRegDate <= '".$reg2." 23:59:59'))
								AND nOrderStatus='2') AS price";
			break;
		}
		if($sql_date!=''){
			$query = $this->db->query($sql_date);
			$data = $query->row_array();
		}
		return $data['price'];
	}
}