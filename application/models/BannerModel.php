<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin
  * Banner Model
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터베이스 처리후 컨트롤러로 리턴
  *   # index # BannerList # history
  *   # getBannerList # getBannerCount
  *
*/


class BannerModel extends CI_Model
{

  function __construct(){
      parent::__construct();
      $this->load->database();
  }

  /**
  * Banner 회원 테이블 DB 가져오기
  * @author 채원만
  * @param[pageNo] 페이지 번호
  * @param[total] 총 ROW 갯수
  * @param[limit] 한페이지에 보여질 갯수 */
  public function getBannerList($pageNo, $total, $limit)
  {
    # search
    $keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
    $limit = $this->input->post('limit')?$this->input->post('limit'):'10';
	$vType = $this->input->post('vType')?$this->input->post('vType'):'';	

    # 검색 처리
	if($keyword) $this->db->like("A.vSubject", $keyword, 'both');
	if($vType) $this->db->where("A.vType", $vType, 'both');

    # 페이징처리
    $start_record = ($pageNo - 1) * $limit;
    $this->db->select('A.*');
	$this->db->from('ndg_Banner AS A');
	$this->db->limit($limit, $start_record);
    $this->db->order_by('A.nSeqNo', 'DESC');

    return $this->db->get()->result_array();
  }

  /**
  * Banner 회원 테이블 카운팅
  * @author 채원만 / 2019-11-12 */
  public function getBannerCount()
  {
    # search
    $keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
    $limit = $this->input->post('limit')?$this->input->post('limit'):'10';
	$vType = $this->input->post('vType')?$this->input->post('vType'):'';	

    # 검색 처리
	if($keyword) $this->db->like("A.vSubject", $keyword, 'both');
	if($vType) $this->db->where("A.vType", $vType, 'both');

    $this->db->select( 'A.nSeqNo' );
	$this->db->from('ndg_Banner AS A');
    return $this->db->get()->num_rows();
  }

  public function getBannerwrite($bd_no)
  {
	  if($bd_no!=''){
		$this->db->select('A.*');
		$this->db->from('ndg_Banner AS A');
		$this->db->where('A.nSeqNo', $bd_no);
		return $this->db->get()->result_array();
	  }else{
		  return;
	  }
  }

  public function delBannerData()
  {
    $bd_no = $this->input->post('bd_no')?$this->input->post('bd_no'):'';
	if($bd_no!=''){
		$upDir  = $_SERVER['DOCUMENT_ROOT'] . '/data/banner/';
		$this->db->select('A.*');
		$this->db->from('ndg_Banner AS A');
		$this->db->where('A.nSeqNo', $bd_no);
		$row=$this->db->get()->result_array();
		@unlink($upDir.$row[0]['vImage']);
		$this->db->delete('ndg_Banner',array('nSeqNo'=>$bd_no));
	}
  }

  public function setBannerwrite()
  {
	$upDir  = $_SERVER['DOCUMENT_ROOT'] . '/data/banner/';
	$fileName1=$this->input->post('vImage_ori')?$this->input->post('vImage_ori'):'';	
	$fileName1_del=$this->input->post('vImageDel')?$this->input->post('vImageDel'):'';	
	if($fileName1_del!=''){
		@unlink($upDir.$fileName1_del);
		$fileName1='';
	}
	$vLink=$this->input->post('vLink')?$this->input->post('vLink'):'';
	$bd_no=$this->input->post('bd_no')?$this->input->post('bd_no'):'';

	if(isset($_FILES['vImage'])){
		if ($_FILES['vImage']['name']) {
			$name = $_FILES['vImage']['name'];
			$size = $_FILES['vImage']['size'];
			$tmp  = $_FILES['vImage']['tmp_name'];
			$fileName1 = date('YmdHis').rand(10,99).'_'.$name;

			// 사이즈 체크
			if ($size > 5242880) {
				$this->util->alert('파일용량은 5MB를 초과하지 않아야 합니다.', '');
				exit;
			}
			// 업로드성공
			if (! move_uploaded_file($tmp, $upDir . basename($fileName1))) {
				$this->util->alert('파일1 업로드 실패!', '');
				exit;
			} else {
				// tmp 파일 삭제
				@unlink($_FILES['vImage']['tmp_name']);
			}
		}
	}
	$insertData = [
		'vType'      => $this->input->post('vType'),
		'emKind' => $this->input->post('emKind'),
		'vSubject' => $this->input->post('vSubject'),
		'vImage' => $fileName1,
		'vLink' => $vLink,
	];
	if($bd_no!=''){
		$where = ['nSeqNo =' => $bd_no];
		$result=$this->db->set($insertData)->where($where)->update("ndg_Banner");
	}else{
		$result=$this->db->set($insertData)->insert("ndg_Banner");
	}

	if (!$result) {
		$this->util->alert('DB 오류!', '');
		return false;
	}
	return true;
  }

    public function setBbsupdate()
  {
	$bd_no = $this->input->post('bd_no')?$this->input->post('bd_no'):'';
	$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
	$bo_table = $this->input->post('bo_table')?$this->input->post('bo_table'):'';	
	if($bo_table==''){
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
	}
	if($bo_table=='') $bo_table='theme';
	$udname=$this->input->post('udname')?$this->input->post('udname'):'';	
	$udval=$this->input->post('udval')?$this->input->post('udval'):'';

	$insertData = [
		$udname => $udval,
	];
	if($bd_no!='' && $udname!='' && $udval!=''){
		$where = ['nSeqNo =' => $bd_no];
		$result=$this->db->set($insertData)->where($where)->update("ndg_Board".($bo_table=='reply'?'Reply':''));
	}
	if (!$result) {
		$this->util->alert('DB 오류!', '');
		return false;
	}
	return true;
  }

  public function getBannerMainList($no)
  {
    $this->db->select('*');
    $this->db->from('ndg_BoardMain');
	$this->db->where('vMainVal',$no);
    $this->db->order_by('nSort', 'ASC');
    return $this->db->get()->result_array();
  }

  public function setBannerMainUpdate()
  {
	$fval = $this->input->post('fval')?$this->input->post('fval'):'';
	$fval2=explode('<nodaji>',$fval);
	for($k=0;$k<count($fval2);$k++){
		$vMainVal=$k+4;
		if($fval2[$k]=='') continue;
		$fval3=explode('|',$fval2[$k]);
		for($j=0;$j<count($fval3);$j++){
			if($fval3[$j]=='') continue;
			$fval4=explode(',',$fval3[$j]);
			$udtData=[
				'vType'=>$fval4[1],
				'nBoardNo'=>$fval4[2],
			];
			$where = ['vMainVal =' => $vMainVal,'nSort =' => $fval4[0]];
			$result=$this->db->set($udtData)->where($where)->update("ndg_BoardMain");
		}
	}
	return true;
  }

  public function getBannerMainAutoList($mainval)
  {
	if($mainval==6) $ea=5; else $ea=3;
    $this->db->select('*');
	$this->db->from('ndg_Banner');
	$this->db->where("vType IN ('theme','period','rumor')");
	$this->db->order_by('nSeqNo', 'DESC');
    $this->db->limit($ea,0);
	return $this->db->get()->result_array();
  }

  public function updateField($no,$field,$fval)
  {
	$data=array($field=>$fval);
	$this->db->where('nSeqNo', $no);
    $this->db->update('ndg_Banner',$data);
  }
}
