<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin
  * Point Model
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터베이스 처리후 컨트롤러로 리턴
  *   # index # StockList # history
  *   # getStockList # getStockCount
  *
*/


class PointModel extends CI_Model
{

  function __construct(){
      parent::__construct();
      $this->load->database();
  }

  /**
  * Point 회원 테이블 DB 가져오기
  * @author 채원만
  * @param[pageNo] 페이지 번호
  * @param[total] 총 ROW 갯수
  * @param[limit] 한페이지에 보여질 갯수 */
  public function getPointList($pageNo, $total, $limit)
  {
    # search
    $keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
    $limit = $this->input->post('limit')?$this->input->post('limit'):'10';
    $vType = $this->input->post('vType')?$this->input->post('vType'):'';
    $reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';
    $reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';
	$keyword_where=$vType_where=$reg1_where=$reg2_where='';

    # 검색 처리
	if($keyword) $keyword_where=" AND CONCAT(A.vName,'|',A.vPhone) LIKE '%".$keyword."%'";
	if($reg1) $reg1_where=" AND dtRegDate >= '".$this->db->escape($reg1." 00:00:00")."'";
	if($reg2) $reg2_where=" AND dtRegDate <= '".$this->db->escape($reg2." 00:00:00")."'";
    if($vType=='free') $vType_where=" AND A.nSeqNo NOT IN (SELECT nUserNo FROM ndg_Payment) "; else if($vType=='charge') $vType_where=" AND A.nSeqNo IN (SELECT nUserNo FROM ndg_Payment) ";

    # 페이징처리
    $start_record = ($pageNo - 1) * $limit;

	$sql="SELECT A.*, (SELECT COUNT(vShareObj) FROM ndg_Share WHERE vShareObj='facebook' AND nUserNo=A.nSeqNo ".$reg1_where.$reg2_where.") AS nFb, (SELECT COUNT(vShareObj) FROM ndg_Share WHERE vShareObj='kakao' AND nUserNo=A.nSeqNo ".$reg1_where.$reg2_where.") AS nKakao, (SELECT COUNT(vShareObj) FROM ndg_Share WHERE vShareObj='sms' AND nUserNo=A.nSeqNo ".$reg1_where.$reg2_where.") AS nSms FROM ndg_User A WHERE A.emDelFlag='N' AND A.vPhone<>'admin' ".$keyword_where.$vType_where." ORDER BY (SELECT COUNT(vShareObj) FROM ndg_Share WHERE nUserNo=A.nSeqNo ".$reg1_where.$reg2_where.") DESC LIMIT ".$start_record.", ".$limit;
	$query=$this->db->query($sql);
    return $query->result_array();
  }

  /**
  * Stock 회원 테이블 카운팅
  * @author 채원만 / 2019-11-12 */
  public function getPointCount()
  {
    # search
    $keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
	$vType = $this->input->post('vType')?$this->input->post('vType'):'';	
    $reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';
    $reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';
	$keyword_where=$vType_where=$reg1_where=$reg2_where='';

    # 검색 처리
	if($keyword) $keyword_where=" AND CONCAT(A.vName,'|',A.vPhone) LIKE '%".$keyword."%'";
	if($reg1) $reg1_where=" AND dtRegDate >= '".$this->db->escape($reg1." 00:00:00")."'";
	if($reg2) $reg2_where=" AND dtRegDate <= '".$this->db->escape($reg2." 00:00:00")."'";
    if($vType=='free') $vType_where=" AND A.nSeqNo NOT IN (SELECT nUserNo FROM ndg_Payment) "; else if($vType=='charge') $vType_where=" AND A.nSeqNo IN (SELECT nUserNo FROM ndg_Payment) ";

	$sql="SELECT A.nSeqNo FROM ndg_User A WHERE A.emDelFlag='N' AND A.vPhone<>'admin' ".$keyword_where.$vType_where;
	$query=$this->db->query($sql);
    return $query->num_rows();
  }

  public function setPoint($no,$point,$msg='관리자 포인트 증정')
  {
	  if($no!='' && $point!=''){
		$insertData = [
			'nUserNo'    => $no,
			'nPointKind'	=> (ceil($point)<0?2:1),
			'nPoint'      => $point,
			'vDescription' => $msg,
			'vUrl' => "/admin/point",
			'vDate' => date("Y-m"),
			'vDay' => date("d"),
		];
		$result=$this->db->set($insertData)->insert("ndg_UserPoint");
		$result2=$this->db->query("UPDATE ndg_User SET nPoint=nPoint+ ? WHERE nSeqNo= ?", array($point,$no));
		if (!$result || !$result2) {
			$this->util->alert('DB 오류!', '');
			return false;
		}
	  }
	  return true;
  }
}
