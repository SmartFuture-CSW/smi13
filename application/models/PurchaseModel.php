<?php defined('BASEPATH') OR exit('No direct script access allowed');


class PurchaseModel extends CI_Model {


	const TABLE_GOODS = 'goods';
	const TABLE_GOODS_POINT = 'goods_point';
	const TABLE_PAY_REULST = 'ndg_Payment';
	const TABLE_PAY_REFUND = 'ndg_Refund';
	const TABLE_PAY_LOG = 'ndg_PaymentLog';
	const TABLE_CHANGE_LOG = 'ndg_ChangeLog';
	const TABLE_USER = 'ndg_User';
	const TABLE_USERPOINTLOG = 'ndg_UserPoint';


	const sendNum = '18995445';
	const smsapikey = '7F5724409BB21A870C576B4AEB2DDC6D176473415CD638F3C2CDE8C91C132152';


	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('Secret');
	}

	// VIP구독 상품정보 출력
	public function getGoodsInfo(){
		$rtn = null;
		$this->db->order_by('nOrderNo asc');
		$this->db->from(self::TABLE_GOODS);
		$this->db->where('nStatus', 1);
		$data = $this->db->get()->result_array();
		foreach($data as $row){
			$rtn[$row['vGoodsKind']][] = $row;
		}
		return $rtn;
	}

	// VIP구독 상품정보 출력
	public function getGoodsData($nGoodsNo){
		$rtn = null;
		$this->db->from(self::TABLE_GOODS);
		$this->db->where('nSeqNo', $nGoodsNo);
		$data = $this->db->get()->row_array();
		return $data;
	}

	// 오늘의 정기결제 갱신 데이터 출력
	public function getTodayPay(){
		$today = date("Y-m-d");

		/**
		SELECT * FROM 
			`ndg_Payment` AS P
			LEFT OUTER JOIN ndg_Refund AS RF ON P.nSeqNo = RF.nPayNo AND P.nUserNo = RF.nUserNo
		WHERE 
			nType = 1 
			AND vStartDate = '2020-09-10'
			AND RF.nSeqNo IS NULL
		*/
		/*SELECT `P`.* FROM `ndg_Payment` as `P` LEFT OUTER JOIN `ndg_Refund` as `RF` ON `P`.`nSeqNo` = `RF`.`nPayNo` AND `P`.`nUserNo` = `RF`.`nUserNo` 
		WHERE `P`.`nType` = '1' AND `P`.`vEndDate` = '2020-10-24' AND `RF`.`nSeqNo` is null AND `P`.`nSeqNo` != 563 
		ORDER BY `P`.`nSeqNo` asc 확인용*/

		$this->db->select('P.*');
		$this->db->from(self::TABLE_PAY_REULST.' as P');
		$this->db->join(self::TABLE_PAY_REFUND. ' as RF', 'P.nSeqNo = RF.nPayNo AND P.nUserNo = RF.nUserNo', 'left outer');
		$this->db->where('P.nType', '1');
		$this->db->where('P.vEndDate', $today);
		$this->db->where('RF.nSeqNo is null');
		$this->db->where('P.nSeqNo NOT IN (SELECT nPayNo FROM '.self::TABLE_PAY_REFUND.' WHERE nPayNo > 0)');//201106 추가
		$this->db->order_by('P.nSeqNo asc');
		$data = $this->db->get()->result_array();
		return $data;
	}

	// 오늘의 정기결제 실패자 추가처리용
	public function getTodayPayETC($nPayNo){
		//$today = date("Y-m-d");

		/**
		SELECT * FROM 
			`ndg_Payment` AS P
			LEFT OUTER JOIN ndg_Refund AS RF ON P.nSeqNo = RF.nPayNo AND P.nUserNo = RF.nUserNo
		WHERE 
			nType = 1 
			AND vStartDate = '2020-09-10'
			AND RF.nSeqNo IS NULL
		*/

		$this->db->select('P.*');
		$this->db->from(self::TABLE_PAY_REULST.' as P');
		$this->db->join(self::TABLE_PAY_REFUND. ' as RF', 'P.nSeqNo = RF.nPayNo AND P.nUserNo = RF.nUserNo', 'left outer');
		$this->db->where('P.nType', '1');
		$this->db->where('RF.nSeqNo is null');
		$this->db->where('P.nSeqNo', $nPayNo);
		$this->db->order_by('P.nSeqNo asc');
		$data = $this->db->get()->result_array();
		return $data;

	}



	// VIP구독 결제정보 입력
	public function postPayInfo(){

		// 전달 변수 정리
		$nGoodsNo = (!empty($this->input->post("nGoodsNo"))) ? $this->input->post("nGoodsNo") : '';
		$vCardNo = (!empty($this->input->post("vCardNo"))) ? $this->input->post("vCardNo") :  '';
		$vExpdt = (!empty($this->input->post("vExpdt"))) ? $this->input->post("vExpdt") :  '';
		$vName = (!empty($this->input->post("vName"))) ? $this->input->post("vName") : '';
		$vPhone = (!empty($this->input->post("vPhone"))) ? $this->input->post("vPhone") : '';
//		$vExpdt = substr($vExpdt,2,2).substr($vExpdt,0,2);

		$emPayYN = (!empty($this->input->post("emPayYN"))) ? $this->input->post("emPayYN") : 'N';

		if ($emPayYN == "Y"){
			$data = $this->getPayInfo();
			$vCardNo = $data['vCardNo'];
			$vExpdt = $data['vExpdt'];
			$vPhone = $data['vPhone'];
			$vCardNo = substr($vCardNo, 0, 4).$this->secret->secretDecode(substr($vCardNo, 4, strlen($vCardNo)));
			$vExpdt = $this->secret->secretDecode($vExpdt);
			$vPhone = $this->secret->secretDecode($vPhone);
		}
		else{
			// 월년 자리 변경
			$vExpdt = substr($vExpdt,2,2).substr($vExpdt,0,2);
		}


		// 선택 상품정보
		$this->db->select('vGoodsName, nPrice, nMonth, nRoomNo')
			->from(self::TABLE_GOODS)
			->where('nSeqNo', $nGoodsNo);
		$gooodsInfo = $this->db->get()->row_array();

		$vGoodsName = $gooodsInfo['vGoodsName'];
		$nPrice = $gooodsInfo['nPrice'];
		$nMonth = $gooodsInfo['nMonth'];
		$nRoomNo = $gooodsInfo['nRoomNo'];
		$nUserNo = ($this->session->userdata('UNO')) ? $this->session->userdata('UNO') : $_COOKIE['UNO'];

		$nType = 1;
		$nAmount = $nPrice;
		$emMethod = 'C';

		$ordr_idxx = date("YmdHis").'_'.rand(10000000,99999999);
		$pg_data = [
			'nUserNo' => $nUserNo, 
			'nGoodsNo' => $nGoodsNo,
			'vOrderIdx' => $ordr_idxx,
			'nType' => $nType,
			'nMonth' => $nMonth,
			'nOrderStatus' => 0,
			'nPrice' => $nPrice,
			'nAmount' => $nAmount,
			'vName' => $vName,
			'vIp' => $_SERVER['REMOTE_ADDR'],
			'vPhone' => $vPhone,
			'vGoodsName' => $vGoodsName,
			'vCardNo' => $vCardNo,
			'vExpdt' => $vExpdt,
			'smsTrans' => 'Y',
			'cron' => 'N',
			'nPoint' => 0, 
			'nRoomNo' => $nRoomNo
		];

		$data = $this->postPgApi($pg_data);
		return $data;

	}


	// 포인트 결제 입력
	public function postPointPayInfo(){

		// 전달 변수 정ㄹ
		$nGoodsNo = (!empty($this->input->post("nGoodsNo"))) ? $this->input->post("nGoodsNo") : '';
		$vCardNo = (!empty($this->input->post("vCardNo"))) ? $this->input->post("vCardNo") :  '';
		$vExpdt = (!empty($this->input->post("vExpdt"))) ? $this->input->post("vExpdt") :  '';
		$vName = (!empty($this->input->post("vName"))) ? $this->input->post("vName") : '';
		$vPhone = (!empty($this->input->post("vPhone"))) ? $this->input->post("vPhone") : '';
//		$vExpdt = substr($vExpdt,2,2).substr($vExpdt,0,2);
		$emPayYN = (!empty($this->input->post("emPayYN"))) ? $this->input->post("emPayYN") : 'N';


		if ($emPayYN == "Y"){
			$data = $this->getPayInfo();
			$vCardNo = $data['vCardNo'];
			$vExpdt = $data['vExpdt'];
			$vPhone = $data['vPhone'];
			$vCardNo = substr($vCardNo, 0, 4).$this->secret->secretDecode(substr($vCardNo, 4, strlen($vCardNo)));
			$vExpdt = $this->secret->secretDecode($vExpdt);
			$vPhone = $this->secret->secretDecode($vPhone);
		}
		else{
			// 월년 자리 변경
			$vExpdt = substr($vExpdt,2,2).substr($vExpdt,0,2);
		}


		// 선택 상품정보
		$this->db->select('vGoodsName, nPrice, nPoint, nAddPoint')
			->from(self::TABLE_GOODS_POINT)
			->where('nSeqNo', $nGoodsNo);
		$gooodsInfo = $this->db->get()->row_array();


		$vGoodsName = $gooodsInfo['vGoodsName'];
		$nPrice = $gooodsInfo['nPrice'];
		$nPoint = $gooodsInfo['nPoint'] + $gooodsInfo['nAddPoint'];

		$nUserNo = ($this->session->userdata('UNO')) ? $this->session->userdata('UNO') : $_COOKIE['UNO'];

		$nType = 6;
		$nAmount = $nPrice;
		$emMethod = 'C';


		$ordr_idxx = date("YmdHis").'_'.rand(10000000,99999999);
		$pg_data = [
			'nUserNo' => $nUserNo, 
			'nGoodsNo' => 0,
			'nPointNo' => $nGoodsNo,
			'vOrderIdx' => $ordr_idxx,
			'nType' => $nType,
			'nMonth' => 0,
			'nOrderStatus' => 0,
			'nPrice' => $nPrice,
			'nAmount' => $nAmount,
			'vName' => $vName,
			'vIp' => $_SERVER['REMOTE_ADDR'],
			'vPhone' => $vPhone,
			'vGoodsName' => $vGoodsName,
			'vCardNo' => $vCardNo,
			'vExpdt' => $vExpdt,
			'smsTrans' => 'N',
			'cron' => 'N',
			'nPoint' => $nPoint, 
			'nRoomNo' => null,
			'emPointPay' => "Y"
		];


		$data = $this->postPgApi($pg_data);

		if($data['result'] == "SUCCESS"){
			// 회원 포인트 적립 function putUserPoint($nUserNo, $nPoint, $nPointKind, $nType = null, $nBuyNo = null, $msg = null)
			$this->putUserPoint($nUserNo, $nPoint, 1, null, null, MSG_POINT_BUY);

			// 회원 포인트 적립 유/무료 포인트 분리 적립
			$param = [
				'nUserNo' => $nUserNo
				, 'changePoint' => $nPoint
				, 'changeType' => 'add'
				, 'nType' => $nType
				, 'nBuyNo' => null // 테스트
				, 'msg' => MSG_POINT_BUY
				, 'nPointType' => 1
				, 'nFreePoint' => $gooodsInfo['nAddPoint']
				, 'nPayPoint' => $gooodsInfo['nPoint']
			];
			
			$this->Point_model->setChangePoint($param);

			// 회원 포인트 구매 완료 SMS 발송
			$this->postPayPointSMS($vPhone, MSG_POINT_BUY_SMS_TITLE,  MSG_POINT_BUY_SMS);
		}

		return $data;

	}







	// 종목추천 결제정보 입력
	public function postPayInfoStock(){

		// 전달 변수 정ㄹ
		$nGoodsNo = 0; // 종목추천은 stockNo가 존재
		$vCardNo = (!empty($this->input->post("vCardNo"))) ? $this->input->post("vCardNo") :  '';
		$vExpdt = (!empty($this->input->post("vExpdt"))) ? $this->input->post("vExpdt") :  '';
		$vName = (!empty($this->input->post("vName"))) ? $this->input->post("vName") : '';
		$vPhone = (!empty($this->input->post("vPhone"))) ? $this->input->post("vPhone") : '';

		$usePoint = (!empty($this->input->post("usePoint"))) ? $this->input->post("usePoint") : '';
		$userPoint = (!empty($this->input->post("userPoint"))) ? $this->input->post("userPoint") : '';
		$nAmount = (!empty($this->input->post("nAmount"))) ? $this->input->post("nAmount") : '';

		$emPayYN = (!empty($this->input->post("emPayYN"))) ? $this->input->post("emPayYN") : 'N';

		if ($emPayYN == "Y"){
			$data = $this->getPayInfo();
			$vCardNo = $data['vCardNo'];
			$vExpdt = $data['vExpdt'];
			$vPhone = $data['vPhone'];
			$vCardNo = substr($vCardNo, 0, 4).$this->secret->secretDecode(substr($vCardNo, 4, strlen($vCardNo)));
			$vExpdt = $this->secret->secretDecode($vExpdt);
			$vPhone = $this->secret->secretDecode($vPhone);
		}
		else{
			// 월년 자리 변경
			$vExpdt = substr($vExpdt,2,2).substr($vExpdt,0,2);
		}


		$nPrice = $nAmount - $usePoint;
		$nUserNo = $this->session->userdata('UNO');
		$emMethod = 'C';
		$ordr_idxx = date("YmdHis").'_'.rand(10000000,99999999);
		$pg_data = [
			'nUserNo' => $nUserNo, 
			'nStockNo' => $this->session->userdata('STCK_ID'),
			'nGoodsNo' => $nGoodsNo,
			'vOrderIdx' => $ordr_idxx,
			'nMonth' => 0,	// 종목추천은 개월수 필요 없음
			'nOrderStatus' => 0,
			'nPrice' => $nPrice,
			'nAmount' => $nAmount,
			'vName' => $vName,
			'vIp' => $_SERVER['REMOTE_ADDR'],
			'vPhone' => $vPhone,
			'vGoodsName' => $this->session->userdata('STCK_NAME'),
			'vCardNo' => $vCardNo,
			'vExpdt' => $vExpdt,
			'smsTrans' => 'N',
			'cron' => 'N',
			'vGoodName' => $this->session->userdata('STCK_NAME'),
			'nType' => $this->session->userdata('STCK_TYPE'),
			'nPoint' => $usePoint,
		];

		$data = $this->postPgApi($pg_data);
		return $data;

	}



	/**
	* @brief: 크론으로 실행되는 자동 정기결제
	* @author: csw
	*/
	public function autoPayment($row){

		$nSeqNo = $row['nSeqNo'];

		// 정기결제 상품정보
		$nGoodsNo = $row['nGoodsNo'];
		$nUserNo = $row['nUserNo'];

		// 선택 상품정보
		$this->db->select('vGoodsName, nPrice, nMonth, nRoomNo')
			->from(self::TABLE_GOODS)
			->where('nSeqNo', $nGoodsNo);
		$gooodsInfo = $this->db->get()->row_array();

		$vGoodsName = $gooodsInfo['vGoodsName'];
		$nPrice = $gooodsInfo['nPrice'];
		$nMonth = $gooodsInfo['nMonth'];
		$nRoomNo = $gooodsInfo['nRoomNo'];

		$nType = 1;
		$nAmount = $nPrice;
		$emMethod = 'C';

		$vCardNo = $row['vCardNo'];
		$vExpdt = $row['vExpdt'];
		$vPhone = $row['vPhone'];

		$ordr_idxx = date("YmdHis").'_'.rand(10000000,99999999);
		$vCardNo = substr($vCardNo, 0, 4).$this->secret->secretDecode(substr($vCardNo, 4, strlen($vCardNo)));
		$vExpdt = $this->secret->secretDecode($vExpdt);
		$vPhone = $this->secret->secretDecode($vPhone);

		$vName = $row['vName'];
		//$vPhone= $row['vPhone'];
		$nSeqNo = $row['nSeqNo'];
		$pg_data = [
			'nSeqNo' => $nSeqNo,
			'nUserNo' => $nUserNo, 
			'nGoodsNo' => $nGoodsNo,
			'vOrderIdx' => $ordr_idxx,
			'nType' => $nType,
			'nMonth' => $nMonth,
			'nOrderStatus' => 0,
			'nPrice' => $nPrice,
			'nAmount' => $nAmount,
			'vName' => $vName,
			'vIp' => $_SERVER['REMOTE_ADDR'],
			'vPhone' => $vPhone,
			'vGoodsName' => $vGoodsName,
			'vCardNo' => $vCardNo,
			'vExpdt' => $vExpdt,
			'smsTrans' => 'N',
			'cron' => 'Y', 
			'nRoomNo' => $nRoomNo
		];


		$data = $this->postPgApi($pg_data);

		if($data['result']=='SUCCESS'){//첫결제후 30일 이후 첫번째 정기결제때 포인트 반환 201119
			$this->db->select('nSeqNo')
				->from(self::TABLE_PAY_REULST)
				->where('nUserNo', $nUserNo)
				->where('nGoodsNo', $nGoodsNo)
				->where('nType', '1')
				->where('nMonth', '1');
			$repoint = $this->db->get()->num_rows();
			if($repoint==2){
				$setData = array(
					'nUserNo' => $nUserNo,
					'nPoint' => $nPrice,
					'vDescription' => '첫달결제액 포인트로 환불',
					'vUrl' => '/admin/point',
					'vDate' => date("Y-m"),
					'vDay' => date("d")
				);
				$this->db->insert("ndg_UserPoint", $setData);

				$setData = array(
					'nPayNo' => $nSeqNo,
					'nUserNo' => $nUserNo,
					'vName' => $vName,
					'nType' => $nType,
					'vPhone' => $vPhone,
					'vUid' => $data['uid'],
					'vReason' => '첫달결제액 포인트로 환불'
				);
				$this->db->insert(self::TABLE_PAY_LOG, $setData);

				$this->db->set("nPoint", "nPoint + ".$nPrice, false);
				$this->db->where('nSeqNo', $nUserNo);
				$this->db->update(self::TABLE_USER);
			}
		}

		return $data;
	}


	/**
	* @breif: PG사 결제모듈 데이터 전송 및 DB 처리
	*/
	protected function postPgApi($pg_data){

		// reutn 변수 초기화
		$data = [
			'result' => 'FAIL',
			'error' => '',
			'msg' => '',
			'uid' => '',
			'orderstatus' => ''
		];


		// 입력 정보
		$nSeqNo			= (isset($pg_data['nSeqNo'])) ? $pg_data['nSeqNo'] : null ;
		$nUserNo		= $pg_data['nUserNo'];
		$nGoodsNo		= $pg_data['nGoodsNo'];
		$vOrderIdx		= $pg_data['vOrderIdx'];
		$nType			= $pg_data['nType'];
		$nMonth			= $pg_data['nMonth'];
		$nOrderStatus	= $pg_data['nOrderStatus'];
		$nPrice			= $pg_data['nPrice'];
		$nAmount		= $pg_data['nAmount'];
		$vName			= $pg_data['vName'];
		$vIp				= $pg_data['vIp'];
		$vPhone			= $pg_data['vPhone'];
		$vGoodsName	= $pg_data['vGoodsName'];
		$vCardNo		= $pg_data['vCardNo'];
		$vExpdt			= $pg_data['vExpdt'];
		$smsTrans		= $pg_data['smsTrans'];
		$cron				= $pg_data['cron'];
		$nPoint			= (isset($pg_data['nPoint'])) ? $pg_data['nPoint'] : 0 ;
		$nStockNo		= (isset($pg_data['nStockNo'])) ? $pg_data['nStockNo'] : 0 ;
		$nRoomNo		= (isset($pg_data['nRoomNo'])) ? $pg_data['nRoomNo'] : null ;
		$emPointPay		= (isset($pg_data['emPointPay'])) ? $pg_data['emPointPay'] : 'N' ;
		$nPointNo		= (isset($pg_data['nPointNo'])) ? $pg_data['nPointNo'] : 0 ;


		// PG사 변수
		$url = "https://www.shvan.kr/api.php";
		$api_key="*8e9fc600d15d8bd44c4326d46b66a5bb";
		$sh_id="smartfuture2";//shvan 가입한 아이디
		$interesttype="1";//이자구분
		$installment="00";//할부
		$mode='card';

		/**
		* @실제 결제 모듈 데이터 전송
		*/
		$ch = curl_init();
		$url = "https://www.shvan.kr/api.php";
		$para = "api_key=" . urlencode($api_key)
		. "&mode=" . urlencode($mode)
		. "&mb_id=" . urlencode($sh_id)
		. "&amount=" . urlencode($nPrice)
		. "&interesttype=" . urlencode($interesttype)
		. "&expdt=" . urlencode($vExpdt)
		. "&installment=" . urlencode($installment)
		. "&goodname=" . urlencode($vGoodsName)
		. "&ordername=" . urlencode($vName)
		. "&phoneno=" . urlencode($vPhone)
		. "&cardno=" . urlencode($vCardNo);
		 //echo $para;exit;
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $para);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
		curl_setopt($ch,CURLOPT_TIMEOUT, 20);
		$res = curl_exec($ch);
		$jdata=json_decode($res,true);
		curl_close ($ch);
		unset($ch,$res);

		$log = [
			'date' => "[".date("Y-m-d H:i:s")."]",
			'para' => $para,
			'vPhone' => $vPhone,
			'response' => $jdata
		];

		$sYm = date("Ym", time());
		$sYmd = date('Ymd', time());
		$sTargetFile = sprintf("/home/smi13/cron_log/pg/log", $sYm, $sYmd);
//		$this->my_mkdir($sTargetFile); // 폴더생성
	
		if ($oFp = fopen($sTargetFile, 'a+')) {
			fwrite($oFp, json_encode($log, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE) . "\r\n"); // json 형식으로 저장
			fclose($oFp);
		}

		if($jdata['orderstatus'] == "X"){
		//	$data['error'] = $jdata['error'];
			$data['msg'] = $jdata['msg'];
		}
		$data['orderstatus'] = $jdata['orderstatus'];
		$data['uid']=$jdata['uid'];

		if($data['msg']=='' && isset($jdata['msg'])){
			$data['msg'] = $jdata['msg'];
		}

		$cnt_success = 0;
		$cnt_fail = 0;

		// 정기 결제 성공
		if($data['orderstatus']=='O'){
			sleep(1);

			//$vCardNo = $this->secret->secretEncode($vCardNo);
			$vCardNo = substr($vCardNo, 0, 4).$this->secret->secretEncode(substr($vCardNo, 4, strlen($vCardNo)));
			$vExpdt = $this->secret->secretEncode($vExpdt);
			$vPhone = $this->secret->secretEncode($vPhone);

			$ch = curl_init();
			$para = "api_key=" . urlencode($api_key)
			. "&mode=list&uid=".urlencode($data['uid'])
			. "&mb_id=" . urlencode($sh_id);
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, true);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $para);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
			curl_setopt($ch,CURLOPT_TIMEOUT, 20);
			$res = curl_exec($ch);
			$jdata2 = json_decode($res,true);
			curl_close ($ch);
			unset($ch);

			if(substr($res,0,4)=='null'){
				$data['msg']="결제결과 조회실패";
				return json_encode($data);
				exit;
			}

			$vCardName = $jdata2[0]['card_comp'];
			$tno = $jdata2[0]['suc_code'];
			//$app_time = $jdata2[0]['reg_date'];
			$vOrderIdx = $jdata2[0]['order_id'];

		
			$mAgent = array("iPhone","iPod","Android","Blackberry","Opera Mini","Windows ce","Nokia","sony","iPad");
			$chkMobile = false;
			$chkiPhone = false;
			for($i=0; $i<sizeof($mAgent); $i++){
				if(stripos( $_SERVER['HTTP_USER_AGENT'], $mAgent[$i] )){
					$chkMobile = true;
					if($mAgent[$i]=='iPhone' || $mAgent[$i]=='iPod') $chkiPhone = true;
					break;
				}
			}

			$period = $nMonth;
			$vStartDate = date("Y-m-d");
			$vEndDate = date("Y-m-d",strtotime("+".$period." month", time()));

			// 입력 데이터
			$setData = array(
				'nUserNo' => $nUserNo,
				'nGoodsNo' => $nGoodsNo,
				'vGoodName' => $vGoodsName,
				'vOrderIdx' => $vOrderIdx,
				'nType' => $nType,
				'nMonth' => $nMonth,
				'nOrderStatus' => 0,
				'nPrice' => $nPrice,
				'nAmount' => $nAmount,
				'vName' => $vName,
				'vIp' => $_SERVER['REMOTE_ADDR'],
				'vPhone' => $vPhone,
				'vCardName' => $vCardName,
				'vCardNo' => $vCardNo,
				'vExpdt' => $vExpdt,
				'vDevice' => ($chkMobile) ? 'mobile' : 'web',
				'vStartDate' => $vStartDate,
				'vEndDate' => $vEndDate,
				'vUid' => $data['uid'],
				'tno' => $tno,
				'nStockNo' => $nStockNo,
				'nPointNo' => $nPointNo,
				'nPoint' => $nPoint
			);

			$log = [
				'date' => "[".date("Y-m-d H:i:s")."]",
				'data' => $setData
			];
			$sTargetFile = sprintf("/home/smi13/cron_log/pg/log_db");

			if ($oFp = fopen($sTargetFile, 'a+')) {
				fwrite($oFp, json_encode($log, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE) . "\r\n"); // json 형식으로 저장
				fclose($oFp);
			}

			$this->db->insert(self::TABLE_PAY_REULST, $setData);
			$new_nSeqNo = $this->db->insert_id();
			// VIP App 갱신 call_ndg_vip($phone, $startdate, $enddate, $nRoomNo=null)


			if($nStockNo == null && $nRoomNo){
				$aaa = $this->secret->call_ndg_vip($this->secret->secretDecode($vPhone), $vStartDate, $vEndDate, $nRoomNo);
			}

			// SMS 발송 여부
			if($smsTrans == "Y"){
				$this->postPaySMS($this->secret->secretDecode($vPhone), $nGoodsNo);
			}		
			$data['payInfo'] = $setData;
			$data['payInfo']['nBuyNo'] = $new_nSeqNo;
			$sdata = $this->db->select('vGoodsKindName')
				->where('nSeqNo', $nGoodsNo)
				->from(self::TABLE_GOODS)
				->get()->row_array();
			$data['payInfo']['vGoodsKindName'] = $sdata['vGoodsKindName'];
			$data['result'] = 'SUCCESS';
			$cnt_success = $cnt_success + 1;

		}

		// 정기 결제 실패
		else {

				$err = implode("|",$jdata);
				$err = $nSeqNo.'|'.$err;
				$err=$jdata['msg']?$jdata['msg']:$jdata['err_msg'];

				// 입력 데이터
				$setData = array(
					'nPayNo' => $nSeqNo,
					'nUserNo' => $nUserNo,
					'vName' => $vName,
					'nType' => $nType,
					'vPhone' => $this->secret->secretEncode($vPhone),
					'vUid' => $data['uid'],
					'vReason' => $err
				);

				$this->db->insert(self::TABLE_PAY_LOG, $setData);
				$data['payInfo'] = null;
				$data['result'] = 'FAIL';

				$cnt_fail = $cnt_fail + 1;

				if($cron == "Y"){
					$this->postFailSMS($vPhone, $vName, $err);
				}
		}

		$data['cnt_success'] = $cnt_success;
		$data['cnt_fail'] = $cnt_fail;

		return $data;
	}



	// 포인트 구매 SMS 발송
	protected function postPayPointSMS($vPhone, $title= null, $msg = null){

		$ch = curl_init();//문자발송
		$url = "http://www.sendmon.com/_REST/smsApi.asp";
		$category = "send";
		$send_num= self::sendNum;
		$param = "lms";
		$apikey = self::smsapikey;  // API KEY	

		$sendparams = "apikey=" . urlencode($apikey)
		. "&category=" . urlencode($category)
		. "&param=" . urlencode($param)
		. "&send_num=" . urlencode($send_num)
		. "&receive_nums=" . urlencode($vPhone)
		. "&title=" . urlencode($title)
		. "&message=" . urlencode($msg);
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $sendparams);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
		curl_setopt($ch,CURLOPT_TIMEOUT, 20);
		$response = curl_exec($ch);
		curl_close ($ch);	
		unset($ch);

	}



	// 정기결제 구독 신청 SMS 문자 발송
	protected function postPaySMS($vPhone, $nGoodsNo = null){




		if($nGoodsNo != null){
			
			$this->db->select("vGoodsKind");
			$this->db->from(self::TABLE_GOODS);
			$this->db->where('nSeqNo', $nGoodsNo);
			$data = $this->db->get()->row_array();
			$vGoodsKind = $data['vGoodsKind'];

		}



		$ch = curl_init();//문자발송
		$url = "http://www.sendmon.com/_REST/smsApi.asp";
		$category = "send";
		$send_num= self::sendNum;
		$param = "lms";
		$apikey = self::smsapikey;  // API KEY	

		// 프리미엄 VIP
		if($vGoodsKind == "3"){
			$title="안녕하세요 프리미엄 VIP가입이 완료되었습니다. ";
			$msg="안녕하세요 프리미엄 VIP가입이 완료되었습니다. 

다음 안내사항을 꼭 확인해주세요

[안내사항-1] [앱설치 및 이용방법] 
유료회원님을 위한 전용앱이 별도로 운영되고 있습니다. 프리미엄 서비스와 타 서비스의 큰 차이는 종목선정 및 정확한 매수매도 신호를 통한 수익률입니다. VIP전용앱에서는 실시간으로 시황브리핑 매매신호 대응을 드리고 있습니다. 꼭 설치해서 접속해주시기 바랍니다. 

1) VIP앱 설치하기 
구글용 : https://play.google.com/store/apps/details?id=com.nodajivip.talk 
아이폰용 : https://apps.apple.com/kr/app/id1521211001

2) [프리미엄VIP] 서버 접속하기 
설치후 초기 [실시간 리딩] 메뉴에서 접속가능서버가 보이실 겁니다. 구독서비스도 동시에 이용중이라면 하루2종목 추천은 문자를 통해서도 제공되고 있으니 프리미엄 VIP서버에만 접속해서 매매신호대로 대응해주시면 됩니다. 

[안내사항-2] 취소방법
프리미엄서비스는 1개월은 취소가 불가합니다. 다만, 익월에 연장을 취소하시려면 하단취소요청 URL 또는 마이페이지 정기구독 이용내역을 통해 취소신청시 익월 재결제가 되지않습니다. 
VIP구독취소요청하기 : https://www.nodajistock.co.kr/mypage/subscription

앞으로도 더 좋은 정보 및 종목추천을 통해계좌를 +할 수 있도록 최선을 다하는 노다지가 되겠습니다. 감사합니다.";
		}
		else if($vGoodsKind == "2"){
			$title= "안녕하세요 노다지 문자반 가입이 완료되었습니다. ";
			$msg = "안녕하세요 노다지 문자반 가입이 완료되었습니다.   

문자반은 100%문자를 통한 종목추천 및 매수매도 신호가 발송됨으로, 꼭 문자수신이 정상적으로 되는지 사전에 확인이 필요합니다. 

이용전 안내사항을 꼭 확인해주세요

[안내사항] [스팸제거]
간혹 노다지에서 발송되는 문자메시지가 각 통신사별로 자동 스팸 처리되는 경우가 발생하여 인증 번호 및 주요 투자 정보를 못 받는 사례가 발생하고 있습니다. 

이와 관련하여 문자 수신이 안 되시는 분들은 아래 전달드린 링크의 설명대로 앱 설치 후 “스팸 해지 처리” 진행해 주시면 보다 원활한 문자 수신이 가능하십니다.

★★스팸 해지처리 방법: https://bit.ly/2IXHgC0

구독일 기준 1일 이후부터 종목추천 및 매매신호가 나갈 것입니다.  앞으로도 더 좋은 정보 및 종목추천을 통해 구독자분들의 계좌를 +할 수 있도록 최선을 다하는 노다지가 되겠습니다. 감사합니다.";
		}
		else {

		$title="안녕하세요 노다지 구독을 해주셔서 감사합니다.";
		$msg="안녕하세요 노다지 구독을 해주셔서 감사합니다. 

다음 안내사항을 꼭 확인해주세요

[안내사항-1] 노다지 구독자 전용앱설치
노다지 구독자분들을 위한 전용앱이 별도로 운영되고 있습니다. 노다지 구독자분들과 일반 이용자의 가장 큰 차이는 실시간 추천종목입니다. 노다지 메신저 전용앱에서는 실시간으로 시황브리핑 및 오전장, 오후장 각1개 총 하루 2개의 종목추천을 통한 매수매도 대응을 드리고 있습니다. 노다지 구독을 하신분이라면 설치해서 이용해주시기 바랍니다. 

[설치방법안내] 
1) 노다지 메신저앱 설치하기 
구글용 : https://play.google.com/store/apps/details?id=com.nodajivip.talk 
아이폰용 : https://apps.apple.com/kr/app/id1521211001

2) 유료방 [VIP전용 실시간 리딩방] 서버 접속하기 설치후 초기 [실시간 리딩] 메뉴에서 접속가능서버가 1곳 보이실겁니다. 혹시 구독결제이후에도 접속이 안되시는분들은 재실행 최소2회정도만 진행해주시면 접속이 가능하실 것입니다. 

[안내사항-2] 30일 무료체험 및 취소방법
초기 30일동안에는 결제 진행되지만 무료체험이 가능해 취소시 100%환불됩니다.  
30일동안 충분히 노다지 정보를 경험해보고, 괜찮으시면 계속 유지해주시면 됩니다. 
노다지 구독취소요청하기 : https://www.nodajistock.co.kr/mypage/subscription

[안내사항-3] 오픈카톡방
노다지 오픈카톡방도 운영중이며 알찬 정보를 받아 보실 수 있습니다.
오픈카톡방 : https://open.kakao.com/o/gTwxwRoc

앞으로도 더 좋은 정보 및 종목추천을 통해 구독자분들의 계좌를 +할 수 있도록 최선을 다하는 노다지가 되겠습니다. 감사합니다.";
		}

		$sendparams = "apikey=" . urlencode($apikey)
		. "&category=" . urlencode($category)
		. "&param=" . urlencode($param)
		. "&send_num=" . urlencode($send_num)
		. "&receive_nums=" . urlencode($vPhone)
		. "&title=" . urlencode($title)
		. "&message=" . urlencode($msg);
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $sendparams);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
		curl_setopt($ch,CURLOPT_TIMEOUT, 20);
		$response = curl_exec($ch);
		curl_close ($ch);	
		unset($ch);

	}


	// 정기결제 실패 SMS 문자 발송
	protected function postFailSMS($vPhone, $vName, $error_msg){
		sleep(1);
		$ch = curl_init();
		$url = "http://www.sendmon.com/_REST/smsApi.asp";
		$category = "send";
		$send_num= self::sendNum;
		$param = "lms";
		$apikey = self::smsapikey;  // API KEY	
		$title="노다지 정기구독결제 실패안내";
		$msg="[노다지 정기구독결제 실패안내]
고객님의 정기구독결제가 아래의 사유로 처리되지 않았습니다.

사유 : {$error_msg}

카드정보를 확인하시고 다시 결제해 주시기 바랍니다.
정기결제 바로가기 : https://www.nodajistock.co.kr/purchase/payment";

		if(strpos('aa'.$error_msg,'Error. 관리자에게 문의')){
			$receive_nums='01035417344';
			$msg = $error_msg." / tel : ".$vPhone." / name : ".$vName;
		}
		else{
			$receive_nums = $vPhone.",01035417344";
		}

		$sendparams = "apikey=" . urlencode($apikey)
		. "&category=" . urlencode($category)
		. "&param=" . urlencode($param)
		. "&send_num=" . urlencode($send_num)
		. "&receive_nums=" . urlencode($receive_nums)
		. "&title=" . urlencode($title)
		. "&message=" . urlencode($msg);
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $sendparams);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
		curl_setopt($ch,CURLOPT_TIMEOUT, 20);
		$response = curl_exec($ch);
		curl_close ($ch);	
	}



	// 마이페이지 > 해지신청 > 정기결제 구독 해지 @todo 삭제예정
	function modifySubscription($set, $where) {

		$day = $this->db->select('nSeqNo, vStartDate')
			->where('nUserNo', $where['nUserNo'])
			->from(self::TABLE_PAY_REULST)
			->limit(1)
			->order_by('dtRegDate asc')
			->get()->row_array();

		$firstPayDate = strtotime($day['vStartDate']);
		$today = strtotime(date('Y-m-d', time()));
		$diff = ($today - $firstPayDate) / 86400;

		// 최초 결제일 31일 이전 해지신청
		if($diff <= 31){
			// 기존의 해지신청 테이블
			$this->postRefundInfo($where['nSeqNo'], 'all', 0);
		}
		// 최초 결제일 31일 이후 해지신청
		else { 
			$this->db->trans_start();
			// 결제 정보 테이블 수정
			$this->db->set($set);
			$this->db->where('nSeqNo', $where['nSeqNo']);
			$this->db->where('nUserNo', $where['nUserNo']);
			$this->db->update(self::TABLE_PAY_REULST);

			// 정기결제 환불(Cron에서 실행안할 유저 목록 테이블) 입력
			$setData = [
				'nUserNo' => $this->session->userdata('UNO')
				, 'nPayNo' => $where['nSeqNo']
				, 'vName' => $this->session->userdata('UNM')
			];
			$this->db->insert(self::TABLE_PAY_REFUND, $setData);
			$this->db->trans_complete();
		}

		return true;

	}



	// 기존 결제 정보 수집
	public function getPayInfo($nType = null){
		$nUserNo = $this->session->userdata('UNO');
		//$nUserNo=2396;
		$this->db->select('*');
		if($nType != null){
			$this->db->where('nType', $nType);
		}
		$this->db->where('nUserNo', $nUserNo);
		$this->db->where('nOrderStatus', 0);
		$this->db->where('nPrice > 0');
		$this->db->from(self::TABLE_PAY_REULST);
		$this->db->order_by('nSeqNo desc');
		$this->db->limit(1);
		$data = $this->db->get()->row_array();


		$vExpdt = $data['vExpdt'];
//		$vExpdt = $this->secret->secretDecode($data['vExpdt']);
//		$vExpdt = substr($vExpdt,2,2).substr($vExpdt,0,2);
		$vCardNo = $data['vCardNo'];
//		$vCardNo = substr($vCardNo,0,4).$this->secret->secretDecode(substr($vCardNo,4, strlen($vCardNo) - 4));
		$vPhone = $data['vPhone'];
//		$vPhone =  $this->secret->secretDecode($data['vPhone']);

		if($data){
			$rtn = [
				'result' => 'SUCCESS'
				, 'nSeqNo' => $data['nSeqNo']
				, 'vCardNo' => $vCardNo
				, 'vExpdt' => $vExpdt
				, 'vName' => $data['vName']
				, 'vPhone' => $vPhone
				, 'vStartDate' => $data['vStartDate']
				, 'vEndDate' => $data['vEndDate']
				, 'nGoodsNo' => $data['nGoodsNo']
			];
		}
		else{
			$rtn = [
				'result' => 'FAIL'
			];
		}
		return  $rtn;
	}



	// 기존 결제 정보 수집
	public function getPayData($nPayNo){

//		$this->db->select('vCardNo, vExpdt, vName, vPhone, vUid, vStartDate, nPoint, nPrice,nAmount, nUserNo ');
	//	$this->db->where('nType', 1);
		$this->db->where('nSeqNo', $nPayNo);
		$this->db->from(self::TABLE_PAY_REULST);
		$this->db->order_by('nSeqNo desc');
		$this->db->limit(1);
		$data = $this->db->get()->row_array();

		$vExpdt = $this->secret->secretDecode($data['vExpdt']);
		$vExpdt = substr($vExpdt,2,2).substr($vExpdt,0,2);

		$vCardNo = $data['vCardNo'];
		$vCardNo = substr($vCardNo,0,4).$this->secret->secretDecode(substr($vCardNo,4, strlen($vCardNo) - 4));


		if($data){
			$rtn = [
				'result' => 'SUCCESS'
				, 'vCardNo' => $vCardNo
				, 'vExpdt' => $vExpdt
				, 'vName' => $data['vName']
				, 'vPhone' => $data['vPhone']
				, 'vUid' => $data['vUid']
				, 'vStartDate' => $data['vStartDate']
				, 'nPoint' => $data['nPoint']
				, 'nPrice' => $data['nPrice']
				, 'nAmount' => $data['nAmount']
				, 'nUserNo' => $data['nUserNo']
				, 'nStockNo' => $data['nStockNo']
				, 'nType' => $data['nType']
				, 'nGoodsNo' => $data['nGoodsNo']
			];
		}
		else{
			$rtn = null;
		}
		return  $rtn;
	}

	public function putPayInfo(){

		$nUserNo = $this->session->userdata('UNO');
		$vPhone = $this->session->userdata('UID');
		$vCardNo = (!empty($this->input->post("vCardNo"))) ? $this->input->post("vCardNo") :  '';
		$vCardNo = substr($vCardNo,0,4).$this->secret->secretEncode(substr($vCardNo, 4, strlen($vCardNo) -4));	// 암호화

		$vExpdt = (!empty($this->input->post("vExpdt"))) ? $this->input->post("vExpdt") :  '';
		$vExpdt = substr($vExpdt,2,2).substr($vExpdt,0,2);
		$vExpdt = $this->secret->secretEncode($vExpdt);	// 암호화

		// 결제정보 수정 데이터
		$updateSet = array(
			'vCardNo' => $vCardNo,
			'vExpdt' => $vExpdt
		);


		$originalData = $this->getPayInfo();
		$nSeqNo = $originalData['nSeqNo'];

		// 결제정보 수정 로그 입력 데이터 
		$insertSet = array(
			'nUserNo' => $nUserNo
			, 'nPayNo' => $nSeqNo
			, 'vPhone' => $vPhone
			, 'vCardNo' => $vCardNo
			, 'vExpdt' => $vExpdt
		);
	

		$this->db->trans_start();

		// 결제정보 수정
		$this->db->set($updateSet);
		$this->db->where('nSeqNo', $nSeqNo);
		$this->db->update(self::TABLE_PAY_REULST);

		// 결제정보 수정 로그
		$this->db->insert(self::TABLE_CHANGE_LOG, $insertSet);

		$this->db->trans_complete();

		$rtn = [
			'status' => 'SUCCESS',
			'msg' => ''
		];

		return $rtn;

	}



	public function postRefundInfo($nPayNo = null, $sType = null, $cancel_price = null){
//		$this->db->select();

		// PG사 변수
		$url = "https://www.shvan.kr/api.php";
		$api_key="*8e9fc600d15d8bd44c4326d46b66a5bb";
		$sh_id="smartfuture2";//shvan 가입한 아이디
		$interesttype="1";//이자구분
		$installment="00";//할부
		$mode='card';
		$data = [
			'msg' => ''
			,'status' => ''
		];
		$nPayNo = ($nPayNo == null) ? $this->input->post('nPayNo') : $nPayNo;
		$sType = ($sType == null) ? $this->input->post('sType') : $sType;
		$cancel_price = ($cancel_price == null) ? $this->input->post('cancel_price') : $cancel_price;
		$payData = $this->getPayData($nPayNo);

		$nAmount = $payData['nAmount'];
		$nPrice = $payData['nPrice'];
		$nPoint = $payData['nPoint'];
		$nUserNo = $payData['nUserNo'];
		$nGoodsNo = $payData['nGoodsNo'];

		$goodsData = $this->getGoodsData($nGoodsNo);
		$nRoomNo = $goodsData['nRoomNo'];
		$vGoodsKind = $goodsData['vGoodsKind'];

		$vPhone = $this->secret->secretDecode($payData['vPhone']);

		$vStartDate = $payData['vStartDate'];
		$nStockNo = $payData['nStockNo'];
		$nType = $payData['nType'];


		// 전액 포인트 구매
		if($nAmount == $nPoint){
			$data['code']="1";
			$data['msg']="포인트 전액결제 취소 성공 - ";
			$set = [
				'nOrderStatus' => 1
				, 'vCardNo' => ''
				, 'vExpdt' => ''
			];

			$this->db->set($set);
			$this->db->where('nSeqNo', $nPayNo);
			$this->db->update(self::TABLE_PAY_REULST);


			$this->db->select('nPoint');
			$this->db->from(self::TABLE_USER);
			$this->db->where('nSeqNo', $nUserNo);
			$ori = $this->db->get()->row_array();;
			$oriPoint = $ori['nPoint'];


			$userSet = [
				'nPoint' => $oriPoint + $nPoint
			];

			$this->db->set($userSet);
			$this->db->where('nSeqNo', $nUserNo);
			$this->db->update(self::TABLE_USER);


			// 회원 포인트 적립 유/무료 포인트 분리 적립
			$param = [
				'nUserNo' => $nUserNo
				, 'changePoint' => $nPoint
				, 'changeType' => 'add'
				, 'nType' => $nType
				, 'nBuyNo' => null // 테스트
				, 'msg' => '포인트 전액결제 취소 성공'
				, 'nPointType' => 1
				, 'nFreePoint' => 0
				, 'nPayPoint' => $nPoint
			];
			
			$this->Point_model->setChangePoint($param);



			return $data;
		}


		/**
		* @실제 결제 모듈 데이터 전송
		*/
		$ch = curl_init();

		$para = "api_key=" . urlencode($api_key)
		. "&mode=" . urlencode("cancel")
		. "&mb_id=" . urlencode($sh_id)
		. "&uid=" . urlencode($payData['vUid']);
		if($sType=='half') $para.="&can_type=1&can_price=".urlencode($cancel_price);


		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $para);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
		curl_setopt($ch,CURLOPT_TIMEOUT, 20);

		$res = curl_exec($ch);
		$jdata=json_decode($res,true);
		curl_close ($ch);
		unset($ch,$res);


		$data['status']=$jdata['status'];
		if($data['msg']=='' && isset($jdata['msg'])){
			$data['msg'] = $jdata['msg'];
		}


/**
* @@@@
*/
		if($data['status'] != 'O'){
			$data['code']="2";
			$data['msg']="취소실패 - ".$data['msg'];
			if(strpos($data['msg'],' 이미 취소 처리 되')){
				$set = [
					'nOrderStatus' => ($sType == 'half') ? 2:1
					,'nCancelPrice' => ($sType == 'half') ? $cancel_price:'0'
					,'vCardNo' => ''
					,'vExpdt' => ''
				];
				$this->db->set($set);
				$this->db->where('nSeqNo', $nPayNo);
				$this->db->update(self::TABLE_PAY_REULST);
				if($nType == 1){
					$this->secret->call_ndg_vip($vPhone, $vStartDate, date("Y-m-d"), $nRoomNo);
				}
				//call_ndg_vip($vPhone, $vStartDate, date("Y-m-d"));
			}

			return $data;
			//echo $callback.'('.json_encode($data).')';
//			exit;
		}
		else{
			$data['code']="1";
			$set = [
				'nOrderStatus' => ($sType == 'half') ? 2:1
				, 'vCardNo' => ''
				, 'vExpdt' => ''
			];
			$this->db->set($set);
			$this->db->where('nSeqNo', $nPayNo);
			$this->db->update(self::TABLE_PAY_REULST);

			if($nType == 1 && $vGoodsKind != 2){
				$this->secret->call_ndg_vip($vPhone, $vStartDate, date("Y-m-d"), $nRoomNo);
			}

			// 포인트 구매시 포인트 반납
			if($nPoint > 0){

				$this->db->select('nPoint');
				$this->db->from(self::TABLE_USER);
				$this->db->where('nSeqNo', $nUserNo);
				$ori = $this->db->get()->row_array();;

				$oriPoint = $ori['nPoint'];


				$userSet = [
					'nPoint' => $oriPoint + $nPoint
				];

				$this->db->set($userSet);
				$this->db->where('nSeqNo', $nUserNo);
				$this->db->update(self::TABLE_USER);

				return $data;
			}

			return $data;
			//echo $callback.'('.json_encode($data).')';
//			exit;		
		}
	}

	protected function my_mkdir($psPath, $pnMode = 0700){
		$oldumask = umask(0);
		$sPath = dirname($psPath);
		if (!file_exists($sPath)) {
			mkdir($sPath, $pnMode, TRUE);
		}
		umask($oldumask);
	}


	public function getPayCheck(){
		$nUserNo = $this->session->userdata('UNO');
		$nStockNo = $this->session->userdata('STCK_ID');

		$this->db->select('count(nSeqNo) as cnt');
		$this->db->where('nUserNo', $nUserNo);
		$this->db->where('nStockNo', $nStockNo);
		$this->db->where('nOrderStatus', 0);
		$this->db->from(self::TABLE_PAY_REULST);
		$this->db->limit(1);
		$data = $this->db->get()->row_array();


		if($data['cnt'] > 0){
			$rtn = false;
		}else{
			$rtn = true;
		}

		return  $rtn;

		
	}

	public function getBuyGoodsInfo($nGoodsNo){
		$this->db->from(self::TABLE_GOODS);
		$this->db->where('nSeqNo', $nGoodsNo);
		return $this->db->get()->row_array();
	}

	
	public function getPayGoodsInfo(){

		$nUserNo = $this->session->userdata('UNO');
		$nGoodsNo = $this->input->post("nGoodsNo");

		$this->db->select('vGoodsKind');
		$this->db->where('nSeqNo', $nGoodsNo);
		$this->db->from(self::TABLE_GOODS);
		$data = $this->db->get()->row_array();
		$vGoodsKind = $data['vGoodsKind'];

		$this->db->select('count(P.nSeqNo) as cnt');
//		$this->db->where('nGoodsNo', $nGoodsNo);
		$this->db->from(self::TABLE_PAY_REULST." as P");
		$this->db->join(self::TABLE_GOODS." as G", "P.nGoodsNo = G.nSeqNo", "left");
		$this->db->where('P.nUserNo', $nUserNo);
		$this->db->where('P.nOrderStatus', 0);
		$this->db->where('P.nPrice > 0');
		$this->db->where("REPLACE(P.vStartDate, '-', '') <= ".date('Ymd'));
		$this->db->where("REPLACE(P.vEndDate, '-', '') >= ".date('Ymd'));
		$this->db->where("G.vGoodsKind", $vGoodsKind);
		return $this->db->get()->row_array();
			
	}


	public function getCurrentPayGoods(){
		$nUserNo = $this->session->userdata('UNO');
		$this->db->select('G.*');
		$this->db->from(self::TABLE_PAY_REULST." as P");
		$this->db->join(self::TABLE_GOODS." as G", "P.nGoodsNo = G.nSeqNo", "left");
		$this->db->where('P.nUserNo', $nUserNo);
		$this->db->where('P.nOrderStatus', 0);
		$this->db->where('P.nPrice > 0');
		$this->db->where("REPLACE(P.vStartDate, '-', '') <= ".date('Ymd'));
		$this->db->where("REPLACE(P.vEndDate, '-', '') >= ".date('Ymd'));
		return $this->db->get()->result_array();
	}


	// VIP구독 상품정보 출력
	public function getGoodsPoint(){
		$rtn = null;
		$this->db->order_by('nOrderNo asc');
		$this->db->from(self::TABLE_GOODS_POINT);
		$this->db->where('nStatus', 1);
		$data = $this->db->get()->result_array();
		foreach($data as $row){
			$rtn[] = $row;
		}
		return $rtn;
	}

	// 회원 포인트 적립
	public function putUserPoint($nUserNo, $nPoint, $nPointKind, $nType = null, $nBuyNo = null, $msg = null){

		$this->db->trans_start();
		
		$this->db->select('nPoint');
		$this->db->from(self::TABLE_USER);
		$this->db->where('nSeqNo', $nUserNo);
		$ori = $this->db->get()->row_array();;

		$oriPoint = $ori['nPoint'];

		$userSet = [
			'nPoint' => $oriPoint + $nPoint
		];

		$this->db->set($userSet);
		$this->db->where('nSeqNo', $nUserNo);
		$this->db->update(self::TABLE_USER);


		$setData = array(
			'nUserNo' => $nUserNo,
			'nPointKind' => $nPointKind,
			'nType' => $nType,
			'nBuyNo' => $nBuyNo,
			'nPoint' => $nPoint,
			'vDescription' => $msg,
			'vUrl' => '/purchase/paypoint',
			'vDate' => date("Y-m"),
			'vDay' => date("d")
		);
		$this->db->insert(self::TABLE_USERPOINTLOG, $setData);
		$this->db->trans_complete();

	}

	//특정 상품 구매한 회원에게 포인트 적립
	public function addPoint($data){
		$this->db->trans_start();
		$result=false;
		//$nUserNo = ($this->session->userdata('UNO')) ? $this->session->userdata('UNO') : $_COOKIE['UNO'];

		$nUserNo = $data['payInfo']['nUserNo'];

		$this->db->select('*');
		$this->db->from(self::TABLE_PAY_REULST);
		if($data['goodsKind']==2){
			$this->db->where('nGoodsNo IN (6,8)',NULL,FALSE);
		}else if($data['goodsKind']==3){
			$this->db->where('nGoodsNo IN (7,9)',NULL,FALSE);
		}else{
			$this->db->where('nGoodsNo IN (1,2,3,4,5)',NULL,FALSE);
		}
		$this->db->where('nUserNo', $nUserNo);
		$this->db->where("vCardNo <> '' AND vExpdt <> '' AND nOrderStatus=0 ",NULL,FALSE);
		$cnt = $this->db->get()->num_rows();

		if($cnt==1 && $nUserNo!=''){
			$this->db->set('nPoint','nPoint+'.$data['nPoint'],FALSE);
			$this->db->where('nSeqNo', $nUserNo);
			$this->db->update(self::TABLE_USER);

			$setData = array(
				'nUserNo' => $nUserNo,
				'nPoint' => $data['nPoint'],
				'vDescription' => $data['msg'],
				'vUrl' => '/admin/point',
				'vDate' => date("Y-m"),
				'vDay' => date("d")
			);
			$this->db->insert(self::TABLE_USERPOINTLOG, $setData);
			$result=true;
		}
		$this->db->trans_complete();
		return $result;
	}	


	// 환불 레시피 데이터
	public function getRefundReceipt($nPayNo, $nUserNo){

		$today= date("Y-m-d", time());

		// 결제 정보 출력
		$check30d = $this->get30DayCheck($nUserNo, $nPayNo);

		// vip, 일반회원 인지 체크 
		$where = [
			'P.nSeqNo =' => $nPayNo
			, 'P.nUserNo' => $nUserNo
		];

		$select = "";
		$payData =  $this->db->from(TABLE_PAYMENT." as P")
					->join(TABLE_GOODS_SUBSCRIBE." as G", "P.nGoodsNo = G.nSeqNo", "left outer")
					->select("P.vStartDate, P.vEndDate, P.nPrice, G.nPayPoint,G.nMonth,  CONCAT('[', G.vGoodsKindName,']', ' ', P.vGoodName) as vGoodName")
					->where($where)
					->order_by('P.nSeqNo', 'DESC')
					->get()->row_array();

		$where = [
			'nUserNo' => $nUserNo
		];
		$pointData = $this->Point_model->getUserPoint($where);


		// 이용일수 차감액 구하기
		$useDate = date_diff(date_create($payData['vStartDate']), date_create($today));
		$useDay = $useDate->d;
		$checkDay  = ($payData['nMonth'] == '1') ? 30 : 365;
		$day_commission = ($payData['nPrice'] / $checkDay) * $useDay;

		$use_date = $payData['vStartDate']." ~ ". $today; // 이용일수 텍스트

		// 취소수수료 구하기
		$refund_commission = $payData['nPrice'] * 0.1;
		if($check30d != "1") $refund_commission = 0;

		// 사용 포인트 구하기
		$use_point = $pointData['nPayPoint'] - $payData['nPayPoint'];
		if($use_point < 0) $use_point = $use_point * -1;
		else $use_point = 0;

		// 최종 환불 금액 구하기
		$refund_price = $payData['nPrice'] - $day_commission - $refund_commission - $use_point;
		if($refund_price < 0) $refund_price = 0;

		$receiptData = [
			'nPayNo' => $nPayNo
			, 'goods_name' => $payData['vGoodName']
			, 'pay_price' => number_format($payData['nPrice'])
			, 'use_date' => $use_date
			, 'refund_commission'=> number_format($refund_commission)
			, 'day_commission' => number_format($day_commission)
			, 'use_point' => number_format($use_point)
			, 'refund_price' => number_format($refund_price)
		];

		return $receiptData;
	}

	public function get30DayCheck($nUserNo, $nPayNo){

		// 결제 정보
		$payData = $this->getPayData($nPayNo);
		$vPhone = $this->secret->secretDecode($payData['vPhone']);
		$nGoodsNo = $payData['nGoodsNo'];

		// 선택 상품정보
		$this->db->select('vGoodsName, nPrice, nMonth, nRoomNo, nPayPoint, vGoodsKind')
			->from(TABLE_GOODS_SUBSCRIBE)
			->where('nSeqNo', $nGoodsNo);
		$gooodsInfo = $this->db->get()->row_array();
		$nPayPoint = $gooodsInfo['nPayPoint'];
		$nRoomNo = $gooodsInfo['nRoomNo'];
		$vGoodsKind = $gooodsInfo['vGoodsKind'];


		$goodsKind = $this->db->select('nSeqNo')
			->from(TABLE_GOODS_SUBSCRIBE)
			->where('vGoodsKind', $vGoodsKind)
			->get()->result_array();

		$arrGoodsNo = [];
		foreach($goodsKind as $r){
			$arrGoodsNo[] = $r['nSeqNo'];
		}


		// 30일 이내 체크
		$day = $this->db->select('nSeqNo, vStartDate')
			->where('nUserNo', $nUserNo)
			->where_in('nGoodsNo', $arrGoodsNo)
			->from(TABLE_PAYMENT." as P")
			->limit(1)
			->order_by('dtRegDate asc')
			->get()->row_array();

		$firstPayDate = strtotime($day['vStartDate']);
		$today = strtotime(date('Y-m-d', time()));
		$diff = ($today - $firstPayDate) / 86400;
		return ($diff > 30)? "1": "0";

	}



	function postRefundData($where) {

		$nPayNo = $where['nPayNo'];
		$nUserNo = $where['nUserNo'];

		$this->db->trans_start();

		// 결제 정보
		$payData = $this->getPayData($nPayNo);
		$vPhone = $this->secret->secretDecode($payData['vPhone']);
		$nGoodsNo = $payData['nGoodsNo'];

		// 선택 상품정보
		$this->db->select('vGoodsName, nPrice, nMonth, nRoomNo, nPayPoint, vGoodsKind')
			->from(TABLE_GOODS_SUBSCRIBE)
			->where('nSeqNo', $nGoodsNo);
		$gooodsInfo = $this->db->get()->row_array();
		$nPayPoint = $gooodsInfo['nPayPoint'];
		$nRoomNo = $gooodsInfo['nRoomNo'];
		$vGoodsKind = $gooodsInfo['vGoodsKind'];

		// 30일 이내 체크
		/*
		$day = $this->db->select('nSeqNo, vStartDate')
			->where('nUserNo', $nUserNo)
			->from(TABLE_PAYMENT)
			->limit(1)
			->order_by('dtRegDate asc')
			->get()->row_array();

		$firstPayDate = strtotime($day['vStartDate']);
		$today = strtotime(date('Y-m-d', time()));
		$diff = ($today - $firstPayDate) / 86400;
		*/

		$refundInfo = $this->getRefundReceipt($nPayNo, $nUserNo);

		// 정기결제 환불(Cron에서 실행안할 유저 목록 테이블) 입력
		$setData = [
			'nUserNo' => $this->session->userdata('UNO')
			, 'nPayNo' => $nPayNo
			, 'vName' => $this->session->userdata('UNM')
			, 'nMonthCancel' => $this->get30DayCheck($nUserNo, $nPayNo)
			, 'vUseDate' => $refundInfo['use_date']
			, 'nUseDatePrice' => preg_replace('/\.|,|^\/$/', "", $refundInfo['day_commission'])
			, 'nUsePointPrice' => preg_replace('/\.|,|^\/$/', "", $refundInfo['use_point'])
			, 'nCancelPrint' => preg_replace('/\.|,|^\/$/', "", $refundInfo['refund_commission'])
			, 'nRefundPrice' => preg_replace('/\.|,|^\/$/', "", $refundInfo['refund_price'])
		];
		$this->db->insert(TABLE_PAYMENT_REFUND, $setData);

		// 결제 정보 취소 처리
		$set = [
			'nOrderStatus' => 3
			, 'vCardNo' => ''
			, 'vExpdt' => ''
			, 'vEndDate' => date("Y-m-d")
		];
		$this->db->set($set);
		$this->db->where('nSeqNo', $nPayNo);
		$this->db->update(self::TABLE_PAY_REULST);

		$this->secret->call_ndg_vip($vPhone, $day['vStartDate'], date("Y-m-d"), $nRoomNo);

		$changePoint = $nPayPoint - preg_replace('/\.|,|^\/$/', "", $refundInfo['use_point']);

		// 포인트 차감처리
		$param = [
			'nUserNo' => $nUserNo
			, 'changePoint' => $changePoint
			, 'changeType' => 'dud'
			, 'nType' => 1
			, 'nBuyNo' => $nPayNo // 테스트
			, 'msg' => MSG_POINT_REFUND
			, 'nPointType' => null
			, 'nFreePoint' => null
			, 'nPayPoint' => null
		];
		$data = $this->Point_model->setChangePoint($param);


		$this->db->trans_complete();

		return true;

	}

}
