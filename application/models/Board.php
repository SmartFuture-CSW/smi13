<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
    * Board Table 관련 모델 
    * @author 임정원 / 2020-03-30
    * @since  Version 1.0.0
     
    * CRUD 규칙 
    * DB INSERT -> add 테이블명 함수명 
    * DB UPDATE -> set 테이블명 함수명
    * DB DELETE -> del 테이블명 함수명
    * DB SELECT -> get 테이블명 함수명
*/

class Board extends CI_Model
{

	# DB테이블명 정의
    const Board = 'ndg_Board';          # 게시판 테이블
    const Reply = 'ndg_BoardReply';     # 댓글 테이블
    const User  = 'ndg_User';           # 유저 테이블
    const Stock = 'ndg_Stock';          # 게시판 테이블
    const BoardMain = 'ndg_BoardMain';          # 게시판 테이블
    const Banner = 'ndg_Banner';          # 게시판 테이블
    const Category = 'ndg_Category';          # 카테고리 테이블

	const Scrap = 'ndg_Scrap';

    # 생성자
	function __construct(){
      	parent::__construct();
      	$this->load->database();
      	$this->load->library('Secret');	# 비밀번호 암호화 복호화 
	}


	/** 
    * SELETE ndg_Board 
    * @param[$where] : where 절
    **/  
	public function getBoard($where)
	{
		$query = $this->db->from(self::Board)
					->select('*, ABS(TIMESTAMPDIFF(DAY, NOW(), dtRegDate)) as dayDiff')
        			->where($where)
        			->order_by('dtRegDate', 'DESC')
        			->limit(5)->get();

		return $query->result_array();
	}


    /** 
	* SELETE ndg_Board 
	* @param[$where] : where 절
	**/  
	public function getBoardCommunity($where, $limit = null, $like = null) {
		if($limit != null){
			$this->db->limit($limit);
		}

		if($like != null){
			$this->db->like($like);
		}

		$query = $this->db->from(self::Board.' AS b')
					->select('b.nSeqNo, b.vType, b.vSubject, b.txContent, b.vImage, b.txTag, b.nHit, b.nLike, b.dtRegDate, count(r.nBoardNo) as cnt, b.nUserNo, b.nCategoryNo, b.vName, u.vNick')
					->join(self::Reply.' AS r', 'b.nSeqNo = r.nBoardNo', 'left')
					->join(self::User. ' AS u', 'b.nUserNo = u.nSeqNo', 'left')
					->where('b.vType', 'community')
					->where($where)
					->order_by('dtRegDate', 'DESC')
					->group_by("b.nSeqNo")
					->get();

		return $query->result_array();
	}


    /** 
    * SELETE ndg_Board 
    * @param[$where] : where 절
    **/  
    public function getBoardAny($where='')
    {
        $query = $this->db->from(self::Board)
                    ->select('nSeqNo, vType, vSubject, txContent, vImage, txTag, nHit, nLike, dtRegDate')
                    ->order_by('dtRegDate', 'DESC')
                    ->limit(4,6)->get();

        return $query->result_array();
    }
    


	/** 
	* @breif : 인피니티 스크롤에서 사용할 컨텐츠 리스트
	* @param[$where] : where 절
	* @author : csw
	**/  
	public function getBoardInfinity($where='', $page, $vMainVal)
	{
		$offset = 12;
		$limit = ($page -1) * $offset;
		$query = $this->db->from(self::Board.' as A')
					->select('A.nSeqNo, A.vType, A.vSubject, A.txContent, A.vImage, A.txTag, A.nHit, A.nLike, A.dtRegDate')
					->join(self::BoardMain.' as B', 'ON A.nSeqNo = B.nBoardNo AND B.vMainVal = '.$vMainVal, 'left outer')
					->where($where)
					->where('B.nBoardNo is null')
					->order_by('dtRegDate', 'DESC')
					->limit( $offset, $limit)->get();

		return $query->result_array();
	}




	/** 
	* @breif : 인피니티 스크롤에서 사용할 컨텐츠 리스트
	* @param[$where] : where 절
	* @author : csw
	**/  
	public function getBoardInfinitySearch($where='', $page, $vMainVal = null, $keyword)
	{
		$offset = 12;
		$limit = ($page -1) * $offset;

		foreach($where as $k=>$row){
			if(is_arraY($row)){
				$this->db->where_in($k, $row);
			}else{
				$this->db->where($k, $row);
			}
		}

		$query = $this->db->from(self::Board)
					->select('*')
					->like('vSubject', $keyword)
					->order_by('dtRegDate', 'DESC')
					->limit( $offset, $limit)->get();

		return $query->result_array();
	}
    

	/** 
	* @breif : 인피니티 스크롤에서 사용할 컨텐츠 리스트
	* @param[$where] : where 절
	* @author : csw
	**/  
	public function getStockInfinitySearch($where='', $page, $vMainVal = null, $keyword) {
		$offset = 12;
		$limit = ($page -1) * $offset;

		foreach($where as $k=>$row){
			if(is_arraY($row)){
				$this->db->where_in($k, $row);
			}
			else{
				$this->db->where($k, $row);
			}
		}

		$this->db->from(self::Stock);
		$this->db->select('*');
		$this->db->like('vSubject', $keyword);
		$this->db->order_by('dtRegDate', 'DESC');
		//$this->db->limit( $offset, $limit); // 인피니티 스크롤 형식은 두고 실행하지 않음

		$query = $this->db->get();
		return $query->result_array();
	}


	
	/** 
	* @breif : 인피니티 스크롤에서 사용할 컨텐츠 리스트
	* @param[$where] : where 절
	* @author : csw
	**/  
	public function getCommunityInfinitySearch($where='', $page, $vMainVal = null, $keyword) {
		$offset = 12;
		$limit = ($page -1) * $offset;

		$this->db->from(self::Board.' AS b');
		$this->db->join(self::Reply.' AS r', 'b.nSeqNo = r.nBoardNo', 'left');
		$this->db->select('b.nSeqNo, b.vType, b.vSubject, b.txContent, b.vImage, b.txTag, b.nHit, b.nLike, b.dtRegDate, count(r.nBoardNo) as cnt');
		$this->db->like('b.vSubject', $keyword);
		$this->db->where('b.vType', 'community');
		$this->db->order_by('b.dtRegDate', 'DESC');
		$this->db->group_by("b.nSeqNo");
		//$this->db->limit( $offset, $limit); // 인피니티 스크롤 형식은 두고 실행하지 않음



		$query = $this->db->get();
		return $query->result_array();
	}



	/** 
	* SELETE ndg_Board 
	* @param[$keyword] : 검색어
	* @param[$start_record] : 시작 레코드 위치
	**/  
	public function getBoardSearch($keyword, $start_record)
	{
		$query = $this->db->from(self::Board)
					->select('nSeqNo, vType, vSubject, vImage, dtRegDate')
					->like('vSubject', $keyword)
					->where("vType != 'today'")
					->order_by('dtRegDate', 'DESC')
					->limit(5, $start_record)->get();

		return $query->result_array();
	}


    /** 
    * SELETE ndg_Board join ndg_User 
    * @param[$where] : where 절
    **/  
    public function getBoard_User($where)
    {
        $query = $this->db->from(self::Board.' AS b')
                    ->select('b.nSeqNo, b.vSubject, b.txContent, b.nHit, b.dtRegDate, a.vNick, count(r.nBoardNo) as cnt')
                    ->join(self::User.' AS a', 'b.nUserNo = a.nSeqNo')
                    ->join(self::Reply.' AS r', 'b.nSeqNo = r.nBoardNo', 'left')
                    ->where($where)
                    ->order_by('b.dtRegDate', 'DESC')
                    ->limit(5)->get();

        return $query->result_array();
    }


	/** 
	* SELETE ndg_Board 
	* @param[$idx] : 게시판 고유번호 
	**/  
	public function getBoardView($idx){
		$query = $this->db->from(self::Board)
			->select('*,  ABS(TIMESTAMPDIFF(DAY, NOW(), dtRegDate)) as dayDiff')
			->where('nSeqNo', $idx)->get();
		return $query->row_array();
	}


    /** 
    * SELETE ndg_Board join ndg_User 
    * @param[$idx] : 게시판 고유번호
    **/  
    public function getBoardView_User($idx)
    {
        $query = $this->db->from(self::Board.' AS b')
                    ->select('b.*, a.vNick, a.vImage')
                    ->join(self::User.' AS a', 'b.nUserNo = a.nSeqNo')
                    ->where('b.nSeqNo', $idx)
                    ->get();

        return $query->row_array();
    }


    /** 
    * INSERT ndg_Board 
    * @param : 게시판 데이터 
    **/
    public function addBoard($param)
    {
        return $this->db->set($param)->insert(self::Board);
    }


    /** 
     * UPDATE ndg_Board  
     * @param[$set] : set data
     * @param[$where] : where 절 
    **/
    function setBoard($set, $where)
    {
        return $this->db->set($set)->where($where)->update(self::Board);
    }


    /** 
    * UPDATE ndg_Board  
    * @param[$boardNo] : 게시판 고유번호 
    **/
    public function setBoardHit($boardNo,$tb=self::Board)
    {
        return $this->db->set("nHit", "nHit + 1", false)->where("nSeqNo", $boardNo)->update($tb);
    }


    /** 
    * UPDATE ndg_Board  
    * @param[$boardNo] : 게시판 고유번호 
    **/
    public function setBoardLike($boardNo)
    {
        return $this->db->set("nLike", "nLike + 1", false)->where("nSeqNo", $boardNo)->update(self::Board);
    }


    /** 
    * UPDATE ndg_Board  
    * @param[$boardNo] : 게시판 고유번호 
    **/
    public function setBoardShare($boardNo)
    {
        return $this->db->set("nShare", "nShare + 1", false)->where("nSeqNo", $boardNo)->update(self::Board);
    }


    /** 
    * DELETE ndg_Board  
    * @param[$boardNo] : 게시판 고유번호 
    **/
    public function delBoard($param)
    {
        return $this->db->where($param)->delete(self::Board);
    }


    /** 
    * INSERT ndg_BoardReply 
    * @param : 댓글 데이터 
    **/ 
    public function addReply($param)
    {
        return $this->db->set($param)->insert(self::Reply);
    }


	/** 
	* SELETE ndg_BoardReply 
	* @param[$idx] : 댓글 고유번호
	* @param[$start_record] : 더보기 -> 5개씩, limit 갯수  
	**/   
	public function getReply($idx, $start_record) {
		$query = $this->db->from(self::Reply.' AS r')
					->select('r.nSeqNo, r.nBoardNo, r.nUserNo, a.vNick, r.txComment, r.nLike, r.dtRegDate, a.vImage, a.nLevel')
					->join(self::User.' AS a', 'r.nUserNo = a.nSeqNo')
					->where('r.nBoardNo', $idx)
					->order_by('r.nLike', 'DESC')
					->limit(5, $start_record)->get();

		return $query->result_array();
	}


	/** 
	* SELETE ndg_BoardReply 
	* @param[$idx] : 댓글 고유번호
	* @param[$start_record] : 더보기 -> 5개씩, limit 갯수
	* @param[$order_by] : 인기순, 최신순  
	**/   
	public function getReplyOrderBy($idx, $start_record, $order_by, $level)
	{
		$order_by = ( $order_by == 'new' ) ? 'r.dtRegDate':'r.nLike';
		$level = ( $level == '1' ) ? array('1', '2', '10'):array('2');
		$query = $this->db->from(self::Reply.' AS r')
					->select('r.nSeqNo, r.nBoardNo, r.nUserNo, a.vNick, r.txComment, r.nLike, r.dtRegDate, a.vImage, a.nLevel')
					->join(self::User.' AS a', 'r.nUserNo = a.nSeqNo')
					->where('r.nBoardNo', $idx)
					->where_in('a.nLevel', $level)
					->order_by($order_by, 'DESC')
					->limit(5, $start_record)->get();

		return $query->result_array();
	}


    /** 
    * SELETE ndg_BoardReply 
    * @param[$idx] : 댓글 고유번호
    **/
    public function setReplyLike($idx)
    {
        return $this->db->set("nLike", "nLike + 1", false)->where("nSeqNo", $idx)->update(self::Reply);
    }


    /** 
    * DELETE   
    * @param where  
    **/
    public function delReply($param)
    {
        return $this->db->where($param)->delete(self::Reply);
    }


    /** 
    * SELECT ndg_BoardReply 
    * @param[$boardNo] : 게시판 idx 
    **/
    public function getReplyCount($boardNo)
    {
        return $this->db->from(self::Reply)->where('nBoardNo', $boardNo)->count_all_results();
    }



    /** 
    * SELETE ndg_BoardMain 
    * @param[$where] : where 절
    **/  
    public function getBoardMain($where)
    {
        $query = $this->db->from(self::BoardMain. ' AS b')
                    ->select('b.nBoardNo, b.nSort, b.vType, s.*')
                    ->join(self::Board.' AS s', 'b.nBoardNo = s.nSeqNo')
                    ->where($where)
                    ->order_by('b.nSort', 'asc')
                    ->get();

        return $query->result_array();
    }


    /** 
    * SELETE ndg_BoardMain 
    * @param[$where] : where 절
    **/  
    public function getBoardMainStock($where)
    {
        $query = $this->db->from(self::BoardMain. ' AS b')
                    ->select('b.nBoardNo, b.nSort, b.vType, s.*')
                    ->join(self::Stock.' AS s', 'b.nBoardNo = s.nSeqNo')
                    ->where($where)
                    ->order_by('b.nSort', 'asc')
                    ->get();

        return $query->result_array();
    }


	/** 
	* SELETE ndg_Stock
	* @param[$where] : where 절
	**/
	public function getStock($where = null, $like = null) {
		if(!empty($like)) {
			$this->db->like($like);
		}
		if(!empty($where)){
			$this->db->where($where);
		}
		$query = $this->db->from(self::Stock)
					->where('DATE_ADD(dtregdate, INTERVAL nAimDate DAY) > NOW()')
					->where('nstat', 1)
					->order_by('dtRegDate', 'DESC')
					->get();
		return $query->result_array();
	}


	/** 
	* SELETE ndg_Stock
	* @param[$where] : where 절
	**/
	public function getStockResult($offset=null, $like = null) {

		$page = $this->input->post('page') ? $this->input->post('page') : 1;
		$offset = ($offset == null) ? 5 : $offset;
		$limit = ($page -1) * $offset;


		if($like != null){
			$this->db->like($like);
		}

		$query = $this->db->from(self::Stock)
					->where_in('nResult', array('2','3','4'))
					->order_by('dtRegDate', 'DESC')
					->limit($offset, $limit)
					->get();
		return $query->result_array();
	}


	/** 
	* SELETE ndg_Board 
	* @param[$idx] : 게시판 고유번호 
	**/  
	public function getStockView($idx) {
		$query = $this->db->from(self::Stock)->where('nSeqNo', $idx)->get();    
		return $query->row_array();
	}


	/** 
	* SELETE ndg_Banner
	* @param[$where] : where 절
	**/  
	public function getBanner($where)
	{
		if($_SERVER['ios'] == "Y"){
			return array();
			exit;
		}


		$query = $this->db->from(self::Banner)
					->where($where)
					->where('nStat', 1)
					->order_by('dtRegDate', 'DESC')
					->get();
		$banner = $query->result_array();
		shuffle($banner);
		$return[0] = end($banner);
		return $return;
	}


    /** 
    * SELETE ndg_Banner
    * @param[$where] : where 절
    **/  
    public function getBannerPopup($where)
    {
        $query = $this->db->from(self::Banner)
                    ->where($where)
                    ->where('nStat', 1)
                    ->order_by('dtRegDate', 'DESC')
                    ->get();

        return $query->row_array();
    }


	// 검색결과 종목추천_Board
	public function getBoardSearchResult($where, $like) {

		/*
		if(is_array($where['vType'])){
			$this->db->where_in($where['vType']);
		}else{
			$this->db->where($where);
		}
		*/
		foreach($where as $k=>$row){
			if(is_array($row)){
				$this->db->where_in($k, $row);
			}else{
				$this->db->where($k, $row);
			}
		}


		$this->db->from(self::Board);
		$this->db->like($like);
		$this->db->order_by('dtRegDate', 'DESC');
		$this->db->limit(3);

		$query =  $this->db->get();

		return $query->result_array();
	}


	// 검색결과 종목추천_Stock
	public function getStockInfoSearchResult($where, $like) {
		$query = $this->db->from(self::Stock)
					->where_in($where)
					->where('DATE_ADD(dtregdate, INTERVAL nAimDate DAY) > NOW()')
					->where('nstat', 1)
					->like($like)
					->order_by('dtRegDate', 'DESC')
					->limit(3)
					->get();

		return $query->result_array();
	}


	// 검색결과 종목추천_Stock
	public function getStockSearchResult($where, $like) {
		$query = $this->db->from(self::Stock)
					->where($where)
					->where('DATE_ADD(dtregdate, INTERVAL nAimDate DAY) > NOW()')
					->where('nstat', 1)
					->like($like)
					->order_by('dtRegDate', 'DESC')
					->limit(3)
					->get();

		return $query->result_array();
	}



	public function getCategory($vType){


		$query = $this->db->select('nSeqNo, vSubject, vType')
					->from(self::Category)
					->where('vType', $vType)
					->where('emDelFlag', 'N')
					->order_by('nOrderNo', 'ASC')
					->get();


		$data = $query->result_array();


		$arrData = [];

		if($vType != 'important'){
			$arrData[0] = array(
				"nSeqNo" => 0,
				"vSubject" => '전체',
				"vType" => $vType
			);
		}
		$i=1;
		foreach($data as $row){
			$arrData[$i] = $row;
			$i++;
		}

		return $arrData;

	}

	
	// 상세페이지 하단의 오늘의 데이터
	public function getInfoAny($where='') {
		$query = $this->db->from(self::Board)
					->select('nSeqNo, vType, vSubject, txContent, vImage, txTag, nHit, nLike, dtRegDate')
					->where($where)
					->order_by('dtRegDate', 'DESC')
					->limit(4)->get();

		return $query->result_array();
	}
	

	// 커뮤니티 베스트 게시판 배열
	public function getCommunityBest(){
		$query = $this->db->from(self::Board)
					->select('nSeqNo')
					->where('vType', 'community')
					->order_by('nLike', 'DESC')
					->limit(3)
					->get();
		$arr =  $query->result_array();
		$rtn = null;
		foreach($arr as $row){
			$rtn[] = $row['nSeqNo'];
		}
		return $rtn;
	}

	public function getBoardToday($limit = null, $offset=null){
		$order = $this->uri->segment(3) ? urldecode($this->uri->segment(3)) : 'date';
		$limit = ($limit == null) ? 0 : $limit;
		$offset = ($offset == null) ? 5 : $offset;
		switch($order){
			case 'date' : $this->db->order_by('B.dtRegDate', 'desc'); break;
			case 'hit' :  $this->db->order_by('B.nHit', 'desc'); break;
			case 'share' :  $this->db->order_by('B.nShare', 'desc'); break;
			case 'reply' :  $this->db->order_by('count(B.nSeqNo)', 'desc'); break;
			case 'rec' :  $this->db->order_by('B.nLike', 'desc'); break;
			default : $this->db->order_by('B.dtRegDate', 'desc'); break;
		}

		$query = $this->db->from(self::Board." as B ")
					->select('B.*, count(R.nSeqNo) as nReply, ABS(TIMESTAMPDIFF(HOUR, NOW(), B.dtRegDate)) AS hourDiff , ABS(TIMESTAMPDIFF(DAY, NOW(), B.dtRegDate)) as dayDiff')
					->join(self::Reply." as R ", 'B.nSeqNo = R.nBoardNo', 'left')
					->where_in('vType', ['important', 'invest', 'study'])
					->group_by('B.nSeqNo')
					->limit( $offset, $limit)
					->get();
		return $query->result_array();
	}

	public function getBoardTodayNew($limit = null, $offset=null){
		$order = $this->uri->segment(3) ? urldecode($this->uri->segment(3)) : 'date';
		$limit = ($limit == null) ? 0 : $limit;
		$offset = ($offset == null) ? 5 : $offset;
		switch($order){
			case 'date' : $this->db->order_by('dtRegDate', 'desc'); break;
			case 'hit' :  $this->db->order_by('nHit', 'desc'); break;
			case 'share' :  $this->db->order_by('nShare', 'desc'); break;
			case 'reply' :  $this->db->order_by('nReply', 'desc'); break;
			case 'rec' :  $this->db->order_by('nLike', 'desc'); break;
			default : $this->db->order_by('dtRegDate', 'desc'); break;
		}

		$query = $this->db->from(VIEW_LIST_TODAY)
					->where_in('vType', ['important', 'invest', 'study'])
					->group_by('nSeqNo')
					->limit( $offset, $limit)
					->get();
		return $query->result_array();
	}
	


	public function getTxContent($param){
		$query = $this->db->from(self::Board)
			->select('txContent')
			->where('nSeqNo', $param['boardNo'])
			->get();
		return $query->row_array();
	}
}