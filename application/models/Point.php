<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 @breif: 포인트 Ver2.0 사용 모델
 @author: csw
*/
class Point extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}


	// 유저의 전체 포인트 정보 출력
	public function getUserPoint($param){

		$aRtn = $this->select("SUM(nFreePoint + nPayPoint) as nPoint, nFreePoint, nPayPoint")
			->from(TABLE_USER_POINT)
			->where("nUserNo", $param['nUserNo'])
			->get()->num_rows();
		return $aRtn;
	}

}
