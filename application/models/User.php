<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
	* User Table 관련 모델 
	* @author 임정원 / 2020-02-14
	* @since  Version 1.0.0
    
    * CRUD 규칙 
    * DB INSERT -> add 테이블명 함수명 
    * DB UPDATE -> set 테이블명 함수명
    * DB DELETE -> del 테이블명 함수명
    * DB SELECT -> get 테이블명 함수명
*/

class User extends CI_Model
{

	# DB테이블명 정의
    const User 		  = 'ndg_User';		    # 유저 테이블
    const UserLogin = 'ndg_UserLogin'; 	# 유저 로그인 테이블
   	const UserPoint = 'ndg_UserPoint';  # 유저 포인트 테이블 
	const Scrap = 'ndg_Scrap'; # 유저 스크랩 테이블
	const Board = 'ndg_Board'; # 유저 스크랩 테이블
	const PushKey = 'ndg_Push_Key'; # 유저 스크랩 테이블


   	# 생성자
  	function __construct(){
    	parent::__construct();
    	$this->load->database();
    	$this->load->library('Secret');	# 비밀번호 암호화 복호화 
  	}


  	/** 
     * SELETE USER TABLE 
     * @param[$where] : where 절
     * @param[$select] : select 절 
    **/ 
    function getUser($where, $select = '*')
    {   
      	$query = $this->db->from(self::User)
                    ->select($select)
                    ->where($where)
                    ->where('emDelFlag', 'N')
                    ->get();

      	return $query->row_array(); # 1명조회 
    }


  	/** 
     * INSERT USER TABLE 
     * @param[$set] : insert data 
    **/
  	function addUser($set)
  	{
  		$this->db->set($set)->insert(self::User);
  		
  		$idx = $this->db->insert_id();

  		$query = $this->db->from(self::User)
  					->select('nSeqNo, vPhone, vName, vNick, vDevice, nLevel')
  					->where('nSeqNo', $idx)
  					->get();

  		return $query->row_array();
  	}


  	/** 
     * UPDATE USER TABLE  
     * @param[$set] : set data
     * @param[$where] : where 절 
    **/
  	function setUser($set, $where)
    {
  		return $this->db->set($set)->where($where)->update(self::User);
  	}
 	  

    /** 
    * UPDATE ndg_Board  
    * @param[$userNo] : 유저고유번호
    * @param[$point] : 포인트
    **/
    public function setUserPoint($userNo, $point)
    {
        return $this->db->set("nPoint", "nPoint + ".$point, false)->where("nSeqNo", $userNo)->update(self::User);
    }


  	/** 
     * 유저 로그인 로그 저장 
     * @param[$set] : insert data 
    **/ 
  	function addUserLogin($set)
    {
		  return $this->db->set($set)->insert(self::UserLogin);
  	}


    function getUserPoint($userNo)
    {
      $query = $this->db->from(self::UserPoint)
                    ->select('vDate, vDay')
                    ->where('nUserNo', $userNo)
                    ->order_by('nSeqNo', 'DESC')
                    ->get();

      return $query->row_array(); # 1명조회
    }

    function getUserPointCnt($userNo)
    {
      return $this->db->from(self::UserPoint)->where('nUserNo', $userNo)->where('vDate', date('Y-m'))->count_all_results();
    }

    /** 
     * INSERT PaymentInfo TABLE 
     * @param[$set] : insert data 
    **/ 
    function addUserPoint($set)
    {
      return $this->db->set($set)->insert(self::UserPoint);
    }


	public function getUserScrap(){


		$userNo = $this->session->userdata('UNO');

		$query = $this->db->from(self::Scrap.' as S')
				->select('B.*')
				->join(self::Board.' as B', 'on S.nBoardNo = B.nSeqNo')
				->where('S.nUserNo', $userNo)
				->order_by('nSeqNo', 'DESC')
				->get();
		
		return $query->result_array(); # 1명조회

	}

	public function setDeviceKey($set){
		$cnt = $this->db->from(self::PushKey)
				->where('vPushKey', $set['vPushKey'])
				->get()->num_rows();
		if($cnt == 0 && !empty($set['vPushKey'])){
			$this->db->set($set)->insert(self::PushKey);
		} else {
			$row = $this->db->from(self::PushKey)
				->where('vPushKey', $set['vPushKey'])
				->get()->row_array();
			if(empty($row['nUserNo']) && !empty($set['nUserNo'])){
				$this->db->set(['nUserNo'=>$set['nUserNo']])->where('vPushKey', $set['vPushKey'])->update(self::PushKey);
			}
		}
		return true;
	}


}