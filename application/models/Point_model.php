<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 @breif: 포인트 Ver2.0 사용 모델
 @author: csw
*/
class Point_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}


	// 특정 회원의 전체 포인트 정보 출력
	// @param: nUserNo
	public function getUserPoint($param){
		$aRtn = $this->db->select("SUM(nFreePoint + nPayPoint) as nPoint, nFreePoint, nPayPoint")
			->from(TABLE_USER_POINT)
			->where("nUserNo", $param['nUserNo'])
			->get()->row_array();
		return $aRtn;
	}


	// 특정 회원의 전체 포인트 내역 출력
	// @param: nUserNo
	public function getUserPointLog($param){
		$aRtn = $this->db->select("*")
			->from(TABLE_USER_POINT_LOG)
			->where("nUserNo", $param['nUserNo'])
			->order_by('dtRegDate', 'desc')
			->get()->result_array();
		return $aRtn;
	}


	// @breif: 특정 회원 포인트 수정
	// @param: nUserNo, nFreePoint, nPayPoint, nPointKind, nPointType, nType, nBuyNo, nPoint, vDescription
	public function setUserPoint($param){

		$aRtn = [
			'status' => 'FAIL'
			, 'msg' => null
		];

		$nUserNo = !empty($param['nUserNo']) ? $param['nUserNo'] : null;
		$nFreePoint = !empty($param['nFreePoint']) ? $param['nFreePoint'] : 0;
		$nPayPoint = !empty($param['nPayPoint']) ? $param['nPayPoint'] : 0;
		$nPointKind = !empty($param['nPointKind']) ? $param['nPointKind'] : null;
		$nPointType = !empty($param['nPointType']) ? $param['nPointType'] : null;
		$nType = !empty($param['nType']) ? $param['nType'] : null;
		$nBuyNo = !empty($param['nBuyNo']) ? $param['nBuyNo'] : null;
		$vDescription = !empty($param['vDescription']) ? $param['vDescription'] : null;
		$nPoint = !empty($param['nPoint']) ? $param['nPoint'] : 0;


		try{
			if($nUserNo == null) throw new Exception('필수값 누락 userNo', 1);
//			if($nFreePoint == null) throw new Exception('필수값 누락 nFreePoint', 1);
//			if($nPayPoint == null) throw new Exception('필수값 누락 nPayPoint', 1);
			if($nPointKind == null) throw new Exception('필수값 누락 nPointKind', 1);
			if($nPointType == null) throw new Exception('필수값 누락 nPointType', 1);
//			if($nType == null) throw new Exception('필수값 누락 nType', 1);
//			if($nBuyNo == null) throw new Exception('필수값 누락 nBuyNo', 1);
			if($vDescription == null) throw new Exception('필수값 누락 vDescription', 1);
//			if($nPoint == null) throw new Exception('필수값 누락 nPoint', 1);

			$this->db->trans_start();
			// 포인트 데이터 수정

			$updateSet = [];

//			if($nFreePoint != null){
				$updateSet['nFreePoint'] = $nFreePoint;
//			}


//			if($nPayPoint != null ){
				$updateSet['nPayPoint'] = $nPayPoint;
//			}
		//	print_r($updateSet);

			$this->db->set($updateSet);
			$this->db->where('nUserNo', $nUserNo);
			$this->db->update(TABLE_USER_POINT);

			// 포인트 내역 추가
			$insertSet = [
				'nUserNo' => $nUserNo
				, 'nPointKind' => $nPointKind
				, 'nPointType' => $nPointType
				, 'nType' => $nType
				, 'nBuyNo' => $nBuyNo
				, 'nPoint' => $nPoint
				, 'vDescription' => $vDescription
			];
			$this->db->insert(TABLE_USER_POINT_LOG, $insertSet);
			$this->db->trans_complete();


			throw new Exception('성공', 2);
		}
		catch(Exception $e){
			if($e->getCode() == 1){
				$aRtn['status'] = 'FAIL';
			}
			else if($e->getCode() == 2){
				$aRtn['status'] = 'SUCCESS';
			}
			$aRtn['msg'] = $e->getMessage();
		}

		return $aRtn;
	}


	// 포인트 사용 차감 
	// @param: nUserNo, changePoint, changeType(add/dud), $nType,
	// changePoint-> 변동금액 -면 차감 
	public function setChangePoint($param){

		$aRtn = [
			'status' => 'FAIL'
			, 'msg' => null
		];

		$nUserNo = !empty($param['nUserNo']) ? $param['nUserNo'] : null;
		$nType = !empty($param['nType']) ? $param['nType'] : null;
		$nBuyNo = !empty($param['nBuyNo']) ? $param['nBuyNo'] : null;
		$vDescription = $param['msg'];
		$nChangePoint = $param['changePoint'];

		// 아래의 금액은 계산용
		$changeType = $param['changeType'];
		$changePoint = $param['changePoint'];

		$data = $this->getUserPoint($param);

		$nPoint = $data['nPoint'];	// 현재 보유 포인트의 합
		$ori_nFreePoint = $data['nFreePoint']; // 현재 보유중인 무료 포인트
		$ori_nPayPoint = $data['nPayPoint']; // 현재 보유중인 유료 포인트
		$data = null;

		if($changeType == "add"){
			$cnt = $this->db->from(TABLE_USER_POINT)
				->where('nUserNo', $nUserNo)
				->get()->num_rows();
			if($cnt == 0){
				$insertSet = [
					'nUserNo' => $nUserNo
					, 'nFreePoint' => 0
					, 'nPayPoint' => 0
				];
				$this->db->insert(TABLE_USER_POINT, $insertSet);
			}

			$nPointKind = 1;
			$nFreePoint = !empty($param['nFreePoint'])? $param['nFreePoint'] : 0;
			$nPayPoint = !empty($param['nPayPoint'])? $param['nPayPoint'] : 0;


			// 유료 포인트 적립
			if($nPayPoint > 0){
				$setParam = [
					'nUserNo' => $nUserNo
					, 'nFreePoint' => $ori_nFreePoint + $nFreePoint
					, 'nPayPoint' => $ori_nPayPoint + $nPayPoint
					, 'nPointKind' => $nPointKind
					, 'nPointType' => 1
					, 'nType' => $nType
					, 'nBuyNo' => $nBuyNo
					, 'nPoint' => $nPayPoint
					, 'vDescription' => $vDescription
				];
				$data = $this->setUserPoint($setParam);
			}

			if($nFreePoint > 0){
				$setParam = [
					'nUserNo' => $nUserNo
					, 'nFreePoint' => $ori_nFreePoint + $nFreePoint
					, 'nPayPoint' => $ori_nPayPoint + $nPayPoint
					, 'nPointKind' => $nPointKind
					, 'nPointType' => 2
					, 'nType' => $nType
					, 'nBuyNo' => $nBuyNo
					, 'nPoint' => $nFreePoint
					, 'vDescription' => $vDescription
				];
				$data = $this->setUserPoint($setParam);
			}

		}
		else if($changeType == "dud"){
			$nPointKind = 2;	// 1:적립, 2:사용
			$nFreePoint = $ori_nFreePoint;
			$nPointType = 1;	// 유료만 사용
			// 유료 포인트 소진 체크
			if($ori_nPayPoint > 0 ){
				if($ori_nPayPoint >= $changePoint){
					$nPayPoint = $ori_nPayPoint - $changePoint;	// 유료결제 포인트가 차감금액에 충분하다면 유료포인트 = 유료포인트 - 차감금액
					$changePoint = 0;
				}
				else if($ori_nPayPoint > 0){
					$changePoint = $changePoint - $ori_nPayPoint;	// 유료결제 포인트가 차감금액에 부족하다면 changePoint를 유료포인트 만큼 차감
					$nPayPoint = 0;
					$nChangePoint = $ori_nPayPoint;
				}

				$setParam = [
					'nUserNo' => $nUserNo
					, 'nFreePoint' => $nFreePoint
					, 'nPayPoint' => $nPayPoint
					, 'nPointKind' => $nPointKind
					, 'nPointType' => $nPointType
					, 'nType' => $nType
					, 'nBuyNo' => $nBuyNo
					, 'nPoint' => $nChangePoint * -1
					, 'vDescription' => $vDescription
				];
				$data = $this->setUserPoint($setParam);
				$setParam = null;
			}


			// 무료 포인트 소진 체크
			if($changePoint > 0){
				$nFreePoint = $ori_nFreePoint - $changePoint;	// 
				$nPointType = 2;

				$setParam = [
					'nUserNo' => $nUserNo
					, 'nFreePoint' => $nFreePoint
				//	, 'nPayPoint' => $nPayPoint
					, 'nPointKind' => $nPointKind
					, 'nPointType' => $nPointType
					, 'nType' => $nType
					, 'nBuyNo' => $nBuyNo
					, 'nPoint' => $changePoint * -1
					, 'vDescription' => $vDescription
				];

				//print_r($setParam);
				$data = $this->setUserPoint($setParam);
			}
		}

		return true;
	}


	public function getCheckFirstPay($goodsKind, $nUserNo){

		$this->db->select('*');
		$this->db->from(TABLE_PAYMENT);

		if($goodsKind == 2){
			$this->db->where('nGoodsNo IN (6,8)',NULL,FALSE);
		}
		else if($goodsKind == 3){
			$this->db->where('nGoodsNo IN (7,9)',NULL,FALSE);
		}
		else{
			$this->db->where('nGoodsNo IN (1,2,3,4,5)',NULL,FALSE);
		}

		$this->db->where('nUserNo', $nUserNo);
		$this->db->where("vCardNo <> '' AND vExpdt <> '' AND nOrderStatus=0 ",NULL,FALSE);
		$cnt = $this->db->get()->num_rows();


		return $cnt;

	}

}
