<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin
  * User Model
  * @author 임정원 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터베이스 처리후 컨트롤러로 리턴
  *   # index # userList # history
  *   # getUserList # getUserCount
  *
*/


class UserModel extends CI_Model
{

	function __construct(){
		parent::__construct();

		$this->load->library('Secret');

		$this->load->database();
	}

  /**
  * User 회원 테이블 DB 가져오기
  * @author 임정원
  * @param[pageNo] 페이지 번호
  * @param[total] 총 ROW 갯수
  * @param[limit] 한페이지에 보여질 갯수 */
	public function getUserList($pageNo, $total, $limit) {

		# search
		$keyfield = $this->input->post('keyfield') ? $this->input->post('keyfield') : '';
		$keyword    = $this->input->post('keyword') ? $this->input->post('keyword'):'';
		if($keyfield == "vPhone" && !empty($keyword)){
			$keyword = $this->secret->secretEncode($keyword);
		}

		//	$keyword    = $this->secret->secretEncode($keyword);
		if($limit){
			$limit = $limit;
		}else{
			$limit = $this->input->post('limit')?$this->input->post('limit'):'10';
		}
		$mgroup = $this->input->post('mgroup')?$this->input->post('mgroup'):'';
		$nlevel = $this->input->post('nlevel')?$this->input->post('nlevel'):'';
		$reception = $this->input->post('reception')?$this->input->post('reception'):'';
		$reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';
		$reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';
		$auth = $this->input->post('auth')?$this->input->post('auth'):'';

		# 검색 처리
		if($keyword && $keyfield){
			$this->db->like($keyfield, $keyword, 'both');
		}

		if($reception) $this->db->where("A.vPushOs", $reception);
		if($mgroup=='1') $this->db->where("IFNULL((SELECT nUserNo FROM ndg_Payment WHERE nUserNo=A.nSeqNo AND nType=1 AND nOrderStatus=0 AND CONVERT(vStartDate,DATE) <= CURDATE() AND CONVERT(vEndDate,DATE) >= CURDATE() LIMIT 1),0) > 0"); else if($mgroup=='2') $this->db->where("IFNULL((SELECT nUserNo FROM ndg_Payment WHERE nUserNo=A.nSeqNo AND nType=1 AND  nOrderStatus=0 AND CONVERT(vStartDate,DATE) <= CURDATE() AND CONVERT(vEndDate,DATE) >= CURDATE() LIMIT 1),0)=0");
		if($nlevel) $this->db->where("A.nLevel",$nlevel);
		if($reg1) $this->db->where("A.dtRegDate >= ".$this->db->escape($reg1." 00:00:00"),NULL,FALSE);
		if($reg2) $this->db->where("A.dtRegDate <= ".$this->db->escape($reg2." 23:59:59"),NULL,FALSE);
		if($auth){

			if($auth == "app"){
				$this->db->where_not_in('A.vDevice', ['-', '']);
				$this->db->where('A.vPushKey is not null');
				$this->db->where("A.vPushKey != ''");
			}else if($auth == "web"){
				$this->db->where_in('A.vDevice', ['-', '']);
			}
		}


		# 페이징처리
		$start_record = ($pageNo - 1) * $limit;

		$this->db->select('A.*, IFNULL((SELECT nUserNo FROM ndg_Payment WHERE nUserNo=A.nSeqNo AND nType=1 AND nOrderStatus=0 AND CONVERT(vStartDate,DATE) <= CURDATE() AND CONVERT(vEndDate,DATE) >= CURDATE() LIMIT 1),0) AS mgroup, P.nFreePoint, P.nPayPoint');
		$this->db->from(TABLE_USER.' AS A');
		$this->db->join(TABLE_USER_POINT.' AS P', 'A.nSeqNo = P.nUserNo', 'inner join');
		$this->db->where('A.emDelFlag', 'N');
		//    $this->db->where('A.nLevel', '1');
		$this->db->limit($limit, $start_record);
		$this->db->order_by('A.nSeqNo', 'DESC');
		return $this->db->get()->result_array();
	}

  /**
  * User 회원 테이블 카운팅
  * @author 임정원 / 2019-11-12 */
  public function getUserCount()
  {
    # search
	$keyfield = $this->input->post('keyfield') ? $this->input->post('keyfield') : '';
    $keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
	if($keyfield == "vPhone" && !empty($keyword)){
		$keyword = $this->secret->secretEncode($keyword);
	}
    $limit = $this->input->post('limit')?$this->input->post('limit'):'10';
    $mgroup = $this->input->post('mgroup')?$this->input->post('mgroup'):'';
    $nlevel = $this->input->post('nlevel')?$this->input->post('nlevel'):'';
	$reception = $this->input->post('reception')?$this->input->post('reception'):'';
    $reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';
    $reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';

    # 검색 처리
	if($keyword && $keyfield){
		$this->db->like($keyfield, $keyword, 'both');
	}
    if($reception) $this->db->where("A.vPushOs", $reception);
    if($mgroup=='1') $this->db->where("IFNULL((SELECT nUserNo FROM ndg_Payment WHERE nUserNo=A.nSeqNo AND nType=1 AND nOrderStatus=0 AND CONVERT(vStartDate,DATE) <= CURDATE() AND CONVERT(vEndDate,DATE) >= CURDATE() LIMIT 1),0) > 0"); else if($mgroup=='2') $this->db->where("IFNULL((SELECT nUserNo FROM ndg_Payment WHERE nUserNo=A.nSeqNo AND nType=1 AND emState=1 LIMIT 1),0)=0");
	if($nlevel) $this->db->where("A.nLevel",$nlevel);
	if($reg1) $this->db->where("A.dtRegDate >= ".$this->db->escape($reg1." 00:00:00"),NULL,FALSE);
    if($reg2) $this->db->where("A.dtRegDate <= ".$this->db->escape($reg2." 00:00:00"),NULL,FALSE);

    $this->db->select( 'A.nSeqNo' );
    $this->db->from('ndg_User AS A');
    $this->db->where('A.emDelFlag', 'N');
//    $this->db->where('A.nLevel', '1');

    return $this->db->get()->num_rows();
  }

  public function setUserJoin()
  {
    $setData = array(
      'vUserId' => $this->input->post('userid'),
      'vUserPwd' => md5($this->input->post('passwd')),
      'vName' => $this->input->post('username'),
      'vHp' => $this->input->post('phone'),
      'vEmail' => $this->input->post('email'),
      'nLevel' =>  $this->input->post('level'),
    );

    if($this->db->insert('User', $setData)){
      $this->util->alert('정상적으로 등록되었습니다.', '/admin/user');
    }else{
      $this->util->alert('등록중 오류가 발생하였습니다. 다시확인해주세요.', '/admin/user');
    }


  }

  public function getUserLogin($pageNo, $total, $limit)
  {
    # search
    $keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
    $searchType = $this->input->post('searchType')?$this->input->post('searchType'):'';

    # 검색 처리
    if($searchType)
      $this->db->like($searchType, $keyword, 'both');

    # 페이징처리
    $start_record = ($pageNo - 1) * $limit;

    $this->db->select('UserLogin.*, User.vPhone');
    $this->db->from('ndj_UserLogin');
    $this->db->join('ndj_User', 'UserLogin.nUserNo = User.nSeqNo');
    $this->db->limit($limit, $start_record);
    $this->db->order_by('UserLogin.nSeqNo', 'DESC');

    return $this->db->get()->result_array();

  }

  public function getUserLoginCount()
  {
    # search
    $keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
    $searchType = $this->input->post('searchType')?$this->input->post('searchType'):'';

    $this->db->select('UserLogin.*, User.vUserId');
    $this->db->from('UserLogin');
    $this->db->join('User', 'UserLogin.nUserNo = User.nSeqNo');

    if($searchType) $this->db->like($searchType, $keyword, 'after');

    return $this->db->get()->num_rows();
  }

  public function getUserBuyList($no)
  {
    $this->db->select('*');
    $this->db->from('ndg_Payment');
	$this->db->where('nUserNo', $no);
    $this->db->order_by('dtRegDate', 'DESC');
    return $this->db->get()->result_array();
  }

  public function delUser($delno)
  {
//    $this->db->delete('ndg_User','nSeqNo IN ('.$delno.')');
	$data=array('emDelFlag'=>'Y');
	$this->db->where_in('nSeqNo', $delno);
	$this->db->update('ndg_User',$data);
  }

  public function updateUser($no,$levelval)
  {
	$data=array('nLevel'=>$levelval);
	$this->db->where('nSeqNo', $no);
    $this->db->update('ndg_User',$data);
  }



	function addUser($set) {
		$this->db->set($set)->insert('ndg_User');
		return true;
	}


	function getAdList(){

		$this->db->select('*');
		$this->db->from('ndg_User');
		$this->db->where('emDelFlag', 'N');
		$this->db->where('nLevel', '3');
		return $this->db->get()->result_array();

	}

}
