<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CampainModel extends CI_Model
{

	const CAMPAIN_TABLE = 'ndg_Campain';
	const USER_TABLE = 'ndg_User';
	const VISIT_TABLE = 'ndg_Visit';

	function __construct(){
		parent::__construct();
		$this->load->library('Secret');
		$this->load->database();
	}


	public function postCampain($param){

		$aRtn = null;

		$this->db->from(self::CAMPAIN_TABLE);
		$this->db->where('vReferer', $param['vReferer']);
		$cnt = $this->db->get()->num_rows();

		if($cnt > 0){
			$aRtn['status'] = 'FAIL';
			$aRtn['message'] = 'REFERER KEY를 이미 사용중 입니다.';
		}
		else{
			$this->db->set($param)->insert(self::CAMPAIN_TABLE);
			$aRtn['status'] = 'SUCCESS';
			$aRtn['message'] = '';
		}
		return $aRtn;
	}


	public function getCampainCount(){

		
		$keyfield = $this->input->post('keyfield') ? $this->input->post('keyfield') : '';
		$keyword = $this->input->post('keyword') ? $this->input->post('keyword'):'';

		// 검색
		if($keyword && $keyfield){
			$this->db->like($keyfield, $keyword, 'both');
		}

		$this->db->from(self::CAMPAIN_TABLE.' as C');
		$this->db->join(self::USER_TABLE.' as U', 'C.nUserNo = U.nSeqNo');
		$this->db->where("C.emDelFlag", "N");
		if($this->session->userdata("ULV") != 10){
			$this->db->where('C.nUserNo', $this->session->userdata("UNO"));
		}

		return $this->db->get()->num_rows();

	}


	public function getCampainList($pageNo, $total){

		$keyfield = $this->input->post('keyfield') ? $this->input->post('keyfield') : '';
		$keyword = $this->input->post('keyword') ? $this->input->post('keyword'):'';
		$limit = $this->input->post('limit')?$this->input->post('limit'):'10';

		// 검색
		if($keyword && $keyfield){
			$this->db->like($keyfield, $keyword, 'both');
		}
		// 페이징처리
		$start_record = ($pageNo - 1) * $limit;


		$this->db->select('C.nSeqNo,C.vCampain,U.vName, C.vAdDevice, U.vNick, C.vAdDevice, C.dtRegDate, C.vAdUrl');
		$this->db->from(self::CAMPAIN_TABLE.' as C');
		$this->db->join(self::USER_TABLE.' as U', 'C.nUserNo = U.nSeqNo');
		$this->db->where("C.emDelFlag", "N");
		if($this->session->userdata("ULV") != 10){
			$this->db->where('C.nUserNo', $this->session->userdata("UNO"));
		}
		$this->db->limit($limit, $start_record);
		$this->db->order_by('C.nSeqNo', 'DESC');
		return $this->db->get()->result_array();

	}


	public function removeCampain($param){
		$data=array('emDelFlag'=>'Y');
		$this->db->where_in('nSeqNo', $param['delNo']);
		$this->db->update(self::CAMPAIN_TABLE, $data);
	}


	public function getStatsSum(){

/**
SELECT C.*, COUNT(V.nSeqNo) AS click, SUM(V.nInstall) AS `install`, SUM(V.nPay) AS pay, SUM(V.nJoin) AS `join` FROM 
`ndg_Campain` AS C
LEFT JOIN ndg_Visit AS V ON C.vReferer=V.vReferer
WHERE C.emDelFlag ='N'
GROUP BY V.vReferer
*/

		$this->db->select('C.*, COUNT(V.nSeqNo) AS click, SUM(V.nInstall) AS `install`, SUM(V.nPay) AS pay, SUM(V.nJoin) AS `join`');
		$this->db->from(self::CAMPAIN_TABLE.' as C');
		$this->db->join(self::VISIT_TABLE.' as V', 'C.vReferer=V.vReferer', 'left outer');
		$this->db->where("C.emDelFlag", "N");
		if($this->session->userdata("ULV") != 10){
			$this->db->where('C.nUserNo', $this->session->userdata("UNO"));
		}
		$this->db->group_by('C.vReferer');
		return $this->db->get()->result_array();


	}

	public function getLiveCampain(){



		if($this->session->userdata("ULV") != 10){
			$this->db->where('nUserNo', $this->session->userdata("UNO"));
		}
		$this->db->select("*")
			->from(self::CAMPAIN_TABLE);
		return $this->db->get()->result_array();
	}


	public function getVisitCount($aReferer){

		
		if(count($aReferer) == 0){
			return 0;
		}

		$keyfield = $this->input->post('keyfield') ? $this->input->post('keyfield') : '';
		$keyword = $this->input->post('keyword') ? $this->input->post('keyword'):'';

		// 검색
		if($keyword && $keyfield){
			$this->db->like('V.'.$keyfield, $keyword, 'both');
		}
		if($this->session->userdata('ULV') != 10){
			$this->db->where('C.nUserNo', $this->session->userdata('UNO'));
		}

		$this->db->select("*")
			->from(self::VISIT_TABLE." as V")
			->join(self::CAMPAIN_TABLE." as C", "V.vReferer = C.vReferer", "left")
			->where_in('V.vReferer', $aReferer)
			->order_by('V.dtRegDate', 'desc');
		return $this->db->get()->num_rows();
	}

	public function getVisitData($pageNo, $total, $aReferer){

		$keyfield = $this->input->post('keyfield') ? $this->input->post('keyfield') : '';
		$keyword = $this->input->post('keyword') ? $this->input->post('keyword'):'';
		$limit = $this->input->post('limit')?$this->input->post('limit'):'10';


		$nJoin = $this->input->post('nJoin');
		$nPay = $this->input->post('nPay');
		$nInstall = $this->input->post('nInstall');
		$campain = $this->input->post('campain') ? $this->input->post('campain') : null;
//		$nJoin = $this->input->post('nJoin') ? $this->input->post('nJoin') : null;


		// 검색
		if($keyword && $keyfield){
			$this->db->like('V.'.$keyfield, $keyword, 'both');
		}
		if($this->session->userdata('ULV') != 10){
			$this->db->where('C.nUserNo', $this->session->userdata('UNO'));
		}

		if($nJoin != null){
			$this->db->where('V.nJoin', $nJoin);
		}
		if($nPay != null){
			$this->db->where('V.nPay', $nPay);
		}
		if($nInstall != null){
			$this->db->where('V.nInstall', $nInstall);
		}
		if($campain != null){
			$this->db->where('C.nSeqNo', $campain);
		}

		// 페이징처리
		$start_record = ($pageNo - 1) * $limit;

		$this->db->select("V.*, U.vName, U.vNick, U.dtRegDate as dtJoinDate")
			->from(self::VISIT_TABLE." as V")
			->join(self::CAMPAIN_TABLE." as C", "V.vReferer = C.vReferer", "left")
			->join(self::USER_TABLE." as U", "V.nUserNo = U.nSeqNo", "left")
			->where_in('V.vReferer', $aReferer)
			->order_by('V.dtRegDate', 'desc');
		$this->db->limit($limit, $start_record);
		return $this->db->get()->result_array();
	}
}
