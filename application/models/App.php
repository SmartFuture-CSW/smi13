<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
    * Board Table 관련 모델 
    * @author 임정원 / 2020-03-30
    * @since  Version 1.0.0
     
    * CRUD 규칙 
    * DB INSERT -> add 테이블명 함수명 
    * DB UPDATE -> set 테이블명 함수명
    * DB DELETE -> del 테이블명 함수명
    * DB SELECT -> get 테이블명 함수명
*/

class App_model extends CI_Model
{

	# DB테이블명 정의
	const User  = 'ndg_User';           # 유저 테이블
	const Device  = 'ndg_Device';           # 디바이스키 테이블

	# 생성자
	function __construct(){
		parent::__construct();
		$this->load->database();
//		$this->load->library('Secret');	# 비밀번호 암호화 복호화 
	}


	public function postDeviceInfo(){


		$param = array(
			"deviceKey" => $_POST['deviceKey'],
			"platform" => $_POST['deviceType']
		);

		$this->db->set($param)->insert('test');

	}



}