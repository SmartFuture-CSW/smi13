<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin
  * Board Model
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터베이스 처리후 컨트롤러로 리턴
  *   # index # BoardList # history
  *   # getBoardList # getBoardCount
  *
*/


class BoardModel extends CI_Model
{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	/**
	* Board 회원 테이블 DB 가져오기
	* @author 채원만
	* @param[pageNo] 페이지 번호
	* @param[total] 총 ROW 갯수
	* @param[limit] 한페이지에 보여질 갯수
	*/
	public function getBbsList($pageNo, $total, $limit){
		# search
		$keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
		$searchhash    = $this->input->post('searchhash')?$this->input->post('searchhash'):'';
		if(!$limit){
			$limit = $this->input->post('limit')?$this->input->post('limit'):'10';
		}

		$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
		if($bo_table=='') $bo_table='today';

		$mainval = $this->input->post('mainval') ? $this->input->post('mainval'):'';
		$orderby = $this->input->post('orderby')?$this->input->post('orderby'):'nSeqNo';
		$reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';
		$reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';

		# 검색 처리
		if($bo_table=='reply'){
			if($keyword) $this->db->like("CONCAT(A.vNick,'|',A.txComment)", $keyword, 'both');
		}
		else{
			if($keyword) $this->db->like("CONCAT(A.vSubject,'|',A.txContent)", $keyword, 'both');
		}

		if($searchhash) $this->db->like("A.txTag", $searchhash, 'both');
		if($reg1) $this->db->where("A.dtRegDate >= ".$this->db->escape($reg1." 00:00:00"),NULL,FALSE);
		if($reg2) $this->db->where("A.dtRegDate <= ".$this->db->escape($reg2." 00:00:00"),NULL,FALSE);

		# 페이징처리
		$start_record = ($pageNo - 1) * $limit;

		$this->db->select('A.*, B.vName');
		if($bo_table=='reply'){
			$this->db->from('ndg_BoardReply AS A'); 
		}
		else { 
			$this->db->from('ndg_Board AS A');
		}

		$this->db->join('ndg_User AS B', 'A.nUserNo=B.nSeqno', 'LEFT');
		if($bo_table!='reply'){
			if($bo_table != "today"){
				$this->db->where('A.vType', $bo_table);
			}
			$this->db->where('A.emDelFlag', 'N');
		}

		$this->db->limit($limit, $start_record);
		$this->db->order_by('A.'.$orderby.'', 'DESC');

		return $this->db->get()->result_array();
	}

  /**
  * Board 회원 테이블 카운팅
  * @author 채원만 / 2019-11-12 */
  public function getBbsCount()
  {
    # search
    $keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
    $limit = $this->input->post('limit')?$this->input->post('limit'):'10';
	$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
	$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
	if($bo_table=='') $bo_table='today';
    $reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';
    $reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';
	$mainval = $this->input->post('mainval') ? $this->input->post('mainval'):'';

    # 검색 처리
	if($bo_table=='reply'){
		if($keyword) $this->db->like("CONCAT(A.vNick,'|',A.txComment)", $keyword, 'both');
	}else{
		if($keyword) $this->db->like("CONCAT(A.vSubject,'|',A.txContent)", $keyword, 'both');
	}
	if($reg1) $this->db->where("A.dtRegDate >= ".$this->db->escape($reg1." 00:00:00"),NULL,FALSE);
    if($reg2) $this->db->where("A.dtRegDate <= ".$this->db->escape($reg2." 00:00:00"),NULL,FALSE);

    $this->db->select( 'A.nSeqNo' );
	if($mainval=='2'){
		$this->db->from('ndg_Stock AS A');
	}else{
		if($bo_table=='reply') $this->db->from('ndg_BoardReply AS A'); else $this->db->from('ndg_Board AS A');
	}
	$this->db->join('ndg_User AS B', 'A.nUserNo=B.nSeqno', 'LEFT');
	if($bo_table!='reply'){
	    $this->db->where('A.vType', $bo_table);
		$this->db->where('A.emDelFlag', 'N');
	}
    return $this->db->get()->num_rows();
  }

  public function getBbsAllCount()
  {
    $this->db->select( 'A.nSeqNo' );
	$this->db->from('ndg_Board AS A');
	$this->db->join('ndg_User AS B', 'A.nUserNo=B.nSeqno', 'LEFT');
	$this->db->where('A.emDelFlag', 'N');
    return $this->db->get()->num_rows();
  }

  public function getReplyCount()
  {
    $this->db->select( 'A.nSeqNo' );
	$this->db->from('ndg_BoardReply AS A');
	$this->db->join('ndg_User AS B', 'A.nUserNo=B.nSeqno', 'LEFT');
    return $this->db->get()->num_rows();
  }

  public function getBbswrite($bd_no)
  {
	  if($bd_no!=''){
		$this->db->select('A.*');
		$this->db->from('ndg_Board AS A');
		$this->db->where('A.nSeqNo', $bd_no);
		return $this->db->get()->result_array();
	  }else{
		  return;
	  }
  }

  public function delBbsData()
  {
    $bd_no = $this->input->post('bd_no')?$this->input->post('bd_no'):'';
	$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
	$bo_table = $this->input->post('bo_table')?$this->input->post('bo_table'):'';	
	if($bo_table==''){
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
	}
	if($bo_table=='') $bo_table='today';
	if($bd_no!=''){
		if($bo_table!='reply'){
			$upDir  = $_SERVER['DOCUMENT_ROOT'] . '/data/board/thumb/';
			$this->db->select('A.*');
			$this->db->from('ndg_Board AS A');
			$this->db->where('A.nSeqNo', $bd_no);
			$row=$this->db->get()->result_array();
			@unlink($upDir.$row[0]['vImage']);
			@unlink($upDir.$row[0]['vImage2']);
			$this->db->delete('ndg_Board',array('nSeqNo'=>$bd_no));
		}else{
			$this->db->delete('ndg_BoardReply',array('nSeqNo'=>$bd_no));
		}
	}
  }

  public function setBbswrite()
  {
	$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
	$bd_no = $this->input->post('bd_no')?$this->input->post('bd_no'):'';
	$bo_table = $this->input->post('bo_table')?$this->input->post('bo_table'):'';	
	if($bo_table=='') $bo_table='today';
	$upDir  = $_SERVER['DOCUMENT_ROOT'] . '/data/board/thumb/';
	$fileName1=$this->input->post('vImage_ori')?$this->input->post('vImage_ori'):'';	
	$fileName2=$this->input->post('vImage2_ori')?$this->input->post('vImage2_ori'):'';	
	$fileName1_del=$this->input->post('vImageDel')?$this->input->post('vImageDel'):'';	
	$fileName2_del=$this->input->post('vImage2Del')?$this->input->post('vImage2Del'):'';	
	if($fileName1_del!=''){
		@unlink($upDir.$fileName1_del);
		$fileName1='';
	}
	if($fileName2_del!=''){
		@unlink($upDir.$fileName2_del);
		$fileName2='';
	}
	$vRecom1=$this->input->post('vRecom1')?$this->input->post('vRecom1'):'';
	$vRecom2=$this->input->post('vRecom2')?$this->input->post('vRecom2'):'';
	$vRecom3=$this->input->post('vRecom3')?$this->input->post('vRecom3'):'';
	$vRecom4=$this->input->post('vRecom4')?$this->input->post('vRecom4'):'';
	$vSearchHash=$this->input->post('vSearchHash')?$this->input->post('vSearchHash'):'';
	$vBasicEvent=$this->input->post('vBasicEvent')?$this->input->post('vBasicEvent'):'';
	$vEventLink=$this->input->post('vEventLink')?$this->input->post('vEventLink'):'';
	$vNewsLink=$this->input->post('vNewsLink')?$this->input->post('vNewsLink'):'';

	$nCategoryNo = $this->input->post('nCategoryNo') ? $this->input->post('nCategoryNo') : 0;


	if(isset($_FILES['vImage'])){
		if ($_FILES['vImage']['name']) {
			$name = $_FILES['vImage']['name'];
			$size = $_FILES['vImage']['size'];
			$tmp  = $_FILES['vImage']['tmp_name'];
			$fileName1 = date('YmdHis').rand(10,99).'_'.$name;

			// 사이즈 체크
			if ($size > 5242880) {
				$this->util->alert('파일용량은 5MB를 초과하지 않아야 합니다.', '');
				exit;
			}
			// 업로드성공
			if (! move_uploaded_file($tmp, $upDir . basename($fileName1))) {
				$this->util->alert('파일1 업로드 실패!', '');
				exit;
			} else {
				// tmp 파일 삭제
				@unlink($_FILES['vImage']['tmp_name']);
			}
		}
	}
	if(isset($_FILES['vImage2'])){
		if ($_FILES['vImage2']['name']) {
			$name = $_FILES['vImage2']['name'];
			$size = $_FILES['vImage2']['size'];
			$tmp  = $_FILES['vImage2']['tmp_name'];
			$fileName2 = date('YmdHis').rand(10,99).'_'.$name;

			// 사이즈 체크
			if ($size > 5242880) {
				$this->util->alert('파일용량은 5MB를 초과하지 않아야 합니다.', '');
				exit;
			}
			// 업로드성공
			if (! move_uploaded_file($tmp, $upDir . basename($fileName2))) {
				$this->util->alert('파일2 업로드 실패!', '');
				exit;
			} else {
				// tmp 파일 삭제
				@unlink($_FILES['vImage2']['tmp_name']);
			}
		}
	}

	$emPush = ($this->input->post('emPush')?'1':'0');
	/*201008 ksg*/
	$before=array('http://faststockinfo.co.kr/data/editor/2004/8379f976295c5c028d57d03b9dd46cba_1587090469_1665.jpg','http://faststockinfo.co.kr/data/editor/2004/8379f976295c5c028d57d03b9dd46cba_1587090500_923.jpg','http://faststockinfo.co.kr/data/editor/2004/8379f976295c5c028d57d03b9dd46cba_1587090606_5673.jpg');
	$after=array('/asset/img/kakao.jpg','/asset/img/telegram.jpg','/asset/img/superant.jpg');
	$txContent=str_replace($before,$after,$this->input->post('txContent'));
	/*201008 ksg*/

	$insertData = [
		'nUserNo'    => $this->session->userdata('UNO'),
		'vType'      => $this->input->post('bo_table'),
		'vSubject' => $this->input->post('vSubject'),
		'vImage' => $fileName1,
		'txContent' => $txContent,
		'txTag' => $this->input->post('txTag'),
		'vIP' => $_SERVER['REMOTE_ADDR'],
		'emPush' => $emPush,
		'vName' => $this->input->post('vName'),
		'vImage2' => $fileName2,
		'vRecom1' => $vRecom1,
		'vRecom2' => $vRecom2,
		'vRecom3' => $vRecom3,
		'vRecom4' => $vRecom4,
		'vSearchHash' => $vSearchHash,
		'vBasicEvent' => $vBasicEvent,
		'vEventLink' => $vEventLink,
		'vNewsLink' => $vNewsLink,
		'nCategoryNo' => $nCategoryNo
	];
	if($bd_no!=''){
		$where = ['nSeqNo =' => $bd_no];
		$result=$this->db->set($insertData)->where($where)->update("ndg_Board");
		$boardNo = $bd_no;
	}else{
		$result=$this->db->set($insertData)->insert("ndg_Board");
		$boardNo = $this->db->insert_id();
	}

	if (!$result) {
		$this->util->alert('DB 오류!', '');
		return false;
	}

	return $boardNo;
  }

    public function setBbsupdate()
  {
	$bd_no = $this->input->post('bd_no')?$this->input->post('bd_no'):'';
	$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
	$bo_table = $this->input->post('bo_table')?$this->input->post('bo_table'):'';	
	if($bo_table==''){
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
	}
	if($bo_table=='') $bo_table='today';
	$udname=$this->input->post('udname')?$this->input->post('udname'):'';	
	$udval=$this->input->post('udval')?$this->input->post('udval'):'';

	$insertData = [
		$udname => $udval,
	];
	if($bd_no!='' && $udname!='' && $udval!=''){
		$where = ['nSeqNo =' => $bd_no];
		$result=$this->db->set($insertData)->where($where)->update("ndg_Board".($bo_table=='reply'?'Reply':''));
	}
	if (!$result) {
		$this->util->alert('DB 오류!', '');
		return false;
	}
	return true;
  }

	public function getBbsMainList($no) {
		$this->db->select('*');
		$this->db->from('ndg_BoardMain');
		$this->db->where('vMainVal',$no);
		$this->db->order_by('nSort', 'ASC');
		return $this->db->get()->result_array();
	}


	public function getBbsTopBoardList($vType, $mainNo, $categoryNo) {

		$this->db->select('nSeqNo, nBoardNo, nSort, vType');
		$this->db->from('ndg_BoardMain');
		$this->db->where_in('vMainVal',$mainNo);
		$this->db->where('nCategoryNo',$categoryNo);
		if($vType == "today"){
			$orderbyCase = "CASE WHEN vType = 'today' THEN 1  WHEN vType = 'important' THEN 2 WHEN vType = 'invest' THEN 3 WHEN vType = 'study' THEN 4 END ASC, nSort asc";
			$this->db->order_by($orderbyCase);
		}
		else if($vType == "stock"){
			$orderbyCase = "CASE WHEN vType = 'period' THEN 1  WHEN vType = 'news' THEN 2 WHEN vType = 'theme' THEN 3 END, nSort asc";
			$this->db->order_by($orderbyCase);
		}
		else{
			$this->db->order_by('nSort', 'ASC');
		}
		return $this->db->get()->result_array();
	}


  public function setBbsMainUpdate()
  {
	$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
	$cate = $this->input->post('category')?$this->input->post('category'):$this->input->get('category');



	switch($cate){
		case 'today' : $vMainVal = 1; break;
		case 'important' : $vMainVal = 3; break;
		case 'invest' : $vMainVal = 7; break;
		case 'study' : $vMainVal = 8; break;
	}


	$fval = $this->input->post('fval')?$this->input->post('fval'):$this->input->get('fval');
	$fval2=explode('<nodaji>',$fval);

	for($k=0;$k<count($fval2);$k++){
		//if($k==1) $vMainVal=2;
		if($fval2[$k]=='') continue;

		$fval3=explode('|',$fval2[$k]);



		for($j=0;$j<count($fval3);$j++){

			$fval4=explode(',',$fval3[$j]);

			if($cate == "today" && $k == 0){
				$vMainVal = 1;
			} else if($cate == "today" && count($fval4) == 3) {
				switch($fval4[1]){
					case 'important' : $vMainVal = 2; break;
					case 'invest' : $vMainVal = 9; break;
					case 'study' : $vMainVal = 10; break;
					case 'undefined' : continue;
				}
			}
			if($fval3[$j]=='') continue;

			$udtData=[
				'vType'=>$fval4[1],
				'nBoardNo'=>$fval4[2],
			];

			$where = ['vMainVal =' => $vMainVal,'nSort =' => $fval4[0]];
			$result=$this->db->set($udtData)->where($where)->update("ndg_BoardMain");
		}
	}
	return true;
  }

	public function getBbsMainAutoList($mainval)
	{
		switch($mainval){
			case 1 : 
				$table = "ndg_Board";
				$where = "vType IN ('today')";
			break;

			case 2 :
				$table = "ndg_Board";
				$where = "vType = 'important'";
			break;

			case 3 :
				$table = "ndg_Board";
				$where = "vType = 'important'";
			break;

			case 7 :
				$table = "ndg_Board";
				$where = "vType = 'invest'";
			break;

			case 8 :
				$table = "ndg_Board";
				$where = "vType = 'study'";
			break;
		}
		
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$this->db->order_by('nHit', 'DESC');
	/*
		if($mainval==2){
			$this->db->from('ndg_Stock');
			$this->db->where("vType IN ('theme','period','rumor')");
		}else{
			$this->db->from('ndg_Board');
			$this->db->where("vType IN ('important','news')");
		}
		if($mainval==3){
			$this->db->order_by('nHit', 'DESC');
		}else if($mainval==7){
			$this->db->where(" vType = 'invest' ");
		}else if($mainval==8){
			$this->db->where(" vType = 'study' ");
		}else{
			$this->db->order_by('nSeqNo', 'DESC');
		}    
	*/
		

		$this->db->limit(5,0);
		return $this->db->get()->result_array();
	}



  public function changeStat($no,$agrno)
  {
	if($agrno=='1') $chgno='0'; else $chgno='1';
	$data=array('emAgree'=>$chgno);
	$this->db->where('nSeqNo', $no);
    $this->db->update('ndg_Payment',$data);
  }


	// 사용중인 게시판 이름, type
	public function getBoardName(){
		$rtn = null;
		$this->db->select('vType, vName');
		$this->db->from('ndg_BoardType');
		$this->db->where('emDelFlag', 'N');
		return  $this->db->get()->result_array();
	}


	// 상단 게시물 입력
	public function postTopBoard(){

		$arrBoardNo		= $this->input->post('nBoardNo');
		$arrType		= $this->input->post('vType');
		$arrCategory	= $this->input->post('categoryNo');
		$arrSort		= $this->input->post('nSort');
		for($i=0; $i<count($arrBoardNo); $i++){
			$nBoardNo = $arrBoardNo[$i];
			$vType = $arrType[$i];
			$nCategoryNo = $arrCategory[$i];
			$nSort = $arrSort[$i];
			$where = ['vType' => $vType, 'nCategoryNo'=> $nCategoryNo, 'nSort' => $nSort];
			$data = ['nBoardNo' => $nBoardNo];
			$this->db->where($where);
			$this->db->update('ndg_BoardMain', $data);
		}

		return true;
	}


	public function getBbsListTop($vType, $listArr=null, $categoryNo){

		$table = ($vType == 'stock') ? 'ndg_Stock' : 'ndg_Board';
		$mainval = $this->input->post('mainval') ? $this->input->post('mainval'):'';
		$this->db->select('vType, nSeqNo, vSubject');
		$this->db->from($table);
		if($vType == 'today' || $vType == "stock"){
			$this->db->where_In('vType', $listArr);
		}
		else{
			$this->db->where('vType', $vType);
			if($categoryNo != 0){
				$this->db->where('nCategoryNo', $categoryNo);
			}
		}
		$this->db->order_by('nSeqNo', 'DESC');

		return $this->db->get()->result_array();
	}

	public function getBoardAutolist($vType, $categoryNo, $listArr) {
		$table = ($vType == 'stock') ? 'ndg_Stock' : 'ndg_Board';
		$this->db->select('vType, nSeqNo');
		$this->db->from($table);
		if($vType == 'today' || $vType == "stock"){
			$this->db->where_In('vType', $listArr);
		}
		else{
			$this->db->where('vType', $vType);
			if($categoryNo != 0){
				$this->db->where('nCategoryNo', $categoryNo);
			}
		}
		$this->db->order_by('nSeqNo', 'DESC');
		$this->db->limit(5,0);
		return $this->db->get()->result_array();
	}


}
