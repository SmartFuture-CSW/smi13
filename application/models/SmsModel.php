<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin
  * Stock Model
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터베이스 처리후 컨트롤러로 리턴
  *   # index # StockList # history
  *   # getStockList # getStockCount
  *
*/

class SmsModel extends CI_Model
{

	# DB테이블명 정의
	const SmsLog 		  = 'ndg_Sms_Log';		    # 유저 테이블
	const TABLE_STOCK 			= 'ndg_Stock';			# stock table
	const TABLE_PAYMENT 		= 'ndg_Payment';		# payment table
	const TABLE_USER 		= 'ndg_User';			# User table
	const TABLE_GOODS		= 'goods';
    
	function __construct(){
	    parent::__construct();
	    $this->load->database();
	 	$this->load->library('Sms');
	}

	/** 
     * insert sms_log TABLE  
     * @param[$set] : set data 
    **/
  	function setSmsLog($set)
    {
  		return $this->db->set($set)->insert(self::SmsLog);
  	}

	public function getUserList($field,$opt=''){
		$reg1=$reg2='';
		if($opt!=''){
			if(substr($opt,0,1)=='s'){
				$reg1=substr($opt,1);
				if(strpos($reg1,'e')){
					$reg=explode('e',$reg1);
					$reg1=$reg[0];
					$reg2=$reg[1];
				}
			}else if(substr($opt,0,1)=='e'){
				$reg2=substr($opt,1);
			}
		}
		if($field=='count') $data['all']=$this->db->select('COUNT(*) AS cnt'); else if($field=='vPhone') $data['all']=$this->db->select($field);
		$data['all']=$this->db->from('ndg_User');
		$data['all']=$this->db->where('nLevel','1');
		$data['all']=$this->db->where("vPhone NOT IN ('admin', 'nodaji') ");
		if($reg1!='') $data['all']=$this->db->where("dtRegDate >= ".$this->db->escape($reg1." 00:00:00"),NULL,FALSE);
		if($reg2!='') $data['all']=$this->db->where("dtRegDate <= ".$this->db->escape($reg2." 00:00:00"),NULL,FALSE);
		$data['all']=$this->db->get()->result_array();

		if($field=='count'){
			$data['charge']=$this->db->select('COUNT(*) AS cnt');
		}else if($field=='vPhone'){
			$data['charge']=$this->db->select('A.vPhone');
		}
		if($reg1!='') $data['charge']=$this->db->where("A.dtRegDate >= ".$this->db->escape($reg1." 00:00:00"),NULL,FALSE);
		if($reg2!='') $data['charge']=$this->db->where("A.dtRegDate <= ".$this->db->escape($reg2." 00:00:00"),NULL,FALSE);

		$data['charge']=$this->db->from('ndg_User A');
		$data['charge']=$this->db->where('IFNULL((SELECT nUserNo FROM ndg_Payment WHERE nUserNo=A.nSeqNo AND nType=1 AND nOrderStatus=0 AND CONVERT(vStartDate,DATE) <= CURDATE() AND CONVERT(vEndDate,DATE) >= CURDATE() LIMIT 1),0) > 0');
		$data['charge']=$this->db->where('A.nLevel','1');
		$data['charge']=$this->db->where("A.vPhone NOT IN ('admin', 'nodaji') ");
		if($field=='vPhone') $data['charge']=$this->db->where("A.nSeqNo > 1");
		$data['charge']=$this->db->get()->result_array();
		return $data;
	}

	public function getStockUserList(){
/*
		SELECT NS.vStockName, COUNT(NP.nSeqNo) FROM 
			`ndg_Stock` AS NS
			LEFT OUTER JOIN ndg_Payment AS NP ON NS.nSeqNo = NP.nStockNo
		WHERE NS.nStat = 1 AND 
			NOW() < DATE_ADD(NS.dtRegDate, INTERVAL NS.nAimDate DAY)
			GROUP BY NS.nSeqNo
*/
		$aRtn = null;
		$aRtn = $this->db->select("NS.nSeqNo, NS.vStockName, count(NP.nSeqNo) as cnt")
		->from(self::TABLE_STOCK." as NS")
		->join(self::TABLE_PAYMENT." as NP","NS.nSeqNo = NP.nStockNo", 'LEFT OUTER')
		->where('NS.nStat = 1')
		->where('NOW() < DATE_ADD(NS.dtRegDate, INTERVAL NS.nAimDate DAY)')
		->group_by('NS.nSeqNo')->get()->result_array();
		return $aRtn;
	}


	public function getPremiumSMSUserList(){
		$aRtn = null;
		$reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';//201210 추가
		$reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';

		$aRtn = $this->db->select('U.*');
		$aRtn = $this->db->from(self::TABLE_PAYMENT." as P");
		$aRtn = $this->db->join(self::TABLE_GOODS." as G", 'P.nGoodsNo = G.nSeqNo', 'left');
		$aRtn = $this->db->join(self::TABLE_USER." as U", "P.nUserNo = U.nSeqNo");
		$aRtn = $this->db->where('G.vGoodsKind', 2);
		$aRtn = $this->db->where("P.nOrderStatus < 1");
		$aRtn = $this->db->where("P.vEndDate >= '".date("Y-m-d")."'");
		if($reg1!='') $aRtn=$this->db->where("U.dtRegDate >= ".$this->db->escape($reg1." 00:00:00"),NULL,FALSE);
		if($reg2!='') $aRtn=$this->db->where("U.dtRegDate <= ".$this->db->escape($reg2." 00:00:00"),NULL,FALSE);
		$aRtn = $this->db->group_by('U.nSeqNo')->get()->result_array();
		return $aRtn;
	}


	public function sendSms() {
		$rl = $this->input->post('receive_list') ? $this->input->post('receive_list'):'';
		$vType = $this->input->post('vType') ? $this->input->post('vType'):'';
		$vSubject = $this->input->post('vSubject') ? $this->input->post('vSubject'):'노다지주식정보';
		$txContent = $this->input->post('txContent') ? $this->input->post('txContent'):'';
		$txContent = str_replace("'", "", $txContent);
		$reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';//201210 추가
		$reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';

		$sendnum = Sms::SendNum;

		$receive_num = ''; 
		$receive_num1 = ''; 
		$receive_num2 = '';

		if($rl != "all" && $rl != 'free' && $rl != 'charge' && $rl != 'test' && $rl != 'research' && $rl != 'premiumSMS'){
			$nStockNo = $rl;
			$mem=$this->db->select('NU.vPhone');
			$mem=$this->db->from(self::TABLE_PAYMENT.' as NP');
			$mem=$this->db->join(self::TABLE_USER.' as NU', 'NP.nUserNo = NU.nSeqNo');
			$mem=$this->db->where('nStockNo', $nStockNo);
			if($reg1!='') $mem=$this->db->where("NU.dtRegDate >= ".$this->db->escape($reg1." 00:00:00"),NULL,FALSE);
			if($reg2!='') $mem=$this->db->where("NU.dtRegDate <= ".$this->db->escape($reg2." 00:00:00"),NULL,FALSE);
			$mem=$this->db->get()->result_array();
			foreach($mem as $row){
				if(isset($row['vPhone'])) $receive_num[] = $this->secret->secretDecode($row['vPhone']);
			}

		} else if($rl == "premiumSMS"){
			// test
			$mem = $this->getPremiumSMSUserList();
			foreach($mem as $val){
				if($val!='') $receive_num[] = $this->secret->secretDecode($val['vPhone']);
			}


		} else if($rl == "test"){
			// test
			$receive_num[]  = '01031058689';
			$receive_num[]  = '01045439285';

		} else if($rl == 'research') {
			$receive_num[]  = '01045439285';
			$receive_num[]  = '01048156507';
			$receive_num[]  = '01050281087';

		} else {
			$opt='';
			if($reg1!='') $opt.='s'.$reg1;
			if($reg2!='') $opt.='e'.$reg2;

			$mem = $this->getUserList('vPhone',$opt);
			if(substr($rl,0,3)=='all' || substr($rl,0,4)=='free'){
				foreach($mem['all'] as $val){
					if($val!='') $receive_num1[] = $this->secret->secretDecode($val['vPhone']);
				}
			}
			if(substr($rl,0,4)=='free' || substr($rl,0,6)=='charge'){
				foreach($mem['charge'] as $val){
					if($val!='') $receive_num2[] = $this->secret->secretDecode($val['vPhone']);
				}
			}
			if(substr($rl,0,4)=='free'){
				$receive_num=array_diff($receive_num1,$receive_num2);
			}else if(substr($rl,0,3)=='all'){ 
				$receive_num=$receive_num1;
			}else{
				$receive_num=$receive_num2;
			}
		}

		$receive_num=implode(",",$receive_num);
		if(substr($vType,0,3)=='sms' || substr($vType,0,3)=='lms'){
			$i = 0;
/*
			$arrReceiveNum = explode(",", $receive_num);
			$txt_phone_no = "";
			foreach($arrReceiveNum as $row){
				if($txt_phone_no == ""){
					$txt_phone_no = $row;
				}
				else{
					$txt_phone_no = ",".$row;
				}
				if($i >= 500){
					$this->sms->smsSend($txt_phone_no, substr($vType,0,3),$this->util->cut_str($vSubject,18), $txContent);
					$txt_phone_no = "";
					$i = 0;
				}
				$i++;
			}
*/
			$this->sms->smsSend($receive_num, substr($vType,0,3),$this->util->cut_str($vSubject,18), $txContent);
			$insertData = [
				'vType' => $vType,
				'vSubject' => $vSubject,
				'txContent' => $txContent,
				'vSendNum' => $sendnum,
				'vReceiveNum' => $receive_num,
				'vIP' => $_SERVER['REMOTE_ADDR'],
			];
			$result=$this->db->set($insertData)->insert("ndg_Sms_Log");
		}

		if(strpos('a'.$vType,'push')){
			//if(count($android_to)){
			//	$send_result = send_fcm($title, $contents, $target, "", "", $android_to);
				$rnum=explode(',',$receive_num);
				$insertData = [
					'vPage' => '문자발송',
					'vSubject' => $vSubject,
					'txContent' => $txContent,
					'vEtc1' => $sendnum,
					'vEtc3'=>'-1',
					'vEtc4' => $rl,
					'vMemo' => count($rnum),
					'vIP' => $_SERVER['REMOTE_ADDR'],
				];
				$result=$this->db->set($insertData)->insert("ndg_Push_Log");
			//}
		}
		return true;	
	}

/*
function send_fcm($title, $message, $group_idx, $category, $board_idx, $id){
	send_fcm_type($title, $message, $group_idx, $category, $board_idx, $id, 0);
}

function send_fcm_type($title, $message, $group_idx, $category, $board_idx,  $id, $type){
	$limit_once = 999;
	$limit_check = false;

	$title = mb_substr($title, 0, 50, "UTF-8");

	$message = mb_substr($message, 0, 100, "UTF-8");

	$headers = array ('Authorization: key=' . GOOGLE_SERVER_KEY,'Content-Type: application/json');

	if(count($id) > $limit_once){
		$id = array_chunk($id,$limit_once);
		$limit_check = true;
	}
	if($limit_check){
		foreach($id as $idarr){
			$arr = array('data' => array('title'=>$title,'message'=>$message, 'group_idx'=>$group_idx, 'category'=>$category, 'board_idx'=>$board_idx),'registration_ids' => $idarr);
			$ch = curl_init();
			//curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
			curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($arr));
			$response = curl_exec($ch);
			curl_close($ch);
		}
	}else{
		$arr = array('data' => array('title'=>$title,'message'=>$message, 'group_idx'=>$group_idx, 'category'=>$category, 'board_idx'=>$board_idx),'registration_ids' => $id);
		$ch = curl_init();
		//curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($arr));
		$response = curl_exec($ch);
		curl_close($ch);
	}

	$obj = json_decode($response);
	$cnt = $obj->{"success"};
	return "success";

}*/

}