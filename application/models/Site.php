<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
	* siteinfo Table 관련 모델 
	* @author 임정원 / 2020-02-14
	* @since  Version 1.0.0
    
    * CRUD 규칙 
    * DB INSERT -> add 테이블명 함수명 
    * DB UPDATE -> set 테이블명 함수명
    * DB DELETE -> del 테이블명 함수명
    * DB SELECT -> get 테이블명 함수명
*/

class Site extends CI_Model
{
	# DB테이블명 정의
    const SiteInfo  = 'ndg_Siteinfo';		    # 유저 테이블
    
    # 생성자
  	function __construct(){
    	parent::__construct();
    	$this->load->database();
  	}

  	/** 
     * SELETE USER TABLE 
     * @param[$where] : where 절
     * @param[$select] : select 절 
    **/ 
    function getSiteInfo($where, $select = '*')
    {   
      	$query = $this->db->from(self::SiteInfo)
                    ->select($select)
                    ->where($where)
                    ->get();

      	return $query->row_array(); # 1명조회 
    }

}