<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
@brief: Crontab에서 실행되는 정기결제 데이터베이스 쿼리 처리 모델
@detail: 정기결제외 Crontab으로 실행시 데이터베이스 처리하는 모든 모델 다 여기에
@author: csw
@date: 2020-09-09
@version: 1.0.0
*/


class CronModel extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}


	

}
