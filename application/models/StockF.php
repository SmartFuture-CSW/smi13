<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
*/

class StockF extends CI_Model {
	# DB테이블명 정의
	const Stock 	= 'ndg_Stock';          # 게시판 테이블

	# 생성자
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('Secret');	# 비밀번호 암호화 복호화 
	}


	/** 
	* UPDATE ndg_Stock  
	* @param[$stock] : 게시판 고유번호 
	**/
	public function getStockData($stockNo) {
		$query = $this->db->from(self::Stock)
        			->where('nSeqNo', $stockNo)->get();
		return $query->row_array();
	}

	/** 
	* UPDATE ndg_Stock  
	* @param[$stock] : 게시판 고유번호 
	**/
	public function setStockShare($stockNo) {
		return $this->db->set("nShare", "nShare + 1", false)->where("nSeqNo", $stockNo)->update(self::Stock);
	}

	/** 
	* UPDATE ndg_Board  
	* @param[$boardNo] : 게시판 고유번호 
	**/
	public function setStockLike($stockNo) {
		return $this->db->set("nLike", "nLike + 1", false)->where("nSeqNo", $stockNo)->update(self::Stock);
	}


}