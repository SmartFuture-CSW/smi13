<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @breif: 게시판 카테고리 관리 위한 Model
* @detail: 게시판 카테고리 관리 리스트, 입력, 수정, 삭제 출력
* @author: csw
*/
class CategoryModel extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();

		// table 명칭 상수선언
		defined('TABLE_CATEGORY')		OR define('TABLE_CATEGORY', 'ndg_Category');
		defined('TABLE_BOARD')			OR define('TABLE_BOARD', 'ndg_Board');
		defined('TABLE_BOARDTYPE')			OR define('TABLE_BOARDTYPE', 'ndg_BoardType');
	}

	// Category 테이블 리스트
	public function getCategoryList($pageNo, $total, $limit) {
		$limit = $this->input->post('limit') ? $this->input->post('limit') : '10';
		$orderby = $this->input->post('orderby') ? $this->input->post('orderby') : 'nSeqNo';
		$start_record = ($pageNo - 1) * $limit;
		$this->db->from(TABLE_CATEGORY);
		$this->db->where('emDelFlag', 'N');
		$this->db->limit($limit, $start_record);
		$this->db->order_by($orderby.'', 'DESC');
		return $this->db->get()->result_array();
	}

	// 게시판별 Category 테이블 리스트
	public function getBoardCategoryList($vType) {
		$this->db->select('nSeqNo, vSubject');
		$this->db->from(TABLE_CATEGORY);
		$this->db->where('vType', $vType);
		$this->db->where('emDelFlag', 'N');
		$this->db->order_by('nSeqNo asc');
		return $this->db->get()->result_array();
	}


	// Category 테이블 사용할 게시판 리스트
	public function getBoardList(){
		$notUseBoards = array('news', 'notice');
		$this->db->select('vType, vName');
		$this->db->from(TABLE_BOARDTYPE);
		$this->db->where_not_in('vType', $notUseBoards);
		return $this->db->get()->result_array();
	}

	// Category 테이블 상세
	public function getCategoryInfo(){
		$nSeqNo = $this->input->post('nSeqNo') ? $this->input->post('nSeqNo') : null;
		$this->db->select('*');
		$this->db->from(TABLE_CATEGORY);
		$this->db->where('nSeqNo', $nSeqNo);
		return $this->db->get()->row_array();
	}

	// Category 테이블 삭제
	public function removeCategory(){
		$nSeqNo = $this->input->post('nSeqNo') ? $this->input->post('nSeqNo') : null;
		if($nSeqNo == null){
			$this->util->alert('DB 오류!', '');
			return false;
		}
		$where = ['nSeqNo =' => $nSeqNo];
		$this->db->delete(TABLE_CATEGORY, $where);
		return true;
	}

	// 카테고리 입력/수정
	public function postCategory(){
		$mode = $this->input->post('mode') ? $this->input->post('mode') : '';
		$vName = $this->input->post('vName') ? $this->input->post('vName') : '';
		$vType = $this->input->post('vType') ? $this->input->post('vType') : '';
		$vSubject = $this->input->post('vSubject') ? $this->input->post('vSubject') : '';
		$insertData = [
			'nUserNo'    => $this->session->userdata('UNO'),
			'vType'      => $this->input->post('vType'),
			'vSubject' => $this->input->post('vSubject'),
			'vName' => $this->input->post('vName'),
			'emDelFlag' => 'N',
			'vIp' => $_SERVER['REMOTE_ADDR']
		];
		// 입력
		if($mode  == 'post'){
			$result=$this->db->set($insertData)->insert(TABLE_CATEGORY);
		}
		// 수정
		else{
			$nSeqNo = $this->input->post('nSeqNo') ? $this->input->post('nSeqNo') : null;
			if($nSeqNo == null){
				$this->util->alert('DB 오류!', '');
				return false;
			}
			$where = ['nSeqNo =' => $nSeqNo];
			$result=$this->db->set($insertData)->where($where)->update(TABLE_CATEGORY);
		}
		if (!$result) {
			$this->util->alert('DB 오류!', '');
			return false;
		}
		return true;
	}

	// 입력된 전체 카테고리 갯수
	public function getCategoryCount() {
		$this->db->select('nSeqNo');
		$this->db->from(TABLE_CATEGORY);
		$this->db->where('emDelFlag' , 'N');
		return $this->db->get()->num_rows();
	}

}
