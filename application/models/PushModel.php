<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin
  * Push Model
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터베이스 처리후 컨트롤러로 리턴
  *   # index # StockList # history
  *   # getStockList # getStockCount
  *
*/

class PushModel extends CI_Model
{
	function __construct(){
	    parent::__construct();
	    $this->load->database();
	 	$this->load->library('Sms');
	}

  public function getPushList($pageNo, $total, $limit)
  {
    # search
    $limit = $this->input->post('limit')?$this->input->post('limit'):'10';
    $reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';
    $reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';

    # 검색 처리
	if($reg1) $this->db->where("A.dtRegDate >= ".$this->db->escape($reg1." 00:00:00"),NULL,FALSE);
    if($reg2) $this->db->where("A.dtRegDate <= ".$this->db->escape($reg2." 00:00:00"),NULL,FALSE);

    # 페이징처리
    $start_record = ($pageNo - 1) * $limit;

    $this->db->select('A.*');
	$this->db->from('ndg_Push_Log AS A');
	$this->db->limit($limit, $start_record);
    $this->db->order_by('A.nSeqNo', 'DESC');

    return $this->db->get()->result_array();
  }

  /**
  * Push 회원 테이블 카운팅
  * @author 채원만 / 2019-11-12 */
  public function getPushCount()
  {
    # search
    $reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';
    $reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';
	if($reg1) $this->db->where("A.dtRegDate >= ".$this->db->escape($reg1." 00:00:00"),NULL,FALSE);
    if($reg2) $this->db->where("A.dtRegDate <= ".$this->db->escape($reg2." 00:00:00"),NULL,FALSE);

    $this->db->select( 'A.nSeqNo' );
	$this->db->from('ndg_Push_Log AS A');
    return $this->db->get()->num_rows();
  }

  public function getUserList($field)
  {
	if($field=='count'){
		$data['all']=$this->db->select('COUNT(*) AS cnt');
	} else if($field=='vPhone'){ 
		$data['all']=$this->db->select($field);
	} else if($field == 'vPushKey'){
		$data['all']=$this->db->select($field);
	}

    $data['all']=$this->db->from('ndg_User');
	$data['all']=$this->db->where('nLevel','1');
	$data['all']=$this->db->where("vPhone <> 'admin'");
	$data['all']=$this->db->get()->result_array();
	if($field=='count'){
		$data['charge']=$this->db->select('COUNT(DISTINCT(A.nUserNo)) AS cnt');
	}else if($field=='vPhone'){
		$data['charge']=$this->db->select('B.vPhone');
		$data['charge']=$this->db->join('ndg_User AS B', 'A.nUserNo=B.nSeqno', 'LEFT');
	}else if($field=='vPushKey'){
		$data['charge']=$this->db->select('B.vPushKey');
		$data['charge']=$this->db->join('ndg_User AS B', 'A.nUserNo=B.nSeqno', 'LEFT');
	}
    $data['charge']=$this->db->from('ndg_Payment A');
	$data['charge']=$this->db->where('A.nAmount <> A.nPoint');
	if($field=='vPhone') $data['charge']=$this->db->where("B.nSeqNo > 1");
	$data['charge']=$this->db->get()->result_array();
	return $data;
  }

	public function sendPush($rl = null, $vType = null, $vEtc2 = null, $txContent = null, $vSubject = null)
	{
		$rl = ($rl == null) ? $this->input->post('receive_list') : $rl;
		$vType = ($vType == null) ? $this->input->post('vType') : $vType;
		$vEtc2 = ($vEtc2 == null) ? $this->input->post('vEtc2') : $vEtc2;
		$txContent = ($txContent == null) ? $this->input->post('txContent') : $txContent;
		$vSubject = ($vSubject == null) ? $this->util->cut_str($txContent,20) : $vSubject;

		$sendnum = Sms::SendNum;

		if($rl == 'tester'){
			//01045439285

			$tester = [
				$this->secret->secretEncode('01031058689'),  $this->secret->secretEncode('01045439285')
			];

			$mem = $this->db->select('vPushKey')
				->from('ndg_User')
				->where_in('vPhone', $tester)
				->get()
				->result_array();


			foreach($mem as $row){
				if(isset($row['vPushKey'])) $receive_num[] = $row['vPushKey'];
			}

		}else{

			$mem=$this->getUserList('vPushKey');
			$receive_num=$receive_num1=$receive_num2='';
			if($rl=='all' || $rl=='free'){
				foreach($mem['all'] as $val){
					if($val['vPushKey'] == ''){
						continue;
					}
					if($val!='') $receive_num1[]=$val['vPushKey'];
				}
			}
			if($rl=='free' || $rl=='charge'){
				foreach($mem['charge'] as $val){
					if($val['vPushKey'] == ''){
						continue;
					}
					if($val!='') $receive_num2[]=$val['vPushKey'];
				}
			}

			if($rl=='free'){
				$receive_num=array_diff($receive_num1,$receive_num2); 
			} else if($rl=='all'){ 
				$receive_num=$receive_num1;
			} else {
				$receive_num=$receive_num2;
			}
		}


		$receive_num=implode(",",$receive_num);


		if(strpos('a'.$vType,'push')){
			$rnum=explode(',',$receive_num);

			$vSubject = strip_tags($vSubject);
			$txContent = strip_tags($txContent);


			$insertData = [
				'vPage' => '푸시발송',
				'vSubject' => $vSubject,
				'txContent' => $txContent,
				'vEtc1' => $sendnum,
				'vEtc2' => $vEtc2,
				'vEtc3'=>'-1',
				'vEtc4' => $rl,
				'vMemo' => count($rnum),
				'vIP' => $_SERVER['REMOTE_ADDR'],
			];
			$result=$this->db->set($insertData)->insert("ndg_Push_Log");

			//테스트 기기 토큰
			//f4jlPkoExig:APA91bGj4-sFU7wnMUjyTkDwmRJVbdOXdWuiTso4zeriMFoJI2hGpCQttx2g9dG25pq1ODrPdisk8pj_KYtDPDtxCwBWST8w8d3FvogYP2RhU0u7-70xtVGItCq02eckiWzEVTh9GkL9
			$tokens = array_values(array_unique($rnum));

			$title = $vSubject;
			$content = $txContent;
			$messageData = array("message" => $content, 'url' => $vEtc2);
			$fields = array(
				'registration_ids' => $tokens,
				'data' => $messageData,
				'notification' => array('title' => $title, 'body' => $content, 'click_action' => "FCM_PLUGIN_ACTIVITY", 'icon' => 'ic_action_push', 'sound' => 'default')
			);	
			$headers = array(
				'Authorization:key = AAAAuWCzDa8:APA91bFdKVXIWYMHdiaV1K3wdC1jL-WQ9kgUDbFnuFKsmku9uSlLcfbCVb6WCSeHvsAn3qkwgnZ3hkYr1TsFLnzbOi-6O1LwYPhuwCtlH8eJm2EVxSnBdK42ghXOPoxXjo8-dNXIi2bY',
				'Content-Type: application/json'
			);


			$tt = 1;
			$aa = [];
			foreach($tokens as $row){
				$aa[] = $row;
				if($tt == 900){
					$tt = 0;
					$fields['registration_ids'] = $aa;
					$aa = [];
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
					$result = curl_exec($ch);
					curl_close($ch);
				} 
				$tt++;
			}


			$fields['registration_ids'] = $aa;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			$result = curl_exec($ch);
			curl_close($ch);

			/*
			Db에서 검색하여 토큰 배열로 전달
			$table = '회원테이블';
			$where = array(); //조건
			$query = $this->db->get_where($table, $where);
			$result = $query->result();

			foreach($result as $val)
			{
				$tokens[] = 'f4jlPkoExig:APA91bGj4-sFU7wnMUjyTkDwmRJVbdOXdWuiTso4zeriMFoJI2hGpCQttx2g9dG25pq1ODrPdisk8pj_KYtDPDtxCwBWST8w8d3FvogYP2RhU0u7-70xtVGItCq02eckiWzEVTh9GkL9';	
			}
			*/


			//}
		}
		return true;	
	}

	public function getPushReceiveList(){
		
		$pushKeyTable = "ndg_Push_Key";

		$data['all'] = $this->db->select("vPushKey")->from($pushKeyTable)->get()->result_array();
		$data['charge'] = $this->db->select('P.vPushKey')
			->from($pushKeyTable.' as P')
			->join('ndg_Payment as Pay' , 'P.nUserNo = Pay.nUserNo', 'LEFT')
			->where('P.nUserNo is NOT NULL')
			->where('Pay.nSeqNo is NOT NULL')
			->where('Pay.nOrderStatus', 0)
			->where("DATE_FORMAT(vStartDate, '%Y-%m-%d') <= DATE_FORMAT(NOW(), '%Y-%m-%d')")
			->where("DATE_FORMAT(vEndDate, '%Y-%m-%d') >= DATE_FORMAT(NOW(), '%Y-%m-%d')")
			->group_by("P.nSeqNo")->get()->result_array();

		$farr = $this->db->select('vPushKey, nUserNo, (SELECT vEndDate FROM ndg_Payment AS Pay WHERE Pay.nUserNo = PK.nUserNo AND Pay.nType = 1 AND nOrderStatus = 0 ORDER BY vEndDate DESC LIMIT 1) AS vEndDate')
			->from($pushKeyTable.' as PK')
			->where('PK.nUserNo is NOT NULL')
			->get()->result_array();
		foreach($farr as $row){
			$endDate = strtotime($row['vEndDate']);
			$current = time();
			if($endDate < $current){
				$data['free'][] = $row;
			}
		}
		$data['nonmem'] = $this->db->select("vPushKey")->from($pushKeyTable)->where('nUserNo is null')->get()->result_array();
		return $data;
	}


	public function sendPushAndroid($rl = null, $vType = null, $vEtc2 = null, $txContent = null, $vSubject = null) {

		$rl = ($rl == null) ? $this->input->post('receive_list') : $rl;
		$vType = ($vType == null) ? $this->input->post('vType') : $vType;
		$vEtc2 = ($vEtc2 == null) ? $this->input->post('vEtc2') : $vEtc2;
		$txContent = ($txContent == null) ? $this->input->post('txContent') : $txContent;
		$vSubject = ($vSubject == null) ? $this->util->cut_str($this->input->post('vSubject'),20) : $vSubject;
		$sendnum = Sms::SendNum;

		$mem = $this->getPushReceiveList();

		if(in_array('tester', $rl)){
			//$tester = [ 12, 3102, 608 ];
			$tester = [3102];
			$mem = $this->db->select('vPushKey')
				->from('ndg_Push_Key')
				->where_in('nUserNo', $tester)
				->get()
				->result_array();

			foreach($mem as $row){
				if(isset($row['vPushKey'])) $receive_num[] = $row['vPushKey'];
			}
		}

		
		if(in_array('all', $rl)){
			foreach($mem['all'] as $row){
				$receive_num[] = $row['vPushKey'];
			}
		}
		else {
			
			if(in_array('charge', $rl)){
				foreach($mem['charge'] as $row){
					$receive_num[] = $row['vPushKey'];
				}
			}
			if(in_array('free', $rl)){
				foreach($mem['free'] as $row){
					$receive_num[] = $row['vPushKey'];
				}
			}
			if(in_array('nonmem', $rl)){
				foreach($mem['nonmem'] as $row){
					$receive_num[] = $row['vPushKey'];
				}
			}

		}


/*

		if($rl == 'tester'){
			// 12 황욱 , 3102 최상운
			$tester = [ 12, 3102 ];

			$mem = $this->db->select('vPushKey')
				->from('ndg_Push_Key')
				->where_in('nUserNo', $tester)
				->get()
				->result_array();

			foreach($mem as $row){
				if(isset($row['vPushKey'])) $receive_num[] = $row['vPushKey'];
			}


		}else{

			

			exit;

			$mem=$this->getPushReceiveList();
			$receive_num=$receive_num1=$receive_num2='';
			if($rl=='all' || $rl=='free'){
				foreach($mem['all'] as $val){
					if($val['vPushKey'] == ''){
						continue;
					}
					if($val!='') $receive_num1[]=$val['vPushKey'];
				}
			}
			if($rl=='free' || $rl=='charge'){
				foreach($mem['charge'] as $val){
					if($val['vPushKey'] == ''){
						continue;
					}
					if($val!='') $receive_num2[]=$val['vPushKey'];
				}
			}

			if($rl=='free'){
				$receive_num=array_diff($receive_num1,$receive_num2); 
			} else if($rl=='all'){ 
				$receive_num=$receive_num1;
			} else {
				$receive_num=$receive_num2;
			}
		}
*/

		$receive_num=implode(",",$receive_num);
		

		if(strpos('a'.$vType,'push')){
			$rnum=explode(',',$receive_num);
			//print_r($rnum);

			$vSubject = strip_tags($vSubject);
			$txContent = strip_tags($txContent);


			$insertData = [
				'vPage' => '푸시발송',
				'vSubject' => $vSubject,
				'txContent' => $txContent,
				'vEtc1' => $sendnum,
				'vEtc2' => $vEtc2,
				'vEtc3'=>'-1',
				'vEtc4' => implode(',', $rl),
				'vMemo' => count($rnum),
				'vIP' => $_SERVER['REMOTE_ADDR'],
			];
			$result=$this->db->set($insertData)->insert("ndg_Push_Log");

			//테스트 기기 토큰
			//f4jlPkoExig:APA91bGj4-sFU7wnMUjyTkDwmRJVbdOXdWuiTso4zeriMFoJI2hGpCQttx2g9dG25pq1ODrPdisk8pj_KYtDPDtxCwBWST8w8d3FvogYP2RhU0u7-70xtVGItCq02eckiWzEVTh9GkL9
			$tokens = array_values(array_unique($rnum));

			$title = $vSubject;
			$content = $txContent;
			$messageData = array("message" => $content);
			if($vEtc2 != null){
				$messageData['url'] = $vEtc2;
			}
			$fields = array(
				'registration_ids' => $tokens,
				'data' => $messageData,
				'notification' => array('title' => $title, 'body' => $content, 'click_action' => "FCM_PLUGIN_ACTIVITY", 'icon' => 'ic_action_push', 'sound' => 'default')
			);	
			$headers = array(
				'Authorization:key = AAAAuWCzDa8:APA91bFdKVXIWYMHdiaV1K3wdC1jL-WQ9kgUDbFnuFKsmku9uSlLcfbCVb6WCSeHvsAn3qkwgnZ3hkYr1TsFLnzbOi-6O1LwYPhuwCtlH8eJm2EVxSnBdK42ghXOPoxXjo8-dNXIi2bY',
				'Content-Type: application/json'
			);


			$tt = 1;
			$aa = [];
			foreach($tokens as $row){
				$aa[] = $row;
				if($tt == 900){
					$tt = 0;
					$fields['registration_ids'] = $aa;
					$aa = [];
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
					$result = curl_exec($ch);
					curl_close($ch);

				} 
				$tt++;
			}


			$fields['registration_ids'] = $aa;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			$result = curl_exec($ch);
			curl_close($ch);

		}
		return true;	
	}



	/*
	function send_fcm($title, $message, $group_idx, $category, $board_idx, $id){
		send_fcm_type($title, $message, $group_idx, $category, $board_idx, $id, 0);
	}

	function send_fcm_type($title, $message, $group_idx, $category, $board_idx,  $id, $type){
		$limit_once = 999;
		$limit_check = false;

		$title = mb_substr($title, 0, 50, "UTF-8");

		$message = mb_substr($message, 0, 100, "UTF-8");

		$headers = array ('Authorization: key=' . GOOGLE_SERVER_KEY,'Content-Type: application/json');

		if(count($id) > $limit_once){
			$id = array_chunk($id,$limit_once);
			$limit_check = true;
		}
		if($limit_check){
			foreach($id as $idarr){
				$arr = array('data' => array('title'=>$title,'message'=>$message, 'group_idx'=>$group_idx, 'category'=>$category, 'board_idx'=>$board_idx),'registration_ids' => $idarr);
				$ch = curl_init();
				//curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
				curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($arr));
				$response = curl_exec($ch);
				curl_close($ch);
			}
		}else{
			$arr = array('data' => array('title'=>$title,'message'=>$message, 'group_idx'=>$group_idx, 'category'=>$category, 'board_idx'=>$board_idx),'registration_ids' => $id);
			$ch = curl_init();
			//curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
			curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($arr));
			$response = curl_exec($ch);
			curl_close($ch);
		}

		$obj = json_decode($response);
		$cnt = $obj->{"success"};
		return "success";
	}*/
}