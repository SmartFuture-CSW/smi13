<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin
  * Stock Model
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터베이스 처리후 컨트롤러로 리턴
  *   # index # StockList # history
  *   # getStockList # getStockCount
  *
*/


class StockModel extends CI_Model
{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

  /**
  * Stock 회원 테이블 DB 가져오기
  * @author 채원만
  * @param[pageNo] 페이지 번호
  * @param[total] 총 ROW 갯수
  * @param[limit] 한페이지에 보여질 갯수 */
  public function getStockList($pageNo, $total, $limit)
  {
    # search
    $keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
    $searchhash    = $this->input->post('searchhash')?$this->input->post('searchhash'):'';
    $limit = $this->input->post('limit')?$this->input->post('limit'):'10';
	$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
	$bo_table=$this->input->post('vType')?$this->input->post('vType'):'';	
	if($bo_table=='') $bo_table='theme';
	$mainval = $this->input->post('mainval') ? $this->input->post('mainval'):'';
    $orderby = $this->input->post('orderby')?$this->input->post('orderby'):'nSeqNo';
    $reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';
    $reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';
	$nResult = $this->input->post('nResult') ? $this->input->post('nResult'): null;
    # 검색 처리
	if($keyword) $this->db->like("CONCAT(A.vSubject,'|',A.txContent,'|',A.vStockName)", $keyword, 'both');
    if($searchhash) $this->db->like("A.txTag", $searchhash, 'both');
	if($reg1) $this->db->where("A.dtRegDate >= ".$this->db->escape($reg1." 00:00:00"),NULL,FALSE);
    if($reg2) $this->db->where("A.dtRegDate <= ".$this->db->escape($reg2." 00:00:00"),NULL,FALSE);
		if($nResult) $this->db->where("A.nResult", $nResult);

    # 페이징처리
    $start_record = ($pageNo - 1) * $limit;

    $this->db->select('A.*, B.vName');
	$this->db->from('ndg_Stock AS A');
	$this->db->join('ndg_User AS B', 'A.nUserNo=B.nSeqno', 'LEFT');
	if($bo_table != "all"){
		$this->db->where('A.vType', $bo_table);
	}
	$this->db->where('A.emDelFlag', 'N');
	$this->db->limit($limit, $start_record);
    $this->db->order_by('A.'.$orderby.'', 'DESC');

    return $this->db->get()->result_array();
  }

	/**
	* Stock 회원 테이블 카운팅
	* @author 채원만 / 2019-11-12 */
	public function getStockCount() {
		# search
		$keyword    = $this->input->post('keyword')?$this->input->post('keyword'):'';
		$limit = $this->input->post('limit')?$this->input->post('limit'):'10';
		$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
		if($bo_table=='') $bo_table='theme';
		$reg1 = $this->input->post('reg1')?$this->input->post('reg1'):'';
		$reg2 = $this->input->post('reg2')?$this->input->post('reg2'):'';
		$mainval = $this->input->post('mainval') ? $this->input->post('mainval'):'';
		$nResult = $this->input->post('nResult') ? $this->input->post('nResult'): null;

		# 검색 처리
		if($keyword) $this->db->like("CONCAT(A.vSubject,'|',A.txContent,'|',A.vStockName)", $keyword, 'both');
		if($reg1) $this->db->where("A.dtRegDate >= ".$this->db->escape($reg1." 00:00:00"),NULL,FALSE);
		if($reg2) $this->db->where("A.dtRegDate <= ".$this->db->escape($reg2." 00:00:00"),NULL,FALSE);
		if($nResult) $this->db->where("A.nResult", $nResult);

		$this->db->select( 'A.nSeqNo' );
		$this->db->from('ndg_Stock AS A');
		$this->db->join('ndg_User AS B', 'A.nUserNo=B.nSeqno', 'LEFT');
		if($bo_table != "all"){
			$this->db->where('A.vType', $bo_table);
		}
		$this->db->where('A.emDelFlag', 'N');
		return $this->db->get()->num_rows();
	}

  public function getStockwrite($bd_no)
  {
	  if($bd_no!=''){
		$this->db->select('A.*');
		$this->db->from('ndg_Stock AS A');
		$this->db->where('A.nSeqNo', $bd_no);
		return $this->db->get()->result_array();
	  }else{
		  return;
	  }
  }

  public function delStockData()
  {
    $bd_no = $this->input->post('bd_no')?$this->input->post('bd_no'):'';
	$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
	$bo_table = $this->input->post('bo_table')?$this->input->post('bo_table'):'';	
	if($bo_table==''){
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
	}
	if($bo_table=='') $bo_table='theme';
	if($bd_no!=''){
		$upDir  = $_SERVER['DOCUMENT_ROOT'] . '/data/stock/thumb/';
		$this->db->select('A.*');
		$this->db->from('ndg_Stock AS A');
		$this->db->where('A.nSeqNo', $bd_no);
		$row=$this->db->get()->result_array();
		@unlink($upDir.$row[0]['vImage']);
		@unlink($upDir.$row[0]['vImage2']);
		$this->db->delete('ndg_Stock',array('nSeqNo'=>$bd_no));
	}
  }

	public function setStockwrite() {
		$bd_no = $this->input->post('bd_no')?$this->input->post('bd_no'):'';
		$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
		$bo_table = $this->input->post('bo_table')?$this->input->post('bo_table'):'';	
		if($bo_table=='') $bo_table='theme';
		$upDir  = $_SERVER['DOCUMENT_ROOT'] . '/data/stock/thumb/';
		$fileName1=$this->input->post('vImage_ori')?$this->input->post('vImage_ori'):'';	
		$fileName2=$this->input->post('vImage2_ori')?$this->input->post('vImage2_ori'):'';	
		$fileName1_del=$this->input->post('vImageDel')?$this->input->post('vImageDel'):'';	
		$fileName2_del=$this->input->post('vImage2Del')?$this->input->post('vImage2Del'):'';	
		if($fileName1_del!=''){
			@unlink($upDir.$fileName1_del);
			$fileName1='';
		}
		if($fileName2_del!=''){
			@unlink($upDir.$fileName2_del);
			$fileName2='';
		}
		$nPrice=$this->input->post('nPrice')?$this->input->post('nPrice'):'0';
		$vStockName=$this->input->post('vStockName')?$this->input->post('vStockName'):'';
		$nAimDate=$this->input->post('nAimDate')?$this->input->post('nAimDate'):'0';
		$nAimPercent=$this->input->post('nAimPercent')?$this->input->post('nAimPercent'):'0';
		$nBuyPrice1=$this->input->post('nBuyPrice1')?$this->input->post('nBuyPrice1'):'0';
		$nBuyPrice2=$this->input->post('nBuyPrice2')?$this->input->post('nBuyPrice2'):'0';
		$nAimPrice=$this->input->post('nAimPrice')?$this->input->post('nAimPrice'):'0';
		$nLossPrice=$this->input->post('nLossPrice')?$this->input->post('nLossPrice'):'0';
		$nMarketAtt=$this->input->post('nMarketAtt')?$this->input->post('nMarketAtt'):'0';


		if(isset($_FILES['vImage'])){
			if ($_FILES['vImage']['name']) {
				$name = $_FILES['vImage']['name'];
				$size = $_FILES['vImage']['size'];
				$tmp  = $_FILES['vImage']['tmp_name'];
				$fileName1 = date('YmdHis').rand(10,99).'_'.$name;

				// 사이즈 체크
				if ($size > 5242880) {
					$this->util->alert('파일용량은 5MB를 초과하지 않아야 합니다.', '');
					exit;
				}
				// 업로드성공
				if (! move_uploaded_file($tmp, $upDir . basename($fileName1))) {
					$this->util->alert('파일1 업로드 실패!', '');
					exit;
				} else {
					// tmp 파일 삭제
					@unlink($_FILES['vImage']['tmp_name']);
				}
			}
		}
		if(isset($_FILES['vImage2'])){
			if ($_FILES['vImage2']['name']) {
				$name = $_FILES['vImage2']['name'];
				$size = $_FILES['vImage2']['size'];
				$tmp  = $_FILES['vImage2']['tmp_name'];
				$fileName2 = date('YmdHis').rand(10,99).'_'.$name;

				// 사이즈 체크
				if ($size > 5242880) {
					$this->util->alert('파일용량은 5MB를 초과하지 않아야 합니다.', '');
					exit;
				}
				// 업로드성공
				if (! move_uploaded_file($tmp, $upDir . basename($fileName2))) {
					$this->util->alert('파일2 업로드 실패!', '');
					exit;
				} else {
					// tmp 파일 삭제
					@unlink($_FILES['vImage2']['tmp_name']);
				}
			}
		}
		$insertData = [
			'nUserNo'    => $this->session->userdata('UNO'),
			'vType'      => $bo_table,
			'vSubject' => $this->input->post('vSubject'),
			'vImage' => $fileName1,
			'txContentBefore' => $this->input->post('txContentBefore'),
			'txContent' => $this->input->post('txContent'),
			'txRecomReason' => $this->input->post('txRecomReason'),
			'txTag' => $this->input->post('txTag'),
			'vIP' => $_SERVER['REMOTE_ADDR'],
			'emPush' => ($this->input->post('emPush')?'1':'0'),
			'vName' => $this->input->post('vName'),
			'vImage2' => $fileName2,
			'nPrice' => $nPrice,
			'vStockName' => $vStockName,
			'nAimDate' => $nAimDate,
			'nAimPercent' => $nAimPercent,
			'nBuyPrice1' => $nBuyPrice1,
			'nBuyPrice2' => $nBuyPrice2,
			'nAimPrice' => $nAimPrice,
			'nLossPrice' => $nLossPrice,
			'nMarketAtt' => $nMarketAtt,
		];

		// 뉴스별 종목 추가 입력
		if($bo_table == "news"){
			$vBasicEvent = $this->input->post("vBasicEvent") ? $this->input->post("vBasicEvent") : '';
			$vEventLink = $this->input->post("vEventLink") ? $this->input->post("vEventLink") : '';
			$vNewsLink = $this->input->post("vNewsLink") ? $this->input->post("vNewsLink") : '';
			$insertData['vBasicEvent'] = $vBasicEvent;
			$insertData['vEventLink'] = $vEventLink;
			$insertData['vNewsLink'] = $vNewsLink;
		}

		if($bd_no!=''){
			$where = ['nSeqNo =' => $bd_no];
			$result=$this->db->set($insertData)->where($where)->update("ndg_Stock");
		}else{
			$result=$this->db->set($insertData)->insert("ndg_Stock");
		}

		if (!$result) {
			$this->util->alert('DB 오류!', '');
			return false;
		}
		return true;
	}

    public function setBbsupdate()
  {
	$bd_no = $this->input->post('bd_no')?$this->input->post('bd_no'):'';
	$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
	$bo_table = $this->input->post('bo_table')?$this->input->post('bo_table'):'';	
	if($bo_table==''){
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
	}
	if($bo_table=='') $bo_table='theme';
	$udname=$this->input->post('udname')?$this->input->post('udname'):'';	
	$udval=$this->input->post('udval')?$this->input->post('udval'):'';

	$insertData = [
		$udname => $udval,
	];
	if($bd_no!='' && $udname!='' && $udval!=''){
		$where = ['nSeqNo =' => $bd_no];
		$result=$this->db->set($insertData)->where($where)->update("ndg_Board".($bo_table=='reply'?'Reply':''));
	}
	if (!$result) {
		$this->util->alert('DB 오류!', '');
		return false;
	}
	return true;
  }

  public function getStockMainList($no)
  {
    $this->db->select('*');
    $this->db->from('ndg_BoardMain');
	$this->db->where('vMainVal',$no);
    $this->db->order_by('nSort', 'ASC');
    return $this->db->get()->result_array();
  }

  public function setStockMainUpdate()
  {
	$fval = $this->input->post('fval')?$this->input->post('fval'):'';
	$fval2=explode('<nodaji>',$fval);
	for($k=0;$k<count($fval2);$k++){
		$vMainVal=$k+4;
		if($fval2[$k]=='') continue;
		$fval3=explode('|',$fval2[$k]);
		for($j=0;$j<count($fval3);$j++){
			if($fval3[$j]=='') continue;
			$fval4=explode(',',$fval3[$j]);
			$udtData=[
				'vType'=>$fval4[1],
				'nBoardNo'=>$fval4[2],
			];
			$where = ['vMainVal =' => $vMainVal,'nSort =' => $fval4[0]];
			$result=$this->db->set($udtData)->where($where)->update("ndg_BoardMain");
		}
	}
	return true;
  }

	public function getStockMainAutoList($mainval)
	{
		//if($mainval==6) $ea=5; else $ea=3;

		$arrMainval = array(
			"4" => "vType = 'period'",
			"5" => "vType = 'news'",
			"6" => "vType = 'theme'"
		);

		$this->db->select('*');
		$this->db->from('ndg_Stock');
		//$this->db->where("vType IN ('theme','period','rumor')");
		$this->db->where($arrMainval[$mainval]);
		$this->db->order_by('nSeqNo', 'DESC');
		//$this->db->limit($ea,0);
		return $this->db->get()->result_array();
	}

  public function changeStat($no,$stat)
  {
	if($stat=='1') $chgno='2'; else $chgno='1';
	$data=array('nStat'=>$chgno);
	$this->db->where('nSeqNo', $no);
    $this->db->update('ndg_Stock',$data);
  }

	public function putStoploss($nSeqNo){
		
		
		// $nSeqNo = $this->input->post('nSeqNo') ? $this->input->post('nSeqNo') : null;


		// 트랜잭션 시작
		$this->db->trans_start();

		// 손절확정 처리 종목 확인
		$this->db->select('nSeqNo, nPrice');
		$this->db->where('nSeqNo', $nSeqNo);
		$this->db->from('ndg_Stock');
		$data = $this->db->get()->row_array();
		$nStockNo = $data['nSeqNo'];
		$nPrice = $data['nPrice'];


		
		//$this->db->select()
		$this->db->where('nStockNo', $nStockNo);
		$this->db->where('nOrderStatus', '0');
		// $this->db->where("vCardNo <> ''");
		$this->db->from('ndg_Payment');
		$pay = $this->db->get()->result_array();


		$userNo = array();
		foreach($pay as $row){
			// user table에 반영할 유저들
			$userNo[] = $row['nUserNo'];
			$userPrice[$row['nUserNo']] =  $row['nAmount'];
			$goodsnm = $row['vGoodName'];

			// payment log table에 반영할 데이터
			$logData[] = [
				'nPayNo' => $row['nSeqNo']
				, 'nUserNo' => $row['nUserNo']
				, 'vName' => $row['vName']
				, 'nType' => $row['nType']
				, 'vPaycode' => 'nodaji'
				, 'vPhone' => $row['vPhone']
				, 'vReason' => '손절 확정으로 인한 포인트 환불'
				, 'nState' => 1
			];
			//userpoint table에 반영할 데이터
			$logData2[] = [
				 'nUserNo' => $row['nUserNo']
				, 'nPoint' => $userPrice[$row['nUserNo']]
				, 'vDescription' => '['.$goodsnm.'] 손절 확정으로 인한 포인트 환불'
				, 'vUrl' => '/admin/point'
				, 'vDate' => date("Y-m")
				, 'vDay' => date("d")
			];
			$logData3[] = [
				 'nUserNo' => $row['nUserNo']
				, 'nPointKind' => 1
				, 'nPointType' => 2
				, 'nPoint' => $userPrice[$row['nUserNo']]
				, 'vDescription' => '['.$goodsnm.'] 손절 확정으로 인한 포인트 환불'
			];

		}

		if(count($userNo) == 0){

			$this->db->trans_complete();
		}
		else{
			// 추후 제거
			$this->db->where_in('nSeqNo', $userNo);
			$this->db->from('ndg_User');
			$user = $this->db->get()->result_array();

			foreach($user as $row){
				$userData[] = [
					'nSeqNo' => $row['nSeqNo']
					, 'nPoint' => $row['nPoint'] + $userPrice[$row['nSeqNo']]
				];
			}
			// 유저 포인트 추가
			$this->db->update_batch('ndg_User', $userData, 'nSeqNo');

			// 통합 포인트 추가
			$this->db->where_in('nUserNo', $userNo);
			$this->db->from(TABLE_USER_POINT);
			$user = $this->db->get()->result_array();

			foreach($user as $row){
				$userData2[] = [
					'nUserNo' => $row['nUserNo']
					, 'nFreePoint' => $row['nFreePoint'] + $userPrice[$row['nUserNo']]
					, 'nPayPoint' => $row['nPayPoint']
				];
			}

			// 유저 포인트 추가
			$this->db->update_batch(TABLE_USER_POINT, $userData2, 'nUserNo');

			// 손절확정 로그 추가
			$this->db->insert_batch('ndg_PaymentLog', $logData);	// 추후 제거
			$this->db->insert_batch('ndg_UserPoint', $logData2);	// 추후 제거
			$this->db->insert_batch(TABLE_USER_POINT_LOG, $logData3);

		}
		// stoploss 상태값 변경
		$data = [
			'nStoploss' => 2
		];

		$this->db->where('nSeqNo', $nSeqNo);
		$this->db->update('ndg_Stock',$data);

		$this->db->trans_complete();
		
		return true;
	}

	public function putStockResult($nSeqNo, $nResult){
		$setData = ['nResult' => $nResult];
		$where = ['nSeqNo' => $nSeqNo];
		$this->db->set($setData)->where($where)->update(TABLE_GOODS_STOCK);
		return true;
	}


	public function clearStock(){

		$this->db->where('DATE_ADD(dtregdate, INTERVAL nAimDate DAY) < NOW()');
		$this->db->where('nStat', 1);
		$this->db->update('ndg_Stock', array('nStat'=>2));

		return 'success';

	}

}
