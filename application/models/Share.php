<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
    * Share Table 관련 모델 
    * @author 임정원 / 2020-07-13
    * @since  Version 1.0.0
     
    * CRUD 규칙 
    * DB INSERT -> add 테이블명 함수명 
    * DB UPDATE -> set 테이블명 함수명
    * DB DELETE -> del 테이블명 함수명
    * DB SELECT -> get 테이블명 함수명
*/

class Share extends CI_Model
{

	# DB테이블명 정의
    const Share = 'ndg_Share';          # 공유 게시판
    
    # 생성자
	function __construct(){
      	parent::__construct();
      	$this->load->database();
      	$this->load->library('Secret');	# 비밀번호 암호화 복호화 
	}

    /** 
    * SELETE ndg_Board 
    * @param[$idx] : 게시판 고유번호 
    **/  
    public function getShare($userNo, $boardNo, $type)
    {
        $query = $this->db->from(self::Share)
                    ->where('nUserNo', $userNo)
                    ->where('nBoardNo', $boardNo)
                    ->where('vShareObj', $type)
                    ->get();    
        
        return $query->row_array();
    }

     /** 
    * INSERT ndg_Board 
    * @param : 게시판 데이터 
    **/
    public function addShare($param)
    {
        return $this->db->set($param)->insert(self::Share);
    }


}