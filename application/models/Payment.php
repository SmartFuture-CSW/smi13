<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
    * Payment Table 관련 모델 
    * @author 임정원 / 2020-03-30
    * @since  Version 1.0.0
     
    * CRUD 규칙 
    * DB INSERT -> add 테이블명 함수명 
    * DB UPDATE -> set 테이블명 함수명
    * DB DELETE -> del 테이블명 함수명
    * DB SELECT -> get 테이블명 함수명
*/

class Payment extends CI_Model{

	# DB테이블명 정의
	const Payment 		= 'ndg_Payment';        # 게시판 테이블
	const PaymentInfo 	= 'ndg_PaymentInfo';	  # 댓글 테이블
	const User  		= 'ndg_User';           # 유저 테이블
	const Stock  		= 'ndg_Stock';           # 유저 테이블
	const Goods			= 'goods';

  # 생성자
	function __construct(){
  	parent::__construct();
  	$this->load->database();
  	$this->load->library('Secret');	# 비밀번호 암호화 복호화 
	}


  /** 
   * INSERT PaymentInfo TABLE 
   * @param[$set] : insert data 
  **/ 
  function addPayment($set)
  {
    return $this->db->set($set)->insert(self::Payment);
  }


  /** 
  **/ 
  function getPayment($where, $select = '*')
  {   
	$query = $this->db->from(self::Payment." as P")
				->join(self::Goods." as G", "P.nGoodsNo = G.nSeqNo", "left outer")
				->select($select)
				->where($where)
				->order_by('P.nSeqNo', 'DESC')
				->get();

	return $query->result_array();
  }

	function getPaymentRefund($where, $select = '*'){

		$query = $this->db->from(TABLE_PAYMENT." as P")
					->join(TABLE_GOODS_SUBSCRIBE." as G", "P.nGoodsNo = G.nSeqNo", "left outer")
					->join(TABLE_PAYMENT_REFUND." as R", "P.nSeqNo= R.nPayNo", "left outer")
					->select($select)
					->where($where)
					->order_by('P.nSeqNo', 'DESC')
					->get();

		return $query->result_array();
	}



  /** 
   * SELETE Payment TABLE 
   * @param[$where] : where 절
   * @param[$select] : select 절 
  **/ 
	function getPaymentMyPage($where, $select = 'P.*, S.vType') {
		$query = $this->db->from(self::Payment.' as P')
				->join(self::Stock.' as S', 'P.nStockNo = S.nSeqNo', 'left outer join')
				->select($select)
				->where($where)
				->order_by('nSeqNo', 'DESC')
				->get();

		return $query->result_array();
	}

	function getPointList($where, $select = '*') {
		$query = $this->db->from('ndg_UserPoint as P')
				->select($select)
				->where($where)
				->order_by('nSeqNo', 'DESC')
				->get();

		return $query->result_array();
	}


	function getPointListDetail($where, $select = '*') {
		$query = $this->db->from('ndg_UserPointLog as P')
				->select($select)
				->where($where)
				->order_by('nSeqNo', 'DESC')
				->get();

		return $query->result_array();
	}



  /** 
   * UPDATE Payment TABLE  
   * @param[$set] : set data
   * @param[$where] : where 절 
  **/
  function setPayment($set, $where)
  {
    return $this->db->set($set)->where($where)->update(self::Payment);
  }
    

	/** 
   * INSERT PaymentInfo TABLE 
   * @param[$set] : insert data 
  **/ 
  function addPaymentInfo($set)
  {
    return $this->db->set($set)->insert(self::PaymentInfo);
  }


	/** 
	* UPDATE Payment TABLE  
	* @param[$set] : set data
	* @param[$where] : where 절 안쓸 예정
	**/
	function setPaymentInfo($set, $where) {
		return $this->db->set($set)->where($where)->update(self::PaymentInfo);
	}


  /** 
   * SELETE PaymentInfo TABLE 
   * @param[$where] : where 절
   * @param[$select] : select 절 
  **/ 
  function getPaymentInfo($where, $select = '*')
  {   
    $query = $this->db->from(self::PaymentInfo)
                ->select($select)
                ->where($where)
                ->get();

    return $query->row_array(); # 1 row 
  }

  function getStockAccess($userNo, $stockNo)
  {
    $query = $this->db->from(self::Payment)
                ->select('nSeqNo')
                ->where('nUserNo', $userNo)
                ->where('nStockNo', $stockNo)
                ->where('nOrderStatus', 0)
                ->get();

    return $query->row_array(); # 1 row
  }

}