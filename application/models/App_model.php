<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
    * Board Table 관련 모델 
    * @author 임정원 / 2020-03-30
    * @since  Version 1.0.0
     
    * CRUD 규칙 
    * DB INSERT -> add 테이블명 함수명 
    * DB UPDATE -> set 테이블명 함수명
    * DB DELETE -> del 테이블명 함수명
    * DB SELECT -> get 테이블명 함수명
*/

class App_model extends CI_Model
{

	const TABLE_CRON_MONITER = 'ndg_CronMoniter';

	# 생성자
	function __construct(){

		$this->load->library('session');
		$this->load->database();

		parent::__construct();
	}

	public function setDeviceInfo($data){
		$sessionData = array(
			"deviceKey" => $data['deviceKey'],
			"platform" => $data['deviceType']
		);
		$this->session->set_userdata($sessionData);
	}

	// Cron 모니터링 체크
	public function cronMoniter($data){
		$no = $data['where']['nSeqNo'];
		$set = $data['set'];
		$this->db->where('nSeqNo', $no);
		$this->db->update(self::TABLE_CRON_MONITER,$set);
	}

	// referer 정보 입력
	public function postReferer($param){
		$this->db->set($param)->insert("ndg_Visit");
		$nSeqNo = $this->db->insert_id();
		$data['nVisitNo'] = $nSeqNo;
		return $data;
	}

	// referer 정보 입력
	public function setReferer($param){
		$VNO = ($this->session->userdata('VNO')) ? $this->session->userdata('VNO') : get_cookie('VNO');

		if(!empty($VNO)){
			$this->db->where('nSeqNo', $VNO);
			$this->db->update("ndg_Visit", $param);
		}
//		return $data;
	}


}