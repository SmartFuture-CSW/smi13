<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * Admin
  * Siteinfo Model
  * @author 채원만 / 2020-02-14
  * @since  Version 1.0.0
  * @filesource 데이터베이스 처리후 컨트롤러로 리턴
  *   # index # StockList # history
  *   # getStockList # getStockCount
  *
*/


class SiteinfoModel extends CI_Model
{

  function __construct(){
      parent::__construct();
      $this->load->database();
  }

  /**
  * Siteinfo 테이블 DB 가져오기
  * @author 채원만
  * @param[pageNo] 페이지 번호
  * @param[total] 총 ROW 갯수
  * @param[limit] 한페이지에 보여질 갯수 */
  public function getPrivacy()
  {
    $this->db->select('A.*');
	$this->db->from('ndg_Siteinfo AS A');
	$this->db->limit(1, 0);
    $this->db->order_by('A.nSeqNo', 'DESC');
    return $this->db->get()->result_array();
  }

  public function setPrivacywrite()
  {
	$mode=$this->input->post('mode')?$this->input->post('mode'):'';
	if($mode=='1'){
		$txPrivacy=$this->input->post('txPrivacy')?$this->input->post('txPrivacy'):'';
		$txTerms=$this->input->post('txTerms')?$this->input->post('txTerms'):'';
		$txAdterms=$this->input->post('txAdterms')?$this->input->post('txAdterms'):'';
		$insertData = [
			'txPrivacy'    => $txPrivacy,
			'txTerms'      => $txTerms,
			'txAdterms' => $txAdterms,
		];
	}else if($mode=='2'){
		$txIpAllow=$this->input->post('txIpAllow')?$this->input->post('txIpAllow'):'';
		$vVersion=$this->input->post('vVersion')?$this->input->post('vVersion'):'';
		$insertData = [
			'txIpAllow'    => $txIpAllow,
			'vVersion'      => $vVersion,
		];
	}
	$where = ['nSeqNo =' => '1'];
	if($mode!=''){
		$result=$this->db->set($insertData)->where($where)->update("ndg_Siteinfo");
		if (!$result) {
			$this->util->alert('DB 오류!', '');
			return false;
		}
	}
	return true;
  }





    public function setBbsupdate()
  {
	$bd_no = $this->input->post('bd_no')?$this->input->post('bd_no'):'';
	$bo_table=$this->uri->segment(3)?$this->uri->segment(3):'';
	$bo_table = $this->input->post('bo_table')?$this->input->post('bo_table'):'';	
	if($bo_table==''){
		$bo_table = $this->input->post('vType')?$this->input->post('vType'):'';	
	}
	if($bo_table=='') $bo_table='theme';
	$udname=$this->input->post('udname')?$this->input->post('udname'):'';	
	$udval=$this->input->post('udval')?$this->input->post('udval'):'';

	$insertData = [
		$udname => $udval,
	];
	if($bd_no!='' && $udname!='' && $udval!=''){
		$where = ['nSeqNo =' => $bd_no];
		$result=$this->db->set($insertData)->where($where)->update("ndg_Board".($bo_table=='reply'?'Reply':''));
	}
	if (!$result) {
		$this->util->alert('DB 오류!', '');
		return false;
	}
	return true;
  }

  public function getStockMainList($no)
  {
    $this->db->select('*');
    $this->db->from('ndg_BoardMain');
	$this->db->where('vMainVal',$no);
    $this->db->order_by('nSort', 'ASC');
    return $this->db->get()->result_array();
  }

  public function setStockMainUpdate()
  {
	$fval = $this->input->post('fval')?$this->input->post('fval'):'';
	$fval2=explode('<nodaji>',$fval);
	for($k=0;$k<count($fval2);$k++){
		$vMainVal=$k+4;
		if($fval2[$k]=='') continue;
		$fval3=explode('|',$fval2[$k]);
		for($j=0;$j<count($fval3);$j++){
			if($fval3[$j]=='') continue;
			$fval4=explode(',',$fval3[$j]);
			$udtData=[
				'vType'=>$fval4[1],
				'nBoardNo'=>$fval4[2],
			];
			$where = ['vMainVal =' => $vMainVal,'nSort =' => $fval4[0]];
			$result=$this->db->set($udtData)->where($where)->update("ndg_BoardMain");
		}
	}
	return true;
  }

  public function getStockMainAutoList($mainval)
  {
	if($mainval==6) $ea=5; else $ea=3;
    $this->db->select('*');
	$this->db->from('ndg_Stock');
	$this->db->where("vType IN ('theme','period','rumor')");
	$this->db->order_by('nSeqNo', 'DESC');
    $this->db->limit($ea,0);
	return $this->db->get()->result_array();
  }

  public function changeStat($no,$stat)
  {
	if($stat=='1') $chgno='2'; else $chgno='1';
	$data=array('nStat'=>$chgno);
	$this->db->where('nSeqNo', $no);
    $this->db->update('ndg_Stock',$data);
  }
}
