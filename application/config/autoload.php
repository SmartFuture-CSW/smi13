<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * 자동으로 호출할 패키지들
  *
  * @author 임정원 / 2020-02-14
  * @filesource
  *   	1. Packages    				# $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
  * 	2. libraries 				# $autoload['libraries'] = array('database', 'email', 'session');
  *		3. drivers 					# $autoload['drivers'] = array('cache');
  * 	4. Helper files 			# $autoload['helper'] = array('url', 'file');
  * 	5. Custom config files 		# $autoload['config'] = array('config1', 'config2');
  * 	6. Language files 			# $autoload['language'] = array('lang1', 'lang2');
  *		7. Models 					# $autoload['model'] = array('first_model' => 'first');
  *
*/

$autoload['packages'] = array();
$autoload['libraries'] = array('session', 'Util', 'Paging', 'Response');
$autoload['drivers'] = array();
$autoload['helper'] = array('url', 'cookie');
$autoload['config'] = array('config_smi');
$autoload['language'] = array();
$autoload['model'] = array();
