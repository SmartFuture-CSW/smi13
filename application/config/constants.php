<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * @author 임정원
  *
   constants 선언
*/




# PATH
define('CSS', '/asset/css');
define('IMG', '/asset/img');
define('JS', '/asset/js');
define('TEXT',  $_SERVER["DOCUMENT_ROOT"].'/asset/text');
define('FONT', '/asset/font');
define('KCP_JS', '/kcp/mobile_sample/js');
define('VIEW_PATH', $_SERVER["DOCUMENT_ROOT"].'/application/views');
define('THUMB_PATH', $_SERVER["DOCUMENT_ROOT"].'/data/board/thumb/');


# form_validator value
define('FV_PHONE', 'trim|required|numeric|min_length[10]|max_length[11]');
define('FV_PWD', 'trim|required|alpha_numeric|min_length[5]|max_length[20]');
define('FV_UNAME', 'trim|required|max_length[11]');
define('FV_CODE', 'trim|required|numeric|exact_length[4]');
define('FV_NICK', 'trim|required|max_length[10]');
define('FV_BANK', 'trim|required|max_length[11]');
define('FV_CARD', 'trim|required|numeric|exact_length[4]');
define('FV_BIRTH', 'trim|required|numeric|exact_length[6]');
define('FV_SUBJECT', 'trim|required|max_length[50]');
define('FV_CONTENT', 'trim|required');

# error code
define('ERROR_01', '전화번호를 작성해주세요.');
define('ERROR_02', '패스워드를 작성해 주세요.');
define('ERROR_03', '회원정보가 없습니다. 다시 확인해 주세요.');
define('ERROR_04', '잘못된 접근입니다.');
define('ERROR_05', '로그인한 상태입니다.');
define('ERROR_06', '입력값을 다시 확인해 주세요.');
define('ERROR_07', '인증번호가 틀립니다. 다시 입력 후 확인해주세요.');
define('ERROR_08', 'SMS 인증코드를 발송하였습니다.');
define('ERROR_09', '이미 가입한 회원입니다.');
define('ERROR_10', '발송된 인증코드가 이미 있습니다.');
define('ERROR_11', '잘못된 요청입니다.');
define('ERROR_12', '이미 사용 중인 닉네임 입니다.다시 설정해 주세요.');
define('ERROR_13', '이미 비밀번호가 설정되었습니다.');
define('ERROR_14', '해당 번호는 로그인이 필요한 서비스를 사용하실 수 없습니다.');
define('MSG_01', '인증이 완료되었습니다.');
define('MSG_02', '비밀번호 설정이 완료되었습니다.');






# 관리자 페이지 CONSTANTS path
define('CSS_PATH', '/public/admin/css');
define('IMG_PATH', '/public/admin/img');
define('JS_PATH', '/public/admin/js');
define('MODULES_PATH', '/public/admin/modules');







# codeigniter 기본 세팅 
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

#채원만 추가
define('COM_NUMBER',"18995445");
define('COM_YESBIT_080',"무료거부0808556327인증3458");
define('COM_YESBIT_KEY',"7F5724409BB21A870C576B4AEB2DDC6D176473415CD638F3C2CDE8C91C132152");
define('CONNECTION_LIMIT',"0");


# SITE INFO
defined('SSL')				OR define('SSL', "http://");
defined('HTTP_HOST')		OR define('HTTP_HOST', $_SERVER['HTTP_HOST']);
defined('SITE_URL')			OR define('SITE_URL', SSL.HTTP_HOST);
defined('PAGE_URL')			OR define('PAGE_URL', SITE_URL.$_SERVER['REQUEST_URI']);
defined('SITE_NAME')		OR define('SITE_NAME', '노다지');
defined('SITE_TITLE')		OR define('SITE_TITLE', '노다지 - 뉴스보다 빠른 증권정보');


# POINT 내역 문구 정리
defined('MSG_POINT_BUY')				OR define('MSG_POINT_BUY', "포인트 구매 적립");
defined('MSG_POINT_CHARGE')				OR define('MSG_POINT_CHARGE', "증정 포인트 적립");
defined('MSG_POINT_ADMIN_CHARGE')		OR define('MSG_POINT_ADMIN_CHARGE', "관리자 포인트 증정 기능을 통한 포인트 증정");
defined('MSG_POINT_USE_FOR_STOCK')		OR define('MSG_POINT_USE_FOR_STOCK', "유료 추천종목 구매 사용");
defined('MSG_POINT_USE_FOR_CONTENTS')	OR define('MSG_POINT_USE_FOR_CONTENTS', "유료 컨텐츠 구매 사용");


defined('MSG_POINT_REFUND')				OR define('MSG_POINT_REFUND', "결제 취소로 인한 포인트 차감");



$smsPointPayTitle = "안녕하세요 노다지 포인트 구매가 완료 되었습니다.";
$smsPointPay = "안녕하세요 노다지 포인트 구매가 완료 되었습니다. 

다음 안내 사항을 꼭 확인해 주세요 

[포인트 사용 방법]
매일 새롭게 업데이트 되는
유료 추천종목 및 유료 콘텐츠를 구매할 수 있습니다. 

* 유료 추천종목 구매하기 : https://www.nodajistock.co.kr/stock
*유료 컨텐츠는 오픈 시 별도로 공지 예정입니다. 

[포인트 충전 내역 확인 방법]
구매하신 포인트는 ‘마이페이지’ 내 
‘포인트 구매 및 사용내역’
메뉴를 통해서 확인하실 수 있습니다. 

[포인트 사용 유의사항] 
* 유효기간
노다지 포인트의 유효기간은 최종사용일로부터 5년 입니다. 5년동안 사용내역이 없는 경우 금액에 관계없이 자동 소멸되며, 소멸 이후 사용 및 환불이 불가능합니다. 

* 환불 및 취소 
각종 이벤트 등을 통해 회사가 무료로 제공한 노다지 포인트는 환불이 불가합니다.
노다지 포인트를 이용하고 남은 잔액 중 유로로 구매한 포인트는 위약금 없이 환불하여 드립니다. (단, 환불수수료는 공제함)
노다지 포인트를 이용하고 남은 잔액은 재정경제부에서 고시한 <인터넷이용관련소비자피해보상>에서 규정한 환불수수료(결제대행수수료 등 기타 제반비용, 환전 금액의 10%)를 공제하고 환불됩니다.
* 결제일로 부터 7일 이내 미사용 환불 시 환불수수료가 차감되지 않습니다. 
단, 환불 대상 금액이 1,000원 미만인 경우 환불 불가(충전 시 결제 대행 수수료로 처리됩니다.) 
";
defined('MSG_POINT_BUY_SMS')	OR define('MSG_POINT_BUY_SMS', $smsPointPay);
defined('MSG_POINT_BUY_SMS_TITLE')	OR define('MSG_POINT_BUY_SMS_TITLE', $smsPointPayTitle);




/**
* @breif: DB TABLE 상수 선언
* @detail: _로 분리하고 미들네임이 주요 사용용도
* @author: csw
* @todo: 상품 테이블 이름 변경
*/

// 공통 테이블
defined('TABLE_PUSH_KEY')			OR define('TABLE_PUSH_KEY', "ndg_Push_Key");		// PUSH KEY 수집 테이블
defined('TABLE_SESSION')			OR define('TABLE_SESSION', "ndg_Session");			// SESSION KEY 수집 테이블
defined('TABLE_SITEINFO')			OR define('TABLE_SITEINFO', "ndg_Siteinfo");		// 사이트 정보 테이블
defined('TABLE_BANNER')				OR define('TABLE_BANNER', "ndg_Banner");			// 사이트 노출 배너 관리 테이블

// 상품 테이블
defined('TABLE_GOODS_SUBSCRIBE')	OR define('TABLE_GOODS_SUBSCRIBE', "goods");		// 정기구독 상품 테이블
defined('TABLE_GOODS_POINT')		OR define('TABLE_GOODS_POINT', "goods_point");		// 포인트 상품 테이블
defined('TABLE_GOODS_STOCK')		OR define('TABLE_GOODS_STOCK', "ndg_Stock");		// 추천종목 상품 테이블 (게시판형)

// 게시판 테이블
defined('TABLE_BOARD')				OR define('TABLE_BOARD', "ndg_Board");				// 게시판 테이블
defined('TABLE_BOARD_MAIN')			OR define('TABLE_BOARD_MAIN', "ndg_BoardMain");		// 게시판 상단노출 테이블
defined('TABLE_BOARD_REPLY')		OR define('TABLE_BOARD_REPLY', "ndg_BoardReply");	// 게시판 답변 리플 테이블
defined('TABLE_BOARD_TYPE')			OR define('TABLE_BOARD_TYPE', "ndg_BoardType");		// 게시판 분류별 이름 테이블
defined('TABLE_BOARD_CATEGORY')		OR define('TABLE_BOARD_CATEGORY', "ndg_Category");	// 게시판 분류별 카테고리(2depth) 테이블
defined('TABLE_BOARD_SCRAP')		OR define('TABLE_BOARD_SCRAP', "ndg_Scrap");		// 게시판 스크랩 기능 테이블
defined('TABLE_BOARD_SHARE')		OR define('TABLE_BOARD_SHARE', "ndg_Share");		// 게시판 공유하기 저장 테이블

// 광고 관련 테이블
defined('TABLE_AD_CAMPAIN')			OR define('TABLE_AD_CAMPAIN', "ndg_Campain");		// 캠페인 관리 테이블
defined('TABLE_AD_VISIT')			OR define('TABLE_AD_VISIT', "ndg_Visit");			// 캠페인 유입 관리 테이블

// 로그 관련 테이블
defined('TABLE_LOG_CHANGE')			OR define('TABLE_LOG_CHANGE', "ndg_ChangeLog");		// 회원 결제정보 변경 테이블
defined('TABLE_LOG_CRON')			OR define('TABLE_LOG_CRON', "ndg_CronMoniter");		// 정기 크론 동작 관제 테이블
defined('TABLE_LOG_PAYMENT')		OR define('TABLE_LOG_PAYMENT', "ndg_PaymentLog");	// 회원 정기결제 실패 로그 테이블
defined('TABLE_LOG_PUSH')			OR define('TABLE_LOG_PUSH', "ndg_Push_Log");		// 회원 푸시 발송 테이블
defined('TABLE_LOG_SMS')			OR define('TABLE_LOG_SMS', "ndg_Sms_Log");			// 회원 SMS 발송 테이블

// 결제 테이블
defined('TABLE_PAYMENT')			OR define('TABLE_PAYMENT', "ndg_Payment");			// 회원 결제 관리 테이블
defined('TABLE_PAYMENT_INFO')		OR define('TABLE_PAYMENT_INFO', "ndg_PaymentInfo");	// 회원 카드 결제정보 테이블
defined('TABLE_PAYMENT_REFUND')		OR define('TABLE_PAYMENT_REFUND', "ndg_Refund");	// 탈퇴 회원 신청 테이블

// 회원 테이블
defined('TABLE_USER')				OR define('TABLE_USER', "ndg_User");				// 회원 테이블
defined('TABLE_USER_LOGIN')			OR define('TABLE_USER_LOGIN', "ndg_UserLogin");		// 회원 로그인 LOG 테이블
//defined('TABLE_USER_POINT')			OR define('TABLE_USER_POINT', "ndg_UserPoint");		// 회원 포인트 사용 내역 테이블


// 포인트 테이블
defined('TABLE_USER_POINT')			OR define('TABLE_USER_POINT', "ndg_UserPointSum");		// 회원 테이블
defined('TABLE_USER_POINT_LOG')		OR define('TABLE_USER_POINT_LOG', "ndg_UserPointLog");	// 회원 테이블



// VIEW
defined('VIEW_LIST_TODAY')				OR define('VIEW_LIST_TODAY', "v_list_today");				// 게시판 테이블

