<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


# 클라이언트 main controller 
$route['main'] = 'main/main';
$route['login'] = 'main/login';
$route['auth'] = 'main/auth';
$route['logout'] = 'main/logout';
$route['join'] = 'main/join';
$route['joinComplete'] = 'main/joinComplete';
$route['joinNick'] = 'main/joinNick';
$route['loginOrigin'] = 'main/loginOrigin';
$route['fdPasswd'] = 'main/fdPasswd';

# info controller 
$route['key/(:any)'] = 'info/key/$1';
$route['boardView/(:any)/(:num)'] = 'info/boardView/$1/$2';
$route['community'] = 'info/community';
$route['community/reg'] = 'info/communityReg';
$route['community/reg/(:num)'] = 'info/communityReg/$1';

# mypage controller 
$route['profile'] = 'mypage/profile';
$route['notice'] = 'info/notice';

# payment controller
$route['pay/subscription'] = 'pay/index/subscription';
$route['pay/point/(:num)/(:num)/(:any)'] = 'pay/index/point/$1/$2/$3'; // 가격/stockNo
$route['pay/point_test/(:num)/(:num)/(:any)'] = 'pay/index/point_test/$1/$2/$3'; // 가격/stockNo
$route['pay/result'] = 'pay/result';
$route['pay/pointPay'] = 'pay/pointPay';

$route['pay/postStockSMS/(:any)/(:any)/(:any)'] = 'pay/postStockSMS/$1/$2/$3';


# stock controller 
$route['stock/view/(:any)/(:num)'] = 'stock/view/$1/$2';
$route['stock/period/(:any)/(:num)'] = 'stock/period/$1/$2';
$route['stock/news/(:any)/(:num)'] = 'stock/news/$1/$2';

//$route['test/(:any)'] = 'main/test/$1';
//$route['test'] = 'main/test';






# 관리자 페이지 ROUTER
$route['admin'] = 'admin/main';	# 메인 페이지
$route['admin/login'] = 'admin/main/login'; # 로그인 화면
$route['admin/loginCheck'] = 'admin/main/loginCheck'; # 로그인 처리
$route['admin/logOut'] = 'admin/main/logOut'; # 로그아웃 처리

$route['admin/user'] = 'admin/user/userList'; # 회원목록
$route['admin/payment'] = 'admin/payment/paymentList'; # 

$route['admin/payment/vip'] = 'admin/payment/paymentList/vip'; # 
$route['admin/payment/stock'] = 'admin/payment/paymentList/stock'; # 
$route['admin/payment/point'] = 'admin/payment/paymentList/point'; # 

$route['admin/payment/paylog'] = 'admin/payment/paylog'; # 
$route['admin/payment/refund'] = 'admin/payment/refund'; # 
$route['admin/payment/view/(:num)'] = 'admin/payment/view/$1'; # 


$route['admin/campain'] = 'admin/campain/manage'; 
$route['admin/campain/manage'] = 'admin/campain/manage'; # 캠페인 등록
$route['admin/campain/list'] = 'admin/campain/listCampain'; # 캠페인 등록




$route['admin/board'] = 'admin/board/bbsList/today'; # 
$route['admin/board/today'] = 'admin/board/bbsList/today'; # 
$route['admin/board/important'] = 'admin/board/bbsList/important'; # 
$route['admin/board/invest'] = 'admin/board/bbsList/invest'; # 
$route['admin/board/study'] = 'admin/board/bbsList/study'; # 
$route['admin/board/news'] = 'admin/board/bbsList/news'; # 
$route['admin/board/community'] = 'admin/board/bbsList/community'; # 
$route['admin/board/reply'] = 'admin/board/bbsList/reply'; # 
$route['admin/board/notice'] = 'admin/board/bbsList/notice'; # 
$route['admin/board/key'] = 'admin/board/bbsList/key'; # 
$route['admin/stock'] = 'admin/stock/stockList'; # 
$route['admin/stock/all'] = 'admin/stock/stockList'; # 
$route['admin/stock/period'] = 'admin/stock/stockList/period'; # 
$route['admin/stock/theme'] = 'admin/stock/stockList/theme'; # 
$route['admin/stock/rumor'] = 'admin/stock/stockList/rumor'; # 
$route['admin/stock/news'] = 'admin/stock/stockList/news'; # 
$route['admin/privacy'] = 'admin/privacy/privacy'; # 
$route['admin/privacy/setperm'] = 'admin/privacy/setPerm'; # 
$route['admin/banner'] = 'admin/banner/bannerList'; # 
$route['admin/adminsms'] = 'admin/adminsms/smsWrite'; # 
$route['admin/push'] = 'admin/push/pushWrite'; # 
$route['admin/point'] = 'admin/point/pointList'; # 

$route['admin/userLogin'] = 'admin/user/userLogin'; # 접속정보
$route['admin/join'] = 'admin/user/join'; # 회원가입
//$route['admin/setUserInfo'] = 'admin/user/setUserInfo'; # 회원가입