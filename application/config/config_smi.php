<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
  * 중요 설정 config
  *
  * @author 임정원 / 2020-02-14
  * @filesource
  *
*/

$config['auth_key'] = '';

$config['secret_key'] = 'ndg_smi13';

$config['payment_flag'] = 'live'; // test 환경

# 사용자 레벨설정
$config['level'] = array(
    1 => 'New',
    2 => 'Normal',
    3 => 'Basic',
    4 => 'Basic',
    5 => 'Standard',
    6 => 'Family',
    7 => 'Gold',
    8 => 'Premium',
    9 => 'Master',
);
$config['level_label'] = array(
    1 => '<span class="badge badge-dark">New</span>',
    2 => '<span class="badge badge-secondary">Normal</span>',
    3 => '<span class="badge badge-light">Basic</span>',
    4 => '<span class="badge badge-light">Basic</span>',
    5 => '<span class="badge badge-success">Standard</span>',
    6 => '<span class="badge badge-primary">Family</span>',
    7 => '<span class="badge badge-warning">Gold</span>',
    8 => '<span class="badge badge-info">Premium</span>',
    9 => '<span class="badge badge-danger">Master</span>',
);