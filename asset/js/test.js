

this.version = '1.0';
this.lastUpdate = '2020-03-08';

// Cmmo => 공통 function 모음 
var Cmmn = { 

	log : function (msg) { console.log( '[ NDG ] Debug Mode : ', msg ); },

	alertMsg : function (msg) {
		var $target = $('[data-remodal-id="alertMsg"]'), html = '';

		html += '<p>'+msg+'</p><div class="pop_bottom">'+
				'<button class="btn_m full" data-remodal-action="confirm">OK</button></div>';

		$('#alertMsg').html(html);
		$['remodal'].lookup[$target.data('remodal')].open(); // remodal.js 사용 
	},

	alertMsgRemodal : function (msg, tgid) {
		var $target = $('[data-remodal-id="alertMsg"]'), html = '';

		html += '<p>'+msg+'</p><div class="pop_bottom">'+
				'<button class="btn_m full" data-remodal-target="'+tgid+'">OK</button></div>';

		$('#alertMsg').html(html);
		$['remodal'].lookup[$target.data('remodal')].open(); // remodal.js 사용 
	},

	alertId : function (id) {
		var $target = $('[data-remodal-id=\"'+id+'\"]');
    	$['remodal'].lookup[$target.data('remodal')].open(); // remodal.js 사용 
	},

	checkAll : function (checkbox_id) {
		if($("#"+checkbox_id).is(":checked")) $('input:checkbox').prop('checked', true);
        else $('input:checkbox').prop('checked', false);
	},

};

// Is => 폼체크 function 모음 
var Is = {	

	phone : function (phone) {
		if ( !phone || phone == "" || phone == null || phone == undefined ) {
			Cmmn.alertMsg("전화번호를 작성해주세요"); return false;
		}
		if ( !(/^[0-9]+$/).test(phone) ){
	        Cmmn.alertMsg("숫자만 가능합니다."); return false;
	    }
	    if ( !(/^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/).test(phone) ){
	        Cmmn.alertMsg("휴대폰 번호를 확인해 주세요"); return false;
	    }
	    return true;
	},

	pwd : function (pwd) {
		/*
		var num = pwd.search(/[0-9]/g);		// 숫자체크 
		var eng = pwd.search(/[a-z]/ig);	// 영문체크 
		//var spe = pwd.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi); 특수문자
		if ( !pwd || pwd == "" || pwd == null || pwd == undefined ) {
			Cmmn.alertMsg("비밀번호를 작성해주세요 "); return false;
		}
		else if(pwd.length < 6 || pwd.length > 15){
			Cmmn.alertMsg("6자리 ~ 15자리 이내로 입력해주세요."); return false;
		}
		else if(pwd.search(/\s/) != -1){
		  	Cmmn.alertMsg("비밀번호는 공백 없이 입력해주세요."); return false;
		}
		else if(num < 0 || eng < 0 ){ //|| spe < 0 특문체크제외
			Cmmn.alertMsg("영문, 숫자 혼합하여 6자 이상 입력해주세요."); return false;
		}
		else { return true; }
		*/
	},

	name : function (name) {
		if ( !name || name == "" || name == null || name == undefined ) {
			Cmmn.alertMsg("이름을 작성해주세요."); return false;
		}
		return true;
	},

	code : function (num) {
		if ( !num || num == "" || num == null || num == undefined ) {
			Cmmn.alertMsg("인증번호를 작성해주세요."); return false;
		}
		return true;
	},

	checkBox : function (checkbox_id) {
		if( $("#"+checkbox_id).is(":checked") == false ) return false;
		else return true;
	},
	
}

// 쿠키 관련 function 
var Cookie = {

	get : function (name) {
        var value = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
		return value? value[2] : null;
    },

    set : function (name, value, exp) {
        var date = new Date();
		date.setTime(date.getTime() + exp*24*60*60*1000);
		document.cookie = name + '=' + value + ';expires=' + date.toUTCString() + ';path=/';
    },

    del : function (name) { document.cookie = name + '=; expires=Thu, 01 Jan 1999 00:00:10 GMT;'; },

}
