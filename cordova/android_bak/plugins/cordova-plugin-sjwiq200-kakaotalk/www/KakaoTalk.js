cordova.define("cordova-plugin-sjwiq200-kakaotalk.KakaoTalk", function(require, exports, module) {
var exec = require('cordova/exec');

var KakaoTalk = {
    share : function(options, successCallback, errorCallback) {
        exec(successCallback, errorCallback, 'KakaoTalk', 'share', [options]);
    }
};

module.exports = KakaoTalk;

});
