var app = {
	// Application Constructor
	initialize: function() {
		document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
	},
	onDeviceReady: function() {
		this.receivedEvent('deviceready');
	},
	receivedEvent: function(id) {


		FCMPlugin.getToken(function(success){
//			alert(success);
			localStorage.setItem("deviceKey", success);
			localStorage.setItem("deviceType", device.platform);
			var data = {
				'deviceKey' : success,
				'deviceType' : device.platform
			}
			$.post("/app/setDeviceInfo",data,function(response){
			},'json');

		});
		///////푸시 받았을때 실행 event
		FCMPlugin.onNotification(function(data){
			if(data.wasTapped){
				//탭하고 들어온경우
				if(typeof data.url != "undefined" && data.url != null && data.url != ""){
					location.href = data.url;
				}

			}else{
				//앱이 백그라운드에서 실행했을경우
				if(typeof data.url != "undefined" && data.url != null && data.url != ""){
					alert(data.url);
					location.href = data.url;
			  }
			}
		});
		cordova.plugins.InstallReferrer.open(
			function(success){
				//alert('Success Message: ' + success)
			}
			, function(error){
				//alert('Error Message: ' + error);
			}
		);
		if( localStorage.getItem("visitLogCheck") != "Y" ) {
			getReferrer();		
		}

	}
};
app.initialize();
function getReferrer() {
	if( localStorage.getItem("visitLogCheck") != "Y" ) {
		cordova.plugins.InstallReferrer.getParams(
			function(success){			
				var data = {
					'referer' : JSON.stringify(success)
				}

			//	alert(data.referer);
				$.ajax({
					type : "post",
					url : "/app/postReferer",
					dataType : 'json',
					data : data,
					success : function(response) {
						localStorage.setItem("visitLogCheck", 'Y');
					}, 
					error : function(error) { 
						console.log(JSON.stringify(error))
					}
				});
			}
			, function(error){
			//	alert('error')
				getReferrer();
			}
		);
	}
}
