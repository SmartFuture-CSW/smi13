var app = {
	// Application Constructor
	initialize: function() {
		document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
	},
	onDeviceReady: function() {
		this.receivedEvent('deviceready');
	},
	receivedEvent: function(id) {

		//window.plugins.sim.requestReadPermission(successCallback, errorCallback);

		FCMPlugin.getToken(function(success){
//			alert(success);
			localStorage.setItem("deviceKey", success);
			localStorage.setItem("deviceType", device.platform);
			var data = {
				'deviceKey' : success,
				'deviceType' : device.platform
			}
			$.post("/app/setDeviceInfo",data,function(response){
			},'json');

		});
		///////푸시 받았을때 실행 event
		FCMPlugin.onNotification(function(data){
			if(data.wasTapped){
				//탭하고 들어온경우
				if(typeof data.url != "undefined" && data.url != null && data.url != ""){
					location.href = data.url;
				}

			}else{
				//앱이 백그라운드에서 실행했을경우
				if(typeof data.url != "undefined" && data.url != null && data.url != ""){
					alert(data.url);
					location.href = data.url;
			  }
			}
		});
		cordova.plugins.InstallReferrer.open(
			function(success){
				//alert('Success Message: ' + success)
			}
			, function(error){
				//alert('Error Message: ' + error);
			}
		);
		if( localStorage.getItem("visitLogCheck") != "Y" ) {
			//window.plugins.sim.requestReadPermission(successCallback, errorCallback);
			getReferrer();		
		}
/*
		hasReadPermission();
		requestReadPermission();
		getSimInfo();
*/
	}
};
app.initialize();
var vPhone = '';

function successCallback(result) {
	if(result == "OK")
	{
		window.plugins.sim.getSimInfo(successCallback, errorCallback);
	}
	else if(result.phoneNumber)
	{
		vPhone = result.phoneNumber;
		getReferrer();
		//alert(result.phoneNumber);
	}
}
function errorCallback(error) {
//  alert('error : ' + error);
}


function getReferrer()
{
	if( localStorage.getItem("visitLogCheck") != "Y" ) {
		cordova.plugins.InstallReferrer.getParams(
			function(success){			


				var data = {
					'referer' : JSON.stringify(success)
					, 'vPhone' : vPhone
				}

			//	alert(data.referer);
				$.ajax({
					type : "post",
					url : "/app/postReferer",
					dataType : 'json',
					data : data,
					success : function(response) {
						localStorage.setItem("visitLogCheck", 'Y');
					}, 
					error : function(error) { 
						console.log(JSON.stringify(error))
					}
				});
			}
			, function(error){
			//	alert('error')
				getReferrer();
			}
		);
	}
}

//퍼미션확인
function hasReadPermission() {
	window.plugins.sim.hasReadPermission(
		function(result){
			alert('PS' + result);
		}
		,function(result){
			alert('PE' + result);
		}
	);
}

function getSimInfo(){
	//sim정보 가져오기
	window.plugins.sim.getSimInfo(
		function(result){
			alert('GS' + result);
		}
		,function(result){
			alert('GE' + result);
		}
	);
}


function requestReadPermission(){
	window.plugins.sim.requestReadPermission(
	function(result){
		alert('AS' + result);
	}
	,function(result){
		alert('AE' + result);
	});
}