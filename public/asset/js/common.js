/* Tabs */
(function(){
	var $tabContainer = $('.tabContainer');
	if(!$tabContainer) return;
	
	$tabContainer.on('click', '.tabButton', tabClickEvent);
	$tabContainer.on('keydown', '.tabList', tabKeyEvent);
	
	function activateTab(tab) {
		if(!tab) return;
		$(tab)
			.addClass('active')
			.attr({
				'tabindex': '0',
				'aria-selected': 'true'
			})
			.focus()
			.siblings()
				.removeClass('active')
				.attr({
					'tabindex': '-1',
					'aria-selected': 'false'
				})
	}
	
	function activateTabPanel(tab) {
		if(!tab) return;
		
		$('#' + tab.getAttribute('aria-controls'))
			
			.attr({
				'tabindex': '0'
			})
			.prop({
				'hidden': false
			})
			.addClass('active')
			.siblings('.tabPanel')
				.attr({
					'tabindex': '-1'
				})
				.prop({
					'hidden': true
				})
				.removeClass('active')
	}
	
	function tabClickEvent(e) {
		e = e || window.event;
		e.stopPropagation();
		var currTab = e.currentTarget;
		
		activateTab(currTab);
		activateTabPanel(currTab);
	}
	
	function tabKeyEvent(e) {
		e = e || window.event;
		e.stopPropagation();
		
	var keycode = e.keyCode || e.which;				
		
		switch(keycode) {
			case 37: //left arrow
				if(e.target.previousElementSibling) {
					$(e.target)
						.attr({
							'tabindex': '-1'
						})
						.prev()
							.attr({
								'tabindex': '0'
							})
							.focus()
				} else {
					$(e.target)
						.attr({
							'tabindex': '-1'
						})
						.siblings(':last')
							.attr({
								'tabindex': '0'
							})
							.focus()
				}
				break;
				
			case 39: //right arrow
				if(e.target.nextElementSibling) {
					$(e.target)
						.attr({
							'tabindex': '-1'
						})
						.next()
							.attr({
								'tabindex': '0'
							})
							.focus()
				} else {
					$(e.target)
						.attr({
							'tabindex': '-1'
						})
						.siblings(':first')
							.attr({
								'tabindex': '0'
							})
							.focus()
				}
				break;
			case 32: // spacebar
			case 13: // enter
				e.preventDefault();
				activateTab(e.target);
				activateTabPanel(e.target);
				break;
		}
	}
})();




/* Components */
(function(){
	/* Input only Number */
	$('.onlyNum').on('input', function(){
		$(this).val( $(this).val().replace(/\D/g,'') );
	});


	/* Textarea */
	$('.writing_box textarea').on('focus', function(){
		$(this).parent().addClass('focused');
	});
	$('.writing_box textarea').on('blur', function(){
		if ( !$(this).val() ) {
			$(this).parent().removeClass('focused');
		} else {
			return;
		}
	});


})();

